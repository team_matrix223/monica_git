﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;


public partial class manageHSNCode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CITIES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string Insert(int Id, string Title)
    {
        HSNCodeEntities objHSN = new HSNCodeEntities()
        {
            Id = Id,
            Title = Title.Trim().ToUpper(),


        };
        int status = new HSNBLL().InsertUpdate(objHSN);
        var JsonData = new
        {
            HSN = objHSN,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]

    public static string Delete(Int32 Id)
    {
        HSNCodeEntities objHSN = new HSNCodeEntities()
        {
            Id = Id,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new HSNBLL().DeleteHSN(objHSN);
        var JsonData = new
        {
            HSN = objHSN,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}