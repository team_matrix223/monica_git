﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managepages.aspx.cs" Inherits="managepages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
   <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="docsupport/prism.css">
    <link rel="stylesheet" href="chosen.css">

<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>
  <script language="javascript">

      doChosen();
      function doChosen() {

          var config = {
              '.chosen-select': {},
              '.chosen-select-deselect': { allow_single_deselect: true },
              '.chosen-select-no-single': { disable_search_threshold: 10 },
              '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
              '.chosen-select-width': { width: "95%" }
          }
          for (var selector in config) {
              $(selector).chosen(config[selector]);
          }

      }


      function ApplyRoles(Roles) {


          $("#<%=hdnRoles.ClientID%>").val(Roles);
      }
      function ResetControls() {
          var txtTitle = $("#txtTitle");
          var btnAdd = $("#btnAdd");
          var btnUpdate = $("#btnUpdate");
          $("#<%=ddlRoles.ClientID %> > option").attr("selected", false);
         $("#<%=ddlRoles.ClientID %>").trigger("chosen:updated");
         txtTitle.focus();
         txtTitle.val("");
         txtTitle.focus();
         var arrRole = [];
         arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
         btnAdd.css({ "display": "none" });
         for (var i = 0; i < arrRole.length; i++) {
             if (arrRole[i] == "1") {

                 $("#btnAdd").css({ "display": "block" });
                 
             }

         }


         $("#btnUpdate").css({ "display": "none" });


         $("#btnReset").css({ "display": "none" });
         $("#chkIsActive").prop("checked", "checked");

        

         $("#hdnId").val("0");
        
     }



     function TakeMeTop() {
         $("html, body").animate({ scrollTop: 0 }, 500);
     }

     function RefreshGrid() {
         $('#jQGridDemo').trigger('reloadGrid');

     }

     function InsertUpdate() {

       
         var btnAdd = $("#btnAdd");
         var btnUpdate = $("#btnUpdate");

         var Id = $("#hdnId").val();
         

         var Title = $("#txtTitle").val();
         if ($.trim(Title) == "") {
             $("#txtTitle").focus();
             //alert("Please enter Title");
             return;
         }

         $.uiLock('');
         if (Id == "0") {
             btnAdd.unbind('click');
         }
         else {
             btnUpdate.unbind('click');
         }



         var IsActive = false;


         if ($('#chkIsActive').is(":checked")) {
             IsActive = true;
         }
         var arrRoles = $('#<%=ddlRoles.ClientID %>').val();



         $.ajax({
             type: "POST",
             data: '{"id":"' + Id + '", "title": "' + Title + '","isActive": "' + IsActive + '","arrRoles":"' + arrRoles + '"}',
             url: "managepages.aspx/Insert",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);

                 if (obj.Status == 0) {
                     ResetControls();

                     if (Id == "0") {
                         btnAdd.bind('click', function () { InsertUpdate(); });
                     }
                     else {

                         btnUpdate.bind('click', function () { InsertUpdate(); });
                     }


                     alert("Insertion Failed.MembershipPage with duplicate name already exists.");
                     return;

                 }



                 if (obj.Status == -11) {
                     alert("You don't have permission to perform this action..Consult Admin Department.");
                     return;
                 }

                 if (Id == "0") {
                     btnAdd.bind('click', function () { InsertUpdate(); });

                     jQuery("#jQGridDemo").jqGrid('addRowData', obj.MembershipPage.MembershipPageId, obj.MembershipPage, "last");
                  
                     
                   
                     ResetControls();

                     alert("MembershipPage added successfully.");

                 }
                 else {

                     btnUpdate.bind('click', function () { InsertUpdate(); });
                    
                     var myGrid = $("#jQGridDemo");
                     var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                     myGrid.jqGrid('setRowData', selRowId, obj.MembershipPage);

                     alert("MembershipPage Updated successfully.");
                     ResetControls();

                 }

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {
                 $.uiUnlock();

             }



         });

     }




     $(document).ready(
  function () {

      BindGrid();


      ValidateRoles();

      function ValidateRoles() {

          var arrRole = [];
          arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

          for (var i = 0; i < arrRole.length; i++) {
              if (arrRole[i] == "1") {

                  $("#btnAdd").show();


                  $("#btnAdd").click(
     function () {

         InsertUpdate();


     }
     );

              }

              else if (arrRole[i] == "3") {

                  $("#btnUpdate").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Page is selected to Edit");
               return;
           }

           InsertUpdate();
       }
       );

              }

          }

      }

      



      $('#txtTitle').focus();
      $('#txtTitle').keypress(function (event) {


          var keycode = (event.keyCode ? event.keyCode : event.which);

          if (keycode == '13') {

              InsertUpdate();
          }


      });


      $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


  }
    );



     

 </script>


 <input type="hidden" id="hdnId" value="0"/>
   
  <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Membership Pages</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">


                          <form class="form-horizontal form-label-left"   runat="server" id="formID" method="post">
                               <asp:HiddenField ID="hdnRoles" runat="server"/>
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Add MemberShip Page </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="first-name">Title <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                           <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 213px"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="last-name">Roles <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <asp:DropDownList ID="ddlRoles" runat="server" data-placeholder="" multiple class="chosen-select" style="width:421px;">
                                             
                                             </asp:DropDownList>
                                       
                                            </div>
                                        </div>
                                       


                                         <div class="form-group">
                                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="last-name">Is Active 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                           
                                                  <input type="checkbox" data-index="2" id="chkIsActive" checked=checked  name="chkIsActive" />
                                            
                                       
                                            </div>
                                        </div>
                                       
										


                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <table >
                                            <tr>
                                           <td> <div id="btnAdd"  class="btn btn-primary btn-small" style="display:none;" ><i class="fa fa-external-link">Add Page</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i>Update Page</div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >

<i class="fa fa-trash m-right-xs"></i>Cancel</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                        </div>

                                    </form>
                                </div>
                            </div>


                                <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Membership Pages</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>

    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                            </form>
                        </div>
                    </div>
 
                 
                </div>
                <!-- /page content -->

               

            </div>


  
     <script type="text/javascript">


      
         function BindGrid() {
            
             jQuery("#jQGridDemo").GridUnload();
             
             jQuery("#jQGridDemo").jqGrid({

                      url: 'handlers/ManageMembershipPages.ashx',
                      ajaxGridOptions: { contentType: "application/json" },
                      datatype: "json",

                      colNames: ['Id', 'Title','IsActive','Roles'],
                      colModel: [
                                  { name: 'MembershipPageId', key: true, index: 'MembershipPageId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                  { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                
                                   { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                     { name: 'Roles', index: 'Roles', width: 200, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                      ],
                      rowNum: 10,

                      mtype: 'GET',
                      loadonce: true,
                      rowList: [10, 20, 30],
                      pager: '#jQGridDemoPager',
                      sortname: 'Bank_ID',
                      viewrecords: true,
                      height: "100%",
                      width: "400px",
                      sortorder: 'asc',
                      caption: "Pages List",

                      editurl: 'handlers/ManageMembershipPages.ashx',
                      ignoreCase: true,
                      toolbar: [true, "top"],


                  });


                  var   $grid = $("#jQGridDemo");
                  // fill top toolbar
                  $('#t_' + $.jgrid.jqID($grid[0].id))
                      .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                  $("#globalSearchText").keypress(function (e) {
                      var key = e.charCode || e.keyCode || 0;
                      if (key === $.ui.keyCode.ENTER) { // 13
                          $("#globalSearch").click();
                      }
                  });
                  $("#globalSearch").button({
                      icons: { primary: "ui-icon-search" },
                      text: false
                  }).click(function () {
                      var postData = $grid.jqGrid("getGridParam", "postData"),
                          colModel = $grid.jqGrid("getGridParam", "colModel"),
                          rules = [],
                          searchText = $("#globalSearchText").val(),
                          l = colModel.length,
                          i,
                          cm;
                      for (i = 0; i < l; i++) {
                          cm = colModel[i];
                          if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                              rules.push({
                                  field: cm.name,
                                  op: "cn",
                                  data: searchText
                              });
                          }
                      }
                      postData.filters = JSON.stringify({
                          groupOp: "OR",
                          rules: rules
                      });
                      $grid.jqGrid("setGridParam", { search: true });
                      $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                      return false;
                  });









                  $("#jQGridDemo").jqGrid('setGridParam',
       {
           onSelectRow: function (rowid, iRow, iCol, e) {

               validateForm("detach");
               var txtTitle = $("#txtTitle");


                var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                      $("#btnUpdate").css({ "display": "none" });
                      $("#btnReset").css({ "display": "none" });
                      $("#btnAdd").css({ "display": "none" });
                  

                      for (var i = 0; i < arrRole.length; i++) {

                          if (arrRole[i] == 1) {

                              $("#btnAdd").css({ "display": "block" });
                          }

                          if (arrRole[i] == 3) {

               $("#hdnId").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MembershipPageId'));

               var strRoles = $('#jQGridDemo').jqGrid('getCell', rowid, 'Roles');
               var parts = strRoles.split(',');

               $("#<%=ddlRoles.ClientID %> > option").attr("selected", false);

                 for (var i = 0; i < parts.length; i++) {

                     $('#<%=ddlRoles.ClientID %> option[value=' + parts[i] + ']').prop('selected', 'selected');
                 }
                 $("#<%=ddlRoles.ClientID %>").trigger("chosen:updated");



                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                     $('#chkIsActive').prop('checked', true);
                 }
                 else {
                     $('#chkIsActive').prop('checked', false);

                 }
                 txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                 txtTitle.focus();
                 $("#btnAdd").css({ "display": "none" });
                 $("#btnUpdate").css({ "display": "block" });
                 $("#btnReset").css({ "display": "block" });
                 }

                 }
                 TakeMeTop();
             }
         });

                    var DataGrid = jQuery('#jQGridDemo');
                    DataGrid.jqGrid('setGridWidth', '500');

                    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                                     {
                                         refresh: false,
                                         edit: false,
                                         add: false,
                                         del: false,
                                         search: false,
                                         searchtext: "Search",
                                         addtext: "Add",
                                     },

                                     {//SEARCH
                                         closeOnEscape: true

                                     }

                                       );



                }





    </script>







            <script type="text/javascript">
                var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': { allow_single_deselect: true },
                    '.chosen-select-no-single': { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width': { width: "95%" }
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }

  </script>

  

</asp:Content>

