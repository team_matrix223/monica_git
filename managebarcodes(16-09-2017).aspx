﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebarcodes.aspx.cs" Inherits="managebarcodes" %>
<%@ Register src="~/Templates/barcode.ascx" tagname="PrintBarcode" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
    <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <link href="<%=CommonFunctions.SiteUrl()%>css/tabcontent.css" rel="stylesheet" type="text/css" />
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
     
    <script language="javascript" type="text/javascript">
        var m_ItemId = 0;
        var BestExp = 0;
        var Day = 0;

        function BindInformation(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;


                    $("#txtItemCOde").val(obj.ItemOptions.Item_Code);
                    $("#txtItemName").val(obj.ItemOptions.Item_Name);
                    $("#txtMRP").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtSaleRate").val(obj.ItemOptions.Sale_Rate);
                    $("#txtExpDate").val(obj.ItemOptions.strExpDate);
                    BestExp = obj.ItemOptions.BestExp;
                    Day = obj.ItemOptions.Day;
                    $("#txtDay").val(obj.ItemOptions.Day);
                    if (BestExp == true) {
                        $("#<%=lbltype.ClientID %>").html("Consumed Day");
                        $("#txtDay").css("display", "block");
                        $("#txtExpDate").css("display", "none");
                    }
                    else {
                        $("#<%=lbltype.ClientID %>").html("Choose Date");
                        $("#txtDay").css("display", "none");
                        $("#txtExpDate").css("display", "block");
                    }


                    $("#ProductDialog").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        modal: true
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }



        function BindBatchInformation(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;


                    $("#txtbatchCode").val(obj.ItemOptions.Item_Code);
                    $("#txtbatchName").val(obj.ItemOptions.Item_Name);
                    $("#txtbatchMrp").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtbatchSaleRate").val(obj.ItemOptions.Sale_Rate);
        
                    $("#DvBatch").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        modal: true
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }



        $(document).ready(
    function () {



        $('#txtbatchDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $("#txtbatchDate").val($("#<%=hdnDate.ClientID%>").val());

        $('#txtExpDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        BindGrid();

        $("#btnBarcode").click(
                                        function () {

                                            if (m_ItemId == 0) {
                                                alert("No Product is selected for Barcode Printing");
                                                return;
                                            }


                                            var ItemId = m_ItemId;


                                            BindInformation(ItemId);


                                        }
                                        );



        $("#btnBatch").click(
                                        function () {

                                            if (m_ItemId == 0) {
                                                alert("No Product is selected for Barcode Printing");
                                                return;
                                            }


                                            var ItemId = m_ItemId;


                                            BindBatchInformation(ItemId);


                                        }
                                        );



        $("#btnPrint").click(
            function () {


                var Qty = $("#txtQty").val();
                var Date = $("#txtExpDate").val();
                var Day1 = $("#txtDay").val();


                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

                iframe = document.createElement("iframe");
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);

                window.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1;
                //document.getElementById('reportout').contentWindow.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId;
                //alert("hi");

            });





        $("#btnBatchPrint").click(
            function () {

                var Qty = $("#txtbatchqty").val();
                var Name = $("#txtbatchName").val();
                var MRP = $("#txtbatchMrp").val();
                var Date = $("#txtbatchDate").val();
                var BatchNo = $("#txtbatchNo").val();
               
                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

                iframe = document.createElement("iframe");
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);

                window.location = 'ReportBatch.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&MRP=' + MRP + '&MfdDate=' + Date + '&BatchNo=' + BatchNo + '&Name=' + Name;
                //document.getElementById('reportout').contentWindow.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId;
                //alert("hi");

            });



    });
    
    </script>


      <style type="text/css">
        .table
        {
            margin: 5px;
        }
    </style>
<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:HiddenField ID="hdnDate" runat="server"/>
 <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>BARCODES</h3>
                        </div>
                       
      
                    <div class="x_panel">
                         <div class="form-group">
                                
                              

                <div class="youhave"  >
                     <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnBarcode" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Barcode</div></td>
                                       <td style="padding-top:5px"> <div id="btnBatch" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Batch Printing</div></td>
                                            </tr>

                                         
                                            </table>
    <div id="jQGridDemoPager">
    </div>

    
                </div>

<div class="row" id="DvBatch" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="tblbatch" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchCode" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchName" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchMrp" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchSaleRate" disabled="disabled"> </td>
                                    
                                    </tr>

                                   <%-- <tr>
                                    <td style="width:60px"><asp:Label ID="Label1" runat="server"></asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="Text5" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="Text6" >
                                    </td>
                                  
                                    </tr>--%>

                                      <tr>
                                    <td style="width:60px"> <label>BatchNo 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchNo" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>


                                     <tr>
                                    <td style="width:60px"> <label>Mfd Date: 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchDate" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>


                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchqty" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btnBatchPrint" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>



                            
<div class="row" id="ProductDialog" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="formbarcode" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemCOde" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemName" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtMRP" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtSaleRate" disabled="disabled"> </td>
                                    
                                    </tr>

                                    <tr>
                                    <td style="width:60px"><asp:Label ID="lbltype" runat="server"></asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtExpDate" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtDay" >
                                    </td>
                                  
                                    </tr>





                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtQty" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btnPrint" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>


                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>

</div>
  <iframe id="reportout" width="0" height="0"></iframe>
</form>



     <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageProducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ItemId','MasterCode', 'ItemCode', 'ItemName', 'Barcode',   'SalesInUnit', 'PurchaseRate', 'SaleRate', 'MRP'],
                           
                        colModel: [
                                    { name: 'ItemID', key: true, index: 'ItemID', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                    { name: 'Master_Code', index: 'Master_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Code', index: 'Item_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                    { name: 'Bar_Code', index: 'Bar_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    
                                    
                                    { name: 'Sales_In_Unit', index: 'Sales_In_Unit', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                     { name: 'Purchase_Rate', index: 'Purchase_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Sale_Rate', index: 'Sale_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ItemID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Items List",

                        editurl: 'handlers/ManageProducts.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {

                onSelectRow: function (rowid, iRow, iCol, e) {

                    m_ItemId = 0;

                           m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');
                      

                           
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>

