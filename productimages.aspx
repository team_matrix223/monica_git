﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productimages.aspx.cs" Inherits="productimages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
         <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/css.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>

  
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
  
 

  
     <script type="text/javascript">
         var m_PhotoUrl = "";
         var PStatus = 1;


         function uploadFile(ProductId) {

             document.getElementById("img" + ProductId).src = "productimages/loader.gif";
        
 
             var fd = new FormData();
             fd.append("ProductId", ProductId);
             var count = document.getElementById('fu' + ProductId).files.length;

             for (var index = 0; index < count; index++) {

                 var file = document.getElementById('fu' + ProductId).files[index];
                 

                 fd.append(file.name, file);

             }


            
             var xhr = new XMLHttpRequest();

             xhr.upload.addEventListener("progress", uploadProgress, false);

             xhr.addEventListener("load", uploadComplete, false);

             xhr.addEventListener("error", uploadFailed, false);

             xhr.addEventListener("abort", uploadCanceled, false);

          
             xhr.open("POST", "savetofile.aspx");

             xhr.send(fd);



         }

         function uploadProgress(evt) {

             if (evt.lengthComputable) {

                 var percentComplete = Math.round(evt.loaded * 100 / evt.total);

                 document.getElementById('progress').innerHTML = percentComplete.toString() + '%';

             }

             else {

                 document.getElementById('progress').innerHTML = 'unable to compute';

             }

         }

         function uploadComplete(evt) {


         

             document.getElementById("img" + evt.target.responseText).src ="productimages/T_"+ evt.target.responseText + ".jpg";
        
 

         

            /// alert(evt.target.responseText);

         }

         function uploadFailed(evt) {

             alert("There was an error attempting to upload the file.");
             $.uiUnlock();
         }

         function uploadCanceled(evt) {

             alert("The upload has been canceled by the user or the browser dropped the connection.");
             $.uiUnlock();
         }
 
 



      $(document).ready(
      function () {


          $("#ddlCategories").change(
          function () {

              Search($(this).val(), "");
          }
          );





          function Search(CatId, Keyword) {



              $.ajax({
                  type: "POST",
                  data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
                  url: "productimages.aspx/AdvancedSearch",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      var obj = jQuery.parseJSON(msg.d);

                      $("#products").html(obj.productData);

                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {

                  }

              }
        );


          }




          $.ajax({
              type: "POST",
              data: '{}',
              url: "productimages.aspx/BindCategories",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);


                  $("#ddlCategories").html(obj.categoryData);


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }

          }
        );


      }

      );
  </script>

</head>
<body style="background:white">
       
    <form id="form1" runat="server">
    <div>
    <br />
    <br />

    <table style="width:100%">
    <tr><td align="center">
    <table>
    <tr><td> <div id="progress" style="font-size:20px;font-weight:bold;;text-align:center"></div></td></tr>
    <tr><td> <select id="ddlCategories">
    
    </select></td></tr>
    </table>
   
    </td></tr>
    <tr>
    <td>
    <div id="products">
    
    
    </div>
    
    
    </td>
    </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
