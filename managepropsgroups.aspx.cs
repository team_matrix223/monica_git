﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managepropsgroups : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDepartments();
        }
        CheckRole();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.SUBGROUPS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()

                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    void BindDepartments()
    {

        ddlDepartment.DataSource = new DepartmentBLL().GetAll();
        ddlDepartment.DataValueField = "PROP_ID";
        ddlDepartment.DataTextField = "PROP_NAME";
        ddlDepartment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Department--";
        li1.Value = "0";
        ddlDepartment.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string Insert(int SGroupId, string SGroupName,  bool ShowInMenu, bool IsActive,int DepartmentId,int GroupId)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.SUBGROUPS));

        string[] arrRoles = sesRoles.Split(',');


        if (SGroupId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        PropSGroups objPropSGroups = new PropSGroups()
        {
            SGroup_ID = SGroupId,
            SGroup_Name = SGroupName.Trim().ToUpper(),
           
            ShowInMenu = ShowInMenu,
            IsActive = IsActive,
            Department_Id = DepartmentId,
            Group_Id = GroupId,
            UserId = Id,

        };
        status = new PropSGroupBLL().InsertUpdate(objPropSGroups);
        var JsonData = new
        {
            SubGroups = objPropSGroups,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindGroups(int DepartmentId)
    {

        string Group = new PropGroupBLL().GetOptionsByDepartmentId(DepartmentId);


        


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            GroupOptions = Group,
           

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 SGroupId)
    {

        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.SUBGROUPS));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        PropSGroups objPropSGroup = new PropSGroups()
        {
            SGroup_ID = SGroupId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new PropSGroupBLL().DeletePropSGroup(objPropSGroup);
        var JsonData = new
        {
            PropSgroup = objPropSGroup,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}