﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;


public partial class managepurchasereturn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdntodaydate.Value = DateTime.Now.ToShortDateString();
            BindDropDownList();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
            ddlSupplier.DataSource = new PurchaseBLL().GetAllSupplier();
            ddlSupplier.DataTextField = "CName";
            ddlSupplier.DataValueField = "CCode";
            ddlSupplier.DataBind();
            ListItem li = new ListItem();
            li.Text = "--SELECT--";
            li.Value = "0";
            ddlSupplier.Items.Insert(0, li);
            ddlGodown.DataSource = new GodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_Id";
            ddlGodown.DataBind();
            ddlGodown.Items.Insert(0, li);


           

               
                string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

                if (strDate == "")
                {

                    Response.Redirect("index.aspx?DayOpen=Close");
                }

            

        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASERETURN));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string BindServices()
    {
        string serviceData = new ProductBLL().GetActiveOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;

        var JsonData = new
        {
            ServiceOptions = serviceData,


        };
        return ser.Serialize(JsonData);
    }

    public void BindDropDownList()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new ProductBLL().GetPurchaseOptions2(hdnProducts, Branch);
    }

    [System.Web.Services.WebMethod]
    public static string GetServerDate(string format)
    {
        if (format.Equals("utc"))
        {
            return DateTime.Now.ToUniversalTime().ToString();
        }
        else
        {
            return DateTime.Now.ToLocalTime().ToString();
        }
    }







    [WebMethod]
    public static string GetByTaxStructure(decimal TaxRate)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_Rate = TaxRate
        };

        new TaxStructureBLL().GetTaxStructure(objTaxStructure, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            PurchaseData = objTaxStructure,


        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindProducts()
    {
        return "Hello";
        // string productData = new ProductBLL().GetPurchaseOptions();
        //return productData;

        //JavaScriptSerializer ser = new JavaScriptSerializer();
        //ser.MaxJsonLength = int.MaxValue;

        //var JsonData = new
        //{
        //    ProductOptions = productData,


        //};
        //return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Insert(string GrnDate, string BillNo, string BillDate, string GrNo, string GrDate,
        string VehNo, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
         string dis1Arr, string dis2Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr,
        string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, string arrExcise, decimal SurchrgAmt, string ExciseAmtArr,string SchemeArr
        )
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASERETURN));

        string[] arrRoles = sesRoles.Split(',');


        if (GrNo == "0")
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        PurchaseReturn objPurchase = new PurchaseReturn()
        {

            GRNDate = Convert.ToDateTime(GrnDate),
            BillNo = BillNo,
            BillDate = Convert.ToDateTime(BillDate),
            GRNo = GrNo,
            GRDate = Convert.ToDateTime(GrDate),
            VehNo = VehNo,
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            SurchrgAmt = SurchrgAmt,
            TaxAmt = taxamt,
            BranchId = Branch,
            UserNo = UserNo,

        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
  
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');
        string[] Excisedata = arrExcise.Split(',');
        string[] ExciseAmtdata = ExciseAmtArr.Split(',');
        string[] Schemedata = SchemeArr.Split(',');
      
    

        DataTable dt = new DataTable();
        dt.Columns.Add("GrnNo");
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("Excise");
        dt.Columns.Add("ExciseAmt");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
        dt.Columns.Add("ItemMargin");
        dt.Columns.Add("ItemBasRateWTax");
        dt.Columns.Add("surval");
        dt.Columns.Add("Scheme");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");




        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["GrnNo"] = Convert.ToInt16(1);
            dr["OrderNo"] = "0";
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = Convert.ToDecimal(dis2Data[i]);
            dr["Excise"] = Convert.ToDecimal(Excisedata[i]);
            dr["ExciseAmt"] = Convert.ToDecimal(ExciseAmtdata[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);
            dr["Free"] = 0;
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);
            dr["ItemMargin"] = 0;
            dr["ItemBasRateWTax"] = Convert.ToDecimal(dis2Data[i]);
            dr["surval"] = Convert.ToDecimal(dis2Data[i]);
            dr["Scheme"] = Convert.ToBoolean(Schemedata[i]);
            dt.Rows.Add(dr);

        }
        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }

        status = new PurchaseReturnBLL().InsertUpdate(objPurchase, dt, dt1);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Purchase = objPurchase

        };
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string Update(int GrnNo, string GrnDate, string BillNo, string BillDate, string GrNo, string GrDate,
        string VehNo, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
         string dis1Arr, string dis2Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr,
        string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, string arrExcise, decimal SurchrgAmt, string ExciseAmtArr, string SchemeArr
        )
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASERETURN));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -11;
        }
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        PurchaseReturn objPurchase = new PurchaseReturn()
        {
            GrnNo = Convert.ToInt32(GrnNo),
            GRNDate = Convert.ToDateTime(GrnDate),
            BillNo = BillNo,
            BillDate = Convert.ToDateTime(BillDate),
            GRNo = GrNo,
            GRDate = Convert.ToDateTime(GrDate),
            VehNo = VehNo,
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            SurchrgAmt = SurchrgAmt,
            TaxAmt = taxamt,
            UserNo = UserNo,


        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');

        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');
        string[] Excisedata = arrExcise.Split(',');
        string[] ExciseAmtdata = ExciseAmtArr.Split(',');
        string[] Schemedata = SchemeArr.Split(',');



        DataTable dt = new DataTable();
        dt.Columns.Add("GrnNo");
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("Excise");
        dt.Columns.Add("ExciseAmt");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
        dt.Columns.Add("ItemMargin");
        dt.Columns.Add("ItemBasRateWTax");
        dt.Columns.Add("surval");
        dt.Columns.Add("Scheme");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");




        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["GrnNo"] = Convert.ToInt16(1);
            dr["OrderNo"] = "0";
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = Convert.ToDecimal(dis2Data[i]);
            dr["Excise"] = Convert.ToDecimal(Excisedata[i]);
            dr["ExciseAmt"] = Convert.ToDecimal(ExciseAmtdata[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);
            dr["Free"] = 0;
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);
            dr["ItemMargin"] = 0;
            dr["ItemBasRateWTax"] = Convert.ToDecimal(dis2Data[i]);
            dr["surval"] = Convert.ToDecimal(dis2Data[i]);
            dr["Scheme"] = Convert.ToBoolean(Schemedata[i]);
            dt.Rows.Add(dr);

        }
        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }


        status = new PurchaseReturnBLL().InsertUpdate(objPurchase, dt, dt1);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Purchase = objPurchase

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindPurchaseDetail(int pid)
    {
        int cntr = 0;
        string serviceData = new PurchaseReturnBLL().GetPurchaseDetailOptions(pid, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }










    [WebMethod]
    public static string InsertGodown(string title)
    {

        Godowns objGodowns = new Godowns()
        {
            Godown_Name = title.Trim().ToUpper()

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new GodownsBLL().InsertUpdate(objGodowns);
        var JsonData = new
        {
            Godown = objGodowns,
            Status = status
        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string FillSettings()
    {
        string RPRate;
        string RSRate;
        string RMRP;
        string RAmt;
        string RDis1;
        string RDis2;
        string RTax1;
        string RTax2;

        new CommonSettingsBLL().GetPurchaseReturnSettings(out  RPRate, out  RSRate, out  RMRP, out  RAmt, out  RDis1, out  RDis2, out  RTax1, out  RTax2);
        var JsonData = new
        {

            RPRate = RPRate,
            RSRate = RSRate,
            RMRP = RMRP,
            RAmt = RAmt,
            RDis1 = RDis1,
            RDis2 = RDis2,
            RTax1 = RTax1,
            RTax2 = RTax2,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}