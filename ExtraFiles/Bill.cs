﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Bill
/// </summary>
public class Bill
{
  
    public string Bill_Prefix { get; set; }
    public int Bill_No { get; set; }
    public string BillNowPrefix { get; set; }
    public DateTime Bill_Date { get; set; }
    public string Customer_ID { get; set; }
    public string Customer_Name { get; set; }
    public decimal Bill_Value { get; set; }
    public decimal DiscountPer { get; set; }
    public decimal Less_Dis_Amount { get; set; }
    public decimal Add_Tax_Amount { get; set; }
    public decimal Net_Amount { get; set; }
    public string BillMode { get; set; }
    public string CreditBank { get; set; }
    public int UserNO { get; set; }
    public int Bill_Type { get; set; }
    public decimal Cash_Amount { get; set; }
    public decimal Credit_Amount { get; set; }
    public decimal CrCard_Amount { get; set; }
    public decimal Round_Amount { get; set; }
    public bool Bill_Printed { get; set; }
    public bool Passing { get; set; }
    public int CashCust_Code { get; set; }
    public string CashCust_Name { get; set; }
    public decimal Tax_Per { get; set; }
    public int Godown_ID { get; set; }
    public DateTime ModifiedDate { get; set; }
    public decimal R_amount { get; set; }
    public int tokenno { get; set; }
    public int tableno { get; set; }
    public string remarks { get; set; }
    public decimal servalue { get; set; }
    public int ReceiviedGRNNo { get; set; }
    public int EmpCode { get; set; }
    public int OrderNo { get; set; }
    public int BranchId { get; set; }
    public bool Surval { get; set; }
    public string strBD { get { return Bill_Date.ToString("d"); } }
    public string strMD { get { return ModifiedDate.ToString("d"); } }

    public string CashCustAddress { get; set; }
    public string CreditCustAddress { get; set; }
    public string BillType { get; set; }
    public decimal OnlinePayment { get; set; }
    public decimal DeliveryCharges { get; set; }
    public string BillTime { get; set; }
    public string LocalOut { get; set; }
    public string TransportMode { get; set; }
    public string VehicleNo { get; set; }

    public Bill()
    {
        TransportMode = "";
        VehicleNo = "";
        LocalOut = "";
        CashCustAddress = string.Empty;
        CreditCustAddress = string.Empty;
        BillTime = "";
        Bill_Prefix = "";
        Bill_No = 0;
        BillNowPrefix = "";
        Bill_Date = DateTime.Now;
        Customer_ID = "";
        Customer_Name = "";
        Bill_Value = 0;
        DiscountPer = 0;
        Less_Dis_Amount = 0;
        Add_Tax_Amount = 0;
        Net_Amount = 0;
        BillMode = "";
        CreditBank = "";
        UserNO = 0;
        Bill_Type = 0;
        Cash_Amount = 0;
        Credit_Amount = 0;
        CrCard_Amount = 0;
        Round_Amount = 0;
        Bill_Printed = false;
        Passing = false;
        CashCust_Code = 0;
        CashCust_Name = "";
        Tax_Per = 0;
        Godown_ID = 0;
        ModifiedDate = DateTime.Now;
        R_amount = 0;
        tokenno = 0;
        tableno = 0;
        remarks = "";
        servalue = 0;
        ReceiviedGRNNo = 0;
        EmpCode = 0;
        BranchId = 0;
        BillType = "";
        OnlinePayment = 0;
        DeliveryCharges = 0;


    }
}