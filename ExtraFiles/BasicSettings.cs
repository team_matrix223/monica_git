﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicSettings
/// </summary>
public class BasicSettings
{

    public bool service_tax { get; set; }
    public decimal tax_per { get; set; }
    public bool homedel_charges { get; set; }
    public decimal min_bill_value { get; set; }
    public decimal del_charges { get; set; }
    public string retail_bill { get; set; }
    public string vat_bill { get; set; }
    public string cst_bill { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }
    public bool AlloServicetax_TakeAway { get; set; }
    public bool CouponPrinting { get; set; }
    public bool CouponPrintingDeptWise { get; set; }
    public bool BarcodePrintOnBill { get; set; }
    public string UTorS { get; set; }

    public string PrintCopies { get; set; }
    public bool TaxBiferication { get; set; }
    public string SiteUrl { get; set; }

	public BasicSettings()
	{
       
        service_tax = false;
        tax_per = 0;
        homedel_charges = false;
        min_bill_value = 0;
        del_charges = 0;
        retail_bill = "";
        vat_bill = "";
        cst_bill = "";
        UserId = 0;
        BranchId = 0;
        AlloServicetax_TakeAway = false;
        CouponPrinting = false;
        BarcodePrintOnBill = false;
        CouponPrintingDeptWise = false;
        UTorS = "";
       
        TaxBiferication = false;
        SiteUrl = "";
	}
}