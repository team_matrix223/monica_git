﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BranchDAL
/// </summary>
public class BranchDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BranchGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Branches objBranch)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@BranchId ", objBranch.BranchId);
        objParam[1] = new SqlParameter("@BranchName", objBranch.BranchName);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objBranch.IsActive);
        objParam[4] = new SqlParameter("@UserId", objBranch.UserId);
        objParam[5] = new SqlParameter("@StateId", objBranch.StateId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BranchInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objBranch.BranchId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetById(Branches objBranch)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objBranch.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BranchGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}