﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BasicSettingsDAL
/// </summary>
public class BasicSettingsDAL:Connection
{
    public SqlDataReader GetMasterSettings(BasicSettings objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objSetting.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBasic", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(BasicSettings objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[19];

        objParam[0] = new SqlParameter("@service_tax", objSettings.service_tax);
        objParam[1] = new SqlParameter("@tax_per", objSettings.tax_per);
        objParam[2] = new SqlParameter("@homedel_charges", objSettings.homedel_charges);
        objParam[3] = new SqlParameter("@min_bill_value", objSettings.min_bill_value);
        objParam[4] = new SqlParameter("@del_charges", objSettings.del_charges);
        objParam[5] = new SqlParameter("@retail_bill", objSettings.retail_bill);
        objParam[6] = new SqlParameter("@vat_bill", objSettings.vat_bill);
        objParam[7] = new SqlParameter("@cst_bill", objSettings.cst_bill);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[10] = new SqlParameter("@BranchId", objSettings.BranchId);
        objParam[11] = new SqlParameter("@AlloServicetax_TakeAway", objSettings.AlloServicetax_TakeAway);
        objParam[12] = new SqlParameter("@CouponPrinting", objSettings.CouponPrinting);
        objParam[13] = new SqlParameter("@CouponPrintingDept", objSettings.CouponPrintingDeptWise);
        objParam[14] = new SqlParameter("@BarcodePrintOnBill", objSettings.BarcodePrintOnBill);
        objParam[15] = new SqlParameter("@SorUT", objSettings.UTorS);
        objParam[16] = new SqlParameter("@PrintCopies", objSettings.PrintCopies);
        objParam[17] = new SqlParameter("@TaxBiferication", objSettings.TaxBiferication);
        objParam[18] = new SqlParameter("@SiteUrl", objSettings.SiteUrl);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersetting_Basic", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}