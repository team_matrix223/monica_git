﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Data;

public partial class BillScreen : System.Web.UI.Page
{
    public string m_getdate { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            ltDateTime.Text ="<span style='font-weight:bold;'>Today - "+ String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now)+"</span>" ;
            m_getdate = DateTime.Now.ToString("ddddd,MMMM-dd-yyyy"); 
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
			gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
           
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }
            BindCreditCustomers();
            BindOtherPayment();
        }

        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString() | m == Convert.ToInt16(Enums.Roles.RETURN).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string UpdateGSTBill(string LocalOut, string BillNowPrefix, Boolean BillType, string CustomerId, string CustomerName)
    {

        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new BillDAL().UpdateGSTBill(LocalOut, BillNowPrefix, BillType, CustomerId, CustomerName, UserNo);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {


        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string InsertOnlineOtherPayment(int ID, int OtherPayment_ID, string Bill_No, string CoupanNo)
    {
        string[] ItemCode = CoupanNo.Split(',');
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        if (ID == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Bill objOnlineOtherPayment = new Bill();
        for (int i = 0; i < ItemCode.Length; i++)
        {
            objOnlineOtherPayment = new Bill()
            {
                ID = ID,
                OtherPayment_ID = Convert.ToInt32(OtherPayment_ID),

                BillNowPrefix = Bill_No,
                CoupanNo = Convert.ToString(ItemCode[i]),

            };
            status = new BillBLL().InsertOnlineOtherPayment(objOnlineOtherPayment);
        }




        var JsonData = new
        {
            bill = objOnlineOtherPayment,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string CheckReturn()
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.RETURN).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -12;
        }
        else
        {
            status = 1;
        }

        
        JavaScriptSerializer ser = new JavaScriptSerializer();

       
        var JsonData = new
        {
           
            Status = status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindEmployees()
    {

        string EmployeesData = new EmployeeBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            EmployeeOptions = EmployeesData

        };
        return ser.Serialize(JsonData);
    }


    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string LoadUserControl(string counter)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
            (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }


    void BindOtherPayment()
    {

        ddlOtherPayment.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOtherPayment.DataValueField = "OtherPayment_ID";
        ddlOtherPayment.DataTextField = "OtherPayment_Name";
        ddlOtherPayment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Other--";
        li1.Value = "0";
        ddlOtherPayment.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string Reprint(string PrintType, string BillNowPrefix)
    {        
        
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new CommonMasterDAL().Reprint(UserNo,PrintType,Branch,BillNowPrefix);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
             

        };
        return ser.Serialize(JsonData);

    }




    [WebMethod]
    public static string GetLastBill()
    {
        Bill objBill = new Bill();
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objBill.UserNO = UserNo;
        new BillBLL().GetLastBill(objBill);
        var JsonData = new
        {

            lastrecord = objBill,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

   

    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(string BillNowPrefix)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


       
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        if (roles.Count() == 0)
        {
            Status = -10;
        }
        else
        {
           Status = new BillBLL().DeleteBill(objBill);
        }
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    //[WebMethod]
    //public static string InsertKOT(int TableID, int PaxNo, decimal Value, decimal DisPercentage, decimal DisAmount, decimal ServiceCharges,decimal TaxAmount, decimal TotalAmount, int EmpCode, string itemcodeArr, string priceArr, string qtyArr, string AmountArr, string taxArr,string TaxAmountArr)
    //{

    //    int status = 0;
    //    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

    //    string[] arrRoles = sesRoles.Split(',');



    //    var roles = from m in arrRoles
    //                where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
    //                select m;


    //    if (roles.Count() == 0)
    //    {
    //        status = -11;
    //    }

    //    int Status = 0;
    //    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    //    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    //    //User objuser = new User()
    //    //{
    //    //    UserNo = UserNo

    //    //};

    //    //Int32 status2 = new UserBLL().chkIsLogin(objuser);

    //    KOT objKOT = new KOT()
    //    {
    //        MKOTNo = 0,
    //        TableID = TableID,
    //        PaxNo = PaxNo,
    //        R_Code = 1,
    //        M_Code = UserNo,
    //        Value = Value,
    //        DisAmount = DisAmount,
    //        DisPercentage = DisPercentage,
    //        ServiceCharges = ServiceCharges,
    //        TaxAmount = TaxAmount,
    //        TotalAmount = TotalAmount,
    //        Complementary = false,
    //        Happy = false,
    //        EmpCode = EmpCode,
    //        BillNoWPrefix = "",
    //        Pass = false,
    //        BranchId = Branch,



    //    };
    //    //if (status2 != 0)
    //    //{
    //    string[] ItemCode = itemcodeArr.Split(',');
    //    string[] Price = priceArr.Split(',');
    //    string[] Qty = qtyArr.Split(',');            
    //    string[] Amount = AmountArr.Split(',');
    //    string[] Tax = taxArr.Split(',');    
    //    string[] Tax_Amount = TaxAmountArr.Split(',');


    //    DataTable dt = new DataTable();
    //    dt.Columns.Add("ItemCode");
    //    dt.Columns.Add("Rate");
    //    dt.Columns.Add("Qty");
    //    dt.Columns.Add("Amount");
    //    dt.Columns.Add("Tax");
    //    dt.Columns.Add("Tax_Amount");



    //    for (int i = 0; i < ItemCode.Length; i++)
    //    {
    //        DataRow dr = dt.NewRow();
    //        dr["ItemCode"] = ItemCode[i];
    //        dr["Rate"] = Convert.ToDecimal(Price[i]);
    //        dr["Qty"] = Convert.ToDecimal(Qty[i]);
    //        dr["Amount"] = Convert.ToDecimal(Amount[i]);
    //        dr["Tax"] = Convert.ToDecimal(Tax[i]);
    //        dr["Tax_Amount"] = Convert.ToDecimal(Tax_Amount[i]);

    //        dt.Rows.Add(dr);
    //    }




    //        Status = new kotBLL().Insert(objKOT, dt);



    //    JavaScriptSerializer ser = new JavaScriptSerializer();


    //    var JsonData = new
    //    {
    //        kot = objKOT,
    //        BNF = objKOT.KOTNo,
    //        Status = Status
    //    };
    //    return ser.Serialize(JsonData);
    //}


    [WebMethod]
    public static string KOTprint()
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        List<KotPrint> objkot = new KotPrintBLL().GetDataByUserNo(UserNo);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            kotprint = objkot

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string DeletePrinter(string BillNowPrefix)
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new BillBLL().DeletePrinter(objBill, UserNo);
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string DeletePrinterUserWise()
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;
       
        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new PrinterDAL().DeletePrinter(UserNo);
        var JsonData = new
        {
            
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}