﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for BranchBLL
/// </summary>
public class BranchBLL
{
    public void GetById(Branches objBranch)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BranchDAL().GetById(objBranch);
            if (dr.HasRows)
            {
                dr.Read();


                objBranch.BranchName = dr["BranchName"].ToString();
                objBranch.BranchId = Convert.ToInt16(dr["BranchId"]);
                objBranch.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objBranch.UserId = Convert.ToInt32(dr["UserId"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<Branches> GetAll()
    {
        List<Branches> BranchList = new List<Branches>();

        SqlDataReader dr = null;
        try
        {
            dr = new BranchDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Branches objBranch = new Branches()
                    {
                        BranchName = dr["BranchName"].ToString(),
                        StateId = Convert.ToInt32(dr["StateId"].ToString()),
                        BranchId = Convert.ToInt16(dr["BranchId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    BranchList.Add(objBranch);
                }

            }
        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return BranchList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BranchDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["BranchId"].ToString(), dr["BranchName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Branches objBranch)
    {

        return new BranchDAL().InsertUpdate(objBranch);
    }
}