﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Reports_DeliveryNoteChecklist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
           
        }

        CheckRole();


        string notetype = "";
        string query = "";

        if (rdbAll.Checked == true)
        {
            query = "All";
        }
        else
        {
            query = hdnsale.Value;
        }
        if (rdbAllExcise.Checked == true)
        {
            notetype = "All";
        }
        else if (rdbExcise.Checked == true)
        {
            notetype = "Excise";
        }
        else if (rdbnonexcise.Checked == true)
        {
            notetype = "NonExcise";
        }
        rptDeliveryIssueChkList objBreakageExpiry = new rptDeliveryIssueChkList(query, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text),notetype);
        ReportViewer1.Report = objBreakageExpiry;

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPGROUPSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindBranches()
    {
        ltBranchs.Text = GetBranchHtml();
    }


    public string GetBranchHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetAllCreditCustomers();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    html += "<tr><td><input type='checkbox' value='" + dr["CCode"] + "' name='branch' id='chkS_" + dr["CCode"] + "'/> <label for='chkS_" + dr["CCode"] + "'>" + dr["CName"] + "</label></td></tr>";



                }
            }
            else
            {
                html = "<tr><td>No Customer Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }


    protected void btnGetRecords_Click(object sender, EventArgs e)
    {





    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}