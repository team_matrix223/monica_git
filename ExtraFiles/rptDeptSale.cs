﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rptDeptSale
/// </summary>
public class rptDeptSale : DevExpress.XtraReports.UI.XtraReport
{
    private DetailBand Detail;
    private XRPageBreak xrPageBreak1;
    private FormattingRule formattingRule1;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell clProductName;
    private XRTableCell clSubtotal;
    private ReportHeaderBand ReportHeader;
    private XRLabel xrLabel14;
    private XRPageInfo xrPageInfo1;
    private XRLabel xrLabel4;
    private XRLabel xrLabel15;
    private XRLabel xrLabel3;
    private DevExpress.XtraReports.Parameters.Parameter MaxRowCountParameter;
    private ReportFooterBand ReportFooter;
    private PageHeaderBand PageHeader;
    private CalculatedField calculatedField1;
    private TopMarginBand topMarginBand1;
    private BottomMarginBand BottomMargin;
    private XRControlStyle EvenStyle;
    private DevExpress.XtraReports.Parameters.Parameter OrderIdParameter;
    private XRControlStyle OddStyle;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell3;
    private XRTable xrTable8;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell8;
    private XRTable xrTable7;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell7;
    private XRTable xrTable6;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell6;
    private XRTable xrTable5;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell5;
    private XRTable xrTable4;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell2;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell4;
    private XRLabel xrLabel2;
    private XRTable xrTable36;
    private XRTableRow xrTableRow36;
    private XRTableCell xrTableCell36;
    private XRTable xrTable35;
    private XRTableRow xrTableRow35;
    private XRTableCell xrTableCell35;
    private XRTable xrTable34;
    private XRTableRow xrTableRow34;
    private XRTableCell xrTableCell34;
    private XRTable xrTable33;
    private XRTableRow xrTableRow33;
    private XRTableCell xrTableCell33;
    private XRTable xrTable32;
    private XRTableRow xrTableRow32;
    private XRTableCell xrTableCell32;
    private XRTable xrTable31;
    private XRTableRow xrTableRow31;
    private XRTableCell xrTableCell31;
    private XRTable xrTable30;
    private XRTableRow xrTableRow30;
    private XRTableCell xrTableCell30;
    private XRTable xrTable29;
    private XRTableRow xrTableRow29;
    private XRTableCell xrTableCell29;
    private XRTable xrTable28;
    private XRTableRow xrTableRow28;
    private XRTableCell xrTableCell28;
    private XRTable xrTable27;
    private XRTableRow xrTableRow27;
    private XRTableCell xrTableCell27;
    private XRTable xrTable26;
    private XRTableRow xrTableRow26;
    private XRTableCell xrTableCell26;
    private XRTable xrTable25;
    private XRTableRow xrTableRow25;
    private XRTableCell xrTableCell25;
    private XRTable xrTable24;
    private XRTableRow xrTableRow24;
    private XRTableCell xrTableCell24;
    private XRTable xrTable23;
    private XRTableRow xrTableRow23;
    private XRTableCell xrTableCell23;
    private XRTable xrTable22;
    private XRTableRow xrTableRow22;
    private XRTableCell xrTableCell22;
    private XRTable xrTable21;
    private XRTableRow xrTableRow21;
    private XRTableCell xrTableCell21;
    private XRTable xrTable20;
    private XRTableRow xrTableRow20;
    private XRTableCell xrTableCell20;
    private XRTable xrTable19;
    private XRTableRow xrTableRow19;
    private XRTableCell xrTableCell19;
    private XRTable xrTable18;
    private XRTableRow xrTableRow18;
    private XRTableCell xrTableCell18;
    private XRTable xrTable17;
    private XRTableRow xrTableRow17;
    private XRTableCell xrTableCell17;
    private XRTable xrTable16;
    private XRTableRow xrTableRow16;
    private XRTableCell xrTableCell16;
    private XRTable xrTable15;
    private XRTableRow xrTableRow15;
    private XRTableCell xrTableCell15;
    private XRTable xrTable14;
    private XRTableRow xrTableRow14;
    private XRTableCell xrTableCell14;
    private XRTable xrTable13;
    private XRTableRow xrTableRow13;
    private XRTableCell xrTableCell13;
    private XRTable xrTable10;
    private XRTableRow xrTableRow10;
    private XRTableCell xrTableCell10;
    private XRTable xrTable9;
    private XRTableRow xrTableRow9;
    private XRTableCell xrTableCell9;
    private XRLabel xrLabel13;
    private XRLabel xrLabel6;
    private XRLabel xrLabel7;
    private XRLabel xrLabel1;
    private XRLabel xrLabel5;
    private XRTable xrTable12;
    private XRTableRow xrTableRow12;
    private XRTableCell xrTableCell12;
    private XRTable xrTable11;
    private XRTableRow xrTableRow11;
    private XRTableCell xrTableCell11;
    private dsReport dsReport1;
    private dsReportTableAdapters.report_sp_DepartmentWiseSaleReportTableAdapter report_sp_DepartmentWiseSaleReportTableAdapter1;
    private XRTableRow xrTableRow38;
    private XRTableCell xrTableCell38;
    private XRTableRow xrTableRow37;
    private XRTableCell xrTableCell37;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public rptDeptSale(DateTime DateFrom, DateTime DateTo,int BranchId)
	{
        Connection con = new Connection();
        InitializeComponent();
        report_sp_DepartmentWiseSaleReportTableAdapter1.Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        report_sp_DepartmentWiseSaleReportTableAdapter1.Fill(dsReport1.report_sp_DepartmentWiseSaleReport, Convert.ToDateTime(DateFrom), Convert.ToDateTime(DateTo), BranchId);

        xrLabel5.Text = DateFrom.ToString("dd-MMM-yyyy");
        xrLabel7.Text = DateTo.ToString("dd-MMM-yyyy");
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "rptDeptSale.resx";
        System.Resources.ResourceManager resources = global::Resources.rptDeptSale.ResourceManager;
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
        this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.clProductName = new DevExpress.XtraReports.UI.XRTableCell();
        this.clSubtotal = new DevExpress.XtraReports.UI.XRTableCell();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.MaxRowCountParameter = new DevExpress.XtraReports.Parameters.Parameter();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.calculatedField1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
        this.OrderIdParameter = new DevExpress.XtraReports.Parameters.Parameter();
        this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
        this.dsReport1 = new dsReport();
        this.report_sp_DepartmentWiseSaleReportTableAdapter1 = new dsReportTableAdapters.report_sp_DepartmentWiseSaleReportTableAdapter();
        this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsReport1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageBreak1,
            this.xrTable2});
        this.Detail.HeightF = 25F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrPageBreak1
        // 
        this.xrPageBreak1.FormattingRules.Add(this.formattingRule1);
        this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrPageBreak1.Name = "xrPageBreak1";
        this.xrPageBreak1.Visible = false;
        // 
        // formattingRule1
        // 
        this.formattingRule1.Condition = "([DataSource.CurrentRowIndex] % [Parameters.MaxRowCountParameter]  == 0) And ([Da" +
            "taSource.CurrentRowIndex] != 0)";
        this.formattingRule1.DataMember = "OrderDetails";
        // 
        // 
        // 
        this.formattingRule1.Formatting.Visible = DevExpress.Utils.DefaultBoolean.True;
        this.formattingRule1.Name = "formattingRule1";
        // 
        // xrTable2
        // 
        this.xrTable2.BorderColor = System.Drawing.Color.Black;
        this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable2.BorderWidth = 1;
        this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(801.9999F, 25F);
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.clProductName,
            this.clSubtotal});
        this.xrTableRow2.EvenStyleName = "EvenStyle";
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.OddStyleName = "OddStyle";
        this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow2.Weight = 1D;
        // 
        // clProductName
        // 
        this.clProductName.BackColor = System.Drawing.Color.LightGray;
        this.clProductName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Prop_Name")});
        this.clProductName.Name = "clProductName";
        this.clProductName.Padding = new DevExpress.XtraPrinting.PaddingInfo(7, 5, 3, 3, 100F);
        this.clProductName.StylePriority.UseBackColor = false;
        this.clProductName.Text = "clProductName";
        this.clProductName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.clProductName.Weight = 0.57609664520967785D;
        // 
        // clSubtotal
        // 
        this.clSubtotal.BackColor = System.Drawing.Color.LightGray;
        this.clSubtotal.BorderWidth = 1;
        this.clSubtotal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Amount", "{0:#.00}")});
        this.clSubtotal.Name = "clSubtotal";
        this.clSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.clSubtotal.StylePriority.UseBackColor = false;
        this.clSubtotal.StylePriority.UseTextAlignment = false;
        this.clSubtotal.Text = "clSubtotal";
        this.clSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.clSubtotal.Weight = 0.4239033547903221D;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel1,
            this.xrLabel5,
            this.xrLabel2,
            this.xrLabel14,
            this.xrLabel13,
            this.xrPageInfo1,
            this.xrLabel4,
            this.xrLabel15});
        this.ReportHeader.HeightF = 231.7083F;
        this.ReportHeader.Name = "ReportHeader";
        this.ReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel6
        // 
        this.xrLabel6.BackColor = System.Drawing.Color.Black;
        this.xrLabel6.CanShrink = true;
        this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel6.ForeColor = System.Drawing.Color.White;
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(402.4613F, 200.7083F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(79.15335F, 21F);
        this.xrLabel6.StylePriority.UseBackColor = false;
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UseForeColor = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.Text = "To";
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel7
        // 
        this.xrLabel7.BackColor = System.Drawing.Color.White;
        this.xrLabel7.CanShrink = true;
        this.xrLabel7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel7.ForeColor = System.Drawing.Color.Black;
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(481.6146F, 200.7083F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(99.98666F, 21F);
        this.xrLabel7.StylePriority.UseBackColor = false;
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UseForeColor = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel1
        // 
        this.xrLabel1.BackColor = System.Drawing.Color.Black;
        this.xrLabel1.CanShrink = true;
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel1.ForeColor = System.Drawing.Color.White;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(236.0654F, 200.7083F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(84.98667F, 21F);
        this.xrLabel1.StylePriority.UseBackColor = false;
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.StylePriority.UseForeColor = false;
        this.xrLabel1.StylePriority.UseTextAlignment = false;
        this.xrLabel1.Text = "From";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel5
        // 
        this.xrLabel5.BackColor = System.Drawing.Color.White;
        this.xrLabel5.CanShrink = true;
        this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel5.ForeColor = System.Drawing.Color.Black;
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(321.0522F, 200.7083F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(71.65335F, 21F);
        this.xrLabel5.StylePriority.UseBackColor = false;
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UseForeColor = false;
        this.xrLabel5.StylePriority.UseTextAlignment = false;
        this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel2
        // 
        this.xrLabel2.CanShrink = true;
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Header5")});
        this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 99F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(801.9999F, 33F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "xrLabel2";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel14
        // 
        this.xrLabel14.CanShrink = true;
        this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Header3")});
        this.xrLabel14.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 33F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(801.9999F, 33F);
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.Text = "xrLabel14";
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel13
        // 
        this.xrLabel13.CanShrink = true;
        this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Header2")});
        this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(801.9999F, 33F);
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "xrLabel13";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrPageInfo1
        // 
        this.xrPageInfo1.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(544.2501F, 165F);
        this.xrPageInfo1.Name = "xrPageInfo1";
        this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo1.SizeF = new System.Drawing.SizeF(245F, 16F);
        this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel4
        // 
        this.xrLabel4.CanShrink = true;
        this.xrLabel4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 132F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(801.9999F, 33F);
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.Text = "DEPARTMENT WISE SALE REPORT";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel15
        // 
        this.xrLabel15.CanShrink = true;
        this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Header4")});
        this.xrLabel15.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 66F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(801.9999F, 33F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "xrLabel15";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.BackColor = System.Drawing.Color.Black;
        this.xrLabel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(177)))), ((int)(((byte)(181)))));
        this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrLabel3.CanGrow = false;
        this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Amount")});
        this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
        this.xrLabel3.ForeColor = System.Drawing.Color.White;
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(468.0293F, 0F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(333.9706F, 21F);
        this.xrLabel3.StylePriority.UseBackColor = false;
        this.xrLabel3.StylePriority.UseForeColor = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrLabel3.Summary = xrSummary1;
        this.xrLabel3.Text = "xrLabel3";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrLabel3.XlsxFormatString = "\"Total order cost: \"$0.00";
        // 
        // MaxRowCountParameter
        // 
        this.MaxRowCountParameter.Description = "Max Rows per Page: ";
        this.MaxRowCountParameter.Name = "MaxRowCountParameter";
        this.MaxRowCountParameter.Type = typeof(int);
        this.MaxRowCountParameter.Value = 10;
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12,
            this.xrTable11,
            this.xrTable36,
            this.xrTable35,
            this.xrTable34,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable28,
            this.xrTable27,
            this.xrTable26,
            this.xrTable25,
            this.xrTable24,
            this.xrTable23,
            this.xrTable22,
            this.xrTable21,
            this.xrTable20,
            this.xrTable19,
            this.xrTable18,
            this.xrTable17,
            this.xrTable16,
            this.xrTable15,
            this.xrTable14,
            this.xrTable13,
            this.xrTable10,
            this.xrTable9,
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3,
            this.xrLabel3});
        this.ReportFooter.HeightF = 474.1251F;
        this.ReportFooter.Name = "ReportFooter";
        this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable12
        // 
        this.xrTable12.BorderColor = System.Drawing.Color.Black;
        this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable12.BorderWidth = 1;
        this.xrTable12.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 171F);
        this.xrTable12.Name = "xrTable12";
        this.xrTable12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
        this.xrTable12.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow12
        // 
        this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12});
        this.xrTableRow12.EvenStyleName = "EvenStyle";
        this.xrTableRow12.Name = "xrTableRow12";
        this.xrTableRow12.OddStyleName = "OddStyle";
        this.xrTableRow12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow12.Weight = 1D;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.BorderWidth = 1;
        this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.OnlinePaymentSale", "{0:#.00}")});
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell12.StylePriority.UseTextAlignment = false;
        this.xrTableCell12.Text = "xrTableCell12";
        this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell12.Weight = 1D;
        // 
        // xrTable11
        // 
        this.xrTable11.BorderColor = System.Drawing.Color.Black;
        this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable11.BorderWidth = 1;
        this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 171F);
        this.xrTable11.Name = "xrTable11";
        this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
        this.xrTable11.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow11
        // 
        this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
        this.xrTableRow11.EvenStyleName = "EvenStyle";
        this.xrTableRow11.Name = "xrTableRow11";
        this.xrTableRow11.OddStyleName = "OddStyle";
        this.xrTableRow11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow11.Weight = 1D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.BorderWidth = 1;
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell11.StylePriority.UseTextAlignment = false;
        this.xrTableCell11.Text = "Online Payment Sale";
        this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell11.Weight = 1D;
        // 
        // xrTable36
        // 
        this.xrTable36.BorderColor = System.Drawing.Color.Black;
        this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable36.BorderWidth = 1;
        this.xrTable36.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 421F);
        this.xrTable36.Name = "xrTable36";
        this.xrTable36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow38});
        this.xrTable36.SizeF = new System.Drawing.SizeF(339.9705F, 50F);
        this.xrTable36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow36
        // 
        this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36});
        this.xrTableRow36.EvenStyleName = "EvenStyle";
        this.xrTableRow36.Name = "xrTableRow36";
        this.xrTableRow36.OddStyleName = "OddStyle";
        this.xrTableRow36.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow36.Weight = 1D;
        // 
        // xrTableCell36
        // 
        this.xrTableCell36.BorderWidth = 1;
        this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CreditCardTransactions", "{0:#.00}")});
        this.xrTableCell36.Name = "xrTableCell36";
        this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell36.StylePriority.UseTextAlignment = false;
        this.xrTableCell36.Text = "xrTableCell36";
        this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell36.Weight = 1D;
        // 
        // xrTable35
        // 
        this.xrTable35.BorderColor = System.Drawing.Color.Black;
        this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable35.BorderWidth = 1;
        this.xrTable35.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 421F);
        this.xrTable35.Name = "xrTable35";
        this.xrTable35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35,
            this.xrTableRow37});
        this.xrTable35.SizeF = new System.Drawing.SizeF(462.0294F, 50F);
        this.xrTable35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow35
        // 
        this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35});
        this.xrTableRow35.EvenStyleName = "EvenStyle";
        this.xrTableRow35.Name = "xrTableRow35";
        this.xrTableRow35.OddStyleName = "OddStyle";
        this.xrTableRow35.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow35.Weight = 1D;
        // 
        // xrTableCell35
        // 
        this.xrTableCell35.BorderWidth = 1;
        this.xrTableCell35.Name = "xrTableCell35";
        this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell35.StylePriority.UseTextAlignment = false;
        this.xrTableCell35.Text = "Credit Card Transactions";
        this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell35.Weight = 1D;
        // 
        // xrTable34
        // 
        this.xrTable34.BorderColor = System.Drawing.Color.Black;
        this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable34.BorderWidth = 1;
        this.xrTable34.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 396F);
        this.xrTable34.Name = "xrTable34";
        this.xrTable34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
        this.xrTable34.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow34
        // 
        this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34});
        this.xrTableRow34.EvenStyleName = "EvenStyle";
        this.xrTableRow34.Name = "xrTableRow34";
        this.xrTableRow34.OddStyleName = "OddStyle";
        this.xrTableRow34.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow34.Weight = 1D;
        // 
        // xrTableCell34
        // 
        this.xrTableCell34.BorderWidth = 1;
        this.xrTableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CashInHand", "{0:#.00}")});
        this.xrTableCell34.Name = "xrTableCell34";
        this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell34.StylePriority.UseTextAlignment = false;
        this.xrTableCell34.Text = "xrTableCell34";
        this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell34.Weight = 1D;
        // 
        // xrTable33
        // 
        this.xrTable33.BorderColor = System.Drawing.Color.Black;
        this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable33.BorderWidth = 1;
        this.xrTable33.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 396F);
        this.xrTable33.Name = "xrTable33";
        this.xrTable33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
        this.xrTable33.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow33
        // 
        this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
        this.xrTableRow33.EvenStyleName = "EvenStyle";
        this.xrTableRow33.Name = "xrTableRow33";
        this.xrTableRow33.OddStyleName = "OddStyle";
        this.xrTableRow33.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow33.Weight = 1D;
        // 
        // xrTableCell33
        // 
        this.xrTableCell33.BorderWidth = 1;
        this.xrTableCell33.Name = "xrTableCell33";
        this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell33.StylePriority.UseTextAlignment = false;
        this.xrTableCell33.Text = "Cash In Hand";
        this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell33.Weight = 1D;
        // 
        // xrTable32
        // 
        this.xrTable32.BorderColor = System.Drawing.Color.Black;
        this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable32.BorderWidth = 1;
        this.xrTable32.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 371F);
        this.xrTable32.Name = "xrTable32";
        this.xrTable32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
        this.xrTable32.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow32
        // 
        this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32});
        this.xrTableRow32.EvenStyleName = "EvenStyle";
        this.xrTableRow32.Name = "xrTableRow32";
        this.xrTableRow32.OddStyleName = "OddStyle";
        this.xrTableRow32.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow32.Weight = 1D;
        // 
        // xrTableCell32
        // 
        this.xrTableCell32.BorderWidth = 1;
        this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.SaleReturn", "{0:#.00}")});
        this.xrTableCell32.Name = "xrTableCell32";
        this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell32.StylePriority.UseTextAlignment = false;
        this.xrTableCell32.Text = "xrTableCell32";
        this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell32.Weight = 1D;
        // 
        // xrTable31
        // 
        this.xrTable31.BorderColor = System.Drawing.Color.Black;
        this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable31.BorderWidth = 1;
        this.xrTable31.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 371F);
        this.xrTable31.Name = "xrTable31";
        this.xrTable31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
        this.xrTable31.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow31
        // 
        this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31});
        this.xrTableRow31.EvenStyleName = "EvenStyle";
        this.xrTableRow31.Name = "xrTableRow31";
        this.xrTableRow31.OddStyleName = "OddStyle";
        this.xrTableRow31.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow31.Weight = 1D;
        // 
        // xrTableCell31
        // 
        this.xrTableCell31.BorderWidth = 1;
        this.xrTableCell31.Name = "xrTableCell31";
        this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell31.StylePriority.UseTextAlignment = false;
        this.xrTableCell31.Text = "Sale Return";
        this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell31.Weight = 1D;
        // 
        // xrTable30
        // 
        this.xrTable30.BorderColor = System.Drawing.Color.Black;
        this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable30.BorderWidth = 1;
        this.xrTable30.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 346F);
        this.xrTable30.Name = "xrTable30";
        this.xrTable30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
        this.xrTable30.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow30
        // 
        this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30});
        this.xrTableRow30.EvenStyleName = "EvenStyle";
        this.xrTableRow30.Name = "xrTableRow30";
        this.xrTableRow30.OddStyleName = "OddStyle";
        this.xrTableRow30.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow30.Weight = 1D;
        // 
        // xrTableCell30
        // 
        this.xrTableCell30.BorderWidth = 1;
        this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Discount", "{0:#.00}")});
        this.xrTableCell30.Name = "xrTableCell30";
        this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell30.StylePriority.UseTextAlignment = false;
        this.xrTableCell30.Text = "xrTableCell30";
        this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell30.Weight = 1D;
        // 
        // xrTable29
        // 
        this.xrTable29.BorderColor = System.Drawing.Color.Black;
        this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable29.BorderWidth = 1;
        this.xrTable29.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 346F);
        this.xrTable29.Name = "xrTable29";
        this.xrTable29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
        this.xrTable29.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow29
        // 
        this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29});
        this.xrTableRow29.EvenStyleName = "EvenStyle";
        this.xrTableRow29.Name = "xrTableRow29";
        this.xrTableRow29.OddStyleName = "OddStyle";
        this.xrTableRow29.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow29.Weight = 1D;
        // 
        // xrTableCell29
        // 
        this.xrTableCell29.BorderWidth = 1;
        this.xrTableCell29.Name = "xrTableCell29";
        this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell29.StylePriority.UseTextAlignment = false;
        this.xrTableCell29.Text = "Discount";
        this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell29.Weight = 1D;
        // 
        // xrTable28
        // 
        this.xrTable28.BorderColor = System.Drawing.Color.Black;
        this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable28.BorderWidth = 1;
        this.xrTable28.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 321F);
        this.xrTable28.Name = "xrTable28";
        this.xrTable28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
        this.xrTable28.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow28
        // 
        this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28});
        this.xrTableRow28.EvenStyleName = "EvenStyle";
        this.xrTableRow28.Name = "xrTableRow28";
        this.xrTableRow28.OddStyleName = "OddStyle";
        this.xrTableRow28.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow28.Weight = 1D;
        // 
        // xrTableCell28
        // 
        this.xrTableCell28.BorderWidth = 1;
        this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CreditCardAdvance", "{0:#.00}")});
        this.xrTableCell28.Name = "xrTableCell28";
        this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell28.StylePriority.UseTextAlignment = false;
        this.xrTableCell28.Text = "xrTableCell28";
        this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell28.Weight = 1D;
        // 
        // xrTable27
        // 
        this.xrTable27.BorderColor = System.Drawing.Color.Black;
        this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable27.BorderWidth = 1;
        this.xrTable27.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 321F);
        this.xrTable27.Name = "xrTable27";
        this.xrTable27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
        this.xrTable27.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow27
        // 
        this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27});
        this.xrTableRow27.EvenStyleName = "EvenStyle";
        this.xrTableRow27.Name = "xrTableRow27";
        this.xrTableRow27.OddStyleName = "OddStyle";
        this.xrTableRow27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow27.Weight = 1D;
        // 
        // xrTableCell27
        // 
        this.xrTableCell27.BorderWidth = 1;
        this.xrTableCell27.Name = "xrTableCell27";
        this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell27.StylePriority.UseTextAlignment = false;
        this.xrTableCell27.Text = "Credit Card Advance";
        this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell27.Weight = 1D;
        // 
        // xrTable26
        // 
        this.xrTable26.BorderColor = System.Drawing.Color.Black;
        this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable26.BorderWidth = 1;
        this.xrTable26.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 296F);
        this.xrTable26.Name = "xrTable26";
        this.xrTable26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
        this.xrTable26.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow26
        // 
        this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26});
        this.xrTableRow26.EvenStyleName = "EvenStyle";
        this.xrTableRow26.Name = "xrTableRow26";
        this.xrTableRow26.OddStyleName = "OddStyle";
        this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow26.Weight = 1D;
        // 
        // xrTableCell26
        // 
        this.xrTableCell26.BorderWidth = 1;
        this.xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CashAdvance", "{0:#.00}")});
        this.xrTableCell26.Name = "xrTableCell26";
        this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell26.StylePriority.UseTextAlignment = false;
        this.xrTableCell26.Text = "xrTableCell26";
        this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell26.Weight = 1D;
        // 
        // xrTable25
        // 
        this.xrTable25.BorderColor = System.Drawing.Color.Black;
        this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable25.BorderWidth = 1;
        this.xrTable25.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 296F);
        this.xrTable25.Name = "xrTable25";
        this.xrTable25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
        this.xrTable25.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow25
        // 
        this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25});
        this.xrTableRow25.EvenStyleName = "EvenStyle";
        this.xrTableRow25.Name = "xrTableRow25";
        this.xrTableRow25.OddStyleName = "OddStyle";
        this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow25.Weight = 1D;
        // 
        // xrTableCell25
        // 
        this.xrTableCell25.BorderWidth = 1;
        this.xrTableCell25.Name = "xrTableCell25";
        this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell25.StylePriority.UseTextAlignment = false;
        this.xrTableCell25.Text = "Cash Advance Payment";
        this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell25.Weight = 1D;
        // 
        // xrTable24
        // 
        this.xrTable24.BorderColor = System.Drawing.Color.Black;
        this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable24.BorderWidth = 1;
        this.xrTable24.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 271F);
        this.xrTable24.Name = "xrTable24";
        this.xrTable24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
        this.xrTable24.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow24
        // 
        this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24});
        this.xrTableRow24.EvenStyleName = "EvenStyle";
        this.xrTableRow24.Name = "xrTableRow24";
        this.xrTableRow24.OddStyleName = "OddStyle";
        this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow24.Weight = 1D;
        // 
        // xrTableCell24
        // 
        this.xrTableCell24.BorderWidth = 1;
        this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CreditCardSettlement", "{0:#.00}")});
        this.xrTableCell24.Name = "xrTableCell24";
        this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell24.StylePriority.UseTextAlignment = false;
        this.xrTableCell24.Text = "xrTableCell24";
        this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell24.Weight = 1D;
        // 
        // xrTable23
        // 
        this.xrTable23.BorderColor = System.Drawing.Color.Black;
        this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable23.BorderWidth = 1;
        this.xrTable23.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 271F);
        this.xrTable23.Name = "xrTable23";
        this.xrTable23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
        this.xrTable23.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow23
        // 
        this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23});
        this.xrTableRow23.EvenStyleName = "EvenStyle";
        this.xrTableRow23.Name = "xrTableRow23";
        this.xrTableRow23.OddStyleName = "OddStyle";
        this.xrTableRow23.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow23.Weight = 1D;
        // 
        // xrTableCell23
        // 
        this.xrTableCell23.BorderWidth = 1;
        this.xrTableCell23.Name = "xrTableCell23";
        this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell23.StylePriority.UseTextAlignment = false;
        this.xrTableCell23.Text = "CrCard Settlement Payment";
        this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell23.Weight = 1D;
        // 
        // xrTable22
        // 
        this.xrTable22.BorderColor = System.Drawing.Color.Black;
        this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable22.BorderWidth = 1;
        this.xrTable22.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 246F);
        this.xrTable22.Name = "xrTable22";
        this.xrTable22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
        this.xrTable22.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow22
        // 
        this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
        this.xrTableRow22.EvenStyleName = "EvenStyle";
        this.xrTableRow22.Name = "xrTableRow22";
        this.xrTableRow22.OddStyleName = "OddStyle";
        this.xrTableRow22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow22.Weight = 1D;
        // 
        // xrTableCell22
        // 
        this.xrTableCell22.BorderWidth = 1;
        this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CashSettlement", "{0:#.00}")});
        this.xrTableCell22.Name = "xrTableCell22";
        this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell22.StylePriority.UseTextAlignment = false;
        this.xrTableCell22.Text = "xrTableCell22";
        this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell22.Weight = 1D;
        // 
        // xrTable21
        // 
        this.xrTable21.BorderColor = System.Drawing.Color.Black;
        this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable21.BorderWidth = 1;
        this.xrTable21.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 246F);
        this.xrTable21.Name = "xrTable21";
        this.xrTable21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
        this.xrTable21.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow21
        // 
        this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21});
        this.xrTableRow21.EvenStyleName = "EvenStyle";
        this.xrTableRow21.Name = "xrTableRow21";
        this.xrTableRow21.OddStyleName = "OddStyle";
        this.xrTableRow21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow21.Weight = 1D;
        // 
        // xrTableCell21
        // 
        this.xrTableCell21.BorderWidth = 1;
        this.xrTableCell21.Name = "xrTableCell21";
        this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell21.StylePriority.UseTextAlignment = false;
        this.xrTableCell21.Text = "Cash Settlement Payment";
        this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell21.Weight = 1D;
        // 
        // xrTable20
        // 
        this.xrTable20.BorderColor = System.Drawing.Color.Black;
        this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable20.BorderWidth = 1;
        this.xrTable20.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 221F);
        this.xrTable20.Name = "xrTable20";
        this.xrTable20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
        this.xrTable20.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow20
        // 
        this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20});
        this.xrTableRow20.EvenStyleName = "EvenStyle";
        this.xrTableRow20.Name = "xrTableRow20";
        this.xrTableRow20.OddStyleName = "OddStyle";
        this.xrTableRow20.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow20.Weight = 1D;
        // 
        // xrTableCell20
        // 
        this.xrTableCell20.BorderWidth = 1;
        this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.FOCSALE", "{0:#.00}")});
        this.xrTableCell20.Name = "xrTableCell20";
        this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell20.StylePriority.UseTextAlignment = false;
        this.xrTableCell20.Text = "xrTableCell20";
        this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell20.Weight = 1D;
        // 
        // xrTable19
        // 
        this.xrTable19.BorderColor = System.Drawing.Color.Black;
        this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable19.BorderWidth = 1;
        this.xrTable19.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 221F);
        this.xrTable19.Name = "xrTable19";
        this.xrTable19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
        this.xrTable19.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow19
        // 
        this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19});
        this.xrTableRow19.EvenStyleName = "EvenStyle";
        this.xrTableRow19.Name = "xrTableRow19";
        this.xrTableRow19.OddStyleName = "OddStyle";
        this.xrTableRow19.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow19.Weight = 1D;
        // 
        // xrTableCell19
        // 
        this.xrTableCell19.BorderWidth = 1;
        this.xrTableCell19.Name = "xrTableCell19";
        this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell19.StylePriority.UseTextAlignment = false;
        this.xrTableCell19.Text = "FOC Sale";
        this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell19.Weight = 1D;
        // 
        // xrTable18
        // 
        this.xrTable18.BorderColor = System.Drawing.Color.Black;
        this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable18.BorderWidth = 1;
        this.xrTable18.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 196F);
        this.xrTable18.Name = "xrTable18";
        this.xrTable18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
        this.xrTable18.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow18
        // 
        this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18});
        this.xrTableRow18.EvenStyleName = "EvenStyle";
        this.xrTableRow18.Name = "xrTableRow18";
        this.xrTableRow18.OddStyleName = "OddStyle";
        this.xrTableRow18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow18.Weight = 1D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.BorderWidth = 1;
        this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.Expenditure", "{0:#.00}")});
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell18.StylePriority.UseTextAlignment = false;
        this.xrTableCell18.Text = "xrTableCell18";
        this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell18.Weight = 1D;
        // 
        // xrTable17
        // 
        this.xrTable17.BorderColor = System.Drawing.Color.Black;
        this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable17.BorderWidth = 1;
        this.xrTable17.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 196F);
        this.xrTable17.Name = "xrTable17";
        this.xrTable17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
        this.xrTable17.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow17
        // 
        this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17});
        this.xrTableRow17.EvenStyleName = "EvenStyle";
        this.xrTableRow17.Name = "xrTableRow17";
        this.xrTableRow17.OddStyleName = "OddStyle";
        this.xrTableRow17.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow17.Weight = 1D;
        // 
        // xrTableCell17
        // 
        this.xrTableCell17.BorderWidth = 1;
        this.xrTableCell17.Name = "xrTableCell17";
        this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell17.StylePriority.UseTextAlignment = false;
        this.xrTableCell17.Text = "Expenditure";
        this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell17.Weight = 1D;
        // 
        // xrTable16
        // 
        this.xrTable16.BorderColor = System.Drawing.Color.Black;
        this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable16.BorderWidth = 1;
        this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 146F);
        this.xrTable16.Name = "xrTable16";
        this.xrTable16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
        this.xrTable16.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow16
        // 
        this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16});
        this.xrTableRow16.EvenStyleName = "EvenStyle";
        this.xrTableRow16.Name = "xrTableRow16";
        this.xrTableRow16.OddStyleName = "OddStyle";
        this.xrTableRow16.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow16.Weight = 1D;
        // 
        // xrTableCell16
        // 
        this.xrTableCell16.BorderWidth = 1;
        this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CreditSale", "{0:#.00}")});
        this.xrTableCell16.Name = "xrTableCell16";
        this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell16.StylePriority.UseTextAlignment = false;
        this.xrTableCell16.Text = "xrTableCell16";
        this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell16.Weight = 1D;
        // 
        // xrTable15
        // 
        this.xrTable15.BorderColor = System.Drawing.Color.Black;
        this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable15.BorderWidth = 1;
        this.xrTable15.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 146F);
        this.xrTable15.Name = "xrTable15";
        this.xrTable15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
        this.xrTable15.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow15
        // 
        this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15});
        this.xrTableRow15.EvenStyleName = "EvenStyle";
        this.xrTableRow15.Name = "xrTableRow15";
        this.xrTableRow15.OddStyleName = "OddStyle";
        this.xrTableRow15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow15.Weight = 1D;
        // 
        // xrTableCell15
        // 
        this.xrTableCell15.BorderWidth = 1;
        this.xrTableCell15.Name = "xrTableCell15";
        this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell15.StylePriority.UseTextAlignment = false;
        this.xrTableCell15.Text = "Credit Sale";
        this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell15.Weight = 1D;
        // 
        // xrTable14
        // 
        this.xrTable14.BorderColor = System.Drawing.Color.Black;
        this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable14.BorderWidth = 1;
        this.xrTable14.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 121F);
        this.xrTable14.Name = "xrTable14";
        this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
        this.xrTable14.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow14
        // 
        this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
        this.xrTableRow14.EvenStyleName = "EvenStyle";
        this.xrTableRow14.Name = "xrTableRow14";
        this.xrTableRow14.OddStyleName = "OddStyle";
        this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow14.Weight = 1D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.BorderWidth = 1;
        this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.CreditCardSale", "{0:#.00}")});
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell14.StylePriority.UseTextAlignment = false;
        this.xrTableCell14.Text = "xrTableCell14";
        this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell14.Weight = 1D;
        // 
        // xrTable13
        // 
        this.xrTable13.BorderColor = System.Drawing.Color.Black;
        this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable13.BorderWidth = 1;
        this.xrTable13.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(2.000062F, 121F);
        this.xrTable13.Name = "xrTable13";
        this.xrTable13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
        this.xrTable13.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow13
        // 
        this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
        this.xrTableRow13.EvenStyleName = "EvenStyle";
        this.xrTableRow13.Name = "xrTableRow13";
        this.xrTableRow13.OddStyleName = "OddStyle";
        this.xrTableRow13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow13.Weight = 1D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.BorderWidth = 1;
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell13.StylePriority.UseTextAlignment = false;
        this.xrTableCell13.Text = "CrCard Sale";
        this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell13.Weight = 1D;
        // 
        // xrTable10
        // 
        this.xrTable10.BorderColor = System.Drawing.Color.Black;
        this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable10.BorderWidth = 1;
        this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 96F);
        this.xrTable10.Name = "xrTable10";
        this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
        this.xrTable10.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow10
        // 
        this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
        this.xrTableRow10.EvenStyleName = "EvenStyle";
        this.xrTableRow10.Name = "xrTableRow10";
        this.xrTableRow10.OddStyleName = "OddStyle";
        this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow10.Weight = 1D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.BorderWidth = 1;
        this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.NetCashSale", "{0:#.00}")});
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell10.StylePriority.UseTextAlignment = false;
        this.xrTableCell10.Text = "xrTableCell10";
        this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell10.Weight = 1D;
        // 
        // xrTable9
        // 
        this.xrTable9.BorderColor = System.Drawing.Color.Black;
        this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable9.BorderWidth = 1;
        this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(2.000061F, 96F);
        this.xrTable9.Name = "xrTable9";
        this.xrTable9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
        this.xrTable9.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow9
        // 
        this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9});
        this.xrTableRow9.EvenStyleName = "EvenStyle";
        this.xrTableRow9.Name = "xrTableRow9";
        this.xrTableRow9.OddStyleName = "OddStyle";
        this.xrTableRow9.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow9.Weight = 1D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.BorderWidth = 1;
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell9.StylePriority.UseTextAlignment = false;
        this.xrTableCell9.Text = "Net Cash Sale";
        this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell9.Weight = 1D;
        // 
        // xrTable8
        // 
        this.xrTable8.BorderColor = System.Drawing.Color.Black;
        this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable8.BorderWidth = 1;
        this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 71F);
        this.xrTable8.Name = "xrTable8";
        this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
        this.xrTable8.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow8
        // 
        this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
        this.xrTableRow8.EvenStyleName = "EvenStyle";
        this.xrTableRow8.Name = "xrTableRow8";
        this.xrTableRow8.OddStyleName = "OddStyle";
        this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow8.Weight = 1D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.BorderWidth = 1;
        this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.TotalSale", "{0:#.00}")});
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell8.StylePriority.UseTextAlignment = false;
        this.xrTableCell8.Text = "xrTableCell8";
        this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell8.Weight = 1D;
        // 
        // xrTable7
        // 
        this.xrTable7.BorderColor = System.Drawing.Color.Black;
        this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable7.BorderWidth = 1;
        this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(2.000061F, 71F);
        this.xrTable7.Name = "xrTable7";
        this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
        this.xrTable7.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7});
        this.xrTableRow7.EvenStyleName = "EvenStyle";
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.OddStyleName = "OddStyle";
        this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow7.Weight = 1D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.BorderWidth = 1;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell7.StylePriority.UseTextAlignment = false;
        this.xrTableCell7.Text = "Total Sale";
        this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell7.Weight = 1D;
        // 
        // xrTable6
        // 
        this.xrTable6.BorderColor = System.Drawing.Color.Black;
        this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable6.BorderWidth = 1;
        this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 46F);
        this.xrTable6.Name = "xrTable6";
        this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
        this.xrTable6.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6});
        this.xrTableRow6.EvenStyleName = "EvenStyle";
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.OddStyleName = "OddStyle";
        this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow6.Weight = 1D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.BorderWidth = 1;
        this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.RetailSale", "{0:#.00}")});
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell6.StylePriority.UseTextAlignment = false;
        this.xrTableCell6.Text = "xrTableCell6";
        this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell6.Weight = 1D;
        // 
        // xrTable5
        // 
        this.xrTable5.BorderColor = System.Drawing.Color.Black;
        this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable5.BorderWidth = 1;
        this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(2.000061F, 46F);
        this.xrTable5.Name = "xrTable5";
        this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
        this.xrTable5.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5});
        this.xrTableRow5.EvenStyleName = "EvenStyle";
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.OddStyleName = "OddStyle";
        this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow5.Weight = 1D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.BorderWidth = 1;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell5.StylePriority.UseTextAlignment = false;
        this.xrTableCell5.Text = "Retail Sale";
        this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell5.Weight = 1D;
        // 
        // xrTable4
        // 
        this.xrTable4.BorderColor = System.Drawing.Color.Black;
        this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable4.BorderWidth = 1;
        this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(2.000061F, 21F);
        this.xrTable4.Name = "xrTable4";
        this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
        this.xrTable4.SizeF = new System.Drawing.SizeF(462.0294F, 25F);
        this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
        this.xrTableRow4.EvenStyleName = "EvenStyle";
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.OddStyleName = "OddStyle";
        this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow4.Weight = 1D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.BorderWidth = 1;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell2.StylePriority.UseTextAlignment = false;
        this.xrTableCell2.Text = "Order Sale";
        this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell2.Weight = 1D;
        // 
        // xrTable3
        // 
        this.xrTable3.BorderColor = System.Drawing.Color.Black;
        this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable3.BorderWidth = 1;
        this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(464.0294F, 21F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(339.9705F, 25F);
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
        this.xrTableRow3.EvenStyleName = "EvenStyle";
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.OddStyleName = "OddStyle";
        this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow3.Weight = 1D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BorderWidth = 1;
        this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.OrderSale", "{0:#.00}")});
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell4.StylePriority.UseTextAlignment = false;
        this.xrTableCell4.Text = "xrTableCell4";
        this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell4.Weight = 1D;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
        this.PageHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
        this.PageHeader.HeightF = 27.375F;
        this.PageHeader.Name = "PageHeader";
        this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable1
        // 
        this.xrTable1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(214)))), ((int)(((byte)(211)))));
        this.xrTable1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(177)))), ((int)(((byte)(183)))));
        this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable1.BorderWidth = 2;
        this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
        this.xrTable1.SizeF = new System.Drawing.SizeF(801.9999F, 27.375F);
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3});
        this.xrTableRow1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableRow1.Weight = 1D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.Black;
        this.xrTableCell1.BorderWidth = 1;
        this.xrTableCell1.ForeColor = System.Drawing.Color.White;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseForeColor = false;
        this.xrTableCell1.Text = "Item Name";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell1.Weight = 0.57609657157488314D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.Black;
        this.xrTableCell3.BorderWidth = 1;
        this.xrTableCell3.ForeColor = System.Drawing.Color.White;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseForeColor = false;
        this.xrTableCell3.Text = "Amount";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell3.Weight = 0.42390342842511691D;
        // 
        // calculatedField1
        // 
        this.calculatedField1.DisplayName = "SubTotal";
        this.calculatedField1.Expression = "[Quantity]*[UnitPrice]";
        this.calculatedField1.Name = "calculatedField1";
        // 
        // topMarginBand1
        // 
        this.topMarginBand1.HeightF = 12F;
        this.topMarginBand1.Name = "topMarginBand1";
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 75F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // EvenStyle
        // 
        this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(240)))));
        this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(207)))), ((int)(((byte)(189)))));
        this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.EvenStyle.BorderWidth = 1;
        this.EvenStyle.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.EvenStyle.ForeColor = System.Drawing.Color.Black;
        this.EvenStyle.Name = "EvenStyle";
        // 
        // OrderIdParameter
        // 
        this.OrderIdParameter.Description = "Order Id:";
        this.OrderIdParameter.Name = "OrderIdParameter";
        this.OrderIdParameter.Type = typeof(int);
        this.OrderIdParameter.Value = 11077;
        // 
        // OddStyle
        // 
        this.OddStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(232)))), ((int)(((byte)(220)))));
        this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(207)))), ((int)(((byte)(189)))));
        this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.OddStyle.BorderWidth = 1;
        this.OddStyle.Font = new System.Drawing.Font("Tahoma", 8.25F);
        this.OddStyle.ForeColor = System.Drawing.Color.Black;
        this.OddStyle.Name = "OddStyle";
        // 
        // dsReport1
        // 
        this.dsReport1.DataSetName = "dsReport";
        this.dsReport1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // report_sp_DepartmentWiseSaleReportTableAdapter1
        // 
        this.report_sp_DepartmentWiseSaleReportTableAdapter1.ClearBeforeFill = true;
        // 
        // xrTableRow37
        // 
        this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37});
        this.xrTableRow37.Name = "xrTableRow37";
        this.xrTableRow37.Weight = 1D;
        // 
        // xrTableCell37
        // 
        this.xrTableCell37.Name = "xrTableCell37";
        this.xrTableCell37.Text = "Total Delivery Charges";
        this.xrTableCell37.Weight = 1D;
        // 
        // xrTableRow38
        // 
        this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38});
        this.xrTableRow38.Name = "xrTableRow38";
        this.xrTableRow38.Weight = 1D;
        // 
        // xrTableCell38
        // 
        this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "report_sp_DepartmentWiseSaleReport.DeliveryCharge")});
        this.xrTableCell38.Name = "xrTableCell38";
        this.xrTableCell38.Text = "xrTableCell38";
        this.xrTableCell38.Weight = 1D;
        // 
        // rptDeptSale
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.ReportFooter,
            this.PageHeader,
            this.BottomMargin,
            this.topMarginBand1});
        this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedField1});
        this.DataAdapter = this.report_sp_DepartmentWiseSaleReportTableAdapter1;
        this.DataMember = "report_sp_DepartmentWiseSaleReport";
        this.DataSource = this.dsReport1;
        this.DataSourceSchema = resources.GetString("$this.DataSourceSchema");
        this.DisplayName = "Customer Order";
        this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
        this.Margins = new System.Drawing.Printing.Margins(11, 33, 12, 75);
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.OrderIdParameter,
            this.MaxRowCountParameter});
        this.ReportPrintOptions.DetailCountAtDesignTime = 7;
        this.RequestParameters = false;
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.EvenStyle,
            this.OddStyle});
        this.StyleSheetPath = "..\\..\\..\\Data\\\\StyleSheetLavender.repss";
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsReport1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
