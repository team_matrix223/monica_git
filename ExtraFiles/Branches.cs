﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Branches
/// </summary>
public class Branches
{

    public int BranchId { get; set; }
    public string BranchName { get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }
    public int StateId { get; set; }

	public Branches()
	{
        BranchName = "";
        BranchId = 0;
        UserId = 0;
        IsActive = false;
        StateId = 0;
	}
}