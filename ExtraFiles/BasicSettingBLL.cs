﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicSettingBLL
/// </summary>
public class BasicSettingBLL
{
    public Int16 UpdateBasicSettings(BasicSettings objSettings)
    {

        return new BasicSettingsDAL().UpdateBasicSettings(objSettings);
    }



    public void GetSettings(BasicSettings objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BasicSettingsDAL().GetMasterSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();
                objSettings.service_tax = Convert.ToBoolean(dr["service_tax"]);
                objSettings.tax_per = Convert.ToDecimal(dr["tax_per"]);
                objSettings.homedel_charges = Convert.ToBoolean(dr["homedel_charges"]);
                objSettings.min_bill_value = Convert.ToDecimal(dr["min_bill_value"]);
                objSettings.del_charges = Convert.ToDecimal(dr["del_charges"]);
                objSettings.retail_bill = Convert.ToString(dr["retail_bill"]);
                objSettings.vat_bill = Convert.ToString(dr["vat_bill"]);
                objSettings.cst_bill = Convert.ToString(dr["cst_bill"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.AlloServicetax_TakeAway = Convert.ToBoolean(dr["AlloServicetax_TakeAway"]);
                objSettings.CouponPrinting = Convert.ToBoolean(dr["CouponPrinting"]);
                objSettings.CouponPrintingDeptWise = Convert.ToBoolean(dr["CouponPrintingDeptWise"]);
                objSettings.BarcodePrintOnBill = Convert.ToBoolean(dr["BarcodePrintOnBill"]);
                objSettings.UTorS = Convert.ToString(dr["UTorS"]);
                objSettings.PrintCopies = Convert.ToString(dr["PrintCopies"]);
                objSettings.TaxBiferication = Convert.ToBoolean(dr["TaxBiferication"]);
                objSettings.SiteUrl = Convert.ToString(dr["SiteUrl"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

}