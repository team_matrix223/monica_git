﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true"
    CodeFile="managekits.aspx.cs" Inherits="managekits" %>
    <%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
  

<%@ Register Src="~/Templates/Kit.ascx" TagName="AddKit" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <script language="javascript" type="text/javascript">
        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
            }



       var m_KitID = 0;
       var ProductCollection = [];
      var mrpeditable = false;
      var saleRateeditable = false;


       function clsProduct() {
           this.Item_ID = 0;
           this.Item_Code = "";
           this.Item_Name = "";
           this.Qty = 0;
           this.Rate = 0;
           this.MRP = 0;
           this.Amount = 0;
           this.Tax_ID = 0;
           this.Tax_Rate = 0;
       }

       var m_KitID = 0;

       function CommonCalculation() {

           var Amt = Number($("#txtQty").val()) * $("#txtRate").val();

           $("#txtAmount").val(Amt);
       }


       $(document).on("click", "#btnDel", function (event) {

           var RowIndex = Number($(this).closest('tr').index());
         
           ProductCollection.splice(RowIndex, 1);
           BindRows();


       });



       function BindRows() {

           var TotalAmount = 0;
           var html = "";
           for (var i = 0; i < ProductCollection.length; i++) {

                


               html += "<tr>";
               html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
               html += "<td>" + ProductCollection[i]["Qty"] + "</td>";
               html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
               html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
               html += "<td>" + ProductCollection[i]["Amount"] + "</td>";
               html += "<td>" + ProductCollection[i]["Tax_Rate"] + "</td>";
               html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
               html += "</tr>";

               TotalAmount += parseFloat(ProductCollection[i]["Amount"])

           }
       
           $("#txtTotalAmount").val(TotalAmount);

           $("#tbKitProducts").html(html);

       }


       function ResetControls() {

           m_KitID = 0;
           $("#tbKitProducts").html("");
           $("#txtTotalAmount").val("");
           $("#txtMRP").val("");
           $("#txtSaleRate").val("");
           $("#txtddlItems").val("").removeAttr("disabled");
           $("#ddlItems").html("<option value='0'></option>")
           ProductCollection = [];
           ResetList();
       }

       function ResetList() {

           $("#txtCode").val("");
           $("#txtName").val("");
           $("#txtQty").val("");
           $("#txtRate").val("");
           $("#txtMarketPrice").val("");
           $("#txtAmount").val("");
           $("#txtTax").val("");
           $("#ddlProducts").html("<option value='0'></option>");
           $("#txtddlProducts").val("").focus();
           
           var arrRole = [];
           arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

           $("#btnEdit").css({ "display": "none" });
           $("#btnNew").css({ "display": "none" });

           $("#btnDelete").css({ "display": "none" });

             for (var i = 0; i < arrRole.length; i++) {

                 if (arrRole[i] == "1") {

                     $("#btnNew").css({ "display": "block" });
                 }

                 if (arrRole[i] == "3") {

                     $("#btnEdit").css({ "display": "block" });
                 }

                 if (arrRole[i] == "2") {

                     $("#btnDelete").css({ "display": "block" });
                 }
             }


         

          
           


           
       }

       function GetPluginData(Type) {

           if (Type == "Kit") {

               var IsExist = $("#ddlItems option:selected").attr("is_exist");

               if (IsExist != 0) {

                   alert("Kit Already Created");
                   $("#txtddlItems").val("").focus();
                   $("#ddlItems").html("<option value='0'></option>")

                   return;

               }
               
               
               
               $("#txtMRP").val($("#ddlItems option:selected").attr("mrp"));
               $("#txtSaleRate").val($("#ddlItems option:selected").attr("sale_rate"));

               
               
               $("#txtTotalAmount").val("0");



           }
           else if (Type == "Product") {

               $("#txtCode").val($("#ddlProducts option:selected").attr("item_code"));
               $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
               $("#txtQty").val("").focus();
               $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
               $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
               $("#txtAmount").val(0);
               $("#txtTax").val($("#ddlProducts option:selected").attr("tax_code"));


           }
       }

       $(document).ready(function () {
           $.ajax({
               type: "POST",
               data: '{ }',
               url: "managekits.aspx/FillSettings",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);
                   mrpeditable = obj.Mrp;
                   saleRateeditable = obj.Sale;
                  
                 
                   if (saleRateeditable == "0") {
                      
                       $("#txtRate").attr('disabled', 'disabled');

                   }
                   else {
                      
                       $("#txtRate").removeAttr('disabled');
                   }
                  
                   if (mrpeditable == "0") {
                       $("#txtMarketPrice").attr('disabled', 'disabled');

                   }
                   else {
                      
                       $("#txtMarketPrice").removeAttr('disabled');
                   }

                
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {


               }

           });


           ResetList();

           $("#btnCancelDialog").click(
           function () {
               $("#KitDialog").dialog("close");

           }
           );

         

           $("#btnSave").click(
            function () {


                var val1 = parseFloat($("#txtTotalAmount").val());
                var val2 = parseFloat($("#txtSaleRate").val());


                if (!(val1 == val2)) {

                    alert("Total Amount  is not  Equal to Sale Rate.");
                    return;

                }


                var Master_Code = $("#ddlItems option:selected").val();
               
                if (Master_Code == "") {
                    alert("hi");
                    $("#txtddlItems").focus();
                    return;
                }
                if (ProductCollection.length == 0) {

                    alert("Please choose Kit Items");
                    return;
                }

               
                var objKit = {};
                objKit.Kit_ID = m_KitID;
                objKit.Master_Code = $("#ddlItems option:selected").val();
                objKit.Item_Name = $("#ddlItems option:selected").text();
                objKit.Amount = $("#txtTotalAmount").val();
                objKit.MRP = $("#txtMRP").val();
                objKit.SALE_RATE = $("#txtSaleRate").val();


                var DTO = { 'objKitDetails': ProductCollection, 'objKit': objKit };


                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "managekits.aspx/InsertUpdate",
                    data: JSON.stringify(DTO),
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                       
                        if (obj.status == -11) {
                            alert("You don't have permission to perform this action..Consult Admin Department.");
                            return;
                        }


                        if (m_KitID == 0) {

                            alert("Kit Added Successfully");

                        }
                        else {

                            alert("Kit Updated Successfully");

                        }
                      
                        BindGrid();
                        $("#KitDialog").dialog("close");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                    }
                });



            });


           $("#btnAddKitItems").click(
           function () {

               if ($("#ddlItems option:selected").val() == "0") {

                   $("#txtddlItems").val("").focus();
                   return;
               }

               if (!(Number($("#txtQty").val()) > 0)) {
                   $("#txtQty").focus();
                   return;

               }

               TO = new clsProduct();

               TO.Item_ID = $("#ddlProducts option:selected").val();
               TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
               TO.Item_Name = $("#ddlProducts option:selected").attr("item_name");
               TO.Qty = $("#txtQty").val();
               TO.Rate = $("#txtRate").val();
               TO.MRP = $("#ddlProducts option:selected").attr("mrp")
               TO.Amount = $("#txtAmount").val();
               TO.Tax_ID = $("#ddlProducts option:selected").attr("tax_id")
               TO.Tax_Rate = $("#ddlProducts option:selected").attr("tax_code");

               ProductCollection.push(TO);

               BindRows();
               ResetList();

           }

           );



           $("#txtQty").keyup(
           function () {

               CommonCalculation();

           }
           );

           $("#txtRate").keyup(
           function () {
               CommonCalculation();


           }
           );



           $("#ddlItems").supersearch({
               Type: "Kit",
               Caption: "Please enter Kit Name/Code ",
               AccountType: "",
               Width: 214,
               DefaultValue: 0,
               Godown: 0
           });


           $("#ddlProducts").supersearch({
               Type: "Product",
               Caption: "Please enter Item Name/Code ",
               AccountType: "",
               Width: 214,
               DefaultValue: 0,
               Godown: 0
           });





           BindGrid();


           ValidateRoles();

           function ValidateRoles() {

               var arrRole = [];
               arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

               for (var i = 0; i < arrRole.length; i++) {
                   if (arrRole[i] == "1") {
                       $("#btnNew").show();
                       $("#btnNew").click(
                                 function () {


                                     ResetControls();
                                     $("#KitDialog").dialog({
                                         autoOpen: true,

                                         width: 1000,
                                         resizable: false,
                                         modal: true
                                     });
                                 }
                                 );


                   }
                   else if (arrRole[i] == "2") {
                       $("#btnDelete").show();

                       $("#btnDelete").click(
      function () {

          var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
          if ($.trim(SelectedRow) == "") {
              alert("No Kit is selected to Delete");
              return;
          }

          var KitID = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Kit_ID')
          if (confirm("Are You sure to delete this record")) {
              $.uiLock('');


              $.ajax({
                  type: "POST",
                  data: '{"KitID":"' + KitID + '"}',
                  url: "managekits.aspx/Delete",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      var obj = jQuery.parseJSON(msg.d);


                      BindGrid();
                      alert("Kit is Deleted successfully.");

                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {
                      $.uiUnlock();
                  }
              });

          }


      }
      );

                   }
                   else if (arrRole[i] == "3") {
                     

                       $("#btnEdit").show();
                       $("#btnEdit").click(

         function () {


             $("#txtddlItems").prop("disabled", true);

             $.ajax({
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 url: "managekits.aspx/GetById",
                 data: '{"KitId":"' + m_KitID + '"}',
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     $("#txtMRP").val(obj.Kit.MRP);
                     $("#txtTotalAmount").val(obj.Kit.Amount);
                     $("#txtSaleRate").val(obj.Kit.SALE_RATE);
                     $("#txtddlItems").val(obj.Kit.Item_Name);
                     $("#ddlItems").html("<option selected=selected value='" + obj.Kit.Master_Code + "'>" + obj.Kit.Item_Name + "</option>");





                     ProductCollection = obj.KitDetail;


                     BindRows();

                     $("#KitDialog").dialog({
                         autoOpen: true,

                         width: 1000,
                         resizable: false,
                         modal: true
                     });

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }
             });



         }
         );



                   }
               }
           }

          


       });
   
    </script>
    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnRoles" runat="server" />
    <asp:HiddenField ID="hdnDate" runat="server" />
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Manage Kits</h3>
                </div>
                <div class="x_panel">
                    <div class="form-group">
                        <div class="youhave"  >
                            <table id="jQGridDemo">
                            </table>
                            <table cellspacing="0" cellpadding="0" style="margin-top: 5px">
                                <tr>
                                   
                                    <td  >
                                        <div id="btnNew" style="display:none;" class="btn btn-primary">
                                           <i class="fa fa-external-link"></i> New</div>
                                    </td>
                                    
                                    <td  >
                                        <div id="btnEdit" style="display:none;" class="btn btn-success">
                                           <i class="fa fa-edit m-right-xs"></i> Edit</div>
                                    </td>
                                   
                                    <td >
                                        <div id="btnDelete" style="display:none;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i> Delete</div>
                                    </td>
                                </tr>
                            </table>
                            <div id="jQGridDemoPager">
                            </div>
                        </div>
                        <div class="row" id="KitDialog" style="display: none">
                            <uc1:AddKit ID="ucAddKit" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            
       
        </div>
    </form>
    <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageKits.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Kit Id','MasterCode','Name','Amount','MRP','SaleRate'],
  


                        colModel: [
                                    { name: 'Kit_ID', key: true, index: 'Kit_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Master_Code', index: 'Master_Code', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Name', index: 'Item_Name', width:400, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Amount', index: 'Amount', width: 150, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'MRP', index: 'MRP', width:150, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'SALE_RATE', index: 'SALE_RATE', width: 150, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                   

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Kit_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Kits List",

                        editurl: 'handlers/ManageKits.ashx',
                           ignoreCase: true,
                         toolbar: [true, "top"],


                    });

                     var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    ResetControls();

                    m_KitID = 0;

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                   $("#btnUpdate").css({ "display": "none" });
                   $("#btnReset").css({ "display": "none" });
                   $("#btnAdd").css({ "display": "none" });
                  

                   for (var i = 0; i < arrRole.length; i++) {

                       if (arrRole[i] == 1) {

                           $("#btnNew").css({ "display": "block" });
                       }
                       if (arrRole[i] == 2) {

                           $("#btnDelete").css({ "display": "block" });
                       }
                       if (arrRole[i] == 3) {
                           $("#btnEdit").css({ "display": "block" });
                           m_KitID = $('#jQGridDemo').jqGrid('getCell', rowid, 'Kit_ID');

                       }
                   }
 
             
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>
