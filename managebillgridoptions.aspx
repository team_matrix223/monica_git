﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebillgridoptions.aspx.cs" Inherits="ApplicationSettings_managebillgridoptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    
     <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Bill Grid Options</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                      
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                        <tr>
                          <td>
                          <table>
                              <tr>
                               <td>
                                 <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double" >
                                     


                                      <tr><td colspan ="100%"  class="tableheadings" >BillType</td></tr>

                                      <tr>
                                              <td class="headings" align="left" style="text-align:left">BRANCH:</td>
                                             <td class="headings" align="left" style="text-align:left;width:100px" colspan="100%"><asp:DropDownList id="ddlBGBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>
                                      <tr><td class="headings" align="left" style="text-align:left">BILL TYPE:</td><td align="left" style="text-align:left;width:100px"><select id="ddlBGBillType" style="width:200px" >
                                    <option value="Retail">Retail</option>
                                    <option value="VAT">VAT</option>
                                   <option value="CST">CST</option>
                                    </select></td></tr> 
                                 </table>

                               </td>

                              </tr>
                              <tr>
                                  <td colspan ="100%">
                                   <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double" >
                                   <tr><td colspan ="100%"  class="tableheadings" >Bill Grid Options</td></tr>
                                   <tr><td class="headings" align="left" style="text-align:left">Enable MRP Editable:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGSaleRate" data-index="2"  name="chkBGSaleRate" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Enable Sale Rate Editable:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGMRP" data-index="2"  name="chkBGMRP" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Show Dis1(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGDis1" data-index="2"  name="chkBGDis1" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Dis1 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGDis1Amt" data-index="2"  name="chkBGDis1Amt" /></td></tr> 
                                  <tr><td class="headings" align="left" style="text-align:left">Show Dis2(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGDis2" data-index="2"  name="chkBGDis2" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Dis2 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGDis2Amt" data-index="2"  name="chkBGDis2Amt" /></td></tr> 
                                       
                                        </table>

                                  </td>

                              </tr>

                          </table>
                          </td>
                            <td valign="top">
                                <table>
                                    <tr>

                                        <td >
                                      <table cellpadding="10" cellspacing="5" border="0" class="table" style="margin-bottom:20px;border-style:double">

                                      <tr><td colspan ="100%" class="tableheadings" >Bill Grid Tax1 Settings</td></tr>
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1" data-index="2"  name="chkBGTax1" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1Amt" data-index="2"  name="chkBGTax1Amt" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1 SurCharge1(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1Sur1" data-index="2"  name="chkBGTax1Sur1" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1 SurCharge1 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1Sur1Amt" data-index="2"  name="chkBGTax1Sur1Amt" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1 SurCharge2(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1Sur2" data-index="2"  name="chkBGTax1Sur2" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax1 SurCharge2 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax1Sur2Amt" data-index="2"  name="chkBGTax1Sur2Amt" /></td></tr> 
                                      </table>
                                  </td>


                                    </tr>


                                </table>



                            </td>
                            
                          <td valign="top">
                              <table>

                                  <tr>

                                  <td>
                                 <table cellpadding="10" cellspacing="5" border="0" class="table" style="margin-bottom:20px;border-style:double">
                                   <tr><td colspan ="100%" class="tableheadings">Bill Grid Tax2 Settings</td></tr>
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2" data-index="2"  name="chkBGTax2" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2Amt" data-index="2"  name="chkBGTax2Amt" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2 SurCharge1(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2Sur1" data-index="2"  name="chkBGTax2Sur1" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2 SurCharge1 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2Sur1Amt" data-index="2"  name="chkBGTax2Sur1Amt" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2 SurCharge2(%):</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2Sur2" data-index="2"  name="chkBGTax2Sur2" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tax2 SurCharge2 Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBGTax2Sur2Amt" data-index="2"  name="chkBGTax2Sur2Amt" /></td></tr> 
                                 </table>

                                 </td>

                                  </tr>

                                   <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnBGAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>
                                                Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                                               

                              </table>
                          </td>


                        </tr>
                     
                     

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>

     <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">

    
       function InsertUpdateGridOptions() {
           var BranchId = 0;
           var objSettings = {};
           var MRP = false;
           var SRate = false;
           var Dis1Per = false;
           var Dis1Amt = false;
           var Dis2Per = false;
           var Dis2Amt = false;
           var Tax1Per = false;
           var Tax1Amt = false;
           var Tax2Per = false;
           var Tax2Amt = false;
           var Tax1Su1Per = false;
           var Tax1Sur1Amt = false;
           var Tax1Sur2Per = false;
           var Tax1Sur2Amt = false;
           var Tax2Su1Per = false;
           var Tax2Sur1Amt = false;
           var Tax2Sur2Per = false;
           var Tax2Sur2Amt = false;
           var Type = "";
           Type = $("#ddlBGBillType").val();
           BranchId = $("#ddlBGBranch").val();
           if (BranchId == "0")
           {
               alert("Choose Branch");
               $("#ddlBGBranch").focus();
               return
           }
           if ($('#chkBGMRP').is(":checked")) {
               MRP = true;

           }
           if ($('#chkBGSaleRate').is(":checked")) {
               SRate = true;

           }
           if ($('#chkBGDis1').is(":checked")) {
               Dis1Per = true;

           }
           if ($('#chkBGDis1Amt').is(":checked")) {
               Dis1Amt = true;

           }
           if ($('#chkBGDis2').is(":checked")) {
               Dis2Per = true;

           }

          
           if ($('#chkBGDis2Amt').is(":checked")) {
               Dis2Amt = true;

           }
           if ($('#chkBGTax1').is(":checked")) {
               Tax1Per = true;

           }
           if ($('#chkBGTax1Amt').is(":checked")) {
               Tax1Amt = true;

           }


           if ($('#chkBGTax1Sur1').is(":checked")) {
               Tax1Su1Per = true;

           }
           if ($('#chkBGTax1Sur1Amt').is(":checked")) {
               Tax1Sur1Amt = true;

           }
           if ($('#chkBGTax1Sur2').is(":checked")) {
               Tax1Sur2Per = true;

           }
           if ($('#chkBGTax1Sur2Amt').is(":checked")) {
               Tax1Sur2Amt = true;

           }
           if ($('#chkBGTax2').is(":checked")) {
               Tax2Per = true;

           }
           if ($('#chkBGTax2Amt').is(":checked")) {
               Tax2Amt = true;

           }
           if ($('#chkBGTax2Sur1').is(":checked")) {
               Tax2Su1Per = true;

           }

           if ($('#chkBGTax2Sur1Amt').is(":checked")) {
               Tax2Sur1Amt = true;

           }
           if ($('#chkBGTax2Sur2').is(":checked")) {
               Tax2Sur2Per = true;

           }

           if ($('#chkBGTax2Sur2Amt').is(":checked")) {
               Tax2Sur2Amt = true;

           }
         
           objSettings.MRP_Enable = MRP;
           objSettings.SRate_Enable = SRate;
           objSettings.Dis1_Per = Dis1Per;
           objSettings.Dis1_Amt = Dis1Amt;
           objSettings.Dis2_Per = Dis2Per;
           objSettings.Dis2_Amt = Dis2Amt;
           objSettings.Tax1_Per = Tax1Per;
           objSettings.Tax1_Amt = Tax1Amt;
           objSettings.Tax1_Sur1_Per = Tax1Su1Per;
           objSettings.Tax1_Sur1_Amt = Tax1Sur1Amt;
           objSettings.Tax1_Sur2_Per = Tax1Sur2Per;
           objSettings.Tax1_Sur2_Amt = Tax1Sur2Amt;      
           objSettings.Tax2_Per = Tax2Per;
           objSettings.Tax2_Amt = Tax2Amt;
           objSettings.Tax2_Sur1_Per = Tax2Su1Per;
           objSettings.Tax2_Sur1_Amt = Tax2Sur1Amt;
           objSettings.Tax2_Sur2_Per = Tax2Sur2Per;
           objSettings.Tax2_Sur2_Amt = Tax2Sur2Amt;
           objSettings.Type = Type;
           objSettings.BranchId = BranchId;

           var DTO = { 'objSettings': objSettings };

           $.uiLock('');

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebillgridoptions.aspx/InsertGridOptions",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {


        function BindSettings()
        {
            $("input[type='checkbox']").prop("checked", false);
            var Type = $("#ddlBGBillType").val();
            var Branch = $("#ddlBGBranch").val();
            if (Branch == "0")
            {
                alert("Please select Branch");
                return;
            }

            $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" ,"Branch":"' + Branch + '"}',
                url: "managebillgridoptions.aspx/FillGridSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    var MRP = obj.setttingData.MRP_Enable;
                    if (MRP == true) {
                        $('#chkBGMRP').prop('checked', true);
                    }
                    else {
                        $('#chkBGMRP').prop('checked', false);
                    }

                    var SRate = obj.setttingData.SRate_Enable;
                    if (SRate == true) {
                        $('#chkBGSaleRate').prop('checked', true);
                    }
                    else {
                        $('#chkBGSaleRate').prop('checked', false);
                    }

                    var Dis1 = obj.setttingData.Dis1_Per;
                    if (Dis1 == true) {
                        $('#chkBGDis1').prop('checked', true);
                    }
                    else {
                        $('#chkBGDis1').prop('checked', false);
                    }

                    var Dis1Amt = obj.setttingData.Dis1_Amt;
                    if (Dis1Amt == true) {
                        $('#chkBGDis1Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGDis1Amt').prop('checked', false);
                    }

                    var Dis2 = obj.setttingData.Dis2_Per;
                    if (Dis2 == true) {
                        $('#chkBGDis2').prop('checked', true);
                    }
                    else {
                        $('#chkBGDis2').prop('checked', false);
                    }

                    var Dis2Amt = obj.setttingData.Dis2_Amt;
                    if (Dis2Amt == true) {
                        $('#chkBGDis2Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGDis2Amt').prop('checked', false);
                    }

                    var Tax1 = obj.setttingData.Tax1_Per;
                    if (Tax1 == true) {
                        $('#chkBGTax1').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1').prop('checked', false);
                    }

                    var Tax1Amt = obj.setttingData.Tax1_Amt;
                    if (Tax1Amt == true) {
                        $('#chkBGTax1Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1Amt').prop('checked', false);
                    }


                    var Tax1Sur1 = obj.setttingData.Tax1_Sur1_Per;
                    if (Tax1Sur1 == true) {
                        $('#chkBGTax1Sur1').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1Sur1').prop('checked', false);
                    }

                    var Tax1Sur1Amt = obj.setttingData.Tax1_Sur1_Amt;
                    if (Tax1Sur1Amt == true) {
                        $('#chkBGTax1Sur1Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1Sur1Amt').prop('checked', false);
                    }


                    var Tax1Sur2 = obj.setttingData.Tax1_Sur2_Per;
                    if (Tax1Sur2 == true) {
                        $('#chkBGTax1Sur2').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1Sur2').prop('checked', false);
                    }

                    var Tax1Sur2Amt = obj.setttingData.Tax1_Sur2_Amt;
                    if (Tax1Sur2Amt == true) {
                        $('#chkBGTax1Sur2Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax1Sur2Amt').prop('checked', false);
                    }



                    var Tax2 = obj.setttingData.Tax2_Per;
                    if (Tax2 == true) {
                        $('#chkBGTax2').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2').prop('checked', false);
                    }

                    var Tax2Amt = obj.setttingData.Tax2_Amt;
                    if (Tax2Amt == true) {
                        $('#chkBGTax2Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2Amt').prop('checked', false);
                    }


                    var Tax2Sur1 = obj.setttingData.Tax2_Sur1_Per;
                    if (Tax2Sur1 == true) {
                        $('#chkBGTax2Sur1').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2Sur1').prop('checked', false);
                    }

                    var Tax2Sur1Amt = obj.setttingData.Tax2_Sur1_Amt;
                    if (Tax2Sur1Amt == true) {
                        $('#chkBGTax2Sur1Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2Sur1Amt').prop('checked', false);
                    }


                    var Tax2Sur2 = obj.setttingData.Tax2_Sur2_Per;
                    if (Tax2Sur2 == true) {
                        $('#chkBGTax2Sur2').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2Sur2').prop('checked', false);
                    }

                    var Tax2Sur2Amt = obj.setttingData.Tax2_Sur2_Amt;
                    if (Tax2Sur2Amt == true) {
                        $('#chkBGTax2Sur2Amt').prop('checked', true);
                    }
                    else {
                        $('#chkBGTax2Sur2Amt').prop('checked', false);
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }

            });



        }


        $("#ddlBGBranch").change(function () {


            BindSettings();
        });

     

        $("#ddlBGBillType").change(function () {
            
         
            BindSettings();
        });



        $("#btnBGAdd").click(
        function () {
         
            InsertUpdateGridOptions();
        }
        );


    });


        </script>

</asp:Content>

