﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // BindBranches();
        }
    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void getsalerate()
    {
        string con = ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString;
        SqlConnection conn = new SqlConnection(con);
        string com = "select Sale_Rate from branch_master where branchid=" + ddlBranch.SelectedValue;
        SqlCommand cmd = new SqlCommand(com, conn);
        conn.Open();
        SqlDataReader DR1 = cmd.ExecuteReader();
        if (DR1.Read())
        {
            ddlsalerate.Text = DR1.GetValue(0).ToString();
        }
        conn.Close();

    }
    protected void getsalerate(object sender, EventArgs e)
    {
        getsalerate();
    }
    protected void itemSelected(object sender, EventArgs e)
    {
         var locid = ddlLocation.SelectedValue;
        if (locid == "1")
        {
           Response.Cookies[Constants.DataBase].Value = "1";
            BindBranches();
        }
        else if (locid == "2")
        {
          Response.Cookies[Constants.DataBase].Value = "6";
            BindBranches();
        }
        else if (locid == "3")
        {
            //Response.Cookies[Constants.DataBase].Value = "7";
            Response.Cookies[Constants.DataBase].Value = "7";
            BindBranches();
        }
        else if (locid == "4")
        {
           Response.Cookies[Constants.DataBase].Value = "8";
            BindBranches();
        }

       
        else
        {

            Response.Write("<script>alert('Choose Location First')</script>");
            return;
        }


    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {

        if (ddlBranch.SelectedValue == "0")
        {

            Response.Write("<script>alert('Choose Branch First')</script>");
            return;
        }

        User objuser = new User()
        {
            User_ID = txtUserName.Value,
            UserPWD = txtPassword.Value

        };

        int Branchvalue = Convert.ToInt32(ddlBranch.SelectedValue);

        Int32 status = new UserBLL().SuperUserLoginCheck(objuser,Branchvalue);


        if (status.ToString() == "-1")
        {
            Response.Write("<script>alert('Invalid User Name');</script>");
        }
        else if (status.ToString() == "-2")
        {
            Response.Write("<script>alert('Invalid Password');</script>");
        }
        else if (status.ToString() == "-3")
        {
            Response.Write("<script>alert('Invalid Branch');</script>");
        }
        else
        {
            int Result = new SaleBLL().DeletePrinterTable(status);
            Response.Redirect("http://localhost:62606/SuperWelcome.aspx?bid=" + ddlBranch.SelectedValue + "&bname=" + ddlBranch.SelectedItem.Text + "&Sale_Rate=" + ddlsalerate.Text + "&UserId=" + status + "&UserName=" + txtUserName.Value.Trim());
        }


    }
}