﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebillseries.aspx.cs" Inherits="ApplicationSettings_managebillseries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    
     <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Bill Series</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                      
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                        <tr>
                          <td>
                          <table>
                              <tr>
                               <td>
                                 <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double" >
                                      <tr><td colspan ="100%"  class="tableheadings" >Bill Series Bill Type Wise</td></tr>
                                     
                                     
                                       <tr><td class="headings" align="left" style="text-align:left">BRANCH:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBSLocation" style="width:200px"></asp:DropDownList></td></tr>
                                      <tr><td class="headings" align="left" style="text-align:left">BILL TYPE:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><select id="ddlBSBillType" style="width:200px" >
                                    <option value="Retail">Retail</option>
                                    <option value="VAT">VAT</option>
                                   <option value="CST">Against Sale tax</option>
                                    </select></td></tr>
                                     
                                   
                                    </asp:DropDownList></td><td class="headings" align="left" style="text-align:left">Cash Series</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id ="txtBSCashSeries" style="width:100px" /></td></tr>

                                      <tr><td class="headings" align="left" style="text-align:left">Location Prefix:</td><td align="left" style="text-align:left;width:100px" ><input type="text" id ="txtBSPrefix" style="width:100px" /></td>
                                          <td class="headings" align="left" style="text-align:left">Credit Series</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id ="txtBSCreditSeries" style="width:100px" /></td></tr>

                                      <tr><td class="headings" align="left" style="text-align:left">Default Godown:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList ClientIDMode="Static" runat="server" id="ddlBSGodown" style="width:200px" ></asp:DropDownList></td>
                                      <td class="headings" align="left" style="text-align:left">CrCard Series</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id ="txtBSCrCardSeries" style="width:100px" /></td></tr>
                                      
                                     <tr><td class="headings" align="left" style="text-align:left">Series Name:</td><td align="left" style="text-align:left;width:100px" ><input type="text" id ="txtBSSeriesName" style="width:100px" /></td>
                                         <td class="headings" align="left" style="text-align:left">Open/Close:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkBSOpenClose" data-index="2"  name="chkBSOpenClose" /></td>
                                        <td class="headings" align="left" style="text-align:left">Bill Date Editable:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkBSBillDate" data-index="2"  name="chkBSBillDate" /></td>
                                     
                                     </tr>
                                    
                                     
                                 </table>

                               </td>

                              </tr>
                           
                          </table>
                          </td>
                           
                                  </tr>

                                   <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnBSAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Apply Settings</div></td>
                                            <td> <div id="btnBSRefresh"   class="btn btn-danger btn-small" ><i class= m-right-xs"></i>Refresh</div></td>
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                                               

                             
                     

                     </table>

                        </div>
                    </div>


       <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Bill Series</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>

     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i>Delete Series</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>



    
    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">

       var Type = "";
       var Branch = 0;

       function Reset() {
           
         

           $("#txtBSPrefix").val("");

           $("#txtBSCashSeries").val("");
           $("#txtBSCreditSeries").val("");
            $("#txtBSCrCardSeries").val("");
          $("#ddlBSGodown").val("0");

           $("#txtBSSeriesName").val("");
           //$("#ddlBSBillType").val("0");
           jQuery("#jQGridDemo").GridUnload();
           BindGrid(Type,Branch);

       }
       function InsertUpdateSeries() {
          
           var objSettings = {};
           var LocationId = "" ;
           var Locationname = "";
           var Prefix = "";
           var CashSeries = "" ;
           var CreditSeries = "";
           var CrCardSeries = "";
           var open = false;
           var billdate = false ;
           var defaultgodown = "";
           var Billtype = "";
           var seriesname = "";

           LocationId = $("#ddlBSLocation").val();
           Locationname = $("#ddlBSLocation option:selected").text();
           Prefix = $("#txtBSPrefix").val();
         
           CashSeries = $("#txtBSCashSeries").val();
           CreditSeries = $("#txtBSCreditSeries").val();
           CrCardSeries = $("#txtBSCrCardSeries").val();
           defaultgodown = $("#ddlBSGodown").val();
          
           seriesname = $("#txtBSSeriesName").val();
           Billtype = $("#ddlBSBillType").val();
           if ($('#chkBSOpenClose').is(":checked")) {
               open = true;

           }

           if ($('#chkBSBillDate').is(":checked")) {
               billdate = true;

           }
          
           objSettings.BranchId = LocationId;
           objSettings.BranchName = Locationname;
           objSettings.Prefix = Prefix;
           objSettings.CashSeries = CashSeries;
           objSettings.CreditSeries = CreditSeries;
           objSettings.CrCardSeries = CrCardSeries;
           objSettings.OpenClose = open;
           objSettings.BillDate = billdate;
           objSettings.DefaultGodown = defaultgodown;
           objSettings.Type = Billtype;
           objSettings.Series_Name = seriesname;
         

           var DTO = { 'objSettings': objSettings };

           $.uiLock('');


           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebillseries.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {
                       BindGrid(Type);
                       alert("Setting Applied Successfully.");
                       Reset();
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }

       $(document).ready(
  function () {


      $("#ddlBSLocation").change(
          function ()
          {
              Reset();

          }
          );
      $("#ddlBSBillType").change(function () {
         
          Reset();
           
         

         });



      $("#btnBSAdd").click(
    function () {

        InsertUpdateSeries();
    }
    );

      $("#btnBSRefresh").click(
  function () {

      Reset();
  }
  );


  });


   </script>




      <script type="text/javascript">
          function BindGrid(Type, Branch) {


              var Type = $("#ddlBSBillType").val();
              var Branch = $("#ddlBSLocation").val();

              if (Branch == "0") {
                  alert("Please select Branch");
                  return;
              }

             


              jQuery("#jQGridDemo").GridUnload();
              jQuery("#jQGridDemo").jqGrid({
                  url: 'handlers/GetBillSeriesByType.ashx?Type=' + Type +'&Branch='+Branch,
                  ajaxGridOptions: { contentType: "application/json" },
                  datatype: "json",

                  colNames: ['LocationID', 'LocationName', 'Prefix', 'CashSeries', 'CreditSeries', 'CrCardSeries','Open/Close','BillDate','Godown','BillType','Series'],
                  colModel: [
                             

                              { name: 'BranchId', index: 'BranchId', width: 200, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                              { name: 'BranchName', index: 'BranchName', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                              { name: 'Prefix', index: 'Prefix', width: 200, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                              { name: 'CashSeries', index: 'CashSeries', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                              { name: 'CreditSeries', index: 'CreditSeries', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                              { name: 'CrCardSeries', index: 'CrCardSeries', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                               { name: 'OpenClose', index: 'OpenClose', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                              { name: 'BillDate', index: 'BillDate', width: 150, editable: true, hidden: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                               { name: 'DefaultGodown', index: 'DefaultGodown', width: 200, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                              { name: 'Type', index: 'Type', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                              { name: 'Series_Name', index: 'Series_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                            

                             
                  ],
                  rowNum: 10,

                  mtype: 'GET',
                  loadonce: true,
                  rowList: [10, 20, 30],
                  pager: '#jQGridDemoPager',
                  sortname: 'LocationID',
                  viewrecords: true,
                  height: "100%",
                  width: "400px",
                  sortorder: 'asc',
                  caption: "AddOn List",

                  editurl: 'handlers/managebrand.ashx',



                  ignoreCase: true,
                  toolbar: [true, "top"],


              });


              var $grid = $("#jQGridDemo");
              // fill top toolbar
              $('#t_' + $.jgrid.jqID($grid[0].id))
                  .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
              $("#globalSearchText").keypress(function (e) {
                  var key = e.charCode || e.keyCode || 0;
                  if (key === $.ui.keyCode.ENTER) { // 13
                      $("#globalSearch").click();
                  }
              });
              $("#globalSearch").button({
                  icons: { primary: "ui-icon-search" },
                  text: false
              }).click(function () {
                  var postData = $grid.jqGrid("getGridParam", "postData"),
                      colModel = $grid.jqGrid("getGridParam", "colModel"),
                      rules = [],
                      searchText = $("#globalSearchText").val(),
                      l = colModel.length,
                      i,
                      cm;
                  for (i = 0; i < l; i++) {
                      cm = colModel[i];
                      if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                          rules.push({
                              field: cm.name,
                              op: "cn",
                              data: searchText
                          });
                      }
                  }
                  postData.filters = JSON.stringify({
                      groupOp: "OR",
                      rules: rules
                  });
                  $grid.jqGrid("setGridParam", { search: true });
                  $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                  return false;
              });

              $("#jQGridDemo").jqGrid('setGridParam',
      {
          onSelectRow: function (rowid, iRow, iCol, e) {

              var location = $('#jQGridDemo').jqGrid('getCell', rowid, 'LocationID')
              $('#ddlBSLocation option[value=' + location + ']').prop('selected', 'selected');

              $("#txtBSPrefix").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Prefix'));
              $("#txtBSCashSeries").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CashSeries'));
              $("#txtBSCreditSeries").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CreditSeries'));
              $("#txtBSCrCardSeries").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CrCardSeries'));
              var open = $('#jQGridDemo').jqGrid('getCell', rowid, 'OpenClose');
              
             

              if (open == "true") {
                 
                  $('#chkBSOpenClose').prop('checked', true);
              }
              else {
                  $('#chkBSOpenClose').prop('checked', false);
              }
             
           
              var Billdate = $('#jQGridDemo').jqGrid('getCell', rowid, 'BillDate');

             
              if (Billdate == "true") {
                 
                  $('#chkBSBillDate').prop('checked', true);
              }
              else {
                  $('#chkBSBillDate').prop('checked', false);
              }
              
              $("#txtBSSeriesName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Series_Name'));

              var Godown = $('#jQGridDemo').jqGrid('getCell', rowid, 'DefaultGodown')
              $('#ddlBSGodown option[value=' + Godown + ']').prop('selected', 'selected');

              var Type = $('#jQGridDemo').jqGrid('getCell', rowid, 'Type')
              $('#ddlBSBillType option[value=' + Type + ']').prop('selected', 'selected');
          }
            });

             var DataGrid = jQuery('#jQGridDemo');
             DataGrid.jqGrid('setGridWidth', '700');
             $.uiUnlock();
             $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                              {
                                  refresh: false,
                                  edit: false,
                                  add: false,
                                  del: false,
                                  search: false,
                                  searchtext: "Search",
                                  addtext: "Add",
                              },

                              {//SEARCH
                                  closeOnEscape: true

                              }

                                );



         }





    </script>


      
</asp:Content>

