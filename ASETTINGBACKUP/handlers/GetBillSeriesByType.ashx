﻿<%@ WebHandler Language="C#" Class="GetBillSeriesByType" %>

using System;
using System.Web;

public class GetBillSeriesByType : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string Type = context.Request.QueryString["Type"];
        if (Type != null)
        {


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new BillSeriesSettingBLL().GetByBillType(Convert.ToString(Type))
               );

            context.Response.Write(xss);
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}