﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managekitsetting.aspx.cs" Inherits="ApplicationSettings_managekitsetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    
    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Item Master Settings</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">
    <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">
        
            <tr><td colspan="100%"  class="tableheadings"><b>Kit Detail Setting</b> </td></tr>
           
       
            
          <asp:Literal ID="ltKitSettings" runat="server"></asp:Literal> 

               
          
            
          
         <tr> <td colspan="100%"><br /><input type="button" id="btnUpdate" class="btn btn-primary btn-small" value="Apply Settings"/></td></tr>
    </table>

                            </div>

                        </div>

                    </div>

       </div>
        </form>


       <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="../js/jquery.uilock.js"></script>

    <script language="javascript" type="text/javascript">

        $(document).ready(
            function () {
                $("#btnUpdate").click(
                    function () {
                        var cname = "";
                        var cedit = "";

                        $("input[name='settings']").each(
                            function () {
                                cname = cname + $(this).val() + ",";
                                cedit = cedit + $(this).prop("checked") + ",";
                            }

                            );



                        $.uiLock('');

                        $.ajax({
                            type: "POST",
                            data: '{"Column":"' + cname + '","Value": "' + cedit + '"}',
                            url: "managekitsetting.aspx/Update",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);
                                if (obj.Status == "1") {
                                    alert("Settings Applied Successfully");
                                }
                                else {
                                    alert("Updation Failed. Please try again Later");

                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {
                                $.uiUnlock();
                            }
                        });



                    }

                    );


            }


            );

    </script>

   
</asp:Content>

