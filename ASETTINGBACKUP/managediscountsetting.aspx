﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managediscountsetting.aspx.cs" Inherits="ApplicationSettings_managediscountsetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    
     <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Discount Setting</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                      
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                        <tr>
                          <td>
                          <table>
                              <tr>
                               <td>
                                 <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double" >
                                      <tr><td colspan ="100%"  class="tableheadings" >Discount Options</td></tr>
                                      <tr>
                                              <td class="headings" align="left" style="text-align:left">BRANCH:</td>
                                             <td class="headings" align="left" style="text-align:left;width:100px" colspan="100%"><asp:DropDownList id="ddlBDBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>

                                      <tr><td class="headings" align="left" style="text-align:left">BILLING TYPE:</td><td align="left" style="text-align:left;width:100px"><select id="ddlBDBillType" style="width:200px" >
                                    <option value="Retail">Retail</option>
                                    <option value="VAT">VAT</option>
                                   <option value="CST">CST</option>
                                    </select></td></tr> 

                                    <tr><td class="headings" align="left" style="text-align:left">Allow Discount In Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBDAllowDisOnBilling" data-index="2"  valu name="chkBDAllowDisOnBilling" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Enable Discount Column:</td><td align="left" style="text-align:left;width:100px"><input type="radio"  id="rdoBDDisCol" name="rdoBDDisOnBillValue" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Discount On Bill Value:</td><td align="left" style="text-align:left;width:100px"><input type="radio"  id="rdoBDDisOnBillValue" name="rdoBDDisOnBillValue" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Enable Back End Discount:</td><td align="left" style="text-align:left;width:100px"><input type="radio"  id="rdoBDBackendDis" name="rdoBDDisOnBillValue" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Enable Discount(%) And Amount:</td><td align="left" style="text-align:left;width:100px"><input type="radio"  id="rdoBDDisAmount" name="rdoBDDisOnBillValue" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Enable Customer Discount:</td><td align="left" style="text-align:left;width:100px"><input type="radio"  id="rdoBDCustDis" name="rdoBDDisOnBillValue" /></td></tr> 


                                 </table>

                               </td>

                              </tr>
                              

                          </table>
                          </td>
                           </tr>

                             <tr>
                                 <td colspan="100%">
                                     <table class="table">
                                         <tr>
                                             <th></th>
                                             <th>Start Value</th>
                                             <th>End Value</th>
                                             <th>Discount</th>
                                            <th></th>
                                         </tr>
                                         <tr>
                                             <td></td>
                                             <td><input type="text" id="txtStartValue" /></td>
                                             <td><input type="text" id="txtEndValue" /></td>
                                             <td><input type="text" id="txtDiscountValue" /></td>


                                             <td><input type="button" class="btn btn-primary" id="btnAddDiscount" style="padding:1px 10px 1px 10px" value="Add"/></td>

                                         </tr>

                                         <tbody id="tbItems">



                                         </tbody>
                                     </table>

                                 </td>


                             </tr>

                                   <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnBDAdd"   class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                                               

                              
                     

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>

     <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">

      
       var ProductCollection = [];
       function clsProduct() {
           this.StartValue = 0;
           this.EndValue = 0;
           this.DisPer= 0;
   
       }
       $(document).on("click", "#btnDel", function (event) {

           var RowIndex = Number($(this).closest('tr').index());

           ProductCollection.splice(RowIndex, 1);
           BindRows();


       });


       function InsertUpdateDiscountOptions() {


           if ($('#rdoBDBackendDis').prop('checked') == true) {

               if (ProductCollection.length == 0) {

                   alert("Please add Discount Details");
                   $("#txtStartValue").focus();
                   return;
               }
           }
 

           var objSettings = {};

           var BranchId = 0;

           var AllowDisInBill = false;
           var EnableDisCol = false;
           var BackEndDis = false;
           var DisOnBill = false;
           var EnableDisAndAmt = false;
           var EnableCustDis = false;
          

           BranchId = $("#ddlBDBranch").val();
           if (BranchId == "0")
           {
               alert("Choose Branch");
               $("#ddlBDBranch").focus();
               return;
           }
           var Type = "";
           Type = $("#ddlBDBillType").val();

          
           if ($('#chkBDAllowDisOnBilling').is(":checked")) {
               AllowDisInBill = true;

           }

           if ($('#rdoBDDisCol').prop('checked') == true) {
               EnableDisCol = true;
           }
          
           if ($('#rdoBDBackendDis').prop('checked') == true) {
               BackEndDis = true;
           }
           if ($('#rdoBDDisOnBillValue').prop('checked') == true) {
               DisOnBill = true;
           }
           if ($('#rdoBDDisAmount').prop('checked') == true) {
               EnableDisAndAmt = true;
           }

           if ($('#rdoBDCustDis').prop('checked') == true) {
               EnableCustDis = true;
           }

           objSettings.Allow_Dis_on_Billing = AllowDisInBill;
           objSettings.Enable_Dis_Col = EnableDisCol;
           objSettings.Dis_Bill_Value = DisOnBill;
           objSettings.Back_End_Discount = BackEndDis;
           objSettings.Enable_Dis_Amt = EnableDisAndAmt;
           objSettings.Enable_Cust_Dis = EnableCustDis;
           objSettings.Type = Type;
           objSettings.BranchId = BranchId;

           var DTO = { 'objSettings': objSettings, 'objDisDetail': ProductCollection};

           $.uiLock('');

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managediscountsetting.aspx/InsertDiscountOptions",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }



       function BindRows() {
 
           var html = "";
           for (var i = 0; i < ProductCollection.length; i++) {




               html += "<tr><td></td>";
               html += "<td>" + ProductCollection[i]["StartValue"] + "</td>";
               html += "<td>" + ProductCollection[i]["EndValue"] + "</td>";
               html += "<td>" + ProductCollection[i]["DisPer"] + "</td>";
               html += "<td><img id='btnDel' src='../images/trashico.png'  style='cursor:pointer'   /></td>";
               html += "</tr>";

              

           }

      
           $("#tbItems").html(html);

       }

       function ResetList()
       {

           $("#txtStartValue").val("").focus();
           $("#txtEndValue").val("");
           $("#txtDiscountValue").val("");

       }

       $(document).ready(
    function () {

      
        $("#txtStartValue").prop('disabled', true);
        $("#txtEndValue").prop('disabled', true);
        $("#txtDiscountValue").prop('disabled', true);


        $("input[name='rdoBDDisOnBillValue']").change(function () {

            if ($(this).attr("id") == "rdoBDBackendDis") {


                $("#txtStartValue").prop('disabled', false);
                $("#txtEndValue").prop('disabled', false);
                $("#txtDiscountValue").prop('disabled', false);
            }
            else {

                $("#txtStartValue").prop('disabled', true);
                $("#txtEndValue").prop('disabled', true);
                $("#txtDiscountValue").prop('disabled', true);

            }
        }

        );


        $("#btnAddDiscount").click(
            function ()
            {

                if (isNaN($("#txtStartValue").val()) || $("#txtStartValue").val()=="")
                {          
                    $("#txtStartValue").focus();
                    return;
                }



                if (isNaN($("#txtEndValue").val()) || $("#txtEndValue").val() == "") {
                    $("#txtEndValue").focus();
                    return;
                }




                if (isNaN($("#txtDiscountValue").val()) || $("#txtDiscountValue").val() == "") {
                    $("#txtDiscountValue").focus();
                    return;
                }



                if (parseFloat($("#txtStartValue").val()) >= parseFloat($("#txtEndValue").val())) {
                    alert("End Value Should Be Greater Than Start Value");
                    $("#txtEndValue").focus();
                    return;


                }

                TO = new clsProduct();

                TO.StartValue = $("#txtStartValue").val(); 
                TO.EndValue = $("#txtEndValue").val();
                TO.DisPer = $("#txtDiscountValue").val();
                
                ProductCollection.push(TO);

                BindRows();
                ResetList();

            }

            );


        function BindSettings()
        {

            var Branch = $("#ddlBDBranch").val();
            var Type = $("#ddlBDBillType").val();

            if (Branch == "0")
            {
                alert("Please select Branch");
                return;
            }

            $.uiLock('');


            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '","Branch":"' + Branch + '" }',
                url: "managediscountsetting.aspx/FillGridSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    var AllowdisOnBill = obj.setttingData.Allow_Dis_on_Billing;
                    if (AllowdisOnBill == true) {
                        $('#chkBDAllowDisOnBilling').prop('checked', true);
                    }
                    else {
                        $('#chkBDAllowDisOnBilling').prop('checked', false);
                    }

                    var EnableDisCol = obj.setttingData.Enable_Dis_Col;
                    if (EnableDisCol == true) {
                        $('#rdoBDDisCol').prop('checked', true);
                    }
                    else {
                        $('#rdoBDDisCol').prop('checked', false);
                    }

                    var DiscountOnBillvalue = obj.setttingData.Dis_Bill_Value;
                    if (DiscountOnBillvalue == true) {
                        $('#rdoBDDisOnBillValue').prop('checked', true);
                    }
                    else {
                        $('#rdoBDDisOnBillValue').prop('checked', false);
                    }

                    var BackendDis = obj.setttingData.Back_End_Discount;
                    if (BackendDis == true) {
                        $('#rdoBDBackendDis').prop('checked', true);
                        $("#txtStartValue").prop('disabled', false);
                        $("#txtEndValue").prop('disabled', false);
                        $("#txtDiscountValue").prop('disabled', false);

                    }
                    else {
                        $('#rdoBDBackendDis').prop('checked', false);
                        $("#txtStartValue").prop('disabled', true);
                        $("#txtEndValue").prop('disabled', true);
                        $("#txtDiscountValue").prop('disabled', true);

                    }


                    var EnableDisAmount = obj.setttingData.Enable_Dis_Amt;
                    if (EnableDisAmount == true) {
                        $('#rdoBDDisAmount').prop('checked', true);
                    }
                    else {
                        $('#rdoBDDisAmount').prop('checked', false);
                    }

                    var EnableCustDis = obj.setttingData.Enable_Cust_Dis;
                    if (EnableCustDis == true) {
                        $('#rdoBDCustDis').prop('checked', true);
                    }
                    else {
                        $('#rdoBDCustDis').prop('checked', false);
                    }

                    ProductCollection = obj.DiscountDetail;
                    BindRows();

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }

            });




        }


        $("#ddlBDBillType").change(function () {
            
            BindSettings();
        });


        $("#ddlBDBranch").change(function () {

            BindSettings();
        });

        


        $("#btnBDAdd").click(
        function () {

            InsertUpdateDiscountOptions();
        }
        );

    });


        </script>
</asp:Content>

