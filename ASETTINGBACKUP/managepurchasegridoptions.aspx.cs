﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managepurchasegridoptions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridSettingsTab1();
            BindGridSettingsTab2();
            
        }

        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASESETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    [WebMethod]
    public static string Update(string Column,string Value)
    {
        Column = Column.Substring(0, Column.Length - 1);
        Value = Value.Substring(0, Value.Length - 1);

        string[] arrColumn = Column.Split(',');
        string[] arrValue = Value.Split(',');

        string Query = "";
        for (int i = 0; i < arrColumn.Length; i++)
        {
            var val = arrValue[i] == "true" ? "1" : "0";

            Query += "update MasterSettting_PurchaseGridOption  set CEDIT='" + val + "' where ColumnName='" + arrColumn[i] + "' and Type='PR'";
        
        }
      int status=  new CommonSettingsBLL().UpdateOptions(Query);

        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
       
    }

    [WebMethod]
    public static string UpdatePurchaseReturn(string Column, string Value)
    {
        Column = Column.Substring(0, Column.Length - 1);
        Value = Value.Substring(0, Value.Length - 1);

        string[] arrColumn = Column.Split(',');
        string[] arrValue = Value.Split(',');

        string Query = "";
        for (int i = 0; i < arrColumn.Length; i++)
        {
            var val = arrValue[i] == "true" ? "1" : "0";

            Query += "update MasterSettting_PurchaseGridOption  set CEDIT='" + val + "' where ColumnName='" + arrColumn[i] + "' and Type='PP'";

        }
        int status = new CommonSettingsBLL().UpdateOptions(Query);

        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }


    public void BindGridSettingsTab1()
    {
        ltPurchaseSettings.Text = new CommonSettingsBLL().GetPurchaseGridOptionsByType("PR");
    
    }

    public void BindGridSettingsTab2()
    {
        ltPurchaseReturnSettings.Text = new CommonSettingsBLL().GetPurchaseGridOptionsByType("PP");

    }
}