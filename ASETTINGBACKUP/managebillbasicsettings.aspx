﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebillbasicsettings.aspx.cs" Inherits="ApplicationSettings_managebillbasicsettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

    <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
     <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Bill Form Settings</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                        <tr>
                          <td valign="top">
                          <table>

                               <tr>
                                              <td class="tableheadings"  align="right" style="text-align:left">Branch:</td></tr>
                                             <tr> <td class="headings" align="left" style="text-align:left;width:300px;padding-bottom:10px;padding-top:10px" colspan="100%"><asp:DropDownList id="ddlBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>
                              <tr>
                                  <td>
                                   <table cellpadding="10" cellspacing="5" border="0"   class="table" style="margin-bottom:20px;border-style:double" >
                                   <tr><td colspan ="100%" class="tableheadings"  >CUSTOMER SETTINGS</td></tr>
                                   <tr><td class="headings" align="left" style="text-align:left">Enable Cash Customer:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkCashCustomer" data-index="2"  name="chkCashCustomer" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Start Customer Point System:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkStrtCustomer" data-index="2"  name="chkStrtCustomer" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Default Paymode:</td><td align="left" style="text-align:left;width:100px"><select id="ddlPaymode" style="width:200px" >
                                    <option value="Cash">Cash</option>
                                    <option value="Credit">Credit</option>
                                    <option value="CreditCard">Credit Card</option>
                                    </select></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Default Bank:</td><td align="left" style="text-align:left;width:100px"><asp:DropDownList id="ddlBank" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td></tr> 
                                  <tr></tr>
                                       <tr></tr>
                                       
                                        </table>

                                  </td>

                              </tr>
                              <tr>
                                  <td>
                                      <table cellpadding="10" cellspacing="5" border="0" class="table" style="margin-bottom:20px;">

                                      <tr><td colspan ="100%"  class="tableheadings" >BILL OPTIONS</td></tr>
                                   <tr><td class="headings" align="left" style="text-align:left">Round Bill Amount:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkRoundAmt" data-index="2"  name="chkRoundAmt" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Show Rate Window:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkShowRate" data-index="2"  name="chkShowRate" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Allow ItemName Editable:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkItemNameEdit" data-index="2"  name="chkItemNameEdit" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Apply Tax 2 On Retail Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkTaxRetail" data-index="2"  name="chkTaxRetail" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Apply Tax 2 On VAT Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkTaxVAT" data-index="2"  name="chkTaxVAT" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Apply Tax 2 On CST Billing:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkTaxCST" data-index="2"  name="chkTaxCST" /></td></tr> 
                                      </table>
                                  </td>
                              </tr>

                              
                           
                          </table>
                          </td>
                            
                          <td valign="top">
                              <table>


                                  <tr>
                                  <td>
                                 <table cellpadding="10" cellspacing="5" border="0" class="table"  style="margin-bottom:20px;border-style:double">
                                     <tr><td colspan ="100%"  class="tableheadings" >STOCK OPTIONS</td></tr>
                                      <tr><td class="headings" align="left" style="text-align:left">Afect FOC Stock:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkFOC" data-index="2"  name="chkFOC" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Allow -Ve Stock:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkNegative" data-index="2"  name="chkNegative" /></td></tr> 
                                    <tr><td class="headings" align="left" style="text-align:left">Stock:</td><td align="left" style="text-align:left;width:100px"><select id="ddlStock" style="width:200px" >
                                    <option value="0">Multi Rate Tacking</option>
                                    <option value="1">Single Rate Tacking</option>
                                   
                                    </select></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Kit Stock:</td><td align="left" style="text-align:left;width:100px"><select id="ddlKit" style="width:200px" >
                                  <option value="0">Add Kit</option>
                                  <option value="1">Billing Kit item Stock</option>

                                   </select></td></tr> 
                                 </table>

                                 </td>

                                  </tr>


                                  <tr>

                                  <td>
                                 <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">
                                   <tr><td colspan ="100%"  class="tableheadings" >OTHER OPTIONS</td></tr>
                                    <tr><td class="headings" align="left" style="text-align:left">Enable Decimal In Barcode:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBarcode" data-index="2"  name="chkBarcode" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Bill Hold Lock System:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkHoldSystem" data-index="2"  name="chkHoldSystem" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Show Tender Window After Bill Save:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkTenderWindow" data-index="2"  name="chkTenderWindow" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Box System:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkBox" data-index="2"  name="chkBox" /></td></tr> 
                                  
                                   <tr><td class="headings" align="left" style="text-align:left">Kot System:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkKot" data-index="2"  name="chkKot" /></td></tr> 
                                 </table>

                                 </td>

                                  </tr>

                                    <tr>
                                 <td>
                                 <table cellpadding="10" cellspacing="5" border="0" class="table"  style="margin-bottom:20px;border-style:double">
                                     <tr><td colspan ="100%"  class="tableheadings" >PRINT OPTIONS</td></tr>
                                     <tr><td class="headings" align="left" style="text-align:left">Print Item ShortName:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkShortName" data-index="2"  name="chkShortName" /></td></tr> 
                                   <tr><td class="headings" align="left" style="text-align:left">Print Header Duplicate:</td><td align="left" style="text-align:left;width:100px"><input type="checkbox" id="chkHeaderDuplicate" data-index="2"  name="chkHeaderDuplicate" /></td></tr> 
                                 </table>

                                 </td>
                             </tr>
                                  
                                            <tr>
                                           
                                            <td   align="right" style="padding-top:30px">
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="width:200px"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>
                                                Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                              </table>
                          </td>


                        </tr>
                     
                      

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>



     <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">


       function ResetControls() {
           $("#ddlKit").val("0");
           $("#ddlStock").val("0");
           $("#ddlPaymode").val("0");
           $("#ddlBank").val("0");
           $('#chkCashCustomer').prop('checked', false);
           $('#chkStrtCustomer').prop('checked', false);
           $('#chkRoundAmt').prop('checked', false);
           $('#chkShowRate').prop('checked', false);
           $('#chkItemNameEdit').prop('checked', false);
           $('#chkTaxRetail').prop('checked', false);
           $('#chkTaxVAT').prop('checked', false);
           $('#chkTaxCST').prop('checked', false);

           $('#chkBarcode').prop('checked', false);
           $('#chkHoldSystem').prop('checked', false);
           $('#chkTenderWindow').prop('checked', false);
           $('#chkBox').prop('checked', false);
           $('#chkKot').prop('checked', false);
           $('#chkShortName').prop('checked', false);
           $('#chkHeaderDuplicate').prop('checked', false);
       
           
         

       }


       function InsertUpdate() {


           var BranchId = 0;
           var BankId = 0;
           var PaymodeId = "";       
           var objSettings = {};
           var CashCustomer = false;
           if ($('#chkCashCustomer').is(":checked")) {
               CashCustomer = true;
               
           }

           var StartCustomer = false;
           if ($('#chkStrtCustomer').is(":checked")) {
               StartCustomer = true;

           }
           BankId = $("#ddlBank").val();
           var BankName = $("#ddlBank option:selected").text();
           
           PaymodeId = $("#ddlPaymode").val();
           var Paymodename = $("#ddlPaymode option:selected").text();


           BranchId = $("#ddlBranch").val();

           var RoundAmt = false;
           var Showrate = false;
           var ItemName = false;
           var Taxretail = false;
           var TaxVat = false;
           var TaxCst = false;
           var Shortname = false;
           var HeaderDuplicate = false;

           var Barcode = false;
           var HoldSystem = false;
           var TenderWindow = false;
           var BoxSystem = false;
           var KotSystem = false;
           var FocAffect = false;
           var NegativeStock = false;
           var Stock = "";
           var KitStock = "";

           if ($('#chkRoundAmt').is(":checked")) {
               RoundAmt = true;

           }
           if ($('#chkShowRate').is(":checked")) {
               Showrate = true;

           }
           if ($('#chkItemNameEdit').is(":checked")) {
               ItemName = true;

           }
           if ($('#chkTaxRetail').is(":checked")) {
               Taxretail = true;

           }
           if ($('#chkTaxVAT').is(":checked")) {
               TaxVat = true;

           }
           if ($('#chkTaxCST').is(":checked")) {
               TaxCst = true;

           }
           if ($('#chkShortName').is(":checked")) {
               Shortname = true;

           }
           if ($('#chkHeaderDuplicate').is(":checked")) {
               HeaderDuplicate = true;

           }


           if ($('#chkBarcode').is(":checked")) {
               Barcode = true;

           }
           if ($('#chkHoldSystem').is(":checked")) {
               HoldSystem = true;

           }
           if ($('#chkTenderWindow').is(":checked")) {
               TenderWindow = true;

           }
           if ($('#chkBox').is(":checked")) {
               BoxSystem = true;

           }
           if ($('#chkKot').is(":checked")) {
               KotSystem = true;

           }
           if ($('#chkFOC').is(":checked")) {
               FocAffect = true;

           }
           if ($('#chkNegative').is(":checked")) {
               NegativeStock = true;

           }


           if (BranchId == "0") {
               alert("Choose Branch");
               $("#ddlBranch").focus();
               return;
           }

           Stock = $("#ddlStock").val();
           KitStock = $("#ddlKit").val();

           objSettings.CashCustomer = CashCustomer;
           objSettings.StartCustomerPoint = StartCustomer;
           objSettings.defaultpaymodeID = PaymodeId;
           objSettings.defaultpaymode = Paymodename;
           objSettings.defaultBankID = BankId;
           objSettings.defaultbankName = BankName;
           objSettings.roundamt = RoundAmt;
           objSettings.showrate = Showrate;
           objSettings.itemname = ItemName;
           objSettings.Tax2onRetail = Taxretail;
           objSettings.Tax2onVAT = TaxVat;
           objSettings.Tax2onCST = TaxCst;
           objSettings.shortname = Shortname;
           objSettings.headerduplicate = HeaderDuplicate;
           objSettings.barcode = Barcode;
           objSettings.holdsys = HoldSystem;
           objSettings.Tenderwindow = TenderWindow;
           objSettings.BoxSystem = BoxSystem;
           objSettings.KotSystem = KotSystem;
           objSettings.focaffect = FocAffect;
           objSettings.NegtiveStock = NegativeStock;
           objSettings.stock = Stock;
           objSettings.KitStock = KitStock;
           objSettings.BranchId = BranchId;

          
          DTO = { 'objSettings': objSettings };


          $.uiLock('');

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebillbasicsettings.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                     
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {

        
        $("#ddlBranch").change(function () {


            var Type = $("#ddlBranch").val();

            $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" }',
                url: "managebillbasicsettings.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    var CashCustomer = obj.setttingData.CashCustomer;
                    if (CashCustomer == true) {
                        $('#chkCashCustomer').prop('checked', true);
                    }
                    else {
                        $('#chkCashCustomer').prop('checked', false);
                    }

                    var StartCustomerPoint = obj.setttingData.StartCustomerPoint;
                    if (StartCustomerPoint == true) {
                        $('#chkStrtCustomer').prop('checked', true);
                    }
                    else {
                        $('#chkStrtCustomer').prop('checked', false);
                    }

                    var Bank = obj.setttingData.defaultBankID;
                    $("#ddlBank option[value='" + Bank + "']").prop("selected", true);

                    var Paymode = obj.setttingData.defaultpaymodeID;
                    $("#ddlPaymode option[value='" + Paymode + "']").prop("selected", true);

                    var roundamt = obj.setttingData.roundamt;
                    if (roundamt == true) {
                        $('#chkRoundAmt').prop('checked', true);
                    }
                    else {
                        $('#chkRoundAmt').prop('checked', false);
                    }

                    var showrate = obj.setttingData.showrate;
                    if (showrate == true) {
                        $('#chkShowRate').prop('checked', true);
                    }
                    else {
                        $('#chkShowRate').prop('checked', false);
                    }

                    var ItemName = obj.setttingData.itemname;
                    if (ItemName == true) {
                        $('#chkItemNameEdit').prop('checked', true);
                    }
                    else {
                        $('#chkItemNameEdit').prop('checked', false);
                    }

                    var cst = obj.setttingData.Tax2onCST;
                    if (cst == true) {
                        $('#chkTaxCST').prop('checked', true);
                    }
                    else {
                        $('#chkTaxCST').prop('checked', false);
                    }

                    var retail = obj.setttingData.Tax2onRetail;
                    if (retail == true) {
                        $('#chkTaxRetail').prop('checked', true);
                    }
                    else {
                        $('#chkTaxRetail').prop('checked', false);
                    }

                    var vat = obj.setttingData.Tax2onVAT;
                    if (vat == true) {
                        $('#chkTaxVAT').prop('checked', true);
                    }
                    else {
                        $('#chkTaxVAT').prop('checked', false);
                    }


                    var shortname = obj.setttingData.shortname;
                    if (shortname == true) {
                        $('#chkShortName').prop('checked', true);
                    }
                    else {
                        $('#chkShortName').prop('checked', false);
                    }

                    var header = obj.setttingData.headerduplicate;
                    if (header == true) {
                        $('#chkHeaderDuplicate').prop('checked', true);
                    }
                    else {
                        $('#chkHeaderDuplicate').prop('checked', false);
                    }


                    var foc = obj.setttingData.focaffect;
                    if (foc == true) {
                        $('#chkFOC').prop('checked', true);
                    }
                    else {
                        $('#chkFOC').prop('checked', false);
                    }

                    var negativestock = obj.setttingData.NegtiveStock;
                    if (negativestock == true) {
                        $('#chkNegative').prop('checked', true);
                    }
                    else {
                        $('#chkNegative').prop('checked', false);
                    }

                    var stock = obj.setttingData.stock;
                    $("#ddlStock option[value='" + stock + "']").prop("selected", true);

                    var kit = obj.setttingData.KitStock;
                    $("#ddlKit option[value='" + kit + "']").prop("selected", true);

                    var barcode = obj.setttingData.barcode;
                    if (barcode == true) {
                        $('#chkBarcode').prop('checked', true);
                    }
                    else {
                        $('#chkBarcode').prop('checked', false);
                    }


                    var holdsystem = obj.setttingData.holdsys;
                    if (holdsystem == true) {
                        $('#chkHoldSystem').prop('checked', true);
                    }
                    else {
                        $('#chkHoldSystem').prop('checked', false);
                    }

                    var tenderwindow = obj.setttingData.Tenderwindow;
                    if (tenderwindow == true) {
                        $('#chkTenderWindow').prop('checked', true);
                    }
                    else {
                        $('#chkTenderWindow').prop('checked', false);
                    }


                    var box = obj.setttingData.BoxSystem;
                    if (box == true) {
                        $('#chkBox').prop('checked', true);
                    }
                    else {
                        $('#chkBox').prop('checked', false);
                    }

                    var kot = obj.setttingData.KotSystem;
                    if (kot == true) {
                        $('#chkKot').prop('checked', true);
                    }
                    else {
                        $('#chkKot').prop('checked', false);
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }

            });


        });


        $("#btnAdd").click(
        function () {

            InsertUpdate();
        }
        );

      
    });


   </script>
</asp:Content>

