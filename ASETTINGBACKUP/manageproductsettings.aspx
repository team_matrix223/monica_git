﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageproductsettings.aspx.cs" Inherits="ApplicationSettings_manageproductsettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    


    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 >Setting</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed"  >
                             <tr>
                                 <td>
                                     <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">

  <tr>
                                              <td class="tableheadings" align="right" style="text-align:left;width:20px" colspan="100%">Branch:</td>
                                              </tr>
                                         <tr>
                                              <td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>


                                           <tr><td colspan ="100%"  class="tableheadings" >PRODUCT ENTRY SETTINGS</td></tr>
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Auto Code Generation:</td><td align="left" style="text-align:left;width:150px"><input type="checkbox" id="chkPRAutoCode"  data-index="2"  name="chkPRAutoCode" /></td></tr>                                         
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">ItemCode Length:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id="txtPRCodeLen" data-index="2"  name="txtPRCodeLen" /></td></tr> 
  
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">ItemCode Start From:</td><td align="left" style="text-align:left;width:150px"><input type="text" id="txtPRCodeStart"  data-index="2"  name="txtPRCodeStart" /></td></tr>                                         
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Allow(Alphabets) Character In Code:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkPRAllowChar" data-index="2"  name="chkPRAllowChar" /></td></tr>               
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Company Wise Discount On Item:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkPRCompDis" data-index="2"  name="chkPRCompDis" /></td></tr>               
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Product BatchNo & Expiry:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkPRBatchNo" data-index="2"  name="chkPRBatchNo" /></td></tr> 
                        <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%">Check ItemName Duplicate:</td></tr> 
                         <tr><td align="left" style="text-align:left;width:200px"><input type="radio"  checked="checked"  id="rdoPRItemName" name="rdoPRItem" />
                         <label class="headings" for="rdoPRItemName">ItemName</label></td>   </tr>      

                        <tr><td align="left" style="text-align:left;width:200px"><input type="radio"  checked="checked"  id="rdoItemCompny" name="rdoPRItem" />
                         <label class="headings" for="rdoItemCompny">ItemName  & Company</label></td>   </tr>    
                                          </table>
                              
                             </tr>
                   
                           
                                
                                            <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>
                                                Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>

    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">


       function InsertUpdate() {
           var BranchId = 0;
           var objSettings = {};
           var AutoGen = false;
           var ItemCodeLength = 0;
           var ItemCodeStart = 0;
           var AlphabetInCode = false;
           var CompnyWiseDis = false;
           var BatchNo = false;
           var ItemDup = false;
           var ItemCompDup = false;

         
           if ($('#chkPRAutoCode').is(":checked")) {
               AutoGen = true;
               
           }

           ItemCodeLength = $("#txtPRCodeLen").val();
           ItemCodeStart = $("#txtPRCodeStart").val();

           if ($('#chkPRAllowChar').is(":checked")) {
               AlphabetInCode = true;

           }
           if ($('#chkPRCompDis').is(":checked")) {
               CompnyWiseDis = true;

           }
           if ($('#chkPRBatchNo').is(":checked")) {
               BatchNo = true;

           }

         
           if ($('#rdoPRItemName').prop('checked') == true) {
               ItemDup = true
           }
          
           if ($('#rdoItemCompny').prop('checked') == true) {
               ItemCompDup = true
           }
           BranchId = $("#ddlBranch").val();
           if (BranchId == "0")
           {
               alert("Choose Branch");
           }

           objSettings.Auto_GenCode = AutoGen;
           objSettings.Item_CodeLen = ItemCodeLength;
           objSettings.Item_CodeStart = ItemCodeStart;
           objSettings.Alphabet_Code = AlphabetInCode;
           objSettings.Comp_WiseDis = CompnyWiseDis;
           objSettings.Batch_No = BatchNo;
           objSettings.ItemDup = ItemDup;
           objSettings.ItemCompDup = ItemCompDup;
           objSettings.BranchId = BranchId;

           var DTO = { 'objSettings': objSettings };



           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "manageproductsettings.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {
        $("#ddlBranch").change(function () {

            var Type = $("#ddlBranch").val();
            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" }',
                url: "manageproductsettings.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    var autogen = obj.setttingData.Auto_GenCode;
                    if (autogen == true) {

                        $('#chkPRAutoCode').prop('checked', true);
                    }
                    else {
                        $('#chkPRAutoCode').prop('checked', false);

                    }
                    $("#txtPRCodeLen").val(obj.setttingData.Item_CodeLen);
                    $("#txtPRCodeStart").val(obj.setttingData.Item_CodeStart);

                    var Alphacode = obj.setttingData.Alphabet_Code;
                    if (Alphacode == true) {
                        $('#chkPRAllowChar').prop('checked', true);
                    }
                    else {
                        $('#chkPRAllowChar').prop('checked', false);
                    }

                    var CmpnyWise = obj.setttingData.Comp_WiseDis;
                    if (CmpnyWise == true) {
                        $('#chkPRCompDis').prop('checked', true);
                    }
                    else {
                        $('#chkPRCompDis').prop('checked', false);
                    }

                    var Batchno = obj.setttingData.Batch_No;
                    if (Batchno == true) {
                        $('#chkPRBatchNo').prop('checked', true);
                    }
                    else {
                        $('#chkPRBatchNo').prop('checked', false);
                    }

                    var ItemDup = obj.setttingData.ItemDup;
                    if (ItemDup == true) {
                        $('#rdoPRItemName').prop('checked', true);
                    }
                    else {
                        $('#rdoPRItemName').prop('checked', false);
                    }


                    var ItemCompDup = obj.setttingData.ItemCompDup;
                    if (ItemCompDup == true) {
                        $('#rdoItemCompny').prop('checked', true);
                    }
                    else {
                        $('#rdoItemCompny').prop('checked', false);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();

                }

            });


        });


        $("#btnAdd").click(
        function () {

            InsertUpdate();
        }
        );

      
    });


   </script>
</asp:Content>

