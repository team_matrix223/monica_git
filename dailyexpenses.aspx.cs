﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dailyexpenses : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            hdnDate.Value = DateTime.Now.ToShortDateString();
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }

    }




    [WebMethod]
    public static string GetById()
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        List<expenses> ExpenseDetails = new ExpenseBLL().GetById(Branch);
        var JsonData = new
        {

            ExpenseDetail = ExpenseDetails
        };
        return ser.Serialize(JsonData);


    }



    [WebMethod]
    public static string GetByDate(DateTime Date)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        List<expenses> ExpenseDetails = new ExpenseBLL().GetByDate(Branch,Date);
        var JsonData = new
        {

            ExpenseDetail = ExpenseDetails
        };
        return ser.Serialize(JsonData);


    }


    [WebMethod]
    public static string InsertUpdate(expenses[] objExpenseDetail,DateTime Date)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        expenses objExpense = new expenses();
        objExpense.Userno = Id;
        objExpense.BranchId = Branch;
        //objExpense.Date = Date;
       

        DataTable dt = new DataTable();
        dt.Columns.Add("Description");
        dt.Columns.Add("Amount");
        DataRow dr;
        foreach (var item in objExpenseDetail)
        {
            dr = dt.NewRow();
            dr["Description"] = item.Description;
            dr["Amount"] = item.Amount;
            dt.Rows.Add(dr);

        }

        DateTime date = Date;
        int status = new ExpenseBLL().InsertUpdate(objExpense, dt, date);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}