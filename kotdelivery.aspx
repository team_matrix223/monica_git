﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="kotdelivery.aspx.cs" Inherits="kotdelivery" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <script src="Scripts/jquery-2.0.3.min.js"></script>
    <script src="Scripts/knockout-3.0.0.js"></script>
    <script src="js/jquery.uilock.js" type="text/javascript"></script>
<link href="css3/bootstrap.css" rel="stylesheet" />
<link href="css3/KOTStylesheet.css" rel="stylesheet" />
    <script src="ViewModel/KotDeliveryViewModel.js" type="text/javascript"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Index</title>
 


<style type="text/css">
       .SelectedRow
    {
    background:#FDF0DF;    
    }

#table tr
{
	border-bottom:dashed 1px silver;
	 
}

#table tr td
{
	padding:5px;
	font-weight:bold;
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	color:black;
}
	
</style>


</head>



<body>





<%--
<!-- ko foreach: getOrders(1) -->
<div data-bind="foreach:UniqueBill">

 
</div>
<!-- /ko -->
--%>
<div class="container-fluid">
<div class="background">

<div class="row">
<div class="col-lg-12">
<div class="butt">
<div class="btn btn-danger" style="cursor:pointer" data-bind="click: $root.RefreshList"  >REFRESH</div>
 
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">

 

 

<div data-bind="foreach:uniqueCountries"  >


<div class="col-md-6" style="padding:0px">
<div class="menubg" style="">
<div  class="bgh" style="max-height:300px;min-height:300px;overflow-y:scroll">

<span data-bind="text:BillNowPrefix" style="font-size:25px"></span> - Table No 
 

<%--<div data-bind="click:$root.ChakLo">Get Table Number</div>--%>
 <select  style="width:width:100px;border:solid 1px silver;height:25px;width:100px" data-bind="options: $root.Tables, value: TableNo"></select>
<br />

<table width="100%" id="table" >
<tbody data-bind="foreach: $root.Products.index.BillNowPrefix()[BillNowPrefix()]">


<tr data-bind="css: { 'SelectedRow': IsDelivered}">
 
<td data-bind="text: Item_Name"  style="font-size:20px;width:420px"> </td>
 
 
<td>
 
<div class="btn" style="cursor:pointer;font-size:20px;width:160px;height:40px" data-bind="click: $root.MarkDelivered,css:{'btn-warning':IsDelivered,'btn-danger': IsNewOrStarted,'btn-success': IsNewDelivery} " >
<div style="width:30px;float:left">
<img data-bind="attr:{src: ImagePath}" style="height:30px;"> 

</div>
 
<div data-bind="text:DeliveryButtonText" style="width:100px;float:left;text-align:left;padding-left:2px;"></div>
 

</tr>
</table>

 
 


</div>

</td>
</tr>

 
 </tbody>


</table>
</div>

</div>

</div>
 

 

</div>


 
<br>
<br>

</div>
</div>

</div>


</div>


</body>
</html>

