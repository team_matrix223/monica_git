﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class managetaxes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!IsPostBack)
        {
             //BindData();
        }
    }


   
 


    [WebMethod]
    public static string Insert(int TaxId, decimal TaxRate, string Description, string DrAcc, string CrAcc, bool DrVat, bool CrVat, string DrVatAcc, string CrVatAcc, bool ChkSur, decimal SurVal, string DrSur, string CrSur, bool IsActive)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Session[Constants.AdminId].ToString());
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_ID = TaxId,
            Tax_Rate = TaxRate,
            Description = Description.Trim().ToUpper(),
            Dr_AccCode = DrAcc,
            Cr_AccCode = CrAcc,
            VatCode_Dr = DrVat,
            Dr_VatCode = DrVatAcc,
            VatCode_Cr = CrVat,
            Cr_VatCode = CrVatAcc,
            ChkSur = ChkSur,
            SurValue = SurVal,
            Dr_Sur = DrSur,
            Cr_Sur = CrSur,
            UserId = Id,
            IsActive = IsActive,

        };
        int status = new TaxStructureBLL().InsertUpdate(objTaxStructure);
        var JsonData = new
        {
            Tax = objTaxStructure,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(Int32 TaxId)
    {
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_ID = TaxId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new TaxStructureBLL().DeleteTax(objTaxStructure);
        var JsonData = new
        {
            Tax = objTaxStructure,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}