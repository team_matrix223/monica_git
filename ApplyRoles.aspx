﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ApplyRoles.aspx.cs" Inherits="ApplyRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
   <script src="chosen.jquery.js" type="text/javascript"></script>
      <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="chosen.css">
    <script src="js/jquery.uilock.js" type="text/javascript"></script>
  <script language="javascript">

      doChosen();
      function doChosen() {

          var config = {
              '.chosen-select': {},
              '.chosen-select-deselect': { allow_single_deselect: true },
              '.chosen-select-no-single': { disable_search_threshold: 10 },
              '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
              '.chosen-select-width': { width: "95%" }
          }
          for (var selector in config) {
              $(selector).chosen(config[selector]);
          }

      }

      function Reset() {

          $("#<%=ddlDepartment.ClientID %>").val(0);
          $("#ddlRoles  option").attr("selected", false);
          $("#ddlRoles").trigger("chosen:updated");
      }


      function InsertUpdate() {


        
          var Designation = $("#<%=ddlDepartment.ClientID %>").val();
         
          if (Designation == "0") {
              alert("Choose Designation First");
              $("#<%=ddlDepartment.ClientID %>").focus();
              return;
          }
          var Role = $("#ddlRoles").val();
          if (Role == null) {
            
              alert("No Role is choosen to apply.Atleast One Role should be selected.");
              $("#ddlRoles").focus();
              return;
          }
     

          $.uiLock('');
       
          $.ajax({
              type: "POST",
              data: '{"DesignationId": "' + Designation + '","Roles": "' + Role + '"}',

              url: "ApplyRoles.aspx/Insert",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {


                  var obj = jQuery.parseJSON(msg.d);
                  if (obj.Status == "0") {
                      alert("Updation Failed. Please try again later.");
                      return;
                  }
                  else {
                      alert("Roles Applied Successfully.");
                      Reset()
                      return;
                  }


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

                  $.uiUnlock();
              }

          });


      }

      $(document).ready(

     function () {



         $("#btnSave").click(
        function () {



            InsertUpdate();


        }
        );


         $("#<%=ddlDepartment.ClientID %>").change(
         function () {

             var Designation = "0";
             Designation = $("#<%=ddlDepartment.ClientID %>").val();

             if ($(this).val() == "0") {

                 $("#ddlRoles  option").attr("selected", false);
                 $("#ddlRoles").trigger("chosen:updated");
                 return;
             }

             $.uiLock('');
             $.ajax({
                 type: "POST",
                 data: '{"DesignationID": "' + Designation + '"}',

                 url: "ApplyRoles.aspx/GetRoles",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {


                     var obj = jQuery.parseJSON(msg.d);


                     $("#ddlRoles  option").attr("selected", false);


                     var x = obj.DesgRoles.Role;

                     var parts = x.split(',');
                    
                     if (x == "") {
                         $.uiUnlock();
                         return;
                     }
                     else {

                         for (var i = 0; i < parts.length; i++) {

                             $('#ddlRoles option[value=' + parts[i] + ']').prop('selected', 'selected');
                         }

                     }



                     $("#ddlRoles").trigger("chosen:updated");


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();



                 }

             });
         }

         );

     }

     );

 </script>


 
      
  <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Roles Management</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">


                          <form class="form-horizontal form-label-left"   runat="server" id="formID" method="post">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Designation Roles </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Designation <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                             <asp:DropDownList ID ="ddlDepartment" runat="server" style = "width:200px"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Roles <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                           <select  multiple class="chosen-select" id="ddlRoles" style="width:300px" >
    <option value="1">Menu Card Print</option>
    <option value="2">Delhi</option>
    <option value="3">America</option>
    <option value="4">Sachin</option>
    <option value="5">Orange</option>
    
    </select>
                                       
                                            </div>
                                        </div>
                                       
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                              
                                                <button type="button" id="btnSave" class="btn btn-success">Apply Roles</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
 
                 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            <script type="text/javascript">
                var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': { allow_single_deselect: true },
                    '.chosen-select-no-single': { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width': { width: "95%" }
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
      
  </script>

  

</asp:Content>

