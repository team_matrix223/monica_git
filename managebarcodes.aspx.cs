﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class managebarcodes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnDate.Value = DateTime.Now.ToShortDateString();

        //ddlBranch.DataSource = new BranchBLL().GetAll();
        //ddlBranch.DataValueField = "BranchId";
        //ddlBranch.DataTextField = "BranchName";
        //ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);
    }

    //protected void btnGetNutrition_Click(object sender, EventArgs e)
    //{

    //    using (MemoryStream ms = new MemoryStream())
    //    {
    //        //for (int i = 0; i < 3; i++)
    //        //{
                
           
    //        int itemid = Convert.ToInt32(hdnitemid.Value);
    //        ingSbPrint r = new ingSbPrint(itemid);
    //        r.CreateDocument();
    //        //ingSbPrint r2 = new ingSbPrint(itemid);
    //        //r2.CreateDocument();
    //        //ingSbPrint r3 = new ingSbPrint(itemid);
    //        //r3.CreateDocument();
    //        //ingSbPrint r4 = new ingSbPrint(itemid);
    //        //r4.CreateDocument();
    //        //ingSbPrint r5 = new ingSbPrint(itemid);
    //        //r5.CreateDocument();

    //        //r.Pages.AddRange(r2.Pages);
    //        //r.Pages.AddRange(r3.Pages);
    //        //r.Pages.AddRange(r4.Pages);
    //        //r.Pages.AddRange(r5.Pages);
         
    //        r.PrintingSystem.ContinuousPageNumbering = true;  
    //        PdfExportOptions opts = new PdfExportOptions();
    //        opts.ShowPrintDialogOnOpen = true;
    //        r.ExportToPdf(ms, opts);
    //        ms.Seek(0, SeekOrigin.Begin);
    //        byte[] report = ms.ToArray();
    //        Page.Response.ContentType = "application/pdf";
    //        Page.Response.Clear();
    //        Page.Response.OutputStream.Write(report, 0, report.Length);
    //        //
    //        //}

    //    }

    //}
    [WebMethod]
    public static string BindNutritionDetail(int ItemId)
    {

        ItemMaster ObjItem = new ItemMaster();
        ObjItem.ItemID = ItemId;

        List<NutritionFacts> lst = new ItemMasterBLL().GetProductById(ObjItem);
        JavaScriptSerializer ser = new JavaScriptSerializer();
       
        var JsonData = new
        {
            ItemOptions = ObjItem,
            NutritionFact = lst
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindIemDetail(int ItemId)
    {

        ItemMaster ObjItem = new ItemMaster();
        ObjItem.ItemID = ItemId;

        List<NutritionFacts> lst = new ItemMasterBLL().GetProductById(ObjItem);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        
        var JsonData = new
        {
            ItemOptions = ObjItem,
            NutritionFact = lst
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Bindbranches()
    {

        string BranchData = new BranchBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BranchOptions = BranchData

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetSaleRate(int BranchId,string ItemCode)
    {
        decimal SaleRate = 0;
        decimal MRP = 0;
        SqlDataReader dr = null;
        dr = new BranchDAL().GetSaleRateAll(BranchId, ItemCode);
            if (dr.HasRows)
            {
                dr.Read();
                SaleRate = Convert.ToDecimal(dr["Sale_Rate"].ToString());
                MRP = Convert.ToDecimal(dr["MRP"].ToString());

            }
               
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            saleRate = SaleRate,
            mRP = MRP

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static void insert_whtbarcode(int wht_itemcode, string wht_rate, string wht_weight)
    {
        Connection con = new Connection();
       using(SqlConnection conn=new SqlConnection (con.sqlDataString))
       {
           conn.Open();
           SqlCommand cmd = new SqlCommand("insert into wht_barcode(prefix_wht_id,item_code,rate,weight)values((select top 1 prefix+cast(max(wht_id)+1 as varchar)from wht_barcode group by  prefix,wht_id order by wht_id desc),@wht_itemcode,@wht_rate,@wht_weight)", conn);
           cmd.CommandType = CommandType.Text;
           cmd.Parameters.AddWithValue("@wht_itemcode", wht_itemcode);
           cmd.Parameters.AddWithValue("@wht_rate", wht_rate);
           cmd.Parameters.AddWithValue("@wht_weight", wht_weight);
           cmd.ExecuteNonQuery();
       }
    }


    public void btnPrint_Click(object sender, EventArgs e)
    {
        
        //int qty = Convert.ToInt32(txtqty.Text);

        //   for (int i = 0; i < qty; i++)
        //   {
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        rptBarcode r = new rptBarcode();
        //        r.CreateDocument();
        //        PdfExportOptions opts = new PdfExportOptions();
        //        opts.ShowPrintDialogOnOpen = true;
        //        r.ExportToPdf(ms, opts);
        //        ms.Seek(0, SeekOrigin.Begin);
        //        byte[] report = ms.ToArray();
        //        Page.Response.ContentType = "application/pdf";
        //        Page.Response.Clear();
        //        Page.Response.OutputStream.Write(report, 0, report.Length);
        //        Page.Response.End();
        //    }
        //}


    }  
  
    //protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //      //  string con = ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString;
    //      //  SqlConnection conn = new SqlConnection(con);
    //      ////  string com = "select Sale_Rate from branch_master where branchid=" + ddlBranch.SelectedValue;
    //      //  SqlCommand cmd = new SqlCommand(com, conn);
    //      //  conn.Open();
    //      //  SqlDataReader DR1 = cmd.ExecuteReader();
    //      //  if (DR1.Read())
    //      //  {
    //      //      ddlsalerate.Text = DR1.GetValue(0).ToString();
    //      //  }
    //      //  conn.Close();
       
    //}
}