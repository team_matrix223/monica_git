﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managebasicpurchasesettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGodowns();
            BindBranches();

        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASESETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindGodowns()
    {

        ddlPGodown.DataSource = new GodownsBLL().GetAll();
        ddlPGodown.DataValueField = "Godown_ID";
        ddlPGodown.DataTextField = "Godown_Name";
        ddlPGodown.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account To Debit--";
        li1.Value = "0";
        ddlPGodown.Items.Insert(0, li1);

    }


    void BindBranches()
    {

        ddlPBranch.DataSource = new BranchBLL().GetAll();
        ddlPBranch.DataValueField = "BranchId";
        ddlPBranch.DataTextField = "BranchName";
        ddlPBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlPBranch.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string FillSettings(int Type)
    {
        BasicPurchaseSetting ObjSettings = new BasicPurchaseSetting();

       
        ObjSettings.BranchId = Type;
        new BasicPurchaseSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Insert(BasicPurchaseSetting objSettings)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
       
        objSettings.UserId = Id;
      
        int status = new BasicPurchaseSettingBLL().UpdateBasicSettings(objSettings);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



}