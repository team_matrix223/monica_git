﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Welcome : System.Web.UI.Page
{

    public Int32 UserId { get { return Request.QueryString["Id"] != null ? Convert.ToInt32(Request.QueryString["Id"]) : 0; } }
    public Int32 db { get { return Request.QueryString["db"] != null ? Convert.ToInt32(Request.QueryString["db"]) : 0; } }
    public string Sale_Rate { get { return Request.QueryString["Sale_Rate"] != null ? Convert.ToString(Request.QueryString["Sale_Rate"]) : ""; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cookies[Constants.AdminId].Value = UserId.ToString();
        Response.Cookies[Constants.DataBase].Value = db.ToString();
       
        string SessionId = Session.SessionID;
        User objuser = new User()
        {
            UserNo = (UserId),
            SessionId = SessionId

        };

        Int32 status = new UserBLL().UserLoginCheck(objuser);
        if (status == -1)
        {
            Response.Write("<script>alert('Invalid User');</script>");
          
        }
        if (status == -3)
        
        {
            Response.Write("<script>alert('Please First  Login From Window EXE');</script>");
           
        }
        if (status == -2)
        {
            Response.Write("<script>alert('Already Login on some other machine');</script>");

        }
        if (status > 0)
        {

            User objUser = new User
            {
                UserNo = UserId
            };
            new UserBLL().GetByUserId(objuser);

            Response.Cookies[Constants.AdminId].Value = UserId.ToString();
            Response.Cookies[Constants.DesignationId].Value = objuser.Counter_NO.ToString();
            Response.Cookies[Constants.BranchId].Value = objuser.BranchId.ToString();
            Response.Cookies[Constants.BranchName].Value = objuser.BranchName;
            Response.Cookies[Constants.EmployeeName].Value = objuser.User_ID;
            Response.Cookies[Constants.Sale_Rate].Value = Sale_Rate;
            Response.Redirect("BillScreen.aspx");

        }

    }
}