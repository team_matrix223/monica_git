﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for CustomerDAL
/// </summary>
public class CustomerDAL:Connection
{


    public SqlDataReader GetAllCreditCustomers()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllCreditCustomers", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public DataSet KeywordSearch(string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Keyword", Keyword);

        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CustomerKeywordSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }



   

    public DataSet AdvancedSearch(string searchOn, string searchCriteria, string searchText)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@SearchOn", searchOn);
        objParam[1] = new SqlParameter("@SearchCriteria", searchCriteria);
        objParam[2] = new SqlParameter("@SearchText", searchText);

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "POS_sp_customerAdvancedSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;
    }


    public DataSet AdvancedSearchCreditCustomers(string searchOn, string searchCriteria, string searchText,string Type)
    {
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@SearchOn", searchOn);
        objParam[1] = new SqlParameter("@SearchCriteria", searchCriteria);
        objParam[2] = new SqlParameter("@SearchText", searchText);
        objParam[3] = new SqlParameter("@Type", Type);
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "POS_sp_AdvancedSearchCreditCustomers", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;
    }


    public SqlDataReader GetByMobileNo(string MobileNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@MobileNo", MobileNo);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GeyByMobileNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }



    public SqlDataReader GetById(Customers objCustomer)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Customer_ID", objCustomer.Customer_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CashCustomersGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_CashCustomersGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetAll()
    {
        List<Customers> CustomerList = new List<Customers>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CashCustomersGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Customers objCustomer)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[21];

        objParam[0] = new SqlParameter("@Customer_ID", objCustomer.Customer_ID);
        objParam[1] = new SqlParameter("@Prefix", objCustomer.Prefix);
        objParam[2] = new SqlParameter("@Customer_Name", objCustomer.Customer_Name);
        objParam[3] = new SqlParameter("@Address_1", objCustomer.Address_1);
        objParam[4] = new SqlParameter("@Address_2", objCustomer.Address_2);
        objParam[5] = new SqlParameter("@Area_ID", objCustomer.Area_ID);
        objParam[6] = new SqlParameter("@City_ID", objCustomer.City_ID);
        objParam[7] = new SqlParameter("@State_ID", objCustomer.State_ID);
        objParam[8] = new SqlParameter("@Date_Of_Birth", objCustomer.Date_Of_Birth);
        objParam[9] = new SqlParameter("@Date_Anniversary", objCustomer.Date_Anniversary);
        objParam[10] = new SqlParameter("@Discount", objCustomer.Discount);
        objParam[11] = new SqlParameter("@Contact_No", objCustomer.Contact_No);
        objParam[12] = new SqlParameter("@Tag", objCustomer.Tag);
        objParam[13] = new SqlParameter("@FocBill", objCustomer.FocBill);
        objParam[14] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[14].Direction = ParameterDirection.ReturnValue;
        objParam[15] = new SqlParameter("@UserId", objCustomer.UserId);
        objParam[16] = new SqlParameter("@IsActive", objCustomer.IsActive);
        objParam[17] = new SqlParameter("@grpid", objCustomer.grpid);
        objParam[18] = new SqlParameter("@BranchId", objCustomer.BranchId);
        objParam[19] = new SqlParameter("@EmailId", objCustomer.EmailId);
        objParam[20] = new SqlParameter("@GSTNo", objCustomer.GSTNo);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CashCustomerInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[14].Value);
            objCustomer.Customer_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int16 Insert(Customers objCustomer)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[16];

        objParam[0] = new SqlParameter("@Customer_ID", objCustomer.Customer_ID);
        objParam[1] = new SqlParameter("@Prefix", objCustomer.Prefix);
        objParam[2] = new SqlParameter("@Customer_Name", objCustomer.Customer_Name);
        objParam[3] = new SqlParameter("@Address_1", objCustomer.Address_1);
        objParam[4] = new SqlParameter("@Address_2", objCustomer.Address_2);
        objParam[5] = new SqlParameter("@Area_ID", objCustomer.Area_ID);
        objParam[6] = new SqlParameter("@City_ID", objCustomer.City_ID);
        objParam[7] = new SqlParameter("@State_ID", objCustomer.State_ID);
        objParam[8] = new SqlParameter("@Date_Of_Birth", objCustomer.Date_Of_Birth);
        objParam[9] = new SqlParameter("@Date_Anniversary", objCustomer.Date_Anniversary);
        objParam[10] = new SqlParameter("@Discount", objCustomer.Discount);
        objParam[11] = new SqlParameter("@Contact_No", objCustomer.Contact_No);
      
        objParam[12] = new SqlParameter("@FocBill", objCustomer.FocBill);
        objParam[13] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[13].Direction = ParameterDirection.ReturnValue;
        objParam[14] = new SqlParameter("@UserId", objCustomer.UserId);
        objParam[15] = new SqlParameter("@EmailId", objCustomer.EmailId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CustomerInsert", objParam);
            retValue = Convert.ToInt16(objParam[13].Value);
            objCustomer.Customer_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Customers objCustomer)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Customer_ID", objCustomer.Customer_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CashCustomersDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objCustomer.Customer_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public DataSet AdvancedSearchFOC(string searchOn, string searchCriteria, string searchText)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@SearchOn", searchOn);
        objParam[1] = new SqlParameter("@SearchCriteria", searchCriteria);
        objParam[2] = new SqlParameter("@SearchText", searchText);

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "POS_sp_customerAdvancedSearchFOC", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;
    }

    public SqlDataReader GetByMobileNoFOC(string MobileNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@MobileNo", MobileNo);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GeyByMobileNoFOC", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }

}