﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managepropsgroups.aspx.cs" Inherits="managepropsgroups" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
   <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

<script language="javascript" type="text/javascript">


    function ApplyRoles(Roles) {


        $("#<%=hdnRoles.ClientID%>").val(Roles);
      }
    var m_SGroupId = -1;
    var Group = 0;
    function ResetControls() {
        m_SGroupId = -1;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        $("#<%=hdnUpdate.ClientID %>").val("0");
        var arrRole = [];
        arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


       for (var i = 0; i < arrRole.length; i++) {

           if (arrRole[i] == "1") {

               $("#btnAdd").css({ "display": "block" });
           }

       }
     

        btnUpdate.css({ "display": "none" });
   
        $("#chkIsActive").prop("checked", "checked");
        $("#chkShow").prop("checked", "checked");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_SGroupId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }

        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }
        var Show = false;

        if ($('#chkShow').is(":checked")) {
            Show = true;
        }
        var department = $("#<%=ddlDepartment.ClientID%>").val();
        var group = $("#<%=ddlGroup.ClientID%>").val();

        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{"SGroupId":"' + Id + '", "SGroupName": "' + Title + '","ShowInMenu": "' + Show + '","IsActive": "' + IsActive + '","DepartmentId": "' + department + '","GroupId": "' + group + '"}',
            url: "Managepropsgroups.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                if (obj.Status == -11) {
                    alert("You don't have permission to perform this action..Consult Admin Department.");
                    return;
                }



                if (obj.Status == -1) {

                    alert("Insertion Failed.SGroup with duplicate name already exists.");
                    return;
                }

                if (Id == "-1") {
                    ResetControls();
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.SubGroups.SGroup_ID, obj.SubGroups, "last");
                    alert("SGroup is added successfully.");
                }
                else {
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.SubGroups);
                    alert("SGroup is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {

        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").show();
                    $("#btnAdd").click(
                    function () {


                        m_SGroupId = -1;
                        InsertUpdate();
                    }
                    );
                }

                else if (arrRole[i] == "3") {
                    $("#btnUpdate").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No SGroup is selected to Edit");
               return;
           }

           InsertUpdate();
       }
       );
                }
                else if (arrRole[i] == "2") {

                    $("#btnDelete").show();
                    $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No SGroup is selected to Delete");
             return;
         }

         var SGroupId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'SGroup_ID')
         if (confirm("Are You sure to delete this record")) {
             $.uiLock('');




             $.ajax({
                 type: "POST",
                 data: '{"SGroupId":"' + SGroupId + '"}',
                 url: "managepropsgroups.aspx/Delete",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.status == -10) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }

                     if (obj.status == -1) {
                         alert("Deletion Failed.Sub Group is in Use.");
                         return
                     }

                     BindGrid();
                     alert("SGroup is Deleted successfully.");




                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     $.uiUnlock();
                 }
             });









         }


     }
     );

                }

            }

        }
        $("#<%=ddlGroup.ClientID%>").change(function () {

            if ($(this).val() == "0") {

                jQuery("#jQGridDemo").GridUnload();
                Group = 0;
                return;
            }


            if ($("#<%=hdnUpdate.ClientID %>").val() != "1") {

                Group = $("#<%=ddlGroup.ClientID %>").val();

                BindGrid(Group);
            }
        });





        $("#<%=ddlDepartment.ClientID %>").change(
            function () {


              
                if ($("#<%=hdnUpdate.ClientID %>").val() != "1") {
                    $("#txtTitle").val("");
                }


                    $("#<%=ddlGroup.ClientID %>").html('');

                    var DId = $("#<%=ddlDepartment.ClientID %>").val();
                    if (DId == "0") {
                        alert("First Choose Department");
                        return;
                    }

                   

                    $.uiLock('');


                    $.ajax({
                        type: "POST",
                        data: '{ "DepartmentId": "' + $(this).val() + '"}',
                        url: "managepropsgroups.aspx/BindGroups",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);

                            if (obj.GroupOptions.length == "0") {
                                alert("NO Group Found against this department");
                                return;
                                $.uiLock('');
                            }
                            else {
                                $("#<%=ddlGroup.ClientID %>").html(obj.GroupOptions);
                            }

                        }, error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function (msg) {
                            $.uiUnlock();

                        }

                    });

                }

            

            );





        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }



        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
   <asp:HiddenField ID="hdnUpdate" runat="server" Value ="0"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>SubGroups</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit SubGroup</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" style="width:300px">
                     
                      <tr><td class="headings">Department:</td><td> <asp:DropDownList CssClass="validate ddlrequired" style="width:300px;height:35px" ID ="ddlDepartment" runat="server"></asp:DropDownList>  </td></tr>
                      <tr><td class="headings">Group:</td><td> <asp:DropDownList CssClass="validate ddlrequired" style="width:300px;height:35px" ID ="ddlGroup" runat="server"></asp:DropDownList>  </td></tr>
                      <tr><td class="headings">Title:</td><td>  <input type="text"  name="txtTitle" class="form-control validate required "  data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                      <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr>

                                            <tr><td class="headings">ShowInMenu:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkShow" checked="checked" data-index="2"  name="chkShow" /></td></tr>
                      
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i> Update</div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" ><i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage SubGroups</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" >
                    
      	          <table id="jQGridDemo">
    </table>

     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete" style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                       <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid(Group) {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageSGroupByGroupId.ashx?Group='+Group,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['SubGroup Id', 'Title','IsActive','ShowInMenu','Department','Group'],
                        colModel: [
                                    { name: 'SGroup_ID', key: true, index: 'Group_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'SGroup_Name', index: 'Group_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                     { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'ShowInMenu', index: 'ShowInMenu', width: 150, editable: true,hidden:true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Department_Id', index: 'Department_Id', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Group_Id', index: 'Group_Id', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'SGroup_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "SubGroups List",

                        editurl: 'handlers/ManagePropSGroup.ashx',



                     ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_SGroupId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");

                    $("#<%=hdnUpdate.ClientID %>").val("1");

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                     $("#btnUpdate").css({ "display": "none" });
                     $("#btnReset").css({ "display": "none" });
                     $("#btnAdd").css({ "display": "none" });
                  

                     for (var i = 0; i < arrRole.length; i++) {

                         if (arrRole[i] == 1) {

                             $("#btnAdd").css({ "display": "block" });
                         }

                         if (arrRole[i] == 3) {

                             m_SGroupId = $('#jQGridDemo').jqGrid('getCell', rowid, 'SGroup_ID');


                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'ShowInMenu') == "true") {
                                 $('#chkShow').prop('checked', true);
                             }
                             else {
                                 $('#chkShow').prop('checked', false);

                             }

                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                 $('#chkIsActive').prop('checked', true);
                             }
                             else {
                                 $('#chkIsActive').prop('checked', false);

                             }

                             txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'SGroup_Name'));
                             var Department = $('#jQGridDemo').jqGrid('getCell', rowid, 'Department_Id');


                             $("#<%=ddlDepartment.ClientID%> option[value='" + Department + "']").prop("selected", true);

                             var group = $('#jQGridDemo').jqGrid('getCell', rowid, 'Group_Id');

                             $("#<%=ddlGroup.ClientID%> option[value='" + group + "']").prop("selected", true);
                             txtTitle.focus();
                             $("#btnAdd").css({ "display": "none" });
                             $("#btnUpdate").css({ "display": "block" });
                             $("#btnReset").css({ "display": "block" });

                         }

                     }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>
