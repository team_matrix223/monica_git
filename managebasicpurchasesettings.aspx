﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebasicpurchasesettings.aspx.cs" Inherits="ApplicationSettings_managebasicpurchasesettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

  <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>

    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 >Settings</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed"  >
                             <tr>
                                 <td>
                                     <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">
                                           <tr><td colspan ="100%"  class="tableheadings" >PURCHASE SETTINGS</td></tr>


                                          <tr>
                                              <td class="headings" align="right" style="text-align:left;width:20px">Branch:</td>
                                              <td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlPBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Check Of MRP>SaleRate>PurchaseRate:</td><td align="left" style="text-align:left;width:150px"><input type="checkbox" id="chkPMRP"  data-index="2"  name="chkPMRP" /></td></tr>                                         
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Update Sale Rate From Purchase:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chPUpdSaleRate" data-index="2"  name="chPUpdSaleRate" /></td></tr> 
  
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Update MRP From Purchase:</td><td align="left" style="text-align:left;width:150px"><input type="checkbox" id="chkPUpdMRP"  data-index="2"  name="chkPUpdMRP" /></td></tr>                                         
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Show Purchase Margin After Save:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkPShowMargin" data-index="2"  name="chkPShowMargin" /></td></tr>               
                        <tr><td class="headings" align="left" style="text-align:left;width:300px">Show Sale Only Items In Purchase Order:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkPSaleOnlyItems" data-index="2"  name="chkPSaleOnlyItems" /></td></tr>               
                        <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%">Purchase Order On The Basis Of ReOrder Level Of Godown:</td></tr>
                                         <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><select id="ddlPReorderLevel" style="width:200px" >
                                    <option value="Main">Main</option>
                                    <option value="Consolidated">Consolidated</option>
                                   
                                    </select></td></tr>               
                        <tr><td class="headings" align="left" style="text-align:left;width:200px" colspan="100%">Main Godown:</td></tr>
                                         <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlPGodown" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>      

                                          <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%">ReOrder In Case Of Purchase Order On The Basis Of Sale:</td></tr>
                                         <tr><td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><select id="ddlPReorderCal" style="width:200px" >
                                    <option value="ReOrderLevel">Reorder Level</option>
                                    <option value="Stock Rq.-Act Stk.">Stock Rq.-Act Stk.</option>
                                   
                                    </select></td></tr>      
                                     </table>
                              
                             </tr>
                   
                           
                                
                                            <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>

    
     <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">



       function ResetControls() {
           $("#ddlPGodown").val("0");
           $("#ddlPReorderLevel").val("0");
           $("#ddlPReorderCal").val("0");
           $('#chPUpdSaleRate').prop('checked', false);
           $('#chkPMRP').prop('checked', false);
           $('#chkPUpdMRP').prop('checked', false);
           $('#chkPShowMargin').prop('checked', false);
           $('#chkPSaleOnlyItems').prop('checked', false);
           
       }

       function InsertUpdate() {

           var GodownId = 0;
           var Reorder = "";
           var ReorderCal = "";
           var BranchId = 0;
           var BranchName = "";
           var objSettings = {};
           
           GodownId = $("#ddlPGodown").val();
           Reorder = $("#ddlPReorderLevel").val();
           ReorderCal = $("#ddlPReorderCal").val();

           BranchId = $("#ddlPBranch").val();
          
           var CheckMRP = false;
           var UpdSaleRate = false;
           var UpdMRP = false;
           var ShowMargin = false;
           var ShowSaleItem = false;
          
           if ($('#chkPMRP').is(":checked")) {
               CheckMRP = true;

           }
           if ($('#chPUpdSaleRate').is(":checked")) {
               UpdSaleRate = true;

           }
           if ($('#chPUpdMRP').is(":checked")) {
               UpdMRP = true;

           }
           if ($('#chkPShowMargin').is(":checked")) {
               ShowMargin = true;

           }
           if ($('#chkPSaleOnlyItems').is(":checked")) {
               ShowSaleItem = true;

           }
           
           objSettings.BranchId = BranchId;
           objSettings.BranchName = BranchName;
           objSettings.Check_MRP = CheckMRP;
           objSettings.Update_SaleRatePur = UpdSaleRate;
           objSettings.Update_MRPPur = UpdMRP;
           objSettings.Show_MarginPur = ShowMargin;
           objSettings.Show_SaleItemInPurOrder = ShowSaleItem;
           objSettings.ReOrderLevel = Reorder;
           objSettings.ReOrderLevelCal = ReorderCal;
           objSettings.Defaultgodown = GodownId;
         
           var DTO = { 'objSettings': objSettings };

           $.uiLock('');

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebasicpurchasesettings.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");

                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {



        $("#ddlPBranch").change(function () {
           

           var Type = $("#ddlPBranch").val();
           $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" }',
                url: "managebasicpurchasesettings.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    var checkMRP = obj.setttingData.Check_MRP;
                    if (checkMRP == true) {
                        $('#chkPMRP').prop('checked', true);
                    }
                    else {
                        $('#chkPMRP').prop('checked', false);
                    }

                    var UpdateSalerate = obj.setttingData.Update_SaleRatePur;
                    if (UpdateSalerate == true) {
                        $('#chPUpdSaleRate').prop('checked', true);
                    }
                    else {
                        $('#chPUpdSaleRate').prop('checked', false);
                    }



                    var Branch = obj.setttingData.BranchId;
                    $("#ddlPBranch option[value='" + Branch + "']").prop("selected", true);


                    var Godown = obj.setttingData.Defaultgodown;
                    $("#ddlPGodown option[value='" + Godown + "']").prop("selected", true);

                    var Reorder = obj.setttingData.ReOrderLevel;
                    $("#ddlPReorderLevel option[value='" + Reorder + "']").prop("selected", true);


                    var ReorderCal = obj.setttingData.ReOrderLevelCal;
                    $("#ddlPReorderCal option[value='" + ReorderCal + "']").prop("selected", true);


                    var UpdateMRP = obj.setttingData.Update_MRPPur;
                    if (UpdateMRP == true) {
                        $('#chkPUpdMRP').prop('checked', true);
                    }
                    else {
                        $('#chkPUpdMRP').prop('checked', false);
                    }

                    var ShowMargin = obj.setttingData.Show_MarginPur;
                    if (ShowMargin == true) {
                        $('#chkPShowMargin').prop('checked', true);
                    }
                    else {
                        $('#chkPShowMargin').prop('checked', false);
                    }

                    var ShowSaleItem = obj.setttingData.Show_SaleItemInPurOrder;
                    if (ShowSaleItem == true) {
                        $('#chkPSaleOnlyItems').prop('checked', true);
                    }
                    else {
                        $('#chkPSaleOnlyItems').prop('checked', false);
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }

            });






           
        });







       

        $("#btnAdd").click(
        function () {

            InsertUpdate();
        }
        );


    });


   </script>


</asp:Content>

