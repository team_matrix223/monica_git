﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

 

public partial class managekits : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.KITMASTER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    [WebMethod]
    public static string InsertUpdate(KitDetail[] objKitDetails, Kit objKit)
    {

        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.KITMASTER));

        string[] arrRoles = sesRoles.Split(',');


        if (objKit.Kit_ID == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                Status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                Status = -11;
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_ID");
        dt.Columns.Add("Master_Code");
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Item_Name");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax_ID");
        DataRow dr;
            
       

        foreach (var item in objKitDetails)
        {
            dr = dt.NewRow();
            dr["Item_ID"] = item.Item_ID;
            dr["Master_Code"] = objKit.Master_Code;
            dr["Item_Code"] = item.Item_Code;
            dr["Item_Name"] = item.Item_Name;
            dr["Qty"] = item.Qty;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = item.Amount;
            dr["Tax_ID"] = item.Tax_ID;
            dt.Rows.Add(dr);
        
        
        }

        Status =  new KitBLL().InsertUpdate(objKit,dt);
        var JsonData = new
        {
           
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetById(Int32 KitId)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        Kit objKit = new Kit();
        objKit.Kit_ID = KitId;
        List<KitDetail>KitDetail =  new KitBLL().GetKitById(objKit);
        var JsonData = new
        {
            Kit = objKit,
            KitDetail = KitDetail
        };
        return ser.Serialize(JsonData);
    
    
    }

    
    [WebMethod]
    public static string Delete(Int32 KitID)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.KITMASTER));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Kit objKit = new Kit()
        {
            Kit_ID = KitID,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new KitBLL().DeleteKit(objKit);
        var JsonData = new
        {
            kitmaster = objKit,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string FillSettings()
    {
        string MrpEdit;
        string SaleRate;

        new CommonSettingsBLL().GetKitSettings(out MrpEdit,out SaleRate);
        var JsonData = new
        {

          Mrp = MrpEdit,
          Sale=SaleRate,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}