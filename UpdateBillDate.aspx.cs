﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class UpdateBillDate : System.Web.UI.Page
{
    SqlConnection con;    
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(ParamsClass.sqlDataString);
    }
    protected void btnGetDate_Click(object sender, EventArgs e)
    {
        if (txtBillNo.Text != "")
        {
            txtBillDate.Text = "";
            SqlCommand com = new SqlCommand("Select * from Bill_Master where Bill_No = @BillNo");
            com.CommandType = CommandType.Text;
            com.Connection = con;
            com.Parameters.AddWithValue("@BillNo", txtBillNo.Text);
            using (SqlDataAdapter da = new SqlDataAdapter(com))
            {
                DataTable dt = new DataTable();
                dt.Clear();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    txtBillDate.Text = (Convert.ToDateTime(dt.Rows[0]["Bill_Date"]).ToShortDateString()).ToString();
                }
                else
                {
                    Response.Write("<script> alert ('Bill No Doesn't Exists..!!');</script>");
                }
            }
        }
        else
        {
            Response.Write("<script> alert ('Please Enter Bill No..!!');</script>");
        }
    }
    protected void btnUpdateDate_Click(object sender, EventArgs e)
    {
        if (txtBillNo.Text != "")
        {
         
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_BillDateUpdate", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BillDate", txtBillDate.Text);
            cmd.Parameters.AddWithValue("@BillNo", txtBillNo.Text);
            Response.Write("<script> alert ('Updated SuccessFully..!!');</script>");
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            txtBillNo.Text = "";
            txtBillDate.Text = "";
        }
        else
        {
            Response.Write("<script> alert ('Please Enter Bill No..!!');</script>");
        }
         
    }
}