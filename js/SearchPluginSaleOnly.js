﻿(function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                                        doneTyping = function (el) {
                                            if (!timeoutReference) return;
                                            timeoutReference = null;
                                            callback.call(el);
                                        };
            return this.each(function (i, el) {
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress', function (e) {
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too premptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type == 'keyup' && e.keyCode != 8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);







(function ($) {
    // Plugin definition.
    $.fn.supersearch = function (options) {
        // Extend our default options with those provided.
        // Note that the first argument to extend is an empty
        // object – this is to keep from overriding our "defaults" object.
        var opts = $.extend({}, $.fn.supersearch.defaults, options);
        // Our plugin implementation code goes here.

        $.fn.supersearch.defaults = {
            Type: "",
            AccountType: "",
            Godown: 0

        };

        var cntrl_ID = $(this).attr("id");


        $(this).wrap("<div id='dvOuter_" + cntrl_ID + "' style='width:" + options.Width + "px' name='outerDiv' class='ui search dropdown selection downward active visible'  >   </div>");
        $(this).after("<i class=''></i> <input class='search' id='txt" + cntrl_ID + "' placeholder='" + options.Caption + "' autocomplete='off' tabindex='0'/><span id='spLoader" + cntrl_ID + "' style='display:none;height:0px;float:right;margin-right:-15px'><img src='images/pluginloader.gif'/></span><div class='menu' tabindex='-1' id='dvR_" + cntrl_ID + "' style='display:none'></div>");




        $("#txt" + cntrl_ID + "").click(
        function () {
            $(this).val("");

        }

        );

        $("#txt" + cntrl_ID + "").blur(
       function () {

           $(this).val($("#" + cntrl_ID + " option:selected").text());

       }

       );


        $("#txt" + cntrl_ID + "").donetyping(
                function () {


                    var Keyword = $(this).val();

                    if (Keyword.length >= 3) {

                        $("div[name='outerDiv']").css("z-index", "9");
                        $("#dvOuter_" + cntrl_ID + "").css("z-index", "99999");
                        $("#spLoader" + cntrl_ID + "").css("display", "block");


                        $.ajax({
                            type: "POST",
                            data: '{"Keyword":"' + Keyword + '","Type":"' + options.Type + '","AccountType":"' + options.AccountType + '","GodownId":"' + options.Godown + '"}',
                            url: "plugin.aspx/KeywordSearchSaleOnly",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {
                                var obj = jQuery.parseJSON(msg.d);



                                if (obj.Data.length > 0) {

                                    $("#dvR_" + cntrl_ID + "").html(obj.Data).css("display", "block");
                                    $("#" + cntrl_ID).html(obj.Options);

                                }
                                else {
                                    $("#txt" + cntrl_ID + "").val("No Result Found");
                                    $("#" + cntrl_ID).html("<option value=" + options.DefaultValue + " ></option>");


                                }







                            }, error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function (msg) {
                                $("#spLoader" + cntrl_ID + "").css("display", "none");


                                $(".cutomdivitem").click(
                function () {

                    var SelId = $(this).attr("data-value");
                    $("#" + cntrl_ID + " option[value='" + SelId + "']").prop("selected", true);
                    $("#dvR_" + cntrl_ID + "").css("display", "none");
                    $("#txt" + cntrl_ID + "").val($("#" + cntrl_ID + " option:selected").text());


                    GetPluginData(options.Type);



                }

                );



                            }

                        });



                    }
                    else {

                        $("#dvR_" + cntrl_ID + "").css("display", "none");
                    }
                }

                );

    };

})(jQuery);
