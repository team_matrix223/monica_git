﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>


<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MONICA'S</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

   

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form id="form1" runat="server">
                        <h1>Login Form</h1>

                         <div>
                         <asp:DropDownList class="form-control" id="ddlLocation" runat="server" placeholder="Choose Location" AutoPostBack="true" style="width:350px;margin-bottom:20px;height:32px" 
                             onselectedindexchanged="itemSelected" >
                               <asp:ListItem Value="0" Text="Choose Location"></asp:ListItem> 
                             <asp:ListItem Value="1" Text="Connect Any Branch"></asp:ListItem>  
                             <asp:ListItem Value="3" Text="Connect Monica1617"></asp:ListItem>  
                             <asp:ListItem Value="2" Text="Connect Gelato"></asp:ListItem>  
                             <asp:ListItem Value="4" Text="Connect Gelato1617"></asp:ListItem>    
                                    </asp:DropDownList>
                        
                        </div>




                        <div>
                         <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList>
                        
                        </div>
                        
                        <div>
                      
                          <input type="text" id="txtUserName" class="form-control" runat="server" placeholder="Username" required="" />
                        </div>
                        <div>
                        
                           <input type="password" id="txtPassword" class="form-control" runat="server" placeholder="Password" required="" />
                        </div>
                        <div>
                        <asp:Button id="btnLogin" style="width:90px;height:35px"  class="btn btn-default submit" Text="Log In" OnClick="btnLogin_Click" runat="server"/>
                      
                          
                        </div>
                        <div class="clearfix"></div>
                       
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
          
        </div>
    </div>

</body>

</html>
