﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin : System.Web.UI.MasterPage
{
    public string EmployeeName { get; set; }
    public string BranchName { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Request.Cookies.AllKeys.Contains(Constants.AdminId))
        {
            Response.Redirect("loginerror.aspx");
        }

        EmployeeName = Request.Cookies[Constants.EmployeeName].Value;
        BranchName = Request.Cookies[Constants.BranchName].Value;


    }
}
