﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class managebranches : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
        if (!IsPostBack)
        {
            Bind();
        }
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BRANCHES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    public void Bind()
    {
        ddlState.DataSource = new StateBLL().GetAll();
        ddlState.DataValueField = "State_ID";
        ddlState.DataTextField = "State_Name";
        ddlState.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Other--";
        li1.Value = "0";
        ddlState.Items.Insert(0, li1);


    }

    [WebMethod]
    public static string Insert(int BranchId, string BranchName, bool IsActive,int StateId)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BRANCHES));

        string[] arrRoles = sesRoles.Split(',');


        if (BranchId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }



        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Branches objBranch = new Branches()
        {
            BranchId = BranchId,
            BranchName = BranchName.Trim().ToUpper(),
            IsActive = IsActive,
            UserId = Id,
            StateId = StateId

        };
        status = new BranchBLL().InsertUpdate(objBranch);
        var JsonData = new
        {
            branch = objBranch,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}