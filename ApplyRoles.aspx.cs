﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ApplyRoles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            BindDesignations();
        }
    }



    void BindDesignations()
    {



        ddlDepartment.DataSource = new DesignationBLL().GetAll();
        ddlDepartment.DataValueField = "DesgID";
        ddlDepartment.DataTextField = "DesgName";
        ddlDepartment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Designation--";
        li1.Value = "0";
        ddlDepartment.Items.Insert(0, li1);



    }


    [WebMethod]
    public static string Insert(int DesignationId, string Roles)
    {
        Roles objRoles = new Roles()
        {
            DesignationId = DesignationId,
            Role = Roles,
           
        };
        int status = new RolesBLL().InsertUpdate(objRoles);
        var JsonData = new
        {
            Roles = objRoles,
            Status = status
           
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetRoles(int DesignationID)
    {
        Roles objRoles = new Roles() { DesignationId = DesignationID };


         new RolesBLL().GetByDesignationId(objRoles);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            DesgRoles = objRoles
        };
        return ser.Serialize(JsonData);
    }
}