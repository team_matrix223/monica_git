﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managetaxes1.aspx.cs" Inherits="managetaxes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    
    
   

 
    <link href="semantic.css" rel="stylesheet" type="text/css" />
 
 
 
        
 

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Taxes</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Tax</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

      <table cellpadding="0" cellspacing="0" style="width:100%" border="0" id="frmCity" style="text-align:left" >
                     
                   
                     <tr><td class="headings">Tax(%):</td><td>  <input type="text"
                       name="txtRate" class="form-control validate required "
                          data-index="1" id="txtRate" style="width: 213px"/></td>
                          
                          </tr>
                     <tr><td class="headings">Description:</td><td>  <input type="text"  name="txtDescription" class="form-control validate required "   data-index="1" id="txtDescription" style="width: 213px"/></td></tr>
                     <tr>
                     <td class="headings">Acc. To Debit:</td>
                     <td>
       
      <select id="ddlDrAccount" style="width:192px;height:35px;margin-top:5px;margin-bottom:5px"></select>
      
 
                     
                     
                 
                     
                     </td></tr>                               
                     <tr><td class="headings">Vat Acc. To Debit:</td>
                     <td align="left"  >
                     <table>
                     <tr>
                     <td style="padding-right:2px">
                     <input type="checkbox" id="chkDrVat" checked="checked" data-index="2"  name="chkDrVat" /></td>
                     <td>
 <asp:DropDownList   ClientIDMode="Static"  style="width:192px;height:35px;margin-top:5px;margin-bottom:5px"  ID ="ddlVatDr" runat="server" >
                     </asp:DropDownList></td></tr>
                     </table>

                     
                  
                     </td>
                 </tr>                                
                      
                     <tr><td class="headings">Acc. To Credit:</td><td>  
<asp:DropDownList   ClientIDMode="Static"  style="width:215px;height:35px;margin-top:5px;margin-bottom:5px" CssClass="validate ddlrequired" ID ="ddlCrAccount" runat="server" ></asp:DropDownList></td></tr>                               
                     <tr><td class="headings">Vat Acc. To Credit:</td>
                     <td align="left" style="text-align:left">
                     <input type="checkbox" id="chkCrVat" checked="checked" data-index="2"  name="chkCrVat" />
               <asp:DropDownList style="width:192px;height:35px;margin-top:5px;margin-bottom:5px"  ClientIDMode="Static" ID ="ddlVatCr" runat="server" >
                     </asp:DropDownList> 
                     </td>
                    </tr>                                
                                           
                     <tr><td class="headings">Use Surcharges:</td>
                     
                     <td align="left" style="text-align:left">
                     <table>
                    <tr>
                    <td> <input type="checkbox" id="chkSur" checked="checked" data-index="2"  name="chkSur" /></td>
                    <td class="headings">SurCharge(%)</td>
                    <td><input type="text"  name="txtSurCharge" class="form-control"   
                     data-index="1" id="txtSurCharge" style="width: 150px"/></td>
                    
                    </tr>
                     </table>

                     </td>
                     
                     
                     
                     </tr>                                                      
                     <tr><td class="headings">Surcharge Acc. To Debit:</td><td> 
                      <asp:DropDownList ClientIDMode="Static" style="width:192px;height:35px;margin-top:5px;margin-bottom:5px"  ID ="ddlSurDr"  runat="server" ></asp:DropDownList></td></tr>                               
                     <tr><td class="headings">Surcharge Acc. To Credit:</td><td>  
                     <asp:DropDownList   ClientIDMode="Static"  style="width:192px;height:35px;margin-top:5px;margin-bottom:5px"  ID ="ddlSurCr" runat="server" ></asp:DropDownList></td></tr>                               
                     <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Tax</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Tax</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Taxes</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>


    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete"  class="btn btn-primary btn-small" >Delete Tax</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


 
</form>



<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
       <script src="js/jquery.jqGrid.js" type="text/javascript"></script>

     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
      <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

       <script type="text/javascript" src="js/SearchPlugin.js"></script> 
    <script language="javascript" type="text/javascript">

        $(document).ready(
        function () {

            $("#ddlDrAccount").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });

            $("#ddlVatDr").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });


            $("#ddlCrAccount").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });

            $("#ddlVatCr").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });


            $("#ddlSurCr").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });


            $("#ddlSurDr").supersearch({
                Type: "Accounts",
                Caption: "Please enter Account ",
                AccountType: "A"
            });

            

            
            

        }

        );
    
    </script>
 

 <style type="text/css">
 
 .headings
 {
     text-align:left;
 }
 
 
 </style>
     
   
<script language="javascript" type="text/javascript">
    var m_TaxId = -1;

    function getAccount(code,name,type) {

        if (type == "1") {

            $("#ddlDrAccount").val(code);
            $("#txtDRAccount").val(name);
            $(".tbSearchResult").css("display", "none");
        }
    }

    function ResetControls() {
        m_TaxId = -1;
        var txtRate = $("#txtRate");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtRate.focus();

        txtRate.val("0");
        $("#txtDescription").val("");
        $("#txtSurCharge").val("");
        txtRate.focus();
        $("#ddlDrAccount").val("0");
        $("#ddlCrAccount").val("0");
        $("#ddlSurDr").val("0");
        $("#ddlSurCr").val("0");
        $("#ddlVatDr").val("0");
        $("#ddlVatCr").val("0");
        $("#chkSur").prop("checked", false);
        $("#chkDrVat").prop("checked", false);
        $("#chkIsActive").prop("checked", false);
        $("#chkCrVat").prop("checked", false);
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Tax");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Tax");

        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_TaxId;
        var Rate = $("#txtRate").val();
        if ($.trim(Rate) == "") {
            $("#txtRate").focus();

            return;
        }
        var Description = $("#txtDescription").val();
        var DrAcc = $("#ddlDrAccount").val();
        var CrAcc = $("#ddlCrAccount").val();
        var DrVatAcc = $("#ddlVatDr").val();
        var CrVatAcc = $("#ddlVatCr").val();

        var DrSurAcc = $("#ddlSurDr").val();
        var CrSurAcc = $("#ddlSurCr").val();

        var DrVat = false;
        if ($('#chkDrVat').is(":checked")) {
            DrVat = true;
        }

        var CrVat = false;
        if ($('#chkCrVat').is(":checked")) {
            CrVat = true;
        }
        var Surchrg = false;
        var SurchargeValue = 0;
        if ($('#chkSur').is(":checked")) {
            Surchrg = true;
            SurchargeValue = $("#txtSurCharge").val();
        }


        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }

        $.ajax({
            type: "POST",
            data: '{"TaxId":"' + Id + '", "TaxRate": "' + Rate + '","Description": "' + Description + '","DrAcc": "' + DrAcc + '","CrAcc": "' + CrAcc + '","DrVat": "' + DrVat + '","CrVat": "' + CrVat + '","DrVatAcc": "' + DrVatAcc + '","CrVatAcc": "' + CrVatAcc + '","ChkSur": "' + Surchrg + '","SurVal": "' + SurchargeValue + '","DrSur": "' + DrSurAcc + '","CrSur": "' + CrSurAcc + '","IsActive": "' + IsActive + '"}',
            url: "managetaxes.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {

                    alert("Insertion Failed.Tax with duplicate Rate already exists.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.Tax.Tax_ID, obj.Tax, "last");
                    alert("Tax is added successfully.");
                }
                else {
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.Tax);
                    alert("Tax is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {


       


        $("#ddlVatDr").attr("disabled", "disabled");
        $("#ddlVatCr").attr("disabled", "disabled");
        $("#chkSur").prop("checked", false);
        $("#chkDrVat").prop("checked", false);
        $("#chkIsActive").prop("checked", false);
        $("#chkCrVat").prop("checked", false);
        BindGrid();

        $("#btnAdd").click(
        function () {


            m_TaxId = 1;
            InsertUpdate();
        }
        );


        $("#chkDrVat").change(function () {

            if ($('#chkDrVat').is(":checked")) {
                $("#ddlVatDr").removeAttr("disabled")

            }
            else {
                $("#ddlVatDr").attr("disabled", "disabled")
            }

        });

        $("#chkCrVat").change(function () {

            if ($('#chkCrVat').is(":checked")) {
                $("#ddlVatCr").removeAttr("disabled")

            }
            else {
                $("#ddlVatCr").attr("disabled", "disabled")
            }

        });

        $("#btnUpdate").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Tax is selected to Edit");
                return;
            }

            InsertUpdate();
        }
        );



        $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Tax is selected to Delete");
               return;
           }

           var TaxId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Tax_ID')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');

               $.ajax({
                   type: "POST",
                   data: '{"TaxId":"' + TaxId + '"}',
                   url: "managetaxes.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       BindGrid();
                       alert("Tax is Deleted successfully.");




                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });









           }


       }
       );

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageTaxes.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Tax Id', 'Tax Rate','Description','DebitTo','CreditTo','VatDr','VatDrCode','VatCr','VatCrCode','ChkSur','Surcharge','SurDrCode','SurCrCode','IsActive'],
  


                        colModel: [
                                    { name: 'Tax_ID', key: true, index: 'Tax_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Tax_Rate', index: 'Tax_Rate', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Description', index: 'Description', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Dr_AccCode', index: 'Dr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Cr_AccCode', index: 'Cr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'VatCode_Dr', index: 'VatCode_Dr', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Dr_VatCode', index: 'Dr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'VatCode_Cr', index: 'VatCode_Cr', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Cr_VatCode', index: 'Cr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'ChkSur', index: 'ChkSur', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'SurValue', index: 'SurValue', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Dr_Sur', index: 'Dr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Cr_Sur', index: 'Cr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                      { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Tax_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Taxes List",

                        editurl: 'handlers/ManageTaxes.ashx',


 ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_TaxId = 0;
                    validateForm("detach");
                    var txtRate = $("#txtRate");
                    m_TaxId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Tax_ID');
                  
                    
                     txtRate.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Tax_Rate'));
                     txtRate.focus();
                    
                     $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));
                     var DrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_AccCode');
                    $("#ddlDrAccount").val(DrAccount);
                    $("#ddlDrAccount"> option[value='" + DrAccount + "']").prop("selected", true);

                      var CrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_AccCode');
                     $("#ddlCrAccount option[value='" + CrAccount + "']").prop("selected", true);
                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VatCode_Dr') == "true") {
                        $('#chkDrVat').prop('checked', true);
                    }
                    else {
                        $('#chkDrVat').prop('checked', false);

                    }
                     if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VatCode_Cr') == "true") {
                        $('#chkCrVat').prop('checked', true);
                    }
                    else {
                        $('#chkCrVat').prop('checked', false);

                    }
                     var DrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_VatCode');
                     $("#ddlVatDr option[value='" + DrVat + "']").prop("selected", true);
                  
                    var CrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_VatCode');
                    $("#ddlVatCr option[value='" + CrVat + "']").prop("selected", true);

                     if ($('#jQGridDemo').jqGrid('getCell', rowid, 'ChkSur') == "true") {
                        $('#chkSur').prop('checked', true);
                    }
                    else {
                        $('#chkSur').prop('checked', false);

                    }


                      if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                     $("#txtSurCharge").val($('#jQGridDemo').jqGrid('getCell', rowid, 'SurValue'));
                    var CrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_Sur');
                    $("#ddlSurCr option[value='" + CrSur + "']").prop("selected", true);
                     var DrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_Sur');
                    $("#ddlSurDr option[value='" + DrSur + "']").prop("selected", true);
                    
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


            </script>


</asp:Content>

