﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ManagePropGroups : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            BindDepartments();
        }
        CheckRole();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.GROUPS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()

                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    void BindDepartments()
    {

        ddlDepartment.DataSource = new DepartmentBLL().GetAll();
        ddlDepartment.DataValueField = "PROP_ID";
        ddlDepartment.DataTextField = "PROP_NAME";
        ddlDepartment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Department--";
        li1.Value = "0";
        ddlDepartment.Items.Insert(0, li1);

    }



    [WebMethod]
    public static string Insert(int GroupId, string GroupName, string ImageUrl, bool IsActive, bool ShowInMenu, Int32 DepartmentId)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        PropGroups objPropGroups = new PropGroups()
        {
            Group_ID = GroupId,
            Group_Name = GroupName.Trim().ToUpper(),
            ImageUrl = ImageUrl,
            ShowInMenu = ShowInMenu,
            IsActive = IsActive,
            Department_Id = DepartmentId,
            UserId = Id,

        };
        int status = new PropGroupBLL().InsertUpdate(objPropGroups);
        var JsonData = new
        {
            Groups = objPropGroups,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 GroupId)
    {
        PropGroups objPropGroup = new PropGroups()
        {
            Group_ID = GroupId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new PropGroupBLL().DeletePropGroup(objPropGroup);
        var JsonData = new
        {
           Propgroup = objPropGroup,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}