﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;

public partial class PhysicalStock : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection();
   // public string DayOpen { get { return Request.QueryString["DayOpen"] != null ? Convert.ToString(Request.QueryString["DayOpen"]) : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetCurrentStatus();

        }
    }


    void GetCurrentStatus()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        hdnClosedDay.Value = "";
        hdnOpenedDay.Value = "";
       
        string date = "";
        string Status = "";
        date = new DayOpenCloseDAL().GetCurrentStatus(Branch, out Status);

        if (Status == "1")
        {
            hdnOpenedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        else
        {
            hdnClosedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        hdnStatus.Value = Status;


        con.Close();
        if (hdnOpenedDay.Value != "")
        {
 
            hdnLastOpenedDate.Value = date;
            Response.Write("<script>alert('Day is Not Closed.Please Close Your Day to enter physical stock')</script>");
            
            Response.Redirect("index.aspx");
        }
        else
        {

            hdnLastOpenedDate.Value = (Convert.ToDateTime(date)).ToShortDateString();
           
           
        }

    }


    [WebMethod]
    public static string BindControls(string Type)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //string godownData = new GodownsBLL().GetOptions();
        string godownFrom = new GodownsBLL().GetOptionsGodownFrom(Branch);
        string groupstock = new PhysicalStockBLL().GetOptions(Type);
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
           // GodownOptions = godownData,
            GodownFromOptions = godownFrom,
            GroupStock = groupstock

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetAllPrroductsByType(string Type, string Sorting, Int32 Id, string Name,string OrderType)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<StockExpiry> StockList = new List<StockExpiry>();

        
        int cntr = 0;
        StockList = new PhysicalStockBLL().GetAllPrroductsByType(Type, Sorting, Id, Name, OrderType,Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = StockList,
           
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Insert(int GodownId, string Status,  string ItemCodeArr, string phystockArr)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string[] ItemCodeData = ItemCodeArr.Split(',');
        string[] PhyStockData = phystockArr.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Phy_Stock");
       
        for (int i = 0; i < ItemCodeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["Item_Code"] = Convert.ToString(ItemCodeData[i]);
            dr["Phy_Stock"] = Convert.ToDecimal(PhyStockData[i]);
           
            dt.Rows.Add(dr);
        }

        int status = 0;
       
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);

        status = new PhysicalStockBLL().Insert(GodownId, dt, Status, Branch,UserNo);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,
            

        };
        return ser.Serialize(JsonData);
    }






}