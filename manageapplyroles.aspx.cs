﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageapplyroles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.APPLYROLES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string GetRolesByDesignation(int designationId)
    {

        string RolesData = new RolesBLL().GetRolesByDesignation(designationId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Data = RolesData
        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetDesignations()
    {
        string Designations = new DesignationBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            DesignationOptions = Designations

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Insert(string Pid, string Rid, int Did)
    {





        string[] PidData = Pid.Split(',');
        string[] RidData = Rid.Split(',');


        DataTable dt = new DataTable();

        dt.Columns.Add("RoleId");
        dt.Columns.Add("PageId");




        for (int i = 0; i < PidData.Length; i++)
        {
            if (RidData[i] != "")
            {
                DataRow dr = dt.NewRow();

                dr["RoleId"] = Convert.ToDecimal(RidData[i]);
                dr["PageId"] = Convert.ToInt32(PidData[i]);


                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new RolesBLL().AssignRoles(Did, dt);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }

}