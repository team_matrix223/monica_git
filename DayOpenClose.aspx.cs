﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
public partial class backoffice_DayOpenClose : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            GetCurrentStatus();

        }
    }

    void GetCurrentStatus()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        hdnClosedDay.Value = "";
        hdnOpenedDay.Value = "";
        txtDate.Text = DateTime.Now.ToString("d");
        string date = "";
        string Status = "";
        date = new DayOpenCloseDAL().GetCurrentStatus(Branch, out Status);

        if (Status == "1")
        {
            hdnOpenedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        else
        {
            hdnClosedDay.Value = Convert.ToDateTime(date).ToString("d");
        }
        hdnStatus.Value = Status;


        con.Close();
        if (hdnOpenedDay.Value != "")
        {
            rbDayClose.Enabled = true;
            rbDayOpen.Enabled = false;
            
            rbDayOpen.Checked = false;
            rbDayClose.Checked = true;

         
            hdnLastOpenedDate.Value = date;
            lblMessage.Text = "Date '" + date + "' is not Closed";
        }
        else
        {
            rbDayClose.Enabled = false;
            rbDayOpen.Enabled = true;
           
            rbDayOpen.Checked = true;
            rbDayClose.Checked = false;

        }

    }



    protected void btnOK_Click(object sender, EventArgs e)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string OpenClose = "Close";
        if (rbDayOpen.Checked)
        {
            OpenClose = "Open";

        }

        int retVal = new DayOpenCloseDAL().DayOpenCloseCheckDelivery(OpenClose, Convert.ToDateTime(txtDate.Text), Branch);
            if (retVal==-1)
            {

             Response.Write("<script>alert('Day Already Opened')</script>");
            
            }

            else  if (retVal==-2)
            {

                Response.Write("<script>alert('Billing Already Done In Future Date  ')</script>");
            
            }

            else if (retVal == -3)
            {

                Response.Write("<script>alert('Past Day Is Not Closed ')</script>");

            }

            else if (retVal == -5)
            {
                Response.Write("<script>alert('Day Opened Successfully '  )</script>");
                Response.Write("<script>window.close();</script>");
            }
            else if (retVal == -11)
            {
                Response.Write("<script>alert('Day already Closed'   )</script>");
            }
            else if (retVal == -10)
            {
                pnlOpenClose.Visible = false;
                pnlVerify.Visible = true;
            }
            else if (retVal == -15)
            {
                Response.Write("<script>alert('Day Closed Successfully' )</script>");
                Response.Write("<script>window.close();</script>");
            }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

        Response.Write("<script>window.close();</script>");
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        pnlOpenClose.Visible = true;
        pnlVerify.Visible = false;
        Response.Redirect("Reports/rptDeliveryNote.aspx?Date=" + Convert.ToDateTime(txtDate.Text) + "");


       
    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int retVal = new DayOpenCloseDAL().DayOpenCloseForceClose(Branch);
        if (retVal == 1)
        {
            Response.Write("<script>alert('Day Closed Successfully' )</script>");
            Response.Write("<script>window.close();</script>");
            pnlOpenClose.Visible = true;
            pnlVerify.Visible = false;
        }
    }
}