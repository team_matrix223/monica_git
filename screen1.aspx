﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="screen1.aspx.cs" Inherits="screen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/css.css" rel="stylesheet" />
 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Super Store Billing</title>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
   
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/bootstrap-glyphicons.css" />
 
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="js/jquery.uilock.js"></script>
 


<%--<script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>--%>
<style type="text/css">
#tbProductInfo tr
{
    border-bottom:dotted 1px silver;
    
    }

#tbProductInfo tr td
{
padding:3px;
    
    }
 
 
 
 #tboption tr
{
    border-bottom:solid 1px black;
    
    }

#tboption tr td
{
padding:10px;
    
    }   
    
</style>


 <script language="javscript" type="text/javascript">

  var Sertax = 0;
  var Takeaway = 0;
  var Takeawaydefault = 0;


  //.....................................

  function DEVBalanceCalculation()
{


var txtCashReceived=$("#txtCashReceived");
var txtCreditCard=$("#Text13");
var txtCheque=$("#Text15");
var txtFinalBillAmount=$("#txtFinalBillAmount");


if(Number(txtCashReceived.val())>=Number(txtFinalBillAmount.val()))
{
txtCreditCard.val(0);
txtCheque.val(0);
}
else if( (Number(txtCashReceived.val())+ Number(txtCreditCard.val()))>=Number(txtFinalBillAmount.val()))
{
txtCreditCard.val(Number(txtFinalBillAmount.val())-Number(txtCashReceived.val()));
txtCheque.val(0);

}
else if( (Number(txtCashReceived.val())+ Number(txtCreditCard.val())+ Number(txtCheque.val()))>=Number(txtFinalBillAmount.val()))
{
txtCheque.val(Number(txtFinalBillAmount.val())-(Number(txtCashReceived.val())+Number(txtCreditCard.val())));

}



var balReturn=Number((Number(txtCashReceived.val())+Number(txtCreditCard.val())+Number(txtCheque.val())- Number(txtFinalBillAmount.val()))  );
 

$("#txtBalanceReturn").val(balReturn.toFixed(2));
 

}





  //...................................

   var DiscountAmt = 0;
      
         var Total = 0;
         var DisPer = 0;
         var VatAmt = 0;
         var TaxAmt = 0;


          function bindGrid2() {
    
    var searchon=$("input[name='searchon1']:checked").val();
    var criteria=$("input[name='searchcriteria1']:checked").val();
    var stext=$("#Txtsrchcredit").val();
  
       alert(stext);
                   jQuery("#jQGridDemoCredit").GridUnload();
  
             jQuery("#jQGridDemoCredit").jqGrid({
            url: 'handlers/CreditCustomerSearch.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['Code','Name', 'CSTNO', 'TINNO' ],
            colModel: [
                        { name: 'CCODE',key:true, index: 'CCODE', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'CNAME', index: 'CNAME', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'CST_NO', index: 'CST_NO', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'TINNO', index: 'TINNO', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 
                         
   		     
   		     
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerCredit',
            sortname: 'CCODE',
            viewrecords: true,
            height: "100%",
            width:"800px",
            sortorder: 'desc',
            caption: "Customers List" 
             
             
        });


        $('#jQGridDemoCredit').jqGrid('navGrid', '#jQGridDemoPagerCredit',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );

       var Datad = jQuery('#jQGridDemoCredit'); 
     Datad.jqGrid('setGridWidth', '680');

        $("#jQGridDemoCredit").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {

         
            
             var customerId=  $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CCODE') ;
                   $("#hdnCreditCustomerId").val(customerId);
                   $("#lblCreditCustomerName").text($('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME'));
                  $('#dvCreditCustomerSearch').dialog('close');
                     $("#creditCustomer").css("display","block");
                      $("#ddlbilltype option[value='Credit']").prop("selected",true);



             }
         });

      }
      



  function bindGrid() {
    
    var searchon=$("input[name='searchon']:checked").val();
    var criteria=$("input[name='searchcriteria']:checked").val();
    var stext=$("#txtSearch1").val();
       alert(stext);
     
                   jQuery("#jQGridDemo").GridUnload();
  
             jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/CustomersList.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ID','Name', 'Address1', 'Address2','Area','City','State','DateOfBirth','AnniversaryDate','Discount','ContactNo','Tag','FocBill','Group'],
            colModel: [
                        { name: 'Customer_ID',key:true, index: 'Customer_ID', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
   		                 { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                
   		                 { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Date_Of_Birth', index: 'Date_Of_Birth', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Date_Anniversary', index: 'Date_Anniversary', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'Discount', index: 'Discount', width: 150, stype: 'text',hidden:false, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text',hidden:false, sortable: true, editable: true ,editrules: { required: true }},
   		                
   		                { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'grpid', index: 'grpid', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'Code',
            viewrecords: true,
            height: "100%",
            width:"800px",
            sortorder: 'desc',
            caption: "Customers List" 
             
             
        });


        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );

       var Datad = jQuery('#jQGridDemo'); 
     Datad.jqGrid('setGridWidth', '680');



     $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
                var Discount=$('#jQGridDemo').jqGrid('getCell', rowid, 'Discount');
                 
                       $('#dvSearch').dialog('close');
          
                  
                  $("div[id='dvdisper']").html(Discount);
                   DiscountAmt = (Number(Total)* Number(Discount))/100;
                   $("div[id='dvdiscount']").html( DiscountAmt.toFixed(2));
                  $("div[id='dvnetAmount']").html(( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                       
             }
         });

   


     }

        function RestControls() {
       
            
              ProductCollection = [];

              $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            

            

              m_ItemId =0;
              m_ItemCode = "";
              m_ItemName = "";
              m_Qty = 0;
              m_Price = 0;
              m_Vat = 0;

          }

   

    function InsertUpdate() {
      if($("#ddlbillttype").val() == "Cash")
      {
             
              var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var BIllValue = $("#dvsbtotal").html();
           
             var DisPer = $("#dvdisper").html();
          
             var lessdisamt = $("#dvdiscount").html();
              
             var addtaxamt = $("#dvVat").html();

             var NetAmt = $("#txtFinalBillAmount").val();
              
             var billmode = $("#ddlbillttype").val();
             
             var CreditBank = $("#ddlBank").val();
              
             var CashAmt = $("#txtCashReceived").val();
             
             var creditAmt =0;
             if(billmode == "Credit")
             {
             creditAmt = Number(BIllValue)-Number(CashAmt);
            
             
             }
             
             
             var CreditCardAmt = $("#Text13").val();
              
             var RoundAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";
              if ($.trim(SelectedRow) == "") {
                cashcustcode = 0;
                cashcustName = "CASH"
              }
              else
              {
              cashcustcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_ID');
              cashcustName= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_Name');
              }
           
              var Tableno = 0;
             var setatx = $("#dvTax").html();
                
                
                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];


                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    OrgSaleRate[i] = ProductCollection[i]["Price"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];

                
                }
                }
              

              else   if($("#ddlbillttype").val() == "Credit")
      {
             
              var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
              var CustomerId = 0;
              var CustomerName = "";
               if ($.trim(SelectedRow) == "") {
                CustomerId = 0;
                CustomerName = "";
              }
              else
              {
              CustomerId= $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CCODE');
              CustomerName= $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CNAME');
              }
            
             var BIllValue = $("#dvsbtotal").html();
           
             var DisPer = $("#dvdisper").html();
          
             var lessdisamt = $("#dvdiscount").html();
              
             var addtaxamt = $("#dvVat").html();

             var NetAmt = $("#txtFinalBillAmount").val();
              
             var billmode = $("#ddlbillttype").val();
             
             var CreditBank = $("#ddlBank").val();
              
             var CashAmt = $("#txtCashReceived").val();
             
             var creditAmt =0;
             if(billmode == "Credit")
             {
             creditAmt = Number(BIllValue)-Number(CashAmt);
            
             
             }
             
             
             var CreditCardAmt = $("#Text13").val();
              
             var RoundAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";


              var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
              if ($.trim(SelectedRow2) == "") {
                cashcustcode = 0;
                cashcustName = "";
              }
              else
              {
              cashcustcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow2, 'Customer_ID');
              cashcustName= $('#jQGridDemo').jqGrid('getCell', SelectedRow2, 'Customer_Name');
              }
           
              var Tableno = 0;
             var setatx = $("#dvTax").html();
                
                
                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];


                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    OrgSaleRate[i] = ProductCollection[i]["Price"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];

                
                }
                }
            

             alert('"CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + Sertax + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '"');

                $.ajax({
                    type: "POST",
                    data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + Sertax + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '"}',
                    url: "screen.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        alert(obj.Status);
                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else
                        {
                        alert("Hello");
                        }
                      

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        RestControls();
                        $.uiUnlock();
                    }

                });

            }


 var option = "";


     $(document).ready(
        function () {


        
          $("#btnSearch").click(
        function () {

            var Keyword = $("#txtSearch");
            if (Keyword.val().trim() != "") {

                Search(0, Keyword.val());
            }
            else {
                Keyword.focus();
            }

        });


        
            $("#txtSearch").keyup(
            function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {


                    var Keyword = $("#txtSearch");
                    if (Keyword.val().trim() != "") {

                        Search(0, Keyword.val());
                    }
                    else {
                        Keyword.focus();
                    }


                }

            }

            );



  $("#btnBillWindowClose").click(
  function(){
  $("#dvBillWindow").dialog('close');
  }
  );

         $("#dvGetCreditCustomers,#dvGetCreditCustomersMember,#dvGetCreditCustomersPackage").click(
        function () {

        
            var DataGrid = jQuery('#jQGridDemo1');
            DataGrid.jqGrid('setGridWidth', '680');
            //DataGrid.jqGrid('setGridHeight', '200');
            jQuery('#jQGridDemo1').GridUnload();
            $('#dvCreditCustomerSearch').dialog(
        {
            autoOpen: false,

            width: 720,
            resizable: false,
            modal: false

        });


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#dvCreditCustomerSearch');
            dialogDiv.dialog("option", "position", [233, 48]);
            dialogDiv.dialog('open');
            return false;


        });





        $("#ddlbillttype").change(
    function()
    {
    
 

    if($(this).val()=="Credit")
    {


    
            $("#lblCashHeading").text("Cash Receipt Amt:");
            $("#ddlbillttype option[value='Credit']").prop("selected",true);
            $("#txtCashReceived").val("0").prop("readonly",false);
            $("#Text13").val("0").prop("readonly",true);
            $("#Text14").val("").prop("readonly",true);
            $("#Text15").val("0").prop("readonly",true);
            $("#Text16").val("").prop("readonly",true);
            $("#ddlType").prop("disabled",true);
            $("#ddlBank").prop("disabled",true);
           // alert($("#hdnCreditCustomerId").val()); 
            
            if($("#hdnCreditCustomerId").val()=="0")
            {

             
                jQuery('#jQGridDemo1').GridUnload();


                    $('#dvCreditCustomerSearch').dialog(
                    {
                        //autoOpen: false,

                        width: 720,
                        resizable: false,
                        modal: false

                    });

 
                var dialogDiv = $('#dvCreditCustomerSearch');
                dialogDiv.dialog("option", "position", [233, 48]);
               dialogDiv.dialog('open');
                return false;
           
            }
            else
            {
            $("#creditCustomer").css("display","block");
            }
        

           



    }
    else
    {
    $("#lblCashHeading").text("Cash Received:");
     $("#creditCustomer").css("display","none");

            $("#ddlbillttype option[value='Cash']").prop("selected",true);
            
                    
            $("#txtCashReceived").val("0").prop("readonly",false);
            $("#Text13").val("0").prop("readonly",false);
            $("#Text14").val("").prop("readonly",false);
            $("#Text15").val("0").prop("readonly",false);
            $("#Text16").val("").prop("readonly",false);
            $("#ddlType").prop("disabled",false);
            $("#ddlBank").prop("disabled",false);


    }

    }
    );




         option = $("#<%=ddloption.ClientID%>").val();
       

        $("#<%=ddloption.ClientID%>").change(
        function(){
        option = $("#<%=ddloption.ClientID%>").val();
        Bindtr();
          
        }
        );


            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                  

                    $("#categories").html(obj.categoryData);
                      
                    Sertax = obj.setttingData.SerTax;
                    Takeaway = obj.setttingData.TakeAway;
                    Takeawaydefault = obj.setttingData.TakeAwayDefault;

                   
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );



        $("#btnBillWindowOk").click(
function(){



var billtype = $("#ddlbillttype").val();
if(billtype == "")
{
alert("Select BillType First");
$("#ddlbillttype").focus();
return;

}
  $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');



 
  DEVBalanceCalculation();



  var txtcreditcardcheck = $("#Text13").val();

     if(txtcreditcardcheck != "0")
     {
         if($("#ddlType").val() == "")
         {
         $.uiUnlock();
         alert("Please Select Credit Card Type");
         $("#ddlType").focus();
         return;
         }
         if($("#Text14").val() == "")
         {
         $.uiUnlock();
         alert("Please Enter Credit Card No");
         $("#Text14").focus();
         return;
         }

     }

     var  txtchequecheck = $("#Text15").val();
     if(txtchequecheck != "0")
     {
        if($("#ddlBank").val() == "")
        {
        $.uiUnlock();
         alert("Please Select Bank");
         $("#ddlBank").focus();
         return;
        }
     if($("#Text16").val() == "")
     {
     $.uiUnlock();
     alert("Please Enter Cheque No");
     $("#Text16").focus();
       return;
     }
     }


      var cashamount = $("#txtCashReceived").val();

      
      if(billtype=="Cash")
      {
         if(Number($("#txtBalanceReturn").val())<0)
        {
        $.uiUnlock();
        alert("Total amount is not equal to Bill Amount....Please first tally amount.");
        return;
        }
        else
        {
        cashamount=cashamount-Number($("#txtBalanceReturn").val());
        }

       }
  
        if(Number(cashamount)<0)
        {
        $.uiUnlock();
         alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
            return;
        }

        InsertUpdate();


}
);







        $("#txtCashReceived").keyup(
function()
{
DEVBalanceCalculation();

}
);



$("#Text13").keyup(
function()
{
DEVBalanceCalculation();

}
);
$("#Text15").keyup(
function()
{
DEVBalanceCalculation();

}
);

  function ResetBillCntrols(){

  
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
          
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
  }



        
        $("#btnsavebill").click(
        function () {



            ResetBillCntrols();
            $("#hdnCreditCustomerId").val("0");
            $("#ddlbillttype option[value='Cash']").prop("selected",true); 
            $("#creditCustomer").css("display","none");

     
$("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly",true);
   
   
     $.ajax({
            type: "POST",
            data: '{}',
            url: "screen.aspx/BindBanks",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                $("#ddlBank").html(obj.BankOptions);
                 
            }


        });

             $('#dvBillWindow').dialog(
          {
            autoOpen: false,
                closeOnEscape: false,
                draggable:false,
            width:570,
            resizable: false,
            modal: false,
         
            
        });//.parent().find('.ui-dialog-titlebar-close').hide();

              //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#dvBillWindow');
            dialogDiv.dialog("option", "position", [180,200]);
            dialogDiv.dialog('open');
         
            return false;

        });




         $("#btncreditsrch").click(
   function()
   {
    bindGrid2();
   }
   );




            $("#btnCustomer").click(
        function () {

             $('#dvSearch').dialog(
            {
            autoOpen: false,

            width:720,
     
          
            resizable: false,
            modal: true,
                  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvSearch');
            dialogDiv.dialog("option", "position", [238, 36]);
            dialogDiv.dialog('open');
            return false;
                
   

        });


            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {

                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");

                }
                Bindtr();

            });





            $("#btnSearch1").click(
        function () {
        
          bindGrid();

        }
        );

        });

     var m_ItemId = 0;
     var m_ItemCode = "";
     var m_ItemName = "";
     var m_Qty = 0;
     var m_Price = 0;
     var m_TaxRate = 0;
     var m_Surval = 0;
     
      
     var ProductCollection = [];
     function clsproduct() {
         this.ItemId = 0;
         this.ItemCode= "";
         this.ItemName = "";
         this.Qty = 0;
         this.Price = 0;
         this.TaxCode = 0;
         this.SurVal = 0;
         this.ProductAmt = 0;
         this.Producttax = 0;
         this.ProductSurchrg = 0;
     }

     function Bindtr() {
           DiscountAmt = 0;
           VatAmt = 0;
           Total = 0;
           var fPrice = 0;
         $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
         for (var i = 0; i < ProductCollection.length; i++) {

         var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose'><img src='images/trash.png'/></i></td></tr>";
 
//  var tr = "<tr> <td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td>";
//             tr=tr+"<td style='text-align:center'><table ><tr style='border:0px'><td><div id='btnMinus' class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>-</div></td><td>" + ProductCollection[i]["Qty"] + " </td><td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>+</div></td></tr></table> </td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
//            
 
              $("#tbProductInfo").append(tr);
             fPrice = 0;
            fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
             var TAx = (Number(fPrice)* Number(ProductCollection[i]["TaxCode"])/100);
             var surchrg =(Number(TAx)* Number(ProductCollection[i]["SurVal"]))/100;
             var tottax = TAx+surchrg;
             VatAmt = VatAmt+ tottax;
             Total = Total + fPrice;
            ProductCollection[i]["ProductAmt"] = fPrice;
            ProductCollection[i]["Producttax"] = TAx;
            ProductCollection[i]["ProductSurchrg"] = surchrg;

         }
         $("div[id='dvsbtotal']").html(Total.toFixed(2)); 
          $("div[id='dvdisper']").html("0");
         $("div[id='dvdiscount']").html( DiscountAmt.toFixed(2));

         $("div[id='dvVat']").html(VatAmt.toFixed(2));
         TaxAmt =  GetServiceTax();

         $("div[id='dvnetAmount']").html(( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2));
       
        
         
     }

     function addToList(ProductId,Name, Price,TaxCode,SurVal,Code) {

         m_ItemId = ProductId;
         m_ItemCode = Code;
         m_Price = Price;
         m_ItemName = Name;
         m_Qty = 1;
         m_TaxRate = TaxCode; 
         m_Surval = SurVal;

         var item = $.grep(ProductCollection, function (item) {
             return item.ItemId == m_ItemId;
         });

         if (item.length) {

             alert("Product Alredy Added in shop list");

             return;

         }


         TO = new clsproduct();

         TO.ItemId = m_ItemId;
         TO.ItemCode = m_ItemCode
         TO.ItemName = m_ItemName;
         TO.Qty = 1;
         TO.Price = m_Price;
         TO.TaxCode = m_TaxRate;
         TO.SurVal = m_Surval;

     
         ProductCollection.push(TO);
         Bindtr();


     }
    

     function GetServiceTax() {
      

       var TaxAmt = 0;

       $("#dvsertaxper").html(Sertax);
      
     
      if(option == "TakeAway")
     {
        if(Takeaway == "1")
        {
         TaxAmt = (Number(Total) * Number(Sertax))/100 ; 
         $("#dvTax").html(TaxAmt.toFixed(2));
       
       
        }     
        else
        {
          $("#dvsertaxper").html("0");
         TaxAmt == "0";
         $("#dvTax").html(TaxAmt.toFixed(2));
        }
      }
      else  if(option == "Dine")
      {
        TaxAmt = (Number(Total) * Number(Sertax))/100 ; 
        $("#dvTax").html(TaxAmt.toFixed(2));
       

      }

    
     return TaxAmt;
     
     }


     function Search(CatId, Keyword) {


         $.ajax({
             type: "POST",
             data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
             url: "screen.aspx/AdvancedSearch",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);

                 $("#products").html(obj.productData);

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {

             }

         }
        );


     }

     //........................................

     $(document).on("click", "#btnPlus", function (event) {


         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];

         var Mode = "Plus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];
         var fQty = Number(Qty) + 1;




         ProductCollection[RowIndex]["Qty"] = fQty;

         Bindtr();

     });


     $(document).on("click", "#btnMinus", function (event) {

         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];
         var Mode = "Minus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];

         var fQty = Number(Qty) - 1;
         ProductCollection[RowIndex]["Qty"] = fQty;
         if (fQty == "0") {
             ProductCollection.splice(RowIndex, 1);
         }

         Bindtr();


     });



     //............................................




    
    </script>


    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
        <script>
            $(function () {
                $('#txtSearch,#txtSearch1').keyboard();



                $('#txtMobSearchBox').keyboard({
                    layout: 'custom',
                    customLayout: {
                        'default': [
    '1 2 3 4 5',
    '6 7 8 9 0',
    ' {bksp}',
    '{a} {c}'
   ]
                    },
                    maxLength: 10,
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    useCombos: false // don't want A+E to become a ligature
                }).addTyping();




                $('#txtCashReceived').keyboard({
                    layout: 'custom',
                    customLayout: {
                        'default': [
    '9 8 7 6 5',
    '4 3 2 1 0',
    ' . {bksp}',
    '{a} {c}'
   ]
                    },
                    maxLength: 6,
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    useCombos: false // don't want A+E to become a ligature
                }).addTyping();


            });
	</script>


</head>



<body>
<form runat = "server">

 <input type="hidden" id="hdnCreditCustomerId" value="0" />
<div class="container">

<div class="row">
<div class="nav">


  
<ul id ="categories">

  
</ul>
</div>
</div>

<div class="row">

<div class="col-md-4" style="padding-left:0px">

<div class="cate">
<table  style="width:100%" id = "tboption">
<tr>
<td style="font-weight:bold;font-size:15px;">
Select Option
</td>
<td>
  <asp:DropDownList ID="ddloption" runat="server" style="width:150px" >
                <asp:ListItem Text="Take Away" Value ="TakeAway"></asp:ListItem>
                <asp:ListItem Text="Dine" Value ="Dine"></asp:ListItem>
               
                </asp:DropDownList>
</td>
</tr>
</table>

</div>



<div class="leftside">

<table style="width:100%">

  <tr>

 
   
<%--    <th  style="width:80px;text-align:center">Id</th>--%>
<%--    <th style="width:200px;text-align:center">Name</th>
    <th style="width:50px;text-align:center"></th>
    <th style="width:50px;text-align:center">Qty</th>
    <th style="width:50px;text-align:center"></th>		
    <th style="width:100px;text-align:center">Price</th>--%>
<%--    
    <th style="width:50px;text-align:center; color: #FFFFFF;">Remove</th>--%>


     <th style="width:200px;text-align:center; color: #FFFFFF;">Name</th>
 
    <th style="width:50px;text-align:center; color: #FFFFFF;">Qty</th>
 
    <th style="width:70px;text-align:center; color: #FFFFFF;">Price</th>
    
    <th style="width:30px;text-align:center; color: #FFFFFF;"> </th>
  </tr>
  </table>
</div>

<div class="cate">
 <table style="width:100%;font-size:10px" id="tbProductInfo">
 <%--<tr ><td>1</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
 <tr><td>2</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>3</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>4</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>5</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>6</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>7</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>8</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>9</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>10</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>11</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>12</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>13</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>
<tr><td>14</td><td>Beverages</td><td>3 Kg</td><td>$22</td></tr>--%>

 </table>
  
<table id = "tbamountinfo"  style="width:100%;text-align:right;background:#F5FFFA">
                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr><td style="width:60%;font-weight:bold;text-align:right" text>Amount:</td><td style="width:100px;text-align:right;"><div id ="dvsbtotal"></div></td></tr>
                     <tr><td style="width:60%;font-weight:bold;text-align:right">Discount:</td><td style="width:30px;text-align:right;"><div id ="dvdisper"></div></td><td>%</td><td style="width:100px;text-align:right;"><div id ="dvdiscount"></div></td></tr>
                     <tr><td style="width:60%;font-weight:bold;text-align:right">ServiceTax:</td><td style="width:30px;text-align:right;"><div id ="dvsertaxper"></div></td><td>%</td><td style="width:100px;text-align:right"><div id ="dvTax"></div></td></tr>
                       
                         <tr><td  style="width:60%;font-weight:bold;text-align:right">Vat:</td><td style="width:100px;text-align:right;"><div id ="dvVat"></div></td></tr>
                     <tr><td  style="width:60%;font-weight:bold;text-align:right">Net Amount:</td><td style="width:100px;text-align:right"><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                
                        
                     </tr>

                     </table>


<table>
<tr>
<td colspan="100%">
<table>
        <tbody><tr><td><input type="text" placeholder="Mobile Number" style="border:solid 1px silver;padding:5px" id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all" aria-haspopup="true" role="textbox"></td><td>

  <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png">
</td></tr>
      </tbody></table>
</td>
</tr>
<tr>
<td><div class="button" id="btnCustomer" >Cash Customers</div></td>
<td><div class="button1" id="btnsavebill" >Bill</div></td>
</tr>
</table>

 
<%--<div class="row">
<div class="col-md-4">

</div>
<div class="col-md-4">
<div class="button1" id="btnCreditCust" >
Credit Customers
</div>
</div>
<div class="col-md-4">

</div>

<div class="col-md-4">
<div class="button2">
<h2>Check Out</h2>
</div>
</div>


</div>--%>

</div>

</div>


<div class="col-md-8" style="padding-left:0px">
 
<div id ="products" style="overflow-y: scroll; height: 408px;">

</div>
 

<div class="Search">
<div class="input-group">
  <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2" id="txtSearch" style="padding:10px 10px; width:80%;border:0px;height:50px">
  <span class="input-group-addon" id="btnSearch" value="Search"> <img src="images/search-button.png" alt="" style="width:50px;padding-left:10px"/></span>
</div>

</div>


</div>

 

</div>


 <div id="dvSearch" style="display:none">
  <table width="100%">
  <tr><td>
   <div id="dvLeft" style="float:left;width:37%;border:1px solid silver">
 
  
   <table  cellpadding="5" cellspacing="0"  width="100%">
      <tr style="background-color:#E6E6E6"><td colspan="100%" >Search On:</td></tr>
   <tr><td><input type="radio" id="rbPhoneNo" value="M" name="searchon" checked="checked" /> <label for="rbPhoneNo" style="font-weight:normal" > Phone No.</label></td><td><input type="radio" id="rbCustomerName" value="N" name="searchon" /> <label for="rbCustomerName"  style="font-weight:normal"> Customer Name</label></td></tr>
   </table>
     
   </div>
   <div id="dvRight" style="float:right;width:60%;border:1px solid silver">
   <table cellpadding="5" cellspacing="0" width="100%">
   <tr style="background-color:#E6E6E6"><td colspan="100%" >Search Criteria:</td></tr>
   <tr><td><input type="radio" id="rbStartingWith" value="S"  name="searchcriteria"/> <label for="rbStartingWith"  style="font-weight:normal"> Starting With</label></td><td><input type="radio" value="C" id="rbContaining" checked="checked"  name="searchcriteria"/> <label for="rbContaining" style="font-weight:normal" > Containing</label></td><td><input type="radio" id="rbEndingWith" value="E" name="searchcriteria"/> <label for="rbEndingWith"  style="font-weight:normal"> Ending With</label></td><td><input type="radio" id="rbExact" name="searchcriteria" value="EX"/> <label for="rbExact"  style="font-weight:normal"> Exact</label></td></tr>
   </table>
   
   </div>

  </td></tr>
  <tr><td>
  <table>
  <tr><td>Type to Search:</td><td><input type="text" class="form-control input-small" style="width:400px"   id="txtSearch1"/></td><td><div id="btnSearch1"  class="btn btn-primary btn-small"    >Search</div></td></tr>
  </table>
  
  </td></tr>
   <tr><td>
   
   			           <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
   
   </td></tr>

  </table>
  
  </div>

<div id="dvCreditCustomerSearch" style="display:none">
  <table width="100%">
  <tr><td>
   <div id="dvLeft1" style="float:left;width:37%;border:1px solid silver">
 
  
   <table  cellpadding="5" cellspacing="0"  width="100%">
      <tr style="background-color:#E6E6E6"><td colspan="100%" >Search On:</td></tr>
   <tr><td><input type="radio" id="rbPhoneNo1" value="M" checked="checked"  name="searchon1"/> <label for="rbPhoneNo1" style="font-weight:normal" > Phone No.</label></td><td><input type="radio" id="rbCustomerName1" value="N" name="searchon1"/> <label for="rbCustomerName1"  style="font-weight:normal"> Customer Name</label></td></tr>
   </table>
     
   </div>
   <div id="dvRight1" style="float:right;width:60%;border:1px solid silver">
   <table cellpadding="5" cellspacing="0" width="100%">
   <tr style="background-color:#E6E6E6"><td colspan="100%" >Search Criteria:</td></tr>
   <tr><td><input type="radio" id="rbStartingWith1" value="S"  name="searchcriteria1"/> <label for="rbStartingWith1"  style="font-weight:normal"> Starting With</label></td><td><input type="radio" value="C" id="rbContaining1" checked="checked"  name="searchcriteria1"/> <label for="rbContaining1" style="font-weight:normal" > Containing</label></td><td><input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1"/> <label for="rbEndingWith1"  style="font-weight:normal"> Ending With</label></td><td><input type="radio" id="rbExact1" name="searchcriteria1" value="EX"/> <label for="rbExact1"  style="font-weight:normal"> Exact</label></td></tr>
   </table>
   
   </div>

  </td></tr>
  <tr><td>
  <table>
  <tr><td>Type to Search:</td><td><input type="text" class="form-control input-small" style="width:400px"   id="Txtsrchcredit"/></td>
  <td><div id="btncreditsrch"  class="btn btn-primary btn-small"    >Search</div></td>
    <td>
                       
                     <div id="btnAllowCredit"  onclick="return creditpopup('allowcredit.aspx')"  class="btn btn-primary btn-small" style="cursor:pointer;color:White"  >
  Allow Credit 
  </div>
  </td>
  
  
  </tr>
  </table>
  
  </td></tr>
   <tr><td>
   
   			           <table id="jQGridDemoCredit">
    </table>
    <div id="jQGridDemoPagerCredit">
    </div>
   
   </td></tr>

  </table>
  
  </div>




<div id="dvBillWindow" style="display:none;background-color:#FFE6E6"> 
<table cellpadding="5"  >
<tr><td>Bill Amount:</td><td><input type="text"    class="form-control input-small" style="width:120px"  id="txtFinalBillAmount" /></td><td>Bill Type:</td><td>
<select id="ddlbillttype" style="height:30px;width:180px;padding-left:0px"">
<%--<option></option>--%>
<option value="Cash">Cash</option>
<option value="Credit">Credit</option>
 
</select></td></tr>
<tr >
<td colspan="100%">
<table width="88%" id="creditCustomer" style="display:none;border:dashed 1px silver" cellpadding="2">
<tr><td  >Credit Customer Name:</td><td   ><label  id="lblCreditCustomerName" style="font-weight:normal;margin-top:1px"   ></label></td><td style="padding-left:10px"><div style='cursor:pointer' id="dvGetCreditCustomers"><i class='glyphicon glyphicon-search'></i></div></td></tr>
</table>
</td>
</tr>


<tr><td><label id="lblCashHeading" style="font-weight:normal" >Cash Received:</label></td><td colspan="100%"><input type="text"    class="form-control input-small" style="width:120px" value="0"  id="txtCashReceived" /></td></tr>
<tr><td>Credit Card</td><td><input type="text"    class="form-control input-small" style="width:120px" value="0"  id="Text13" /></td><td>Type:</td><td>
<select id="ddlType" style="height:30px;width:180px">
<option></option>
<option value="Visa">VISA</option>
<option value="Maestro">MAESTRO</option>
<option value="Master">MASTER</option>
 
</select></td></tr>
<tr><td>Credit Card No:</td><td colspan="100%"><input type="text"    class="form-control input-small" style="width:240px"  id="Text14" /></td></tr>
<tr><td>Cheque:</td><td><input type="text"    class="form-control input-small" style="width:120px"  id="Text15" value="0" /></td><td>Bank:</td><td><select id="ddlBank" style="height:30px;width:180px"></select></td></tr>
<tr><td>Cheque No:</td><td colspan="100%"><input type="text"    class="form-control input-small" style="width:240px"  id="Text16" /></td></tr>
<tr>
<td colspan="100%">
<table>
<tr><td style="width:120px">Balance Return:</td><td style="width:135px"><input type="text"    class="form-control input-small" style="width:120px"  id="txtBalanceReturn" readonly ="readonly" /></td><td colspan="100%"><table cellpadding="2" cellspacing="0"><tr><td><div id="btnBillWindowOk"  class="btn btn-primary btn-small"    >Ok</div></td><td><div id="btnBillWindowClose"  class="btn btn-primary btn-small"    >Close</div></td></tr></table></td></tr>



</table>
</td>
</tr>
</table>
</div>
</div>
</form>
</body>
 
</html>
