﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managetaxes.aspx.cs" Inherits="managetaxes" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <script language="javascript" type="text/javascript">

        function ApplyRoles(Roles) {

            $("#<%=hdnRoles.ClientID%>").val(Roles);
    }

    </script>


<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Taxes</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Tax</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table" >
                     
                      <tr>
                                <td class="headings">Choose Branch:</td>              <td class="headings"style="text-align:left;" colspan="100%"><asp:DropDownList id="ddlBranch" ClientIDMode="Static" runat="server" style="width:215px;height:35px" >
                                  
                                    </asp:DropDownList></td></tr>
                     <tr><td class="headings">Tax(%):</td><td style="text-align:left">  <input type="text"  name="txtRate" class="form-control  validate required float "   data-index="1" id="txtRate" style="width: 213px"/></td></tr>
                     <tr><td class="headings">Description:</td><td  style="text-align:left">  <input type="text"  name="txtDescription" class="form-control validate required "   data-index="1" id="txtDescription" style="width: 213px"/></td></tr>
                     <tr><td class="headings">Acc. To Debit:</td><td style="text-align:left">  <asp:DropDownList ClientIDMode="Static" style="width:215px;height:35px" ID ="ddlDrAccount"  runat="server" ><asp:ListItem Value="0"></asp:ListItem></asp:DropDownList></td></tr>                               
                     <tr><td class="headings">Vat Acc. To Debit:</td>
                     <td align="left" style="text-align:left">
       
         <input type="checkbox" id="chkDrVat" checked="checked" data-index="2"   name="chkDrVat" />
         
         <asp:DropDownList  ClientIDMode="Static"  style="width:215px;height:35px" 
          ID ="ddlVatDr" runat="server" ><asp:ListItem Value="0"></asp:ListItem></asp:DropDownList>
         

         </td>
         
         
          
          </tr>                                
                      
                     <tr><td class="headings">Acc. To Credit:</td><td  style="text-align:left"> 
                     
                      <asp:DropDownList ClientIDMode="Static"  style="width:215px;height:35px"
                        ID ="ddlCrAccount" runat="server" >
                       <asp:ListItem Value="0"></asp:ListItem>
                       </asp:DropDownList></td></tr>                               
                     <tr><td class="headings">Vat Acc. To Credit:</td>
                     
                     <td align="left" style="text-align:left">
                     <input type="checkbox" id="chkCrVat" checked="checked" data-index="2" 
                      name="chkCrVat" />
                      
                      
                      <asp:DropDownList  ClientIDMode="Static"  
                      style="width:215px;height:35px"  ID ="ddlVatCr" runat="server" >

                      <asp:ListItem Value="0"></asp:ListItem>
                      </asp:DropDownList> 
                      </td>
                     
                      
                      </tr>                                
                                           
                     <tr><td class="headings">Use Surcharges:</td>

                     <td align="left" style="text-align:left">
                     <input type="checkbox" id="chkSur" checked="checked" data-index="2" 
                      name="chkSur" /> SurCharge(%)   <input type="text"  name="txtSurCharge" class="form-control validate float" 
                         data-index="1" id="txtSurCharge" style="width: 113px"/></td></tr>                                          
                         
                                     
                     <tr><td class="headings">Surcharge Acc. To Debit:</td>
                     <td  style="text-align:left">  
                     <asp:DropDownList ClientIDMode="Static"  style="width:215px;height:35px"  ID ="ddlSurDr"  runat="server" >
                    <asp:ListItem Value="0"></asp:ListItem>
                     </asp:DropDownList></td></tr>                               
                     
                     <tr><td class="headings">Surcharge Acc. To Credit:</td>
                     
                     <td  style="text-align:left">  <asp:DropDownList ClientIDMode="Static"  
                     style="width:215px;height:35px"  ID ="ddlSurCr" runat="server" >
                     <asp:ListItem Value="0"></asp:ListItem>
                     </asp:DropDownList></td>
                     
                     </tr>                               
                     <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i>
 Update</div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" ><i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Taxes</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" >
                    
      	          <table id="jQGridDemo">
    </table>


    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete" style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>


 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    
   <script type="text/javascript" src="js/SearchPlugin.js"></script> 
  <link href="semantic.css" rel="stylesheet" type="text/css" />
     
   
<script language="javascript" type="text/javascript">
    var m_TaxId = -1;

    function ResetControls() {
        
        m_TaxId = -1;
        var txtRate = $("#txtRate");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtRate.focus();



        txtRate.val("0");
        $("#txtDescription").val("");
        $("#txtSurCharge").val("");
        txtRate.focus();
        $("#ddlDrAccount").html("<option value='0'></option>");
        $("#ddlCrAccount").html("<option value='0'></option>");
        $("#ddlSurDr").html("<option value='0'></option>");
        $("#ddlSurCr").html("<option value='0'></option>");
        $("#ddlVatDr").html("<option value='0'></option>");
        $("#ddlVatCr").html("<option value='0'></option>");

        $("#txtddlDrAccount").val('');
        $("#txtddlCrAccount").val('');
        $("#txtddlSurDr").val('');
        $("#txtddlSurCr").val('');
        $("#txtddlVatDr").val('');
        $("#txtddlVatCr").val('');

        
        $("#txtddlVatDr").attr("readonly", "readonly");
        $("#txtddlVatCr").attr("readonly", "readonly");
        $("#ddlVatDr").attr("disabled", "disabled");
        $("#ddlVatCr").attr("disabled", "disabled");

        $("#txtSurCharge").val("0");
        $("#txtSurCharge").attr("disabled", "disabled");

        $("#txtddlSurDr").attr("readonly", "readonly");
        $("#txtddlSurCr").attr("readonly", "readonly");
        $("#ddlSurDr").attr("disabled", "disabled");
        $("#ddlSurCr").attr("disabled", "disabled");

        $("#chkSur").prop("checked", false);
        $("#chkDrVat").prop("checked", false);
        $("#chkIsActive").prop("checked", false);
        $("#chkCrVat").prop("checked", false);
        var arrRole = [];
        arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
          btnAdd.css({ "display": "none" });
          for (var i = 0; i < arrRole.length; i++) {
              if (arrRole[i] == "1") {

                  btnAdd.css({ "display": "block" });
              }

          }
        btnUpdate.css({ "display": "none" });
   

        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {


     
        if (!validateForm("frmCity")) {
            return;

        }
        var BranchId = 0;
        var Id = m_TaxId;
        var Rate = $("#txtRate").val();
        if ($.trim(Rate) == "") {
            $("#txtRate").focus();

            return;
        }
        var Description = $("#txtDescription").val();
        var DrAcc = $("#ddlDrAccount").val();
        var CrAcc = $("#ddlCrAccount").val();
       
       

        

        var DrVat = false;
        var DrVatAcc = 0;
        if ($('#chkDrVat').is(":checked")) {
            DrVat = true;
            DrVatAcc = $("#ddlVatDr").val();
            if (DrVatAcc == 0) {
                alert("Choose Account to debit for VAT");
                $("#txtddlVatDr").focus();
                return;
            }
        }

        var CrVat = false;
        var CrVatAcc = 0;
        if ($('#chkCrVat').is(":checked")) {
            CrVat = true;

            CrVatAcc = $("#ddlVatCr").val();
            if (CrVatAcc == 0) {
                alert("Choose Account to credit for VAT");
                $("#txtddlVatCr").focus();
                return;
            }
        }



        var Surchrg = false;
        var SurchargeValue = 0;
        var DrSurAcc = 0;
        var CrSurAcc = 0;
        if ($('#chkSur').is(":checked")) {
            Surchrg = true;
            SurchargeValue = $("#txtSurCharge").val();
            DrSurAcc = $("#ddlSurDr").val();
            CrSurAcc = $("#ddlSurCr").val();

            if ($("#txtSurCharge").val() == 0)
            {
                alert("Enter Surcharge Percentage");
                $("#txtSurCharge").focus();
                return;

            }

            if (DrSurAcc == 0) {
                alert("Choose Account to debit for surcharge");
                $("#txtddlSurDr").focus();
                return;
            }

            if (CrSurAcc == 0) {
                alert("Choose Account to credit for surcharge");
                $("#txtddlSurCr").focus();
                return;
            }

        }
      

        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
            $.uiLock('');

        }

        BranchId = $("#ddlBranch").val();
        if (BranchId == "0") {
            alert("Choose Branch");
        }

        $.ajax({
            type: "POST",
            data: '{"TaxId":"' + Id + '", "TaxRate": "' + Rate + '","Description": "' + Description + '","DrAcc": "' + DrAcc + '","CrAcc": "' + CrAcc + '","DrVat": "' + DrVat + '","CrVat": "' + CrVat + '","DrVatAcc": "' + DrVatAcc + '","CrVatAcc": "' + CrVatAcc + '","ChkSur": "' + Surchrg + '","SurVal": "' + SurchargeValue + '","DrSur": "' + DrSurAcc + '","CrSur": "' + CrSurAcc + '","IsActive": "' + IsActive + '","BranchId": "' + BranchId + '"}',
            url: "managetaxes.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == -11) {
                    alert("You don't have permission to perform this action..Consult Admin Department.");
                    return;
                }
                if (obj.Status == -1) {

                    alert("Insertion Failed.Tax with duplicate Rate already exists.");
                    return;
                }

                if (Id == "-1") {
                    ResetControls();
                  //  jQuery("#jQGridDemo").jqGrid('addRowData', obj.Tax.Tax_ID, obj.Tax, "last");
                    BindGrid();
                    alert("Tax is added successfully.");
                }
                else {
                    ResetControls();
                    //var myGrid = $("#jQGridDemo");
                    //var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                    BindGrid();

                   // myGrid.jqGrid('setRowData', selRowId, obj.Tax);
                    alert("Tax is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {

        $("#txtSurCharge").attr("disabled", "disabled");


        $("#ddlDrAccount").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });

        $("#ddlVatDr").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 197,
            DefaultValue: 0,
            Godown: 0
        });


        $("#ddlCrAccount").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });

        $("#ddlVatCr").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 197,
            DefaultValue: 0,
            Godown: 0
        });


        $("#ddlSurCr").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });


        $("#ddlSurDr").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });



        $("#txtddlVatDr").attr("readonly", "readonly");
        $("#txtddlVatCr").attr("readonly", "readonly");


        $("#ddlVatDr").attr("disabled", "disabled");
        $("#ddlVatCr").attr("disabled", "disabled");
        $("#chkSur").prop("checked", false);
        $("#chkDrVat").prop("checked", false);
        $("#chkIsActive").prop("checked", false);
        $("#chkCrVat").prop("checked", false);
        BindGrid();


        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {
                    $("#btnAdd").show();
                    $("#btnAdd").click(
                    function () {


                        m_TaxId = -1;
                        InsertUpdate();
                    }
                    );
                }
                else if (arrRole[i] == "3") {
                    $("#btnUpdate").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Tax is selected to Edit");
                return;
            }

            InsertUpdate();
        }
        );

                }
                else if (arrRole[i] == "2") {
                    $("#btnDelete").show();
                    $("#btnDelete").click(
      function () {

          var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
          if ($.trim(SelectedRow) == "") {
              alert("No Tax is selected to Delete");
              return;
          }

          var TaxId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Tax_ID')
          if (confirm("Are You sure to delete this record")) {
              $.uiLock('');

              $.ajax({
                  type: "POST",
                  data: '{"TaxId":"' + TaxId + '"}',
                  url: "managetaxes.aspx/Delete",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      var obj = jQuery.parseJSON(msg.d);
                      if (obj.status == -10) {
                          alert("You don't have permission to perform this action..Consult Admin Department.");
                          return;
                      }

                      BindGrid();
                      ResetControls();
                      alert("Tax is Deleted successfully.");

                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {
                      $.uiUnlock();
                  }
              });


          }


      }
      );

                }
            }
        }


        $("#chkDrVat").change(function () {

            if ($('#chkDrVat').is(":checked")) {
                $("#txtddlVatDr").removeAttr("readonly");
            }
            else {

                $("#txtddlVatDr").attr("readonly", "readonly").val('');

                $("#ddlVatDr").html('<option value="0"></option>');


            }

        });

        $("#chkCrVat").change(function () {

            if ($('#chkCrVat').is(":checked")) {
                $("#txtddlVatCr").removeAttr("readonly");

            }
            else {

                $("#txtddlVatCr").attr("readonly", "readonly").val('');
                $("#ddlVatCr").html('<option value="0"></option>');
            }

        });

        $("#chkSur").change(function () {

            if ($('#chkSur').is(":checked")) {
                $("#txtSurCharge").removeAttr("disabled");
                $("#txtddlSurCr").removeAttr("readonly");
                $("#txtddlSurDr").removeAttr("readonly");
            }
            else {
                $("#txtSurCharge").val("0");
                $("#txtSurCharge").attr("disabled", "disabled");

                $("#txtddlSurDr").attr("readonly", "readonly").val('');
                $("#ddlSurDr").html('<option value="0"></option>');

                $("#txtddlSurCr").attr("readonly", "readonly").val('');
                $("#ddlSurCr").html('<option value="0"></option>');
            }
        });



        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );

        $("#ddlBranch").change(function () {

           //var Type = $("#ddlBranch").val();
            var BranchId = $("#ddlBranch").val();
            if (BranchId == "0") {
                alert("Choose Branch");
            }
             
                jQuery("#jQGridDemo").GridUnload();
                BindGrid();             

        });

    }
    );

</script>


            <script type="text/javascript">
                function BindGrid() {
                 var Type = $("#ddlBranch").val();
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageTaxes.ashx?Type='+Type,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Tax Id', 'Tax Rate','Description','DebitTo','CreditTo','VatDr','VatDrCode','VatCr','VatCrCode','ChkSur','Surcharge','SurDrCode','SurCrCode','IsActive','a','a','a','a','a','a'],
  


                        colModel: [
                                    { name: 'Tax_ID', key: true, index: 'Tax_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Tax_Rate', index: 'Tax_Rate', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Description', index: 'Description', width: 500, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'sDr_AccCode', index: 'sDr_AccCode', width: 400, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'sCr_AccCode', index: 'sCr_AccCode', width: 400, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'VatCode_Dr', index: 'VatCode_Dr', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Dr_VatCode', index: 'Dr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'VatCode_Cr', index: 'VatCode_Cr', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'Cr_VatCode', index: 'Cr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'ChkSur', index: 'ChkSur', width: 150, editable: true, edittype: "checkbox",hidden:true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'SurValue', index: 'SurValue', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Dr_Sur', index: 'Dr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Cr_Sur', index: 'Cr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                      { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                                    { name: 'Dr_AccCode', index: 'Dr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Cr_AccCode', index: 'Cr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'sDr_VatCode', index: 'sDr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'sCr_VatCode', index: 'sCr_VatCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'sDr_Sur', index: 'sDr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'sCr_Sur', index: 'sCr_Sur', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Tax_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Taxes List",

                        editurl: 'handlers/ManageTaxes.ashx',


                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_TaxId = 0;
                    validateForm("detach");
                    var txtRate = $("#txtRate");

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                     $("#btnUpdate").css({ "display": "none" });
                     $("#btnReset").css({ "display": "none" });
                     $("#btnAdd").css({ "display": "none" });
                  

                     for (var i = 0; i < arrRole.length; i++) {

                         if (arrRole[i] == 1) {

                             $("#btnAdd").css({ "display": "block" });
                         }

                         if (arrRole[i] == 3) {


                             m_TaxId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Tax_ID');


                             txtRate.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Tax_Rate'));
                             txtRate.focus();

                             $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));


                             var DrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_AccCode');
                             var sDrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'sDr_AccCode');
                             $("#ddlDrAccount").html("<option selected=selected value='" + DrAccount + "'>" + sDrAccount + "</option>");
                             $("#txtddlDrAccount").val(sDrAccount);


                             var CrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_AccCode');
                             var sCrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'sCr_AccCode');
                             $("#ddlCrAccount").html("<option  selected=selected  value='" + CrAccount + "'>" + sCrAccount + "</option>");
                             $("#txtddlCrAccount").val(sCrAccount);


                           


                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VatCode_Dr') == "true") {
                                 $('#chkDrVat').prop('checked', true);

                                 var DrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_VatCode');
                                 var sDrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'sDr_VatCode');

                                 $("#ddlVatDr").html("<option  selected=selected  value='" + DrVat + "'>" + sDrVat + "</option>");
                                 $("#txtddlVatDr").val(sDrVat);

                                 $("#txtddlVatDr").removeAttr("readonly");
                             }
                             else {
                                 $('#chkDrVat').prop('checked', false);
                                 $("#txtddlVatCr").attr("readonly", "readonly");
                                 $("#ddlVatCr").attr("disabled", "disabled");

                             }
                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VatCode_Cr') == "true") {
                                 $('#chkCrVat').prop('checked', true);
                                 var CrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_VatCode');
                                 var sCrVat = $('#jQGridDemo').jqGrid('getCell', rowid, 'sCr_VatCode');

                                 $("#ddlVatCr").html("<option  selected=selected  value='" + CrVat + "'>" + sCrVat + "</option>");
                                 $("#txtddlVatCr").val(sCrVat);
                                 $("#txtddlVatCr").removeAttr("readonly");
                             }
                             else {
                                 $('#chkCrVat').prop('checked', false);
                                 $("#txtddlVatCr").attr("readonly", "readonly");
                                 $("#ddlVatCr").attr("disabled", "disabled");
                             }


                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'ChkSur') == "true") {
                                 $('#chkSur').prop('checked', true);
                                 $("#txtSurCharge").removeAttr("disabled");

                                 var CrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_Sur');
                                 var sCrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'sCr_Sur');


                                 $("#ddlSurCr").html("<option  selected=selected  value='" + CrSur + "'>" + sCrSur + "</option>");
                                 $("#txtddlSurCr").val(sCrSur);

                                 //$("#ddlIMSubItem").html("<option selected=selected value='" + obj.ItemOptions.Master_Code + "'>" + obj.ItemOptions.MasterItemName + "</option>");
                                 //$("#txtddlIMSubItem").val(obj.ItemOptions.MasterItemName);



                                 var DrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_Sur');
                                 var sDrSur = $('#jQGridDemo').jqGrid('getCell', rowid, 'sDr_Sur');
                                 $("#ddlSurDr").html("<option  selected=selected  value='" + DrSur + "'>" + sDrSur + "</option>");
                                 $("#txtddlSurDr").val(sDrSur);

                                 $("#txtddlSurCr").removeAttr("readonly");
                                 $("#txtddlSurDr").removeAttr("readonly");
                             }
                             else {
                                 $('#chkSur').prop('checked', false);

                                 $("#txtddlSurDr").attr("readonly", "readonly");
                                 $("#txtddlSurCr").attr("readonly", "readonly");
                                 $("#ddlSurDr").attr("disabled", "disabled");
                                 $("#ddlSurCr").attr("disabled", "disabled");

                             }


                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                 $('#chkIsActive').prop('checked', true);
                             }
                             else {
                                 $('#chkIsActive').prop('checked', false);

                             }
                             $("#txtSurCharge").val($('#jQGridDemo').jqGrid('getCell', rowid, 'SurValue'));


                             $("#btnAdd").css({ "display": "none" });
                             $("#btnUpdate").css({ "display": "block" });
                             $("#btnReset").css({ "display": "block" });

                         }

                     }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '650');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


            </script>


</asp:Content>

