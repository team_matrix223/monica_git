﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managenotification.aspx.cs" Inherits="managenotification" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
<script language="javascript" type="text/javascript">


    </script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>

   <div class="right_col" role="main">

         
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Notification</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Notification</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                     <tr><td class="headings" >Notification1:</td><td > <textarea  id= "txtNot1" style="width:300px"></textarea> </td></tr>
                      <tr><td class="headings" >Notification2:</td><td > <textarea id="txtNot2" style ="width:300px"></textarea></td></tr>
                      <tr><td class="headings" >Declaration:</td><td >  <textarea id = "txtDecl" style="width:300px"></textarea></td></tr>

                       <tr><td class="headings" >Range:</td><td >  <input type="text"  name="txtNot1"  data-index="1" id="txtRange" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >Division:</td><td >  <input type="text"  name="txtNot2"   data-index="1" id="txtDivision" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >CommissionRate:</td><td >  <input type="text"  name="txtDec"   data-index="1" id="txtCRate" style="width: 300px"/></td></tr>

                       <tr><td class="headings" >TINNO:</td><td >  <input type="text"  name="txtNot1" class=   data-index="1" id="txtTinNo" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >PANNO:</td><td >  <input type="text"  name="txtNot2" class=   data-index="1" id="txtPanNo" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >C.E. Regn No:</td><td >  <input type="text"  name="txtDec" class=   data-index="1" id="txtCEReg" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >Branch:</td><td>
                      <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:300px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList>
                      </td></tr>
                      
               
                                            <tr>
                                             <td></td>
                                            <td>
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add  </div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-trash m-right-xs"></i> Update  </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" >
<i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

<link href="semantic.css" rel="stylesheet" type="text/css" />
 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    
   <script type="text/javascript" src="js/SearchPlugin.js"></script> 


   <script type="text/javascript">
       $(document).ready(
       function () {


           GetAll();
           $("#btnAdd").click(
           function () {
               InsertUpdate();
           }
           );


           function InsertUpdate() {

               if (!validateForm("frmCity")) {
                   return;
               }

               var Notification1 = $("#txtNot1").val();


               var Notification2 = $("#txtNot2").val();
               var Declaration = $("#txtDecl").val();


               var Range = $("#txtRange").val();


               var Division = $("#txtDivision").val();
               var CommissionRate = $("#txtCRate").val();

               var TINNO = $("#txtTinNo").val();


               var PANNO = $("#txtPanNo").val();
               var CERegn = $("#txtCEReg").val();
               var BranchId = $("#<%=ddlBranch.ClientID %>").val();


               $.ajax({
                   type: "POST",
                   data: '{"Notification1":"' + Notification1 + '", "Notification2": "' + Notification2 + '","Declaration": "' + Declaration + '","Range":"' + Range + '","Division":"' + Division + '","CommissionRate":"' + CommissionRate + '","TINNO":"' + TINNO + '","PANNO":"' + PANNO + '","CERegn":"' + CERegn + '","BranchId":"'+BranchId+'"}',
                   url: "managenotification.aspx/Insert",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       alert("Notification Saved Successfully");





                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }


           function GetAll() {

               $.ajax({
                   type: "POST",
                   data: '',
                   url: "managenotification.aspx/FillSettings",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       $("#txtNot1").val(obj.setttingData.Notification1);
                       $("#txtNot2").val(obj.setttingData.Notification2);
                       $("#txtDecl").val(obj.setttingData.Declaration);
                       $("#txtRange").val(obj.setttingData.Range);
                       $("#txtDivision").val(obj.setttingData.Division);
                       $("#txtCRate").val(obj.setttingData.CommissionRate);
                       $("#txtTinNo").val(obj.setttingData.TINNO);
                       $("#txtPanNo").val(obj.setttingData.PANNO);
                       $("#txtCEReg").val(obj.setttingData.CERegn);
                       $("#<%=ddlBranch.ClientID %>").val(obj.setttingData.BranchId);







                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();

                   }

               });
           }



       }
       );
   </script>
   


            





</asp:Content>

