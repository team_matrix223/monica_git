﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageitemissuereceive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdntodaydate.Value = DateTime.Now.ToShortDateString();
        if (!IsPostBack)
        {
           
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }

    }

    [WebMethod]
    public static string GetById(Int32 RefNo)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();


        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        List<ItemIssue> TransferDetail = new ItemIssueBLL().GetByRefNo(RefNo, BranchId);
        var JsonData = new
        {

            TransferDetail = TransferDetail
        };
        return ser.Serialize(JsonData);


    }

    [WebMethod]
    public static string InsertUpdate(ItemIssue[] objItemIssue, int RefNo, int GodownId, DateTime Date, int GodownTo)
    {




        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();

        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Item_Name");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Stock");
        DataRow dr;



        foreach (var item in objItemIssue)
        {
            dr = dt.NewRow();

            dr["Item_Code"] = item.Item_Code;
            dr["Item_Name"] = item.Item_Name;
            dr["Qty"] = item.Qty;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = item.Amount;
            dr["Stock"] = item.Stock;
            dt.Rows.Add(dr);


        }

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;
        Status = new ItemIssueBLL().InsertUpdate(Date, GodownId, BranchId, RefNo, dt, UserNo, GodownTo);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }





    [WebMethod]
    public static string Delete(Int32 RefNo)
    {



        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int Status = 0;
        Status = new ItemIssueBLL().Delete(RefNo, BranchId);
        var JsonData = new
        {

            Status = Status
        };
        return ser.Serialize(JsonData);
    }
}