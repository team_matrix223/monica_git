﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class chnagepassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        User objUser = new User();
      
        objUser.User_ID = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.EmployeeName].Value);
        objUser.UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objUser.UserPWD = txtOldPassword.Value;
        int result = Convert.ToInt32(new UserBLL().UserLoginCheck(objUser));

        if (result <= 0)
        {
            Response.Write("<script>alert('Invalid Old Password');</script>");
        }
        else
        {
            objUser.UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
            objUser.UserPWD = txtNewPassword.Value;
            string str = new UserBLL().ChangePassword(objUser);
            if (str == "1")
            {
                Response.Write("<script>alert('Password Changed Successfully');</script>");
            }
            else
            {
                Response.Write("<script>alert('Operation Failed. Please try again later');</script>");
            }

        }



    }
}