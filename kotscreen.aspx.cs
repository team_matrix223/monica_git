﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
 

public partial class kotscreen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }
        ltDepartment.Text = HttpContext.Current.Request.Cookies["ck"].Values[0]; 


    }
    [WebMethod]
    public static Processing[] FetchProducts()
    {
       
        string DepartmentName = HttpContext.Current.Request.Cookies["ck"].Values[0];
        var data = new ProcessingBLL().GetAll(DepartmentName, "New");
        return data.ToArray();
    }


    [WebMethod]
    public static Processing[] GetNewProcessingItems(Processing[] data)
    {
        string DepartmentName = HttpContext.Current.Request.Cookies["ck"].Values[0];
       
        DataTable dt = new DataTable();
        dt.Columns.Add("ProcessingId");
        DataRow dr;
           
        foreach (var item in data)
        {
            dr = dt.NewRow();
            dr["ProcessingId"] = item.ProcessingId;
            dt.Rows.Add(dr);  
        }

        var products = new ProcessingBLL().GetNewProcessingItems(dt,DepartmentName);
        return products.ToArray();
    }

    [WebMethod]
    public static string  UpdateStatus(int ProcessingId,string Status)
    {
        new ProcessingBLL().UpdateProcessingStatus(Status, ProcessingId);
        return "1";

    }

}