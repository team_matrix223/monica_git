﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class ChallanToBill : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
            BindCreditCustomers();
            hdnDate.Value = DateTime.Now.ToShortDateString();
        }
    }
    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string GetChallanDetail(DateTime DateFrom, DateTime DateTo, string CustomerId)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<ChallanMaster> lstBills = new ChallanBLL().GetChallanBYCustomer(DateFrom, DateTo, CustomerId, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetChallanValue(string arrbillnowprefix)
    {
      
        string[] BIllNowprefix = arrbillnowprefix.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("BIllNowprefix");

        for (int i = 0; i < BIllNowprefix.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["BIllNowprefix"] = BIllNowprefix[i];
            dt.Rows.Add(dr);

        }
       decimal TotablBillValue = 0;
       decimal TotalDiscount = 0;
       decimal TOtalTax = 0;
       decimal TotalNetAmount = 0;
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        List<ChallanDetail> lstBills = new ChallanBLL().GetChallanDetail(dt, Branch, out TotablBillValue, out TotalDiscount, out  TOtalTax, out TotalNetAmount);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills,
            TotBill = TotablBillValue,
            TotDis = TotalDiscount,
            TotTa = TOtalTax,
            TotNe = TotalNetAmount,



        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string ConvertChallanTOBill(string arrbillnowprefix, decimal BillValue, decimal Tax, decimal Discount, decimal NetAmount, decimal Round_Amount, int Customer_ID, string CustomerName, string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden,DateTime BillDate)
    {
        Int32 UserNO = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        string[] BIllNowprefix = arrbillnowprefix.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("BIllNowprefix");

        for (int i = 0; i < BIllNowprefix.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["BIllNowprefix"] = BIllNowprefix[i];
            dt.Rows.Add(dr);

        }

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");

        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        ChallanMaster objChallan = new ChallanMaster();
        Int32 Status = new ChallanBLL().ConvertChallanToBill(dt, Branch, BillValue, Tax, Discount, NetAmount, UserNO, Round_Amount, Customer_ID, CustomerName, dt1, objChallan, BillDate);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            status = Status,
            BNF = objChallan.BillNowPrefix,


        };
        return ser.Serialize(JsonData);

    }

}