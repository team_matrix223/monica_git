﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="screen1.aspx.cs" Inherits="screen1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


  <meta name="viewport" content="width=device-width, initial-scale=1,  maximum-scale=1.0, user-scalable=no" />
    <title>Super Store Billing</title>
    <script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>
    <style type="text/css">
        .box
        {
            float: left;
            border: solid 1px silver;
            margin: 1px;
            padding: 2px;
            cursor: pointer;
            width:140px;
            text-align:center;
            height:115px;
            font-size:11px;
        }
    </style>
    <script language="javscript" type="text/javascript">

      

        var m_ItemId = 0;
        var m_ItemName = "";
        var m_Qty = 0;
        var m_Price = 0;


        var ProductCollection = [];
        function clsproduct() {
            this.ItemId = 0;
            this.ItemName = "";
            this.Qty = 0;
            this.Price = 0;

        }

        function Bindtr() {

            var DiscountAmt = 0;
            var TaxAmt = 0;
            var Total = 0;
            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            for (var i = 0; i < ProductCollection.length; i++) {


                var tr = "<tr><td >" + ProductCollection[i]["ItemId"] + "</td><td >" + ProductCollection[i]["ItemName"] + "</td><td><div id='btnMinus'>-</div> " + ProductCollection[i]["Qty"] + " <div id='btnPlus' >+</div> </td><td>" + ProductCollection[i]["Price"] + "</td><td><i id='dvClose'><img src='images/remove.png'/></i> </td></tr>";
                $("#tbProductInfo").append(tr);
                var fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

                Total =  Total+ fPrice;
            }

           
            $("div[id='dvsbtotal']").html(" Rs. " + Total);
            if (Number(DiscountAmt) == 0) {
                $("#trDiscount").css({ "display": "none" });
            }
            else {
                $("#trDiscount").css({ "display": "block" });
            }
            if (Number(TaxAmt) == 0) {
                $("#trTax").css({ "display": "none" });
            }
            else {
                $("#trTax").css({ "display": "block" });
            }
            $("div[id='dvdiscount']").html(" Rs. " + DiscountAmt);
            $("div[id='dvTax']").html(" Rs. " + TaxAmt);
            $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total) + Number(TaxAmt) - Number(DiscountAmt)));
        }

        function addToList(ProductId, Name, Price) {
            m_ItemId = ProductId;
            m_Price = Price;
            m_ItemName = Name;
            m_Qty = 1;
            var item = $.grep(ProductCollection, function (item) {
                return item.ItemId == m_ItemId;
            });

            if (item.length) {

                alert("Product Alredy Added in shop list");

                return;

            }


            TO = new clsproduct();

            TO.ItemId = m_ItemId;
            TO.ItemName = m_ItemName;
            TO.Qty = 1;
            TO.Price = m_Price;

            ProductCollection.push(TO);
            Bindtr();


           
              


        }



        function Search(CatId, Keyword) {

  
            $.ajax({
                type: "POST",
                data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
                url: "screen1.aspx/AdvancedSearch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#products").html(obj.productData);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );


        }

//........................................

        $(document).on("click", "#btnPlus", function (event) {


            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];

            var Mode = "Plus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];
            var fQty = Number(Qty) + 1;
          

         
          
            ProductCollection[RowIndex]["Qty"] = fQty;

            Bindtr();

        });


        $(document).on("click", "#btnMinus", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];
            var Mode = "Minus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];

            var fQty = Number(Qty) - 1;
            ProductCollection[RowIndex]["Qty"] = fQty;
            if (fQty == "0") {
                ProductCollection.splice(RowIndex, 1);
            }   

            Bindtr();


        });



//............................................
      
      
      
      
        $(document).ready(
        function () {


            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {

                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");

                }

            });





            $("#txtSearch").keyup(
            function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {


                    var Keyword = $("#txtSearch");
                    if (Keyword.val().trim() != "") {

                        Search(0, Keyword.val());
                    }
                    else {
                        Keyword.focus();
                    }


                }

            }

            );

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen1.aspx/BindCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#categories").html(obj.categoryData);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );




            $("#btnSearch").click(
        function () {

            var Keyword = $("#txtSearch");
            if (Keyword.val().trim() != "") {

                Search(0, Keyword.val());
            }
            else {
                Keyword.focus();
            }

        }
        );










        });
    
    </script>
</head>
<body>
 <a id="open-left">Open right</a>
<div id="right-slidebar" off-canvas="off-canvas-2 right push" class="transition-500">
        <input type="checkbox" />
        <input type="checkbox" />
<input type="checkbox" />
<input type="checkbox" />

		</div>

    <div>
        <table>
            <tr>
                <td colspan="100%" style="border-bottom: dotted 1px silver; padding-bottom: 10px">
                    <div id="categories">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:450px;vertical-align:top">

                                <table class="tablesorter" style = "width: 100%"  id="tbProductInfo"  cellspacing="0"> 
			<thead> 
				<tr> 
   					  <th style="width:100px;">
                                            ProductId
                                        </th>
                                        <th  style="width:200px" >
                                          Name
                                        </th>
                                       
                                        <th  style="width:150px" >
                                            Qty
                                        </th>
                                     
                                         <th  style="width:100px" >
                                            Price
                                        </th>
                                        <th  style="width:100px">Delete</th>
				</tr> 
			</thead> 
			<tbody>
       
       
			</tbody> 
			</table>
                </td>
                <td>
                <table>
              <tr>
              <td>  <div id="products" style="max-height:400px;overflow-y:scroll;">
                    </div></td>
              </tr>


              <tr>
              <td><table>
                        <tr>
                            <td>
                                <input type="text" id="txtSearch" width="50%" />
                            </td>
                            <td>
                                <input type="button" id="btnSearch" value="Search"/>
                            </td>
                        </tr>
                    </table></td>
              </tr>

              <tr>
              
                <td ><table>
                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr><td style="width:60%">Gross Amount:</td><td style="width:100px"><div id ="dvsbtotal"></div></td></tr>

                     <tr ><td colspan="100%"><div id="trDiscount"><table><tr>
                     <td  style="width:60%">Discount:</td><td style="width:100px"><div id ="dvdiscount"></div></td>
                     </tr></table></div></td></tr>
                        <tr ><td colspan="100%"><div id="trTax"><table><tr>
                        <td  style="width:60%">Tax Amount:</td><td style="width:100px"><div id ="dvTax"></div></td>
                        </tr></table></div></td></tr>
                     <tr><td  style="width:60%">Net Amount:</td><td style="width:100px"><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                
                        
                     </tr>

                     </table></td>
              
              </tr>
                </table>

                  
                  
                    
                </td>
            </tr>
        </table>


    </div>


    	<link rel="stylesheet" href="slidebars.css">
         	<script src="slidebars-extended.js"></script>
		<script>
		    (function ($) {
		        // Create a new instance of Slidebars
		        var controller = new slidebars();

		        // Test events
		        $(controller.events).on('init', function () {
		            console.log('Slidebars initialized');
		        });

		        $(controller.events).on('opening', function (event, id) {
		            console.log('Opening a Slidebar');

		            if (id) {
		                console.log(id);
		            }

		            if (id === 'off-canvas-5') {
		                console.log('Opening top');
		            }
		        });

		        // Initialize Slidebars
		        controller.init();

		        // Dummy content
		        var content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla hendrerit volutpat neque. Nam a ipsum mauris. Maecenas nisi elit, dapibus sit amet eleifend a, vulputate vel ligula. Phasellus in sagittis nulla. Ut tincidunt sapien in metus sollicitudin viverra. Suspendisse potenti. In ac sapien nulla. Fusce vel augue eu nisi blandit accumsan sit amet ut tellus. Fusce sit amet tortor lobortis, suscipit magna a, rutrum est. Sed imperdiet urna id magna consequat blandit. Curabitur sodales feugiat est, eget mattis enim consectetur ac. In efficitur luctus odio aliquam suscipit. Ut commodo enim sit amet aliquam sodales. Nullam porta lorem vel molestie aliquet. Praesent eget ligula aliquet, semper sapien eu, scelerisque massa. Integer pharetra lectus tellus.</p><p>Morbi sed iaculis lacus, ac feugiat mauris. Nunc nec sodales ex, a condimentum dolor. Curabitur erat massa, lacinia quis pellentesque in, pretium non lacus. Mauris luctus neque a elit gravida, sed bibendum neque pharetra. Curabitur tortor tellus, sollicitudin a mauris at, luctus feugiat nibh. Integer vel feugiat odio. Donec arcu lacus, pretium sed posuere nec, laoreet sed urna. Mauris maximus dolor et posuere volutpat. Aliquam erat volutpat. Duis tempor cursus arcu, quis pretium dolor scelerisque interdum. Cras enim ante, consectetur ut blandit eget, posuere at mauris. Proin in turpis egestas nunc fringilla scelerisque non ac enim. Sed porta elit ut lorem elementum, id dignissim erat lacinia.</p>';

		        // Create a few Slidebars
		        controller.create('off-canvas-5', 'top', 'shift', content);
		        controller.create('off-canvas-6', 'right', 'reveal', '<p>This is the content to be inserted.</p>');
		        controller.create('off-canvas-7', 'bottom', 'reveal', content);

		        // Destroy a Slidebar
		        $('#destroy-top').on('click', function () {
		            controller.destroy('off-canvas-1', function () {
		                alert('Boom!');
		            });
		        });





		        $("#dvProductForm").click(
       function () {


           controller.close();
       }
       );

		        // Open Slidebars
		        $('#open-top').on('click', function () {
		            controller.open('off-canvas-5');
		        });

		        $('#open-left').on('click', function () {
		            controller.open('off-canvas-2');
		        });

		        $('#open-bottom').on('click', function () {
		            controller.open('off-canvas-3');
		        });

		        $('#open-left').on('click', function () {
		            controller.open('off-canvas-4');
		        });

		        $('#close-top').on('click', function () {
		            controller.close('off-canvas-5', function () {
		                console.log('Callback function on close');
		            });
		        });

		        $('#close-any').on('click', function () {
		            controller.close();
		        });

		        $('#check-top').on('click', function () {
		            console.log(controller.active('off-canvas-5'));
		        });

		        $('#check-any').on('click', function () {
		            console.log(controller.active('slidebar'));
		        });

		        $('#exit').on('click', function () {
		            controller.exit(function () {
		                alert('Bye');
		            });
		        });

		        $('#init').on('click', function () {
		            controller.init();
		        });

		        $('#get').on('click', function () {
		            var getWhat = prompt('Enter a Slidebar id or leave blank for array of Slidebar ids.', '');

		            console.log(controller.get(getWhat));
		        });

		        $('#modify-bottom').on('click', function () {
		            controller.modify('off-canvas-5', 'bottom', 'overlay');
		        });

		        $('#modify-top').on('click', function () {
		            controller.modify('off-canvas-5', 'top', 'reveal', function () {
		                controller.open('off-canvas-5');
		            });
		        });
		    })(jQuery);
		</script>


</body>
</html>
