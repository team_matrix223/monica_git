﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managesuppliers.aspx.cs" Inherits="managesuppliers" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">
       var m_SupplierId = 0;
       function ApplyRoles(Roles) {


           $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

       function GetVMResponse(Id, Title, IsActive, Status, Type) {
           $("#dvVMDialog").dialog("close");

           var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
           if (Type == "Area") {

               $("#<%=ddlArea.ClientID %>").append(opt);

           }

           if (Type == "City") {

               $("#<%=ddlCity.ClientID %>").append(opt);

           }

           if (Type == "State") {

               $("#<%=ddlState.ClientID %>").append(opt);

           }



       }

       function OpenVMDialog(Type) {


           $.ajax({
               type: "POST",
               data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
               url: "managearea.aspx/LoadUserControl",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   $("#dvVMDialog").remove();
                   $("body").append("<div id='dvVMDialog'/>");
                   $("#dvVMDialog").html(msg.d).dialog({ modal: true });
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {

               }
           });

       }



       function ResetControls() {

           $("input[name='company']").prop("checked", false);

           m_SupplierId = -1;
           var txtName = $("#txtName");
           var btnAdd = $("#btnAdd");
           var btnUpdate = $("#btnUpdate");
           txtName.focus().val("");
           $("#chkIsActive").prop("checked", "checked");
           
           $("#txtAddress1").val("");
           $("#ddlPrefix").val("");
           $("#txtAddress2").val("");
           $("#txtCST").val("");
           $("#txtCSTDate").val("");
           $("#txtTIN").val("");
           $("#txtContactPerson").val("");
           $("#txtContactNumber").val("");
           $("#txtTOT").val("");


          
           $("#<%=ddlCity.ClientID%> option").removeAttr("selected");
           $("#<%=ddlArea.ClientID%> option").removeAttr("selected");
           $("#<%=ddlState.ClientID%> option").removeAttr("selected");
           var arrRole = [];
           arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


           for (var i = 0; i < arrRole.length; i++) {

               if (arrRole[i] == "1") {

                   $("#btnAdd").css({ "display": "block" });
               }

           }
   

           btnUpdate.css({ "display": "none" });
   

           $("#btnReset").css({ "display": "none" });
           $("#hdnId").val("0");
           validateForm("detach");
       }



       function TakeMeTop() {
           $("html, body").animate({ scrollTop: 0 }, 500);
       }

       function RefreshGrid() {
           $('#jQGridDemo').trigger('reloadGrid');

       }

       function InsertUpdate() {

           if (!validateForm("frmCity")) {
               return;
           }

           var objSupplier = {};
           var Id = m_SupplierId;
           var Prefix = $("#ddlPrefix").val();
           var Name = $("#txtName").val();
           if ($.trim(Name) == "") {
               $("#txtName").focus();

               return;
           }
           var Address1 = $("#txtAddress1").val();
           var Address2 = $("#txtAddress2").val();
           var City = $("#<%=ddlCity.ClientID%>").val();
           var State = $("#<%=ddlState.ClientID%>").val();
           var Area = $("#<%=ddlArea.ClientID%>").val();
           var CSTNo = $("#txtCST").val();
           var Cstdate = $("#txtCSTDate").val();
           var TinNo = $("#txtTIN").val();
           var TotNo = $("#txtTOT").val();
           var ContctPrsn = $("#txtContactPerson").val();
           var CntctNo = $("#txtContactNumber").val();
           var IsActive = false;

           if ($('#chkIsActive').is(":checked")) {
               IsActive = true;
           }

           var CompaniesId = $('input[name="company"]:checkbox:checked').map(function () {
               return this.value;
           }).get();

        
    
           objSupplier.Supplier_ID = Id;
           objSupplier.Prefix = Prefix;
           objSupplier.Supplier_Name = Name;
           objSupplier.Address_1 = Address1;
           objSupplier.Address_2 = Address2;
           objSupplier.Area_ID = Area;
           objSupplier.City_ID = City;

           objSupplier.State_ID = State;
           objSupplier.CST_No = CSTNo;

           if (Cstdate != "") {
               objSupplier.CST_Date = Cstdate;
           }
           objSupplier.TIN_No = TinNo;
           objSupplier.TOT_No = TotNo;
           objSupplier.Cont_Person = ContctPrsn;
           objSupplier.Cont_No = CntctNo;
           objSupplier.IsActive = IsActive;
           


           var DTO = { 'objSupplier': objSupplier,'CompanyList':''+CompaniesId+''};
           $.uiLock('');
          
           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managesuppliers.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == -1) {

                       alert("Insertion Failed.Supplier with duplicate name already exists.");
                       return;
                   }
                   if (obj.Status == -11) {
                       alert("You don't have permission to perform this action..Consult Admin Department.");
                       return;
                   }
                   if (Id == "-1") {
                       ResetControls();

                       jQuery("#jQGridDemo").jqGrid('addRowData', obj.supplier.Suppier_ID, obj.supplier, "last");

                       alert("Supplier is added successfully.");
                   }
                   else {
                       ResetControls();

                       var myGrid = $("#jQGridDemo");
                       var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                       myGrid.jqGrid('setRowData', selRowId, obj.supplier);
                       alert("Supplier is Updated successfully.");
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }



       $(document).ready(
      function () {

          BindGrid();


          $('#txtCSTDate').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1"
          }, function (start, end, label) {


              console.log(start.toISOString(), end.toISOString(), label);
          });


          ValidateRoles();

          function ValidateRoles() {

              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

              for (var i = 0; i < arrRole.length; i++) {
                  if (arrRole[i] == "1") {
                      $("#btnAdd").show();
                      $("#btnAdd").click(
                    function () {


                        m_SupplierId = -1;
                        InsertUpdate();
                    }
                    );
                  }
                  else if (arrRole[i] == "3") {
                       $("#btnUpdate").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Supplier is selected to Edit");
                return;
                  }

            InsertUpdate();
                  }
        );
                  }

                  else if (arrRole[i] == "2") {


                      $("#btnDelete").show();
                      $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Supplier is selected to Delete");
             return;
         }

         var supplierid = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Supplier_ID')
         if (confirm("Are You sure to delete this record")) {
             $.uiLock('');




             $.ajax({
                 type: "POST",
                 data: '{"SupplierId":"' + supplierid + '"}',
                 url: "managesuppliers.aspx/Delete",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                     if (obj.status == -10) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }
                     if (obj.status == -1) {
                         alert("Deletion Failed. Supplier is in Use.");
                         return
                     }

                     BindGrid();
                     ResetControls();
                     alert("Supplier is Deleted successfully.");




                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     $.uiUnlock();
                 }
             });









         }


     }
     );
                  }
              }
          }


         



        

          $('#txtName').focus();


          $("#btnReset").click(
        function () {

            ResetControls();

        }
        );
      });
   
   
   </script>


    <style type="text/css">
 #tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
 #tblist tr td{text-align:left;padding:2px}
  #tblist tr:nth-child(even){background:#F7F7F7}
 </style>

 <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Suppliers</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Supplier</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                              
                             <tr><td class="headings">Prefix:</td><td>
                             <select id="ddlPrefix" style="width:213px;height:38px" class="validate ddlrequired" >
                     <option value="Ms">M/s</option>
                    
                    
                     <option value="Mr">Mr</option>
                     <option value="Miss">Miss</option><option value="Mrs">Mrs</option></select></td><td class="headings">Name</td><td> <input type="text"  name="txtName" class="form-control validate required alphanumeric"   data-index="1" id="txtName" style="width: 213px"/></td></tr>
                     <tr><td class="headings">Address1:</td><td><textarea class="form-control validate required alphanumeric" id="txtAddress1"  style="width: 213px"></textarea></td><td class="headings">Address2:</td><td><textarea class="form-control" id="txtAddress2"  required></textarea></td></tr>      
                     <tr><td class="headings">Area:</td>
                     <td> 

                     <table>
                     <tr><td><asp:DropDownList style="width:190px;height:35px" class="form-control validate ddlrequired"  
                     ID ="ddlArea" runat="server" ></asp:DropDownList></td>
                     <td>
                     
                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('Area')" style="font-size:25px;padding:10px 0px 0px 3px;cursor:pointer"></span>
                     
                     </td>
                    
                     
                     </tr>

                     </table>
                     
                     
                     
                     
                     </td>
                     
                     
                     <td class="headings">City:</td>
                     
                     
                     
                     <td>
                     <table>
                     <tr>
                     <td>
                     
                      <asp:DropDownList style="width:190px;height:35px" class="form-control validate ddlrequired" 
                       ID ="ddlCity" runat="server" ></asp:DropDownList>
                       </td>
                      <td>
                     
                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')" style="font-size:25px;padding:10px 0px 0px 3px;cursor:pointer"></span>
                     
                     </td>
                    
                     
                     </tr>

                     </table>  
                       
                       
                       </td></tr>         
                     
                     
                     <tr><td class="headings">State:</td><td>
                     <table>
                     <tr>
                     <td>
                       <asp:DropDownList style="width:190px;height:35px"  ID ="ddlState"
                       class="form-control validate ddlrequired" runat="server" ></asp:DropDownList>
                        </td>
                      <td>
                     
                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size:25px;padding:10px 0px 0px 3px;cursor:pointer"></span>
                     
                     </td>
                    
                     
                     </tr>

                     </table>  
                       
                       
                       
                       
                       </td><td></td><td></td></tr>         
                     <tr><td class="headings">CST No:</td><td><input type="text"  name="txtCST" class="form-control validate required alphanumeric"   data-index="1" id="txtCST" style="width: 213px"/></td><td class="headings">CST Date:</td><td><input type="text"  name="txtCSTDate" class="form-control"   data-index="1" id="txtCSTDate" style="width: 213px"/></td></tr>                
                     <tr><td class="headings">TIN No:</td><td> <input type="text"  name="txtTIN" class="form-control"   data-index="1" id="txtTIN" style="width: 213px"/></td><td class="headings">TOT No:</td><td> <input type="text"  name="txtTOT" class="form-control"   data-index="1" id="txtTOT" style="width: 213px"/></td></tr> 
                     <tr><td class="headings">Contact Person:</td><td> <input type="text"  name="txtContactPerson" class="form-control"   data-index="1" id="txtContactPerson" style="width: 213px"/></td><td class="headings">Contact Number:</td><td> <input type="text"  name="txtContactNumber" class="form-control validate valNumber"   data-index="1" id="txtContactNumber" style="width: 213px"/></td></tr>                               
                     <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td><td></td><td></td></tr> 
                             <tr><td class="headings">Companies:</td>
                      
                      <td>
                      <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll">
                      <table style="width:100%" id="tblist">
                      <asp:Literal ID="ltCompanies" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td></tr>
                       
                       
                       
                             <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;"  class="btn btn-primary btn-small" >Add  <i class="fa fa-external-link"></i></div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i> Update  </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" ><i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>    
                                         
                                            </tr>
                                            </table>
                          
                             

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Suppliers</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave"  >
                    
      	          <table id="jQGridDemo">
    </table>

   <div id="btnDelete" style="margin-top:10px;display:none;"  class="btn btn-danger btn-small" >
<i class="fa fa-trash m-right-xs"></i> Delete</div> 
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
               <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageSuppliers.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Supplier Id','Prefix', 'Name','Address1','Address2','Area','City','State','CstNo','CstDate','TinNo','TotNo','ContactPerson','ContactNo','IsActive','UserId','CompanyList'],
                        colModel: [
                                    { name: 'Supplier_ID', key: true, index: 'Supplier_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'Prefix', index: 'Prefix', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Supplier_Name', index: 'Supplier_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Address_1', index: 'Address_1', width: 200, stype: 'text', sortable: true, editable: true,hidden: false , editrules: { required: true } },
                                    { name: 'Address_2', index: 'Address_2', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'Area_ID', index: 'Area_ID', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'City_ID', index: 'City_ID', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                   { name: 'State_ID', index: 'State_ID', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'CST_No', index: 'CST_No', width: 200, stype: 'text', sortable: true, editable: true,hidden: false , editrules: { required: true } },
                                    { name: 'strCD', index: 'strCD', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'TIN_No', index: 'TIN_No', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'TOT_No', index: 'TOT_No', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'Cont_Person', index: 'Cont_Person', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'Cont_No', index: 'Cont_No', width: 200, stype: 'text', sortable: true, editable: true,hidden: true , editrules: { required: true } },
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox",hidden: true , editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'UserId', index: 'UserId', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                           { name: 'CompanyList',hidden:true, index: 'CompanyList', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                
                        
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Supplier_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Suppliers List",

                        editurl: 'handlers/ManageSuppliers.ashx',

                         ignoreCase: true,
                         toolbar: [true, "top"],

                    });



                     var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });


                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_SupplierId = 0;
                    validateForm("detach");
             
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                       $("#btnUpdate").css({ "display": "none" });
                       $("#btnReset").css({ "display": "none" });
                       $("#btnAdd").css({ "display": "none" });
                  

                       for (var i = 0; i < arrRole.length; i++) {

                           if (arrRole[i] == 1) {

                               $("#btnAdd").css({ "display": "block" });
                           }

                           if (arrRole[i] == 3) {


                               m_SupplierId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Supplier_ID');

                               $("input[name='company']").prop("checked", false);


                               var m_CompanyIdList = $('#jQGridDemo').jqGrid('getCell', rowid, 'CompanyList');

                               $("input[name='company']").prop("checked", false);

                               var parts = m_CompanyIdList.split(',');
                               for (var i = 0; i < parts.length; i++) {

                                   $("#chkS_" + parts[i] + "").prop('checked', true);
                               }




                               var div = $('#jQGridDemo').jqGrid('getCell', rowid, 'Prefix')
                               $('#ddlPrefix option[value=' + div + ']').prop('selected', 'selected');
                               $("#txtName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Supplier_Name'));
                               $("#txtAddress1").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address_1'));
                               $("#txtAddress2").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Address_2'));
                               $("#txtCST").val($('#jQGridDemo').jqGrid('getCell', rowid, 'CST_No'));

                               var strCD = $('#jQGridDemo').jqGrid('getCell', rowid, 'strCD');
                               if (strCD == "1/1/1900") {
                                   $("#txtCSTDate").val("");
                               }
                               else {
                                   $("#txtCSTDate").val(strCD);

                               }
                               var City = $('#jQGridDemo').jqGrid('getCell', rowid, 'City_ID');
                               $("#<%=ddlCity.ClientID%> option[value='" + City + "']").prop("selected", true);
                               var Area = $('#jQGridDemo').jqGrid('getCell', rowid, 'Area_ID');
                               $("#<%=ddlArea.ClientID%> option[value='" + Area + "']").prop("selected", true);
                               var State = $('#jQGridDemo').jqGrid('getCell', rowid, 'State_ID');
                               $("#<%=ddlState.ClientID%> option[value='" + State + "']").prop("selected", true);

                               $("#txtTOT").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TOT_No'));
                               $("#txtTIN").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TIN_No'));
                               $("#txtContactPerson").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Cont_Person'));
                               $("#txtContactNumber").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Cont_No'));

                               if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                   $('#chkIsActive').prop('checked', true);
                               }
                               else {
                                   $('#chkIsActive').prop('checked', false);

                               }


                               $("#btnAdd").css({ "display": "none" });
                               $("#btnUpdate").css({ "display": "block" });
                               $("#btnReset").css({ "display": "block" });
                           }

                       }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>

