﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GenerateCookies.aspx.cs" Inherits="GenerateCookies" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:DropDownList ID="ddlDepartments" runat="server">
    <asp:ListItem Text="" Value=""></asp:ListItem>
   
    <asp:ListItem Text="KITCHEN BASE" Value="KITCHEN BASE"></asp:ListItem>
    <asp:ListItem Text="BAKERY" Value="BAKERY"></asp:ListItem>
      <asp:ListItem Text="SNACKS" Value="SNACKS"></asp:ListItem>
  <asp:ListItem Text="NON-FOOD" Value="NON-FOOD"></asp:ListItem>
  <asp:ListItem Text="FOOD" Value="FOOD"></asp:ListItem>
  <asp:ListItem Text="KITCHEN SECTION" Value="KITCHEN SECTION"></asp:ListItem>
  <asp:ListItem Text="M.INDIAN ITEMS" Value="M.INDIAN ITEMS"></asp:ListItem>
  <asp:ListItem Text="Confectionery" Value="Confectionery"></asp:ListItem>
  <asp:ListItem Text="BAR" Value="BAR"></asp:ListItem>
<asp:ListItem Text="DRINK SECTION" Value="DRINK SECTION"></asp:ListItem>
<asp:ListItem Text="BAWARCHI" Value="BAWARCHI"></asp:ListItem>
<asp:ListItem Text="M.IMPORTED ITEMS" Value="M.IMPORTED ITEMS"></asp:ListItem>
 

    </asp:DropDownList>
    
    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
    <asp:Button ID="btnGenerate" runat="server" Text="Generate Cookie" 
            onclick="btnGenerate_Click" />
    </div>
    </form>
</body>
</html>
