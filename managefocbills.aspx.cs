﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class managefocbills : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            hdnDate.Value = DateTime.Now.ToShortDateString();
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();

           

               
                string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

                if (strDate == "")
                {

                    Response.Redirect("index.aspx?DayOpen=Close");
                }

           


        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.FOCBILLING));
     

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string LoadUserControl(string counter)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
            (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }


    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new FOCBillBLL().GetFOCBillByBillNowPrefix(BillNowPrefix, Branch);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindTables()
    {

        string TablesData = new TablesBLL().GetOptions();

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            TableOptions = TablesData

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCategories()
    {
        int CategoryId = 0;
        Settings objSettings = new Settings();
        string catData = new CategoriesBLL().GetCategoriesHTML(objSettings, out CategoryId);
        var JsonData = new
        {
            categoryData = catData,
            setttingData = objSettings,
            CategoryId = CategoryId
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string AdvancedSearch(int CategoryId, string Keyword)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string prodData = new ProductBLL().AdvancedSearch(CategoryId, Keyword.Trim(), Branch);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByItemCode(string ItemCode)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Product objProduct = new Product() { Item_Code = ItemCode };

        new ProductBLL().GetByItemCode(objProduct, Branch);
        var JsonData = new
        {
            productData = objProduct
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

   
    [WebMethod]
    public static string BindBanks()
    {

        string bankData = new BankBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BankOptions = bankData

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdate(string CustomerId, string CustomerName, decimal BillValue, decimal DiscountPer, decimal DiscountAmt, decimal AddTaxAmt, decimal NetAmt, string BillMode, string CreditBank, decimal CashAmt, decimal CreditAmt, decimal CrCardAmt, decimal RoundAmt, int CashCustCode, string CashCustName, int TableNo, decimal SerTax, string Remarks, Int32 OrderNo, string Type, string BillNowPrefix, string itemcodeArr, string qtyArr, string priceArr, string taxArr, string orgsalerateArr,string SurPerArr, string AmountArr, string TaxAmountArr, string SurValArr, string ItemRemarksArr, string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.FOCBILLING));


        string[] arrRoles = sesRoles.Split(',');



        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -11;
        }




        int Status = 0;
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
       

        //User objuser = new User()
        //{
        //    UserNo = UserNo

        //};

        //Int32 status2 = new UserBLL().chkIsLogin(objuser);

        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            Bill_Value = BillValue,
            DiscountPer = DiscountPer,
            Less_Dis_Amount = DiscountAmt,
            Add_Tax_Amount = AddTaxAmt,
            Net_Amount = NetAmt,
            BillMode = BillMode,
            CreditBank = CreditBank,
            UserNO = UserNo,
            Bill_Type = 1,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CrCard_Amount = CrCardAmt,
            Round_Amount = RoundAmt,
            Bill_Printed = false,
            Passing = false,
            CashCust_Code = CashCustCode,
            CashCust_Name = CashCustName,
            Tax_Per = 0,
            R_amount = RoundAmt,
            tableno = TableNo,
            remarks = Remarks,
            servalue = SerTax,
            ReceiviedGRNNo = 0,
            EmpCode = 0,
            OrderNo = OrderNo,
            BranchId = Branch,

        };
        //if (status2 != 0)
        //{
        string[] ItemCode = itemcodeArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] Tax = taxArr.Split(',');
        string[] OrgSaleRate = orgsalerateArr.Split(',');
        string[] SurPer = SurPerArr.Split(',');
        string[] Amount = AmountArr.Split(',');
        string[] TaxAmount = TaxAmountArr.Split(',');
        string[] SurVal = SurValArr.Split(',');
        string[] remarksdata = ItemRemarksArr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("OrgSaleRate");
        dt.Columns.Add("SurPer");
        dt.Columns.Add("SurVal");
        dt.Columns.Add("Remarks");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");

        for (int i = 0; i < ItemCode.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = ItemCode[i];
            dr["Rate"] = Convert.ToDecimal(Price[i]);
            dr["Qty"] = Convert.ToDecimal(Qty[i]);
            dr["Amount"] = Convert.ToDecimal(Amount[i]);
            dr["Tax"] = Convert.ToDecimal(Tax[i]);
            dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
            dr["OrgSaleRate"] = Convert.ToDecimal(OrgSaleRate[i]);
            dr["SurPer"] = Convert.ToDecimal(SurPer[i]);
            dr["SurVal"] = Convert.ToDecimal(SurVal[i]);
            dr["Remarks"] = Convert.ToString(remarksdata[i]);
            dt.Rows.Add(dr);
        }

        for (int j = 0; j < taxdendata.Length; j++)
        {
            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }

        if (objBill.BillNowPrefix == "")
        {
            Status = new FOCBillBLL().Insert(objBill, dt, dt1);
        }
        else
        {
            Status = new FOCBillBLL().Update(objBill, dt, dt1);
        }
      
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Bill = objBill,
            BNF = objBill.BillNowPrefix,
            Status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]

    public static string GetByMobileNo(string MobileNo)
    {
        Customers objCustomers = new Customers()
        {
            Contact_No = MobileNo,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CustomerBLL().GetByMobileNoFOC(objCustomers);
        var JsonData = new
        {
            Customer = objCustomers

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(string BillNowPrefix)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.FOCBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new FOCBillBLL().DeleteBill(objBill);
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    
}