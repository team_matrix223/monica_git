﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebasicsettings.aspx.cs" Inherits="ApplicationSettings_managebasicsettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
      <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }

    </style>
    


    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Basic Settings</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                       
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed"  >
                             <tr>
                                 <td>
                                     <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">

                                          <tr>
                                              <td class="tableheadings" align="right" style="text-align:left;width:20px" colspan="100%">Branch:</td>
                                              </tr>
                                         <tr>
                                              <td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>

                                           <tr><td colspan ="100%"  class="tableheadings" >Setting</td></tr>
                        <tr><td class="headings" align="left" style="text-align:left;width:200px">Service Tax:</td><td align="left" style="text-align:left;width:150px"><input type="checkbox" id="chkServicetax"  data-index="2"  name="chkServicetax" /> <label class="headings">Tax(%)</label><input type="text" id ="txtTax" style="width:80px" /></td></tr> 
                      
                       <tr><td  class="headings" align="left" style="text-align:left;width:200px">ServiceTax on TakeAway</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkServiceTaxOnTake"  data-index="2"  name="chkServiceTaxOnTake" /></td></tr>
                        <tr><td class="headings" align="left" style="text-align:left;width:200px">Home Delivery:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="checkbox" id="chkHomeDel" data-index="2"  name="chkHomeDel" /></td></tr> 
                        <tr><td class="headings" align="left" style="text-align:left;width:200px">Minimum Bill Value:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id ="txtMinimumBill" style="width:100px" /></td></tr> 
                        <tr><td class="headings" align="left" style="text-align:left;width:200px">Delivery Charges:</td><td align="left" style="text-align:left;width:100px" colspan="100%"><input type="text" id ="txtDelCharges" style="width:100px" /></td></tr>          
                       
                                     </table>
                                 </td>
                                  <td valign="top">
                                     <table cellpadding="10" cellspacing="5" border="0"  class="table" style="margin-bottom:20px;border-style:double">

                                          <tr><td colspan ="100%" class="tableheadings">BILL OPTIONS</td></tr>
                       
                                 <tr><td align="left" style="text-align:left;width:200px"><input type="radio"  checked="checked"  id="rdoRetInc" value ="I" name="rdoRetail" >
                    <label class="headings" for="rdoRetInc">Retail Inclusive</label></td>


                         <td align="left" style="text-align:left;width:200px"><input type="radio"  id="rdoRetExc" value="E" name="rdoRetail" >
                    <label class="headings" for="rdoRetExc">Retail Exclusive</label></td>
                       </tr> 

                                 <tr><td align="left" style="text-align:left;width:200px"><input type="radio" checked="checked"   id="rdoVatInc" value ="I" name="rdoVAT" >
                    <label class="headings" for="rdoVatInc">VAT Inclusive</label></td>


                         <td align="left" style="text-align:left;width:200px"><input type="radio"  id="rdoVatExc" value="E" name="rdoVAT" >
                    <label class="headings" for="rdoVatExc">VAT Exclusive</label></td>
                       </tr> 


                                 <tr><td align="left" style="text-align:left;width:200px"><input type="radio" checked="checked" value="I"  id="rdoCSTInc" name="rdoCST" >
                    <label class="headings" for="rdoCSTInc">CST Inclusive</label></td>


                         <td align="left" style="text-align:left;width:200px"><input type="radio" value="E" id="rdoCSTExc" name="rdoCST" >
                    <label class="headings" for="rdoCSTExc">CST Exclusive</label></td>
                       </tr> 
                         

                                     </table>


                                 </td>
                             </tr>
                   
                           
                                
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>



    <link href="<%=CommonFunctions.SiteUrl()%>js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/grid.locale-en.js" type="text/javascript"></script>
     <link href="<%=CommonFunctions.SiteUrl()%>js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="<%=CommonFunctions.SiteUrl()%>js/jquery-ui.js"></script>
    <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="<%=CommonFunctions.SiteUrl()%>js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">




       function ResetControls() {
           $("#ddlPGodown").val("0");
           $("#ddlPReorderLevel").val("0");
           $("#ddlPReorderCal").val("0");
           $('#chkServicetax').prop('checked', false);
           $('#chkServiceTaxOnTake').prop('checked', false);
           $('#chkHomeDel').prop('checked', false);
           $('#rdoRetInc').prop('checked', false);
           $('#rdoVatInc').prop('checked', false);
           $('#rdoCSTInc').prop('checked', false);
           $('#rdoRetExc').prop('checked', false);
           $('#rdoVatExc').prop('checked', false);
           $('#rdoCSTExc').prop('checked', false);
           $("#txtMinimumBill").val("");
           $("#txtDelCharges").val("");
           $("#txtTax").val("");

       }




       function InsertUpdate() {
           var ServicetaxonTakeAway = false;
           var BranchId = 0;
           var TaxRate = 0;
           var BillValue = 0;
           var Deliverycharges = 0;
           var objSettings = {};
           var Servicetax = false;
           if ($('#chkServicetax').is(":checked")) {
               Servicetax = true;
                TaxRate = $("#txtTax").val();
           }

            if ($('#chkServiceTaxOnTake').is(":checked")) {
                ServicetaxonTakeAway = true;
              
            }
           var Homedelivery = false;
           if ($('#chkHomeDel').is(":checked")) {
               Homedelivery = true;
               BillValue = $("#txtMinimumBill").val();
               Deliverycharges = $("#txtDelCharges").val();
           }
          

           var Retail = "";
           if ($('#rdoRetInc').prop('checked') == true) {
               Retail = "I";
           }
           else {
               Retail = "E";
           }


           var Vat = "";
           if ($('#rdoVatInc').prop('checked') == true) {
               Vat = "I";
           }
           else {
               Vat = "E";
           }


           var Cst = "";
           if ($('#rdoCSTInc').prop('checked') == true) {
               Cst = "I";
           }
           else {
               Cst = "E";
           }
           BranchId = $("#ddlBranch").val();
           if (BranchId == "0")
           {
               alert("Choose Branch");
           }
           objSettings.service_tax = Servicetax;
           objSettings.tax_per = TaxRate;
           objSettings.homedel_charges = Homedelivery;
           objSettings.min_bill_value = BillValue;
           objSettings.del_charges = Deliverycharges;
           objSettings.retail_bill = Retail;
           objSettings.vat_bill = Vat;
           objSettings.cst_bill = Cst;
           objSettings.BranchId = BranchId
           objSettings.AlloServicetax_TakeAway = ServicetaxonTakeAway

           var DTO = { 'objSettings': objSettings };

           $.uiLock('');

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "managebasicsettings.aspx/Insert",
               data: JSON.stringify(DTO),
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);


                   if (obj.Status == 1) {

                       alert("Setting Applied Successfully.");
                     
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
    function () {

        $("#txtTax").prop('disabled', true);
        $("#txtMinimumBill").prop('disabled', true);
        $("#txtDelCharges").prop('disabled', true);


        $("#ddlBranch").change(function () {

            var Type = $("#ddlBranch").val();
            $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" }',
                url: "managebasicsettings.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    var servicetax = obj.setttingData.service_tax;
                    if (servicetax == true) {

                        $('#chkServicetax').prop('checked', true);
                        $("#txtTax").val(obj.setttingData.tax_per);
                        $("#txtTax").prop('disabled', false);
                    }
                    else {
                        $('#chkServicetax').prop('checked', false);
                        $("#txtRate").val("");
                        $("#txtTax").prop('disabled', true);
                    }
                    var AlloServicetax_TakeAway = obj.setttingData.AlloServicetax_TakeAway;
                    if (AlloServicetax_TakeAway == true) {
                        $('#chkServiceTaxOnTake').prop('checked', true);
                    }
                    else {
                        $('#chkServiceTaxOnTake').prop('checked', false);
                    }

                    var homedelivery = obj.setttingData.homedel_charges;
                    if (homedelivery == true) {
                        $('#chkHomeDel').prop('checked', true);
                        $("#txtMinimumBill").val(obj.setttingData.min_bill_value);
                        $("#txtDelCharges").val(obj.setttingData.del_charges);
                        $("#txtMinimumBill").prop('disabled', false);
                        $("#txtDelCharges").prop('disabled', false);

                    }
                    else {
                        $('#chkHomeDel').prop('checked', false);
                        $("#txtMinimumBill").val("");
                        $("#txtDelCharges").val("");
                        $("#txtMinimumBill").prop('disabled', true);
                        $("#txtDelCharges").prop('disabled', true);
                    }


                    var RetailOption = obj.setttingData.retail_bill;
                    if (RetailOption == "I") {
                        $('#rdoRetInc').prop('checked', true);
                    }
                    else if (RetailOption == "E") {
                        $('#rdoRetExc').prop('checked', true);
                    }


                    var VATOption = obj.setttingData.vat_bill;
                    if (VATOption == "I") {
                        $('#rdoVatInc').prop('checked', true);
                    }
                    else if (VATOption == "E") {
                        $('#rdoVatExc').prop('checked', true);
                    }


                    var CSTOption = obj.setttingData.cst_bill;
                    if (CSTOption == "I") {
                        $('#rdoCSTInc').prop('checked', true);
                    }
                    else if (CSTOption == "E") {
                        $('#rdoCSTExc').prop('checked', true);
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }

            });


        }
        );


        $("#btnAdd").click(
                    function () {

                        InsertUpdate();
                    }
                    );


        $("#chkServicetax").change(function () {

            if ($('#chkServicetax').prop('checked') == true) {
                $("#txtTax").prop('disabled', false);

            }
            else {
                $("#txtTax").prop('disabled', true);

                $("#txtTax").val("");
            }
        });

        $("#chkHomeDel").change(function () {

            if ($('#chkHomeDel').prop('checked') == true) {
                $("#txtMinimumBill").prop('disabled', false);
                $("#txtDelCharges").prop('disabled', false);


            }
            else {
                $("#txtMinimumBill").prop('disabled', true);
                $("#txtDelCharges").prop('disabled', true);
                $("#txtMinimumBill").val("");
                $("#txtDelCharges").val("");
            }
        });
    });


   </script>
</asp:Content>

