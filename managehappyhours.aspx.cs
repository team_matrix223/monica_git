﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class managehappyhours : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindcategories();
        }

    }

    void Bindcategories()
    {
        ddlCategories.DataSource = new ColorBLL().GetAll();
        ddlCategories.DataTextField = "Color_Name";
        ddlCategories.DataValueField = "Color_ID";
        ddlCategories.DataBind();

    }


    [WebMethod]
    public static string FillHappyHours()
    {
        HappyHours objHappyHour = new HappyHours();
        new HappyHourBLL().GetHappyHours(objHappyHour);
        var JsonData = new
        {

            HappyHourData = objHappyHour,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Insert(string StartTime, string EndTime, bool isActive, string arrCategories)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        HappyHours objHappyHour = new HappyHours()
        {
            IsActive = isActive,
            TimeFrom = StartTime,
            TimeTo = EndTime,
            CategoryId = arrCategories,
            UserId = Id,
            BranchId = Branch,

        };

        string[] pidData = arrCategories.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("Id");

        for (int i = 0; i < pidData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["Id"] = Convert.ToInt32(pidData[i]);
            dt.Rows.Add(dr);

        }

        JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

        int status = new HappyHourBLL().InsertUpdate(objHappyHour, dt);
        var JsonData = new
        {
            MembershipPage = objHappyHour,
            Status = status
        };
        return ser.Serialize(JsonData);



    }
}