﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ChallanToVatBill.aspx.cs" Inherits="ChallanToVatBill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="js/SearchPlugin.js"></script>



<script type="text/javascript">

    var BillCollection = [];
    var ChallanCollection = [];
    function clsBill() {

        this.BillNo = "";
        this.Amount = 0;
        this.BillId = 0;
        this.BillDate = "";


    }
    function clschallan() {

        this.BillNo = "";
        this.ItemCode = "";
        this.ItemName = "";
        this.Qty = 0;
        this.Rate = 0;
        this.Amount = 0;
        this.BillId = 0;


    }


    function BindBills() {

        var html = "";

        for (var i = 0; i < BillCollection.length; i++) {




            html += "<tr>";
            html += "<td><input type='checkbox' class ='checkbox1' checked ='checked' name= 'chkbox' id='chk_" + BillCollection[i]["BillId"] + "' /></td>";
            html += "<td>" + BillCollection[i]["BillNo"] + "</td>";
            html += "<td>" + BillCollection[i]["BillDate"] + "</td>";
            html += "<td>" + BillCollection[i]["Amount"] + "</td>";



            html += "</tr>";


        }


        $("#tbShowBills").html(html);



    }

    function BindChallan() {

        var html = "";

        for (var i = 0; i < ChallanCollection.length; i++) {




            html += "<tr>";

            html += "<td style='display:none'>" + ChallanCollection[i]["BillNo"] + "</td>";
            html += "<td>" + ChallanCollection[i]["ItemCode"] + "</td>";
            html += "<td>" + ChallanCollection[i]["ItemName"] + "</td>";
            html += "<td>" + ChallanCollection[i]["Qty"] + "</td>";
            html += "<td>" + ChallanCollection[i]["Rate"] + "</td>";
            html += "<td>" + ChallanCollection[i]["Amount"] + "</td>";



            html += "</tr>";


        }


        $("#tbshowchallan").html(html);



    }



    function Printt(celValue) {



        $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

        var iframe = document.getElementById('reportout');
        iframe = document.createElement("iframe");

        iframe.setAttribute("id", "reportout");
        iframe.style.width = 0 + "px";

        iframe.style.height = 0 + "px";
        document.body.appendChild(iframe);

        document.getElementById('reportout').contentWindow.location = "Reports/customerretailinvoicerpt.aspx?BillNowPrefix=" + celValue+"&Type=Vat";



    }



    $(document).ready(
    function () {



        $("#btnreprint").click(
        function () {

            var BillNowPrefix = $("#<%=hdnBill.ClientID %>").val();

            Printt(BillNowPrefix);
            $.uiUnlock();

        }
        );

        $("#btnGo").click(
        function () {
            BindGrid();
        }
        );


        $("#dvchkall").css("display", "none");


        var select_all = document.getElementById("chkall"); //select all checkbox
        var checkboxes = document.getElementsByClassName("checkbox1"); //checkbox items

        //select all checkboxes
        select_all.addEventListener("change", function (e) {
            for (i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = select_all.checked;
            }
        });

        //uncheck "select all", if one of the listed checkbox item is unchecked
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].addEventListener('change', function (e) {
                if (this.checked == false) {
                    select_all.checked = false;
                }
            });
        }


        $("#txtStartDate,#txtEndDate,#txtBillDate,#txtDateFrom,#txtDateTo").val($("#<%=hdnDate.ClientID%>").val());
        $('#txtStartDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#txtEndDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#txtBillDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#txtDateFrom').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#txtDateTo').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        BindGrid();


        $("#dvShowBills").css("display", "none");

        $("#btnShow").click(
                   function () {

                       TotalItem = 0;


                       BillCollection = [];


                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                       var CustomerId = "";

                       var CustomerId = $("#ddlChosseCredit").val();

                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();




                       $("#dvShowBills").css("display", "block");
                       $("#dvchkall").css("display", "block");
                       $("#dvshowchallan").css("display", "none");

                       $.ajax({
                           type: "POST",
                           data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","CustomerId": "' + CustomerId + '"}',
                           url: "ChallanToVatBill.aspx/GetChallanDetail",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);
                               if (obj.BillsList.length > 0) {
                                   for (var i = 0; i < obj.BillsList.length; i++) {


                                       TO = new clsBill();


                                       TO.BillNo = obj.BillsList[i]["BillNowPrefix"];
                                       TO.Amount = obj.BillsList[i]["Net_Amount"];
                                       TO.BillId = obj.BillsList[i]["Bill_No"];
                                       TO.BillDate = obj.BillsList[i]["strBD"];
                                       BillCollection.push(TO);

                                   }
                                   BindBills();
                               }
                               else {
                                   alert("No Bills Found.");
                                   return;

                               }

                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {

                               $.uiUnlock();
                           }

                       });





                   });




        $("#btnChallantobill").click(
                   function () {

                       TotalItem = 0;


                       ChallanCollection = [];


                       $('#tbshowchallan tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       var BillNowPrefix = [];
                       for (var i = 0; i < BillCollection.length; i++) {
                           var billid = 0;
                           billid = BillCollection[i]["BillId"];
                           if ($("#chk_" + billid).prop('checked') == true) {
                               BillNowPrefix[i] = BillCollection[i]["BillNo"];
                           }
                       }



                       $("#dvShowBills").css("display", "none");
                       $("#dvchkall").css("display", "none");
                       $("#dvshowchallan").css("display", "block");





                       $.ajax({
                           type: "POST",
                           data: '{ "arrbillnowprefix": "' + BillNowPrefix + '"}',
                           url: "ChallanToVatBill.aspx/GetChallanValue",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);
                               if (obj.BillsList.length > 0) {
                                   for (var i = 0; i < obj.BillsList.length; i++) {


                                       TO = new clschallan();
                                       TO.BillId = 0;
                                       TO.BillNo = 0;
                                       TO.Amount = obj.BillsList[i]["Amount"];
                                       TO.ItemCode = obj.BillsList[i]["Item_Code"];
                                       TO.ItemName = obj.BillsList[i]["Item_Name"];
                                       TO.Qty = obj.BillsList[i]["Qty"];
                                       TO.Rate = obj.BillsList[i]["Rate"];
                                       ChallanCollection.push(TO);

                                       var fPrice = obj.BillsList[i]["Amount"];
                                       var TAx = 0;
                                       if (obj.BillsList[i]["BIllBasicType"] == "I") {

                                           if (obj.BillsList[i]["SurPer"] == "0") {
                                               TAx = (Number(fPrice) * Number(obj.BillsList[i]["Tax"]) / (100 + Number(obj.BillsList[i]["Tax"])));
                                           }
                                           else {

                                               var Survalll = ((Number(obj.BillsList[i]["SurPer"]) * Number(obj.BillsList[i]["Tax"])) / 100);
                                               TAx = (Number(fPrice) * Number(obj.BillsList[i]["Tax"]) / (100 + Number(obj.BillsList[i]["Tax"]) + Survalll));

                                           }

                                       }
                                       else {
                                           var TAx = (Number(fPrice) * Number(obj.BillsList[i]["Tax"]) / 100);

                                       }
                                       var surchrg = (Number(TAx) * Number(obj.BillsList[i]["SurPer"])) / 100;

                                       var amt = $("div[myid='amt_" + obj.BillsList[i]["TaxId"] + "']").html();
                                       var vat = $("div[myid='vat_" + obj.BillsList[i]["TaxId"] + "']").html();
                                       var sur = $("div[myid='sur_" + obj.BillsList[i]["TaxId"] + "']").html();
                                       var A = Number(amt) + Number(fPrice.toFixed(2));
                                       var B = Number(vat) + Number(TAx.toFixed(2));
                                       var C = Number(sur) + Number(surchrg.toFixed(2))
                                       $("div[myid='amt_" + obj.BillsList[i]["TaxId"] + "']").html(A.toFixed(2));
                                       $("div[myid='vat_" + obj.BillsList[i]["TaxId"] + "']").html(B.toFixed(2));
                                       $("div[myid='sur_" + obj.BillsList[i]["TaxId"] + "']").html(C.toFixed(2));


                                   }

                                   $("#dvtotalamount").html(obj.TotBill);
                                   $("#dvDiscount").html(obj.TotDis);
                                   $("#dvTax").html(obj.TotTa);
                                   $("#dvNet").html(obj.TotNe);
                                   var Round = Number(obj.TotNe) - (Number(obj.TotBill) - Number(obj.TotDis) + Number(obj.TotTa));
                                   $("#dvround").html(Round.toFixed(2));






                                   BindChallan();


                               }
                               else {
                                   alert("No Bills Found.");
                                   return;

                               }

                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {

                               $.uiUnlock();
                           }

                       });





                   });


        $("#btnsavetobill").click(
                   function () {


                       var BillNowPrefix = [];
                       for (var i = 0; i < BillCollection.length; i++) {
                           var billid = 0;
                           billid = BillCollection[i]["BillId"];
                           if ($("#chk_" + billid).prop('checked') == true) {
                               BillNowPrefix[i] = BillCollection[i]["BillNo"];
                           }
                       }
                       var BillValue = $("#dvtotalamount").html();
                       var Tax = $("#dvTax").html();
                       var Discount = $("#dvDiscount").html();
                       var NetAmount = $("#dvNet").html();
                       var Round_Amount = $("#dvround").html();
                       var CustomerId = $("#ddlChosseCredit").val();
                       var CustomerName = $("#ddlChosseCredit option:selected").text();

                       var BillDate = $("#txtBillDate").val();
                       TaxDen = [];
                       VatAmtDen = [];
                       VatDen = [];
                       SurDen = [];

                       $("div[name='tax']").each(
           function (y) {
               TaxDen[y] = $(this).html();
           }
           );
                       $("div[name='amt']").each(
           function (z) {
               VatAmtDen[z] = $(this).html();
           }
           );
                       $("div[name='vat']").each(
           function (a) {
               VatDen[a] = $(this).html();
           }
           );
                       $("div[name='sur']").each(
           function (a) {
               SurDen[a] = $(this).html();
           }
           );
                       var BillNo = "";

                       $.ajax({
                           type: "POST",
                           data: '{ "arrbillnowprefix": "' + BillNowPrefix + '","BillValue":"' + BillValue + '","Tax":"' + Tax + '","Discount":"' + Discount + '","NetAmount":"' + NetAmount + '","Round_Amount":"' + Round_Amount + '","Customer_ID":"' + CustomerId + '","CustomerName":"' + CustomerName + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","Billdate":"' + BillDate + '"}',
                           url: "ChallanToVatBill.aspx/ConvertChallanTOBill",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);
                               BillNo = obj.BNF;
                               if (obj.status != "0") {
                                   alert("Challan Converted To Bill Successfully");
                               }



                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {
                               $('#tbshowchallan tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                               $("#dvtotalamount").html("0");
                               $("#dvDiscount").html("0");
                               $("#dvTax").html("0");
                               $("#dvNet").html("0");

                               $("#dvround").html("0");
                               $("#dvShowBills").css("display", "none");
                               $("#dvshowchallan").css("display", "none");
                               Printt(BillNo);
                               BindGrid();
                               $.uiUnlock();
                           }

                       });





                   });




    }
    );
</script>
   

    


<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
     <form id="form1" runat="server">
      <asp:HiddenField ID="hdnDate" runat="server"/>
      <asp:HiddenField ID="hdnBill" runat="server"/>
    <div class="right_col" role="main">
                        <div  id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Challan To Bill(VAT BILL)</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">

                                  <tr><td  style="width: 150px">
                                     <label class="control-label">Choose Customer:</label>
                                    </td>

                                         <td colspan="300px">      
                                     <select id="ddlChosseCredit" style="width:321px;height:30px" clientidmode="Static" runat="server">
                                           
                                             </select>


                                    </td><td></td></tr>
                                    <tr><td style="width:80px"><label class="control-label">From:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtStartDate" style="width:107px" /></td>
                                    <td style="width:80px"><label class="control-label">To:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtEndDate" style="width:107px" /></td>
                                    <td><button type="button" class="btn btn-success" id="btnShow"  style="width:150px">Show Challan</button></td></tr>
                                    
                                        <tr><td style="width:80px"><label class="control-label">Bill on Date:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtBillDate" style="width:107px" /></td>
                                    <td ></td><td></td>
                                    <td></td></tr>
                                     

                                   
                                    </table>

                                </div>
                                </div>
                          

<div id="dvchkall"><input type='checkbox' checked ='checked' style="margin-left:35px" name= 'chkbox' id='chkall' />SELECT ALL</div>
                    

                      <div class="row" id="dvShowBills" style="display:none"> 
               
                    

                               <div class="col-md-12">
                               <div class = "col-md-12">
                               <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                       
                                <div class="x_content">

                                    <table class="table table-striped"  style="font-size:12px;margin-top:-18px;">
                                        <thead>
<tr><th>Select</th><th>BillNo</th><th>Date</th><th>Amount</th></tr>

</thead>
<tbody id="tbShowBills">
 
 


 
</tbody>
                                    </table>








                                 
                                </div>
                            </div>
                               </div>
                
                            


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnChallantobill" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Convert To Bill</div>
    
                                     </td>
                                    
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                         <div class="row" id="dvshowchallan" style="display:none"> 
               
                    

                               <div class="col-md-12">
                               <div class = "col-md-12">
                               <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                       
                                <div class="x_content">

                                    <table class="table table-striped"  style="font-size:12px;margin-top:-18px;">
                                         <thead>
<tr><th style="display:none">BillNo</th><th>ItemCode</th><th>Name</th><th>Qty</th><th>Rate</th><th>Amount</th></tr>
</thead>
<tbody id="tbshowchallan">
 
 


 
</tbody>
                                    </table>








                                 
                                </div>
                            </div>
                               </div>

                            


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>Amount</td><td><div id ="dvtotalamount"></div></td></tr>
                                      <tr><td>Discount</td><td><div id ="dvDiscount"></div></td></tr>
                                       <tr><td>Service Tax</td><td><div id ="dvTax"></div></td></tr>
                                        <tr><td>Round Amt</td><td><div id ="dvround"></div></td></tr>
                                        <tr><td>Net Amount</td><td><div id ="dvNet"></div></td></tr>
                                     <tr><td>
                                       <div id="btnsavetobill" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                    
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>


                    <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                            </div>
          
          
                        </div>

                        
                                <table>
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>


     <table id="jQGridDemoBill">
                </table>
                <div id="jQGridDemoPagerBill">
                </div> 
                <div class="btn btn-primary btn-small" id="btnreprint">
                               Reprint
                                </div>
              
  
    </div>



    <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          
          var DateTo = $("#txtDateTo").val();
          var Type = "2"
         
          jQuery("#jQGridDemoBill").GridUnload();

          jQuery("#jQGridDemoBill").jqGrid({
              url: 'handlers/ManageChallanBill.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo+ '&type=' + Type,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer','CashCustAddr','CreditCustAddr'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },

                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },

              ],

              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'BillNowPrefix',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Challan  List",




          });

          var $grid = $("#jQGridDemoBill");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {


          $("#<%=hdnBill.ClientID %>").val($('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix'));
            

          }
      });





          $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
                  {
                      refresh: false,
                      edit: false,
                      add: false,
                      del: false,
                      search: false,
                      searchtext: "Search",
                      addtext: "Add",
                  },

                  {//SEARCH
                      closeOnEscape: true

                  }

                    );


          var DataGrid = jQuery('#jQGridDemoBill');
          DataGrid.jqGrid('setGridWidth', '700');


      }
  </script>

    


         </form>

</asp:Content>
