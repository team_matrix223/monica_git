﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class manageexcise : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // BindCrAccounts();
            //BindDrAccounts();
        }
        CheckRole();
    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EXCISEMASTER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string Insert(int ExciseId, string ExciseHead, string TeriffNo, string ExciseDuty, string CrAccCode, string DrAccCode,bool IsActive,decimal Abatement)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EXCISEMASTER));

        string[] arrRoles = sesRoles.Split(',');


        if (ExciseId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }


        var Id =Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Excise objExcise = new Excise()
        {
            Excise_ID = ExciseId,
            Excise_Head =ExciseHead.Trim().ToUpper(),
            Teriff_No = TeriffNo,
            ExciseDuty = ExciseDuty,

            Dr_AccCode = DrAccCode,
            Cr_AccCode = CrAccCode,
            UserId = Id,
            IsActive = IsActive,
            Abatement  = Abatement,

        };
        status = new ExciseBLL().InsertUpdate(objExcise);
        var JsonData = new
        {
            ExciseData = objExcise,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    void BindDrAccounts()
    {

        ddlDrAccount.DataSource = new AccountsBLL().GetAccountsByCode("A");
        ddlDrAccount.DataValueField = "CCODE";
        ddlDrAccount.DataTextField = "CNAME";
        ddlDrAccount.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account To Debit--";
        li1.Value = "0";
        ddlDrAccount.Items.Insert(0, li1);

    }


    void BindCrAccounts()
    {

        ddlCrAccount.DataSource = new AccountsBLL().GetAccountsByCode("A");
        ddlCrAccount.DataValueField = "CCODE";
        ddlCrAccount.DataTextField = "CNAME";
        ddlCrAccount.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account To Credit--";
        li1.Value = "0";
        ddlCrAccount.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string Delete(Int32 ExciseId)
    {

        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EXCISEMASTER));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Excise objExcise = new Excise()
        {
            Excise_ID = ExciseId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new ExciseBLL().DeleteExcise(objExcise);
        var JsonData = new
        {
            excise = objExcise,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}