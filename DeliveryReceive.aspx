﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="DeliveryReceive.aspx.cs" Inherits="DeliveryReceive" %>

<%@ Register Src="~/Templates/DeliveryReceive.ascx" TagName="AddKit" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <script language="javascript" type="text/javascript">

        var m_KitID = 0;
        var ProductCollection = [];
        var mrpeditable = false;
        var saleRateeditable = false;


        function clsProduct() {
            this.Item_ID = 0;
            this.Item_Code = "";
            this.Item_Name = "";
            this.Qty_In_Case = 0;
            this.Qty = 0;
            this.Rate = 0;
            this.Sale_Rate = 0;
            this.MRP = 0;
            this.Amount = 0;
            this.QTY_TO_LESS = 0;
            this.Tax_ID = 0;
            this.TaxAmt = 0;
            this.Stock = 0;
            this.Dis = 0;
            this.Excise = 0;

        }

        var m_KitID = 0;

        function CommonCalculation() {

            var Amt = Number($("#txtQty").val()) * $("#txtRate").val();

            $("#txtAmount").val(Amt);
        }


        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });



        function BindRows() {


            var TotalAmount = 0;
            var html = "";

            for (var i = 0; i < ProductCollection.length; i++) {



                html += "<tr>";
                html += "<td>" + ProductCollection[i]["Item_Code"] + "</td>";
                html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["Qty_In_Case"] + "</td>";
                html += "<td>" + ProductCollection[i]["Qty"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["Scheme"] + "</td>";
                html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
                html += "<td>" + ProductCollection[i]["Sale_Rate"] + "</td>";
                html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
                html += "<td>" + ProductCollection[i]["Amount"] + "</td>";
                html += "<td>" + ProductCollection[i]["Dis3P"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["QTY_TO_LESS"] + "</td>";
                html += "<td>" + ProductCollection[i]["Stock"] + "</td>";
                html += "<td>" + ProductCollection[i]["TaxAmt"] + "</td>";
                html += "<td>" + ProductCollection[i]["Excise"] + "</td>";
               // html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
                html += "</tr>";

                TotalAmount += parseFloat(ProductCollection[i]["Amount"])

            }

//            $("#txtBillval").val(TotalAmount);
//            var DisPer = $("#ddlDealer option:selected").attr("dis");
//            var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
//            $("#txtDisPer").val(DisPer);
//            $("#txtDisAmt").val(DisAmt);
//            $("#txtAdj").val("0");
//            $("#txtnetAmt").val(Number(TotalAmount) - Number(DisAmt));
            $("#tbKitProducts").html(html);



        }


        function ResetControls() {

            m_KitID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtddlItems").val("").removeAttr("disabled");
            $("#ddlItems").html("<option value='0'></option>");
            $("#txtExciseAmt").val("");

            ResetList();
        }

        function ResetList() {

            $("#txtCode").val("");
            $("#txtName").val("");
            $("#txtQty").val("");
            $("#txtRate").val("");
            $("#txtMarketPrice").val("");
            $("#txtAmount").val("");
            $("#txtTax").val("");
            $("#ddlProducts").html("<option value='0'></option>");
            $("#txtddlProducts").val("").focus();


        }

        function GetPluginData(Type) {

            if (Type == "Kit") {

                var IsExist = $("#ddlItems option:selected").attr("is_exist");

                if (IsExist != 0) {

                    alert("Kit Already Created");
                    $("#txtddlItems").val("").focus();
                    $("#ddlItems").html("<option value='0'></option>")

                    return;

                }



                $("#txtMRP").val($("#ddlItems option:selected").attr("mrp"));
                $("#txtSaleRate").val($("#ddlItems option:selected").attr("sale_rate"));



                $("#txtTotalAmount").val("0");



            }
            else if (Type == "Product") {

                $("#txtCode").val($("#ddlProducts option:selected").attr("item_code"));
                $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
                $("#txtQty").val("").focus();


                $("#txtRate").val($("#ddlProducts option:selected").attr("DnRate"));
                $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
                $("#txtAmount").val(0);
                $("#txtStock").val($("#ddlProducts option:selected").attr("stock_qty"));


            }
        }

        $(document).ready(function () {

            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            $("#btnGo").click(
                   function () {

                       BindGrid();

                   }
                    );


            $("#txtOrderDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtBillDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDispDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());

            $('#txtOrderDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtBillDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });



            $('#txtDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtDispDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });






            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managekits.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    mrpeditable = obj.Mrp;
                    saleRateeditable = obj.Sale;


                    if (saleRateeditable == "0") {

                        $("#txtRate").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtRate").removeAttr('disabled');
                    }

                    if (mrpeditable == "0") {
                        $("#txtMarketPrice").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtMarketPrice").removeAttr('disabled');
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });


            ResetList();

            $("#btnCancelDialog").click(
           function () {
               $("#KitDialog").dialog("close");

           }
           );

            $("#btnAddInfo").click(

           function () {


               var BillNo = $("#txtBillNo").val();

               if (BillNo == "") {
                   alert("Enter Bill No");
                   $("#txtBillNo").focus();

                   return;
               }

               if ($("#ddlGodown").val() == 0) {
                   alert("Choose Godown");
                   $("#ddlGodown").focus();
                   return;

               }



               $("#txtddlItems").prop("disabled", true);

               $.ajax({
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   url: "DeliveryReceive.aspx/GetById",
                   data: '{"BillNo":"' + BillNo + '"}',
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       if (obj.Delivery.Bill_Value == 0) {
                           alert("Invalid BillNo. Please Try Again");

                           return;

                       }
                       $("#txtBillval").val(obj.Delivery.Bill_Value);
                       $("#txtDisAmt").val(obj.Delivery.Dis3Amt);
                       $("#txtDisPer").val(obj.Delivery.Dis3P);
                       $("#txtAdj").val(obj.Delivery.Adjustment);
                       $("#txtnetAmt").val(obj.Delivery.Net_Amount);
                       $("#txtExciseAmt").val(obj.Delivery.Excise_Amt);

                       ProductCollection = obj.DeliveryDetail;
                       BindRows();

                       $("#KitDialog").dialog({ autoOpen: true,

                           width: 1000,
                           resizable: false,
                           modal: true
                       });



                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {

                       $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");

                       $("#ddlProducts").supersearch({
                           Type: "Product",
                           Caption: "Please enter Item Name/Code ",
                           AccountType: "",
                           Godown: $("#ddlGodown").val(),
                           Width: 214,
                           DefaultValue: 0
                       });

                   }
               });



           }
           );


            $("#btnSave").click(
            function () {


                var BillNo = $("#txtBillNo").val();
                var BranchId = "0";
                var GodownId = $("#ddlGodown").val();

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "DeliveryReceive.aspx/ReceiveDelivery",
                    data: '{"BillNo":"' + BillNo + '","BranchId":"' + BranchId + '","GodownId":"' + GodownId + '"}',
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        alert("Delivery Received Successfully");


                        BindGrid();
                        $("#KitDialog").dialog("close");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                    }
                });









            });


            $("#btnAddKitItems").click(
           function () {

               if (!(Number($("#txtQty").val()) > 0)) {
                   $("#txtQty").focus();
                   return;

               }

               TO = new clsProduct();
               TO.Item_ID = $("#ddlProducts option:selected").val();
               TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
               TO.Item_Name = $("#ddlProducts option:selected").attr("item_name");
               TO.Qty_In_Case = $("#ddlProducts option:selected").attr("Qty_In_Case");
               TO.Scheme = "false";
               TO.Qty = $("#txtQty").val();
               TO.Rate = $("#txtRate").val();
               TO.Sale_Rate = $("#ddlProducts option:selected").attr("Sale_Rate");
               TO.MRP = $("#ddlProducts option:selected").attr("mrp");
               TO.Amount = $("#txtAmount").val();
               TO.QTY_TO_LESS = $("#ddlProducts option:selected").attr("Qty_To_Less");
               TO.Stock = 0;
               TO.Tax_ID = $("#ddlProducts option:selected").attr("tax_id")
               TO.TaxAmt = $("#ddlProducts option:selected").attr("tax_code");
               TO.Dis3P = $("#ddlDealer option:selected").attr("dis");
               TO.Bill_Date = '1/1/1';
               ProductCollection.push(TO);

               BindRows();
               ResetList();

           }

           );



            $("#txtQty").keyup(
           function () {

               CommonCalculation();

           }
           );

            $("#txtRate").keyup(
           function () {
               CommonCalculation();


           }
           );



            //            $("#ddlItems").supersearch({
            //                Type: "Kit",
            //                Caption: "Please enter Kit Name/Code ",
            //                AccountType: "",
            //                Width: 214,
            //                DefaultValue: 0
            //            });

            //            $("#ddlProducts").supersearch({
            //                Type: "Product",
            //                Caption: "Please enter Item Name/Code ",
            //                AccountType: "",
            //                Godown: $("#ddlGodown").val(),
            //                Width: 214,
            //                DefaultValue: 0
            //            });



            $("#ddlGodown").change(
            function () {

                $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");





                $("#ddlProducts").supersearch({
                    Type: "Product",
                    Caption: "Please enter Item Name/Code ",
                    AccountType: "",
                    Godown: $("#ddlGodown").val(),
                    Width: 214,
                    DefaultValue: 0
                });

            }
            );



            BindGrid();
            $("#btnNew").click(
                            function () {


                                ResetControls();

                                $("#KitDialog").dialog({ autoOpen: true,

                                    width: 1000,
                                    resizable: false,
                                    modal: true
                                });
                            }
                            );

            $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Kit is selected to Delete");
               return;
           }

           var KitID = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Kit_ID')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');


               $.ajax({
                   type: "POST",
                   data: '{"KitID":"' + KitID + '"}',
                   url: "managekits.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       BindGrid();
                       alert("Kit is Deleted successfully.");

                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }


       }
       );


        });
   
    </script>
    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnRoles" runat="server" />
      <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
    <asp:HiddenField ID="hdnDate" runat="server" />
    

    <div class="right_col" role="main">
        <div class="">

       
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Receive Delivery Note</h3>
                </div>
                <div class="x_panel">
                    <div class="form-group">
                     

                        <div class="youhave" style="padding-left: 1px">

                        <table>     <tr>
                                    <td align="left">
                                        <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>
                                                    Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr></table>


                            <table id="jQGridDemo">
                            </table>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="padding: 5px">
                                        <div id="btnNew" class="btn btn-primary">
                                           Receiving Challan</div>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="padding: 5px;display:none">
                                        <div id="btnEdit" class="btn btn-danger">
                                            Receiving Challan</div>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="padding: 5px;display:none">
                                        <div id="btnDelete" class="btn btn-success">
                                            Delete</div>
                                    </td>
                                </tr>
                            </table>
                            <div id="jQGridDemoPager">
                            </div>
                        </div>
                        <div class="row" id="KitDialog" style="display: none">
                            <uc1:AddKit ID="ucAddKit" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            
       
        </div>
    </form>
    <script type="text/javascript">

        

                function BindGrid() {


                   
                    var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageReceivedDelivery.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['BillNo','BillDate','Customer','GrNo','GrDate','VehNo','BillVal','Dis3P','Dis3Amt','TaxP','TaxAmt','TotalAmt','Adjustment','NetAmount','CCode','GodownID','Passing'],
  


                        colModel: [
                                   
                                    { name: 'ReceivedBillNO', key: true, index: 'ReceivedBillNO', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                    { name: 'strBillDate', index: 'strBillDate', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                     { name: 'Dealer', index: 'Dealer', width: 400, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'GR_No', index: 'GR_No', width:400, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'strBillDate', index: 'strBillDate', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Veh_No', index: 'Veh_No', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                     { name: 'Dis3P', index: 'Dis3P', width:100, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Dis3Amt', index: 'Dis3Amt', width:400, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'TaxP', index: 'TaxP', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'TaxAmt', index: 'TaxAmt', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Total_Amount', index: 'Total_Amount', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                     { name: 'Adjustment', index: 'Adjustment', width:100, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Net_Amount', index: 'Net_Amount', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'CCODE', index: 'CCODE', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Godown_ID', index: 'Godown_ID', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'PASSING', index: 'PASSING', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                   
                                   
                                   

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Kit_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Delivery Note List",

                        editurl: 'handlers/ManageReceivedDelivery.ashx',
                           ignoreCase: true,
                         toolbar: [true, "top"],


                    });

                     var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  ResetControls();
            m_KitID = $('#jQGridDemo').jqGrid('getCell', rowid, 'Bill_No');
 
             
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>
