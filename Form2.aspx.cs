﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class Form2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindBranches();
        }
    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string GetNonExciseBYDate(DateTime DateFrom, DateTime DateTo,int Branch)
    {


        List<DeliveryMaster> lstNonExciseNOtes = new DeliveryNoteBLL().GetNonExciseByDate(DateFrom, DateTo, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstNonExciseNOtes

        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string Insert(string BillNowPrefix,int Branch)
    {

        string[] BillNowPrefixData = BillNowPrefix.Split(',');
  
        DataTable dt = new DataTable();

        dt.Columns.Add("BillNowPrefix");
       

        for (int i = 0; i < BillNowPrefixData.Length; i++)
        {
            if (BillNowPrefixData[i] != "")
            {
                DataRow dr = dt.NewRow();

                dr["BillNowPrefix"] = Convert.ToString(BillNowPrefixData[i]);

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteDeliveryNotes(Branch, dt);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }
}