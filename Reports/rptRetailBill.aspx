﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rptRetailBill.aspx.cs" Inherits="backoffice_Reports_rptRetailBill" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


td.dxmtb.dxmMenu
{
	padding: 2px;
}

.dxmMenu,
.dxmVerticalMenu
{
	font: 12px Tahoma;
	color: black;
	background-color: #F0F0F0;
	border: 1px solid #A8A8A8;
	padding: 2px;
}

.dxmtb .dxmMenuItem,
.dxmtb .dxmMenuItemWithImage,
.dxmtb .dxmMenuItemWithPopOutImage,
.dxmtb .dxmMenuItemWithImageWithPopOutImage
{
	padding: 4px 5px 5px;
}

.dxmMenuItem,
.dxmMenuItemWithImage
{
	padding: 4px 8px 5px;
}

.dxmMenuItem,
.dxmMenuItemWithImage,
.dxmMenuItemWithPopOutImage,
.dxmMenuItemWithImageWithPopOutImage,
.dxmVerticalMenuItem,
.dxmVerticalMenuItemWithImage,
.dxmVerticalMenuItemWithPopOutImage,
.dxmVerticalMenuItemWithImageWithPopOutImage, 
.dxmVerticalMenuRtlItem,
.dxmVerticalMenuRtlItemWithImage,
.dxmVerticalMenuRtlItemWithPopOutImage,
.dxmVerticalMenuRtlItemWithImageWithPopOutImage,
.dxmMenuLargeItem,
.dxmMenuLargeItemWithImage,
.dxmMenuLargeItemWithPopOutImage,
.dxmMenuLargeItemWithImageWithPopOutImage,
.dxmVerticalMenuLargeItem,
.dxmVerticalMenuLargeItemWithImage,
.dxmVerticalMenuLargeItemWithPopOutImage,
.dxmVerticalMenuLargeItemWithImageWithPopOutImage,
.dxmVerticalMenuLargeRtlItem,
.dxmVerticalMenuLargeRtlItemWithImage,
.dxmVerticalMenuLargeRtlItemWithPopOutImage,
.dxmVerticalMenuLargeRtlItemWithImageWithPopOutImage
{
	white-space: nowrap;
}
.dxmtb .dxmMenuItemSpacing,
.dxmtb .dxmMenuItemSeparatorSpacing
{
	width: 2px;
}

.dxmMenuItemSpacing,
.dxmMenuLargeItemSpacing,
.dxmMenuItemSeparatorSpacing,
.dxmMenuLargeItemSeparatorSpacing
{
	width: 2px;
}
.dxmMenuFullHeightSeparator
{
	display: none;
}

.dxmMenuSeparator .dx,
.dxmMenuFullHeightSeparator,
.dxmMenuVerticalSeparator
{
	background-color: #A8A8A8;
	width: 1px;
}
.dxmMenuSeparator,
.dxmMenuFullHeightSeparator
{
	width: 1px;
}

.dxmMenuSeparator .dx,
.dxmMenuFullHeightSeparator .dx 
{
	font-size: 0;
	line-height: 0;
	overflow: hidden;
	width: 1px;
	height: 1px;
}
        .style1
        {
            border-width: 0px;
            background-image: url('mvwres://DevExpress.XtraReports.v11.2.Web,%20Version=11.2.10.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/DevExpress.XtraReports.Web.ASPxReports.Images.sprite.gif');
        }
        .style2
        {
            border-width: 0px;
            background-image: url('mvwres://DevExpress.Web.ASPxEditors.v11.2,%20Version=11.2.10.0,%20Culture=neutral,%20PublicKeyToken=b88d1754d700e49a/DevExpress.Web.ASPxEditors.Images.sprite.gif');
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" 
            ReportViewerID="ReportViewer1" ShowDefaultButtons="False">
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <styles>
                <LabelStyle><Margins MarginLeft='3px' MarginRight='3px' /></LabelStyle>
            </styles>
        </dx:ReportToolbar>
    
    <dx:ReportViewer ID="ReportViewer1" runat="server">
    </dx:ReportViewer>
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
    
    </div>
    </form>
</body>
</html>
