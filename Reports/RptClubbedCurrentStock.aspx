﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptClubbedCurrentStock.aspx.cs" Inherits="Reports_RptClubbedCurrentStock" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
 <style type="text/css">
#tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblist tr td{text-align:left;padding:2px}
#tblist tr:nth-child(even){background:#F7F7F7}
 
#tblistItem tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblistItem tr td{text-align:left;padding:2px}
#tblistItem tr:nth-child(even){background:#F7F7F7}
 </style>
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(
   function () {


       $("#btnPrint").click(
    function () {

        PrintGridData();
    }
    );

       function PrintGridData() {

           //var prtGrid = document.getElementById('<%=gvUserInfo.ClientID %>');
           var prtGrid = document.getElementById('dvle');
           prtGrid.border = 0;
           var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
           prtwin.document.write(prtGrid.outerHTML);
           prtwin.document.close();
           prtwin.focus();
           prtwin.print();
           prtwin.close();
       }


       $("input[name ='department']").click(function () {
           var CompaniesId = $('input[name="department"]:checkbox:checked').map(function () {
               return this.value;
           }).get();

           $("#<%=hdnDept.ClientID %>").val(CompaniesId);
       });

       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>

<script type="text/javascript">

    $(document).ready(
   function () {


       $("input[name ='group']").click(function () {
           var ItemsId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnGroup.ClientID %>").val(ItemsId);
       });

       $("#<%=rdbAllGroup.ClientID %>").change(
    function () {


        if ($("#<%=rdbAllGroup.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelectGroup.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelectGroup.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelectGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllGroup.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnDept" runat="server" />
<asp:HiddenField ID= "hdnGroup" runat="server" />

<div style="padding-top:30px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
     CURRENT STOCK REPORT </td></tr>
    
</table>


<table>

    
<tr>
<td valign="top" style="width:200px;padding-top:20px;padding-left:10px">
 

<table style="border:1px">

<tr>
<td>Item Type:</td><td><asp:DropDownList ID ="ddlItemType" Width="150px" runat="server"></asp:DropDownList></td>
</tr>




  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px"><asp:RadioButton ID="rdbSelect"   runat="server" Text="Selected" /></td>  </tr>
 <tr><td class="headings" colspan="2" style="padding-top:20px">Department:</td></tr>
                      <tr>

                       <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:200px; overflow-y:scroll">
                      <table style="width:100%" id="tblist">
                      <asp:Literal ID="ltDepartments" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>

  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAllGroup" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px"><asp:RadioButton ID="rdbSelectGroup"   runat="server" Text="Selected" /></td>  </tr>
 <tr><td class="headings" colspan="2" style="padding-top:20px">Group:</td></tr>
                      <tr>

                      <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll">
                      <table style="width:100%" id="tblistItem">
                      <asp:Literal ID="ltGroups" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>

                      <tr>          
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/>
            
               <td><input type="button" id="btnPrint" value="Print" onclick="PrintGridData()" /></td>
       
            </td>
            </tr>

            <tr> <td><asp:Button ID="btnexport" runat="server"  Text="Export To Excel" 
            onclick="btnexport_Click"/></td></tr>
</table>
</td> 
 <td>
 <div style="width:1050px;font-size:15px" id ="dvle">
<asp:GridView ID="gvUserInfo" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"  HorizontalAlign="Left"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>
</div>
</td>
</tr>
</table>



<%--   <table width="1050px" style="background-color:gray;color:white">

   <tr><td>Choose Branch</td><td><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >                                  
         </asp:DropDownList></td></tr>

   <tr><td>Item Type:</td><td><asp:DropDownList ID ="ddlItemType" Width="150px" runat="server"></asp:DropDownList></td>
<td>Godown:</td><td><asp:DropDownList ID ="ddlgodown" Width="150px" runat="server"></asp:DropDownList></td>
<td>Company:</td><td><asp:DropDownList ID ="ddlcompany" Width="150px" runat="server"></asp:DropDownList></td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>
    </table>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="1050px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
    </dx:ReportViewer>--%>

</div>
</asp:Content>

