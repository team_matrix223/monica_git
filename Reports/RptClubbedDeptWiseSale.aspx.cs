﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;



public partial class Reports_RptClubbedDeptWiseSale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();

        }
        //  CheckRole();

          lbldatefrom.Text = txtDateFrom.Text;
        lbldateto.Text = txtDateTo.Text;

        if (ddlval.Text == "Value")
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetDeptSaleClubbed(txtDateFrom.Text, txtDateTo.Text);
            gvUserInfo.DataBind();
            
           
        }
        else if (ddlval.Text == "Qty")
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetDeptSaleClubbedByQty(txtDateFrom.Text, txtDateTo.Text);
            gvUserInfo.DataBind();
        }
        SqlDataReader dr = new ReportDAL().GetHeader(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value));
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                lblheaader1.Text = dr["Header1"].ToString();
                lblheaader2.Text = dr["Header2"].ToString();
                lblheaader3.Text = dr["Header3"].ToString();
                lblheaader4.Text = dr["Header4"].ToString();
                lblheaader5.Text = dr["Header5"].ToString();

            }
        }


    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPDEPTSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


    }


    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void gvUserInfo_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Visible = false;
    }
}