﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_ComparisonRpt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
    
        }

        ComparisonReport objBreakageExpiry = new ComparisonReport(txtDateFrom.Text, txtDateTo.Text,Convert.ToInt32(ddlBranch.SelectedItem.Value));
        ReportViewer1.Report = objBreakageExpiry;

    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}