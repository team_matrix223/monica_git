﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="internalissueitemwise.aspx.cs" Inherits="Reports_internalissueitemwise" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
 <style type="text/css">
#tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblist tr td{text-align:left;padding:2px}
#tblist tr:nth-child(even){background:#F7F7F7}
 
#tblistItem tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblistItem tr td{text-align:left;padding:2px}
#tblistItem tr:nth-child(even){background:#F7F7F7}
 </style>
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>

<script type="text/javascript">


    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblistItem");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    $(document).ready(
   function () {

       

       $("#myInput").hide();
       $("#td_2").hide();
       $("#<%=rdbSelectItem.ClientID %>").click(function () {
           $("#td_2").show();
           $("#myInput").show();
       });
       $("#<%=rdbAllItem.ClientID %>").click(function () {
           $("#myInput").hide();
           $("#td_2").hide();
       });

       $("input[name ='item']").click(function () {
           var ItemsId = $('input[name="item"]:checkbox:checked').map(function () {
               return this.value;
           }).get();

           //alert(ItemsId)
           $("#<%=hdnItem.ClientID %>").val(ItemsId);
       });

       $("#<%=rdbAllItem.ClientID %>").change(
    function () {


        if ($("#<%=rdbAllItem.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelectItem.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelectItem.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelectItem.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllItem.ClientID %>").prop('checked', false);

        }

    }
    );



   
   });

</script>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnItem" runat="server" />
<div style="padding-top:30px;padding-left:30px;" >
<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
     INTERNAL ISSUE DEPT REPORT </td></tr>
   
</table>


<table>
 <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
    
<tr>
<td valign="top" style="width:200px;padding-top:20px;padding-left:10px">
 

<table style="border:1px">
<tr><td><b>Select Date</b></td></tr>
<tr>
<td style="padding-top:10px">From Date:</td><td style="padding-top:10px"><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td>
</tr>

<tr>
<td style="padding-top:10px">To Date:</td><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
    <td style="padding-top:10px"><asp:TextBox ID="txtDateTo" name="All" runat="server" Width="100px"></asp:TextBox></td>
</tr>

  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAllItem" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px">
  <asp:RadioButton ID="rdbSelectItem"   runat="server" Text="Selected" />
</td> 
   
 </tr>
 <tr>
 <td id="td_2">Search</td><td>  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"></td></tr>
 

 <tr><td class="headings" colspan="2" style="padding-top:20px">Items:</td></tr>
                      <tr>

                      <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:350px;overflow-y:scroll">
                      <table style="width:100%" id="tblistItem">
                      <asp:Literal ID="ltItems" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>

                      <tr>          
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>
</table>
</td> 
 <td>
  <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="800px">
    </dx:ReportViewer>

</td>
</tr>
</table>    
</div>
</asp:Content>

