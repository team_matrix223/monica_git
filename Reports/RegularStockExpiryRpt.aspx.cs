﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RegularStockExpiryRpt : System.Web.UI.Page
{
    string ReportType = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            for (int i = 0; i < months.Length-1; i++)
            {
                Ddmonth.Items.Add(new ListItem(months[i], Convert.ToInt32(i+1).ToString()));
            }
            //txtDateFrom.Text = DateTime.Now.ToShortDateString();
            //txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            BindGroup();
            BindDepartments();
            BindAllItems();
        }

        //RegularExpiryStockRpt objBreakageExpiry = new RegularExpiryStockRpt(HdnReq.Value, txtDateFrom.Text, txtDateTo.Text, dditem.SelectedItem.Value, ddDepartment.SelectedItem.Value, ddGroup.SelectedItem.Value, Convert.ToInt32(ddlBranch.SelectedItem.Value));
        //ReportViewer1.Report = objBreakageExpiry;
        CheckRole();

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPBILLCANCEL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

        void BindGroup()
    {

        ddGroup.DataSource = new RGodownsDAL().GetAllGroup();
        ddGroup.DataValueField = "Group_Id";
        ddGroup.DataTextField = "Group_Name";
        ddGroup.DataBind();
        ListItem li2 = new ListItem();
        li2.Text = "--All--";
        li2.Value = "0";
   
        ddGroup.Items.Insert(0, li2);

    }

        void BindDepartments()
        {

            ddDepartment.DataSource = new RGodownsDAL().GetAllDepartments();
            ddDepartment.DataValueField = "Prop_Id";
            ddDepartment.DataTextField = "Prop_Name";
            ddDepartment.DataBind();
          
            ListItem li2 = new ListItem();
            li2.Text = "--All--";
            li2.Value = "0";
         
            ddDepartment.Items.Insert(0, li2);

        }

        void BindAllItems()
        {

            dditem.DataSource = new RGodownsDAL().GetAllItems();
            dditem.DataValueField = "item_code";
            dditem.DataTextField = "item_Name";
            dditem.DataBind();
      
            ListItem li2 = new ListItem();
            li2.Text = "--All--";
            li2.Value = "0";
     
            dditem.Items.Insert(0, li2);

        }
    public void BindGrid()
    {
        if (ddType.SelectedValue == "Expiry")
        {
            ReportType = "strp_regular_stockexp";
            reporthead.InnerText = "REGULAR STOCK EXPIRY REPORT";

        }
        else if (ddType.SelectedValue == "Sale")
        {
            ReportType = "strp_regular_sale";
            reporthead.InnerText = "REGULAR SALE REPORT";
        }

        else if (ddType.SelectedValue == "Issue")
        {
            ReportType = "strp_regular_issueqty";
            reporthead.InnerText = "REGULAR ISSUE REPORT";
        }

        Connection sqlcon = new Connection();
        using (SqlConnection con = new SqlConnection(sqlcon.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(ReportType, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@branchid", ddlBranch.SelectedValue);
            cmd.Parameters.AddWithValue("@month", Ddmonth.SelectedValue);
            cmd.Parameters.AddWithValue("@item_code", dditem.SelectedValue);
            cmd.Parameters.AddWithValue("@department", ddDepartment.SelectedValue);
            cmd.Parameters.AddWithValue("@group", ddGroup.SelectedValue);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            gv_display.DataSource = dt;
            gv_display.DataBind();
            ShowingGroupingDataInGridView(gv_display.Rows, 0, 3);
        }




    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        if (ddDepartment.SelectedValue != "0")
        {
            lblGroupHeader.Text = "Department:" + ddDepartment.SelectedItem.Text;

        }

       else if (ddGroup.SelectedValue != "0")
        {
            lblGroupHeader.Text = "Group:" + ddGroup.SelectedItem.Text;

        }
        BindGrid();

    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_display.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

    protected void ddDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        dditem.SelectedValue = "0";
        ddGroup.SelectedValue = "0";

    }

    protected void dditem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddDepartment.SelectedValue = "0";
        ddGroup.SelectedValue = "0";
    }

    protected void ddGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddDepartment.SelectedValue = "0";
        dditem.SelectedValue = "0";
    }


    void ShowingGroupingDataInGridView(GridViewRowCollection gridViewRows, int startIndex, int totalColumns)
    {
        if (totalColumns == 0) return;
        int i, count = 1;
        ArrayList lst = new ArrayList();
        lst.Add(gridViewRows[0]);
        var ctrl = gridViewRows[0].Cells[startIndex];
        for (i = 1; i < gridViewRows.Count; i++)
        {
            TableCell nextTbCell = gridViewRows[i].Cells[startIndex];
            if (ctrl.Text == nextTbCell.Text)
            {
                count++;
                nextTbCell.Visible = false;
                lst.Add(gridViewRows[i]);
            }
            else
            {
                if (count > 1)
                {
                    ctrl.RowSpan = count;
                    ShowingGroupingDataInGridView(new GridViewRowCollection(lst), startIndex + 1, totalColumns - 1);
                }
                count = 1;
                lst.Clear();
                ctrl = gridViewRows[i].Cells[startIndex];
                lst.Add(gridViewRows[i]);
            }
        }
        if (count > 1)
        {
            ctrl.RowSpan = count;
            ShowingGroupingDataInGridView(new GridViewRowCollection(lst), startIndex + 1, totalColumns - 1);
        }
        count = 1;
        lst.Clear();
    }
 
}