﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class itemsale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            BindProducts();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }


        string query = "";

        if (rdbAll.Checked == true)
        {
            query = "All";
        }
        else
        {
            query = hdnsale.Value;
        }

        RptItemSale objBreakageExpiry = new RptItemSale(query, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId);
        ReportViewer1.Report = objBreakageExpiry;
    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPITEMSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindProducts()
    {
        ltItems.Text = new RGodownsBLL().GetProductsHtml();
    }




    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

      



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}