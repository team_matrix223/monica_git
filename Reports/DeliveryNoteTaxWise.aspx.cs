﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_DeliveryNoteTaxWise : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();

        }

        CheckRole();


        string notetype = "";
        string query = "";

        if (rdbAll.Checked == true)
        {
            query = "All";
        }
        else
        {
            query = hdnsale.Value;
        }
        if (rdbAllExcise.Checked == true)
        {
            notetype = "All";
        }
        else if (rdbExcise.Checked == true)
        {
            notetype = "Excise";
        }
        else if (rdbnonexcise.Checked == true)
        {
            notetype = "NonExcise";
        }
        RptDeliveryNoteTaxWise objBreakageExpiry = new RptDeliveryNoteTaxWise(query, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), notetype);
        ReportViewer1.Report = objBreakageExpiry;

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPGROUPSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }

    }

    void BindBranches()
    {
        ltBranchs.Text = new RGodownsBLL().GetBranchHtml();
    }


    protected void btnGetRecords_Click(object sender, EventArgs e)
    {





    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}