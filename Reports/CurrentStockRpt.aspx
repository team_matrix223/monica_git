﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="CurrentStockRpt.aspx.cs" Inherits="CurrentStockRpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(
        function () {

         
            BindGrid();
            function BindGrid() {


                $.ajax({
                    type: "POST",
                    data: '{}',
                    url: "CurrentStockRpt.aspx/BindGrid",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                     

                        var obj = jQuery.parseJSON(msg.d);
                      

                     
                        $("#ddlgodown").append(obj.GodownOptions);



                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        //$.uiUnlock();

                    }


                });
            }
        }
        );
    </script>

<div class="container">
<div class="row">
				<div class="four columns">
					<div class="box_c">
						<div class="box_c_heading cf box_actions">
							<div class="box_c_ico"><img src="img/ico/icSw2/16-Abacus.png" alt="" /></div>
							<p>Statistics</p>
						</div>
						<div class="box_c_content">
							<p class="inner_heading sepH_c">Latest info</p>
							<div class="box_c">
                            <form action="index.php?page=form_default" class="nice custom">
							
                         

									<div class="formRow">
										<label for="nice_select">ITEM TYPE</label>
                                        <asp:DropDownList ID ="ddlitem" runat="server"></asp:DropDownList>
									
									</div>

                                    <div class="formRow">
										<label for="dd">GODOWN</label>
										<select id="ddlgodown">
										</select>
									</div>

                                    <div class="formRow">
										<label for="nice_select">COMPANY</label>
									  <asp:DropDownList ID ="ddlcompany" runat="server"></asp:DropDownList>
									</div>
									
									
								</form>
					
						<div class="box_c_content form_a">
							<div class="tab_pane" style="display:none">
								
							</div>
							
						</div>
					</div>
						</div>
					</div>
				</div>
				<div class="eight columns">
					<div class="box_c">
						<div class="box_c_heading cf">
							<div class="box_c_ico"><img src="img/ico/icSw2/16-Graph.png" alt="" /></div>
							<p>Charts</p>
						</div>
						<div class="box_c_content">
							<div class="inner_block">
								<div class="h_scrollable sepH_a sw_resizedEL" style="height:280px">
									<div class="items">
										<div class="left">
											<div id="ds_plot1" title="Unique visitors" style="height:280px;margin:0 auto" class="chart_flw"></div>
										</div>
										<div class="left">
											<div id="ds_plot2" title="Another chart" style="height:280px;margin:0 auto" class="chart_flw"></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="four columns">
										<a class="gh_button pill small image_from_chart" style="display:none;margin-top:10px" href="#">Create image from chart</a>
									</div>
									<div class="eight columns">
										<ul class="pagination h_navi right sepV_a">
											<li class="current"><a href="javascript:void(0)">Unique visitors</a></li>
											<li><a href="javascript:void(0)">Another chart</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="twelve columns">
					
				</div>
			</div>
            
		</div>
        
</asp:Content>

