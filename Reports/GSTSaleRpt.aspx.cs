﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Reports_GSTSaleRpt : System.Web.UI.Page
{
    int BranchId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
     
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
        if (txtDateFrom.Text != "")
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetGSTSaleReport(txtDateFrom.Text, txtDateTo.Text, BranchId);
            gvUserInfo.DataBind();


        }





    }

    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        gvUserInfo.PageIndex = e.NewPageIndex;


        gvUserInfo.DataSource = null;
        gvUserInfo.DataSource = new ReportDAL().GetGSTSaleReport(txtDateFrom.Text, txtDateTo.Text, BranchId);
        gvUserInfo.DataBind();
    }
    protected void OnDataBound(object sender, EventArgs e)
    {


        for (int i = gvUserInfo.Rows.Count - 1; i > 0; i--)
        {
            GridViewRow row = gvUserInfo.Rows[i];
            GridViewRow previousRow = gvUserInfo.Rows[i - 1];
            for (int j = 0; j < row.Cells.Count; j++)
            {
                if (row.Cells[0].Text == previousRow.Cells[0].Text)
                {
                    if (previousRow.Cells[14].RowSpan == 0)
                    {
                        if (row.Cells[14].RowSpan == 0)
                        {
                            previousRow.Cells[14].RowSpan += 2;
                        }
                        else
                        {
                            previousRow.Cells[14].RowSpan = row.Cells[14].RowSpan + 1;
                        }
                        row.Cells[14].Visible = false;

            
                    


                        if (row.Cells[15].RowSpan == 0)
                        {
                            previousRow.Cells[15].RowSpan += 2;
                        }
                        else
                        {
                            previousRow.Cells[15].RowSpan = row.Cells[15].RowSpan + 1;
                        }
                        row.Cells[15].Visible = false;
                 


                    }



                }
            }
        }


    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPFOCSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}