﻿<%@ Page Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true"  CodeFile="groupsale.aspx.cs" Inherits="groupsale" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

   <style type="text/css">
 #tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
 #tblist tr td{text-align:left;padding:2px}
  #tblist tr:nth-child(even){background:#F7F7F7}
 </style>

<%-- <link href="../js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="../js/grid.locale-en.js" type="text/javascript"></script>
     <link href="../js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="../js/jquery-ui.js"></script>


    <script src="../js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.uilock.js"></script>--%>

 
     
     
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function myFunction1() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput1");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblist");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    $(document).ready(
   function () {
       
       $("#myInput1").hide();

       $("#td_1").hide();
       $("#<%=rdbSelect.ClientID %>").click(function () {
           $("#myInput1").show();
           $("#td_1").show();
       });
       $("#<%=rdbAll.ClientID %>").click(function () {
           $("#myInput1").hide();
           $("#td_1").hide();
       });



       $("input[name ='group']").click(function () {
           var CompaniesId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnsale.ClientID %>").val(CompaniesId);
          

       });







       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }


    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {


            $("#<%=rdbAll.ClientID %>").prop('checked', false);


        }


    }
    );






       //       $("#btnAdd").click(
       //       function () {

       //           $.uiLock('');


       //           var DateFrom = $("#txtDateFrom").val();
       //           var DateTo = $("#txtDateTo").val();
       //           var arrQry = "";
       //           if ($("#rdoAll").prop("checked") == true) {
       //               arrQry = 'All'
       //           }
       //           else {

       //               arrQry = $('input[name="company"]:checkbox:checked').map(function () {
       //                   return this.value;
       //               }).get();

       //           }
       //           

       //       });






   });




</script>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />
<div style="padding-top:30px;padding-left:30px;" >
<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
      GROUP WISE SALE REPORT </td></tr>
   
</table>


<table>
  <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
<tr>
<td valign="top" style="width:200px;padding-top:20px;padding-left:10px">
 

<table style="border:1px">
<tr><td><b>Select Date</b></td></tr>
<tr>
<td style="padding-top:10px">From Date:</td><td style="padding-top:10px"><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td>
</tr>

<tr>
<td style="padding-top:10px">To Date:</td><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
    <td style="padding-top:10px"><asp:TextBox ID="txtDateTo" name="All" runat="server" Width="100px"></asp:TextBox></td>
</tr>
  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px"><asp:RadioButton ID="rdbSelect"   runat="server" Text="Selected" />
  
    </td>  </tr>
    <tr><td id="td_1">Search</td><td><input type="text" id="myInput1" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name"></td></tr>
 <tr><td class="headings" colspan="2" style="padding-top:20px">Groups:</td></tr>
                      <tr>

                       <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll">
                      <table style="width:100%" id="tblist">
                      <asp:Literal ID="ltGroups" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>


                      <tr>
            
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
            <%--<td><input type="button" id="btnPrint" value="Print" onclick="PrintGridData()" /></td>--%></tr>
           


</table>
 
</td> 

 <td>
<%--<div style="width:1050px;overflow:scroll;height:400px">
<b></b><br /> 
<asp:GridView ID="gvUserInfo" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>

</div>--%>

  <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="800px">
    </dx:ReportViewer>


</td>


</tr>

</table>


    
</div>

        

</asp:Content>
