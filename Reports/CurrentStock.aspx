﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="CurrentStock.aspx.cs" Inherits="Reports_CurrentStock" %>
<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
 <style type="text/css">
#tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblist tr td{text-align:left;padding:2px}
#tblist tr:nth-child(even){background:#F7F7F7}
 
#tblistItem tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblistItem tr td{text-align:left;padding:2px}
#tblistItem tr:nth-child(even){background:#F7F7F7}
 </style>
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    

    function myFunction1() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput1");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblist");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }


    $(document).ready(
   function () {


       $("#td_1").hide();
       $("#myInput1").hide();
       $("#<%=rdbSelect.ClientID %>").click(function () {
           $("#myInput1").show();
           $("#td_1").show();
       });
       $("#<%=rdbAll.ClientID %>").click(function () {
           $("#myInput1").hide();
           $("#td_1").hide();
       });


       $("#td_2").hide();
       $("#myInput").hide();
       $("#<%=rdbSelectGroup.ClientID %>").click(function () {
           $("#myInput").show();
           $("#td_2").show();
       });
       $("#<%=rdbAllGroup.ClientID %>").click(function () {
           $("#td_2").hide();
           $("#myInput").hide();
       });

       $("input[name ='department']").click(function () {
           var CompaniesId = $('input[name="department"]:checkbox:checked').map(function () {
               return this.value;
           }).get();

           $("#<%=hdnDept.ClientID %>").val(CompaniesId);
       });

       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>

<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblistItem");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }


    $(document).ready(
   function () {


       $("input[name ='group']").click(function () {
           var ItemsId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnGroup.ClientID %>").val(ItemsId);
       });

       $("#<%=rdbAllGroup.ClientID %>").change(
    function () {


        if ($("#<%=rdbAllGroup.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelectGroup.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelectGroup.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelectGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllGroup.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnDept" runat="server" />
<asp:HiddenField ID= "hdnGroup" runat="server" />

<div style="padding-top:30px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
     CURRENT STOCK REPORT </td></tr>
    
</table>


<table>
 <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
    
<tr>
<td valign="top" style="width:200px;padding-top:20px;padding-left:10px">
 

<table style="border:1px">

<tr>
<td>Item Type:</td><td><asp:DropDownList ID ="ddlItemType" Width="150px" runat="server"></asp:DropDownList></td>
</tr>

<tr>
<td>Godown:</td><td><asp:DropDownList ID ="ddlgodown" Width="150px" runat="server"></asp:DropDownList></td>
</tr>

<tr>
<td>Company:</td><td><asp:DropDownList ID ="ddlcompany" Width="150px" runat="server"></asp:DropDownList></td>
</tr>

  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px"><asp:RadioButton ID="rdbSelect"   runat="server" Text="Selected" />
    </td>  </tr>
    <tr>
    <td id="td_1">Search</td><td>  <input type="text" id="myInput1" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name"></td></tr>
 <tr><td class="headings" colspan="2" style="padding-top:20px">Department:</td></tr>
                      <tr>

                       <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:200px; overflow-y:scroll">
                      <table style="width:100%" id="tblist">
                      <asp:Literal ID="ltDepartments" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>

  <tr><td style="padding-top:10px"><asp:RadioButton ID="rdbAllGroup" name="All" runat="server" Text="All" Checked="True" />
                   </td> 
  <td style="padding-top:10px"><asp:RadioButton ID="rdbSelectGroup"   runat="server" Text="Selected" /></td>  </tr><tr>
   <td id="td_2">Search</td><td>  <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"></td></tr>
 <tr><td class="headings" colspan="2" style="padding-top:20px">Group:</td></tr>
                      <tr>

                      <td colspan="2" style="padding-top:20px">
                      <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll">
                      <table style="width:100%" id="tblistItem">
                      <asp:Literal ID="ltGroups" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td>
                     </tr>

                      <tr>          
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
</table>
</td> 
 <td>
  <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="800px">
    </dx:ReportViewer>

</td>
</tr>
</table>



<%--   <table width="1050px" style="background-color:gray;color:white">

   <tr><td>Choose Branch</td><td><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >                                  
         </asp:DropDownList></td></tr>

   <tr><td>Item Type:</td><td><asp:DropDownList ID ="ddlItemType" Width="150px" runat="server"></asp:DropDownList></td>
<td>Godown:</td><td><asp:DropDownList ID ="ddlgodown" Width="150px" runat="server"></asp:DropDownList></td>
<td>Company:</td><td><asp:DropDownList ID ="ddlcompany" Width="150px" runat="server"></asp:DropDownList></td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>
    </table>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="1050px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
    </dx:ReportViewer>--%>

</div>
</asp:Content>