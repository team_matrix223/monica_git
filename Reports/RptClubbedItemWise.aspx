﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptClubbedItemWise.aspx.cs" Inherits="Reports_RptClubbedItemWise" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(
function () {



    $("#btnPrint").click(
    function () {
        PrintGridData();
    }
    );

    function PrintGridData() {

       //var prtGrid = document.getElementById('<%=gvUserInfo.ClientID %>');
        var prtGrid = document.getElementById('dvle');
        prtGrid.border = 0;
        var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
        prtwin.document.write(prtGrid.outerHTML);
        prtwin.document.close();
        prtwin.focus();
        prtwin.print();
        prtwin.close();
    }


}
);
</script>

    <div style="padding-top:30px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">



 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
      ITEM WISE SALE CLUBBED REPORT </td></tr>
   
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="1050px" style="background-color:gray;color:white">
   
    <tr><td>Date From:</td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td><td>Date To:</td>
    <td><asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender> </td>
    <td>Report By:</td><td><asp:DropDownList ID="ddlval" runat="server"><asp:ListItem Text="Value">Value</asp:ListItem><asp:ListItem Text="Qty">Qty</asp:ListItem></asp:DropDownList></td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
            <td><input type="button" id="btnPrint" value="Print" onclick="PrintGridData()" /></td>
        <td><asp:Button ID="btnexport" runat="server"  Text="Export To Excel" 
            onclick="btnexport_Click"/></td>
            </tr>

          
    </table>
    

<div style="width:1050px;font-size:15px" id ="dvle">
<b></b><br /> 


<table>
<tr><td colspan="100%"><table><tr style="text-align:center"><td style="font-weight:bold;font-size:17px;padding-left:400px;text-align:center"><asp:Label ID="lblheaader1" runat="server"></asp:Label></td></tr></table></td></tr>
<tr><td colspan="100%"><table><tr><td style="font-weight:bold;font-size:17px;padding-left:400px;text-align:center"><asp:Label ID="lblheaader2" runat="server"></asp:Label></td></tr></table></td></tr>
<tr><td colspan="100%"><table><tr><td style="font-weight:bold;font-size:17px;padding-left:400px;text-align:center"><asp:Label ID="lblheaader3" runat="server"></asp:Label></td></tr></table></td></tr>
<tr><td colspan="100%"><table><tr><td style="font-weight:bold;font-size:17px;padding-left:400px;text-align:center"><asp:Label ID="lblheaader4" runat="server"></asp:Label></td></tr></table></td></tr>
<tr><td colspan="100%"><table><tr><td style="font-weight:bold;font-size:17px;padding-left:400px;text-align:center"><asp:Label ID="lblheaader5" runat="server"></asp:Label></td></tr></table></td></tr>
<tr><td colspan="100%"><table><tr><td style="font-weight:bolder;font-size:19px;padding-left:400px">ITEM WISE SALE CLUBBED REPORT</td></tr></table></td></tr>
<tr style="text-align:center;margin-left:150px"><td style="padding:5px;font-weight:bolder;font-size:15px">Date From:</td><td style="padding:2px;font-weight:bolder;font-size:15px"><asp:Label ID="lbldatefrom" runat="server" ClientIDMode="Static"></asp:Label></td>
<td style="padding:5px;font-weight:bolder;font-size:15px">Date To:</td><td style="padding:2px;font-weight:bolder;font-size:15px"><asp:Label ID="lbldateto" runat="server" ClientIDMode="Static"></asp:Label></td></tr></table>

<asp:GridView ID="gvUserInfo" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" HorizontalAlign="Left"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>


</div>


</div>
</asp:Content>

