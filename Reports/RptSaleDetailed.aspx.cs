﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class RptSaleDetailed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
        }
        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        Int32 BillType = 1;
        BillType = Convert.ToInt32(ddlbilltype.SelectedValue.ToString());
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        string BillMode = "";
        string SaleType = "";
        if (rdbAll.Checked == true)
        {
            BillMode = "All";
        }
        else if (rdbCash.Checked == true)
        {
            BillMode = "Cash";
        }
        else if (rdbCredit.Checked == true)
        {
            BillMode = "Credit";
        }
        else if (rdbCard.Checked == true)
        {
            BillMode = "CreditCard";
        }
        else if (rdbCard.Checked == true)
        {
            BillMode = "OnlinePayment";
        }

        if (rdbOrderSale.Checked == true)
        {
            SaleType = "OrderSale";
        }
        else if (rdbWidoutOrderSale.Checked == true)
        {
            SaleType = "WithoutOrderSale";
        }


        if (rdbDetailed.Checked == true)
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetAllForReport(BillType, BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId, SaleType);
            gvUserInfo.DataBind();
        }
        else
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetSaleDatedReport(BillType, BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId, SaleType);
            gvUserInfo.DataBind();
        }

        SqlDataReader dr = null;
        dr = new ReportDAL().GetAdvanceBooking("Cash", BranchId, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text));
        if (dr.HasRows)
        {
            while (dr.Read())
            {
                lblCash.Text = dr["Amount"].ToString();
            }
        }
        SqlDataReader dr1 = null;
        dr1 = new ReportDAL().GetAdvanceBooking("CreditCard", BranchId, Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text));
        if (dr1.HasRows)
        {
            while (dr1.Read())
            {
                lblCredit.Text = dr1["Amount"].ToString();
            }
        }





    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCASHMEMOSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

     
    }


    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}