﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptLedger.aspx.cs" Inherits="Reports_RptLedger" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<script type ="text/javascript">
    $(document).ready(
function () {




}
);
</script>
    <div style="padding-top:0px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
     CUSTOMER STOCK REPORT </td></tr>
    
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="1050px" style="background-color:gray;color:white">

       <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>


   <tr><td>Customer:</td><td><asp:DropDownList ID ="ddlCust" Width="150px" runat="server"></asp:DropDownList></td>
<td>Date From:</td><td><asp:TextBox ID ="txtDateFrom" runat="server"></asp:TextBox>
 <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender></td>
<td>Date To:</td><td><asp:TextBox ID ="txtDateTo" runat="server"></asp:TextBox>
 <asp:CalendarExtender ID="cc2" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender></td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>

 


    </table>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="1050px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
    </dx:ReportViewer>

</div>
</asp:Content>

