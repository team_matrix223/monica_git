﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RegularStockExpiryRpt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            BindGroup();
            BindDepartments();
            BindAllItems();
        }

        RegularExpiryStockRpt objBreakageExpiry = new RegularExpiryStockRpt(HdnReq.Value, txtDateFrom.Text, txtDateTo.Text, dditem.SelectedItem.Value, ddDepartment.SelectedItem.Value, ddGroup.SelectedItem.Value, Convert.ToInt32(ddlBranch.SelectedItem.Value));
        ReportViewer1.Report = objBreakageExpiry;
        CheckRole();

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPBILLCANCEL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

        void BindGroup()
    {

        ddGroup.DataSource = new RGodownsDAL().GetAllGroup();
        ddGroup.DataValueField = "Group_Id";
        ddGroup.DataTextField = "Group_Name";
        ddGroup.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Group--";
        li1.Value = "0";
        ListItem li2 = new ListItem();
        li2.Text = "--All--";
        li2.Value = "-1";
        ddGroup.Items.Insert(0, li1);
        ddGroup.Items.Insert(1, li2);

    }

        void BindDepartments()
        {

            ddDepartment.DataSource = new RGodownsDAL().GetAllDepartments();
            ddDepartment.DataValueField = "Prop_Id";
            ddDepartment.DataTextField = "Prop_Name";
            ddDepartment.DataBind();
            ListItem li1 = new ListItem();
            li1.Text = "--Choose Department--";
            li1.Value = "-2";
            ListItem li2 = new ListItem();
            li2.Text = "--All--";
            li2.Value = "-1";
            ddDepartment.Items.Insert(0, li1);
            ddDepartment.Items.Insert(1, li2);

        }

        void BindAllItems()
        {

            dditem.DataSource = new RGodownsDAL().GetAllItems();
            dditem.DataValueField = "item_code";
            dditem.DataTextField = "item_Name";
            dditem.DataBind();
            ListItem li1 = new ListItem();
            li1.Text = "--Choose Department--";
            li1.Value = "0";
            ListItem li2 = new ListItem();
            li2.Text = "--All--";
            li2.Value = "-1";
            dditem.Items.Insert(0, li1);
            dditem.Items.Insert(1, li2);

        }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}