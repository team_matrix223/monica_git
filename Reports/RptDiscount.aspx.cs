﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RptDiscount : System.Web.UI.Page
{
    int BranchId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

      
        if (!IsPostBack)
        {
           
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            BindUsers(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value));

            
        }




        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
          
        string Type = "Selected";

        if (rdbAll.Checked == true)
        {
            Type = "All";
        }
        
       
        if (txtDateFrom.Text != "")
        {
            RptDiscountUserWise objBreakageExpiry = new RptDiscountUserWise(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId,Type,Convert.ToInt16(ddlUsers.SelectedValue.ToString()));
            ReportViewer1.Report = objBreakageExpiry;
        }
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);
     

    }

    void BindUsers(int BranchId)
    {
        ddlUsers.DataSource = new UserBLL().GetAll(BranchId);
        ddlUsers.DataValueField = "UserNo";
        ddlUsers.DataTextField = "User_ID";
        ddlUsers.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Choose User--";
        li.Value = "0";
        ddlUsers.Items.Insert(0, li);

    }



    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPBILLCANCEL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindUsers(Convert.ToInt32(ddlBranch.SelectedValue));
    }
}