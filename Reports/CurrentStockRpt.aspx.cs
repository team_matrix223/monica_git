﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class CurrentStockRpt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlitem.DataSource = new RGodownsBLL().GetAllItemType();
            ddlitem.DataTextField = "Item_Type";
            ddlitem.DataValueField = "Item_Type";
            ddlitem.DataBind();

            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();
           
        }
    }

    [WebMethod]
    public static string BindGrid()
    {
        string Godown = new RGodownsBLL().GetOptions();
               
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            GodownOptions = Godown,
   

        };
        return ser.Serialize(JsonData);
    }


}