﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="billdetailedrpt.aspx.cs" Inherits="billdetailedrpt" %>


<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(
function () {



    $("#<%=rdbAll.ClientID %>").change(
    function () {

        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCash.ClientID %>").change(
    function () {

        if ($("#<%=rdbCash.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCredit.ClientID %>").change(
    function () {
        if ($("#<%=rdbCredit.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCard.ClientID %>").change(
    function () {
        if ($("#<%=rdbCard.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbOnline.ClientID %>").change(
    function () {
        if ($("#<%=rdbOnline.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
        }


    }
    );
 

   


}
);
</script>

    <div style="padding-top:30px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="800px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
      BILL DETAILED REPORT </td></tr>
   
</table>


                        


<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="800px" style="background-color:gray;color:white">
    <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
    
    <tr><td>Date From:</td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td><td>Date To:</td>
    <td><asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender> </td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
            <td></td>
            </tr>
            <tr>
            <td>
            </td>
            <td>
            </td>
            
            </tr>
            <tr> 
       <td>Choose Bill Mode</td><td><asp:RadioButton ID="rdbAll"  name ="all" runat="server" Text="All" Checked="True" /></td>
          <td><asp:RadioButton ID="rdbCash" name ="all"  runat="server" Text="Cash" /></td>
             <td><asp:RadioButton ID="rdbCredit" name ="all"  runat="server" Text="Credit" /></td>
            
            <td></td>
            <td></td> </tr>
               <tr> 
       <td></td><td></td>
          
           <td><asp:RadioButton ID="rdbCard" name ="all"  runat="server" Text="Credit Card" /></td>
               <td><asp:RadioButton ID="rdbOnline" name ="all"  runat="server" Text="Online Payment" /></td>
               <td></td>
               <td></td> </tr>
             
    </table>
    

        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>


</div>
</asp:Content>

