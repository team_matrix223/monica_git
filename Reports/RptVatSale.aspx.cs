﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;

public partial class Reports_RptVatSale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
        }
        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        Int32 BillType = 1;
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        string BillMode = "Credit";
        string SaleType = "";
        BillType = 2;


        if (rdbDetailed.Checked == true)
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetAllVATForReport(BillType, BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId);
            gvUserInfo.DataBind();
        }
        else
        {
            gvUserInfo.DataSource = null;
            gvUserInfo.DataSource = new ReportDAL().GetSaleDatedReport(BillType, BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId, SaleType);
            gvUserInfo.DataBind();
        }

      

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCASHMEMOSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}