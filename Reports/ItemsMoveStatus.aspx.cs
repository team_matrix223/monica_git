﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Reports_ItemsMoveStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            BindGride();
        }

        //ItemsMoveReport objBreakageExpiry = new ItemsMoveReport(txtDateFrom.Text, txtDateTo.Text, Convert.ToInt32(ddlBranch.SelectedItem.Value),rdo_statustype.SelectedValue);
        //ReportViewer1.Report = objBreakageExpiry;
    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void BindGride()
    {
        Connection conn = new Connection();

        using (SqlConnection con=new SqlConnection (conn.sqlDataString))
        {

            SqlCommand cmd = new SqlCommand("strp_slow_mov_items", con);
            cmd.Parameters.AddWithValue("@req", null);
            cmd.Parameters.AddWithValue("@from_date", txtDateFrom.Text);
            cmd.Parameters.AddWithValue("@to_date", txtDateTo.Text);
            cmd.Parameters.AddWithValue("@branch_id", Convert.ToInt32(ddlBranch.SelectedItem.Value));
            cmd.Parameters.AddWithValue("@item_status", rdo_statustype.SelectedValue);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            gv_display.DataSource = dt;
            gv_display.DataBind();
        }
    
    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        BindGride();
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}