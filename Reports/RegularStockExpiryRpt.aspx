﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RegularStockExpiryRpt.aspx.cs" Inherits="Reports_RegularStockExpiryRpt" %>



<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <style>


        td {
            text-align:center;
        }

        tr th {
            text-align:center;
            background-color: #92bdd6!important;
            font-weight: bold;

        }

    </style>
<script src="../js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js"></script>
    <link href="../css/bootstrap.css" rel="stylesheet" />


<%--<script type ="text/javascript">
    $(document).ready(
function () {
    $(".sw_full").click();
    $("#dditem").change(function () {
        $("#ddDepartment").val("0");
        $("#ddGroup").val("0");
        $("#HdnReq").val("ItemWise");
    });

    $("#ddDepartment").change(function () {
        $("#dditem").val("0");
        $("#ddGroup").val("0");
        $("#HdnReq").val("DepartmentWise");
    });

    $("#ddGroup").change(function () {
        $("#dditem").val("0");
        $("#ddDepartment").val("0");
        $("#HdnReq").val("GroupWise");
    });
});
</script>--%>
    <asp:HiddenField ID="HdnReq" ClientIDMode="Static" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            
            <ContentTemplate>
                <asp:UpdateProgress id="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="https://icon-library.net/images/ajax-loading-icon/ajax-loading-icon-2.jpg" Width=10% AlternateText="Loading ..." ToolTip="Loading ..." style="padding: 10px;position:fixed;top:45%;left:45%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
    <div style="padding-top:0px;" >

<table style="margin-bottom:5px;text-align:center" width="85%">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
    <span runat="server" id="reporthead">MIS Report</span> </td></tr>
    
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>

   <div class="row">

              <div class="col-lg-2" style="padding-right: 0px;">  Branch <asp:DropDownList class="form-control" id="ddlBranch" ClientIDMode="Static" runat="server" placeholder="Choose Branch">
                                  
                                    </asp:DropDownList></div>

        <div class="col-lg-2" style="width: 10%;padding-right: 0px;padding-left: 0;">  Type <asp:DropDownList class="form-control" id="ddType" ClientIDMode="Static" runat="server" placeholder="Choose Branch">
                                  <asp:ListItem Value="Expiry">Expiry</asp:ListItem>
                       <asp:ListItem Value="Sale">Sale</asp:ListItem>
                       <asp:ListItem Value="Issue">Issue</asp:ListItem>
                                    </asp:DropDownList></div>

                    <div class="col-lg-2" style="width: 13%;padding-right: 0px;padding-left: 0;">Month<asp:DropDownList class="form-control" id="Ddmonth" ClientIDMode="Static" runat="server" placeholder="Choose Month">
                                  
                                    </asp:DropDownList></div>
                                    
                            <div class="col-lg-2" style="padding-right: 0px;padding-left: 0;">Item  <asp:DropDownList AutoPostBack="true" CssClass="form-control" id="dditem"  ClientIDMode="Static" runat="server" placeholder="Choose Branch" OnSelectedIndexChanged="dditem_SelectedIndexChanged">
                                  
                                    </asp:DropDownList></div>        

                                                        
                           <div class="col-lg-2" style="padding-right: 0px;padding-left: 0;">Department <asp:DropDownList AutoPostBack="true"  ClientIDMode="Static" CssClass="form-control" id="ddDepartment" runat="server" placeholder="Choose Branch" OnSelectedIndexChanged="ddDepartment_SelectedIndexChanged">
                                  
                                    </asp:DropDownList></div>    
       
               <div class="col-lg-2" style="padding-right: 0px;padding-left: 0;">

                    Group <asp:DropDownList CssClass="form-control"  ClientIDMode="Static" AutoPostBack="true"  id="ddGroup" runat="server" placeholder="Choose Branch" OnSelectedIndexChanged="ddGroup_SelectedIndexChanged">
                                  
                                    </asp:DropDownList>    
               </div>

              <div class="col-lg-2" style="width: 9%;">
                
        <asp:Button ID="btnGetRecords" runat="server" CssClass="btn-default"  Text="Submit" 
            onclick="btnGetRecords_Click" style="height: 35px;margin-top: 20px;"/>
                   </div>

                                </div> 
        <div class="row col-lg-10" style="margin-top: 18px;">

            <asp:Label ID="lblGroupHeader" runat="server" style="font-weight: bold;"></asp:Label>
        </div>

    <div  style="margin-top: 20px;">


        <asp:GridView ID="gv_display" runat="server" CssClass="table table-bordered table-striped" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="15">

        </asp:GridView>
    </div>


</div>
                                
            </ContentTemplate>
        </asp:UpdatePanel>


</asp:Content>

