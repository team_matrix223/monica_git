﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RptPurchaseprev : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string GrnNo = Request.QueryString["GrnNo"];
        int BranchID = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        RptPurchase objBreakageExpiry = new RptPurchase(GrnNo, BranchID);
        ReportViewer1.Report = objBreakageExpiry;
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}