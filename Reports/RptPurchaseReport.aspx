﻿<%@ Page Language="C#" MasterPageFile="~/Reports/ReportPage.master" CodeFile="RptPurchaseReport.aspx.cs" Inherits="Reports_RptPurchaseReport" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type ="text/javascript">



    $(document).ready(
function () {

    //    $("#<%=txtBillFrom.ClientID %>").prop('disabled', true);
    //    $("#<%=txtBillTo.ClientID %>").prop('disabled', true);
    $("#<%=rdbStateWise.ClientID %>").change(
    function () {


        if ($("#<%=rdbStateWise.ClientID %>").prop('checked') == true) {
            $("#<%=rdbPartyWise.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").prop('disabled', true);
            //            $("#<%=txtBillTo.ClientID %>").prop('disabled', true);
            $("#<%=txtDateFrom.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateTo.ClientID %>").removeAttr('disabled');

        }


    }
    );
    $("#<%=rdbPartyWise.ClientID %>").change(
    function () {

        if ($("#<%=rdbStateWise.ClientID %>").prop('checked') == true) {


            $("#<%=rdbStateWise.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").removeAttr('disabled');
            //            $("#<%=txtBillTo.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateFrom.ClientID %>").prop('disabled');
            $("#<%=txtDateTo.ClientID %>").prop('disabled');

        }


    }
    );



}
);
</script>
    <div style="padding-top:0px;padding-left:90px;" >

<table style="margin-bottom:5px;text-align:center" width="800px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
     PURCHASE WISE SALE REPORT </td></tr>
    
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="800px" style="background-color:gray;color:white">
   <tr>
  <td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px; margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
                                    <tr><td><asp:RadioButton ID="rdbDate" Checked="true" name ="all"   runat="server" Text="DateWise"  Visible="false"/></td><td>
                                    <asp:RadioButton ID="rdbBill" name ="all"   runat="server" Text="BillWise" Visible="false" /></td></tr>
   
                                       <tr><td><asp:RadioButton ID="rdbStateWise" Checked="true" name ="all"   runat="server" Text="StateWise" /></td><td>
                                    <asp:RadioButton ID="rdbPartyWise" name ="all"   runat="server" Text="PartyWise" /></td></tr>
    <tr><td>Date From:</td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td><td>Date To:</td>
    <td><asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender></td><td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>
    <tr><td></td><td><asp:TextBox ID="txtBillFrom" Visible="false" runat="server" 
            Width="150px"></asp:TextBox>
             
            </td><td></td>
    <td><asp:TextBox ID="txtBillTo" runat="server" Visible="false" Width="150px"></asp:TextBox></td><td>
        </td></tr>




    </table>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>



