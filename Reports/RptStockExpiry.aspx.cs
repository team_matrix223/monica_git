﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RptStockExpiry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {



        if (!IsPostBack)
        {
            BindBranches();
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "--Select--";
          



            ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
          

            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();
            ddlcompany.Items.Insert(0, li);

            ddlDepartment.DataSource = new RGodownsBLL().GetAllDepartment();
            ddlDepartment.DataTextField = "Prop_Name";
            ddlDepartment.DataValueField = "Prop_ID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, li);

            ddlItems.DataSource = new ItemMasterBLL().GetAllItems();
            ddlItems.DataTextField = "Item_Name";
            ddlItems.DataValueField = "Item_Code";
            ddlItems.DataBind();
            ddlItems.Items.Insert(0, li);

        }



        CheckRole();
       
        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);

        Int32 CompanyId = Convert.ToInt32(ddlcompany.SelectedValue);
        Int32 Department = Convert.ToInt32(ddlDepartment.SelectedValue);
        string Item = ddlItems.SelectedValue.ToString();

        string Type = "";
        if (rdbFinished.Checked == true)
        {
            Type = "3";
        }
        else if (rdbSemiFinished.Checked == true)
        {
            Type = "5";
        }
        else if (rdbRaw.Checked == true)
        {
            Type = "1";
        }

        string TypeWise = "";
        if (rdbAllcom.Checked == true)
        {
            TypeWise = "ALL";
        }
        else if (rdbSelCompany.Checked == true)
        {
            TypeWise = "Company";
        }
        else if (rdbSelDept.Checked == true)
        {
            TypeWise = "Department";
        }
        else if (rdbItem.Checked == true)
        {
            TypeWise = "Item";
        }


        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
        if (txtDateFrom.Text != "")
        {
            if (rdbRefWise.Checked == true)
            {
                RptExpiry objBreakageExpiry = new RptExpiry(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), Type, TypeWise, GodownId, Department, CompanyId, BranchId, Item);
                ReportViewer1.Report = objBreakageExpiry;
            }
            else
            {
                RptStockConsolidate objBreakageExpiry = new RptStockConsolidate(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), Type, TypeWise, GodownId, Department, CompanyId, BranchId,Item);
                ReportViewer1.Report = objBreakageExpiry;
            }
        }


    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPSTOCKEXPIRY));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }


    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
       


        
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

   
}