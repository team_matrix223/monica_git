﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptStockExpiry.aspx.cs" Inherits="RptStockExpiry" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type ="text/javascript">
    $(document).ready(
function () {







    $("#<%=rdbFinished.ClientID %>").change(
    function () {

        if ($("#<%=rdbFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSemiFinished.ClientID %>").change(
    function () {
        if ($("#<%=rdbSemiFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbRaw.ClientID %>").change(
    function () {
        if ($("#<%=rdbRaw.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
        }


    }
    );





    $("#<%=rdbAllcom.ClientID %>").change(
    function () {

        if ($("#<%=rdbAllcom.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSelCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbSelDept.ClientID %>").prop('checked', false);
            $("#<%=rdbItem.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSelCompany.ClientID %>").change(
    function () {
        if ($("#<%=rdbSelCompany.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllcom.ClientID %>").prop('checked', false);
            $("#<%=rdbSelDept.ClientID %>").prop('checked', false);
            $("#<%=rdbItem.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSelDept.ClientID %>").change(
    function () {
        if ($("#<%=rdbSelDept.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllcom.ClientID %>").prop('checked', false);
            $("#<%=rdbSelCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbItem.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbItem.ClientID %>").change(
    function () {
        if ($("#<%=rdbItem.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllcom.ClientID %>").prop('checked', false);
            $("#<%=rdbSelCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbSelDept.ClientID %>").prop('checked', false);
        }


    }
    );




    $("#<%=rdbRefWise.ClientID %>").change(
    function () {

        if ($("#<%=rdbRefWise.ClientID %>").prop('checked') == true) {

            $("#<%=rdbConsolidate.ClientID %>").prop('checked', false);
           
        }


    }
    );
    $("#<%=rdbConsolidate.ClientID %>").change(
    function () {
        if ($("#<%=rdbConsolidate.ClientID %>").prop('checked') == true) {

            $("#<%=rdbRefWise.ClientID %>").prop('checked', false);
          
        }


    }
    );

}
);
</script>
    <div style="padding-top:0px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
      STOCK EXPIRY REPORT </td></tr>
    
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="1050px" style="background-color:gray;color:white">


        <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:250px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td><td></td><td></td><td></td></tr>

            <tr><td>Choose Type:</td><td><asp:RadioButton ID="rdbFinished"  name ="all" 
                    runat="server" Text="Finished" Checked="True" AutoPostBack="True" /></td>
            <td><asp:RadioButton ID="rdbSemiFinished"  name ="all" runat="server" 
                    Text="Semi-Finished" /></td>
            <td><asp:RadioButton ID="rdbRaw"  name ="all" runat="server" Text="Raw"  /></td><td></td></tr>

                    
            <tr><td>Choose Type:</td><td><asp:RadioButton ID="rdbAllcom"  name ="Company" 
                    runat="server" Text="All Company" Checked="True"  /></td>
            <td><asp:RadioButton ID="rdbSelCompany"  name ="Company" runat="server" 
                    Text="Selected Company"  /></td>
            <td><asp:RadioButton ID="rdbSelDept"  name ="Company" runat="server" Text="Selected Department" 
                     /></td>
            <td><asp:RadioButton ID="rdbItem"  name ="Item" runat="server" Text="Selected Item" 
                     /></td></tr>




               
   <tr>
   
   <td>Select Godown</td><td><asp:DropDownList ID ="ddlgodown" Width="150px" runat="server"></asp:DropDownList></td>
   <td>
       <asp:DropDownList ID ="ddlcompany" Width="150px" runat="server"></asp:DropDownList></td><td>
           <asp:DropDownList ID ="ddlDepartment" Width="150px" runat="server"></asp:DropDownList></td>
           <td>
           <asp:DropDownList ID ="ddlItems" Width="150px" runat="server"></asp:DropDownList></td>


</tr>

<tr><td>Report:</td><td><asp:RadioButton ID="rdbRefWise"  name ="Report" 
                    runat="server" Text="Ref No Wise Detailed" Checked="True"  /></td>
            <td><asp:RadioButton ID="rdbConsolidate"  name ="Report" runat="server" 
                    Text="Consolidate"  /></td>
            <td></td><td></td></tr>

       <tr><td>Date From:</td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td><td>Date To:</td>
    <td><asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender></td>
    <td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td></tr>
 


    </table>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="1050px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
    </dx:ReportViewer>

</div>
</asp:Content>





