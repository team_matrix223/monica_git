﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraPrinting;
using System.IO;

public partial class customerretailinvoicerpt : System.Web.UI.Page
{
    public string BillNowPrefix { get { return Request.QueryString["BillNowPrefix"] != null ? Convert.ToString(Request.QueryString["BillNowPrefix"]) : string.Empty; } }
    public string Type { get { return Request.QueryString["Type"] != null ? Convert.ToString(Request.QueryString["Type"]) : string.Empty; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        using (MemoryStream ms = new MemoryStream())
        {
            rptCustomerRetailInvoice r = new rptCustomerRetailInvoice(BillNowPrefix, Branch, Type);
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);
            Page.Response.End();
        }
    }
  

}