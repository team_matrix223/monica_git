﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptVatSale.aspx.cs" Inherits="Reports_RptVatSale" %>
<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(
function () {



    $("#btnPrint").click(
    function () {
        PrintGridData();
    }
    );

    function PrintGridData() {

        //var prtGrid = document.getElementById('<%=gvUserInfo.ClientID %>');
        var prtGrid = document.getElementById('dvle');
        prtGrid.border = 0;
        var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
        prtwin.document.write(prtGrid.outerHTML);
        prtwin.document.close();
        prtwin.focus();
        prtwin.print();
        prtwin.close();
    }

   
   
  

 

    $("#<%=rdbDetailed.ClientID %>").change(
    function () {
        if ($("#<%=rdbDetailed.ClientID %>").prop('checked') == true) {
            $("#<%=rdbDated.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbDated.ClientID %>").change(
    function () {
        if ($("#<%=rdbDated.ClientID %>").prop('checked') == true) {
            $("#<%=rdbDetailed.ClientID %>").prop('checked', false);
        }


    }
    );

   

 





}
);
</script>

    <div style="padding-top:30px;padding-left:30px;" >

<table style="margin-bottom:5px;text-align:center" width="1050px">
 <tr><td colspan="100%" style="background-color:Silver;color:Black;font-weight:bold;border:solid 2px gray;text-transform:uppercase"> 
      CASHMEMO WISE SALE REPORT </td></tr>
   
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <table width="1050px" style="background-color:gray;color:white">
     <tr><td>Choose Branch</td><td>  <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td></tr>
   
    <tr><td>Date From:</td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox>
            
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
            </td><td>Date To:</td>
    <td><asp:TextBox ID="txtDateTo" runat="server" Width="100px"></asp:TextBox> <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender> </td>
<td>
        <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
            <td><input type="button" id="btnPrint" value="Print" onclick="PrintGridData()" /></td>
        <td><asp:Button ID="btnexport" runat="server"  Text="Export To Excel" 
            onclick="btnexport_Click"/></td>
            </tr>

          
              <tr style="display:none"> 
       <td>Choose Option</td><td><asp:RadioButton ID="rdbDetailed"   runat="server" Text="Detailed" Checked="True" /></td>
          <td><asp:RadioButton ID="rdbDated"  runat="server" Text="Dated" /></td>
            
             <td></td><td></td><td></td></tr>


            
    </table>
    

<div style="width:1050px;overflow:scroll;height:400px" id ="dvle">
<b></b><br /> 
<asp:GridView ID="gvUserInfo" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>



</div>


</div>
</asp:Content>

