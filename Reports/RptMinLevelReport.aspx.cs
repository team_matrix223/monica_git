﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RptMinLevelReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "--Select--";




            ddlGodown.DataSource = new RGodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_id";
            ddlGodown.DataBind();


            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();
            ddlcompany.Items.Insert(0, li);

            ddlDepartment.DataSource = new RGodownsBLL().GetAllDepartment();
            ddlDepartment.DataTextField = "Prop_Name";
            ddlDepartment.DataValueField = "Prop_ID";
            ddlDepartment.DataBind();
            ddlDepartment.Items.Insert(0, li);


            ddlGroup.DataSource = new RGodownsBLL().GetAllGroup();
            ddlGroup.DataTextField = "Group_Name";
            ddlGroup.DataValueField = "Group_ID";
            ddlGroup.DataBind();
            ddlGroup.Items.Insert(0, li);


            ddlSubGroup.DataSource = new RGodownsBLL().GetAllSubGroup();
            ddlSubGroup.DataTextField = "SGroup_Name";
            ddlSubGroup.DataValueField = "SGroup_ID";
            ddlSubGroup.DataBind();
            ddlSubGroup.Items.Insert(0, li);




        }
        CheckRole();
       
        string Type = "";
        if (rdbFinished.Checked == true)
        {
            Type = "3";
        }
        else if (rdbSemiFinished.Checked == true)
        {
            Type = "5";
        }
        else if (rdbRaw.Checked == true)
        {
            Type = "1";
        }
        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        string Option = "";
        string OptionValue = "";
        if (rdbAll.Checked == true)
        {
            Option = "All";
            OptionValue = "0";

        }
        else if (rdbcompany.Checked == true)
        {
            Option = "Company";
            OptionValue = ddlcompany.SelectedValue;
        }
        else if (rdbGroup.Checked == true)
        {
            Option = "Group";
            OptionValue = ddlGroup.SelectedValue;
        }
        else if (rdbSubGroup.Checked == true)
        {
            Option = "SubGroup";
            OptionValue = ddlSubGroup.SelectedValue;
        }
        else if (rdbDept.Checked == true)
        {
            Option = "Department";
            OptionValue = ddlDepartment.SelectedValue;
        }


        MinLevelReport objBreakageExpiry = new MinLevelReport(ddlGodown.SelectedValue.ToString(), Option, OptionValue, BranchId, Type);
        ReportViewer1.Report = objBreakageExpiry;

       
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPMINLEVEL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
       
    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}