﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RptLedger : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            BindBranches();
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            ListItem li = new ListItem();
            li.Text = "None";
            li.Value = "0";
            ddlCust.DataSource = new CustomerBLL().GetAllCreditCustomer();
            ddlCust.DataTextField = "CNAME";
            ddlCust.DataValueField = "CCODE";
            ddlCust.DataBind();
            ddlCust.Items.Insert(0, li);
        }

        CheckRole();
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
       

        
        string Customername = ddlCust.SelectedValue.ToString();
        string custname = ddlCust.SelectedItem.ToString();

        rptLedger objBreakageExpiry = new rptLedger(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), Customername, BranchId,custname);
        ReportViewer1.Report = objBreakageExpiry;
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCUSTLEDGER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}