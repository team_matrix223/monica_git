﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Reports_RptClubbedCurrentStock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            ddlItemType.DataSource = new RGodownsBLL().GetAllItemType();
            ddlItemType.DataTextField = "Item_Type";
            ddlItemType.DataValueField = "Item_TypeID";
            ddlItemType.DataBind();

       
            BindDepartents();
         
            BindGroups();

        }

        CheckRole();

        
        string deptqry = "";

        if (rdbAll.Checked == true)
        {
            deptqry = "All";
        }
        else
        {
            deptqry = hdnDept.Value;
        }

        string groupqry = "";

        if (rdbAllGroup.Checked == true)
        {
            groupqry = "All";
        }
        else
        {
            groupqry = hdnGroup.Value;
        }

      
        String ItemType = ddlItemType.SelectedValue;
        gvUserInfo.DataSource = null;
        gvUserInfo.DataSource = new ReportDAL().GetCurrentStockClubbed(ItemType,groupqry,deptqry);
        gvUserInfo.DataBind();



    }

   

    void BindDepartents()
    {
        ltDepartments.Text = new RGodownsBLL().GetDepartmentHtml();
    }

    void BindGroups()
    {
        ltGroups.Text = new RGodownsBLL().GetGroupHtml();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCURRENTSTOCK));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }


    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

}