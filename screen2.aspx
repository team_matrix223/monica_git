﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="screen2.aspx.cs" Inherits="screen1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


  <meta name="viewport" content="width=device-width, initial-scale=1,  maximum-scale=1.0, user-scalable=no" />
    <title>Super Store Billing</title>
    <script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>
    <style type="text/css">
        .box
        {
            float: left;
            border: solid 1px silver;
            margin: 1px;
            padding: 2px;
            cursor: pointer;
            width:140px;
            text-align:center;
            height:115px;
            font-size:11px;
        }
    </style>
    <script language="javscript" type="text/javascript">

      

        var m_ItemId = 0;
        var m_ItemName = "";
        var m_Qty = 0;
        var m_Price = 0;


        var ProductCollection = [];
        function clsproduct() {
            this.ItemId = 0;
            this.ItemName = "";
            this.Qty = 0;
            this.Price = 0;

        }

        function Bindtr() {

            var DiscountAmt = 0;
            var TaxAmt = 0;
            var Total = 0;
            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            for (var i = 0; i < ProductCollection.length; i++) {


                var tr = "<tr><td >" + ProductCollection[i]["ItemId"] + "</td><td >" + ProductCollection[i]["ItemName"] + "</td><td><div id='btnMinus'>-</div> " + ProductCollection[i]["Qty"] + " <div id='btnPlus' >+</div> </td><td>" + ProductCollection[i]["Price"] + "</td><td><i id='dvClose'><img src='images/remove.png'/></i> </td></tr>";
                $("#tbProductInfo").append(tr);
                var fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

                Total =  Total+ fPrice;
            }

           
            $("div[id='dvsbtotal']").html(" Rs. " + Total);
            if (Number(DiscountAmt) == 0) {
                $("#trDiscount").css({ "display": "none" });
            }
            else {
                $("#trDiscount").css({ "display": "block" });
            }
            if (Number(TaxAmt) == 0) {
                $("#trTax").css({ "display": "none" });
            }
            else {
                $("#trTax").css({ "display": "block" });
            }
            $("div[id='dvdiscount']").html(" Rs. " + DiscountAmt);
            $("div[id='dvTax']").html(" Rs. " + TaxAmt);
            $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total) + Number(TaxAmt) - Number(DiscountAmt)));
        }

        function addToList(ProductId, Name, Price) {
            m_ItemId = ProductId;
            m_Price = Price;
            m_ItemName = Name;
            m_Qty = 1;
            var item = $.grep(ProductCollection, function (item) {
                return item.ItemId == m_ItemId;
            });

            if (item.length) {

                alert("Product Alredy Added in shop list");

                return;

            }


            TO = new clsproduct();

            TO.ItemId = m_ItemId;
            TO.ItemName = m_ItemName;
            TO.Qty = 1;
            TO.Price = m_Price;

            ProductCollection.push(TO);
            Bindtr();


           
              


        }



        function Search(CatId, Keyword) {

  
            $.ajax({
                type: "POST",
                data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
                url: "screen1.aspx/AdvancedSearch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#products").html(obj.productData);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );


        }

//........................................

        $(document).on("click", "#btnPlus", function (event) {


            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];

            var Mode = "Plus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];
            var fQty = Number(Qty) + 1;
          

         
          
            ProductCollection[RowIndex]["Qty"] = fQty;

            Bindtr();

        });


        $(document).on("click", "#btnMinus", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];
            var Mode = "Minus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];

            var fQty = Number(Qty) - 1;
            ProductCollection[RowIndex]["Qty"] = fQty;
            if (fQty == "0") {
                ProductCollection.splice(RowIndex, 1);
            }   

            Bindtr();


        });



//............................................




        $(document).ready(
        function () {


            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {

                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");

                }
                Bindtr();

            });





            $("#txtSearch").keyup(
            function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {


                    var Keyword = $("#txtSearch");
                    if (Keyword.val().trim() != "") {

                        Search(0, Keyword.val());
                    }
                    else {
                        Keyword.focus();
                    }


                }

            }

            );

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen1.aspx/BindCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#categories").html(obj.categoryData);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );




            $("#btnSearch").click(
        function () {

            var Keyword = $("#txtSearch");
            if (Keyword.val().trim() != "") {

                Search(0, Keyword.val());
            }
            else {
                Keyword.focus();
            }

        }
        );










        });
    
    </script>
</head>
<body>
    <div>
        <table>
            <tr>
                <td colspan="100%" style="border-bottom: dotted 1px silver; padding-bottom: 10px">
                    <div id="categories">
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:450px;vertical-align:top;text-align:center">

                                <table class="tablesorter" style = "width: 100%"  id="tbProductInfo"  cellspacing="0"> 
			<thead> 
				<tr> 
   					  <th style="width:100px;">
                                            ProductId
                                        </th>
                                        <th  style="width:200px" >
                                          Name
                                        </th>
                                       
                                        <th  style="width:150px" >
                                            Qty
                                        </th>
                                     
                                         <th  style="width:100px" >
                                            Price
                                        </th>
                                        <th  style="width:100px">Delete</th>
				</tr> 
			</thead> 
			<tbody>
       
       
			</tbody> 
			</table>
                </td>
                <td>
                <table>
              <tr>
              <td>  <div id="products" style="max-height:400px;overflow-y:scroll;">
                    </div></td>
              </tr>


              <tr>
              <td><table>
                        <tr>
                            <td>
                                <input type="text" id="txtSearch" width="50%" />
                            </td>
                            <td>
                                <input type="button" id="btnSearch" value="Search"/>
                            </td>
                        </tr>
                    </table></td>
              </tr>

              <tr>
              
                <td ><table>
                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr><td style="width:60%">Gross Amount:</td><td style="width:100px"><div id ="dvsbtotal"></div></td></tr>

                     <tr ><td colspan="100%"><div id="trDiscount"><table><tr>
                     <td  style="width:60%">Discount:</td><td style="width:100px"><div id ="dvdiscount"></div></td>
                     </tr></table></div></td></tr>
                        <tr ><td colspan="100%"><div id="trTax"><table><tr>
                        <td  style="width:60%">Tax Amount:</td><td style="width:100px"><div id ="dvTax"></div></td>
                        </tr></table></div></td></tr>
                     <tr><td  style="width:60%">Net Amount:</td><td style="width:100px"><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                
                        
                     </tr>

                     </table></td>
              
              </tr>
                </table>

                  
                  
                    
                </td>
            </tr>
        </table>


    </div>
</body>
</html>
