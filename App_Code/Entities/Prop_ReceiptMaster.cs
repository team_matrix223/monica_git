﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Prop_ReceiptMaster
/// </summary>
public class Prop_ReceiptMaster
{

   

    public int Receipt_No { get; set; }
    public DateTime Receipt_Date { get; set; }
    public string strChequedate { get { return Receipt_Date.ToString("d"); } }
    public string CCode { get; set; }
    public string CName { get; set; }
    public decimal Amount { get; set; }
    public string ModeOfPayment { get; set; }
    public string Bank_Name { get; set; }
    public string CHQNO { get; set; }
    public int userno { get; set; }
    public int BranchId { get; set; }
    public string Id { get; set; }
    public string Type { get; set; }
    public decimal AdvAdjustedReceipt_No { get; set; }
   


	public Prop_ReceiptMaster()
	{
        Receipt_No = 0;
        Receipt_Date = DateTime.Now;
        CCode = string.Empty;
        CName = string.Empty;
        Amount = 0;
        ModeOfPayment = string.Empty;
        Bank_Name = string.Empty;
        CHQNO = string.Empty;
        userno = 0;
        BranchId = 0;
        Id = string.Empty;
        Type = string.Empty;
        AdvAdjustedReceipt_No = 0;
        
	}
}