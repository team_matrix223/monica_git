﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Banks
/// </summary>
public class Banks
{

    public int Bank_ID { get; set; }
    public string Bank_Name { get; set; }
    public string CCODE { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }

	public Banks()
	{
        Bank_ID = 0;
        Bank_Name = "";
        CCODE = "";
        UserId = 0;
        IsActive = false;
	}
}