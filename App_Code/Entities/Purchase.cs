﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Purchase
/// </summary>
public class Purchase
{
 
    public int GrnNo { get; set; }
    public DateTime GRNDate { get; set; }
    public string strgrndate { get { return GRNDate.ToString("d"); } }
    public string BillNo { get; set; }
    public DateTime BillDate { get; set; }
    public string strbilldate { get { return BillDate.ToString("d"); } }
    public string GRNo { get; set; }
    public DateTime GRDate { get; set; }
    public string strgrdate { get { return GRDate.ToString("d"); } }
    public string VehNo { get; set; }
    public bool Dis1InRs { get; set; }
    public bool Dis2InRs { get; set; }
    public bool Dis2AftDedDis1 { get; set; }
    public string IsLocal { get; set; }
   
    public bool TaxAfterDis1 { get; set; }
    public bool TaxAfterDis2 { get; set; }
    public string Remarks { get; set; }
    public decimal BillValue { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public bool Dis3AftDis1PDis2 { get; set; }
    public decimal Dis3P { get; set; }
    public decimal Dis3Amt { get; set; }
    public decimal TaxP { get; set; }
    public decimal TaxAmt { get; set; }
  
    public decimal TotalAmount { get; set; }
    public decimal ODisP { get; set; }
    public decimal ODisAmt { get; set; }
    public decimal DisplayAmount { get; set; }
    public decimal Adjustment { get; set; }
    public decimal NetAmount { get; set; }
    public bool Pass { get; set; }
    public int SupplierId { get; set; }
    public int GodownId { get; set; }
    public decimal ExciseAmt { get; set; }
    public decimal SurchrgAmt { get; set; }

    public string RawChk { get; set; }
     public string Purtype { get; set; }
     public string Tcs_AccCode { get; set; }
     public decimal tcs_Amount { get; set; }
     public decimal Tcstax { get; set; }
     public decimal surval { get; set; }
     public Int64 BranchId { get; set; }
     public int UserNo { get; set; }
     public string SupplierName { get; set; }
     public string BNF { get; set; }
     public decimal fright { get; set; }
    public Purchase()
    {
        BNF = "";
        GrnNo = 0;
        GRNDate = DateTime.Now;
        BillNo = string.Empty;
        BillDate = DateTime.Now;
        GRNo = string.Empty;
        GRDate = DateTime.Now;
        VehNo = string.Empty;
        Dis1InRs = false; Dis2InRs = false;
        Dis2AftDedDis1 = false; 
        IsLocal = string.Empty; 
        TaxAfterDis1 = false; 
        TaxAfterDis2 = false;
        Remarks = string.Empty;
        BillValue = 0;
        Dis1Amt = 0;
        Dis2Amt = 0;
        Dis3AftDis1PDis2 = false;
        Dis3P = 0;
        Dis3Amt = 0;
        TaxP = 0;
        TaxAmt = 0;
        TotalAmount = 0;
        ODisP = 0;
        ODisAmt = 0;
        DisplayAmount = 0; 
        Adjustment = 0;
        NetAmount = 0; 
        Pass = false; 
        SupplierId = 0; 
        GodownId = 0;
        ExciseAmt = 0;
        SurchrgAmt = 0;
        RawChk = string.Empty;
        Purtype = string.Empty;
        Tcs_AccCode = string.Empty;
        tcs_Amount = 0;
        Tcstax = 0;
        surval = 0;
        BranchId = 0;
        UserNo = 0;
        SupplierName = string.Empty;
        fright = 0;

    }
}