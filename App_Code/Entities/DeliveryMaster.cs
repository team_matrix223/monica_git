﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeliveryMaster
/// </summary>
public class DeliveryMaster
{
    
    public int Bill_No { get; set; }
    public DateTime Bill_Date { get; set; }
    public string strBillDate { get { return Bill_Date.ToString("d"); } }
    public string GR_No { get; set; }
    public DateTime GR_Date { get; set; }
    public string strGrDate { get { return GR_Date.ToString("d"); } }
    public string Veh_No { get; set; }
    public bool Dis1InRs { get; set; }
    public bool Dis2InRs { get; set; }
    public bool Dis2AftDedDis1 { get; set; }
    public string LocalOut { get; set; }
    public bool TaxAfterDis1 { get; set; }
    public bool TaxAfterDis2 { get; set; }
    public string Remarks { get; set; }
    public decimal Bill_Value { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public bool Dis3AftDis1PDis2 { get; set; }
    public decimal Dis3P { get; set; }
    public decimal Dis3Amt { get; set; }
    public decimal TaxP { get; set; }
    public decimal TaxAmt { get; set; }
    public decimal Total_Amount { get; set; }
    public decimal ODisP { get; set; }
    public decimal ODisAmt { get; set; }
    public decimal Display_Amount { get; set; }
    public decimal Adjustment { get; set; }
    public decimal Net_Amount { get; set; }
    public string CCODE { get; set; }
    public int Godown_ID { get; set; }
    public string Form_Name { get; set; }
    public DateTime Desp_Date { get; set; }
    public string strDespDate { get { return Desp_Date.ToString("d"); } }
    public string Ref_No { get; set; }
    public bool UpdateMaster { get; set; }
    public bool PASSING { get; set; }
    public string ChkRaw { get; set; }
    public string dr_grno { get; set; }
    public bool delchk { get; set; }
    public DateTime Ctime { get; set; }
    public DateTime Mtime { get; set; }
    public string Prefix { get; set; }
    public decimal Excise_Amt { get; set; }
    public decimal EduCess { get; set; }
    public decimal HSCess { get; set; }
    public string BillMode { get; set; }
    public string Dealer { get; set; }
    public int BranchId { get; set; }
    public string ReceivedBillNO { get; set; }
    public int UserNo { get; set; }
    public string BillPrefix { get; set; }
    public Int32 ForBranch { get; set; }
    public string Type { get; set; }
    public string BillNowPrefix { get; set; }

	public DeliveryMaster()
	{
        Bill_No =0; 
        Bill_Date=DateTime.Now; 
        GR_No = string.Empty;
        GR_Date = DateTime.Now;
        Veh_No = string.Empty; 
        Dis1InRs = false; 
        Dis2InRs= false ;
        Dis2AftDedDis1= false; 
        LocalOut = string.Empty;
        TaxAfterDis1 = false; 
        TaxAfterDis2 = false;  
        Remarks = string.Empty;
        Bill_Value=0;
        Dis1Amt=0; 
        Dis2Amt=0;
        Dis3AftDis1PDis2 = false;
        Dis3P= 0;
        Dis3Amt = 0; 
        TaxP = 0; 
        TaxAmt =0 ;
        Total_Amount = 0;
        ODisP = 0; 
        ODisAmt = 0; 
        Display_Amount = 0; 
        Adjustment = 0; 
        Net_Amount = 0; 
        CCODE = string.Empty ;
        Godown_ID =0;
        Form_Name = string.Empty;
        Desp_Date = DateTime.Now;
        Ref_No = string.Empty; 
        UpdateMaster = false; 
        PASSING = false; 
        ChkRaw = string.Empty; 
        dr_grno = string.Empty; 
        delchk = false; 
        Ctime = DateTime.Now; 
        Mtime = DateTime.Now; 
        Prefix = string.Empty;  
        Excise_Amt = 0;
        EduCess = 0;
        HSCess = 0;
        BillMode = string.Empty;
        Dealer = string.Empty;
        BranchId = 0;
        ReceivedBillNO = string.Empty;
        UserNo = 0;
        BillPrefix = string.Empty;
        ForBranch = 0;
        Type = string.Empty;
        BillNowPrefix = string.Empty;
	}
}