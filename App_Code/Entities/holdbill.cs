﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for holdbill
/// </summary>
public class holdbill:Connection
{

    public int Hold_No { get; set; }
    public DateTime Bill_Date { get; set; }
    public int Customer_ID { get; set; }
    public string Customer_Name { get; set; }
    public decimal Bill_Value { get; set; }
    public decimal Less_Dis_Amount { get; set; }
    public decimal Add_Tax_Amount { get; set; }
    public decimal Net_Amount { get; set; }
    public int User_ID { get; set; }
    public decimal BillMode { get; set; }
    public decimal DiscountPer { get; set; }
    public int BranchId { get; set; }

	public holdbill()
	{
        Hold_No = 0;
        Customer_ID = 0;
        Customer_Name = "";
        Bill_Date = DateTime.Now;
        Bill_Value = 0;
        Less_Dis_Amount = 0;
        DiscountPer = 0;
        Add_Tax_Amount = 0;
        Net_Amount = 0;
        User_ID = 0;
        BillMode = 0;
        BranchId = 0;
	}
}