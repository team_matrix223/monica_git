﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Tables
/// </summary>
public class Tables
{
    public int TableID { get; set; }
    public string TableName { get; set; }
    public DateTime ModifiedDate { get; set; }
    public Boolean Reserve { get; set; }
    public TimeSpan Stamp { get; set; }
    public string PosName { get; set; }

	public Tables()
	{
        TableID = 0;
        TableName = "";
        ModifiedDate = DateTime.Now;
        Reserve = false;
      
        PosName = "";
	}
}