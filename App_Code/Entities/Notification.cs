﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Notification
/// </summary>
public class Notification
{
    public int Id { get; set; }
    public string Notification1 { get; set; }
    public string Notification2 { get; set; }
    public string Declaration { get; set; }
    public string Range { get; set; }
    public string Division { get; set; }
    public string CommissionRate { get; set; }
    public string TINNO { get; set; }
    public string PANNO { get; set; }
    public string CERegn { get; set; }
    public int BranchId { get; set; }


	public Notification()
	{
        Id = 0;
        Notification1 = string.Empty;
        Notification2 = string.Empty;
        Declaration = string.Empty;
        Range = string.Empty;
        Division = string.Empty;
        CommissionRate = string.Empty;
        TINNO = string.Empty;
        PANNO = string.Empty;
        CERegn = string.Empty;
        BranchId = 0;

	}
}