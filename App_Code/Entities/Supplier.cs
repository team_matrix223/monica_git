﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Supplier
/// </summary>
public class Supplier
{

    public int Supplier_ID { get; set; }
    public string Prefix { get; set; }
    public string Supplier_Name { get; set; }
    public string Address_1 { get; set; }
    public string Address_2 { get; set; }
    public int Area_ID { get; set; }
    public int City_ID { get; set; }
    public int State_ID { get; set; }
    public string CST_No { get; set; }
    public DateTime  CST_Date   { get; set; }
    public string strCD { get { return CST_Date.ToString("d"); } }
    public string TIN_No { get; set; }
    public string TOT_No { get; set; }
    public string Cont_Person { get; set; }
    public string Cont_No { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }
    public bool IsActive { get; set; }
    public string CompanyList { get; set; }
	public Supplier()
	{
        CompanyList = string.Empty;
        Supplier_ID = 0;
        Prefix = "";
        Supplier_Name = "";
        Address_1 = "";
        Address_2 = "";
        Area_ID = 0;
        City_ID = 0;
        State_ID = 0;
        CST_No = "";
        CST_Date =Convert.ToDateTime("1/1/1900");
        TIN_No = "";
        TOT_No = "";
        Cont_Person = "";
        Cont_No = "";
        UserId = 0;
        IsActive = false;
        BranchId = 0;

	}
}