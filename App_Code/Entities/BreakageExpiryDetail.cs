﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseDetail
/// </summary>
public class BreakageExpiryDetail
{

    public int BreakageId { get; set; }
    public DateTime DateOfBeakage { get; set; }
    public int Qty { get; set; }
    public int StockInHand { get; set; }
    public int GodownId { get; set; }
    public int ItemCode { get; set; }
    public decimal Mrp { get; set; }

    public BreakageExpiryDetail()
    {
        BreakageId = 0;
        DateOfBeakage = DateTime.Now;
        Mrp = 0;
        Qty = 0;
        GodownId = 0;
        ItemCode = 0;
        StockInHand = 0;

    }
}