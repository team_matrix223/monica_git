﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeliveryDetail
/// </summary>
public class DeliveryDetail
{
    
    public string Bill_No { get; set; }
    //public DateTime Bill_Date { get; set; }
    //public string sBillDate { get { return Bill_Date.ToString("d"); } }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public string Unit { get; set; }
    public decimal CaseQty { get; set; }
    public decimal QtyInCase { get; set; }
    public decimal Qty { get; set; }
    public bool Scheme { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public decimal Dis1P { get; set; }
    public decimal Dis2P { get; set; }
    public decimal TaxP { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public decimal TaxAmt { get; set; }
    public decimal Dis3P { get; set; }
    public decimal Dis3Amt { get; set; }
    public decimal Godown_ID { get; set; }
    public decimal Sale_Rate { get; set; }
    public decimal RowNum { get; set; }
    public string MASTER_CODE { get; set; }
    public decimal QTY_TO_LESS { get; set; }
    public decimal Excise_duty { get; set; }
    public decimal Excise_Amt { get; set; }
    public decimal Qty_In_Case { get; set; }
    public decimal Stock { get; set; }
    public decimal Excise { get; set; }
    public int Abatement { get; set; }
  
	public DeliveryDetail()
	{
        Bill_No = string.Empty;
        //Bill_Date = DateTime.Now;
        Item_Code = string.Empty;
        Item_Name = string.Empty;
        Unit = string.Empty;
        CaseQty = 0;
        Qty = 0;
        Scheme = false;
        MRP = 0;
        Rate = 0;
        Amount = 0;
        Dis1P = 0;
        Dis2P = 0;
        TaxP = 0;
        Dis1Amt = 0;
        Dis2Amt = 0;
        TaxAmt = 0;
        Dis3P = 0;
        Dis3Amt = 0;
        Godown_ID = 0;
        Sale_Rate = 0;
        RowNum = 0;
        MASTER_CODE = string.Empty;
        QTY_TO_LESS = 0;
        Excise_duty = 0;
        Excise_Amt = 0;
        Qty_In_Case = 0;
        Stock = 0;
        Excise = 0;
        Abatement = 0;
	}
}