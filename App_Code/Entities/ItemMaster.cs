﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Areas
/// </summary>
public class ItemMaster

{

   public int ItemID { get; set; }
   public int COMPANY_ID { get; set; }
   public int GROUP_ID { get; set; }
   public string Master_Code { get; set; }
   public string Item_Code { get; set; }
   public string Item_Name { get; set; }
   public string Packing { get; set; }
   public string Bar_Code { get; set; }
   public string Short_Name1 { get; set; }
   public string Short_Name2 { get; set; }
   public string Sales_In_Unit { get; set; }
   public int Sales_In_Unit2 { get; set; }

   public decimal Qty_To_Less { get; set; }
   public decimal  Purchase_Rate { get; set; }
   public decimal  Sale_Rate  { get; set; }
   public decimal  Max_Retail_Price  { get; set; }
   public decimal  Mark_Up { get; set; }
   public decimal Qty_in_Case { get; set; }
   public decimal Max_Level { get; set; }
   public decimal Min_Level  { get; set; }
   public decimal Re_Order_Qty  { get; set; }
   public string Cursor_On { get; set; }
   public string Tax_Code { get; set; }
   public string Pur_Code { get; set; }
   public string Sale_Code { get; set; }
   public bool IsSaleable { get; set; }
   public decimal Delear_Margin { get; set; }
  public string Tax_AfterBefore { get; set; }
  public decimal Discount { get; set; }
 public decimal  Location { get; set; }
 public string Item_Type { get; set; }
 public string Department { get; set; }
 public decimal Sale_Rate2 { get; set; }
 public decimal Sale_Rate3 { get; set; }
 public decimal Sale_Rate4 { get; set; }
 public decimal Sale_Rate_Excl { get; set; }
 public decimal Whole_Sale_Rate { get; set; }
 public decimal Delivery_No { get; set; }
 public decimal SGroup_id { get; set; }
  public decimal COLOR_ID { get; set; }
 public bool TAGGED { get; set; }
 public decimal Franchise_SaleRate { get; set; }
 public bool Discontinued { get; set; }
 public string Transaction_Mode { get; set; }
 public bool InHouse_Packing { get; set; }
 public decimal InHouse_PackedQty { get; set; }
 public bool Dis1InRs { get; set; }
 public bool Dis2InRs { get; set; }
 public decimal Dis1Value  { get; set; }
  public decimal Dis2Value { get; set; }
  public bool IsCompBarCode { get; set; }
 public string Remarks  { get; set; }
  public bool AllowPoint { get; set; }
 public int Tax_ID  { get; set; }
public string CpPrinting  { get; set; }
public decimal Excise_ID  { get; set; }
public decimal Excise_Code  { get; set; }
public string ImageUrl  { get; set; }
   public int SubGroupId { get; set; }
   public int Likes { get; set; }
 public int UserId { get; set; }
   public int Day { get; set; }
   public bool BestExp { get; set; }
   public string VEG_NonVeg { get; set; }
   public string Ingredients { get; set; }
   public bool IsSubItem { get; set; }
   public string ParentItemCode { get; set; }
   public string MasterItemName { get; set; }
   public decimal DeliveryNoteRate { get; set; }
   public Int32 BranchId { get; set; }
   public DateTime ExpDate { get; set; }
   public string strExpDate { get { return ExpDate.ToString("d"); } }
   public bool Edit_SaleRate { get; set; }
   public string HSNCode { get; set; }

    public ItemMaster()

	{
        HSNCode = "";
        Sales_In_Unit2 = 0;
        MasterItemName = string.Empty;
        ParentItemCode = "";
        IsSubItem = false;
        Ingredients = string.Empty;
        ItemID = 0;
        COMPANY_ID = 0;
        GROUP_ID = 0;
        Item_Code = "";
        Master_Code = "";
        Item_Name = "";
        Packing = "";
        Bar_Code = "";
         Short_Name1 = "";
        Short_Name2 = "";
        Sales_In_Unit = "";
        Qty_To_Less = 0;
        Sale_Rate2 = 0;
        Sale_Rate3 = 0;
        Sale_Rate4 = 0;
        Purchase_Rate = 0;
        Max_Retail_Price = 0;
        Max_Level = 0;
        Qty_in_Case = 0;
        Min_Level = 0;
        Sale_Rate = 0;
        Sale_Rate_Excl = 0;
        Mark_Up = 0;
        Re_Order_Qty = 0;
        Cursor_On = "";
        Tax_Code = "";
        Pur_Code ="";
        Sale_Code="";
        IsSaleable = true;
        Delear_Margin = 0;
        Tax_AfterBefore ="";
        Discount = 0;
        Location = 0;
        Item_Type = "";
        Department = "";
        Whole_Sale_Rate = 0;
        Delivery_No = 0;
        SGroup_id =0;
        COLOR_ID =0;
        TAGGED = true;
        Franchise_SaleRate =0;
        Discontinued = false;
        Transaction_Mode = "";
        InHouse_Packing =false;
        InHouse_PackedQty =0;
        Dis1InRs =false;
        Dis2InRs = false;
        Dis1Value = 0;
        Dis2Value = 0;
        IsCompBarCode =false;
        Remarks = "";
        AllowPoint = false;
        Tax_ID = 0;
        CpPrinting ="";
        Excise_ID = 0;
        Excise_Code = 0;
        ImageUrl = "";
        SubGroupId = 0;
        Likes = 0;
        UserId = 0;
        Day = 0;
        BestExp = false;
        VEG_NonVeg = "";
        DeliveryNoteRate = 0;
        BranchId = 0;
        ExpDate = DateTime.Now;
        Edit_SaleRate = false;
         



	}
}