﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DiscountPeriod
/// </summary>
public class DiscountPeriod
{

    public int DiscountPeriod_ID { get; set; }
    public int Discount_ID { get; set; }
    public string Category { get; set; }
    public int Company_ID { get; set; }
    public int Group_ID { get; set; }
    public string Master_Code { get; set; }
    public string PName { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public decimal Discount_Per { get; set; }
    public decimal Discount_Amt { get; set; }
    public bool FULLDAY { get; set; }
    public DateTime START_TIME { get; set; }
    public DateTime END_TIME { get; set; }
    public int SGroup_ID { get; set; }
	
    
    public DiscountPeriod()
	{
        DiscountPeriod_ID = 0;
        Discount_ID = 0;
        Category = "";
        Company_ID = 0;
        Group_ID = 0;
        Master_Code = "";
        PName = "";
        Start_Date = DateTime.Now;
        End_Date = DateTime.Now;
        Discount_Per = 0;
        Discount_Amt = 0;
        FULLDAY = false;
        START_TIME = DateTime.Now;
        END_TIME = DateTime.Now;
        SGroup_ID = 0;
	}
}