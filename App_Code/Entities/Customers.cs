﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Customers
/// </summary>
public class Customers
{
    public int Customer_ID { get; set; }
    public string Prefix { get; set; }
    public string Customer_Name { get; set; }
    public string Address_1 { get; set; }
    public string Address_2 { get; set; }
    public int Area_ID { get; set; }
    public int City_ID { get; set; }
    public int State_ID { get; set; }
    public DateTime Date_Of_Birth { get; set; }
    public DateTime Date_Anniversary { get; set; }
    public decimal Discount { get; set; }
    public string Contact_No { get; set; }
    public string Tag { get; set; }
    public bool FocBill { get; set; }
    public int grpid { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public string strDOB { get { return Date_Of_Birth.ToString("d"); } }
    public string strDOA { get { return Date_Anniversary.ToString("d"); } }
    public int BranchId { get; set; }
    public string EmailId { get; set; }
    public string GSTNo { get; set; }
	public Customers()
	{
        GSTNo = "";
        BranchId = 0;
        Customer_ID = 0;
        Prefix = "";
        Customer_Name = "";
        Address_1 = "";
        Address_2 = "";
        Area_ID = 0;
        City_ID = 0;
        State_ID = 0;
        Date_Of_Birth = DateTime.Now;
        Date_Anniversary = DateTime.Now;
        Discount = 0;
        Contact_No = "";
        Tag = "";
        FocBill = false;
        grpid = 0;
        UserId = 0;
        IsActive = false;
        EmailId = "";
	}
}