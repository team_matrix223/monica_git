﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Settings
/// </summary>
public class Settings
{
    public string Apptype { get; set; }
    public decimal SerTax { get; set; }
    public int TakeAway { get; set; }
    public int TakeAwayDefault { get; set; }
    public string DiscountType { get; set; }

	public Settings()
	{
        Apptype = "";
        SerTax = 0;
        TakeAway = 0;
        TakeAwayDefault = 0;
        DiscountType = "";
	}
}