﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChallanDetail
/// </summary>
public class ChallanDetail
{
    public string Bill_Prefix { get; set; }
    public int Bill_No { get; set; }
    public string BillNoWPrefix { get; set; }
    public DateTime Bill_Date { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Qty { get; set; }
    public decimal Dis_Per { get; set; }
    public decimal Dis_Amt { get; set; }
    public decimal Amount { get; set; }
    public string BillMode { get; set; }
    public decimal Tax { get; set; }

    public decimal Tax_Amount { get; set; }
    public string Unit { get; set; }
    public decimal Org_SaleRate { get; set; }
    public string MASTER_CODE { get; set; }
    public decimal QTY_TO_LESS { get; set; }
    public int godown_id { get; set; }
    public decimal SurVal { get; set; }
    public decimal SurPer { get; set; }
    public decimal FreeQty { get; set; }
    public decimal RowNum { get; set; }
    public string Item_remarks { get; set; }
    public int ItemID { get; set; }

    public decimal TOtalBillValue { get; set; }
    public decimal TotalDiscount { get; set; }
    public decimal TotalTax { get; set; }
    public decimal TotalNetAmount { get; set; }
    public decimal TaxId { get; set; }
    public string BIllBasicType { get; set; }

	public ChallanDetail()
	{
        TOtalBillValue = 0;
        TotalDiscount = 0;
        TotalTax = 0;
        TotalNetAmount = 0;
        Bill_Prefix = "";
        Bill_No = 0; 
        BillNoWPrefix="";
        Bill_Date = DateTime.Now; 
        Item_Code = ""; 
        Item_Name = ""; 
        MRP = 0;
        Rate =0 ; 
        Qty =0; 
        Dis_Per = 0; 
        Dis_Amt = 0;
        Amount = 0; 
        BillMode = "";
        Tax = 0 ;
        Tax_Amount = 0 ;
        Unit = "";
        Org_SaleRate = 0; 
        MASTER_CODE = ""; 
        QTY_TO_LESS = 0; 
        godown_id = 0; 
        SurPer = 0; 
        SurVal = 0; 
        FreeQty = 0; 
        RowNum =0;
        Item_remarks = "";
        ItemID = 0;
        TaxId = 0;
        BIllBasicType = "";
	}
}