﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Orders
/// </summary>
public class Orders
{

  
    public int Order_No { get; set; }
    public DateTime Order_Date { get; set; }
    public string Customer_ID { get; set; }
    public string Customer_Name { get; set; }
    public decimal Order_Value { get; set; }
    public decimal Less_Dis_Amount { get; set; }
    public decimal Add_Tax_Amount { get; set; }
    public decimal Net_Amount { get; set; } 
    public string CreditBank { get; set; }
    public decimal Cash_Amount { get; set; }
    public decimal Credit_Amount { get; set; }
    public decimal CrCard_Amount { get; set; }
    public decimal Round_Amount { get; set; }
    public bool Passing { get; set; }
    public int CashCust_Code { get; set; }
    public string CashCust_Name { get; set; }
    public decimal Tax_Per { get; set; }
    public int Godown_ID { get; set; }
    public DateTime? ModifiedDate { get; set; }
    public decimal R_amount { get; set; }
    public int tokenno { get; set; }
    public int tableno { get; set; }
    public string remarks { get; set; }
    public decimal servalue { get; set; }
    public int ReceiviedGRNNo { get; set; }
    public int EmpCode { get; set; }
    public int BranchId { get; set; }
	public Orders()
	{
        Order_No = 0;
        Order_Date = DateTime.Now;
        Customer_ID = "";
        Customer_Name = "";
        Order_Value = 0;
        Less_Dis_Amount = 0;
        Add_Tax_Amount = 0;
        Net_Amount = 0;
        CreditBank = "";
        Cash_Amount = 0;
        Credit_Amount = 0;
        CrCard_Amount = 0;
        Round_Amount = 0;
        Passing = false;
        CashCust_Code=0;
        CashCust_Name = "";
        Tax_Per = 0;
        Godown_ID = 0;
        R_amount = 0;
        tokenno = 0;
        tableno = 0;
        remarks = "";
        servalue = 0;
        ReceiviedGRNNo = 0;
        EmpCode = 0;
        BranchId = 0;
            
	}
}