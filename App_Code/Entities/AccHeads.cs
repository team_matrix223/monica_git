﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AccHeads
/// </summary>
public class AccHeads
{
    public int H_Id { get; set; }
    public string H_CODE { get; set; }
    public string H_NAME { get; set; }
    public decimal AMOUNT { get; set; }
    public string DR_CR { get; set; }
    public string BAL_INC { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
	public AccHeads()
	{
        H_Id = 0;
        H_CODE =" ";
        H_NAME = "";
        AMOUNT = 0;
        DR_CR = "";
        BAL_INC = "";
        UserId = 0;
        IsActive = false;
	}
}