﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for expenses
/// </summary>
public class expenses
{
    public int Id { get; set; }
    public string RefDate { get; set; }
    public string Description { get; set; }
    public decimal Amount { get; set; }
    public int Userno { get; set; }
    public int BranchId { get; set; }

    
	public expenses()
	{
        Id = 0;
        RefDate = string.Empty;
        Description = "";
        Amount = 0;
        Userno = 0;
        BranchId = 0;
	}
}