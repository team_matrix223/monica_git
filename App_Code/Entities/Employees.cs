﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Employees
/// </summary>
public class Employees
{
    public int Code { get; set; }
    public string Name { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string ContactNo { get; set; }
    public string ContactPerson { get; set; }
    public string Remarks { get; set; }
    public bool Auth { get; set; }

	public Employees()
	{
        Code = 0;
        Name = "";
        Address1 = "";
        Address2 = "";
        Address3 = "";
        ContactNo = "";
        ContactPerson = "";
        Remarks = "";
        Auth = false;
	}
}