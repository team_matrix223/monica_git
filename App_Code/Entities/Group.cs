﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Banks
/// </summary>
public class Group
{

    public int GroupId { get; set; }
    public string Title { get; set; }
    public string ImageUrl { get; set; }
    public bool IsActive { get; set; }
    public decimal SerTax { get; set; }
    public int Group_Id { get; set; }
    public string Group_Name { get; set; }
    public Group()
	{
        SerTax = 0;
        GroupId = -1;
        Title = string.Empty;
        ImageUrl = string.Empty;
        IsActive = false;
        Group_Id = 0;
        Group_Name = string.Empty;
	}
}