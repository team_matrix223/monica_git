﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MembershipPages
/// </summary>
public class MembershipPages
{
    public int MembershipPageId { get; set; }
    public string PagePrefix { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public string Roles { get; set; }
  
    public MembershipPages()
    {
        MembershipPageId = 0;
        PagePrefix = "";
        Title = string.Empty;
        IsActive = true;
        Roles = string.Empty;
    }
}