﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InternalIssue
/// </summary>
public class InternalIssue
{
   
    public int ISSUENO { get; set; }
    public string ISSUEDATE { get; set; }
    public string strDate { get { return ISSUEDATE; } }
    //public DateTime ISSUEDATE { get; set; }
    //public string strDate { get { return ISSUEDATE.ToString("d"); } }

    public int DEPARTMENT { get; set; }
    public string REMARKS { get; set; }
    public string MASTER_CODE { get; set; }
    public string ITEM_NAME { get; set; }
    public decimal QTYISSUED { get; set; }
    public decimal RATE { get; set; }
    public decimal AMOUNT { get; set; }
    public string UNIT { get; set; }
    public bool PASSING { get; set; }
    public int RowID { get; set; }
    public decimal Tax { get; set; }
    public string ISSUEMODE { get; set; }
    public int Godown_ID { get; set; }
    public int BranchId { get; set; }
    public int UserNo { get; set; }
    public decimal Qty_To_Less { get; set; }
    public string Item_Code { get; set; }
    public string DepartmentName { get; set; }
    public string Godown { get; set; }
    public decimal STOCK { get; set; }

	public InternalIssue()
	{
        ISSUENO = 0;
        ISSUEDATE = string.Empty;
        DEPARTMENT = 0;
        REMARKS = string.Empty;
        MASTER_CODE = string.Empty;
        ITEM_NAME = string.Empty;
        QTYISSUED = 0;
        RATE = 0;
        AMOUNT = 0;
        UNIT = string.Empty;
        PASSING = false;
        RowID = 0;
        Tax = 0;
        ISSUEMODE = string.Empty;
        Godown_ID = 0;
        BranchId = 0;
        UserNo = 0;
        Qty_To_Less = 0;
        Item_Code = string.Empty;
        DepartmentName = string.Empty;
        Godown = string.Empty;
        STOCK = 0;
             
	}
}