﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Banks
/// </summary>
public class Variable
{

    public int Id { get; set; }
    public string Title{ get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }
	public Variable()
	{
        Id=0;
        Title=string.Empty;
        IsActive=false;
        UserId = 0;
	}
}