﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Acc_Ledger
/// </summary>
public class AccLedger
{

    public string CODE { get; set; }
    public string CCODE { get; set; }
    public string H_CODE { get; set; }
    public string S_CODE { get; set; }
    public string SS_CODE { get; set; }
    public string CNAME { get; set; }
    public string CADD1 { get; set; }
    public string CADD2 { get; set; }
    public int CITY_ID { get; set; }
    public int AREA_ID { get; set; }
    public int STATE_ID { get; set; }
    public string CST_NO { get; set; }
    public DateTime CST_DATE { get; set; }
    public string strCSTDate { get { return CST_DATE.ToString("d"); } }
    public string TINNO { get; set; }
    public string TOTNO { get; set; }
    public decimal CR_LIMIT { get; set; }
    public decimal CR_DAYS { get; set; }
    public string CONT_PER { get; set; }
    public string CONT_NO { get; set; }

    public decimal OP_BAL { get; set; }
    public string DR_CR { get; set; }
    public decimal DIS_PER { get; set; }
    public decimal OS_BAL { get; set; }
    public decimal PURSALE_ACC_PERCENT { get; set; }
    public string PURSALE_ACC_TYPE { get; set; }
    public string PREFIX { get; set; }
    public string ACC_ZONE { get; set; }
    public decimal SRNO { get; set; }
    public bool ShowInLedger { get; set; }

    public string Narr { get; set; }
    public string tAG { get; set; }
    public int AccountId { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }

	public AccLedger()
	{
        AccountId = 0;
        CODE = "";
        CCODE = "";
        S_CODE = "";
        SS_CODE = "";
        CNAME = "";
        CADD1 = "";
        CADD2 = "";
        CITY_ID = 0;
        STATE_ID = 0;
        AREA_ID = 0;
        CST_NO = "";
        CST_DATE = DateTime.Now;
        TINNO = "";
        TOTNO = "";
        CR_LIMIT = 0;
        CR_DAYS = 0;
        CONT_PER = "";
        CONT_NO = "";
        OP_BAL = 0;
        DR_CR = "";
        DIS_PER = 0;
        OS_BAL = 0;
        PURSALE_ACC_PERCENT = 0;
        PURSALE_ACC_TYPE = "";
        PREFIX = "";
        ACC_ZONE = "";
        SRNO = 0;
        ShowInLedger = false;
        Narr = "";
        tAG = "";
        UserId = 0;
        BranchId = 0;


	}
}