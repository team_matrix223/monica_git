﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ItemTypes
/// </summary>
public class ItemTypes
{

    public int ItemType_Id { get; set; }
    public string ItemType_Name { get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }
	public ItemTypes()
	{
        ItemType_Id = 0;
        ItemType_Name = "";
        IsActive = true;
        UserId = 0;
	}
}