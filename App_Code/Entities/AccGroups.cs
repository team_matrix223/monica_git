﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AccGroups
/// </summary>
public class AccGroups
{

    public int S_Id { get; set; }
    public string S_CODE { get; set; }
    public string S_NAME { get; set; }
    public string H_CODE { get; set; }
    public decimal AMOUNT { get; set; }  
    public string BAL_INC { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }


	public AccGroups()
	{
        S_Id = 0;
        S_CODE = "";
        S_NAME = "";
        AMOUNT = 0;
        H_CODE = "";
        BAL_INC = "";
        UserId = 0;
        IsActive = false;
	}
}