﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Headings
/// </summary>
public class AddOn 
{
    
    public int AddOnId{ get; set; }
    public string Title{ get; set; } 
     public bool IsActive{ get; set; }


    public AddOn ()
	{
        
        AddOnId = 0;
        Title = string.Empty;
        IsActive = true;
    }
}