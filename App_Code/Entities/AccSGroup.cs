﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AccSGroup
/// </summary>
public class AccSGroup
{

    public int SS_CODE { get; set; }
    public string SS_NAME { get; set; }
    public string S_CODE { get; set; }
    public decimal AMOUNT { get; set; }    
    public int UserId { get; set; }
    public bool IsActive { get; set; }    
	public AccSGroup()
	{
        SS_CODE = 0;
        SS_NAME = "";
        S_CODE = "";
        AMOUNT = 0;
        UserId = 0;
        IsActive = false;
	}
}