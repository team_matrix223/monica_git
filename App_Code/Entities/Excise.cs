﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Excise
/// </summary>
public class Excise
{
    public int Excise_ID { get; set; }
    public string Excise_Head { get; set; }
    public string Teriff_No { get; set; }
    public string ExciseDuty { get; set; }
    public string Cr_AccCode { get; set; }
    public string Dr_AccCode { get; set; }
    public string Cr_AccCName { get; set; }
    public string Dr_AccCName { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public decimal Abatement { get; set; }
	public Excise()
	{
        Excise_ID = -1;
        Excise_Head = "";
        Teriff_No = "";
        ExciseDuty = "";
        Cr_AccCode = "";
        Dr_AccCode = "";
        UserId = 0;
        IsActive = false;
        Abatement = 0;
	}
}