﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HSNCodeEntities
/// </summary>
public class HSNCodeEntities
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public HSNCodeEntities()
    {
        Id = 0;
        Title = "";
        UserId = 0;
        IsActive = false;

    }
}