﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Item
/// </summary>
public class Item
{
    public int ItemID { get; set; }
    public string Master_Code { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public string Bar_Code { get; set; }
    public string Sales_In_Unit { get; set; }
    public decimal Purchase_Rate { get; set; }
    public decimal Sale_Rate { get; set; }
    public decimal Max_Retail_Price { get; set; }

	public Item()
	{
        ItemID = 0;
        Master_Code = string.Empty;
        Item_Code = string.Empty;
        Item_Name = string.Empty;
        Bar_Code = string.Empty;
        Sales_In_Unit = string.Empty;
        Purchase_Rate = 0;
        Sale_Rate = 0;
        Max_Retail_Price = 0;
	}
}