﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PurchaseOrderDetail
/// </summary>
public class PurchaseOrderDetail
{

    public int OrderDetailId { get; set; }
    public int OrderNo { get; set; }
    public DateTime GrnDate { get; set; }
    public string ItemCode { get; set; }
    public string Unit { get; set; }
    public decimal CaseQty { get; set; }
    public decimal QtyInCase { get; set; }
    public decimal Qty { get; set; }
    public bool Scheme { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public decimal Dis1P { get; set; }
    public decimal Dis2P { get; set; }
    public decimal TaxP { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public decimal TaxAmt { get; set; }
    public int GodownId { get; set; }
    public int RowNum { get; set; }
    public int Free { get; set; }
    public decimal SaleRate { get; set; }
    public PurchaseOrderDetail()
    {
        OrderDetailId = 0;
        OrderNo = 0; GrnDate = DateTime.Now; ItemCode = string.Empty; Unit = string.Empty;
        CaseQty = 0; QtyInCase = 0; Qty = 0; Scheme = false; MRP = 0; Rate = 0; Amount = 0; Dis1P = 0; Dis2P = 0; TaxP = 0;
        Dis1Amt = 0; Dis2Amt = 0; TaxAmt = 0; GodownId = 0; RowNum = 0; Free = 0; SaleRate = 0;
    }
}