﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Roles
/// </summary>
public class Roles
{
    public int RoleId { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public int DesignationId { get; set; }
    public int PageId { get; set; }

	public Roles()
	{
        RoleId = 0;
        Title = "";
        IsActive = false;
        DesignationId = 0;
        PageId = 0;
	}
}