﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    public Int32 UserNo { get; set; }
    public string User_ID { get; set; }
    public string UserPWD { get; set; }
    public int Counter_NO { get; set; }
    public string SessionId { get; set; }
    public bool IsLogin { get; set; }
    public bool Discontinued { get; set; }
    public int BranchId { get; set; }
    public string BranchName { get; set; }
	public User()
	{
        BranchId = 0;
        Counter_NO = 0;
        UserNo = 0;
        User_ID = "";
        UserPWD = "";
        SessionId = "";
        IsLogin = false;
        Discontinued = false;
        BranchName = "";

	}
}