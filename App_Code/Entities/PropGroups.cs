﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropGroups
/// </summary>
public class PropGroups
{

    public int Group_ID { get; set; }
    public string Group_Name { get; set; }
    public string ImageUrl { get; set; }
    public bool ShowInMenu { get; set; }
    public bool IsActive { get; set; }
    public int Department_Id { get; set; }
    public int UserId { get; set; }


	public PropGroups()
	{
        Group_ID = -1;
        Group_Name = "";
        ImageUrl = "";
        ShowInMenu = true;
        IsActive = true;
        Department_Id =0 ;
        UserId = 0;
	}
}