﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cities
/// </summary>
public class Cities
{
    public int City_ID { get; set; }
    public string City_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
	public Cities()
	{
        City_ID = -1;
        City_Name = "";
        UserId = 0;
        IsActive = false;
	}
}