﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Company
/// </summary>
public class Company
{
    public int Company_ID { get; set; }
    public string Company_Name { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public string SupplierList { get; set; }
   
	public Company()
	{
        SupplierList = string.Empty;
        Company_ID = 0;
        Company_Name = "";
        UserId = 0;
        IsActive = false;
	}
}