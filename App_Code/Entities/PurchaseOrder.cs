﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Purchase
/// </summary>
public class PurchaseOrder
{
  
    public int OrderNo { get; set; }
    public DateTime OrderDate { get; set; }
    public string strorddate { get { return OrderDate.ToString("d"); } }
    public bool Dis1InRs { get; set; }
    public bool Dis2InRs { get; set; }
    public bool Dis2AftDedDis1 { get; set; }
    public string IsLocal { get; set; }
    public bool TaxAfterDis1 { get; set; }
    public bool TaxAfterDis2 { get; set; }
    public string Remarks { get; set; }
    public decimal BillValue { get; set; }
    public decimal Dis1Amt { get; set; }
    public decimal Dis2Amt { get; set; }
    public bool Dis3AftDis1PDis2 { get; set; }
    public decimal Dis3P { get; set; }
    public decimal Dis3Amt { get; set; }
    public decimal TaxP { get; set; }
    public decimal TaxAmt { get; set; }
    public decimal TotalAmount { get; set; }
    public decimal ODisP { get; set; }
    public decimal ODisAmt { get; set; }
    public decimal DisplayAmount { get; set; }
    public decimal Adjustment { get; set; }
    public decimal NetAmount { get; set; }
    public int SupplierId { get; set; }
    public int GodownId { get; set; }
    public int BranchId { get; set; }
    public int UserNo { get; set; }
    public string SupplierName { get; set; }
    public PurchaseOrder()
    {
        
        OrderNo = 0;
        OrderDate = DateTime.Now;
        Dis1InRs = false; Dis2InRs = false;
        Dis2AftDedDis1 = false; 
        IsLocal = string.Empty; 
        TaxAfterDis1 = false; 
        TaxAfterDis2 = false;
        Remarks = string.Empty;
        BillValue = 0;
        Dis1Amt = 0;
        Dis2Amt = 0;
        Dis3AftDis1PDis2 = false;
        Dis3P = 0;
        Dis3Amt = 0;
        TaxP = 0;
        TaxAmt = 0;
        TotalAmount = 0;
        ODisP = 0;
        ODisAmt = 0;
        DisplayAmount = 0; 
        Adjustment = 0;
        NetAmount = 0; 
        SupplierId = 0; 
        GodownId = 0;
        BranchId = 0;
        UserNo = 0;
        SupplierName = string.Empty;
    }
}