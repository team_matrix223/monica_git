﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
public class AddOnDAL : Connection
{
     public SqlDataReader GetAll()
    {
        List<AddOn> stateList = new List<AddOn>();
        SqlParameter[] objParam = new SqlParameter[0];
         SqlDataReader dr=null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "master_sp_AddOnGetAll", objParam);
             
         
         }

         finally
         {
             objParam = null;
         }
         return dr;

    }

     public Int16 InsertUpdate(AddOn objAddOn,int UserId)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@AddOnId", objAddOn.AddOnId);
        objParam[1] = new SqlParameter("@Title", objAddOn.Title);
        objParam[2] = new SqlParameter("@IsActive", objAddOn.IsActive);
        objParam[4] = new SqlParameter("@UserId", UserId);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AddOnInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[3].Value);
            objAddOn.AddOnId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

     
}