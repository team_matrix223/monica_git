﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PropGroupDAL
/// </summary>
public class PropGroupDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GroupsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(PropGroups objPropGroup)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@Group_ID", objPropGroup.Group_ID);
        objParam[1] = new SqlParameter("@Group_Name", objPropGroup.Group_Name);
        objParam[2] = new SqlParameter("@IsActive", objPropGroup.IsActive);
        objParam[3] = new SqlParameter("@ImageUrl", objPropGroup.ImageUrl);
        objParam[4] = new SqlParameter("@ShowInMenu", objPropGroup.ShowInMenu);
        objParam[5] = new SqlParameter("@Department_Id", objPropGroup.Department_Id);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@UserId", objPropGroup.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_GroupsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objPropGroup.Group_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetByDepartmentId(int DepartmentId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Department_Id", DepartmentId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PropGroupGetByDepartmentId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public int Delete(PropGroups objPrpGroup)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Group_ID", objPrpGroup.Group_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PropGroupDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objPrpGroup.Group_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}