﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for TaxStructureDAL
/// </summary>
public class TaxStructureDAL :Connection
{
    public SqlDataReader GetAll(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "salon_sp_GetAllTax", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public void GetByTaxStructure(TaxStructure objTaxStructure,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TaxRate", objTaxStructure.Tax_Rate);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetSurchargeForTaxStructure", objParam);
            if (dr.HasRows)
            {
                dr.Read();
                objTaxStructure.Tax_ID = Convert.ToInt32(dr["Tax_ID"]);
                objTaxStructure.Tax_Rate = Convert.ToDecimal(dr["Tax_Rate"]);
                objTaxStructure.Description = Convert.ToString(dr["Description"]);
                objTaxStructure.Dr_AccCode = Convert.ToString(dr["Dr_AccCode"]);
                objTaxStructure.VatCode_Dr = Convert.ToBoolean(dr["VatCode_Dr"]);
                objTaxStructure.Dr_VatCode = Convert.ToString(dr["Dr_VatCode"]);
                objTaxStructure.VatCode_Cr = Convert.ToBoolean(dr["VatCode_Cr"]);
                objTaxStructure.Cr_AccCode = Convert.ToString(dr["Cr_AccCode"]);
                objTaxStructure.Cr_VatCode = Convert.ToString(dr["Cr_VatCode"]);
                objTaxStructure.ChkSur = Convert.ToBoolean(dr["ChkSur"]);
                objTaxStructure.SurValue = Convert.ToDecimal(dr["SurValue"]);
                objTaxStructure.Dr_Sur = Convert.ToString(dr["Dr_Sur"]);
                objTaxStructure.Cr_Sur = Convert.ToString(dr["Cr_Sur"]);




            }


        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }


    }


    public Int16 InsertUpdate(TaxStructure objTaxStructure)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[17];

        objParam[0] = new SqlParameter("@Tax_Rate", objTaxStructure.Tax_Rate);
        objParam[1] = new SqlParameter("@Description", objTaxStructure.Description);
        objParam[2] = new SqlParameter("@Dr_AccCode", objTaxStructure.Dr_AccCode);
        objParam[3] = new SqlParameter("@VatCode_Dr", objTaxStructure.VatCode_Dr);
        objParam[4] = new SqlParameter("@Dr_VatCode", objTaxStructure.Dr_VatCode);
        objParam[5] = new SqlParameter("@VatCode_Cr", objTaxStructure.VatCode_Cr);
        objParam[6] = new SqlParameter("@Cr_AccCode", objTaxStructure.Cr_AccCode);
        objParam[7] = new SqlParameter("@Cr_VatCode", objTaxStructure.Cr_VatCode);
        objParam[8] = new SqlParameter("@Tax_ID", objTaxStructure.Tax_ID);
        objParam[9] = new SqlParameter("@ChkSur", objTaxStructure.ChkSur);
        objParam[10] = new SqlParameter("@SurValue", objTaxStructure.SurValue);
        objParam[11] = new SqlParameter("@Dr_Sur", objTaxStructure.Dr_Sur);
        objParam[12] = new SqlParameter("@Cr_Sur", objTaxStructure.Cr_Sur);
       

        objParam[13] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[13].Direction = ParameterDirection.ReturnValue;
        objParam[14] = new SqlParameter("@UserId", objTaxStructure.UserId);
        objParam[15] = new SqlParameter("@IsActive", objTaxStructure.IsActive);
        objParam[16] = new SqlParameter("@BranchId", objTaxStructure.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_TaxInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[13].Value);
            objTaxStructure.Tax_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(TaxStructure objTaxStructure)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Tax_ID", objTaxStructure.Tax_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_TaxDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objTaxStructure.Tax_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}