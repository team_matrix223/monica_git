﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccSGroupDAL
/// </summary>
public class AccSGroupDAL:Connection
{
    public SqlDataReader GetAll()
    {
        List<AccSGroup> AccSGroupsList = new List<AccSGroup>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccSGroupsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(AccSGroup objAccSGroup)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@SS_CODE", objAccSGroup.SS_CODE);
        objParam[1] = new SqlParameter("@SS_NAME", objAccSGroup.SS_NAME);
        objParam[2] = new SqlParameter("@S_CODE", objAccSGroup.S_CODE);
        objParam[3] = new SqlParameter("@AMOUNT", objAccSGroup.AMOUNT);
        


        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@UserId", objAccSGroup.UserId);
        objParam[6] = new SqlParameter("@IsActive", objAccSGroup.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccSGroupInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[4].Value);
            objAccSGroup.SS_CODE = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public SqlDataReader GetByGroup(string SCODE)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@S_CODE", SCODE);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccSGroupsGetByGroup", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public int Delete(AccSGroup objAccSGroup)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@S_CODE", objAccSGroup.S_CODE);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccSGroupDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objAccSGroup.SS_CODE = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}