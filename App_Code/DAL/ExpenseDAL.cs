﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExpenseDAL
/// </summary>
public class ExpenseDAL:Connection
{
    public Int16 InsertUpdate(expenses objExpense, DataTable dt, DateTime date)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@Userno", objExpense.Userno);
        objParam[1] = new SqlParameter("@BranchId", objExpense.BranchId);
        objParam[4] = new SqlParameter("@Date", date);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;


        objParam[3] = new SqlParameter("@dt", dt);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_ExpenseInsert", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetByID(int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BranchId", BranchId);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_GetExpense", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public SqlDataReader GetByDate(int BranchId, DateTime Date)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@BranchId", BranchId);
        ObjParam[1] = new SqlParameter("@Date", Date);

        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_GetExpenseByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


}