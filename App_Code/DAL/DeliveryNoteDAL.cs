﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for DeliveryNoteDAL
/// </summary>
public class DeliveryNoteDAL:Connection

{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllDealer", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public int Insert(DeliveryMaster objDelivery, DataTable dt,bool Excise)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[47];



        objParam[0] = new SqlParameter("@Bill_No", objDelivery.Bill_No);
        objParam[1] = new SqlParameter("@Bill_Date", objDelivery.Bill_Date);
        objParam[2] = new SqlParameter("@GR_No", objDelivery.GR_No);
        objParam[3] = new SqlParameter("@GR_Date", objDelivery.GR_Date);
        objParam[4] = new SqlParameter("@Veh_No", objDelivery.Veh_No);
        objParam[5] = new SqlParameter("@Dis1InRs", objDelivery.Dis1InRs);
        objParam[6] = new SqlParameter("@Dis2InRs", objDelivery.Dis2InRs);
        objParam[7] = new SqlParameter("@Dis2AftDedDis1", objDelivery.Dis2AftDedDis1);
        objParam[8] = new SqlParameter("@LocalOut", objDelivery.LocalOut);
        objParam[9] = new SqlParameter("@TaxAfterDis1", objDelivery.TaxAfterDis1);
        objParam[10] = new SqlParameter("@TaxAfterDis2", objDelivery.TaxAfterDis2);
        objParam[11] = new SqlParameter("@Remarks", objDelivery.Remarks);
        objParam[12] = new SqlParameter("@Bill_Value", objDelivery.Bill_Value);
        objParam[13] = new SqlParameter("@Dis1Amt", objDelivery.Dis1Amt);
        objParam[14] = new SqlParameter("@Dis2Amt", objDelivery.Dis2Amt);
        objParam[15] = new SqlParameter("@Dis3AftDis1PDis2", objDelivery.Dis3AftDis1PDis2);
        objParam[16] = new SqlParameter("@Dis3P", objDelivery.Dis3P);
        objParam[17] = new SqlParameter("@Dis3Amt", objDelivery.Dis3Amt);
        objParam[18] = new SqlParameter("@TaxP", objDelivery.TaxP);
        objParam[19] = new SqlParameter("@TaxAmt", objDelivery.TaxAmt);
        objParam[20] = new SqlParameter("@Total_Amount", objDelivery.Total_Amount);
        objParam[21] = new SqlParameter("@ODisP", objDelivery.ODisP);
        objParam[22] = new SqlParameter("@ODisAmt", objDelivery.ODisAmt);
        objParam[23] = new SqlParameter("@Display_Amount", objDelivery.Display_Amount);
        objParam[24] = new SqlParameter("@Adjustment", objDelivery.Adjustment);
        objParam[25] = new SqlParameter("@Net_Amount", objDelivery.Net_Amount);
        objParam[26] = new SqlParameter("@CCODE", objDelivery.CCODE);
        objParam[27] = new SqlParameter("@Godown_ID", objDelivery.Godown_ID);
        objParam[28] = new SqlParameter("@Form_Name", objDelivery.Form_Name);
        objParam[29] = new SqlParameter("@Desp_Date", objDelivery.Desp_Date);
        objParam[30] = new SqlParameter("@Ref_No", objDelivery.Ref_No);
        objParam[31] = new SqlParameter("@UpdateMaster", objDelivery.UpdateMaster);
        objParam[32] = new SqlParameter("@PASSING", objDelivery.PASSING);
        objParam[33] = new SqlParameter("@ChkRaw", objDelivery.ChkRaw);
        objParam[34] = new SqlParameter("@dr_grno", objDelivery.dr_grno);
        objParam[35] = new SqlParameter("@delchk", objDelivery.delchk);
        objParam[36] = new SqlParameter("@Excise_Amt", objDelivery.Excise_Amt);
        objParam[37] = new SqlParameter("@EduCess", objDelivery.EduCess);
        objParam[38] = new SqlParameter("@HSCess", objDelivery.HSCess);
        objParam[39] = new SqlParameter("@BillMode", objDelivery.BillMode);
        objParam[43] = new SqlParameter("@UserNo", objDelivery.UserNo);
        objParam[42] = new SqlParameter("@BranchId",objDelivery.BranchId);
        objParam[44] = new SqlParameter("@ForBranch", objDelivery.ForBranch);
        objParam[45] = new SqlParameter("@Excise", Excise);
        objParam[46] = new SqlParameter("@Type", objDelivery.Type);
        objParam[40] = new SqlParameter("@WSRDetail", dt);
        objParam[41] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[41].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_WSRInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[41].Value);
            objDelivery.Bill_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }



    public int ReceiveDelivery(string BillNo,int BranchId,int GodownId)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[43];



        objParam[0] = new SqlParameter("@Bill_No", BillNo);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@GodownId", GodownId);
        objParam[41] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[41].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DeliveryReceive", objParam);
            retValue = Convert.ToInt32(objParam[41].Value);
           
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }



    public int Delete(DeliveryMaster objDelivery)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@Bill_No", objDelivery.Bill_No);
        objParam[1] = new SqlParameter("@Prefix", objDelivery.Prefix);
        objParam[2] = new SqlParameter("@BranchId", objDelivery.BranchId);

        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DeleteDeliveryNote", objParam);
            retValue = Convert.ToInt32(objParam[0].Value);
            objDelivery.Bill_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public SqlDataReader GetAllNote(DateTime DateFrom , DateTime DateTo,Int32 BranchId)
    {
        List<Kit> kitList = new List<Kit>();
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DeliveryGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllReceivedNote(DateTime DateFrom, DateTime DateTo, Int32 BranchId)
    {
        List<Kit> kitList = new List<Kit>();
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DeliveryReceiveGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByID(Int32 BillNo,string Prefix)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@BillNo", BillNo);
        ObjParam[1] = new SqlParameter("@Prefix", Prefix);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_DeliveryNoteGetById", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetByIDByBranch(Int32 BillNo, string Prefix,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@BillNo", BillNo);
        ObjParam[1] = new SqlParameter("@Prefix", Prefix);
        ObjParam[2] = new SqlParameter("@BranchFor", BranchId);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_DeliveryNoteGetByIdBybranch", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetNonExciseByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@DateFrom", DateFrom);

        ObjParam[1] = new SqlParameter("@DateTo", DateTo);

        ObjParam[3] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_GetDeliveryNoteWithReceive", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

}