﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for KitDAL
/// </summary>
public class KitDAL:Connection
{
    public Int32 InsertUpdate(Kit objKit, DataTable dt)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[9];
        ObjParam[0] = new SqlParameter("@Kit_ID", objKit.Kit_ID);

        ObjParam[1] = new SqlParameter("@Master_Code", objKit.Master_Code);
        ObjParam[2] = new SqlParameter("@Item_Name", objKit.Item_Name);
        ObjParam[3] = new SqlParameter("@Amount", objKit.Amount);
        ObjParam[4] = new SqlParameter("@MRP", objKit.MRP);
        ObjParam[5] = new SqlParameter("@SALE_RATE", objKit.SALE_RATE);
        ObjParam[6] = new SqlParameter("@UserId", objKit.UserId);


        ObjParam[7] = new SqlParameter("@KitDetail", dt);

        ObjParam[8] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[8].Direction = ParameterDirection.ReturnValue;

        try
        {

            objKit.Kit_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "master_sp_KitInsertUpdate", ObjParam));

            retval = Convert.ToInt32(ObjParam[8].Value);
            objKit.Kit_ID = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

  

    public SqlDataReader GetByKitID(Int32 KitID)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@Kit_ID", KitID);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_KitGetById", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


   


    public SqlDataReader GetAll()
    {
        List<Kit> kitList = new List<Kit>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_KitsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public int Delete(Kit objKit)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Kit_ID", objKit.Kit_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_KitDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objKit.Kit_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
	
}