﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for UserDAL
/// </summary>
public class UserDAL : Connection
{
    public Int32 userLoginCheck(User objUser)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@UserId", objUser.UserNo);

        objParam[1] = new SqlParameter("@SessionId", objUser.SessionId);
        objParam[2] = new SqlParameter("@retval", SqlDbType.Int, 4);

        objParam[2].Direction = ParameterDirection.ReturnValue;
       
        try
        {
            SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_loginchecktest", objParam);
            retVal = Convert.ToInt32(objParam[2].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }


    public SqlDataReader GetAll(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);

        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_UserInfoGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public Int32 InsertUpdate(User objUser)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@UserNo", objUser.UserNo);
        objParam[1] = new SqlParameter("@User_ID", objUser.User_ID);
        objParam[2] = new SqlParameter("@UserPWD", objUser.UserPWD);
        objParam[3] = new SqlParameter("@Counter_NO", objUser.Counter_NO);
        objParam[4] = new SqlParameter("@Discontinued", objUser.Discontinued);
        objParam[6] = new SqlParameter("@BranchId", objUser.BranchId);


        objParam[5] = new SqlParameter("@retval", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
           "master_sp_UserInfoInsertUpdate", objParam);
            retVal = Convert.ToInt32(objParam[5].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }


    public Int32 chkIsLogin(User objUser)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@UserNo", objUser.UserNo);

  
        objParam[1] = new SqlParameter("@retval", SqlDbType.Int, 4);

        objParam[1].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_chkIsLogin", objParam);
            retVal = Convert.ToInt32(objParam[1].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }

    public SqlDataReader GetByUserId(User objUser)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@UserNo", objUser.UserNo);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_UserGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int32 SuperuserLoginCheck(User objUser,int BranchId)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@User_ID", objUser.User_ID);
        //objParam[1] = new SqlParameter("@Password", HashSHA1(objReg.Password));
        objParam[1] = new SqlParameter("@UserPWD", objUser.UserPWD);
        objParam[2] = new SqlParameter("@retval", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@BranchId", BranchId);


        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SuperUserLoginCheck", objParam);
            retVal = Convert.ToInt32(objParam[2].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }
    public string ChangePassword(User objUser)
    {
        string retVal = "0";
        SqlParameter[] objParam = new SqlParameter[2];

        //objParam[0] = new SqlParameter("@Password", HashSHA1(objReg.Password));
        objParam[0] = new SqlParameter("@UserPWD", objUser.UserPWD);
        objParam[1] = new SqlParameter("@UserNo", objUser.UserNo);

        try
        {
            retVal = SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
                 "master_sp_UserChangePassword", objParam).ToString();


        }
        finally
        {
            objParam = null;
        }

        return retVal;

    }
}