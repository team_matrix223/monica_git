﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
public class CategoriesDAL : Connection
{
     public SqlDataReader GetAll()
    {
        
        SqlParameter[] objParam = new SqlParameter[0];
         SqlDataReader dr=null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "pos_sp_Prop_GroupsGetActive", objParam);
              
         
         }

         finally
         {
             objParam = null;
         }
         return dr;

    }

    
}