﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for DayOpenCloseDAL
/// </summary>
/// 

public class DayOpenCloseDAL : Connection
{
    public string GetCurrentStatus(int BranchId, out string Status)
    {
        string date = "";
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        Status = "";
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetCurrentStatus", objParam);
            if (dr.HasRows)
            {
                dr.Read();
                date = Convert.ToDateTime(dr["Date"]).ToString("D");
                Status = Convert.ToString(dr["OpenClose"]);
            }

        }

        finally
        {
            objParam = null;
        }

        return date;

    }



    public string IsDayOpen(int BranchId)
    {

        string strDate = "";
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);


        try
        {
            strDate = SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
               "master_sp_DayOpenCloseWatcher", objParam).ToString();


        }
        finally
        {
            objParam = null;
        }

        return strDate;



    }



    public int DayOpenCloseForceClose(Int32 BranchId)
    {
        int retVal = 0;
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure, "master_sp_DayOpenForceClose", objParam);
            retVal = Convert.ToInt16(objParam[1].Value);
        }
        finally
        {
            objParam = null;
        }
        return retVal;

    }


    public int DayOpenCloseCheckDelivery(string Status, DateTime Date, Int32 BranchId)
    {

        int retVal = 0;
        SqlParameter[] objParam = new SqlParameter[4];

        objParam[0] = new SqlParameter("@Status", Status);
        objParam[1] = new SqlParameter("@Date", Date);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure, "master_sp_DayOpenCloseCheckDelivery", objParam);

            retVal = Convert.ToInt16(objParam[3].Value);


        }
        finally
        {
            objParam = null;
        }
        return retVal;
    }








}