﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BankDAL
/// </summary>
public class CommonMasterDAL:Connection
{
    public DataSet GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.Text,
            "master_sp_CommonMasterGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public Int32 GetDefaultGodown(int BranchId)
    {

        Int32 GodownId = 0;
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
         
        try
        {
          GodownId=Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_GetDefaultGodownByBranch", objParam));

        }
        finally
        {
            objParam = null;
        }
        return GodownId;


    }

    public void Reprint(int UserNo,string PrintType,int BranchId,string BillNowPrefix)
    {
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];

        objParam[0] = new SqlParameter("@UserNo",UserNo);
        objParam[1] = new SqlParameter("@PrintType", PrintType);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        objParam[3] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_Reprint", objParam);
             
        }
        finally
        {
            objParam = null;
        }
    


    }

}