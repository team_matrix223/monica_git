﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PhysicalStockDAL
/// </summary>
public class ItemIssueDAL : Connection
{
    public int Delete(int RefNo, int BranchId)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@RefNo", RefNo);

        objParam[1] = new SqlParameter("@BranchId", BranchId);


 
        try
        {
          retValue=  SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_ItemIssueDelete", objParam);

        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }

    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int RefNo, DataTable dt, int UserNo,int BranchTo)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@Date", Date);
        objParam[1] = new SqlParameter("@GodownId", GodownId);
        objParam[2] = new SqlParameter("@tempItems", dt);
        objParam[3] = new SqlParameter("@RefNo", RefNo);
        objParam[4] = new SqlParameter("@BranchId", BranchId);

        objParam[5] = new SqlParameter("@UserNo", UserNo);
        objParam[6] = new SqlParameter("@GodownTo", BranchTo);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_Raw_ItemIssueInsertUpdate", objParam);

        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }


    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ItemIssueGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



    public SqlDataReader GetByRefNo(int RefNo,int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@RefNo", RefNo);
        objParam[1] = new SqlParameter("@BranchId", BranchId);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ItemIssueGetByRefNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }





}