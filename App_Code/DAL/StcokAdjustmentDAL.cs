﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for StcokAdjustmentDAL
/// </summary>
public class StcokAdjustmentDAL : Connection
{

    public int InsertUpdate(StockAdj objStock,DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@Ref_Date", objStock.Ref_Date);
        objParam[1] = new SqlParameter("@UserNo", objStock.UserNo);
        objParam[2] = new SqlParameter("@Godown_ID", objStock.Godown_ID);
        objParam[3] = new SqlParameter("@BranchId", objStock.BranchId);
        objParam[4] = new SqlParameter("@StockAdjustment", dt);
       
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_InsertStockAdjustment", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objStock.Ref_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "Pos_sp_GetAllStockAdjustment", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
}