﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for ExciseDAL
/// </summary>
public class ExciseDAL:Connection
{
    public SqlDataReader GetAll()
    {
        List<Excise> ExciseList = new List<Excise>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ExciseGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Excise objExcise)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[10];

        objParam[0] = new SqlParameter("@Excise_ID", objExcise.Excise_ID);
        objParam[1] = new SqlParameter("@Excise_Head", objExcise.Excise_Head);
        objParam[2] = new SqlParameter("@Teriff_No", objExcise.Teriff_No);
        objParam[3] = new SqlParameter("@ExciseDuty", objExcise.ExciseDuty);
        objParam[4] = new SqlParameter("@Cr_AccCode", objExcise.Cr_AccCode);
        objParam[5] = new SqlParameter("@Dr_AccCode", objExcise.Dr_AccCode);


        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@UserId", objExcise.UserId);
        objParam[8] = new SqlParameter("@IsActive", objExcise.IsActive);
        objParam[9] = new SqlParameter("@Abatement", objExcise.Abatement);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ExciseInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objExcise.Excise_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Excise objExcise)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Excise_ID", objExcise.Excise_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ExciseDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objExcise.Excise_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}