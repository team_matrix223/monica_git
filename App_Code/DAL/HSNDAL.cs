﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for HSNDAL
/// </summary>
public class HSNDAL:Connection
{
    public SqlDataReader GetById(HSNCodeEntities objHSN)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ID", objHSN.Id);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_HSNGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllHSNCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(HSNCodeEntities objHSN)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@ID", objHSN.Id);
        objParam[1] = new SqlParameter("@Title", objHSN.Title);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objHSN.UserId);
        objParam[4] = new SqlParameter("@IsActive", objHSN.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_HSNCodeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objHSN.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(HSNCodeEntities objHSN)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ID", objHSN.Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_HSNDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objHSN.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}