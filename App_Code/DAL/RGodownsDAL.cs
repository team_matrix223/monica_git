﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for RRGodownsDAL
/// </summary>
public class RGodownsDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "Select * from dbo.Godown_Master where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllItemType()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_ItemType where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllDepartments()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Departments where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllGroup()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.prop_groups where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllBranch()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Branch_Master", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllSubGroup()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.prop_Sgroup where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllItems()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Packing_Belongs  order by Item_Name", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAllProducts(string ItemType)
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "Select * from dbo.Packing_Belongs where Discontinued = 0 and Item_Type = '" + ItemType + "' order by Item_Name", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllCompany()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.prop_company where IsActive = 1", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetGodownByStock()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_RGodownsGetByStock", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    // public Int16 InsertUpdate(RGodowns objRGodowns)
    //{

    //    Int16 retValue = 0;
    //    SqlParameter[] objParam = new SqlParameter[4];

    //    objParam[0] = new SqlParameter("@GodownId", objRGodowns.Godown_Id);
    //    objParam[1] = new SqlParameter("@Title", objRGodowns.Title);
    //    objParam[2] = new SqlParameter("@IsActive", objRGodowns.IsActive);

    //    objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
    //    objParam[3].Direction = ParameterDirection.ReturnValue;
    //    try
    //    {
    //        SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
    //       "Salon_sp_RGodownsInsertUpdate", objParam);
    //        retValue = Convert.ToInt16(objParam[3].Value);
    //        objRGodowns.GodownId = retValue;
    //    }
    //    finally
    //    {
    //        objParam = null;
    //    }
    //    return retValue;
    //}
}