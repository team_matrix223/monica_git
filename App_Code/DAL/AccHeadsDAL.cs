﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccHeadsDAL
/// </summary>
public class AccHeadsDAL:Connection
{
    public SqlDataReader GetAll()
    {
        List<AccHeads> AccHeadsList = new List<AccHeads>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccHeadsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(AccHeads objAccHeads)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];

        objParam[0] = new SqlParameter("@H_Id", objAccHeads.H_Id);
        objParam[1] = new SqlParameter("@H_CODE", objAccHeads.H_CODE);
        objParam[2] = new SqlParameter("@H_NAME", objAccHeads.H_NAME);
        objParam[3] = new SqlParameter("@Amount", objAccHeads.AMOUNT);
        objParam[4] = new SqlParameter("@DR_CR", objAccHeads.DR_CR);
        objParam[5] = new SqlParameter("@BAL_INC", objAccHeads.BAL_INC);


        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@UserId", objAccHeads.UserId);
        objParam[8] = new SqlParameter("@IsActive", objAccHeads.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccHeadsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objAccHeads.H_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(AccHeads objAccHead)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@H_Id", objAccHead.H_Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccHeadDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objAccHead.H_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}