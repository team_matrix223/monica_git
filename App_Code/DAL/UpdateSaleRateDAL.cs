﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;


/// <summary>
/// Summary description for UpdateSaleRateDAL
/// </summary>
public class UpdateSaleRateDAL : Connection
{

    public SqlDataReader GetProductByDept(int Dept)
    {

        SqlParameter[] objparam = new SqlParameter[1];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@Dept", Dept);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProductByDeptsale", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }
    public SqlDataReader UpdateItemToPackingBelongs()
    {

        SqlParameter[] objparam = new SqlParameter[0];
        SqlDataReader dr = null;

        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "UpdateItemToPackingBelongs", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }


    public SqlDataReader GetProductByCodeorName(String Type, string Code, string Name)
    {

        SqlParameter[] objparam = new SqlParameter[3];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@Type", Type);
        objparam[1] = new SqlParameter("@ItemCode", Code);
        objparam[2] = new SqlParameter("@ItemName", Name);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProductByCodeOrName", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }


    public int UpdatePackingbelongsDetailsale(Int64 ItemID, decimal Sale_Rate, decimal Mrp, decimal Sale_Rate2, decimal tax, decimal Sale_Rate3, decimal Sale_Rate4,Int32 BranchId)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[9];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@ItemID", ItemID);
        objparam[1] = new SqlParameter("@Sale_Rate", Sale_Rate);
        objparam[2] = new SqlParameter("@Mrp", Mrp);
        objparam[4] = new SqlParameter("@Sale_Rate2", Sale_Rate2);
        objparam[5] = new SqlParameter("@tax", tax);
        objparam[6] = new SqlParameter("@Sale_Rate3", Sale_Rate3);
        objparam[7] = new SqlParameter("@Sale_Rate4", Sale_Rate4);
        objparam[8] = new SqlParameter("@Branch", BranchId);
        objparam[3] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[3].Direction = ParameterDirection.ReturnValue;

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "UpdatePackingbelongsDetailsale", objparam);
            retval = Convert.ToInt32(objparam[3].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }


    public SqlDataReader GetById(Departments objDepartment)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Prop_ID", objDepartment.Prop_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DepartmentGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<Departments> DepartmentList = new List<Departments>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DepartmentsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Departments objDepartment)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@PROP_ID ", objDepartment.Prop_ID);
        objParam[1] = new SqlParameter("@PROP_NAME", objDepartment.Prop_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objDepartment.IsActive);
        objParam[4] = new SqlParameter("@UserId", objDepartment.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DepartmentInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objDepartment.Prop_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(Departments objDepartment)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Prop_ID", objDepartment.Prop_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DepartmentDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objDepartment.Prop_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}