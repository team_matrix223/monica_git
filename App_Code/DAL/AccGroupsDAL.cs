﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccGroupsDAL
/// </summary>
public class AccGroupsDAL:Connection
{
    public SqlDataReader GetAll()
    {
        List<AccGroups> AccGroupsList = new List<AccGroups>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccGroupsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(AccGroups objAccGroups)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@S_Id", objAccGroups.S_Id);
        objParam[1] = new SqlParameter("@S_CODE", objAccGroups.S_CODE);
        objParam[2] = new SqlParameter("@S_NAME", objAccGroups.S_NAME);
        objParam[3] = new SqlParameter("@H_CODE", objAccGroups.H_CODE);
        objParam[4] = new SqlParameter("@AMOUNT", objAccGroups.AMOUNT);
      

        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;
        objParam[6] = new SqlParameter("@UserId", objAccGroups.UserId);
        objParam[7] = new SqlParameter("@IsActive", objAccGroups.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccGroupsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[5].Value);
            objAccGroups.S_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public SqlDataReader GetByBALINC(string BALINC)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BALINC", BALINC);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccGroupsGetByBALINC", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public int Delete(AccGroups objAccGroup)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@S_Id", objAccGroup.S_Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AccGroupDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objAccGroup.S_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}