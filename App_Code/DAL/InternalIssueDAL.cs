﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InternalIssueDAL
/// </summary>
public class InternalIssueDAL:Connection
{

    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int Issue, DataTable dt, int UserNo,int Department,String Remarks)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];
        objParam[0] = new SqlParameter("@RefNo", Issue);
        objParam[1] = new SqlParameter("@Date", Date);
        objParam[2] = new SqlParameter("@Department", Department);
        objParam[3] = new SqlParameter("@Remark", Remarks);
        objParam[4] = new SqlParameter("@GodownId", GodownId);
        objParam[5] = new SqlParameter("@BranchId", BranchId);
        objParam[6] = new SqlParameter("@InternalIssue", dt);
        objParam[7] = new SqlParameter("@UserNo", UserNo);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_InternalIssueInsertUpdate", objParam);

        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }


    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_Raw_InternalIssueGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int Delete(int RefNo, int BranchId)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@RefNo", RefNo);

        objParam[1] = new SqlParameter("@BranchId", BranchId);



        try
        {
            retValue = SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
             "pos_sp_Raw_InternalIssueDelete", objParam);

        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }

    public SqlDataReader GetByRefNo(int RefNo, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@RefNo", RefNo);
        objParam[1] = new SqlParameter("@BranchId", BranchId);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_InternalIssueGetByIssueNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

}