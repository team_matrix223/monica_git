﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillDAL
/// </summary>
public class BillDAL:Connection
{
    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetAllBills", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

	
	 public Int32 UpdateGSTBill(string LocalOut, string BNF, bool BillType, string CustomerId, string CustomerName, int UserNo)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[6];
        ObjParam[0] = new SqlParameter("@BNF", BNF);

        ObjParam[1] = new SqlParameter("@LocalOut", LocalOut);
        ObjParam[2] = new SqlParameter("@BillType", BillType);
        ObjParam[3] = new SqlParameter("@CustomerId", CustomerId);
        ObjParam[4] = new SqlParameter("@CustomerName", CustomerName);
        ObjParam[5] = new SqlParameter("@UserNo", UserNo);
        try
        {

            Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "UpdateGSTBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);

        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }
	

	
    public SqlDataReader GetByDateVatBills(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetAllVatBills", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public SqlDataReader GetOrdersBillItemByDate(DateTime DateFrom, DateTime DateTo, string ItemType, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@ItemType", ItemType);
        ObjParam[3] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Master_Sp_GetOrderOrBillsItemByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[33];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BranchId", objBill.BranchId);

        ObjParam[31] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        ObjParam[32] = new SqlParameter("@DeliveryCharges", objBill.DeliveryCharges);
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public Int32 InsertVatBill(Bill objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[36];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BranchId", objBill.BranchId);

        ObjParam[31] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        ObjParam[32] = new SqlParameter("@DeliveryCharges", objBill.DeliveryCharges);
        ObjParam[33] = new SqlParameter("@LocalOut", objBill.LocalOut);
        ObjParam[34] = new SqlParameter("@TransportMode", objBill.TransportMode);
        ObjParam[35] = new SqlParameter("@VehicleNo", objBill.VehicleNo);
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertVatBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public Int32 Update(Bill objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[34];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BillNowPrefix", objBill.BillNowPrefix);
        ObjParam[31] = new SqlParameter("@BranchId", objBill.BranchId);
        ObjParam[32] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        ObjParam[33] = new SqlParameter("@DeliveryCharges", objBill.DeliveryCharges);
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_UpdateBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

     public Int32 UpdateVatBill(Bill objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[37];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BillNowPrefix", objBill.BillNowPrefix);
        ObjParam[31] = new SqlParameter("@BranchId", objBill.BranchId);
        ObjParam[32] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        ObjParam[33] = new SqlParameter("@DeliveryCharges", objBill.DeliveryCharges);
        ObjParam[34] = new SqlParameter("@LocalOut", objBill.LocalOut);
        ObjParam[35] = new SqlParameter("@TransportMode", objBill.TransportMode);
        ObjParam[36] = new SqlParameter("@VehicleNo", objBill.VehicleNo);
         
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_UpdateVatBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


    public Int32 InsertHoldBill(holdbill objHoldBill, DataTable dt)
    {


        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[11];
        ObjParam[0] = new SqlParameter("@Customer_ID", objHoldBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@CustomerName", objHoldBill.Customer_Name);
        ObjParam[2] = new SqlParameter("@BillValue", objHoldBill.Bill_Value);
        ObjParam[3] = new SqlParameter("@LessDisAmt", objHoldBill.DiscountPer);
        ObjParam[4] = new SqlParameter("@TaxAmt", objHoldBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@NetAmt", objHoldBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@UserId", objHoldBill.User_ID);
        ObjParam[7] = new SqlParameter("@BillMode", objHoldBill.BillMode);

        ObjParam[8] = new SqlParameter("@HoldDetail", dt);

        ObjParam[9] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[9].Direction = ParameterDirection.ReturnValue;
        ObjParam[10] = new SqlParameter("@BranchId", objHoldBill.BranchId);

        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertHoldBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[9].Value);
            objHoldBill.Hold_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }




    public SqlDataReader GetByHoldNo(int HoldNo,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@HoldNo", HoldNo);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetByHoldNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByOrderNo(Int32 OrderNo)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderNo", OrderNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetByOrderNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader HoldBillGetAll(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "POS_sp_HoldBillGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader OrdersGetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetAllOrders", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetBillDetailByBillNo(string BillNowPrefix,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetBillDetailByBillNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public int Delete(Bill objBill)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BillNowPrefix", objBill.BillNowPrefix);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_deletebill", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBill.Bill_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public SqlDataReader GetOrdersBillByDate(DateTime DateFrom, DateTime DateTo,string ItemType, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@ItemType", ItemType);
        ObjParam[3] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Master_Sp_GetOrderOrBillsByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetOptions( string ProcessType)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ProcessType", ProcessType);

     
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_Sp_GetOptionsByType", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
	
}