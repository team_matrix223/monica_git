﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for ColorDAL
/// </summary>
public class ColorDAL:Connection
{

    public SqlDataReader GetById(Colors objColors)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Color_ID", objColors.Color_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ColorGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAll()
    {
        List<Colors> ColorList = new List<Colors>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllColors", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Colors objColors)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@Color_ID", objColors.Color_ID);
        objParam[1] = new SqlParameter("@Color_Name", objColors.Color_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objColors.UserId);
        objParam[4] = new SqlParameter("@IsActive", objColors.IsActive);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ColorInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objColors.Color_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Colors objColor)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Color_ID", objColor.Color_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ColorDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objColor.Color_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}