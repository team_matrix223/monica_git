﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for HeadingsDAL
/// </summary>
public class PurchaseDAL : Connection
{
    public SqlDataReader GetPurchaseDetails(int GrnNo,int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@GrnNo", GrnNo);
        objParam[1] = new SqlParameter("@BranchId",BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseDetailGetByGrnNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


   
    
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[1];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo,int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetAllSupplier()
    {
        SqlParameter[] objParam = new SqlParameter[1];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllSupplier", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public int InsertUpdate(Purchase objPurchase,DataTable dt,DataTable dt1)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[45];
 
 

        objParam[0] = new SqlParameter("@GrnNo", objPurchase.GrnNo);
        objParam[1] = new SqlParameter("@GrnDate", objPurchase.GRNDate);
        objParam[2] = new SqlParameter("@BillNo", objPurchase.BillNo);
        objParam[3] = new SqlParameter("@BillDate", objPurchase.BillDate);
        objParam[4] = new SqlParameter("@GRNo", objPurchase.GRNo);
        objParam[5] = new SqlParameter("@GRDate", objPurchase.GRDate);
        objParam[6] = new SqlParameter("@VehNo", objPurchase.VehNo);
        objParam[7] = new SqlParameter("@Dis1InRs", objPurchase.Dis1InRs);
        objParam[8] = new SqlParameter("@Dis2InRs", objPurchase.Dis2InRs);
        objParam[9] = new SqlParameter("@IsLocal", objPurchase.IsLocal);
        objParam[10] = new SqlParameter("@TaxAfterDis1", objPurchase.TaxAfterDis1);
        objParam[11] = new SqlParameter("@TaxAfterDis2", objPurchase.TaxAfterDis2);
        objParam[12] = new SqlParameter("@Remarks", objPurchase.Remarks);
        objParam[13] = new SqlParameter("@BillValue", objPurchase.BillValue);
        objParam[14] = new SqlParameter("@Dis1Amt", objPurchase.Dis1Amt);
        objParam[15] = new SqlParameter("@Dis2Amt", objPurchase.Dis2Amt);
        objParam[16] = new SqlParameter("@Dis3AftDis1PDis2", objPurchase.Dis3AftDis1PDis2);
        objParam[17] = new SqlParameter("@Dis3P", objPurchase.Dis3P);
        objParam[18] = new SqlParameter("@Dis3Amt", objPurchase.Dis3Amt);
        objParam[19] = new SqlParameter("@TaxP", objPurchase.TaxP);
        objParam[20] = new SqlParameter("@TaxAmt", objPurchase.TaxAmt);
        objParam[21] = new SqlParameter("@TotalAmount", objPurchase.TotalAmount);
        objParam[22] = new SqlParameter("@ODisP", objPurchase.ODisP);
        objParam[23] = new SqlParameter("@ODisAmt", objPurchase.ODisAmt);
        objParam[24] = new SqlParameter("@DisplayAmount", objPurchase.DisplayAmount);
        objParam[25] = new SqlParameter("@Adjustment", objPurchase.Adjustment);
        objParam[26] = new SqlParameter("@NetAmount", objPurchase.NetAmount);
        objParam[27] = new SqlParameter("@Pass", objPurchase.Pass);
        objParam[28] = new SqlParameter("@Dis2AftDedDis1", objPurchase.Dis2AftDedDis1);
        objParam[29] = new SqlParameter("@SupplierId", objPurchase.SupplierId);
        objParam[30] = new SqlParameter("@GodownID", objPurchase.GodownId);
        objParam[31] = new SqlParameter("@ExciseAmt", objPurchase.ExciseAmt);
        objParam[32] = new SqlParameter("@SurchrgAmt", objPurchase.SurchrgAmt);
        objParam[36] = new SqlParameter("@RawChk", objPurchase.RawChk);
        objParam[37] = new SqlParameter("@Purtype", objPurchase.Purtype);
        objParam[38] = new SqlParameter("@Tcs_AccCode", objPurchase.Tcs_AccCode);
        objParam[39] = new SqlParameter("@tcs_Amount", objPurchase.tcs_Amount);
        objParam[40] = new SqlParameter("@Tcstax", objPurchase.Tcstax);
        objParam[42] = new SqlParameter("@UserNo", objPurchase.UserNo);
        objParam[33] = new SqlParameter("@PurchaseDetails", dt);
        objParam[34] = new SqlParameter("@PurchaseTax", dt1);
        objParam[35] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[41] = new SqlParameter("@BranchId", objPurchase.BranchId);
        objParam[43] = new SqlParameter("@BNF", objPurchase.BNF);
        objParam[44] = new SqlParameter("@fright", objPurchase.fright);
        objParam[35].Direction = ParameterDirection.ReturnValue;
      
 
      
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PurchaseInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[35].Value);
            objPurchase.GrnNo = retValue;
        }
        finally
        {
            objParam = null;
        }
        //string GRNO = objPurchase.GrnNo.ToString();
        //int branchid =Convert.ToInt32( objPurchase.BranchId);
        //RptPurchase purchase = new RptPurchase(GRNO, branchid);
        //purchase.Print();

        return retValue;




    }


}