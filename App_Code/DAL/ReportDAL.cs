﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ReportDAL
/// </summary>
public class ReportDAL:Connection
{
    public SqlDataReader GetAllForReport(Int32 BillType, string BillMode, string datefrom, string dateto,Int32 BranchId,string SaleType)
    {

        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@BillType", BillType);
        objParam[1] = new SqlParameter("@BillMode", BillMode);
        objParam[2] = new SqlParameter("@FromDate", datefrom);
        objParam[3] = new SqlParameter("@ToDate", dateto);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        objParam[5] = new SqlParameter("@SaleType", SaleType);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_SaleDetailedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetGSTSaleReport(string datefrom, string dateto, Int32 BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[6];
       
        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "Report_sp_GSTBillDetailedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetDeliveryIssueConsolidate(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];
       
        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);
       
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_issueconsolidated", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }

    public SqlDataReader GetItemSaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedItemWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }



    public SqlDataReader GetItemSaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedItemWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetGroupSaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedGroupWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }



    public SqlDataReader GetGroupSaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedGroupWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetHeader(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
     

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "Report_sp_GetHeader", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }



    public SqlDataReader GetSubGroupSaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedSubGroupWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }

    public SqlDataReader GetSubGroupSaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedSubGroupWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetDeptSaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedDeptWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetDeptSaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedDeptWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetTaxSaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedTaxWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }

    public SqlDataReader GetTaxSaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedTaxWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetCompanySaleClubbed(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedCompanyWiseSale", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetCompanySaleClubbedByQty(string datefrom, string dateto)
    {

        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@FromDate", datefrom);
        objParam[1] = new SqlParameter("@ToDate", dateto);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_ClubbedCompanyWiseSaleByQty", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }


    public SqlDataReader GetAllVATForReport(Int32 BillType, string BillMode, string datefrom, string dateto, Int32 BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@BillType", BillType);
        objParam[1] = new SqlParameter("@BillMode", BillMode);
        objParam[2] = new SqlParameter("@FromDate", datefrom);
        objParam[3] = new SqlParameter("@ToDate", dateto);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_VATSaleDetailedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


      public SqlDataReader GetSaleDatedReport(Int32 BillType, string BillMode, string datefrom, string dateto,Int32 BranchId,string SaleType)
    {

        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@BillType", BillType);
        objParam[1] = new SqlParameter("@BillMode", BillMode);
        objParam[2] = new SqlParameter("@FromDate", datefrom);
        objParam[3] = new SqlParameter("@ToDate", dateto);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        objParam[5] = new SqlParameter("@SaleType", SaleType);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_SaleDatedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


      public SqlDataReader GetAdvanceBooking(string BillMode, Int32 BranchId,DateTime DateFrom,DateTime DateTo)
      {

          SqlParameter[] objParam = new SqlParameter[4];

          objParam[0] = new SqlParameter("@Mode", BillMode);
          objParam[1] = new SqlParameter("@BranchId", BranchId);
          objParam[2] = new SqlParameter("@FromDate", DateFrom);
          objParam[3] = new SqlParameter("@TODate", DateTo);
          SqlDataReader dr = null;
          try
          {
              dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "Report_sp_GetAdvanceBooking", objParam);


          }

          finally
          {
              objParam = null;
          }
          return dr;

      }

      public SqlDataReader GetCurrentStockClubbed(string ItemType, string Group, string Dept)
      {

          SqlParameter[] objParam = new SqlParameter[3];

          objParam[0] = new SqlParameter("@ItemType", ItemType);
          objParam[1] = new SqlParameter("@groupqry", Group);
          objParam[2] = new SqlParameter("@deptqry", Dept);

          SqlDataReader dr = null;
          try
          {
              dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "report_sp_ClubbedCurrentStock", objParam);


          }

          finally
          {
              objParam = null;
          }
          return dr;
      }
    
}