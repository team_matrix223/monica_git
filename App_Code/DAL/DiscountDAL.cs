﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for DiscountDAL
/// </summary>
public class DiscountDAL:Connection
{
    public SqlDataReader GetDiscountsByCategory(string Category)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Category", Category);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DiscountGetAllByCategory", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(DiscountMaster objDiscount)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[17];

        objParam[0] = new SqlParameter("@Discount_ID", objDiscount.Discount_ID);
        objParam[1] = new SqlParameter("@Description", objDiscount.Description);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@Category", objDiscount.Category);
        objParam[4] = new SqlParameter("@Start_Date", objDiscount.Start_Date);
        objParam[5] = new SqlParameter("@End_Date", objDiscount.End_Date);
        objParam[6] = new SqlParameter("@Company_ID", objDiscount.Company_ID);

        objParam[7] = new SqlParameter("@Group_ID", objDiscount.Group_ID);
        objParam[8] = new SqlParameter("@SGroup_ID", objDiscount.SGroup_ID);
        objParam[9] = new SqlParameter("@Discount_Per", objDiscount.Discount_Per);
        objParam[10] = new SqlParameter("@Discount_Amt", objDiscount.Discount_Amt);
        objParam[11] = new SqlParameter("@FULLDAY", objDiscount.FULLDAY);
        objParam[12] = new SqlParameter("@START_TIME", objDiscount.START_TIME);
        objParam[13] = new SqlParameter("@END_TIME", objDiscount.END_TIME);
        objParam[14] = new SqlParameter("@UserId", objDiscount.UserId);
        objParam[15] = new SqlParameter("@IsActive", objDiscount.IsActive);
        objParam[16] = new SqlParameter("@Item_ID", objDiscount.Item_ID);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateDiscount", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objDiscount.Discount_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(DiscountMaster objDiscount)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Discount_ID", objDiscount.Discount_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DiscountDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objDiscount.Discount_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}