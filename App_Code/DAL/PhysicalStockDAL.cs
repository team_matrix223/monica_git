﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PhysicalStockDAL
/// </summary>
public class PhysicalStockDAL:Connection
{
    public SqlDataReader GetAll(string Type)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Type", Type);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllGroupsForStock", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAllPrroductsByType(string Type,string Sorting,Int32 Id,string Name,string OrderType,int Branch)
    {

        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@Type", Type);
        objParam[1] = new SqlParameter("@Sorting", Sorting);
        objParam[2] = new SqlParameter("@Id", Id);
        objParam[3] = new SqlParameter("@Name", Name);
        objParam[4] = new SqlParameter("@Ordertype", OrderType);
        objParam[5] = new SqlParameter("@Branch", Branch);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllProductsByType", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public int Insert(Int32 GodownId, DataTable dt, string Status, int BranchId, int UserNo)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@GodownId", GodownId);
        objParam[1] = new SqlParameter("@Status", Status);
        objParam[2] = new SqlParameter("@PhysicalDetails", dt);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        objParam[5] = new SqlParameter("@UserNo", UserNo);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertPhysicalStock", objParam);
           
        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }
}