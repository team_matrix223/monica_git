﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProductDAL
/// </summary>
public class ProductDAL:Connection
{


    public SqlDataReader GetById(int ItemId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemId", ItemId);
    
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PackingBelongsGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public DataSet GetAll(int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAll", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public DataSet KeywordSearch(string Keyword, string Type,string BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@branchId", BranchId);


        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PackingBelongsKeywordSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public SqlDataReader GetByItemType(string ItemType)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ItemType", ItemType);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetItemsByType", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader AdvancedSearchForMenuCard(int CategoryId, string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearchForMenuCard", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public void UpdateImageUrl(Int64 ProductId,string ImageUrl)
    {

 
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@ItemID", ProductId);
        objParam[1] = new SqlParameter("@ImageUrl", ImageUrl);
       
   
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
          "pos_sp_Packing_BelongsUpdateImageUrl", objParam);
           
        }
        finally
        {
            objParam = null;
        }
       
    }


    

    public SqlDataReader AdvancedSearch(int CategoryId,string Keyword,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);
        objParam[2] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetByItemCode(string Code, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllProduct()
    {



        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAll", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

}