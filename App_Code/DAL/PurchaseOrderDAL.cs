﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for HeadingsDAL
/// </summary>
public class PurchaseOrderDAL : Connection
{
    public SqlDataReader GetPurchaseOrderDetails(int OrderNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderNo", OrderNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseOrderDetailGetByOrderNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



    public SqlDataReader GetPurchaseOrderImport(int OrderNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OrderNo", OrderNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseOrderImport", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetBillImport(string BNF)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BIllNowPrefix", BNF);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BillOrderImport", objParam);

        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[1];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseOrderGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetByDate(DateTime DateFrom , DateTime DateTo,int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseOrderGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public int InsertUpdate(PurchaseOrder objPurchaseOrder,DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[29];
 
 

        objParam[0] = new SqlParameter("@OrderNo", objPurchaseOrder.OrderNo);
        objParam[1] = new SqlParameter("@OrderDate", objPurchaseOrder.OrderDate);
        objParam[2] = new SqlParameter("@Dis1InRs", objPurchaseOrder.Dis1InRs);
        objParam[3] = new SqlParameter("@Dis2InRs", objPurchaseOrder.Dis2InRs);
        objParam[4] = new SqlParameter("@IsLocal", objPurchaseOrder.IsLocal);
        objParam[5] = new SqlParameter("@TaxAfterDis1", objPurchaseOrder.TaxAfterDis1);
        objParam[6] = new SqlParameter("@TaxAfterDis2", objPurchaseOrder.TaxAfterDis2);
        objParam[7] = new SqlParameter("@Remarks", objPurchaseOrder.Remarks);
        objParam[8] = new SqlParameter("@BillValue", objPurchaseOrder.BillValue);
        objParam[9] = new SqlParameter("@Dis1Amt", objPurchaseOrder.Dis1Amt);
        objParam[10] = new SqlParameter("@Dis2Amt", objPurchaseOrder.Dis2Amt);
        objParam[11] = new SqlParameter("@Dis3AftDis1PDis2", objPurchaseOrder.Dis3AftDis1PDis2);
        objParam[12] = new SqlParameter("@Dis3P", objPurchaseOrder.Dis3P);
        objParam[13] = new SqlParameter("@Dis3Amt", objPurchaseOrder.Dis3Amt);
        objParam[14] = new SqlParameter("@TaxP", objPurchaseOrder.TaxP);
        objParam[15] = new SqlParameter("@TaxAmt", objPurchaseOrder.TaxAmt);
        objParam[16] = new SqlParameter("@TotalAmount", objPurchaseOrder.TotalAmount);
        objParam[17] = new SqlParameter("@ODisP", objPurchaseOrder.ODisP);
        objParam[18] = new SqlParameter("@ODisAmt", objPurchaseOrder.ODisAmt);
        objParam[19] = new SqlParameter("@DisplayAmount", objPurchaseOrder.DisplayAmount);
        objParam[20] = new SqlParameter("@Adjustment", objPurchaseOrder.Adjustment);
        objParam[21] = new SqlParameter("@NetAmount", objPurchaseOrder.NetAmount);
        objParam[22] = new SqlParameter("@Dis2AftDedDis1", objPurchaseOrder.Dis2AftDedDis1);
        objParam[23] = new SqlParameter("@SupplierId", objPurchaseOrder.SupplierId);
        objParam[24] = new SqlParameter("@GodownID", objPurchaseOrder.GodownId);
        objParam[25] = new SqlParameter("@PurchaseOrderDetails", dt);
        objParam[26] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[26].Direction = ParameterDirection.ReturnValue;
        objParam[27] = new SqlParameter("@BranchId", objPurchaseOrder.BranchId);
        objParam[28] = new SqlParameter("@UserNo", objPurchaseOrder.UserNo);
 
      
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PurchaseOrderInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[26].Value);
            objPurchaseOrder.OrderNo = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


}