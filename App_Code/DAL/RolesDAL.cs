﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for RolesDAL
/// </summary>
public class RolesDAL:Connection
{
    public string GetRolesByDesignationAndPage(int DesignationId, int PageId)
    {
        string retValue = "";
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@DesignationId", DesignationId);
        objParam[1] = new SqlParameter("@PageId", PageId);

        try
        {
            retValue = SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
              "master_sp_GetRolesByDesignationAndPage", objParam).ToString();
        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }

    public DataSet GetRolesByDesignation(int DesignationId)
    {
        SqlParameter[] objParam = new SqlParameter[1];


        objParam[0] = new SqlParameter("@DesignationId", DesignationId);
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetRolesByDesignation", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet GetAllDataSet()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_RolesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_RolesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Roles objRoles)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];

        objParam[0] = new SqlParameter("@RoleId", objRoles.RoleId);
        objParam[1] = new SqlParameter("@Title", objRoles.Title);
        objParam[2] = new SqlParameter("@IsActive", objRoles.IsActive);

        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_RolesInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[3].Value);
            objRoles.RoleId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 AssignRoles(int DesignationId, DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@DesignationId", DesignationId);
        objParam[1] = new SqlParameter("@dtRoles", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_RolesAssignRoles", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    

}