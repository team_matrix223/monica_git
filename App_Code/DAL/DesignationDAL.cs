﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for DesignationDAL
/// </summary>
public class DesignationDAL:Connection
{

    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllDesignations", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Designations objDesignation)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@DesgID ", objDesignation.DesgID);
        objParam[1] = new SqlParameter("@DesgName", objDesignation.DesgName);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objDesignation.IsActive);
        objParam[4] = new SqlParameter("@UserId", objDesignation.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DesignationInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objDesignation.DesgID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetById(Designations objDesigantion)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@DesgID", objDesigantion.DesgID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DesignationGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}