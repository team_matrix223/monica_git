﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for PurchaseReturnDAL
/// </summary>
public class PurchaseReturnDAL : Connection
{
	public PurchaseReturnDAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public SqlDataReader GetPurchaseReturnDetails(int GrnNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@GrnNo", GrnNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseReturnDetailGetByGrnNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[1];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseReturnGetAll", objParam);

        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo,Int32 BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PurchaseReturnGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public int InsertUpdate(PurchaseReturn objPurchase, DataTable dt,DataTable dt1)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[40];



        objParam[0] = new SqlParameter("@GrnNo", objPurchase.GrnNo);
        objParam[1] = new SqlParameter("@GrnDate", objPurchase.GRNDate);
        objParam[2] = new SqlParameter("@BillNo", objPurchase.BillNo);
        objParam[3] = new SqlParameter("@BillDate", objPurchase.BillDate);
        objParam[4] = new SqlParameter("@GRNo", objPurchase.GRNo);
        objParam[5] = new SqlParameter("@GRDate", objPurchase.GRDate);
        objParam[6] = new SqlParameter("@VehNo", objPurchase.VehNo);
        objParam[7] = new SqlParameter("@Dis1InRs", objPurchase.Dis1InRs);
        objParam[8] = new SqlParameter("@Dis2InRs", objPurchase.Dis2InRs);
        objParam[28] = new SqlParameter("@Dis2AftDedDis1", objPurchase.Dis2AftDedDis1);
        objParam[9] = new SqlParameter("@IsLocal", objPurchase.IsLocal);
        objParam[10] = new SqlParameter("@TaxAfterDis1", objPurchase.TaxAfterDis1);
        objParam[11] = new SqlParameter("@TaxAfterDis2", objPurchase.TaxAfterDis2);
        objParam[12] = new SqlParameter("@Remarks", objPurchase.Remarks);
        objParam[13] = new SqlParameter("@BillValue", objPurchase.BillValue);
        objParam[14] = new SqlParameter("@Dis1Amt", objPurchase.Dis1Amt);
        objParam[15] = new SqlParameter("@Dis2Amt", objPurchase.Dis2Amt);
        objParam[16] = new SqlParameter("@Dis3AftDis1PDis2", objPurchase.Dis3AftDis1PDis2);
        objParam[17] = new SqlParameter("@Dis3P", objPurchase.Dis3P);
        objParam[18] = new SqlParameter("@Dis3Amt", objPurchase.Dis3Amt);
        objParam[19] = new SqlParameter("@TaxP", objPurchase.TaxP);
        objParam[20] = new SqlParameter("@TaxAmt", objPurchase.TaxAmt);
        objParam[21] = new SqlParameter("@TotalAmount", objPurchase.TotalAmount);
        objParam[22] = new SqlParameter("@ODisP", objPurchase.ODisP);
        objParam[23] = new SqlParameter("@ODisAmt", objPurchase.ODisAmt);
        objParam[24] = new SqlParameter("@DisplayAmount", objPurchase.DisplayAmount);
        objParam[25] = new SqlParameter("@Adjustment", objPurchase.Adjustment);
        objParam[26] = new SqlParameter("@NetAmount", objPurchase.NetAmount);
        objParam[27] = new SqlParameter("@Pass", objPurchase.Pass);
        objParam[29] = new SqlParameter("@SupplierId", objPurchase.SupplierId);
        objParam[30] = new SqlParameter("@GodownID", objPurchase.GodownId);
        objParam[31] = new SqlParameter("@ExciseAmt", objPurchase.ExciseAmt);
        objParam[37] = new SqlParameter("@Expiry", objPurchase.Expiry);
        objParam[33] = new SqlParameter("@PurchaseDetails", dt);
        objParam[34] = new SqlParameter("@PurchaseTax", dt1);
        objParam[35] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[35].Direction = ParameterDirection.ReturnValue;
        objParam[38] = new SqlParameter("@BranchId", objPurchase.BranchId);
        objParam[39] = new SqlParameter("@UserNo", objPurchase.UserNo);


        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PurchaseReturnInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[35].Value);
            objPurchase.GrnNo = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }
}