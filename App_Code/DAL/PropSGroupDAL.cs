﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for SGroupDAL
/// </summary>
public class PropSGroupDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_SGroupsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByGroupId(int GroupId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Group_Id", GroupId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PropSGroupGetByGroupId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(PropSGroups objPropSGroup)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@SGroup_ID", objPropSGroup.SGroup_ID);
        objParam[1] = new SqlParameter("@SGroup_Name", objPropSGroup.SGroup_Name);
        objParam[2] = new SqlParameter("@ShowInMenu", objPropSGroup.ShowInMenu);
        objParam[3] = new SqlParameter("@Department_Id", objPropSGroup.Department_Id);
        objParam[4] = new SqlParameter("@Group_Id", objPropSGroup.Group_Id);
        objParam[5] = new SqlParameter("@IsActive", objPropSGroup.IsActive);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@UserId", objPropSGroup.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SGroupsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objPropSGroup.SGroup_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(PropSGroups objPrpSGroup)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@SGroup_ID", objPrpSGroup.SGroup_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PropSgroupDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objPrpSGroup.SGroup_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}