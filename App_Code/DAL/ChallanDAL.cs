﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ChallanDAL
/// </summary>
public class ChallanDAL:Connection
{

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "pos_sp_GetAllChallanBills", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetBillDetailByBillNo(string BillNowPrefix, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BillNowPrefix", BillNowPrefix);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetChallanDetailByChallanNo", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public int Delete(Bill objBill)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BillNowPrefix", objBill.BillNowPrefix);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_Challandeletebill", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBill.Bill_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

    public Int32 Insert(ChallanMaster objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[32];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BranchId", objBill.BranchId);

        ObjParam[31] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_ChallanInsertBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }



    public Int32 Update(ChallanMaster objBill, DataTable dt, DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[33];
        ObjParam[0] = new SqlParameter("@Customer_ID", objBill.Customer_ID);

        ObjParam[1] = new SqlParameter("@Bill_Value", objBill.Bill_Value);
        ObjParam[2] = new SqlParameter("@DiscountPer", objBill.DiscountPer);
        ObjParam[3] = new SqlParameter("@Less_Dis_Amount", objBill.Less_Dis_Amount);
        ObjParam[4] = new SqlParameter("@Add_Tax_Amount", objBill.Add_Tax_Amount);
        ObjParam[5] = new SqlParameter("@Net_Amount", objBill.Net_Amount);
        ObjParam[6] = new SqlParameter("@BillMode", objBill.BillMode);
        ObjParam[7] = new SqlParameter("@CreditBank", objBill.CreditBank);

        ObjParam[8] = new SqlParameter("@UserNO", objBill.UserNO);
        ObjParam[9] = new SqlParameter("@Bill_Type", objBill.Bill_Type);
        ObjParam[10] = new SqlParameter("@Cash_Amount", objBill.Cash_Amount);
        ObjParam[11] = new SqlParameter("@Credit_Amount", objBill.Credit_Amount);
        ObjParam[12] = new SqlParameter("@CrCard_Amount", objBill.CrCard_Amount);
        ObjParam[13] = new SqlParameter("@Round_Amount", objBill.Round_Amount);

        ObjParam[14] = new SqlParameter("@Bill_Printed", objBill.Bill_Printed);
        ObjParam[15] = new SqlParameter("@Passing", objBill.Passing);
        ObjParam[16] = new SqlParameter("@CashCust_Code", objBill.CashCust_Code);
        ObjParam[17] = new SqlParameter("@CashCust_Name", objBill.CashCust_Name);
        ObjParam[18] = new SqlParameter("@Tax_Per", objBill.Tax_Per);
        ObjParam[19] = new SqlParameter("@R_amount", objBill.R_amount);

        ObjParam[20] = new SqlParameter("@tableno", objBill.tableno);
        ObjParam[21] = new SqlParameter("@remarks", objBill.remarks);
        ObjParam[22] = new SqlParameter("@servalue", objBill.servalue);
        ObjParam[23] = new SqlParameter("@ReceiviedGRNNo", objBill.ReceiviedGRNNo);
        ObjParam[24] = new SqlParameter("@EmpCode", objBill.EmpCode);
        ObjParam[25] = new SqlParameter("@OrderNo", objBill.OrderNo);
        ObjParam[26] = new SqlParameter("@BillDetail", dt);
        ObjParam[27] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@CustomerName", objBill.Customer_Name);
        ObjParam[30] = new SqlParameter("@BillNowPrefix", objBill.BillNowPrefix);
        ObjParam[31] = new SqlParameter("@BranchId", objBill.BranchId);
        ObjParam[32] = new SqlParameter("@OnlinePayment", objBill.OnlinePayment);
        try
        {

            objBill.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_ChallanUpdateBill", ObjParam));
            //DataSet ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertBill", ObjParam);
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBill.Bill_No = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }
    public SqlDataReader GetChallanBYCustomer(DateTime DateFrom, DateTime DateTo, string Customer, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@DateFrom", DateFrom);
        ObjParam[1] = new SqlParameter("@DateTo", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        ObjParam[3] = new SqlParameter("@CustomerID", Customer);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Pos_sp_GetChallanDetailCustomerWise", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetChallanDetail(DataTable dt, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@BIllno", dt);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
        
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Pos_sp_GetAllChallanDetail", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }



    public SqlDataReader GetChallanDetailVAT(DataTable dt, int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@BIllno", dt);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);

        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Pos_sp_GetAllChallanDetailVat", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }




    public Int32 ConvertChallanToBill(DataTable dt, int BranchId, decimal BillValue, decimal Tax, decimal Discount, decimal NetAmount, int UserNO, decimal Round_Amount, int Customer_ID, string CustomerName, DataTable dt1,ChallanMaster objChallan,DateTime BillDate)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[13];
        ObjParam[0] = new SqlParameter("@BIllno1", dt);
        ObjParam[1] = new SqlParameter("@BranchId",BranchId);
        ObjParam[2] = new SqlParameter("@BillValue", BillValue);
        ObjParam[3] = new SqlParameter("@Tax", Tax);
        ObjParam[4] = new SqlParameter("@Discount", Discount);
        ObjParam[5] = new SqlParameter("@NetAmount", NetAmount);
        ObjParam[6] = new SqlParameter("@UserNO", UserNO);
        ObjParam[7] = new SqlParameter("@Round_Amount",Round_Amount);
        ObjParam[8] = new SqlParameter("@Customer_ID", Customer_ID);
        ObjParam[9] = new SqlParameter("@CustomerName", CustomerName);
        ObjParam[11] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[12] = new SqlParameter("@BillDate", BillDate);
        ObjParam[10] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[10].Direction = ParameterDirection.ReturnValue;
   
        try
        {

            objChallan.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "Pos_sp_ConvertChallanToBill", ObjParam));
            retval = Convert.ToInt32(ObjParam[10].Value);
          
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public Int32 ConvertChallanToBillVat(DataTable dt, int BranchId, decimal BillValue, decimal Tax, decimal Discount, decimal NetAmount, int UserNO, decimal Round_Amount, int Customer_ID, string CustomerName, DataTable dt1,ChallanMaster objChallan,DateTime BillDate)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[13];
        ObjParam[0] = new SqlParameter("@BIllno1", dt);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
        ObjParam[2] = new SqlParameter("@BillValue", BillValue);
        ObjParam[3] = new SqlParameter("@Tax", Tax);
        ObjParam[4] = new SqlParameter("@Discount", Discount);
        ObjParam[5] = new SqlParameter("@NetAmount", NetAmount);
        ObjParam[6] = new SqlParameter("@UserNO", UserNO);
        ObjParam[7] = new SqlParameter("@Round_Amount", Round_Amount);
        ObjParam[8] = new SqlParameter("@Customer_ID", Customer_ID);
        ObjParam[9] = new SqlParameter("@CustomerName", CustomerName);
        ObjParam[11] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[12] = new SqlParameter("@BillDate", BillDate);
        ObjParam[10] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[10].Direction = ParameterDirection.ReturnValue;

        try
        {

           objChallan.BillNowPrefix = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "Pos_sp_ConvertChallanToBillvat", ObjParam));
            retval = Convert.ToInt32(ObjParam[10].Value);

        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }



    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId,string Type)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        ObjParam[3] = new SqlParameter("@Type", Type);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetAllBillsConvertedToChallan", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

}