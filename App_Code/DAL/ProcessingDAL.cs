﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProcessingDAL
/// </summary>
public class ProcessingDAL:Connection
{
    public SqlDataReader GetByDepartment(string Department,string Status)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Department", Department);
        objParam[1] = new SqlParameter("@Status", Status);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProcessingItems", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
 

    public SqlDataReader MarkDelivered(Int32 ProcessingId,string TableNo)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ProcessingId", ProcessingId);
        objParam[1] = new SqlParameter("@TableNo", TableNo);

        

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProcessingMarkDelivered", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetForDelivery()
    {

        SqlParameter[] objParam = new SqlParameter[0];
       
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProcessingGetForDelivery", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


   



     public SqlDataReader GetNewProcessingItems(DataTable dt,string DepartmentName)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ProcessingList", dt);
        objParam[1] = new SqlParameter("@DepartmentName", DepartmentName);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProcessingGetNewProcessingItems", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    

    public void UpdateProcessingStatus(string Status,Int32 ProcessingId)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@Status", Status);
        ObjParam[1] = new SqlParameter("@ProcessingId", ProcessingId);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure, "pos_sp_ProcessingUpdateStatus", ObjParam);


        }
        finally
        {
            ObjParam = null;

        }

    }


   


}