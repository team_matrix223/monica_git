﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for NotificationDAL
/// </summary>
public class NotificationDAL:Connection
{

    public Int32 InsertUpdate(Notification objNotify)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[10];
        ObjParam[0] = new SqlParameter("@Notification1", objNotify.Notification1);
        ObjParam[1] = new SqlParameter("@Notification2",objNotify. Notification2);
        ObjParam[2] = new SqlParameter("@Declaration", objNotify.Declaration);
        ObjParam[3] = new SqlParameter("@Range", objNotify.Range);
        ObjParam[4] = new SqlParameter("@Division", objNotify.Division);
        ObjParam[5] = new SqlParameter("@CommissionRate", objNotify.CommissionRate);
        ObjParam[6] = new SqlParameter("@TINNO", objNotify.TINNO);
        ObjParam[7] = new SqlParameter("@PANNO", objNotify.PANNO);
        ObjParam[8] = new SqlParameter("@CERegn", objNotify.CERegn);
        ObjParam[9] = new SqlParameter("@BranchId", objNotify.BranchId);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
                "pos_sp_InsertUpdateNotification", ObjParam));
          
           
            
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }


    public SqlDataReader GetAll(int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "GetAllNotification", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

}