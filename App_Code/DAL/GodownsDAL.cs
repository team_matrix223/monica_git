﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
public class GodownsDAL : Connection
{
     public SqlDataReader GetAll()
    {
         
        SqlParameter[] objParam = new SqlParameter[0];
         SqlDataReader dr=null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "master_sp_GodownsGetAll", objParam);
          
         
         }

         finally
         {
             objParam = null;
         }
         return dr;

    }


     public SqlDataReader GetGodownByStock(int BranchId)
     {
         SqlParameter[] objParam = new SqlParameter[1];
         objParam[0] = new SqlParameter("@BranchId", BranchId);
         SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "master_sp_GodownsGetByStock", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }


     public SqlDataReader GetGodownByType(int BranchId,string type)
     {
       
         SqlParameter[] objParam = new SqlParameter[2];
         objParam[0] = new SqlParameter("@BranchId", BranchId);
         objParam[1] = new SqlParameter("@Type", type);
         SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "master_sp_GodownsGetBytype", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }

    //    public SqlDataReader GetGodownByStock()
    //{
         
    //    SqlParameter[] objParam = new SqlParameter[0];
    //     SqlDataReader dr=null;
    //     try
    //     {
    //         dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
    //         "master_sp_GodownsGetByStock", objParam);
          
         
    //     }

    //     finally
    //     {
    //         objParam = null;
    //     }
    //     return dr;

    //}


        public Int16 InsertUpdate(Godowns objGodowns)
        {

            Int16 retValue = 0;
            SqlParameter[] objParam = new SqlParameter[8];

            objParam[0] = new SqlParameter("@Godown_Id", objGodowns.Godown_Id);
            objParam[1] = new SqlParameter("@Godown_Name", objGodowns.Godown_Name);
            objParam[2] = new SqlParameter("@Divison", objGodowns.Divison);
            objParam[3] = new SqlParameter("@FinancialYear", objGodowns.FinacialYear);
            objParam[4] = new SqlParameter("@Caption", objGodowns.Caption);
            objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
            objParam[5].Direction = ParameterDirection.ReturnValue;
            objParam[6] = new SqlParameter("@UserId", objGodowns.UserId);
            objParam[7] = new SqlParameter("@IsActive", objGodowns.IsActive);
            try
            {
                SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
               "master_sp_GodownInsertUpdate", objParam);
                retValue = Convert.ToInt16(objParam[5].Value);
                objGodowns.Godown_Id = retValue;
            }
            finally
            {
                objParam = null;
            }
            return retValue;
        }

        public int Delete(Godowns objGodown)
        {
            int retValue = 0;
            SqlParameter[] objParam = new SqlParameter[2];
            objParam[0] = new SqlParameter("@Godown_Id", objGodown.Godown_Id);

            objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
            objParam[1].Direction = ParameterDirection.ReturnValue;
            try
            {
                SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
               "master_sp_GodownDeleteById", objParam);
                retValue = Convert.ToInt32(objParam[1].Value);
                objGodown.Godown_Id = retValue;
            }
            finally
            {
                objParam = null;
            }
            return retValue;


        }
}