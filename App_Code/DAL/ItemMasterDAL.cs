﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for ItemMasterDAL
/// </summary>
public class ItemMasterDAL:Connection
{


    public DataSet VariableMastersGetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_VariableMastersGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public SqlDataReader GetAll()
    {
        List<ItemMaster> ItemsList = new List<ItemMaster>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ProductsGetAll", objParam);


        }
        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllDiscontinued()
    {
        List<ItemMaster> ItemsList = new List<ItemMaster>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ProductsGetAllDiscontinued", objParam);


        }
        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetById(ItemMaster objItemMaster)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ItemMaster_ID", objItemMaster);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ItemMasterGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }




    public Int32 InsertUpdate(ItemMaster objItemMaster,DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[72];

        objParam[0] = new SqlParameter("@ItemID", objItemMaster.ItemID);
        objParam[1] = new SqlParameter("@COMPANY_ID", objItemMaster.COMPANY_ID);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@GROUP_ID", objItemMaster.GROUP_ID);
        objParam[4] = new SqlParameter("@Master_Code", objItemMaster.Master_Code);
        objParam[5] = new SqlParameter("@Item_Code", objItemMaster.Item_Code);
        objParam[6] = new SqlParameter("@Item_Name", objItemMaster.Item_Name);
        objParam[7] = new SqlParameter("@Packing", objItemMaster.Packing);
        objParam[8] = new SqlParameter("@Bar_Code", objItemMaster.Bar_Code);
        objParam[9] = new SqlParameter("@Short_Name1", objItemMaster.Short_Name1);
        objParam[10] = new SqlParameter("@Short_Name2", objItemMaster.Short_Name2);
        objParam[11] = new SqlParameter("@Sales_In_Unit", objItemMaster.Sales_In_Unit);
        objParam[12] = new SqlParameter("@Qty_To_Less", objItemMaster.Qty_To_Less);
        objParam[13] = new SqlParameter("@Purchase_Rate", objItemMaster.Purchase_Rate);
        objParam[14] = new SqlParameter("@Sale_Rate", objItemMaster.Sale_Rate);
        objParam[15] = new SqlParameter("@Max_Retail_Price", objItemMaster.Max_Retail_Price);
        objParam[16] = new SqlParameter("@Mark_Up", objItemMaster.Mark_Up);
        objParam[17] = new SqlParameter("@Qty_in_Case", objItemMaster.Qty_in_Case);
        objParam[18] = new SqlParameter("@Max_Level", objItemMaster.Max_Level);
        objParam[19] = new SqlParameter("@Min_Level", objItemMaster.Min_Level);
        objParam[20] = new SqlParameter("@Re_Order_Qty", objItemMaster.Re_Order_Qty);
        objParam[21] = new SqlParameter("@Cursor_On", objItemMaster.Cursor_On);
        objParam[22] = new SqlParameter("@Tax_Code", objItemMaster.Tax_Code);
        objParam[23] = new SqlParameter("@Pur_Code", objItemMaster.Pur_Code);
        objParam[24] = new SqlParameter("@Sale_Code", objItemMaster.Sale_Code);
        objParam[25] = new SqlParameter("@IsSaleable", objItemMaster.IsSaleable);
        objParam[26] = new SqlParameter("@Delear_Margin", objItemMaster.Delear_Margin);
        objParam[27] = new SqlParameter("@Tax_AfterBefore", objItemMaster.Tax_AfterBefore);
        objParam[28] = new SqlParameter("@Discount", objItemMaster.Discount);
        objParam[29] = new SqlParameter("@Location", objItemMaster.Location);
        objParam[30] = new SqlParameter("@Item_Type", objItemMaster.Item_Type);
        objParam[31] = new SqlParameter("@Department", objItemMaster.Department);
        objParam[32] = new SqlParameter("@Sale_Rate2", objItemMaster.Sale_Rate2);
        objParam[33] = new SqlParameter("@Sale_Rate3", objItemMaster.Sale_Rate3);
        objParam[34] = new SqlParameter("@Sale_Rate4", objItemMaster.Sale_Rate4);
        objParam[35] = new SqlParameter("@Sale_Rate_Excl", objItemMaster.Sale_Rate_Excl);
        objParam[36] = new SqlParameter("@Whole_Sale_Rate", objItemMaster.Whole_Sale_Rate);
        objParam[37] = new SqlParameter("@Delivery_No", objItemMaster.Delivery_No);
        objParam[38] = new SqlParameter("@SGroup_id", objItemMaster.SGroup_id);
        objParam[39] = new SqlParameter("@COLOR_ID", objItemMaster.COLOR_ID);
        objParam[40] = new SqlParameter("@TAGGED", objItemMaster.TAGGED);
        objParam[41] = new SqlParameter("@Franchise_SaleRate", objItemMaster.Franchise_SaleRate);
        objParam[42] = new SqlParameter("@Discontinued", objItemMaster.Discontinued);
        objParam[43] = new SqlParameter("@Transaction_Mode", objItemMaster.Transaction_Mode);
        objParam[44] = new SqlParameter("@InHouse_Packing", objItemMaster.InHouse_Packing);
        objParam[45] = new SqlParameter("@InHouse_PackedQty", objItemMaster.InHouse_PackedQty);
        objParam[46] = new SqlParameter("@Dis1InRs", objItemMaster.Dis1InRs);
        objParam[47] = new SqlParameter("@Dis2InRs", objItemMaster.Dis2InRs);
        objParam[48] = new SqlParameter("@Dis1Value", objItemMaster.Dis1Value);
        objParam[49] = new SqlParameter("@Dis2Value", objItemMaster.Dis2Value);
        objParam[50] = new SqlParameter("@IsCompBarCode", objItemMaster.IsCompBarCode);
        objParam[51] = new SqlParameter("@Remarks", objItemMaster.Remarks);
        objParam[52] = new SqlParameter("@AllowPoint", objItemMaster.AllowPoint);
        objParam[53] = new SqlParameter("@Tax_ID", objItemMaster.Tax_ID);
        objParam[54] = new SqlParameter("@CpPrinting", objItemMaster.CpPrinting);
        objParam[55] = new SqlParameter("@Excise_ID", objItemMaster.Excise_ID);
        objParam[56] = new SqlParameter("@Excise_Code", objItemMaster.Excise_Code);
        objParam[57] = new SqlParameter("@ImageUrl", objItemMaster.ImageUrl);
        objParam[58] = new SqlParameter("@SubGroupId", objItemMaster.SubGroupId);
        objParam[59] = new SqlParameter("@Likes", objItemMaster.Likes);
        objParam[60] = new SqlParameter("@UserId", objItemMaster.UserId);
        objParam[61] = new SqlParameter("@Day", objItemMaster.Day);
        objParam[62] = new SqlParameter("@BestExp", objItemMaster.BestExp);
        objParam[63] = new SqlParameter("@VEG_NonVeg", objItemMaster.VEG_NonVeg);
        objParam[64] = new SqlParameter("@dt", dt);
        objParam[65] = new SqlParameter("@IsSubItem", objItemMaster.IsSubItem);
        objParam[66] = new SqlParameter("@ParentItemCode", objItemMaster.ParentItemCode);
        objParam[67] = new SqlParameter("@Sales_In_Unit2", objItemMaster.Sales_In_Unit2);
        objParam[68] = new SqlParameter("@DeliveryNoteRate", objItemMaster.DeliveryNoteRate);
        objParam[69] = new SqlParameter("@BranchId", objItemMaster.BranchId);
        objParam[70] = new SqlParameter("@Edit_SaleRate", objItemMaster.Edit_SaleRate);
        objParam[71] = new SqlParameter("@HSNCode", objItemMaster.HSNCode);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ProductsInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            
            
            objItemMaster.ItemID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetProductById(ItemMaster objItemMaster)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ItemID", objItemMaster.ItemID);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ProductsGetByID", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public int Delete(ItemMaster objItemMaster)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemID", objItemMaster.ItemID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ItemDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objItemMaster.ItemID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }

}