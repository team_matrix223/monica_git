﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for CitiesDAL
/// </summary>
public class CitiesDAL:Connection
{
    public SqlDataReader GetById(Cities objCities)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@City_ID", objCities.City_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CityGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllCities", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(Cities objCity)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@City_ID", objCity.City_ID);
        objParam[1] = new SqlParameter("@City_Name", objCity.City_Name);
      
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objCity.UserId);
        objParam[4] = new SqlParameter("@IsActive", objCity.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CityInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objCity.City_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Cities objCities)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@City_ID", objCities.City_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CityDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objCities.City_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}