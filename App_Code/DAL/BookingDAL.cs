﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BookingDAL
/// </summary>
public class BookingDAL:Connection
{
    public Int32 InsertUpdate(Booking objBooking, DataTable dt)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[34];
        ObjParam[0] = new SqlParameter("@OrderDate", objBooking.OrderDate);

        ObjParam[1] = new SqlParameter("@Customer_ID", objBooking.Customer_ID);
        ObjParam[2] = new SqlParameter("@CustomerName", objBooking.CustomerName);
        ObjParam[3] = new SqlParameter("@Address", objBooking.Address);
        ObjParam[4] = new SqlParameter("@DeliveryType", objBooking.DeliveryType);
        ObjParam[5] = new SqlParameter("@DeliveryTime", objBooking.DeliveryTime);

        ObjParam[6] = new SqlParameter("@DeliveryAddress", objBooking.DeliveryAddress);
        ObjParam[7] = new SqlParameter("@Advance", objBooking.Advance);

        ObjParam[8] = new SqlParameter("@AdvMode", objBooking.PaymentMode);
        ObjParam[9] = new SqlParameter("@LeftPayRecd", objBooking.LeftPayRecd);
        ObjParam[10] = new SqlParameter("@Remarks", objBooking.Remarks);
        ObjParam[11] = new SqlParameter("@UserNo", objBooking.UserNo);
        ObjParam[12] = new SqlParameter("@Passing", objBooking.Passing);
        ObjParam[13] = new SqlParameter("@DateBalRecd", objBooking.DateBalRecd);

        ObjParam[14] = new SqlParameter("@Ecode", objBooking.Ecode);
        ObjParam[15] = new SqlParameter("@DisAmt", objBooking.DisAmt);
        ObjParam[16] = new SqlParameter("@Picture", objBooking.Picture);
        ObjParam[17] = new SqlParameter("@VatAmount", objBooking.VatAmount);
        ObjParam[18] = new SqlParameter("@NetAmount", objBooking.NetAmount);
        ObjParam[19] = new SqlParameter("@OrderTime", objBooking.OrderTime);

        ObjParam[20] = new SqlParameter("@IndRemarks", objBooking.IndRemarks);
        ObjParam[21] = new SqlParameter("@Prefix", objBooking.Prefix);
        ObjParam[22] = new SqlParameter("@ReadTag", objBooking.ReadTag);
        ObjParam[23] = new SqlParameter("@FDispatch", objBooking.FDispatch);
        ObjParam[24] = new SqlParameter("@EmployeeId", objBooking.Employee);
        ObjParam[25] = new SqlParameter("@DiscountPer", objBooking.DisPer);
        
        ObjParam[26] = new SqlParameter("@OrderNo", objBooking.OrderNo);
        ObjParam[27] = new SqlParameter("@OrderBookingDetail", dt);
     
        ObjParam[28] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[28].Direction = ParameterDirection.ReturnValue;
        ObjParam[29] = new SqlParameter("@BranchId", objBooking.BranchId);
        ObjParam[30] = new SqlParameter("@DelTime", objBooking.DelTime);
        ObjParam[31] = new SqlParameter("@CreditCardNumber", objBooking.CreditCardNumber);

        ObjParam[32] = new SqlParameter("@SerialNo", objBooking.SerialNo);
        ObjParam[33] = new SqlParameter("@ManualOrderNo", objBooking.ManualOrderNo);

        try
        {

            objBooking.OrderNo = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "POS_sp_InsertOrderbooking", ObjParam));
           
            retval = Convert.ToInt32(ObjParam[28].Value);
            objBooking.OrderNo = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        ObjParam[2] = new SqlParameter("@BranchId", BranchId);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_OrderBookingsGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetByOrderNo(Int32 OrderNo,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@OrderNo", OrderNo);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
      
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_GetBookingByOrderNo", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetDispatchDetailByOrderNo(Int32 OrderNo,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@OrderNo", OrderNo);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "pos_sp_OrderDispatchDteailByOrderNo", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }




    public Int32 InsertUpdateDispatch(OrderDispatch objOrderDispatch, DataTable dt,DataTable dt1)
    {


        Int32 retval = 0;

        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[25];
        ObjParam[0] = new SqlParameter("@OrderNo", objOrderDispatch.OrderNo);

        ObjParam[1] = new SqlParameter("@CustCode", objOrderDispatch.CustCode);
        ObjParam[2] = new SqlParameter("@DSpValue", objOrderDispatch.DSpValue);
        ObjParam[3] = new SqlParameter("@Discount", objOrderDispatch.Discount);
        ObjParam[4] = new SqlParameter("@TaxAmount", objOrderDispatch.TaxAmount);
        ObjParam[5] = new SqlParameter("@NetAmount", objOrderDispatch.NetAmount);
        ObjParam[6] = new SqlParameter("@PayMOde", objOrderDispatch.PayMOde);
        ObjParam[7] = new SqlParameter("@CrPay", objOrderDispatch.CrPay);

        ObjParam[8] = new SqlParameter("@CrLeftPay", objOrderDispatch.CrLeftPay);
        ObjParam[9] = new SqlParameter("@TaxType", objOrderDispatch.TaxType);
        ObjParam[10] = new SqlParameter("@UserId", objOrderDispatch.UserId);
        ObjParam[11] = new SqlParameter("@PcName", objOrderDispatch.PcName);
     
        ObjParam[13] = new SqlParameter("@Payment", objOrderDispatch.Payment);
        ObjParam[14] = new SqlParameter("@Disper", objOrderDispatch.Disper);
       
        ObjParam[15] = new SqlParameter("@OrderDispatchDetail", dt);
        ObjParam[17] = new SqlParameter("@PurchaseTax", dt1);
        ObjParam[16] = new SqlParameter("@retval", SqlDbType.Int, 4);
        ObjParam[16].Direction = ParameterDirection.ReturnValue;
        ObjParam[18] = new SqlParameter("@BranchId", objOrderDispatch.BranchId);
        ObjParam[19] = new SqlParameter("@CCODE", objOrderDispatch.CCODE);
        ObjParam[20] = new SqlParameter("@CNAME", objOrderDispatch.CNAME);
        ObjParam[21] = new SqlParameter("@CrdAmount", objOrderDispatch.CrdAmount);
        ObjParam[22] = new SqlParameter("@CardNumber", objOrderDispatch.CardNumber);
        ObjParam[23] = new SqlParameter("@CardType", objOrderDispatch.CardType);
        ObjParam[24] = new SqlParameter("@Bank", objOrderDispatch.Bank);

        try
        {

            objOrderDispatch.OrderNo = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "pos_sp_InsertUpdateOrderDispatch", ObjParam));

            retval = Convert.ToInt32(ObjParam[16].Value);
           //objOrderDispatch.OrderNo = retval;
        }
        finally
        {
            ObjParam = null;

        }
        return retval;
    }

    public Int32 UpdateAdvance(int Orderno, decimal RecAmt, string Paymode, int Userno, int BranchId)
    {

        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[5];
        ObjParam[0] = new SqlParameter("@Orderno", Orderno);
        ObjParam[1] = new SqlParameter("@RecAmt", RecAmt);
        ObjParam[2] = new SqlParameter("@PayMode", Paymode);
        ObjParam[3] = new SqlParameter("@UserNo", Userno);
        ObjParam[4] = new SqlParameter("@BranchId", BranchId);
        try
        {

            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "pos_sp_ReceiveAdvanceOrder", ObjParam));
        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }


    public DataSet GetAllReceiptEntry(int orderno, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        DataSet ds = new DataSet();
        objParam[0] = new SqlParameter("@orderno", orderno);
        objParam[1] = new SqlParameter("@BranchId", BranchId);




        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "GetAllReceiptEntry", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public Int32 DeleteAdvance(int Orderno, int BranchId)
    {

        Int32 retval = 0;
        System.Data.SqlClient.SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@Id", Orderno);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
        try
        {
            retval = Convert.ToInt32(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure, "pos_sp_DeleteAdvanceOrderPayment", ObjParam));
        }
        finally
        {
            ObjParam = null;
        }
        return retval;
    }


}