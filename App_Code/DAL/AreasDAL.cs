﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AreasDAL
/// </summary>
public class AreasDAL:Connection
{

    public SqlDataReader GetById(Areas objArea)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Area_ID", objArea.Area_ID);
  
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AreaGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<Areas> AreaList = new List<Areas>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AreaGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Areas objArea)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@Area_ID", objArea.Area_ID);
        objParam[1] = new SqlParameter("@Area_Name", objArea.Area_Name);
      
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objArea.UserId);
        objParam[4] = new SqlParameter("@IsActive", objArea.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AreaInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objArea.Area_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Areas objArea)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Area_ID", objArea.Area_ID);
        
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_AreaDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objArea.Area_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}