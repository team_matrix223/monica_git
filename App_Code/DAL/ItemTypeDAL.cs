﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ItemTypeDAL
/// </summary>
public class ItemTypeDAL:Connection
{
    public SqlDataReader GetById(ItemTypes objItemType)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ItemType_Id", objItemType.ItemType_Id);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ItemTypeGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<ItemTypes> ItemTypesList = new List<ItemTypes>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ItemTypesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(ItemTypes objItemTypes)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@ItemType_Id", objItemTypes.ItemType_Id);
        objParam[1] = new SqlParameter("@ItemType_Name", objItemTypes.ItemType_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objItemTypes.IsActive);
        objParam[4] = new SqlParameter("@UserId", objItemTypes.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ItemTypeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objItemTypes.ItemType_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(ItemTypes objItemType)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemType_Id", objItemType.ItemType_Id);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ItemTypeDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objItemType.ItemType_Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}