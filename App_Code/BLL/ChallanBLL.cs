﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ChallanBLL
/// </summary>
public class ChallanBLL
{
    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new BillDAL().Insert(objBill, dt, dt1);
    }
    public List<ChallanMaster> GetBillByDate(DateTime FromDate, DateTime ToDate, int BranchId)
    {
        List<ChallanMaster> BillList = new List<ChallanMaster>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new ChallanDAL().GetByDate(FromDate, ToDate, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ChallanMaster objBill = new ChallanMaster()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString()),
                        Bill_No = Convert.ToInt32(dr["Bill_No"].ToString()),
                        Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]),

                        Customer_ID = Convert.ToString(dr["Customer_ID"]),
                        Customer_Name = Convert.ToString(dr["Customer_Name"].ToString()),
                        Bill_Value = Convert.ToDecimal(dr["Bill_Value"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]),
                        Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CreditBank = Convert.ToString(dr["CreditBank"]),
                        UserNO = Convert.ToInt32(dr["UserNO"]),
                        Bill_Type = Convert.ToInt32(dr["Bill_Type"]),
                        Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]),
                        Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]),
                        CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]),
                        CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]),
                        CashCust_Name = Convert.ToString(dr["CashCust_Name"]),
                        Round_Amount = Convert.ToDecimal(dr["Round_Amount"]),
                        Passing = Convert.ToBoolean(dr["Passing"]),
                        Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]),

                        Tax_Per = Convert.ToDecimal(dr["Tax_Per"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]),
                        R_amount = Convert.ToDecimal(dr["R_amount"]),
                        tokenno = Convert.ToInt32(dr["tokenno"]),
                        tableno = Convert.ToInt32(dr["tableno"]),
                        remarks = Convert.ToString(dr["remarks"]),
                        servalue = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]),
                        DiscountPer = Convert.ToDecimal(dr["DisPer"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        CashCustAddress = dr["CashCustAddress"].ToString(),
                        CreditCustAddress = dr["CreditCustAddress"].ToString()

                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }
    public Int32 DeleteBill(Bill objBill)
    {
        return new ChallanDAL().Delete(objBill);
    }


    public List<Product> GetByBillNowPrefix(string BillNowPrefix, int BranchId)
    {
        List<Product> lstProduct = new List<Product>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ChallanDAL().GetBillDetailByBillNo(BillNowPrefix, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Product objProduct = new Product()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        Tax_Code = Convert.ToDecimal(dr["Tax_Code"]),
                        Sale_Rate = Convert.ToDecimal(dr["Rate"]),
                        SurVal = Convert.ToDecimal(dr["SurVal"]),
                        Item_Remarks = dr["Item_Remarks"].ToString(),
                        Discount = Convert.ToDecimal(dr["ItemDiscount"].ToString()),
                        Qty = Convert.ToInt16(dr["Qty"])
                    };
                    lstProduct.Add(objProduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return lstProduct;

    }
    public Int32 Insert(ChallanMaster objBill, DataTable dt, DataTable dt1)
    {
        return new ChallanDAL().Insert(objBill, dt, dt1);
    }


    public Int32 Update(ChallanMaster objBill, DataTable dt, DataTable dt1)
    {
        return new ChallanDAL().Update(objBill, dt, dt1);
    }

    public List<ChallanMaster> GetChallanBYCustomer(DateTime FromDate, DateTime ToDate, string CustomerId, int BranchId)
    {
        List<ChallanMaster> BillList = new List<ChallanMaster>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new ChallanDAL().GetChallanBYCustomer(FromDate, ToDate, CustomerId, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ChallanMaster objBill = new ChallanMaster()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        Bill_No = Convert.ToInt32(dr["Bill_No"]),
                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"]),

                    };


                    BillList.Add(objBill);
                }
              

            }
           
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public List<ChallanDetail> GetChallanDetail(DataTable dt, int BranchId, out decimal TotablBillValue, out decimal TotalDiscount, out decimal TOtalTax, out decimal TotalNetAmount)
    {
        TotablBillValue = 0;
        TotalDiscount = 0;
        TOtalTax = 0;
        TotalNetAmount = 0;
        List<ChallanDetail> BillList = new List<ChallanDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new ChallanDAL().GetChallanDetail(dt, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ChallanDetail objBill = new ChallanDetail()
                    {

                       // BillNoWPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),
                        //Bill_Date = Convert.ToDateTime(dr["Bill_Date"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                       // Bill_No = Convert.ToInt32(dr["Bill_No"]),
                        Item_Code = Convert.ToString(dr["Item_Code"].ToString()),
                        Item_Name = Convert.ToString(dr["Item_Name"].ToString()),
                        MRP = Convert.ToDecimal(dr["MRP"].ToString()),
                        Rate = Convert.ToDecimal(dr["Rate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"].ToString()),
                        Dis_Per = Convert.ToDecimal(dr["Dis_Per"].ToString()),
                        Dis_Amt = Convert.ToDecimal(dr["Dis_Amt"].ToString()),
                        Tax = Convert.ToDecimal(dr["Tax"].ToString()),
                        SurPer = Convert.ToDecimal(dr["SurPer"].ToString()),
                        TaxId = Convert.ToDecimal(dr["Tax_Id"].ToString()),
                        BIllBasicType = Convert.ToString(dr["BIllBasicType"].ToString()),
                       

                    };


                    BillList.Add(objBill);
                }
            }


            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    TotablBillValue = Convert.ToDecimal(dr["TOtalBillValue"].ToString());
                    TotalDiscount = Convert.ToDecimal(dr["TotalDiscount"]);
                    TOtalTax = Convert.ToDecimal(dr["TotalTax"]);
                    TotalNetAmount = Convert.ToDecimal(dr["TotalNetAmount"]);
                }

                   


                 


            }

        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }



    public List<ChallanDetail> GetChallanDetailVat(DataTable dt, int BranchId, out decimal TotablBillValue, out decimal TotalDiscount, out decimal TOtalTax, out decimal TotalNetAmount)
    {
        TotablBillValue = 0;
        TotalDiscount = 0;
        TOtalTax = 0;
        TotalNetAmount = 0;
        List<ChallanDetail> BillList = new List<ChallanDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new ChallanDAL().GetChallanDetailVAT(dt, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ChallanDetail objBill = new ChallanDetail()
                    {

                        //BillNoWPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),
                       // Bill_Date = Convert.ToDateTime(dr["Bill_Date"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                       // Bill_No = Convert.ToInt32(dr["Bill_No"]),
                        Item_Code = Convert.ToString(dr["Item_Code"].ToString()),
                        Item_Name = Convert.ToString(dr["Item_Name"].ToString()),
                        MRP = Convert.ToDecimal(dr["MRP"].ToString()),
                        Rate = Convert.ToDecimal(dr["Rate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"].ToString()),
                        Dis_Per = Convert.ToDecimal(dr["Dis_Per"].ToString()),
                        Dis_Amt = Convert.ToDecimal(dr["Dis_Amt"].ToString()),
                        Tax = Convert.ToDecimal(dr["Tax"].ToString()),
                        SurPer = Convert.ToDecimal(dr["SurPer"].ToString()),
                        TaxId = Convert.ToDecimal(dr["Tax_Id"].ToString()),
                        BIllBasicType = Convert.ToString(dr["BIllBasicType"].ToString()),


                    };


                    BillList.Add(objBill);
                }
            }


            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    TotablBillValue = Convert.ToDecimal(dr["TOtalBillValue"].ToString());
                    TotalDiscount = Convert.ToDecimal(dr["TotalDiscount"]);
                    TOtalTax = Convert.ToDecimal(dr["TotalTax"]);
                    TotalNetAmount = Convert.ToDecimal(dr["TotalNetAmount"]);
                }







            }

        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public Int32 ConvertChallanToBill(DataTable dt, int BranchId, decimal BillValue, decimal Tax, decimal Discount, decimal NetAmount, int UserNO, decimal Round_Amount, int Customer_ID, string CustomerName, DataTable dt1, ChallanMaster objChallan,DateTime BIllDate)
    {
        return new ChallanDAL().ConvertChallanToBill(dt, BranchId, BillValue, Tax, Discount, NetAmount, UserNO, Round_Amount, Customer_ID, CustomerName, dt1, objChallan, BIllDate);
    }
    public Int32 ConvertChallanToBillvat(DataTable dt, int BranchId, decimal BillValue, decimal Tax, decimal Discount, decimal NetAmount, int UserNO, decimal Round_Amount, int Customer_ID, string CustomerName, DataTable dt1, ChallanMaster objChallan, DateTime BIllDate)
    {
        return new ChallanDAL().ConvertChallanToBillVat(dt, BranchId, BillValue, Tax, Discount, NetAmount, UserNO, Round_Amount, Customer_ID, CustomerName, dt1, objChallan, BIllDate);
    }

    public List<Bill> GetBillByDate(DateTime FromDate, DateTime ToDate, int BranchId,string Type)
    {
        List<Bill> BillList = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new ChallanDAL().GetByDate(FromDate, ToDate, BranchId,Type);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString()),
                        Bill_No = Convert.ToInt32(dr["Bill_No"].ToString()),
                        Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]),

                        Customer_ID = Convert.ToString(dr["Customer_ID"]),
                        Customer_Name = Convert.ToString(dr["Customer_Name"].ToString()),
                        Bill_Value = Convert.ToDecimal(dr["Bill_Value"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]),
                        Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CreditBank = Convert.ToString(dr["CreditBank"]),
                        UserNO = Convert.ToInt32(dr["UserNO"]),
                        Bill_Type = Convert.ToInt32(dr["Bill_Type"]),
                        Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]),
                        Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]),
                        CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]),
                        CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]),
                        CashCust_Name = Convert.ToString(dr["CashCust_Name"]),
                        Round_Amount = Convert.ToDecimal(dr["Round_Amount"]),
                        Passing = Convert.ToBoolean(dr["Passing"]),
                        Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]),

                        Tax_Per = Convert.ToDecimal(dr["Tax_Per"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]),
                        R_amount = Convert.ToDecimal(dr["R_amount"]),
                        tokenno = Convert.ToInt32(dr["tokenno"]),
                        tableno = Convert.ToInt32(dr["tableno"]),
                        remarks = Convert.ToString(dr["remarks"]),
                        servalue = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]),
                        DiscountPer = Convert.ToDecimal(dr["DisPer"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        CashCustAddress = dr["CashCustAddress"].ToString(),
                        CreditCustAddress = dr["CreditCustAddress"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),

                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

}