﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for GroupBLL
/// </summary>
public class GroupBLL
{
    public List<Group> GetAll()
    {
        List<Group> GroupList = new List<Group>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new GroupDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Group objGroups = new Group()
                    {
                        GroupId = Convert.ToInt16(dr["GroupId"]),
                        Title = dr["Title"].ToString(),
                        ImageUrl = dr["ImageUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                        SerTax = Convert.ToDecimal(dr["SerTax"].ToString()),

                    };
                    GroupList.Add(objGroups);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return GroupList;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new GroupDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["GroupID"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


}