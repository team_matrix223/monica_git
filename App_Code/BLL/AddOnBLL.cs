﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
public class AddOnBLL
{
    public List<AddOn> GetAll()
    {
        List<AddOn> stateList = new List<AddOn>();
     
        SqlDataReader dr = null;
        try
        {
            dr = new AddOnDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AddOn objAddOn = new AddOn()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        AddOnId = Convert.ToInt16(dr["AddOnId"]),

                    };
                    stateList.Add(objAddOn);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            
        }
        return stateList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AddOnDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["AddOnId"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString() ;

    }



     public Int16 InsertUpdate(AddOn objAddOn,Int32 Id)
    {

        return new AddOnDAL().InsertUpdate(objAddOn,Id);
    }

     
}