﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BillBLL
/// </summary>
public class BillBLL
{

    public Int32 DeleteBill(Bill objBill)
    {
        return new BillDAL().Delete(objBill);
    }

    public List<Bill> GetBillByDate(DateTime FromDate, DateTime ToDate,int BranchId)
    {
        List<Bill> BillList = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetByDate(FromDate, ToDate,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString()),
                        Bill_No = Convert.ToInt32(dr["Bill_No"].ToString()),
                        Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]),
                        BillTime = dr["BillTime"].ToString(),
                        Customer_ID = Convert.ToString(dr["Customer_ID"]),
                        Customer_Name = Convert.ToString(dr["Customer_Name"].ToString()),
                        Bill_Value = Convert.ToDecimal(dr["Bill_Value"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]),
                        Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CreditBank = Convert.ToString(dr["CreditBank"]),
                        UserNO = Convert.ToInt32(dr["UserNO"]),
                        Bill_Type = Convert.ToInt32(dr["Bill_Type"]),
                        Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]),
                        Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]),
                        CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]),
                        CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]),
                        CashCust_Name = Convert.ToString(dr["CashCust_Name"]),
                        Round_Amount = Convert.ToDecimal(dr["Round_Amount"]),
                        Passing = Convert.ToBoolean(dr["Passing"]),
                        Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]),

                        Tax_Per = Convert.ToDecimal(dr["Tax_Per"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]),
                        R_amount = Convert.ToDecimal(dr["R_amount"]),
                        tokenno = Convert.ToInt32(dr["tokenno"]),
                        tableno = Convert.ToInt32(dr["tableno"]),
                        remarks = Convert.ToString(dr["remarks"]),
                        servalue = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]),
                        DiscountPer = Convert.ToDecimal(dr["DisPer"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        CashCustAddress=dr["CashCustAddress"].ToString(),
                        CreditCustAddress=dr["CreditCustAddress"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
						OrderNo = Convert.ToInt32(dr["OrderNo"]),
						manualorderno = Convert.ToInt32(dr["manualorderno"]),

					};


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public List<BillDetail> GetOrdersBillItemByDate(DateTime FromDate, DateTime ToDate, string ItemType, int BranchId)
    {
        List<BillDetail> BillList = new List<BillDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetOrdersBillItemByDate(FromDate, ToDate, ItemType, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    BillDetail objBill = new BillDetail()
                    {

                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        Amount = Convert.ToDecimal(dr["Net_Amount"]),


                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }



    public List<Bill> GetVatBillByDate(DateTime FromDate, DateTime ToDate, int BranchId)
    {
        List<Bill> BillList = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetByDateVatBills(FromDate, ToDate, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString()),
                        Bill_No = Convert.ToInt32(dr["Bill_No"].ToString()),
                        Bill_Prefix = Convert.ToString(dr["Bill_Prefix"]),

                        Customer_ID = Convert.ToString(dr["Customer_ID"]),
                        Customer_Name = Convert.ToString(dr["Customer_Name"].ToString()),
                        Bill_Value = Convert.ToDecimal(dr["Bill_Value"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Less_Dis_Amount = Convert.ToDecimal(dr["Less_Dis_Amount"]),
                        Add_Tax_Amount = Convert.ToDecimal(dr["Add_Tax_Amount"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CreditBank = Convert.ToString(dr["CreditBank"]),
                        UserNO = Convert.ToInt32(dr["UserNO"]),
                        Bill_Type = Convert.ToInt32(dr["Bill_Type"]),
                        Cash_Amount = Convert.ToDecimal(dr["Cash_Amount"]),
                        Credit_Amount = Convert.ToDecimal(dr["Credit_Amount"]),
                        CrCard_Amount = Convert.ToDecimal(dr["CrCard_Amount"]),
                        CashCust_Code = Convert.ToInt32(dr["CashCust_Code"]),
                        CashCust_Name = Convert.ToString(dr["CashCust_Name"]),
                        Round_Amount = Convert.ToDecimal(dr["Round_Amount"]),
                        Passing = Convert.ToBoolean(dr["Passing"]),
                        Bill_Printed = Convert.ToBoolean(dr["Bill_Printed"]),

                        Tax_Per = Convert.ToDecimal(dr["Tax_Per"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]),
                        R_amount = Convert.ToDecimal(dr["R_amount"]),
                        tokenno = Convert.ToInt32(dr["tokenno"]),
                        tableno = Convert.ToInt32(dr["tableno"]),
                        remarks = Convert.ToString(dr["remarks"]),
                        servalue = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ReceiviedGRNNo = Convert.ToInt32(dr["ReceiviedGRNNo"]),
                        DiscountPer = Convert.ToDecimal(dr["DisPer"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        CashCustAddress = dr["CashCustAddress"].ToString(),
                        CreditCustAddress = dr["CreditCustAddress"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
                        LocalOut = dr["LocalOut"].ToString(),
						TransportMode = dr["TransportMode"].ToString(),
                        VehicleNo = dr["VehicleNo"].ToString(),
                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new BillDAL().Insert(objBill, dt, dt1);
    }

    public Int32 InsertVatBill(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new BillDAL().InsertVatBill(objBill, dt, dt1);
    }


    public Int32 Update(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new BillDAL().Update(objBill, dt, dt1);
    }


    public Int32 UpdateVatBill(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new BillDAL().UpdateVatBill(objBill, dt, dt1);
    }

    public Int32 InsertHoldBill(holdbill objHoldBill, DataTable dt)
    {
        return new BillDAL().InsertHoldBill(objHoldBill, dt);
    }


    public string HoldBillGetAllHtml(int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new BillDAL().HoldBillGetAll(BranchId);
            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<tr ><td style='width:50px;padding-bottom:4px'>{0}</td><td style='width:100px;height:30px'>{1}</td><td style='width:120px'>{2}</td><td style='width:70px'>{3}</td><td style='width:30px'><div id='dvUnHold' class='btn btn-primary btn-small' onclick='javascript:UnHoldBill({0})'>UnHold</div></td></tr>", dr["Hold_No"].ToString(), Convert.ToDateTime(dr["Bill_Date"]).ToShortDateString(), dr["Customer_Name"].ToString(), dr["Bill_Value"].ToString()));
                    strBuilder.Append("<tr><td colspan='100%' ><hr style='border-bottom: dashed 1px silver;margin:1px'/></td></tr>");

                }
            }
            strBuilder.Append("<tr><td   colspan='100%'><div style='width:100%;text-align:center;height:26px;margin-bottom:5px' class='btn btn-danger btn-small' onclick='javascript:BackToList();'>BACK TO BILLING WINDOW</div></td></tr>");

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }





    public string OrdersForBillingGetAllHtml()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new BillDAL().OrdersGetAll();
            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<tr ><td style='width:80px;padding-bottom:4px'>{0}</td><td style='width:80px;height:30px'>{1}</td><td style='width:100px'>{2}</td><td style='width:50px'>{3}</td><td style='width:130px'><div id='dvGenerateBill' class='btn btn-primary btn-small' onclick='javascript:GenerateBill(\"{0}\")'>Generate Bill</div></td></tr>", dr["Order_NO"].ToString(), Convert.ToDateTime(dr["Order_Date"]).ToShortDateString(), dr["Customer_Name"].ToString(), dr["TableNo"].ToString()));
                    strBuilder.Append("<tr><td colspan='100%' ><hr style='border-bottom: dashed 1px silver;margin:1px'/></td></tr>");

                }
            }
            //strBuilder.Append("<tr><td   colspan='100%'><div style='width:100%;text-align:center;height:26px;margin-bottom:5px' class='btn btn-danger btn-small'>BACK TO ORDERS LIST</div></td></tr>");

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public List<Product> GetByHoldNo(int HoldNo,int BranchId)
    {
        List<Product> lstProduct = new List<Product>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BillDAL().GetByHoldNo(HoldNo,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Product objProduct = new Product()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        Tax_ID =Convert.ToDecimal(dr["Tax_ID"]),
                        Tax_Code =Convert.ToDecimal(dr["Tax_Code"]),
                        Sale_Rate =Convert.ToDecimal(dr["Rate"]),
                        SurVal =Convert.ToDecimal(dr["SurVal"]),
                        Item_Remarks = dr["Item_Remarks"].ToString(),
                        Discount = Convert.ToDecimal(dr["ItemDiscount"].ToString()) ,
                        Qty = Convert.ToInt16(dr["Qty"])
                    };
                    lstProduct.Add(objProduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return lstProduct;

    }

    public List<Product> GetByOrderNo(Int32 OrderNo)
    {
        List<Product> lstProduct = new List<Product>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BillDAL().GetByOrderNo(OrderNo);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Product objProduct = new Product()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        Tax_Code = Convert.ToDecimal(dr["Tax_Code"]),
                        Sale_Rate = Convert.ToDecimal(dr["Rate"]),
                        SurVal = Convert.ToDecimal(dr["SurVal"]),
                        Item_Remarks = dr["Remarks"].ToString(),
                    };
                    lstProduct.Add(objProduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return lstProduct;

    }



    public List<Product> GetByBillNowPrefix(string BillNowPrefix,int BranchId)
    {
        List<Product> lstProduct = new List<Product>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new BillDAL().GetBillDetailByBillNo(BillNowPrefix, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Product objProduct = new Product()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        Tax_Code = Convert.ToDecimal(dr["Tax_Code"]),
                        Sale_Rate = Convert.ToDecimal(dr["Rate"]),
                        SurVal = Convert.ToDecimal(dr["SurVal"]),
                        Item_Remarks = dr["Item_Remarks"].ToString(),
                        Discount = Convert.ToDecimal(dr["ItemDiscount"].ToString()) ,
                        Qty = Convert.ToInt16(dr["Qty"]) ,
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"].ToString()),
                        ChallanNo = dr["challanno"].ToString(),
                    };
                    lstProduct.Add(objProduct);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return lstProduct;

    }



    public List<Bill> GetOrderBillsByDate(DateTime FromDate, DateTime ToDate,string ItemType, int BranchId)
    {
        List<Bill> BillList = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetOrdersBillByDate(FromDate, ToDate,ItemType, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                      
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        Bill_No = Convert.ToInt32(dr["Bill_No"]),

                    };


                    BillList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }

    public List<Bill> GetOptions(string ProcessType)
    {
        List<Bill> BillOptions = new List<Bill>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillDAL().GetOptions(ProcessType);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bill objBill = new Bill()
                    {
                        BillType = Convert.ToString(dr["BillType"].ToString()),

                    };


                    BillOptions.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillOptions;

    }

}