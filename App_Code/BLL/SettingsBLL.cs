﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SettingsBLL
/// </summary>
public class SettingsBLL
{
    public string InsertUpdate(Settings objSettings)
    {

        return new SettingsDAL().InsertUpdate(objSettings);
    }

    public void GetDiscountType(Settings objSetting)
    {

        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        
         
        try
        {
            dr = new SettingsDAL().GetDiscountType();
            if (dr.HasRows)
            {
                dr.Read();

                objSetting.DiscountType = dr["DiscountType"].ToString();

            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }


    }

}