﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for PurchaseReturnBLL
/// </summary>
public class PurchaseReturnBLL
{
	


    public List<PurchaseReturn> GetByDate(DateTime DateFrom, DateTime DateTo,Int32 BranchId)
    {
        List<PurchaseReturn> PurchaseList = new List<PurchaseReturn>();
        SqlDataReader dr = new PurchaseReturnDAL().GetByDate(DateFrom,DateTo,BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    PurchaseReturn objPurchase = new PurchaseReturn()
                    {
                        GrnNo = Convert.ToInt32(dr["Grn_No"]),
                        GRNDate = Convert.ToDateTime(dr["GRN_Date"]),
                        BillNo = Convert.ToString(dr["Bill_No"]),
                        BillDate = Convert.ToDateTime(dr["Bill_Date"]),
                        GRNo = Convert.ToString(dr["GR_No"]),
                        GRDate = Convert.ToDateTime(dr["GR_Date"]),
                        VehNo = Convert.ToString(dr["Veh_No"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        IsLocal = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        BillValue = Convert.ToDecimal(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        TotalAmount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]),
                        NetAmount = Convert.ToDecimal(dr["Net_Amount"]),
                        Pass = Convert.ToBoolean(dr["Pass"]),
                        SupplierId = Convert.ToInt32(dr["Supplier_Id"]),
                        GodownId = Convert.ToInt32(dr["Godown_Id"]),
                        ExciseAmt = Convert.ToDecimal(dr["Excise_Amt"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"]),

                        Expiry = Convert.ToBoolean(dr["Expiry"]),
                        SupplierName = Convert.ToString(dr["SupplierName"])

                    };
                    PurchaseList.Add(objPurchase);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return PurchaseList;

    }

    public List<PurchaseReturn> GetAll()
    {
        List<PurchaseReturn> PurchaseList = new List<PurchaseReturn>();
        SqlDataReader dr = new PurchaseReturnDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    PurchaseReturn objPurchase = new PurchaseReturn()
                    {
                        GrnNo = Convert.ToInt32(dr["Grn_No"]),
                        GRNDate = Convert.ToDateTime(dr["GRN_Date"]),
                        BillNo = Convert.ToString(dr["Bill_No"]),
                        BillDate = Convert.ToDateTime(dr["Bill_Date"]),
                        GRNo = Convert.ToString(dr["GR_No"]),
                        GRDate = Convert.ToDateTime(dr["GR_Date"]),
                        VehNo = Convert.ToString(dr["Veh_No"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        IsLocal = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        BillValue = Convert.ToDecimal(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        TotalAmount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]),
                        NetAmount = Convert.ToDecimal(dr["Net_Amount"]),
                        Pass = Convert.ToBoolean(dr["Pass"]),
                        SupplierId = Convert.ToInt32(dr["Supplier_Id"]),
                        GodownId = Convert.ToInt32(dr["Godown_Id"]),
                        ExciseAmt = Convert.ToDecimal(dr["Excise_Amt"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"]),
                     
                        Expiry = Convert.ToBoolean(dr["Expiry"]),

                    };
                    PurchaseList.Add(objPurchase);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return PurchaseList;

    }


    public string GetPurchaseDetailOptions(int PurchaseID, out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<PurchaseReturnDetail> PurchaseDetailList = new List<PurchaseReturnDetail>();
        SqlDataReader dr = new PurchaseReturnDAL().GetPurchaseReturnDetails(PurchaseID);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    counterId += 1;

                    strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:70px'/></td>" +
              "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:140px'>" + "<option pid='" + dr["Item_Code"].ToString() + "' taxid = '" + dr["Tax_Id"].ToString() + "' Tax = '" + dr["TaxP"].ToString() + "'  value='" + dr["Item_Code"].ToString() + "' Net='" + dr["Net"].ToString() + "' SurValue ='" + dr["SurValue"].ToString() + "' >" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
               "<td><input type='text' id='txtUnit" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtUnit' style='width:65px' value = '"+dr["Unit"].ToString()+"'/></td>" +
             "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:35px'   value='" + dr["Qty"].ToString() + "' /></td>" +
              "<td style='width:15px'><input type='checkbox' id='txtScheme" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtScheme' style='width:13px;margin-left:15px'/></td>" +
             
              "<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:55px'   value='" + dr["Rate"].ToString() + "' /></td>" +

             "<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:55px'   value='" + dr["Rate"].ToString() + "' /></td>" +
             "<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:55px'   value='" + dr["MRP"].ToString() + "' /></td>" +

            "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:55px'   value='" + dr["Amount"].ToString() + "' /></td>" +
            "<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:40px'   value='" + dr["Dis1P"].ToString() + "'  /></td>" +
            "<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:40px'   value='" + dr["Dis2P"].ToString() + "'  /></td>" +
            "<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:40px'   value='" + dr["TaxP"].ToString() + "' /></td>" +
              "<td><input type='text' id='txtExcisePer" + counterId + "' onkeyup ='javascript:ExciseChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtExcisePer' style='width:40px' value ='" + dr["Excise_Amt"].ToString() + "' /></td>" +
             "<td><input type='text' id='txtExciseAmt" + counterId + "'   counter='" + counterId + "'   class='form-control input-small' name='txtExciseAmt' style='width:70px' value = '"+dr["Excise_Amt"].ToString()+"' /></td>"+
           
            "<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
            "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");


                    //                 strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'   onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' style='width:70px' value='" + dr[0].ToString() + "'/></td>" +
                    //  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'  onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:140px'>" + "<option>" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
                    // "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");'  counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px' value='" + dr[0].ToString() + "'/></td>" +
                    // "<td><input type='text' id='txtRate" + counterId + "'  counter='" + counterId + "' readonly=readonly class='form-control input-small' name='txtRate' style='width:80px' value='" + dr[0].ToString() + "'/></td>" +
                    //"<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "' readonly=readonly class='form-control input-small' name='txtAmount' style='width:80px' value='" + dr[0].ToString() + "'/><input type='text' id='txtCost" + counterId + "' readonly=readonly class='form-control input-small' name='txtCost' style='width:80px;display:none' value='" + dr[0].ToString() + "'/></td>");
                    //                 strBuilder.Append("<td><table cellpadding='0'  border='0'><tr><td style='border:0px;margin:0px;padding:0px'><input type='text'  onkeyup='javascript:DiscountTextChange(" + counterId + ");' id='txtDiscountItem" + counterId + "' name='txtDiscountItem'   counter='" + counterId + "'  class='form-control input-small'  value='" + dr[0].ToString() + "'   style='width:50px;'  /></td><td style='border:0px;margin:0px;padding:0px'>-</td><td  style='border:0px;margin:0px;padding:0px'>	 <input type='text' id='txtDisAmountItem" + counterId + "'  onkeyup='javascript:DisAmountItemTextChange(" + counterId + ");' name='txtDisAmountItem'   counter='" + counterId + "' value='" + dr[0].ToString() + "' class='form-control input-small'    style='width:50px;'  /></td></tr></table>");




                    //                 strBuilder.Append("<td><div id='btnAddRow" + counterId + "'   onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>");
                    //                 strBuilder.Append("<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>");


                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }


    public int InsertUpdate(PurchaseReturn objPurchase, DataTable dt,DataTable dt1)
    {

        return new PurchaseReturnDAL().InsertUpdate(objPurchase, dt,dt1);
    }
}