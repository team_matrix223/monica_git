﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for AccountsBLL
/// </summary>
public class AccountsBLL
{
    public List<AccLedger> GetAccountsByCode(string CODE)
    {
        List<AccLedger> AccountList = new List<AccLedger>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccountsDAL().GetByCODE(CODE);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccLedger objAccLedger = new AccLedger()
                    {
                        AccountId = Convert.ToInt32(dr["AccountId"]),
                        CODE = dr["CODE"].ToString(),
                        CCODE = dr["CCODE"].ToString(),
                        H_CODE = Convert.ToString(dr["H_CODE"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        SS_CODE = Convert.ToString(dr["SS_CODE"]),
                        CNAME = Convert.ToString(dr["CNAME"]),
                        CADD1 = Convert.ToString(dr["CADD1"]),
                        CADD2 = Convert.ToString(dr["CADD2"]),
                        OP_BAL = Convert.ToDecimal(dr["OP_BAL"]),
                        DR_CR = Convert.ToString(dr["DR_CR"]),
                        
                    };
                    AccountList.Add(objAccLedger);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return AccountList;

    }



    public string KeywordSearchForDN(string Keyword, string Type, string AccountType, int GodownId, string ItemType,int BranchId,string Excise, out string Options)
    {
        DataSet ds = new AccountsDAL().KeywordSearchForDN(Keyword, Type, AccountType, GodownId, ItemType,BranchId,Excise);
        StringBuilder str = new StringBuilder();
        StringBuilder strOption = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {



                if (Type == "Accounts")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString()));
                    strOption.Append(string.Format("<option value='{0}' >{1}</option>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString()));
                }
                else if (Type == "Kit")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' sale_rate='{3}'  is_exist='{4}' value='{0}' >{1}</option>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["IsExist"]));

                }

                else if (Type == "Product")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{2}-{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Item_Code"].ToString()));

                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' DnRate = '{10}' Abatement = '{12}' Excise = '{11}' ExciseId = '{13}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}'  value='{0}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"], ds.Tables[0].Rows[i]["DeliveryNoteRate"], ds.Tables[0].Rows[i]["Excise"], ds.Tables[0].Rows[i]["Abatement"], ds.Tables[0].Rows[i]["Excise_ID"]));

                }
              

            }

        }
        Options = strOption.ToString();
        return str.ToString();
    }


    public string KeywordSearchForStock(string Keyword, string Type, string AccountType, int GroupId, string ItemType, int GodownId,int BranchId, out string Options)
    {
        DataSet ds = new AccountsDAL().KeywordSearchForStock(Keyword, Type, AccountType, GroupId, ItemType, GodownId, BranchId);
        StringBuilder str = new StringBuilder();
        StringBuilder strOption = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {



                str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{2}-{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Item_Code"].ToString()));
                strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' DnRate = '{10}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}'  value='{0}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"], ds.Tables[0].Rows[i]["DeliveryNoteRate"]));



            }

        }
        Options = strOption.ToString();
        return str.ToString();
    }

    public string KeywordSearch(string Keyword,string Type,string AccountType,int GodownId, int BranchId,out string Options)
    {
        DataSet ds = new AccountsDAL().KeywordSearch(Keyword, Type, AccountType, GodownId,BranchId);
        StringBuilder str = new StringBuilder();
        StringBuilder strOption = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {



                if (Type == "Accounts")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString()));
                    strOption.Append(string.Format("<option value='{0}' CADD1= '{2}' CONT_NO = '{3}' >{1}</option>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString(), ds.Tables[0].Rows[i]["CADD1"].ToString(), ds.Tables[0].Rows[i]["CONT_NO"].ToString()));

                }
                else if (Type == "Kit")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' sale_rate='{3}'  is_exist='{4}' value='{0}' >{1}</option>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["IsExist"]));
                
                }

                else if (Type == "Product")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{0}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }
                else if (Type == "MasterItem")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{4}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }

                else if (Type == "CashCustomer")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"].ToString()));
                    strOption.Append(string.Format("<option id='{0}' value='{0}'  name='{1} {2}'   address = '{3} {4}' phone='{5}' discount='{6}'     >{1} {2}</option>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Prefix"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"], ds.Tables[0].Rows[i]["Address_1"], ds.Tables[0].Rows[i]["Address_2"], ds.Tables[0].Rows[i]["Contact_No"], ds.Tables[0].Rows[i]["Discount"]));

                }
            
            }

        }
        Options = strOption.ToString();
        return str.ToString();
    }



    public string KeywordSearchSaleOnly(string Keyword, string Type, string AccountType, int GodownId,int BranchId, out string Options)
    {
        DataSet ds = new AccountsDAL().KeywordSearchSaleOnly(Keyword, Type, AccountType, GodownId,BranchId);
        StringBuilder str = new StringBuilder();
        StringBuilder strOption = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {



                if (Type == "Accounts")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString()));
                    strOption.Append(string.Format("<option value='{0}' CADD1= '{2}' CONT_NO = '{3}' >{1}</option>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString(), ds.Tables[0].Rows[i]["CADD1"].ToString(), ds.Tables[0].Rows[i]["CONT_NO"].ToString()));

                }
                else if (Type == "Kit")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' sale_rate='{3}'  is_exist='{4}' value='{0}' >{1}</option>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["IsExist"]));

                }

                else if (Type == "Product")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{0}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }
                else if (Type == "MasterItem")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{4}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }

                else if (Type == "CashCustomer")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"].ToString()));
                    strOption.Append(string.Format("<option id='{0}' value='{0}'  name='{1} {2}'   address = '{3} {4}' phone='{5}' discount='{6}'     >{1} {2}</option>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Prefix"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"], ds.Tables[0].Rows[i]["Address_1"], ds.Tables[0].Rows[i]["Address_2"], ds.Tables[0].Rows[i]["Contact_No"], ds.Tables[0].Rows[i]["Discount"]));

                }

            }

        }
        Options = strOption.ToString();
        return str.ToString();
    }



    public string KeywordSearchRaw(string Keyword, string Type, string AccountType, int GodownId,int BranchId, out string Options)
    {
        DataSet ds = new AccountsDAL().KeywordSearchRaw(Keyword, Type, AccountType, GodownId,BranchId);
        StringBuilder str = new StringBuilder();
        StringBuilder strOption = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {



                if (Type == "Accounts")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString()));
                    strOption.Append(string.Format("<option value='{0}' CADD1= '{2}' CONT_NO = '{3}' >{1}</option>", ds.Tables[0].Rows[i]["CCODE"].ToString(), ds.Tables[0].Rows[i]["CNAME"].ToString(), ds.Tables[0].Rows[i]["CADD1"].ToString(), ds.Tables[0].Rows[i]["CONT_NO"].ToString()));

                }
                else if (Type == "Kit")
                {
                    str.Append(string.Format("<div data-value='{0}' class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' sale_rate='{3}'  is_exist='{4}' value='{0}' >{1}</option>", ds.Tables[0].Rows[i]["Master_Code"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["IsExist"]));

                }

                else if (Type == "Product")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{0}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }
                else if (Type == "MasterItem")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString()));
                    strOption.Append(string.Format("<option mrp='{2}' stock_qty='{7}' Qty_In_Case ='{8}' QTy_To_Less = '{9}' sale_rate='{3}' item_code='{4}' tax_code='{5}'    tax_id='{6}' value='{4}' item_name='{1}' >{1}</option>", ds.Tables[0].Rows[i]["ItemID"].ToString(), ds.Tables[0].Rows[i]["Item_Name"].ToString(), ds.Tables[0].Rows[i]["Max_Retail_Price"], ds.Tables[0].Rows[i]["Sale_Rate"], ds.Tables[0].Rows[i]["Item_Code"], ds.Tables[0].Rows[i]["Tax_Code"], ds.Tables[0].Rows[i]["Tax_ID"], ds.Tables[0].Rows[i]["StockQty"], ds.Tables[0].Rows[i]["Qty_In_Case"], ds.Tables[0].Rows[i]["Qty_To_Less"]));

                }

                else if (Type == "CashCustomer")
                {
                    str.Append(string.Format("<div data-value='{0}'  class='item cutomdivitem'>{1}</div>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"].ToString()));
                    strOption.Append(string.Format("<option id='{0}' value='{0}'  name='{1} {2}'   address = '{3} {4}' phone='{5}' discount='{6}'     >{1} {2}</option>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Prefix"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"], ds.Tables[0].Rows[i]["Address_1"], ds.Tables[0].Rows[i]["Address_2"], ds.Tables[0].Rows[i]["Contact_No"], ds.Tables[0].Rows[i]["Discount"]));

                }

            }

        }
        Options = strOption.ToString();
        return str.ToString();
    }
    public DataSet GetAllDataSet()
    {
        return new AccountsDAL().GetAllDataSet();

    }
    public List<AccLedger> GetAll()
    {
        List<AccLedger> AccountList = new List<AccLedger>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccountsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccLedger objAccLedger = new AccLedger()
                    {
                        AccountId = Convert.ToInt32(dr["AccountId"]),
                        CODE = dr["CODE"].ToString(),
                        CCODE = dr["CCODE"].ToString(),
                        H_CODE = Convert.ToString(dr["H_CODE"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        SS_CODE = Convert.ToString(dr["SS_CODE"]),
                        CNAME = Convert.ToString(dr["CNAME"]),
                        CADD1 = Convert.ToString(dr["CADD1"]),
                        CADD2 = Convert.ToString(dr["CADD2"]),
                        OP_BAL = Convert.ToDecimal(dr["OP_BAL"]),
                        DR_CR = Convert.ToString(dr["DR_CR"]),
                    };
                    AccountList.Add(objAccLedger);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return AccountList;

    }

    public Int32 DeleteAccount(AccLedger objAccLedger)
    {
        return new AccountsDAL().Delete(objAccLedger);
    }
    public Int16 InsertUpdateAccLedger(AccLedger ObjAccLedger)
    {
        return new AccountsDAL().InsertUpdate(ObjAccLedger);
    }

    public void GetById(AccLedger objAccLedger)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new AccountsDAL().GetById(objAccLedger);
            if (dr.HasRows)
            {
                dr.Read();

                objAccLedger.AccountId = Convert.ToInt32(dr["AccountId"].ToString());
                objAccLedger.CODE = Convert.ToString(dr["CODE"]);
                objAccLedger.CCODE = dr["CCODE"].ToString();
                objAccLedger.H_CODE = Convert.ToString(dr["H_CODE"]);
                objAccLedger.S_CODE = Convert.ToString(dr["S_CODE"]);
                objAccLedger.SS_CODE = Convert.ToString(dr["SS_CODE"]);
                objAccLedger.CNAME = Convert.ToString(dr["CNAME"]);
                objAccLedger.CADD1 = Convert.ToString(dr["CADD1"]);
                objAccLedger.CADD2 = Convert.ToString(dr["CADD2"]);
                objAccLedger.CITY_ID = Convert.ToInt16(dr["CITY_ID"]);
                objAccLedger.AREA_ID = Convert.ToInt16(dr["AREA_ID"]);
                objAccLedger.STATE_ID = Convert.ToInt16(dr["STATE_ID"]);
                objAccLedger.CST_NO = Convert.ToString(dr["CST_NO"]);
                objAccLedger.CST_DATE = Convert.ToDateTime(dr["CST_DATE"]);
                objAccLedger.TINNO = Convert.ToString(dr["TINNO"]);
                objAccLedger.TOTNO = Convert.ToString(dr["TOTNO"]);
                objAccLedger.CR_LIMIT = Convert.ToDecimal(dr["CR_LIMIT"]);
                objAccLedger.CR_DAYS = Convert.ToInt16(dr["CR_DAYS"]);
                objAccLedger.CONT_PER = Convert.ToString(dr["CONT_PER"]);
                objAccLedger.CONT_NO = Convert.ToString(dr["CONT_NO"]);
                objAccLedger.OP_BAL = Convert.ToDecimal(dr["OP_BAL"]);
                objAccLedger.DR_CR = Convert.ToString(dr["DR_CR"]);
                objAccLedger.DIS_PER = Convert.ToDecimal(dr["DIS_PER"]);
                objAccLedger.OS_BAL = Convert.ToDecimal(dr["OS_BAL"]);
                objAccLedger.PURSALE_ACC_PERCENT = Convert.ToInt32(dr["PURSALE_ACC_PERCENT"]);
                objAccLedger.PURSALE_ACC_TYPE = Convert.ToString(dr["PURSALE_ACC_TYPE"]);
                objAccLedger.PREFIX = Convert.ToString(dr["PREFIX"]);
                objAccLedger.ACC_ZONE = Convert.ToString(dr["ACC_ZONE"]);
                objAccLedger.SRNO = Convert.ToDecimal(dr["SRNO"]);
                objAccLedger.ShowInLedger = Convert.ToBoolean(dr["ShowInLedger"]);
                objAccLedger.tAG = Convert.ToString(dr["tAG"]);
                objAccLedger.UserId = Convert.ToInt32(dr["UserId"]);
             





            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}