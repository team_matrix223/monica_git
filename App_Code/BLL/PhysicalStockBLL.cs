﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for PhysicalStockBLL
/// </summary>
public class PhysicalStockBLL
{
    public string GetOptions(string Type)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new PhysicalStockDAL().GetAll(Type);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                   
                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["ID"].ToString(), dr["Name"].ToString()));

                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return strBuilder.ToString();

    }





    public List<StockExpiry> GetAllPrroductsByType(string Type, string Sorting, Int32 Id, string Name,string OrderType,int Branch)
    {
      
        List<StockExpiry> StockList = new List<StockExpiry>();
        SqlDataReader dr = null;
        try
        {
            dr = new PhysicalStockDAL().GetAllPrroductsByType(Type, Sorting, Id, Name, OrderType,Branch);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    StockExpiry objStock1= new StockExpiry()
                    {
                        Code = Convert.ToString(dr["Master_Code"]),
                        IName = Convert.ToString(dr["Item_Name"]),
                        Rate = Convert.ToDecimal(dr["Sale_Rate"]),
                        Unit = Convert.ToString(dr["Sales_In_Unit"]),
                        Crstock = Convert.ToDecimal(dr["PHy_Stock"]),

                    };

                    StockList.Add(objStock1);
                    

                  
                }
              
            }

        }
        finally
        {

        }
        return StockList;

    }





    public int Insert(Int32 GodownId, DataTable dt, string Status, int BranchId, int UserNo)
    {

        return new PhysicalStockDAL().Insert(GodownId, dt, Status, BranchId, UserNo);
    }


}