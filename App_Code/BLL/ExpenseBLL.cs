﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ExpenseBLL
/// </summary>
public class ExpenseBLL
{
    public Int16 InsertUpdate(expenses objExpense, DataTable dt, DateTime date)
    {

        return new ExpenseDAL().InsertUpdate(objExpense, dt, date);
    }

    public List<expenses> GetById(int BranchId)
    {

        SqlDataReader dr = null;
        List<expenses> objDetail = new List<expenses>();
        try
        {
            dr = new ExpenseDAL().GetByID(BranchId);
          
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    expenses objKD = new expenses();



                    objKD.Description = Convert.ToString(dr["Description"].ToString());
                    objKD.Amount = Convert.ToDecimal(dr["Amount"]);
                    // objKD.Bill_Date = Convert.ToDateTime(dr["Bill_Date"]);

                    objDetail.Add(objKD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

    public List<expenses> GetByDate(int BranchId, DateTime Date)
    {

        SqlDataReader dr = null;
        List<expenses> objDetail = new List<expenses>();
        try
        {
            dr = new ExpenseDAL().GetByDate(BranchId, Date);

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    expenses objKD = new expenses();



                    objKD.Description = Convert.ToString(dr["Description"].ToString());
                    objKD.Amount = Convert.ToDecimal(dr["Amount"]);
                    // objKD.Bill_Date = Convert.ToDateTime(dr["Bill_Date"]);

                    objDetail.Add(objKD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

}