﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserBLL
/// </summary>
public class UserBLL
{


    public List<User> GetAll(int BranchId)
    {
        List<User> UserList = new List<User>();
      
        SqlDataReader dr = null;
        try
        {
            dr = new UserDAL().GetAll(BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    User objUser = new User()
                    {
                        UserNo = Convert.ToInt32(dr["UserNo"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        
                        User_ID = dr["User_ID"].ToString(),
                        UserPWD = dr["UserPWD"].ToString(),
                        Counter_NO =Convert.ToInt16(dr["Counter_NO"]),
                        Discontinued =Convert.ToBoolean(dr["Discontinued"]),
                        IsLogin =Convert.ToBoolean(dr["IsLogin"]),
                        SessionId =dr["SessionId"].ToString(),
                         

                    };
                    UserList.Add(objUser);
                }
            }

        }

        finally
        {
           
            dr.Close();
            dr.Dispose();
        }
        return UserList;

    }


    public Int32 UserLoginCheck(User objUser)
    {
        return new UserDAL().userLoginCheck(objUser);
    }

    public Int32 InsertUpdate(User objUser)
    {
        return new UserDAL().InsertUpdate(objUser);
    }

    public Int32 chkIsLogin(User objUser)
    {
        return new UserDAL().chkIsLogin(objUser);
    }


    public void GetByUserId(User objUser)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new UserDAL().GetByUserId(objUser);
            if (dr.HasRows)
            {
                dr.Read();

                objUser.UserNo = Convert.ToInt16(dr["UserNo"]);
                objUser.User_ID = dr["User_ID"].ToString();
                objUser.Counter_NO = Convert.ToInt16(dr["Counter_NO"]);
                objUser.BranchId = Convert.ToInt32(dr["BranchId"]);
                objUser.BranchName = dr["BranchName"].ToString();

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

    public Int32 SuperUserLoginCheck(User objUser,int BranchId)
    {
        return new UserDAL().SuperuserLoginCheck(objUser,BranchId);
    }

   
    public string ChangePassword(User objUser)
    {
        return new UserDAL().ChangePassword(objUser);

    }
}