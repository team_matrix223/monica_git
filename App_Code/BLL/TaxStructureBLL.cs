﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

/// <summary>
/// Summary description for TaxStructureBLL
/// </summary>
public class TaxStructureBLL
{
    public List<TaxStructure> GetAll(int BranchId)
    
    {
        List<TaxStructure> TaxList = new List<TaxStructure>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new TaxStructureDAL().GetAll(BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TaxStructure objTax = new TaxStructure()
                    {
                        Tax_Rate = Convert.ToDecimal(dr["Tax_Rate"]),
                        SurValue = Convert.ToDecimal(dr["SurValue"]),
                        Tax_ID = Convert.ToInt32(dr["Tax_ID"]),
                        Description = dr["Description"].ToString(),

                        Dr_AccCode = Convert.ToString(dr["Dr_AccCode"]),
                        VatCode_Dr = Convert.ToBoolean(dr["VatCode_Dr"]),
                        Dr_VatCode = Convert.ToString(dr["Dr_VatCode"]),
                        Cr_AccCode = Convert.ToString(dr["Cr_AccCode"]),
                        Dr_Sur = dr["Dr_Sur"].ToString(),
                        Cr_Sur = dr["Cr_Sur"].ToString(),


                        sDr_AccCode = Convert.ToString(dr["sDr_AccCode"]),
                        sCr_AccCode = Convert.ToString(dr["sCr_AccCode"]),
                        sCr_VatCode = Convert.ToString(dr["sCr_VatCode"]),
                        sDr_VatCode = Convert.ToString(dr["sDr_VatCode"]),
                        sDr_Sur = dr["sDr_Sur"].ToString(),
                        sCr_Sur = dr["sCr_Sur"].ToString(),
                        




                        VatCode_Cr = Convert.ToBoolean(dr["VatCode_Cr"]),
                        Cr_VatCode = Convert.ToString(dr["Cr_VatCode"]),


                        ChkSur = Convert.ToBoolean(dr["ChkSur"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                    };
                    TaxList.Add(objTax);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return TaxList;

    }

    public void GetTaxStructure(TaxStructure objTaxStructure,int BranchId)
    {
        new TaxStructureDAL().GetByTaxStructure(objTaxStructure,BranchId);
    }


    public string GetOptions(int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new TaxStructureDAL().GetAll(BranchId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Tax_ID"].ToString(), dr["Tax_Rate"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(TaxStructure objTaxStructure)
    {

        return new TaxStructureDAL().InsertUpdate(objTaxStructure);
    }

    public Int32 DeleteTax(TaxStructure objTaxStructure)
    {
        return new TaxStructureDAL().Delete(objTaxStructure);
    }
}