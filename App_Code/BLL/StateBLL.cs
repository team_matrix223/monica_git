﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for StateBLL
/// </summary>
public class StateBLL
{

    public Int32 DeleteState(States objState)
    {
        return new StateDAL().Delete(objState);
    }

    public List<States> GetAll()
    {
        List<States> StateList = new List<States>();

        SqlDataReader dr = null;
        try
        {
            dr = new StateDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    States objState = new States()
                    {
                        State_Name = dr["State_Name"].ToString(),
                        State_ID = Convert.ToInt16(dr["State_ID"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    StateList.Add(objState);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return StateList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new StateDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["State_ID"].ToString(), dr["State_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(States objState)
    {

        return new StateDAL().InsertUpdate(objState);
    }

    public void GetById(States objState)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new StateDAL().GetById(objState);
            if (dr.HasRows)
            {
                dr.Read();

                objState.UserId = Convert.ToInt16(dr["UserId"]);
                objState.State_Name = dr["State_Name"].ToString();
                objState.State_ID = Convert.ToInt16(dr["State_ID"]);
                objState.IsActive = Convert.ToBoolean(dr["IsActive"]);


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


}