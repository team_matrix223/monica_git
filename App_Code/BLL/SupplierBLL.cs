﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for SupplierBLL
/// </summary>
public class SupplierBLL
{
    public Int32 DeleteSupplier(Supplier objSupplier)
    {
        return new SupplierDAL().Delete(objSupplier);
    }


    public void GetById(Supplier objSupplier)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new SupplierDAL().GetById(objSupplier);
            if (dr.HasRows)
            {
                dr.Read();

                objSupplier.Supplier_Name = dr["Supplier_Name"].ToString();
                objSupplier.Supplier_ID = Convert.ToInt16(dr["Supplier_ID"]);
                objSupplier.Prefix = dr["Prefix"].ToString();
                objSupplier.Address_1 = Convert.ToString(dr["Address_1"]);
                objSupplier.Address_2 = Convert.ToString(dr["Address_2"]);
                objSupplier.Area_ID = Convert.ToInt32(dr["Area_ID"]);
                objSupplier.City_ID = Convert.ToInt32(dr["City_ID"]);

                objSupplier.CST_No = dr["CST_No"].ToString();
                objSupplier.State_ID = Convert.ToInt16(dr["State_ID"]);
                objSupplier.CST_Date = Convert.ToDateTime(dr["CST_Date"]);
                objSupplier.TIN_No = Convert.ToString(dr["TIN_No"]);
                objSupplier.TOT_No = Convert.ToString(dr["TOT_No"]);
                objSupplier.Cont_Person = Convert.ToString(dr["Cont_Person"]);
                objSupplier.Cont_No = Convert.ToString(dr["Cont_No"]);
                objSupplier.UserId = Convert.ToInt16(dr["UserId"]);

                objSupplier.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objSupplier.CompanyList = Convert.ToString(dr["CompanyList"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }




    public string GetSupplierHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new SupplierDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Supplier_ID"] + "' name='supplier' id='chkS_" + dr["Supplier_ID"] + "'/> <label for='chkS_" + dr["Supplier_ID"] + "'>" + dr["Supplier_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Suppliers Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }


    public List<Supplier> GetAll()
    {
        List<Supplier> SupplierList = new List<Supplier>();

        SqlDataReader dr = null;
        try
        {
            dr = new SupplierDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Supplier objSupplier = new Supplier()
                    {
                        Supplier_Name = dr["Supplier_Name"].ToString(),
                        Supplier_ID = Convert.ToInt16(dr["Supplier_ID"]),
                        Prefix = dr["Prefix"].ToString(),
                        Address_1 = Convert.ToString(dr["Address_1"]),
                        Address_2 = Convert.ToString(dr["Address_2"]),
                        Area_ID = Convert.ToInt32(dr["Area_ID"]),
                        City_ID = Convert.ToInt32(dr["City_ID"]),

                        CST_No = dr["CST_No"].ToString(),
                        State_ID = Convert.ToInt16(dr["State_ID"]),
                        CST_Date = Convert.ToDateTime(dr["CST_Date"]),
                        TIN_No = Convert.ToString(dr["TIN_No"]),
                        TOT_No = Convert.ToString(dr["TOT_No"]),
                        Cont_Person = Convert.ToString(dr["Cont_Person"]),
                        Cont_No = Convert.ToString(dr["Cont_No"]),
                        UserId = Convert.ToInt16(dr["UserId"]),

                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        CompanyList = Convert.ToString(dr["CompanyList"])

                    };
                    SupplierList.Add(objSupplier);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return SupplierList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SupplierDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Supplier_ID"].ToString(), dr["Supplier_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Supplier objSupplier, DataTable dt)
    {

        return new SupplierDAL().InsertUpdate(objSupplier, dt);
    }
}