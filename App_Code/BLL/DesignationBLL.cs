﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for DesignationBLL
/// </summary>
public class DesignationBLL
{
   

    public void GetById(Designations objDesignation)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new DesignationDAL().GetById(objDesignation);
            if (dr.HasRows)
            {
                dr.Read();


                objDesignation.DesgName = dr["DesgName"].ToString();
                objDesignation.DesgID = Convert.ToInt16(dr["DesgID"]);
                objDesignation.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objDesignation.UserId = Convert.ToInt32(dr["UserId"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<Designations> GetAll()
    {
        List<Designations> DesignationList = new List<Designations>();

        SqlDataReader dr = null;
        try
        {
            dr = new DesignationDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Designations objDesignation = new Designations()
                    {
                        DesgName = dr["DesgName"].ToString(),

                        DesgID = Convert.ToInt16(dr["DesgID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    DesignationList.Add(objDesignation);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DesignationList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new DesignationDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["DesgID"].ToString(), dr["DesgName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Designations objDesignation)
    {

        return new DesignationDAL().InsertUpdate(objDesignation);
    }
}