﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for CompanyBLL
/// </summary>
public class CompanyBLL
{
    public Int32 DeleteCompany(Company objCompany)
    {
        return new CompanyDAL().Delete(objCompany);
    }


    public void GetById(Company objCompany)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new CompanyDAL().GetById(objCompany);
            if (dr.HasRows)
            {
                dr.Read();

                objCompany.UserId = Convert.ToInt16(dr["UserId"]);
                objCompany.Company_Name = dr["Company_Name"].ToString();
                objCompany.Company_ID = Convert.ToInt16(dr["Company_ID"]);
                objCompany.IsActive = Convert.ToBoolean(dr["IsActive"]);


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public List<Company> GetAll()
    {
        List<Company> CompanyList = new List<Company>();

        SqlDataReader dr = null;
        try
        {
            dr = new CompanyDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Company objCompany = new Company()
                    {
                        Company_Name = dr["Company_Name"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        Company_ID = Convert.ToInt16(dr["Company_ID"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        SupplierList=Convert.ToString(dr["SupplierList"])
                    };
                    CompanyList.Add(objCompany);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return CompanyList;

    }



    public string GetCompanyHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new CompanyDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {
                        html += "<tr><td><input type='checkbox' value='" + dr["Company_ID"] + "' name='company' id='chkS_" + dr["Company_ID"] + "'/> <label for='chkS_" + dr["Company_ID"] + "'>" + dr["Company_Name"] + "</label></td></tr>";

                    }

                }
            }
            else
            {
                html = "<tr><td>No Suppliers Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new CompanyDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Company_ID"].ToString(), dr["Company_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Company objCompany,DataTable dt)
    {

        return new CompanyDAL().InsertUpdate(objCompany,dt);
    }

}