﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for ItemMasterBLL
/// </summary>
public class ItemMasterBLL
{

    public Int32 DeleteItem(ItemMaster objItemMaster)
    {
        return new ItemMasterDAL().Delete(objItemMaster);
    }


    public DataSet VariableMasterGetAll()
    {
        return new ItemMasterDAL().VariableMastersGetAll();
    }



    public List<NutritionFacts> GetProductById(ItemMaster objItemMaster)
    {
        List<NutritionFacts> lst = new List<NutritionFacts>();

        SqlDataReader dr = null;
        try
        {
            dr = new ItemMasterDAL().GetProductById(objItemMaster);
            if (dr.HasRows)
            {
                dr.Read();

                        objItemMaster.ItemID = Convert.ToInt32(dr["ItemID"]);
                        objItemMaster.COMPANY_ID = Convert.ToInt16(dr["COMPANY_ID"]);
                        objItemMaster.GROUP_ID = Convert.ToInt16(dr["GROUP_ID"]);
                        objItemMaster.Master_Code = dr["Master_Code"].ToString();
                        objItemMaster.Item_Code = dr["Item_Code"].ToString();
                        objItemMaster.Item_Name = dr["Item_Name"].ToString();
                        objItemMaster.Packing = dr["Packing"].ToString();
                        objItemMaster.Bar_Code = dr["Bar_Code"].ToString();
                        objItemMaster.Short_Name1 = dr["Short_Name1"].ToString();
                        objItemMaster.Short_Name2 = dr["Short_Name2"].ToString();
                        objItemMaster.Sales_In_Unit = dr["Sales_In_Unit"].ToString();
                        objItemMaster.Sales_In_Unit2 = Convert.ToInt16(dr["Sales_In_Unit2"]);
                        objItemMaster.Qty_To_Less = Convert.ToDecimal(dr["Qty_To_Less"]);
                        objItemMaster.Sale_Rate2 = Convert.ToDecimal(dr["Sale_Rate2"]);   
                        objItemMaster.Sale_Rate3 = Convert.ToDecimal(dr["Sale_Rate3"]);
                        objItemMaster.Sale_Rate4 = Convert.ToDecimal(dr["Sale_Rate4"]); 
                        objItemMaster.Purchase_Rate = Convert.ToDecimal(dr["Purchase_Rate"]); 
                        objItemMaster.Max_Retail_Price =Convert.ToDecimal(dr["Max_Retail_Price"]); 
                        objItemMaster.Mark_Up = Convert.ToDecimal(dr["Mark_Up"]);
                        objItemMaster.Qty_in_Case = Convert.ToDecimal(dr["Qty_in_Case"]);
                        objItemMaster.Max_Level = Convert.ToDecimal(dr["Max_Level"]);
                        objItemMaster.Min_Level = Convert.ToDecimal(dr["Min_Level"]);
                        objItemMaster.Re_Order_Qty = Convert.ToDecimal(dr["Re_Order_Qty"]);
                        objItemMaster.Cursor_On = Convert.ToString(dr["Cursor_On"]);
                        objItemMaster.Tax_Code = Convert.ToString(dr["Tax_Code"]);
                        objItemMaster.Pur_Code = Convert.ToString(dr["Pur_Code"]);
                        objItemMaster.Sale_Code = Convert.ToString(dr["Sale_Code"]);
                        objItemMaster.IsSaleable = Convert.ToBoolean(dr["IsSaleable"]);
                        objItemMaster.Delear_Margin = Convert.ToDecimal(dr["Delear_Margin"]);
                        objItemMaster.Tax_AfterBefore = Convert.ToString(dr["Tax_AfterBefore"]);
                        objItemMaster.Discount = Convert.ToDecimal(dr["Discount"]);
                        objItemMaster.Location = Convert.ToDecimal(dr["Location"]);
                        objItemMaster.Item_Type = Convert.ToString(dr["Item_Type"]);
                        objItemMaster.Department = Convert.ToString(dr["Department"]);
                        objItemMaster.Sale_Rate = Convert.ToDecimal(dr["Sale_Rate"]);
                        objItemMaster.Sale_Rate_Excl = Convert.ToDecimal(dr["Sale_Rate_Excl"]);   
                        objItemMaster.Whole_Sale_Rate = Convert.ToDecimal(dr["Whole_Sale_Rate"]);
                        objItemMaster.Delivery_No = Convert.ToDecimal(dr["Delivery_No"]);
                        objItemMaster.SGroup_id = Convert.ToDecimal(dr["SGroup_id"]);
                        objItemMaster.TAGGED = Convert.ToBoolean(dr["TAGGED"]);   
                        objItemMaster.Franchise_SaleRate = Convert.ToDecimal(dr["Franchise_SaleRate"]);
                        objItemMaster.Discontinued = Convert.ToBoolean(dr["Discontinued"]);
                        objItemMaster.Transaction_Mode = Convert.ToString(dr["Transaction_Mode"]);
                        objItemMaster.InHouse_Packing = Convert.ToBoolean(dr["InHouse_Packing"]);
                        objItemMaster.InHouse_PackedQty = Convert.ToDecimal(dr["InHouse_PackedQty"]);
                        objItemMaster.Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]);
                        objItemMaster.Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]);
                        objItemMaster.Dis1Value = Convert.ToDecimal(dr["Dis1Value"]);
                        objItemMaster.Dis2Value = Convert.ToDecimal(dr["Dis2Value"]);
                        objItemMaster.IsCompBarCode = Convert.ToBoolean(dr["IsCompBarCode"]);
                        objItemMaster.Remarks = Convert.ToString(dr["Remarks"]);
                        objItemMaster.AllowPoint = Convert.ToBoolean(dr["AllowPoint"]);
                        objItemMaster.Tax_ID = Convert.ToInt32(dr["Tax_ID"]);
                        objItemMaster.CpPrinting = Convert.ToString(dr["CpPrinting"]);
                        objItemMaster.Excise_ID = Convert.ToDecimal(dr["Excise_ID"]);
                        objItemMaster.Excise_Code = Convert.ToDecimal(dr["Excise_Code"]);
                        objItemMaster.ImageUrl = Convert.ToString(dr["ImageUrl"]);
                        objItemMaster.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
                        objItemMaster.Likes = Convert.ToInt32(dr["Likes"]);
                        objItemMaster.Day = Convert.ToInt32(dr["Day"]);
                        objItemMaster.BestExp = Convert.ToBoolean(dr["BestExp"]);
                        objItemMaster.VEG_NonVeg = Convert.ToString(dr["VEG_NonVeg"]);
                        objItemMaster.COLOR_ID = Convert.ToInt32(dr["COLOR_ID"]);
                        objItemMaster.MasterItemName = dr["MasterItemName"].ToString();
                        objItemMaster.ParentItemCode = Convert.ToString(dr["Master_Code"]);
                        objItemMaster.DeliveryNoteRate = Convert.ToDecimal(dr["DeliveryNoteRate"]);
                        objItemMaster.ExpDate = Convert.ToDateTime(dr["ExpDate"]);
                        objItemMaster.Edit_SaleRate = Convert.ToBoolean(dr["Edit_SaleRate"]);
                        objItemMaster.HSNCode = Convert.ToString(dr["HSNCode"]);
            }
            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    NutritionFacts obj = new NutritionFacts();
                    obj.Field1 = dr["Field1"].ToString();
                    obj.Field2 = dr["Field2"].ToString();
                    obj.Field3 = dr["Field3"].ToString();
                    obj.Bold =Convert.ToBoolean(dr["Bold"]);
                    obj.Ingredients = dr["Ingredients"].ToString();
                    lst.Add(obj);
                }
            
            }

            return lst;

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


    public List<Item> GetAllItems()
    {
        List<Item> ItemsList = new List<Item>();

        SqlDataReader dr = null;
        try
        {
            dr = new ItemMasterDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Item objItemMaster = new Item()
                    {

                        ItemID = Convert.ToInt32(dr["ItemID"]),
                       
                        Master_Code = dr["Master_Code"].ToString(),
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        Bar_Code = dr["Bar_Code"].ToString(),
                        Sales_In_Unit = dr["Sales_In_Unit"].ToString(),
                        Purchase_Rate = Convert.ToDecimal(dr["Purchase_Rate"]),
                        Max_Retail_Price = Convert.ToDecimal(dr["Max_Retail_Price"]),

                    };
                    ItemsList.Add(objItemMaster);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ItemsList;

    }



    public List<ItemMaster> GetAll()
    {
        List<ItemMaster> ItemsList = new List<ItemMaster>();

        SqlDataReader dr = null;
        try
        {
            dr = new ItemMasterDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ItemMaster objItemMaster = new ItemMaster()
                    {
                      
                        ItemID = Convert.ToInt32(dr["ID"]),
                        COMPANY_ID = Convert.ToInt16(dr["COMPANY_ID"]),
                        GROUP_ID = Convert.ToInt16(dr["GROUP_ID"]),
                        Master_Code = dr["Master_Code"].ToString(),
                        Item_Code = dr["Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        Packing = dr["Packing"].ToString(),
                        Bar_Code = dr["Bar_Code"].ToString(),
                        Short_Name1 = dr["Short_Name1"].ToString(),
                        Short_Name2 = dr["Short_Name2"].ToString(),
                        Sales_In_Unit = dr["Sales_In_Unit"].ToString(),
                        Sales_In_Unit2=Convert.ToInt16(dr["Sales_In_Unit2"]),
                        Qty_To_Less = Convert.ToDecimal(dr["Qty_To_Less"]),
                        Sale_Rate2 = Convert.ToDecimal(dr["Sale_Rate2"]),    
                        Sale_Rate3 = Convert.ToDecimal(dr["Sale_Rate3"]), 
                        Sale_Rate4 = Convert.ToDecimal(dr["Sale_Rate4"]), 
                        Purchase_Rate = Convert.ToDecimal(dr["Purchase_Rate"]), 
                        Max_Retail_Price =Convert.ToDecimal(dr["Max_Retail_Price"]), 
                        Mark_Up = Convert.ToDecimal(dr["Mark_Up"]),
                        Qty_in_Case = Convert.ToDecimal(dr["Qty_in_Case"]),
                        Max_Level = Convert.ToDecimal(dr["Max_Level"]),
                        Min_Level = Convert.ToDecimal(dr["Min_Level"]),
                        Re_Order_Qty = Convert.ToDecimal(dr["Re_Order_Qty"]),
                        Cursor_On = Convert.ToString(dr["Cursor_On"]),
                        Tax_Code = Convert.ToString(dr["Tax_Code"]),
                        Pur_Code = Convert.ToString(dr["Pur_Code"]),
                        Sale_Code = Convert.ToString(dr["Sale_Code"]),
                        IsSaleable = Convert.ToBoolean(dr["IsSaleable"]),
                        Delear_Margin = Convert.ToDecimal(dr["Delear_Margin"]),
                        Tax_AfterBefore = Convert.ToString(dr["Tax_AfterBefore"]),
                         Discount = Convert.ToDecimal(dr["Discount"]),
                        Location = Convert.ToDecimal(dr["Location"]),
                        Item_Type = Convert.ToString(dr["Item_Type"]),
                        Department = Convert.ToString(dr["Department"]),
                        Sale_Rate = Convert.ToDecimal(dr["Sale_Rate"]),
                        Sale_Rate_Excl = Convert.ToDecimal(dr["Sale_Rate_Excl"]),   
                        Whole_Sale_Rate = Convert.ToDecimal(dr["Whole_Sale_Rate"]),
                        Delivery_No = Convert.ToDecimal(dr["Delivery_No"]),
                        SGroup_id = Convert.ToDecimal(dr["SGroup_id"]),
                        TAGGED = Convert.ToBoolean(dr["TAGGED"]),   
                        Franchise_SaleRate = Convert.ToDecimal(dr["Franchise_SaleRate"]),
                        Discontinued = Convert.ToBoolean(dr["Discontinued"]),
                        Transaction_Mode = Convert.ToString(dr["Transaction_Mode"]),
                        InHouse_Packing = Convert.ToBoolean(dr["InHouse_Packing"]),
                        InHouse_PackedQty = Convert.ToDecimal(dr["InHouse_PackedQty"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis1Value = Convert.ToDecimal(dr["Dis1Value"]),
                        Dis2Value = Convert.ToDecimal(dr["Dis2Value"]),
                        IsCompBarCode = Convert.ToBoolean(dr["IsCompBarCode"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        AllowPoint = Convert.ToBoolean(dr["AllowPoint"]),
                        Tax_ID = Convert.ToInt32(dr["Tax_ID"]),
                        CpPrinting = Convert.ToString(dr["CpPrinting"]),
                        Excise_ID = Convert.ToDecimal(dr["Excise_ID"]),
                        Excise_Code = Convert.ToDecimal(dr["Excise_Code"]),
                        ImageUrl = Convert.ToString(dr["ImageUrl"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        Likes = Convert.ToInt32(dr["Likes"]),
                        //Day = Convert.ToInt32(dr["Day"]),
                        //BestExp = Convert.ToBoolean(dr["BestExp"]),
                        //VEG_NonVeg = Convert.ToString(dr["VEG_NonVeg"]),
                        COLOR_ID = Convert.ToInt32(dr["COLOR_ID"]),
                        DeliveryNoteRate = Convert.ToDecimal(dr["DeliveryNoteRate"]),
                       HSNCode=Convert.ToString(dr["HSNCode"]),
                    };
                    ItemsList.Add(objItemMaster);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ItemsList;

    }

    public Int32 InsertUpdate(ItemMaster objItemMaster,DataTable dt)
    {

        return new ItemMasterDAL().InsertUpdate(objItemMaster,dt);
    }

    public List<Item> GetAllItemsDiscontinued()
    {
        List<Item> ItemsList = new List<Item>();

        SqlDataReader dr = null;
        try
        {
            dr = new ItemMasterDAL().GetAllDiscontinued();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Item objItemMaster = new Item()
                    {

                        ItemID = Convert.ToInt32(dr["ItemID"]),

                        Master_Code = dr["Master_Code"].ToString(),
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        Bar_Code = dr["Bar_Code"].ToString(),
                        Sales_In_Unit = dr["Sales_In_Unit"].ToString(),
                        Purchase_Rate = Convert.ToDecimal(dr["Purchase_Rate"]),
                        Max_Retail_Price = Convert.ToDecimal(dr["Max_Retail_Price"]),

                    };
                    ItemsList.Add(objItemMaster);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ItemsList;

    }

   
  
}