﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for PropSGroupBLL
/// </summary>
public class PropSGroupBLL
{
    public List<PropSGroups> GetAll()
    {
        List<PropSGroups> SGroupList = new List<PropSGroups>();

        SqlDataReader dr = null;
        try
        {
            dr = new PropSGroupDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PropSGroups objPropSGroup = new PropSGroups()
                    {
                        SGroup_Name = dr["SGroup_Name"].ToString(),
                        ShowInMenu = Convert.ToBoolean(dr["ShowInMenu"]),
                        SGroup_ID = Convert.ToInt16(dr["SGroup_ID"]),
                        Group_Id = Convert.ToInt16(dr["Group_Id"]),
                        Department_Id = Convert.ToInt16(dr["Department_Id"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                    };
                    SGroupList.Add(objPropSGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return SGroupList;

    }

    public string GetOptionsByGroupId(int GroupId)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new PropSGroupDAL().GetByGroupId(GroupId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["SGroup_ID"].ToString(), dr["SGroup_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }


    public List<PropSGroups> GetByGroupId(int GroupId)
    {
        List<PropSGroups> GroupList = new List<PropSGroups>();

        SqlDataReader dr = null;
        try
        {
            dr = new PropSGroupDAL().GetByGroupId(GroupId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PropSGroups objPropSGroup = new PropSGroups()
                    {
                        SGroup_Name = dr["SGroup_Name"].ToString(),
                        ShowInMenu = Convert.ToBoolean(dr["ShowInMenu"]),
                        SGroup_ID = Convert.ToInt16(dr["SGroup_ID"]),
                        Group_Id = Convert.ToInt16(dr["Group_Id"]),
                        Department_Id = Convert.ToInt16(dr["Department"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                    };
                    GroupList.Add(objPropSGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return GroupList;

    }


    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new PropSGroupDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["SGroup_ID"].ToString(), dr["SGroup_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(PropSGroups objPropSGroup)
    {

        return new PropSGroupDAL().InsertUpdate(objPropSGroup);
    }

    public Int32 DeletePropSGroup(PropSGroups objPropSGroup)
    {
        return new PropSGroupDAL().Delete(objPropSGroup);
    }
}