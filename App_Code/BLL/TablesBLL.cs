﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for TablesBLL
/// </summary>
public class TablesBLL
{
    public List<Tables> GetAll()
    {
        List<Tables> TableList = new List<Tables>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new TablesDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Tables objTables = new Tables()
                    {
                        TableID = Convert.ToInt32(dr["TableID"]),
                        TableName = dr["TableName"].ToString(),
                        Reserve = Convert.ToBoolean(dr["Reserve"]),

                    };
                    TableList.Add(objTables);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return TableList;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new TablesDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option value ='0'>Table No</option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["TableID"].ToString(), dr["TableName"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

}