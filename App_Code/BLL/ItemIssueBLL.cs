﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for StockAdjustmentBLL
/// </summary>
public class ItemIssueBLL
{


    public List<ItemIssue> GetByRefNo(int RefNo,int BranchId)
    {
        List<ItemIssue> TransferList = new List<ItemIssue>();
        SqlDataReader dr = new ItemIssueDAL().GetByRefNo(RefNo,BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    ItemIssue objItemIssue = new ItemIssue()
                    {
                        RefNo = Convert.ToInt32(dr["ISSUE_NO"]),
                        Date = Convert.ToString(dr["ISSUE_DATE"]),
                 
                        MRP = Convert.ToDecimal(dr["MRP"]),
                        Rate = Convert.ToDecimal(dr["Sale_Rate"]),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Stock = 0,
                        Item_Code =dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                      
                        
                    };
                    TransferList.Add(objItemIssue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }

    public List<ItemIssue> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<ItemIssue> TransferList = new List<ItemIssue>();
        SqlDataReader dr = new ItemIssueDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    ItemIssue objItemIssue = new ItemIssue()
                    {
                        RefNo = Convert.ToInt32(dr["ISSUE_NO"]),
                        Date = Convert.ToString(dr["ISSUE_DATE"]),
                        Godown_ID=Convert.ToInt32(dr["Godown_ID"]),
                        GodownTo = Convert.ToInt32(dr["GodownTo"]),

                    };
                    TransferList.Add(objItemIssue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }


    public int Delete(int RefNo,int BranchId)
    {

        return new ItemIssueDAL().Delete(RefNo,BranchId);
    }

    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int RefNo, DataTable dt, int UserNo, int BranchTo)
    {

        return new ItemIssueDAL().InsertUpdate(Date,GodownId,BranchId,RefNo,dt,UserNo,BranchTo);
    }
}