﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EmployeeBLL
/// </summary>
public class EmployeeBLL
{
    public List<Employees> GetAll()
    {
        List<Employees> EmployeesList = new List<Employees>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new EmployeeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employees objEmployees = new Employees()
                    {
                        Code = Convert.ToInt16(dr["Code"]),
                        Name = dr["Name"].ToString(),
                        Address1 = dr["Address1"].ToString(),
                        Address2 = dr["Address2"].ToString(),
                        Address3 = dr["Address3"].ToString(),
                        ContactNo = dr["ContactNo"].ToString(),
                        ContactPerson = dr["ContactPerson"].ToString(),
                        Auth = Convert.ToBoolean(dr["Auth"].ToString()),
                       

                    };
                    EmployeesList.Add(objEmployees);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return EmployeesList;

    }


}