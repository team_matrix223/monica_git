﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for AccGroupsBLL
/// </summary>
public class AccGroupsBLL
{
    public List<AccGroups> GetAll()
    {
        List<AccGroups> AccGroupsList = new List<AccGroups>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccGroupsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccGroups objAccGroups = new AccGroups()
                    {
                        S_NAME = dr["S_NAME"].ToString(),
                        H_CODE = dr["H_CODE"].ToString(),
                        S_Id = Convert.ToInt16(dr["S_Id"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        AMOUNT = Convert.ToDecimal(dr["AMOUNT"]),
                        BAL_INC = Convert.ToString(dr["BAL_INC"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    AccGroupsList.Add(objAccGroups);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return AccGroupsList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AccHeadsDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["S_CODE"].ToString(), dr["S_NAME"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(AccGroups objAccGroups)
    {

        return new AccGroupsDAL().InsertUpdate(objAccGroups);
    }


    public string GetAccGroupsByBALINC(string BALINC)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new AccGroupsDAL().GetByBALINC(BALINC);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["S_CODE"].ToString(), dr["S_NAME"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public Int32 DeleteAccGroup(AccGroups objAccGroup)
    {
        return new AccGroupsDAL().Delete(objAccGroup);
    }

}