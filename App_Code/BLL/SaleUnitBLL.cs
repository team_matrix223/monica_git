﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for SaleUnitBLL
/// </summary>
public class SaleUnitBLL
{

    public Int32 DeleteSaleUnit(SaleUnits objSaleUnit)
    {
        return new SaleUnitDAL().Delete(objSaleUnit);
    }


    public void GetById(SaleUnits objSaleUnit)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new SaleUnitDAL().GetById(objSaleUnit);
            if (dr.HasRows)
            {
                dr.Read();


                objSaleUnit.Unit_Name = dr["Unit_Name"].ToString();
                objSaleUnit.Unit_Id = Convert.ToInt16(dr["Unit_Id"]);
                objSaleUnit.IsActive = Convert.ToBoolean(dr["IsActive"]);


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<SaleUnits> GetAll()
    {
        List<SaleUnits> SaleUnitList = new List<SaleUnits>();

        SqlDataReader dr = null;
        try
        {
            dr = new SaleUnitDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SaleUnits objSaleUnit = new SaleUnits()
                    {
                        Unit_Name = dr["Unit_Name"].ToString(),

                        Unit_Id = Convert.ToInt16(dr["Unit_Id"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    SaleUnitList.Add(objSaleUnit);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return SaleUnitList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SaleUnitDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Unit_Id"].ToString(), dr["Unit_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(SaleUnits objSaleUnit)
    {

        return new SaleUnitDAL().InsertUpdate(objSaleUnit);
    }
}