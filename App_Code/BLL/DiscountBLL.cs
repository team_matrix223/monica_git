﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DiscountBLL
/// </summary>
public class DiscountBLL
{
    public List<DiscountMaster> GetDiscountsByCategory(string Category)
    {
        List<DiscountMaster> DiscountsList = new List<DiscountMaster>();

        SqlDataReader dr = null;
        try
        {
            dr = new DiscountDAL().GetDiscountsByCategory(Category);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DiscountMaster objDiscount = new DiscountMaster()
                    {
                        Discount_ID = Convert.ToInt16(dr["Discount_ID"]),
                        Description = Convert.ToString(dr["Description"]),
                        Start_Date = Convert.ToDateTime(dr["Start_Date"]),
                        End_Date = Convert.ToDateTime(dr["End_Date"]),
                        FULLDAY = Convert.ToBoolean(dr["FULLDAY"]),
                        START_TIME = Convert.ToString(dr["START_TIME"]),
                        END_TIME = Convert.ToString(dr["END_TIME"]),
                        Discount_Amt =0,
                        Discount_Per = Convert.ToDecimal(dr["Discount_Per"]),
                        Category = Convert.ToString(dr["Category"]),
                        Company_ID = Convert.ToInt32(dr["CompanyId"]),
                        Group_ID = Convert.ToInt32(dr["GroupId"]),
                        SGroup_ID = Convert.ToInt32(dr["SubGroupId"]),
                        Item_ID = Convert.ToInt32(dr["ItemId"]),
                    
           
                    };
                    DiscountsList.Add(objDiscount);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DiscountsList;

    }

    public Int32 DeleteDiscount(DiscountMaster objDiscount)
    {
        return new DiscountDAL().Delete(objDiscount);
    }


    public Int16 InsertUpdate(DiscountMaster objDiscount)
    {

        return new DiscountDAL().InsertUpdate(objDiscount);
    }


}