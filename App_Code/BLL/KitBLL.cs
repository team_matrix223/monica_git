﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for KitBLL
/// </summary>
public class KitBLL
{
    public Int32 DeleteKit(Kit objKit)
    {
        return new KitDAL().Delete(objKit);
    }


    public List<Kit> GetAll()
    {
        List<Kit> KitList = new List<Kit>();

        SqlDataReader dr = null;
        try
        {
            dr = new KitDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Kit objKit = new Kit()
                    {
                        Kit_ID = Convert.ToInt16(dr["Kit_ID"]),
                        Master_Code = Convert.ToString(dr["Master_Code"]),
                        Item_Name = Convert.ToString(dr["Item_Name"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        SALE_RATE = Convert.ToDecimal(dr["SALE_RATE"]),
                        MRP = Convert.ToDecimal(dr["MRP"]),
                        UserId = Convert.ToInt16(dr["UserId"])

                    };
                    KitList.Add(objKit);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return KitList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AreasDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Kit_ID"].ToString(), dr["Item_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int32 InsertUpdate(Kit objKit, DataTable dt)
    {
        return new KitDAL().InsertUpdate(objKit, dt);
    }



    public List<KitDetail> GetKitById(Kit objKit)
    {

        SqlDataReader dr = null;
        List<KitDetail> objDetail = new List<KitDetail>();
        try
        {
            dr = new KitDAL().GetByKitID(objKit.Kit_ID);



            if (dr.HasRows)
            {
                dr.Read();

                objKit.Kit_ID = Convert.ToInt32(dr["Kit_ID"].ToString());
                objKit.Master_Code = Convert.ToString(dr["Master_Code"].ToString());
                objKit.Item_Name = Convert.ToString(dr["Item_Name"].ToString());
                objKit.Amount = Convert.ToDecimal(dr["Amount"]);
                objKit.MRP = Convert.ToDecimal(dr["MRP"]);
                objKit.SALE_RATE = Convert.ToDecimal(dr["SALE_RATE"]);
                objKit.UserId = Convert.ToInt32(dr["UserId"]);
               
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    KitDetail objKD = new KitDetail();


                    objKD.Item_ID = Convert.ToInt32(dr["Item_ID"].ToString());
                    objKD.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objKD.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objKD.Qty = Convert.ToDecimal(dr["Qty"]);
                    objKD.MRP = Convert.ToDecimal(dr["MRP"]);
                    objKD.Rate = Convert.ToDecimal(dr["Rate"]);
                    objKD.Amount = Convert.ToDecimal(dr["Amount"]);
                    objKD.Tax_ID = Convert.ToInt32(dr["Tax_ID"]);
                    objKD.Tax_Rate = Convert.ToDecimal(dr["Tax_Rate"]);
                    
                    objDetail.Add(objKD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }
}