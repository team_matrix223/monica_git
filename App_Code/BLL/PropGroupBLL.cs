﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for PropGroupBLL
/// </summary>
public class PropGroupBLL
{

    public Int32 DeletePropGroup(PropGroups objPropGroup)
    {
        return new PropGroupDAL().Delete(objPropGroup);
    }

    public List<PropGroups> GetAll()
    {
        List<PropGroups> GroupList = new List<PropGroups>();

        SqlDataReader dr = null;
        try
        {
            dr = new PropGroupDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PropGroups objPropGroup = new PropGroups()
                    {
                        Group_Name = dr["Group_Name"].ToString(),
                        ImageUrl = dr["ImageUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ShowInMenu = Convert.ToBoolean(dr["ShowInMenu"]),
                        Group_ID = Convert.ToInt16(dr["Group_ID"]),
                        Department_Id = Convert.ToInt32(dr["Department_Id"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    GroupList.Add(objPropGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return GroupList;

    }



    public List<PropGroups> GetByDepartmentId(int DepartmentId)
    {
        List<PropGroups> GroupList = new List<PropGroups>();

        SqlDataReader dr = null;
        try
        {
            dr = new PropGroupDAL().GetByDepartmentId(DepartmentId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PropGroups objPropGroup = new PropGroups()
                    {
                        Group_Name = dr["Group_Name"].ToString(),
                        ImageUrl = dr["ImageUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ShowInMenu = Convert.ToBoolean(dr["ShowInMenu"]),
                        Group_ID = Convert.ToInt16(dr["Group_ID"]),
                        Department_Id = Convert.ToInt32(dr["Department_Id"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    GroupList.Add(objPropGroup);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return GroupList;

    }

    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new PropGroupDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Group_ID"].ToString(), dr["Group_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(PropGroups objPropGroup)
    {

        return new PropGroupDAL().InsertUpdate(objPropGroup);
    }



    public string GetOptionsByDepartmentId(int DepartmentId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new PropGroupDAL().GetByDepartmentId(DepartmentId);
            if (dr.HasRows)
            {
                strBuilder.Append("<option value='0'></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Group_ID"].ToString(), dr["Group_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
}