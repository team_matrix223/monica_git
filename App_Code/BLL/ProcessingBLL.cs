﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for ProcessingBLL
/// </summary>
public class ProcessingBLL
{
    public List<Processing> GetAll(string Department, string Status)
    {
        List<Processing> ProcessingList = new List<Processing>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProcessingDAL().GetByDepartment(Department, Status);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Processing objProcessings = new Processing()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ProcessingId = Convert.ToInt64(dr["ProcessingId"].ToString()),
                        ProcessingTime = dr["ProcessingTime"].ToString(),
                        Status = dr["Status"].ToString(),
                        TableNo = dr["TableNo"].ToString(),
                        BillNowPrefix=dr["BillNowPrefix"].ToString(),
                        IsActive=Convert.ToBoolean(dr["IsActive"]),
                        Remarks=Convert.ToString(dr["Remarks"])

                    };
                    ProcessingList.Add(objProcessings);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ProcessingList;

    }


    public List<Processing> GetForDelivery()
    {
        List<Processing> ProcessingList = new List<Processing>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProcessingDAL().GetForDelivery();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Processing objProcessings = new Processing()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ProcessingId = Convert.ToInt64(dr["ProcessingId"].ToString()),
                        ProcessingTime = dr["ProcessingTime"].ToString(),
                        Status = dr["Status"].ToString(),
                        TableNo = dr["TableNo"].ToString(),
                        RelatedItems=dr["RelatedItems"].ToString(),
                        BillNowPrefix=dr["BillNowPrefix"].ToString(),
                        Remarks = Convert.ToString(dr["Remarks"])
                    };
                    ProcessingList.Add(objProcessings);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ProcessingList;

    }


    public List<Processing> MarkDeliverd(int ProcessingId,string TableNo)
    {
        List<Processing> ProcessingList = new List<Processing>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProcessingDAL().MarkDelivered(ProcessingId, TableNo);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Processing objProcessings = new Processing()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ProcessingId = Convert.ToInt64(dr["ProcessingId"].ToString()),
                        ProcessingTime = dr["ProcessingTime"].ToString(),
                        Status = dr["Status"].ToString(),
                        TableNo = dr["TableNo"].ToString(),
                        RelatedItems = dr["RelatedItems"].ToString(),
                        BillNowPrefix=dr["BillNowPrefix"].ToString()
                    };
                    ProcessingList.Add(objProcessings);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ProcessingList;

    }



    public void UpdateProcessingStatus(string Status, Int32 ProcessingId)
    {
        new ProcessingDAL().UpdateProcessingStatus(Status,ProcessingId);
    }





    public List<Processing> GetNewProcessingItems(DataTable dt, string DepartmentName)
    {
        List<Processing> ProcessingList = new List<Processing>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ProcessingDAL().GetNewProcessingItems(dt,DepartmentName);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Processing objProcessings = new Processing()
                    {
                        Item_Code = dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),
                        ProcessingId = Convert.ToInt64(dr["ProcessingId"].ToString()),
                        ProcessingTime = dr["ProcessingTime"].ToString(),
                        Status = dr["Status"].ToString(),
                        TableNo = dr["TableNo"].ToString(),
                        BillNowPrefix=dr["BillNowPrefix"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                         Remarks=Convert.ToString(dr["Remarks"])
                    };
                    ProcessingList.Add(objProcessings);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ProcessingList;

    }



}