﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for StockAdjustmentBLL
/// </summary>
public class StockAdjustmentBLL
{


    public List<StockAdj> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<StockAdj> StockList = new List<StockAdj>();
        SqlDataReader dr = new StcokAdjustmentDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    StockAdj objStock = new StockAdj()
                    {
                        Ref_No = Convert.ToInt32(dr["Ref_No"]),
                        Ref_Date = Convert.ToDateTime(dr["Ref_Date"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        Godown = dr["GodownName"].ToString(),
                        Amount = Convert.ToDecimal(dr["Amount"]),

                    };
                    StockList.Add(objStock);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return StockList;

    }


    public int InsertUpdate(StockAdj objStock, DataTable dt)
    {

        return new StcokAdjustmentDAL().InsertUpdate(objStock,dt);
    }
}