﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for ColorBLL
/// </summary>
public class ColorBLL
{
    public Int32 DeleteColor(Colors objColor)
    {
        return new ColorDAL().Delete(objColor);
    }


    public void GetById(Colors objColor)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new ColorDAL().GetById(objColor);
            if (dr.HasRows)
            {
                dr.Read();


                objColor.Color_Name = dr["Color_Name"].ToString();
                objColor.Color_ID = Convert.ToInt16(dr["Color_ID"]);
                objColor.UserId = Convert.ToInt16(dr["UserId"]);
                objColor.IsActive = Convert.ToBoolean(dr["IsActive"]);



            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }




    public List<Colors> GetAll()
    {
        List<Colors> ColorList = new List<Colors>();

        SqlDataReader dr = null;
        try
        {
            dr = new ColorDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Colors objColors = new Colors()
                    {
                        Color_Name = dr["Color_Name"].ToString(),

                        Color_ID = Convert.ToInt16(dr["Color_ID"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    ColorList.Add(objColors);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ColorList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AreasDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Color_ID"].ToString(), dr["Color_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Colors objColor)
    {

        return new ColorDAL().InsertUpdate(objColor);
    }

}