﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;


/// <summary>
/// Summary description for UpdateSaleRateBLL
/// </summary>
public class UpdateSaleRateBLL
{
    public string GetProductByDept(int Dept, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new UpdateSaleRateDAL().GetProductByDept(Dept);
        if (dr.HasRows)
        {


            str.Append("   <tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Sale_Rate</th><th>Sale_Rate2</th><th style='display:none'>Tax</th><th>Sale_Rate3</th><th>Sale_Rate4</th><th></th></tr><tr><td></td></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td style='width:350px' >{1}</td><td style='width:350px' >{2}</td><td style='width:100px;readonly:readonly'><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}'  readonly=readonly style='width:100px'/><td style='width:100px'><input type='text' name='Sale_Rate' id='txtSale_Rate_{5}' value='{4}'  readonly=readonly style='width:100px'/></td><td style='width:100px'><input type='text' name='Sale_Rate2' id='txtSale_Rate2_{5}' value='{6}' style='width:100px'/></td><td style='display:none'><input type='text' name='Tax' id='txttax_{5}' value='{7}' readonly=readonly style='width:100px;diaplay:none;'/></td><td style='width:100px'><input type='text' name='Sale_Rate3' id='txtSale_Rate3_{5}' value='{8}' style='width:100px'/></td><td style='width:100px'><input type='text' name='Sale_Rate4' id='txtSale_Rate4_{5}' value='{9}' style='width:100px'/></td><td style='width:80px'><div style='margin-bottom:5px'  class='btn btn-primary btn-small' onclick='javascript:UpdateRecord({5})' name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"], dr["Sale_Rate2"], dr["Tax"], dr["Sale_Rate3"], dr["Sale_Rate4"]));


            }


            //str.Append("   <tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Sale_Rate</th><th>Sale_Rate2</th><th>Sale_Rate3</th><th>Sale_Rate4</th><th></th></tr><tr><td></td></tr>");
            //while (dr.Read())
            //{
            //    TotalRows = Convert.ToInt16(dr["TotalRows"]);
            //    str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td style='width:350px' >{1}</td><td style='width:350px' >{2}</td><td style='width:100px'><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}' style='width:100px'/><td style='width:100px'><input type='text' name='pSale_Rate' id='txtSale_Rate_{5}' value='{4}' style='width:100px'/></td><td style='width:100px'><input type='text' name='Sale_Rate2' id='txtSale_Rate2_{5}' value='{6}' style='width:100px'/></td><td style='width:100px'><input type='text' name='Sale_Rate3' id='txtSale_Rate3_{5}' value='{7}' style='width:100px'/></td><td style='width:100px'><input type='text' name='Sale_Rate4' id='txtSale_Rate4_{5}' value='{8}' style='width:100px'/></td><td style='width:80px'><div style='margin-bottom:5px'  class='btn btn-primary btn-small' onclick='javascript:UpdateRecord({5})' name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"], dr["Sale_Rate2"], dr["Tax"], dr["Sale_Rate3"], dr["Sale_Rate4"]));


            //}



        }
        return str.ToString();
    }

    public string GetProductByCodeorName(string Type, string Code, string Name, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new UpdateSaleRateDAL().GetProductByCodeorName(Type, Code, Name);
        if (dr.HasRows)
        {

            str.Append("   <tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Sale_Rate</th><th></th></tr><tr><td></td></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td style='width:350px' >{1}</td><td style='width:350px' >{2}</td><td style='width:100px'><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}' style='width:100px'/><td style='width:100px'><input type='text' name='pprice' id='txtSale_Rate_{5}' value='{4}' style='width:100px'/></td><td style='width:80px'><div style='margin-bottom:5px'  class='btn btn-primary btn-small' onclick='javascript:UpdateRecord({5})' name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"]));


            }


        }
        return str.ToString();
    }


    public Int32 DeleteDepartment(Departments objDepartment)
    {
        return new UpdateSaleRateDAL().Delete(objDepartment);
    }

    public void GetById(Departments objDepartments)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new UpdateSaleRateDAL().GetById(objDepartments);
            if (dr.HasRows)
            {
                dr.Read();


                objDepartments.Prop_Name = dr["Prop_Name"].ToString();
                objDepartments.Prop_ID = Convert.ToInt16(dr["Prop_ID"]);
                objDepartments.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objDepartments.UserId = Convert.ToInt32(dr["UserId"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<Departments> GetAll()
    {
        List<Departments> DepartmentsList = new List<Departments>();

        SqlDataReader dr = null;
        try
        {
            dr = new UpdateSaleRateDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Departments objDeaprtment = new Departments()
                    {
                        Prop_Name = dr["Prop_Name"].ToString(),

                        Prop_ID = Convert.ToInt16(dr["Prop_ID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    DepartmentsList.Add(objDeaprtment);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DepartmentsList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new UpdateSaleRateDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["PROP_ID"].ToString(), dr["PROP_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Departments objDepartment)
    {

        return new UpdateSaleRateDAL().InsertUpdate(objDepartment);
    }
}