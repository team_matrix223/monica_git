﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InternalIssueBLL
/// </summary>
public class InternalIssueBLL
{
    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int RefNo, DataTable dt, int UserNo,int Department,String Remarks)
    {

        return new InternalIssueDAL().InsertUpdate(Date, GodownId, BranchId, RefNo, dt, UserNo, Department, Remarks);
    }

    public List<InternalIssue> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<InternalIssue> TransferList = new List<InternalIssue>();
        SqlDataReader dr = new InternalIssueDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    InternalIssue objDailyTransfer = new InternalIssue()
                    {
                        ISSUENO = Convert.ToInt32(dr["ISSUENO"]),
                        ISSUEDATE = Convert.ToString(dr["ISSUEDATE"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        Godown = dr["GodownName"].ToString(),
                        DepartmentName = dr["DepartmentName"].ToString(),
                        REMARKS = dr["REMARKS"].ToString(),
                        PASSING = Convert.ToBoolean(dr["PASSING"]),


                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }

    public List<InternalIssue> GetByRefNo(int RefNo, int BranchId)
    {
        List<InternalIssue> TransferList = new List<InternalIssue>();
        SqlDataReader dr = new InternalIssueDAL().GetByRefNo(RefNo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    InternalIssue objDailyTransfer = new InternalIssue()
                    {
                        ISSUENO = Convert.ToInt32(dr["ISSUENO"]),
                        ISSUEDATE = Convert.ToString(dr["ISSUEDATE"]),
                        DEPARTMENT = Convert.ToInt32(dr["DEPARTMENT"]),
                        REMARKS = dr["REMARKS"].ToString(),
                        MASTER_CODE = dr["MASTER_CODE"].ToString(),
                        ITEM_NAME = dr["ITEM_NAME"].ToString(),
                        QTYISSUED = Convert.ToDecimal(dr["QTYISSUED"]),
                        RATE = Convert.ToDecimal(dr["RATE"]),
                        AMOUNT = Convert.ToDecimal(dr["AMOUNT"]),
                        UNIT = dr["UNIT"].ToString(),
                        PASSING = Convert.ToBoolean(dr["PASSING"]),
                        Qty_To_Less = Convert.ToDecimal(dr["Qty_To_Less"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        UserNo = Convert.ToInt32(dr["UserNo"]),
                        Item_Code = dr["Item_Code"].ToString(),
                        STOCK = Convert.ToDecimal(dr["STOCK"]), 
                      


                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }

    public int Delete(int RefNo, int BranchId)
    {

        return new InternalIssueDAL().Delete(RefNo, BranchId);
    }

}