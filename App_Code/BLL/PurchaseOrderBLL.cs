﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
/// <summary>
/// Summary description for AttributesBLL
/// </summary>
public class PurchaseOrderBLL
{


    public List<PurchaseOrder> GetAll()
    {
        List<PurchaseOrder> PurchaseOrderList = new List<PurchaseOrder>();
        SqlDataReader dr = new PurchaseOrderDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    PurchaseOrder objPurchaseOrder = new PurchaseOrder()
                    {
                        OrderNo = Convert.ToInt32(dr["Order_No"]),
                        OrderDate = Convert.ToDateTime(dr["Order_Date"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        IsLocal = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        BillValue = Convert.ToDecimal(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        TotalAmount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]),
                        NetAmount = Convert.ToDecimal(dr["Net_Amount"]),
                
                        SupplierId = Convert.ToInt32(dr["Supplier_Id"]),
                        GodownId = Convert.ToInt32(dr["Godown_Id"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"])




                    };
                    PurchaseOrderList.Add(objPurchaseOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return PurchaseOrderList;

    }

    public List<PurchaseOrder> GetByDate(DateTime DateFrom, DateTime DateTo,int BranchId)
    {
        List<PurchaseOrder> PurchaseOrderList = new List<PurchaseOrder>();
        SqlDataReader dr = new PurchaseOrderDAL().GetByDate(DateFrom, DateTo,BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    PurchaseOrder objPurchaseOrder = new PurchaseOrder()
                    {
                        OrderNo = Convert.ToInt32(dr["Order_No"]),
                        OrderDate = Convert.ToDateTime(dr["Order_Date"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        IsLocal = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        BillValue = Convert.ToDecimal(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        TotalAmount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]),
                        NetAmount = Convert.ToDecimal(dr["Net_Amount"]),

                        SupplierId = Convert.ToInt32(dr["Supplier_Id"]),
                        GodownId = Convert.ToInt32(dr["Godown_Id"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"]),
                        SupplierName = Convert.ToString(dr["SupplierName"])




                    };
                    PurchaseOrderList.Add(objPurchaseOrder);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return PurchaseOrderList;

    }

 

    public string GetPurchaseOrderDetailOptions(int PurchaseOrderID,out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<PurchaseOrderDetail> PurchaseOrderDetailList = new List<PurchaseOrderDetail>();
        SqlDataReader dr = new PurchaseOrderDAL().GetPurchaseOrderDetails(PurchaseOrderID);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {
 
            if (dr.HasRows)
            {
                
                while (dr.Read())
                {
                    counterId += 1;



                    strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:70px'/></td>" +
              "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:140px'>" + "<option pid='" + dr["Item_Code"].ToString() + "' value='" + dr["Item_Code"].ToString() + "' >" + dr["Item_Name"].ToString() + "</option>" + "</select></td>" +
             "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:35px'   value='" + dr["Qty"].ToString() + "' /></td>" +
             "<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtFree' style='width:30px'   value='" + dr["Free"].ToString() + "' /></td>" +
              "<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:55px'   value='" + dr["Rate"].ToString() + "' /></td>" +

             "<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:55px'   value='" + dr["S_Rate"].ToString() + "' /></td>" +
             "<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:55px'   value='" + dr["MRP"].ToString() + "' /></td>" +

            "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:55px'   value='" + dr["Amount"].ToString() + "' /></td>" +
            "<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:40px'   value='" + dr["Dis1P"].ToString() + "'  /></td>" +
            "<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:40px'   value='" + dr["Dis2P"].ToString() + "'  /></td>" +
            "<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:40px'   value='" + dr["TaxP"].ToString() + "' /></td>" +
            "<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>"+
            "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>");


            



   //                 strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'   onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' style='width:70px' value='" + dr[0].ToString() + "'/></td>" +
   //  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'  onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:140px'>" + "<option>" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
   // "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");'  counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px' value='" + dr[0].ToString() + "'/></td>" +
   // "<td><input type='text' id='txtRate" + counterId + "'  counter='" + counterId + "' readonly=readonly class='form-control input-small' name='txtRate' style='width:80px' value='" + dr[0].ToString() + "'/></td>" +
   //"<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "' readonly=readonly class='form-control input-small' name='txtAmount' style='width:80px' value='" + dr[0].ToString() + "'/><input type='text' id='txtCost" + counterId + "' readonly=readonly class='form-control input-small' name='txtCost' style='width:80px;display:none' value='" + dr[0].ToString() + "'/></td>");
   //                 strBuilder.Append("<td><table cellpadding='0'  border='0'><tr><td style='border:0px;margin:0px;padding:0px'><input type='text'  onkeyup='javascript:DiscountTextChange(" + counterId + ");' id='txtDiscountItem" + counterId + "' name='txtDiscountItem'   counter='" + counterId + "'  class='form-control input-small'  value='" + dr[0].ToString() + "'   style='width:50px;'  /></td><td style='border:0px;margin:0px;padding:0px'>-</td><td  style='border:0px;margin:0px;padding:0px'>	 <input type='text' id='txtDisAmountItem" + counterId + "'  onkeyup='javascript:DisAmountItemTextChange(" + counterId + ");' name='txtDisAmountItem'   counter='" + counterId + "' value='" + dr[0].ToString() + "' class='form-control input-small'    style='width:50px;'  /></td></tr></table>");




   //                 strBuilder.Append("<td><div id='btnAddRow" + counterId + "'   onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>");
   //                 strBuilder.Append("<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>");




                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }


    public string GetPurchaseOrderImport(PurchaseOrder objPurchaseOrder, out int cntr)
    {




        StringBuilder strBuilder = new StringBuilder();
        List<PurchaseOrderDetail> PurchaseOrderDetailList = new List<PurchaseOrderDetail>();
        SqlDataReader dr = new PurchaseOrderDAL().GetPurchaseOrderImport(objPurchaseOrder.OrderNo);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {

            if (dr.HasRows)
            {
                dr.Read();
                objPurchaseOrder.OrderNo = Convert.ToInt32(dr["Order_No"]);
                objPurchaseOrder.OrderDate = Convert.ToDateTime(dr["Order_Date"]);
                objPurchaseOrder.Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]);
                objPurchaseOrder.Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]);
                objPurchaseOrder.Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]);
                objPurchaseOrder.IsLocal = Convert.ToString(dr["LocalOut"]);
                objPurchaseOrder.TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]);
                objPurchaseOrder.TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]);
                objPurchaseOrder.Remarks = Convert.ToString(dr["Remarks"]);
                objPurchaseOrder.Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]);
                objPurchaseOrder.Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]);
                objPurchaseOrder.Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]);
                objPurchaseOrder.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                objPurchaseOrder.Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]);
                objPurchaseOrder.TaxP = Convert.ToDecimal(dr["TaxP"]);
                objPurchaseOrder.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                objPurchaseOrder.TotalAmount = Convert.ToDecimal(dr["Total_Amount"]);
                objPurchaseOrder.ODisP = Convert.ToDecimal(dr["ODisP"]);
                objPurchaseOrder.ODisAmt = Convert.ToDecimal(dr["ODisAmt"]);
                objPurchaseOrder.DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]);
                objPurchaseOrder.Adjustment = Convert.ToDecimal(dr["Adjustment"]);
                objPurchaseOrder.NetAmount = Convert.ToDecimal(dr["Net_Amount"]);
                objPurchaseOrder.SupplierId = Convert.ToInt32(dr["Supplier_ID"]);
                objPurchaseOrder.GodownId = Convert.ToInt32(dr["Godown_ID"]);
                objPurchaseOrder.BillValue = Convert.ToDecimal(dr["Bill_Value"]);

            }

            dr.NextResult();
            if (dr.HasRows)
            {
                
                while (dr.Read())
                {
                    counterId += 1;



                    strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:50px'/></td>" +
"<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:120px'>" + "<option pid='" + dr["Item_Code"].ToString() + "' taxid = '" + dr["Tax_Id"].ToString() + "' Tax = '" + dr["TaxP"].ToString() + "' value='" + dr["Item_Code"].ToString() + "'  Net='" + dr["Net"].ToString() + "' SurValue='" + dr["SurValue"].ToString() + "' >" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
"<td><input type='text' id='txtMargin" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtMargin' style='width:50px' value = '" + dr["Item_Margin"].ToString() + "'/></td>" +
"<td><input type='text' id='txtBalQty" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtBalQty' style='width:50px' value ='" + dr["Item_Code"].ToString() + "'/></td>" +
"<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px'   value='" + dr["Qty"].ToString() + "' /></td>" +
"<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtFree' style='width:50px'   value='" + dr["Free"].ToString() + "' /></td>" +
"<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:50px'   value='" + dr["Rate"].ToString() + "' /></td>" +

"<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:50px'   value='" + dr["S_Rate"].ToString() + "' /></td>" +
"<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:50px'   value='" + dr["MRP"].ToString() + "' /></td>" +

"<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:50px'   value='" + dr["Amount"].ToString() + "' /></td>" +
"<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:50px'   value='" + dr["Dis1P"].ToString() + "'  /></td>" +
"<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:50px'   value='" + dr["Dis2P"].ToString() + "'  /></td>" +
"<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:50px'   value='" + dr["TaxP"].ToString() + "' /></td>" +
"<td style='display:none'><input type='text' id='txtExcisePer" + counterId + "' onkeyup ='javascript:ExciseChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtExcisePer' style='width:50px' value ='0' /></td>" +
"<td style='display:none'><input type='text' id='txtExciseAmt" + counterId + "'   counter='" + counterId + "'   class='form-control input-small' name='txtExciseAmt' style='width:50px' value = '0' /></td>" +
"<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
"<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");







          //          strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:70px'/></td>" +
          //  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:140px'>" + "<option pid='" + dr["Item_Code"].ToString() + "' value='" + dr["Item_Code"].ToString() + "'  Net='" + dr["Net"].ToString() + "' SurValue='" + dr["SurValue"].ToString() + "' >" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
          //  "<td><input type='text' id='txtMargin" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtMargin' style='width:60px' value = '" + dr["Item_Margin"].ToString() + "'/></td>" +
          //  "<td><input type='text' id='txtBalQty" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtBalQty' style='width:60px' value ='" + dr["Item_Code"].ToString() + "'/></td>" +
          // "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px'   value='" + dr["Qty"].ToString() + "' /></td>" +
          // "<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtFree' style='width:60px'   value='" + dr["Free"].ToString() + "' /></td>" +
          //  "<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:70px'   value='" + dr["Rate"].ToString() + "' /></td>" +

          // "<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:70px'   value='" + dr["S_Rate"].ToString() + "' /></td>" +
          // "<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:70px'   value='" + dr["MRP"].ToString() + "' /></td>" +

          //"<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:70px'   value='" + dr["Amount"].ToString() + "' /></td>" +
          //"<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:60px'   value='" + dr["Dis1P"].ToString() + "'  /></td>" +
          //"<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:60px'   value='" + dr["Dis2P"].ToString() + "'  /></td>" +
          //"<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:60px'   value='" + dr["TaxP"].ToString() + "' /></td>" +
          // "<td><input type='text' id='txtExcisePer" + counterId + "' onkeyup ='javascript:ExciseChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtExcisePer' style='width:60px' value ='0' /></td>" +
          //"<td><input type='text' id='txtExciseAmt" + counterId + "'   counter='" + counterId + "'   class='form-control input-small' name='txtExciseAmt' style='width:70px' value = '0' /></td>" +
          //"<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>" +
          //"<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>");







                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }

    public string GetBillOrderImport(PurchaseOrder objPurchaseOrder, string BNF, out int cntr)
    {




        StringBuilder strBuilder = new StringBuilder();
        List<PurchaseOrderDetail> PurchaseOrderDetailList = new List<PurchaseOrderDetail>();
        SqlDataReader dr = new PurchaseOrderDAL().GetBillImport(BNF);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {

            if (dr.HasRows)
            {
                dr.Read();
                objPurchaseOrder.OrderNo = Convert.ToInt32(dr["Bill_No"]);
                objPurchaseOrder.OrderDate = Convert.ToDateTime(dr["Bill_Date"]);
                //objPurchaseOrder.Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]);
                //objPurchaseOrder.Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]);
                //objPurchaseOrder.Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]);
                objPurchaseOrder.IsLocal = Convert.ToString(dr["LocalOut"]);
                //objPurchaseOrder.TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]);
                //objPurchaseOrder.TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]);
                //objPurchaseOrder.Remarks = Convert.ToString(dr["Remarks"]);
                //objPurchaseOrder.Dis1Amt = Convert.ToDecimal(dr["Dis1Amt"]);
                //objPurchaseOrder.Dis2Amt = Convert.ToDecimal(dr["Dis2Amt"]);
                //objPurchaseOrder.Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]);
                //objPurchaseOrder.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                //objPurchaseOrder.Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]);
                //objPurchaseOrder.TaxP = Convert.ToDecimal(dr["TaxP"]);
                //objPurchaseOrder.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                //objPurchaseOrder.TotalAmount = Convert.ToDecimal(dr["Total_Amount"]);
                //objPurchaseOrder.ODisP = Convert.ToDecimal(dr["ODisP"]);
                //objPurchaseOrder.ODisAmt = Convert.ToDecimal(dr["ODisAmt"]);
                //objPurchaseOrder.DisplayAmount = Convert.ToDecimal(dr["Display_Amount"]);
                //objPurchaseOrder.Adjustment = Convert.ToDecimal(dr["Adjustment"]);
                //objPurchaseOrder.NetAmount = Convert.ToDecimal(dr["Net_Amount"]);
                //objPurchaseOrder.SupplierId = Convert.ToInt32(dr["Supplier_ID"]);
                //objPurchaseOrder.GodownId = Convert.ToInt32(dr["Godown_ID"]);
                //objPurchaseOrder.BillValue = Convert.ToDecimal(dr["Bill_Value"]);

            }

            dr.NextResult();
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    counterId += 1;



                    strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:50px'/></td>" +
"<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:120px'>" + "<option pid='" + dr["Item_Code"].ToString() + "' taxid = '" + dr["Tax_Id"].ToString() + "' Tax = '" + dr["Tax"].ToString() + "' value='" + dr["Item_Code"].ToString() + "'  Net='" + dr["Net"].ToString() + "' SurValue='" + dr["SurValue"].ToString() + "' >" + dr["ItemName"].ToString() + "</option>" + "</select></td>" +
"<td><input type='text' id='txtMargin" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtMargin' style='width:50px' value = '" + dr["Item_Margin"].ToString() + "'/></td>" +
"<td><input type='text' id='txtBalQty" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtBalQty' style='width:50px' value ='" + dr["Item_Code"].ToString() + "'/></td>" +
"<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px'   value='" + dr["Qty"].ToString() + "' /></td>" +
"<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtFree' style='width:50px'   value='0' /></td>" +
"<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:50px'   value='" + dr["Rate"].ToString() + "' /></td>" +

"<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:50px'   value='" + dr["Rate"].ToString() + "' /></td>" +
"<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:50px'   value='" + dr["MRP"].ToString() + "' /></td>" +

"<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:50px'   value='" + dr["Amount"].ToString() + "' /></td>" +
"<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:50px'   value='" + dr["Dis_Per"].ToString() + "'  /></td>" +
"<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:50px'   value='" + dr["Dis_Per"].ToString() + "'  /></td>" +
"<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:50px'   value='" + dr["Tax"].ToString() + "' /></td>" +
"<td  style='display:none'><input type='text' id='txtExcisePer" + counterId + "' onkeyup ='javascript:ExciseChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtExcisePer' style='width:50px' value ='0' /></td>" +
"<td  style='display:none'><input type='text' id='txtExciseAmt" + counterId + "'   counter='" + counterId + "'   class='form-control input-small' name='txtExciseAmt' style='width:50px' value = '0' /></td>" +
"<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
"<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");


                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }


    

    public int InsertUpdate(PurchaseOrder objPurchaseOrder, DataTable dt)
    {

        return new PurchaseOrderDAL().InsertUpdate(objPurchaseOrder, dt);
    }
}