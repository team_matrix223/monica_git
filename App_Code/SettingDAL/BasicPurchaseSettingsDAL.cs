﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BasicPurchaseSettingsDAL
/// </summary>
public class BasicPurchaseSettingsDAL:Connection
{
    public SqlDataReader GetMasterSettings(BasicPurchaseSetting objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objSetting.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMasterPurchaseSetting", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(BasicPurchaseSetting objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];

        objParam[0] = new SqlParameter("@Check_MRP", objSettings.Check_MRP);
        objParam[1] = new SqlParameter("@Update_SaleRatePur", objSettings.Update_SaleRatePur);
        objParam[2] = new SqlParameter("@Update_MRPPur", objSettings.Update_MRPPur);
        objParam[3] = new SqlParameter("@Show_MarginPur", objSettings.Show_MarginPur);
        objParam[4] = new SqlParameter("@Show_SaleItemInPurOrder", objSettings.Show_SaleItemInPurOrder);
        objParam[5] = new SqlParameter("@ReOrderLevel", objSettings.ReOrderLevel);
        objParam[6] = new SqlParameter("@ReOrderLevelCal", objSettings.ReOrderLevelCal);
        objParam[7] = new SqlParameter("@Defaultgodown", objSettings.Defaultgodown);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[10] = new SqlParameter("@BranchId", objSettings.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersettingPurchase", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}