﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for PurchaseSettingDAL
/// </summary>
public class PurchaseSettingDAL:Connection
{
    public SqlDataReader GetPurchaseSettings(PurchaseReceiptSettings objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[0];

      

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetPurchaseSettings", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdatePurchaseSettings(PurchaseReceiptSettings objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@Enable_Excise", objSettings.Enable_Excise);
        objParam[1] = new SqlParameter("@UPDATE_SALERATE", objSettings.UPDATE_SALERATE);
        objParam[2] = new SqlParameter("@Margine", objSettings.Margine);
        objParam[3] = new SqlParameter("@ShowSaleOnly", objSettings.ShowSaleOnly);
        objParam[4] = new SqlParameter("@RpCode", objSettings.RpCode);
        objParam[5] = new SqlParameter("@UPdate_mrp", objSettings.UPdate_mrp);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@UserId", objSettings.UserId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertPurchaseReciptSettings", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}