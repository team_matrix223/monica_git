﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for DiscountSettingDAL
/// </summary>
public class DiscountSettingDAL:Connection
{
    public SqlDataReader GetMasterSettingsByType(DiscountSetting objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Type", objSetting.Type);

        objParam[1] = new SqlParameter("@BranchId", objSetting.BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBillDiscount", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(DiscountSetting objSettings,DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];

        objParam[0] = new SqlParameter("@Allow_Dis_on_Billing", objSettings.Allow_Dis_on_Billing);
        objParam[1] = new SqlParameter("@Enable_Dis_Col", objSettings.Enable_Dis_Col);
        objParam[2] = new SqlParameter("@Dis_Bill_Value", objSettings.Dis_Bill_Value);
        objParam[3] = new SqlParameter("@Back_End_Discount", objSettings.Back_End_Discount);
        objParam[4] = new SqlParameter("@Enable_Dis_Amt", objSettings.Enable_Dis_Amt);
        objParam[5] = new SqlParameter("@Enable_Cust_Dis", objSettings.Enable_Cust_Dis);
        objParam[6] = new SqlParameter("@Type", objSettings.Type);
       
        objParam[7] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.ReturnValue;
       
        objParam[8] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[9] = new SqlParameter("@BranchId", objSettings.BranchId);
        objParam[10] = new SqlParameter("@dt", dt);

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "setting_sp_MasterSettingBillDiscount", objParam);
            retValue = Convert.ToInt16(objParam[7].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}