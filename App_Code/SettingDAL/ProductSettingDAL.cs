﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProductSettingDAL
/// </summary>
public class ProductSettingDAL:Connection
{
    public SqlDataReader GetMasterSettings(ProductSetting objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objSetting.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingProduct", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(ProductSetting objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];

        objParam[0] = new SqlParameter("@Auto_GenCode", objSettings.Auto_GenCode);
        objParam[1] = new SqlParameter("@Item_CodeLen", objSettings.Item_CodeLen);
        objParam[2] = new SqlParameter("@Item_CodeStart", objSettings.Item_CodeStart);
        objParam[3] = new SqlParameter("@Alphabet_Code", objSettings.Alphabet_Code);
        objParam[4] = new SqlParameter("@Comp_WiseDis", objSettings.Comp_WiseDis);
        objParam[5] = new SqlParameter("@Batch_No", objSettings.Batch_No);
        objParam[6] = new SqlParameter("@ItemDup", objSettings.ItemDup);
        objParam[7] = new SqlParameter("@ItemCompDup", objSettings.ItemCompDup);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[10] = new SqlParameter("@BranchId", objSettings.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersetting_Product", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}