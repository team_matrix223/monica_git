﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BasicBillSettingsDAL
/// </summary>
public class BasicBillSettingsDAL:Connection
{
    public SqlDataReader GetMasterSettings(BasicBillSettings objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objSetting.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBasicBill", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(BasicBillSettings objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[27];

        objParam[0] = new SqlParameter("@CashCustomer", objSettings.CashCustomer);
        objParam[1] = new SqlParameter("@StartCustomerPoint", objSettings.StartCustomerPoint);
        objParam[2] = new SqlParameter("@defaultpaymodeID", objSettings.defaultpaymodeID);
        objParam[3] = new SqlParameter("@defaultpaymode", objSettings.defaultpaymode);
        objParam[4] = new SqlParameter("@defaultBankID", objSettings.defaultBankID);
        objParam[5] = new SqlParameter("@defaultbankName", objSettings.defaultbankName);
        objParam[6] = new SqlParameter("@roundamt", objSettings.roundamt);
        objParam[7] = new SqlParameter("@showrate", objSettings.showrate);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@tracinguser", objSettings.tracinguser);
        objParam[10] = new SqlParameter("@itemname", objSettings.itemname);
        objParam[11] = new SqlParameter("@shortname", objSettings.shortname);
        objParam[12] = new SqlParameter("@headerduplicate", objSettings.headerduplicate);
        objParam[13] = new SqlParameter("@focaffect", objSettings.focaffect);
        objParam[14] = new SqlParameter("@stock", objSettings.stock);
        objParam[15] = new SqlParameter("@KitStock", objSettings.KitStock);
        objParam[16] = new SqlParameter("@barcode", objSettings.barcode);
        objParam[17] = new SqlParameter("@holdsys", objSettings.holdsys);
        objParam[18] = new SqlParameter("@Tenderwindow", objSettings.Tenderwindow);
        objParam[19] = new SqlParameter("@Tax2onRetail", objSettings.Tax2onRetail);
        objParam[20] = new SqlParameter("@Tax2onVAT", objSettings.Tax2onVAT);
        objParam[21] = new SqlParameter("@Tax2onCST", objSettings.Tax2onCST);
        objParam[22] = new SqlParameter("@KotSystem", objSettings.KotSystem);
        objParam[23] = new SqlParameter("@BoxSystem", objSettings.BoxSystem);
        objParam[24] = new SqlParameter("@NegtiveStock", objSettings.NegtiveStock);
        objParam[25] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[26] = new SqlParameter("@BranchId", objSettings.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersettingBasicBill", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}