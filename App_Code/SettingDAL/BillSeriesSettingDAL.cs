﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillSeriesSettingDAL
/// </summary>
public class BillSeriesSettingDAL:Connection
{
    public SqlDataReader GetMasterSettingsByType(string Type,int Branch)
    {
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Type",Type);
        objParam[1] = new SqlParameter("@BranchId", Branch);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBillSeries", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(BillSeriesSetting objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[13];

        objParam[0] = new SqlParameter("@BranchId", objSettings.BranchId);
        objParam[1] = new SqlParameter("@BranchName", objSettings.BranchName);
        objParam[2] = new SqlParameter("@Prefix", objSettings.Prefix);
        objParam[3] = new SqlParameter("@CashSeries", objSettings.CashSeries);
        objParam[4] = new SqlParameter("@CreditSeries", objSettings.CreditSeries);
        objParam[5] = new SqlParameter("@CrCardSeries", objSettings.CrCardSeries);
        objParam[6] = new SqlParameter("@OpenClose", objSettings.OpenClose);
        objParam[7] = new SqlParameter("@DefaultGodown", objSettings.DefaultGodown);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@BillDate", objSettings.BillDate);
        objParam[10] = new SqlParameter("@Type", objSettings.Type);
        objParam[11] = new SqlParameter("@Series_Name", objSettings.Series_Name);
        objParam[12] = new SqlParameter("@LUser", objSettings.LUser);
      
     
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersettingBillSeries", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}