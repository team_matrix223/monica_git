﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillGridOptionsDAL
/// </summary>
public class BillGridOptionsDAL:Connection
{
    public SqlDataReader GetMasterSettingsByType(BillGridOptions objSetting)
    {
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Type", objSetting.Type);
        objParam[1] = new SqlParameter("@BranchId", objSetting.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetMastersettingBillGridOptions", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateBasicSettings(BillGridOptions objSettings)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[22];

        objParam[0] = new SqlParameter("@MRP_Enable", objSettings.MRP_Enable);
        objParam[1] = new SqlParameter("@SRate_Enable", objSettings.SRate_Enable);
        objParam[2] = new SqlParameter("@Dis1_Per", objSettings.Dis1_Per);
        objParam[3] = new SqlParameter("@Dis1_Amt", objSettings.Dis1_Amt);
        objParam[4] = new SqlParameter("@Dis2_Per", objSettings.Dis2_Per);
        objParam[5] = new SqlParameter("@Dis2_Amt", objSettings.Dis2_Amt);
        objParam[6] = new SqlParameter("@Tax1_Per", objSettings.Tax1_Per);
        objParam[7] = new SqlParameter("@Tax1_Amt", objSettings.Tax1_Amt);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@Tax1_Sur1_Per", objSettings.Tax1_Sur1_Per);
        objParam[10] = new SqlParameter("@Tax1_Sur1_Amt", objSettings.Tax1_Sur1_Amt);
        objParam[11] = new SqlParameter("@Tax1_Sur2_Per", objSettings.Tax1_Sur2_Per);
        objParam[12] = new SqlParameter("@Tax1_Sur2_Amt", objSettings.Tax1_Sur2_Amt);
        objParam[13] = new SqlParameter("@Tax2_Per", objSettings.Tax2_Per);
        objParam[14] = new SqlParameter("@Tax2_Amt", objSettings.Tax2_Amt);
        objParam[15] = new SqlParameter("@Tax2_Sur1_Per", objSettings.Tax2_Sur1_Per);
        objParam[16] = new SqlParameter("@Tax2_Sur1_Amt", objSettings.Tax2_Sur1_Amt);
        objParam[17] = new SqlParameter("@Tax2_Sur2_Per", objSettings.Tax2_Sur2_Per);
        objParam[18] = new SqlParameter("@Tax2_Sur2_Amt", objSettings.Tax2_Sur2_Amt);
        objParam[19] = new SqlParameter("@Type", objSettings.Type);
        objParam[20] = new SqlParameter("@UserId", objSettings.UserId);
        objParam[21] = new SqlParameter("@BranchId", objSettings.BranchId);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_InsertMastersettingBillGridOptions", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}