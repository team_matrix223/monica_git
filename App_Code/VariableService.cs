﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for VariableService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class VariableService : System.Web.Services.WebService {

  

    [WebMethod]
    public string GetById(int Id,string Type) {


        if (Type == "Area")
        {
            Areas objArea = new Areas()
            {
                Area_ID = Id,
            };
            new AreaBLL().GetById(objArea);
            Variable objVariable = new Variable();
            objVariable.Id = objArea.Area_ID;
            objVariable.Title = objArea.Area_Name;
            objVariable.UserId = objArea.UserId;
            objVariable.IsActive = objArea.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "Location")
        {
            Location objLocation = new Location()
            {
                Location_ID = Id,
            };
            new LocationBLL().GetById(objLocation);
            Variable objVariable = new Variable();
            objVariable.Id = objLocation.Location_ID;
            objVariable.Title = objLocation.Location_Name;
            objVariable.UserId = objLocation.UserId;
            objVariable.IsActive = objLocation.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "Color")
        {
            Colors objColor = new Colors()
            {
                Color_ID = Id,
            };
            new ColorBLL().GetById(objColor);
            Variable objVariable = new Variable();
            objVariable.Id = objColor.Color_ID;
            objVariable.Title = objColor.Color_Name;
            objVariable.IsActive = objColor.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "City")
        {
            Cities objCity = new Cities()
            {
                City_ID = Id,
            };
            new CitiesBLL().GetById(objCity);
            Variable objVariable = new Variable();
            objVariable.Id = objCity.City_ID;
            objVariable.Title = objCity.City_Name;
            objVariable.IsActive = objCity.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "HSN")
        {
            HSNCodeEntities objHSN = new HSNCodeEntities()
            {
                Id = Id,
            };
            new HSNBLL().GetById(objHSN);
            Variable objVariable = new Variable();
            objVariable.Id = objHSN.Id;
            objVariable.Title = objHSN.Title;
            objVariable.IsActive = objHSN.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }


        else if (Type == "Department")
        {
            Departments objDepartment = new Departments()
            {
                Prop_ID = Id,
            };
            new DepartmentBLL().GetById(objDepartment);
            Variable objVariable = new Variable();
            objVariable.Id = objDepartment.Prop_ID;
            objVariable.Title = objDepartment.Prop_Name;
            objVariable.IsActive = objDepartment.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "ItemType")
        {
            ItemTypes objItemType = new ItemTypes()
            {
                ItemType_Id = Id,
            };
            new ItemTypeBLL().GetById(objItemType);
            Variable objVariable = new Variable();
            objVariable.Id = objItemType.ItemType_Id;
            objVariable.Title = objItemType.ItemType_Name;
            objVariable.IsActive = objItemType.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "SaleUnit")
        {
            SaleUnits objSaleUnit = new SaleUnits()
            {
                Unit_Id = Id,
            };
            new SaleUnitBLL().GetById(objSaleUnit);
            Variable objVariable = new Variable();
            objVariable.Id = objSaleUnit.Unit_Id;
            objVariable.Title = objSaleUnit.Unit_Name;
            objVariable.IsActive = objSaleUnit.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "State")
        {
            States objState = new States()
            {
                State_ID = Id,
            };
            new StateBLL().GetById(objState);
            Variable objVariable = new Variable();
            objVariable.Id = objState.State_ID;
            objVariable.Title = objState.State_Name;
            objVariable.IsActive = objState.IsActive;


            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        return "-1";
    }

 
    [WebMethod(EnableSession=true)]
    public string InsertUpdate(int Id,string Title,bool IsActive,string Type)
    {
       // var Userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].ToString());
        Int32 Userid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        if (Type == "Area")
        {
            Areas objAreas = new Areas() { Area_ID = Id, Area_Name = Title.ToUpper(),UserId = Userid,IsActive = IsActive};
            new AreaBLL().InsertUpdate(objAreas);
            Variable objVariable = new Variable();
            objVariable.Id = objAreas.Area_ID;
            objVariable.Title = objAreas.Area_Name;
            objVariable.IsActive = objAreas.IsActive;
            var JsonData = new
            {
                Variable = objVariable,
               
            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);
        
        }
        else if (Type == "Location")
        {
            Location objLocations = new Location() { Location_ID = Id, Location_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new LocationBLL().InsertUpdate(objLocations);
            Variable objVariable = new Variable();
            objVariable.Id = objLocations.Location_ID;
            objVariable.Title = objLocations.Location_Name;
            objVariable.IsActive = objLocations.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "Color")
        {
            Colors objColors = new Colors() { Color_ID = Id, Color_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new ColorBLL().InsertUpdate(objColors);
            Variable objVariable = new Variable();
            objVariable.Id = objColors.Color_ID;
            objVariable.Title = objColors.Color_Name;
            objVariable.IsActive = objColors.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        else if (Type == "City")
        {
            Cities objCities = new Cities() { City_ID = Id, City_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new CitiesBLL().InsertUpdate(objCities);
            Variable objVariable = new Variable();
            objVariable.Id = objCities.City_ID;
            objVariable.Title = objCities.City_Name;
            objVariable.IsActive = objCities.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "HSN")
        {
            HSNCodeEntities objHSN = new HSNCodeEntities() { Id = Id, Title = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new HSNBLL().InsertUpdate(objHSN);
            Variable objVariable = new Variable();
            objVariable.Id = objHSN.Id;
            objVariable.Title = objHSN.Title;
            objVariable.IsActive = objHSN.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }





        else if (Type == "Department")
        {
            Departments objDepartment = new Departments() { Prop_ID = Id, Prop_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new DepartmentBLL().InsertUpdate(objDepartment);
            Variable objVariable = new Variable();
            objVariable.Id = objDepartment.Prop_ID;
            objVariable.Title = objDepartment.Prop_Name;
            objVariable.IsActive = objDepartment.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }


        else if (Type == "SaleUnit")
        {
            SaleUnits objSaleUnit = new SaleUnits() { Unit_Id = Id, Unit_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new SaleUnitBLL().InsertUpdate(objSaleUnit);
            Variable objVariable = new Variable();
            objVariable.Id = objSaleUnit.Unit_Id;
            objVariable.Title = objSaleUnit.Unit_Name;
            objVariable.IsActive = objSaleUnit.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "State")
        {
            States objState = new States() { State_ID = Id, State_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new StateBLL().InsertUpdate(objState);
            Variable objVariable = new Variable();
            objVariable.Id = objState.State_ID;
            objVariable.Title = objState.State_Name;
            objVariable.IsActive = objState.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }
        else if (Type == "ItemType")
        {
            ItemTypes objItemType = new ItemTypes() { ItemType_Id = Id, ItemType_Name = Title.ToUpper(), UserId = Userid, IsActive = IsActive };
            new ItemTypeBLL().InsertUpdate(objItemType);
            Variable objVariable = new Variable();
            objVariable.Id = objItemType.ItemType_Id;
            objVariable.Title = objItemType.ItemType_Name;
            objVariable.IsActive = objItemType.IsActive;

            var JsonData = new
            {
                Variable = objVariable,

            };
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(JsonData);

        }

        return "-1";
    }

}
