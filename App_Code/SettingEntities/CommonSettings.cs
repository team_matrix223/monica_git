﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CommonSettings
/// </summary>
public class CommonSettings
{
    public string retail_bill { get; set; }
    public string vat_bill { get; set; }
    public bool homedel_charges { get; set; }
    public decimal min_bill_value { get; set; }
    public decimal del_charges { get; set; }
    public bool CashCustomer { get; set; }
    public bool StartCustomerPoint { get; set; }
    public string defaultpaymodeID { get; set; }
    public string defaultpaymode { get; set; }
    public string defaultBankID { get; set; }
    public string defaultbankName { get; set; }
    public bool roundamt { get; set; }
    public bool showrate { get; set; }
    public bool tracinguser { get; set; }
    public bool itemname { get; set; }
    public bool shortname { get; set; }
    public bool headerduplicate { get; set; }
    public bool focaffect { get; set; }
    public string stock { get; set; }
    public string KitStock { get; set; }
    public bool barcode { get; set; }
    public bool holdsys { get; set; }
    public bool Tenderwindow { get; set; }
    public bool Tax2onRetail { get; set; }
    public bool Tax2onVAT { get; set; }
    public bool Tax2onCST { get; set; }
    public bool KotSystem { get; set; }
    public bool BoxSystem { get; set; }
    public bool NegtiveStock { get; set; }
    public bool Allow_Dis_on_Billing { get; set; }
    public bool Enable_Dis_Col { get; set; }
    public bool Dis_Bill_Value { get; set; }
    public bool Back_End_Discount { get; set; }
    public bool Enable_Dis_Amt { get; set; }
    public bool Enable_Cust_Dis { get; set; }
    public string Type { get; set; }
    public decimal StartValue { get; set; }
    public decimal EndValue { get; set; }
    public decimal DisPer { get; set; }
    public int BranchId { get; set; }
    public Boolean ServiceTax { get; set; }
    public bool AlloServicetax_TakeAway { get; set; }
 
	public CommonSettings()
	{
        retail_bill = "";
        CashCustomer = false;
        StartCustomerPoint = false;
        defaultpaymodeID = "";
        defaultpaymode = "";
        defaultBankID = "";
        defaultbankName = "";
        roundamt = false;
        showrate = false;
        tracinguser = false;
        itemname = false;
        shortname = false;
        headerduplicate = false;
        focaffect = false;
        stock = "";
        KitStock = "";
        barcode = false;
        holdsys = false;
        Tenderwindow = false;
        Tax2onRetail = false;
        Tax2onVAT = false;
        Tax2onCST = false;
        KotSystem = false;
        BoxSystem = false;
        NegtiveStock = false;
        Allow_Dis_on_Billing = false;
        Enable_Dis_Col = false;
        Dis_Bill_Value = false;
        Back_End_Discount = false;
        Enable_Dis_Amt = false;
        Enable_Cust_Dis = false;
        Type = "";
        StartValue = 0;
        EndValue = 0;
        DisPer = 0;
        BranchId = 0;
        ServiceTax = false;
        AlloServicetax_TakeAway = false;
        homedel_charges = false;
        min_bill_value = 0;
        del_charges = 0;
	}
}