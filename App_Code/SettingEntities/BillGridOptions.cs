﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillGridOptions
/// </summary>
public class BillGridOptions
{

    public bool MRP_Enable { get; set; }
    public bool SRate_Enable { get; set; }
    public bool Dis1_Per { get; set; }
    public bool Dis1_Amt { get; set; }
    public bool Dis2_Per { get; set; }
    public bool Dis2_Amt { get; set; }
    public bool Tax1_Per { get; set; }
    public bool Tax1_Amt { get; set; }
    public bool Tax1_Sur1_Per { get; set; }
    public bool Tax1_Sur1_Amt { get; set; }
    public bool Tax1_Sur2_Per { get; set; }
    public bool Tax1_Sur2_Amt { get; set; }
    public bool Tax2_Per { get; set; }
    public bool Tax2_Amt { get; set; }
    public bool Tax2_Sur1_Per { get; set; }
    public bool Tax2_Sur1_Amt { get; set; }
    public bool Tax2_Sur2_Per { get; set; }
    public bool Tax2_Sur2_Amt { get; set; }
    public string Type { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }

	public BillGridOptions()
	{
        MRP_Enable = false;
        SRate_Enable = false;
        Dis1_Per = false;
        Dis1_Amt = false;
        Dis2_Per = false;
        Dis2_Amt = false;
        Tax1_Per = false;
        Tax1_Amt = false;
        Tax1_Sur1_Per = false;
        Tax1_Sur1_Amt = false;
        Tax1_Sur2_Per = false;
        Tax1_Sur2_Amt = false;
        Tax2_Per = false;
        Tax2_Amt = false;
        Tax2_Sur1_Per = false;
        Tax2_Sur1_Amt = false;
        Tax2_Sur2_Per = false;
        Tax2_Sur2_Amt = false;
        Type = "";
        UserId = 0;
        BranchId = 0;
	}
}