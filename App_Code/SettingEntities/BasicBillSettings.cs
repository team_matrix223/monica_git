﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicBillSettings
/// </summary>
public class BasicBillSettings
{

    public bool CashCustomer { get; set; }
    public bool StartCustomerPoint { get; set; }
    public string defaultpaymodeID { get; set; }
    public string defaultpaymode { get; set; }
    public string defaultBankID { get; set; }
    public string defaultbankName { get; set; }
    public bool roundamt { get; set; }
    public bool showrate { get; set; }
    public bool tracinguser { get; set; }
    public bool itemname { get; set; }
    public bool shortname { get; set; }
    public bool headerduplicate { get; set; }
    public bool focaffect { get; set; }
    public string stock { get; set; }
    public string KitStock { get; set; }
    public bool barcode { get; set; }
    public bool holdsys { get; set; }
    public bool Tenderwindow { get; set; }
    public bool Tax2onRetail { get; set; }
    public bool Tax2onVAT { get; set; }
    public bool Tax2onCST { get; set; }
    public bool KotSystem { get; set; }
    public bool BoxSystem { get; set; }
    public bool NegtiveStock { get; set; }
   
    public int UserId { get; set; }
    public int BranchId { get; set; }


	public BasicBillSettings()
	{
        CashCustomer = false;
        StartCustomerPoint = false;
        defaultpaymodeID = "";
        defaultpaymode = "";
        defaultBankID = "";
        defaultbankName = "";
        roundamt = false;
        showrate = false;
        tracinguser = false;
        itemname = false;
        shortname = false;
        headerduplicate = false;
        focaffect = false;
        stock = "";
        KitStock = "";
        barcode = false;
        holdsys = false;
        Tenderwindow = false;
        Tax2onRetail = false;
        Tax2onVAT = false;
        Tax2onCST = false;
        KotSystem = false;
        BoxSystem = false;
        NegtiveStock = false;
       
        UserId = 0;
        BranchId = 0;
	}
}