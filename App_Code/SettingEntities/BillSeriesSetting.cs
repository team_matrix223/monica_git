﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillSeriesSetting
/// </summary>
public class BillSeriesSetting
{
    public int BranchId { get; set; }
    public string BranchName { get; set; }
    public string Prefix { get; set; }
    public string CashSeries { get; set; }
    public string CreditSeries { get; set; }
    public string CrCardSeries { get; set; }
    public bool OpenClose { get; set; }
    public bool BillDate { get; set; }
    public string DefaultGodown { get; set; }
    public string Type { get; set; }
    public string Series_Name { get; set; }
    public string LUser { get; set; }
  


	public BillSeriesSetting()
	{
        BranchId = 0;
        BranchName = "";
        Prefix = "";
        CashSeries = "";
        CreditSeries = "";
        CrCardSeries = "";
        OpenClose = false;
        BillDate = false;
        DefaultGodown = "";
        Type = "";
        Series_Name = "";
        LUser = "";
      

	}
}