﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Constants
/// </summary>

public  class Constants
{


    #region SessionConstants

    public  const string EmployeeId = "eid";
    public const string EmployeeName = "ename";
    public const string  AdminId= "aid";
    public const string ClientId = "cid";
    public const string Roles = "roles";

    public const string Email = "email";
    public const string DesignationId = "desId";
    public const string BranchId = "BranchId";
    public const string PCName = "pcname";
    public const string DataBase = "db";
    public const string Sale_Rate = "Sale_Rate";

    public const string BranchName = "bname";
    #endregion SessionConstants




   
}

