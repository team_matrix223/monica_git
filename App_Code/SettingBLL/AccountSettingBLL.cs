﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AccountSettingBLL
/// </summary>
public class AccountSettingBLL
{
    public Int16 UpdateAccountSettings(AccountSettings objAccountSettings)
    {

        return new AccountSettingDAL().UpdateAccountSettings(objAccountSettings);
    }



    public void GetSettings(AccountSettings objAccountSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new AccountSettingDAL().GetAccountSettings(objAccountSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objAccountSettings.CN_Acc2DebitCode = Convert.ToString(dr["CN_Acc2DebitCode"]);
                objAccountSettings.CN_Acc2DebitName = Convert.ToString(dr["CN_Acc2DebitName"]);
                objAccountSettings.Comp_AccCode = Convert.ToString(dr["Comp_AccCode"]);
                objAccountSettings.Comp_AccName = Convert.ToString(dr["Comp_AccName"]);
                objAccountSettings.OSPurc_AccCode = Convert.ToString(dr["OSPurc_AccCode"]);
                objAccountSettings.OSPurc_AccName = Convert.ToString(dr["OSPurc_AccName"]);
                objAccountSettings.OSPurc_TaxCode = Convert.ToString(dr["OSPurc_TaxCode"]);
                objAccountSettings.OSPurc_TaxName = Convert.ToString(dr["OSPurc_TaxName"]);
                objAccountSettings.CST_ACCCode = Convert.ToString(dr["CST_ACCCode"]);
                objAccountSettings.CST_ACCName = Convert.ToString(dr["CST_ACCName"]);
                objAccountSettings.CST_TaxCode = Convert.ToString(dr["CST_TaxCode"]);
                objAccountSettings.CST_TaxName = Convert.ToString(dr["CST_TaxName"]);
                objAccountSettings.Disp_AccCode = Convert.ToString(dr["Disp_AccCode"]);
                objAccountSettings.Disp_AccName = Convert.ToString(dr["Disp_AccName"]);
                objAccountSettings.Adj_AccCode = Convert.ToString(dr["Adj_AccCode"]);
                objAccountSettings.Adj_AccName = Convert.ToString(dr["Adj_AccName"]);
                objAccountSettings.DebitNote_AccCode = Convert.ToString(dr["DebitNote_AccCode"]);
                objAccountSettings.DebitNote_AccName = Convert.ToString(dr["DebitNote_AccName"]);
                objAccountSettings.Disc_AccCode = Convert.ToString(dr["Disc_AccCode"]);

                objAccountSettings.Disc_AccName = Convert.ToString(dr["Disc_AccName"]);
                objAccountSettings.tcs_accName = Convert.ToString(dr["tcs_accName"]);
                objAccountSettings.tcs_accCode = Convert.ToString(dr["tcs_accCode"]);
                objAccountSettings.Round_accCode = Convert.ToString(dr["Round_accCode"]);
                objAccountSettings.Round_accname = Convert.ToString(dr["Round_accname"]);
                objAccountSettings.OrderSaleAccCode = Convert.ToString(dr["OrderSaleAccCode"]);
                objAccountSettings.OrderSaleAccName = Convert.ToString(dr["OrderSaleAccName"]);
                objAccountSettings.OrderAdvAccCode = Convert.ToString(dr["OrderAdvAccCode"]);
                objAccountSettings.OrderAdvAccName = Convert.ToString(dr["OrderAdvAccName"]);
                objAccountSettings.ServiceTaxAccCode = Convert.ToString(dr["ServiceTaxAccCode"]);
                objAccountSettings.ServiceTaxAccName = Convert.ToString(dr["ServiceTaxAccName"]);
                objAccountSettings.ServiceChgAccCode = Convert.ToString(dr["ServiceChgAccCode"]);
                objAccountSettings.ServiceChgAccName = Convert.ToString(dr["ServiceChgAccName"]);

                objAccountSettings.EduCess_AccCode = Convert.ToString(dr["EduCess_AccCode"]);
                objAccountSettings.EduCess_AccName = Convert.ToString(dr["EduCess_AccName"]);
                objAccountSettings.HECess_AccCode = Convert.ToString(dr["HECess_AccCode"]);
                objAccountSettings.HECess_AccName = Convert.ToString(dr["HECess_AccName"]);
                objAccountSettings.ExciseDuty_Code = Convert.ToString(dr["ExciseDuty_Code"]);
                objAccountSettings.ExciseDuty_AccName = Convert.ToString(dr["ExciseDuty_AccName"]);
                objAccountSettings.BranchTfrSale_Code = Convert.ToString(dr["BranchTfrSale_Code"]);
                objAccountSettings.BranchTfrSale_AccName = Convert.ToString(dr["BranchTfrSale_AccName"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}