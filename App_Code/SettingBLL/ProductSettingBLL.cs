﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductSettingBLL
/// </summary>
public class ProductSettingBLL
{
    public Int16 UpdateBasicSettings(ProductSetting objSettings)
    {

        return new ProductSettingDAL().UpdateBasicSettings(objSettings);
    }



    public void GetSettings(ProductSetting objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new ProductSettingDAL().GetMasterSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.Auto_GenCode = Convert.ToBoolean(dr["Auto_GenCode"]);
                objSettings.Item_CodeLen = Convert.ToString(dr["Item_CodeLen"]);
                objSettings.Item_CodeStart = Convert.ToString(dr["Item_CodeStart"]);
                objSettings.Alphabet_Code = Convert.ToBoolean(dr["Alphabet_Code"]);
                objSettings.Comp_WiseDis = Convert.ToBoolean(dr["Comp_WiseDis"]);
                objSettings.Batch_No = Convert.ToBoolean(dr["Batch_No"]);
                objSettings.ItemDup = Convert.ToBoolean(dr["ItemDup"]);
                objSettings.ItemCompDup = Convert.ToBoolean(dr["ItemCompDup"]);
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}