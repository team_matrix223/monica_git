﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillSeriesSettingBLL
/// </summary>
public class BillSeriesSettingBLL
{
    public Int16 UpdateBasicSettings(BillSeriesSetting objSettings)
    {

        return new BillSeriesSettingDAL().UpdateBasicSettings(objSettings);
    }



    


    public List<BillSeriesSetting> GetByBillType(string Type,Int32 Branch)
    {
        List<BillSeriesSetting> SeriesList = new List<BillSeriesSetting>();

        SqlDataReader dr = null;
        try
        {
            dr = new BillSeriesSettingDAL().GetMasterSettingsByType(Type,Branch);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    BillSeriesSetting objBillSeries = new BillSeriesSetting()
                    {
                        BranchId = Convert.ToInt32(dr["BranchId"].ToString()),
                        BranchName = dr["BranchName"].ToString(),
                        Prefix = Convert.ToString(dr["Prefix"]),
                        CashSeries = Convert.ToString(dr["CashSeries"]),
                        CreditSeries = Convert.ToString(dr["CreditSeries"]),
                        CrCardSeries = Convert.ToString(dr["CrCardSeries"]),
                        OpenClose = Convert.ToBoolean(dr["OpenClose"]),
                        BillDate = Convert.ToBoolean(dr["BillDate"]),
                        Type = Convert.ToString(dr["Type"]),
                        Series_Name = Convert.ToString(dr["Series_Name"]),
                        DefaultGodown = Convert.ToString(dr["DefaultGodown"]),
                        
                    };
                    SeriesList.Add(objBillSeries);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return SeriesList;

    }

}