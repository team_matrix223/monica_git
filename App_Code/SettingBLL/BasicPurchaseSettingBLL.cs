﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicPurchaseSettingBLL
/// </summary>
public class BasicPurchaseSettingBLL
{
    public Int16 UpdateBasicSettings(BasicPurchaseSetting objSettings)
    {

        return new BasicPurchaseSettingsDAL().UpdateBasicSettings(objSettings);
    }



    public void GetSettings(BasicPurchaseSetting objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BasicPurchaseSettingsDAL().GetMasterSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.Check_MRP = Convert.ToBoolean(dr["Check_MRP"]);
                objSettings.Update_SaleRatePur = Convert.ToBoolean(dr["Update_SaleRatePur"]);
                objSettings.Update_MRPPur = Convert.ToBoolean(dr["Update_MRPPur"]);
                objSettings.Show_MarginPur = Convert.ToBoolean(dr["Show_MarginPur"]);
                objSettings.Show_SaleItemInPurOrder = Convert.ToBoolean(dr["Show_SaleItemInPurOrder"]);
                objSettings.ReOrderLevel = Convert.ToString(dr["ReOrderLevel"]);
                objSettings.ReOrderLevelCal = Convert.ToString(dr["ReOrderLevelCal"]);
                objSettings.Defaultgodown = Convert.ToString(dr["Defaultgodown"]);
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);
               
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}