﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicBillSettingBLL
/// </summary>
public class BasicBillSettingBLL
{
    public Int16 UpdateBasicSettings(BasicBillSettings objSettings)
    {

        return new BasicBillSettingsDAL().UpdateBasicSettings(objSettings);
    }



    public void GetSettings(BasicBillSettings objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BasicBillSettingsDAL().GetMasterSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.CashCustomer = Convert.ToBoolean(dr["CashCustomer"]);
                objSettings.StartCustomerPoint = Convert.ToBoolean(dr["StartCustomerPoint"]);
                objSettings.defaultpaymodeID = Convert.ToString(dr["defaultpaymodeID"]);
                objSettings.defaultpaymode = Convert.ToString(dr["defaultpaymode"]);
                objSettings.defaultBankID = Convert.ToString(dr["defaultBankID"]);
                objSettings.defaultbankName = Convert.ToString(dr["defaultbankName"]);
                objSettings.roundamt = Convert.ToBoolean(dr["roundamt"]);
                objSettings.showrate = Convert.ToBoolean(dr["showrate"]);
                objSettings.tracinguser = Convert.ToBoolean(dr["tracinguser"]);
                objSettings.itemname = Convert.ToBoolean(dr["itemname"]);
                objSettings.shortname = Convert.ToBoolean(dr["shortname"]);
                objSettings.headerduplicate = Convert.ToBoolean(dr["headerduplicate"]);
                objSettings.focaffect = Convert.ToBoolean(dr["focaffect"]);

                objSettings.stock = Convert.ToString(dr["stock"]);
                objSettings.KitStock = Convert.ToString(dr["KitStock"]);
                objSettings.barcode = Convert.ToBoolean(dr["barcode"]);
                objSettings.holdsys = Convert.ToBoolean(dr["holdsys"]);
                objSettings.Tenderwindow = Convert.ToBoolean(dr["Tenderwindow"]);
                objSettings.Tax2onRetail = Convert.ToBoolean(dr["Tax2onRetail"]);
                objSettings.Tax2onVAT = Convert.ToBoolean(dr["Tax2onVAT"]);
                objSettings.Tax2onCST = Convert.ToBoolean(dr["Tax2onCST"]);
                objSettings.KotSystem = Convert.ToBoolean(dr["KotSystem"]);
                objSettings.BoxSystem = Convert.ToBoolean(dr["BoxSystem"]);
                objSettings.NegtiveStock = Convert.ToBoolean(dr["NegtiveStock"]);
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

}