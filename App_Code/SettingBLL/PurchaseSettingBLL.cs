﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PurchaseSettingBLL
/// </summary>
public class PurchaseSettingBLL
{
    public Int16 UpdatePurchaseSettings(PurchaseReceiptSettings objSettings)
    {

        return new PurchaseSettingDAL().UpdatePurchaseSettings(objSettings);
    }



    public void GetSettings(PurchaseReceiptSettings objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new PurchaseSettingDAL().GetPurchaseSettings(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.Enable_Excise = Convert.ToBoolean(dr["Enable_Excise"]);
                objSettings.UPDATE_SALERATE = Convert.ToBoolean(dr["UPDATE_SALERATE"]);
                objSettings.Margine = Convert.ToBoolean(dr["Margine"]);
                objSettings.ShowSaleOnly = Convert.ToBoolean(dr["ShowSaleOnly"]);
                objSettings.RpCode = Convert.ToBoolean(dr["RpCode"]);
                objSettings.UPdate_mrp = Convert.ToBoolean(dr["UPdate_mrp"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

}