﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillGridOptionsBLL
/// </summary>
public class BillGridOptionsBLL
{
    public Int16 UpdateBasicSettings(BillGridOptions objSettings)
    {

        return new BillGridOptionsDAL().UpdateBasicSettings(objSettings);
    }



    public void GetSettings(BillGridOptions objSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new BillGridOptionsDAL().GetMasterSettingsByType(objSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.MRP_Enable = Convert.ToBoolean(dr["MRP_Enable"]);
                objSettings.SRate_Enable = Convert.ToBoolean(dr["SRate_Enable"]);
                objSettings.Dis1_Per = Convert.ToBoolean(dr["Dis1_Per"]);
                objSettings.Dis1_Amt = Convert.ToBoolean(dr["Dis1_Amt"]);
                objSettings.Dis2_Per = Convert.ToBoolean(dr["Dis2_Per"]);
                objSettings.Dis2_Amt = Convert.ToBoolean(dr["Dis2_Amt"]);
                objSettings.Tax1_Per = Convert.ToBoolean(dr["Tax1_Per"]);
                objSettings.Tax1_Amt = Convert.ToBoolean(dr["Tax1_Amt"]);
                objSettings.Tax1_Sur1_Per = Convert.ToBoolean(dr["Tax1_Sur1_Per"]);
                objSettings.Tax1_Sur1_Amt = Convert.ToBoolean(dr["Tax1_Sur1_Amt"]);
                objSettings.Tax1_Sur2_Per = Convert.ToBoolean(dr["Tax1_Sur2_Per"]);
                objSettings.Tax1_Sur2_Amt = Convert.ToBoolean(dr["Tax1_Sur2_Amt"]);
                objSettings.Tax2_Per = Convert.ToBoolean(dr["Tax2_Per"]);

                objSettings.Tax2_Amt = Convert.ToBoolean(dr["Tax2_Amt"]);
                objSettings.Tax2_Sur1_Per = Convert.ToBoolean(dr["Tax2_Sur1_Per"]);
                objSettings.Tax2_Sur1_Amt = Convert.ToBoolean(dr["Tax2_Sur1_Amt"]);
                objSettings.Tax2_Sur2_Per = Convert.ToBoolean(dr["Tax2_Sur2_Per"]);
                objSettings.Tax2_Sur2_Amt = Convert.ToBoolean(dr["Tax2_Sur2_Amt"]);
                objSettings.Type = Convert.ToString(dr["Type"]);
              
                objSettings.UserId = Convert.ToInt32(dr["UserId"]);
                objSettings.BranchId = Convert.ToInt32(dr["BranchId"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}