﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateSaleReport.aspx.cs" Inherits="UpdateSaleReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 

<head id="Head1" runat="server">
    <title></title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery-1.10.2.js"  type="text/javascript"></script>
    <script src="js/jquery-ui.js"  type="text/javascript"></script>
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <style type="text/css">
    .active
    {
         background:orange;
    border:solid 1px gray;
    padding:3px;
    margin:2px;
   cursor:pointer;
    text-decoration:none;
     margin-bottom:15px;
    
    }
        
    .paging
    {
    background:silver;
    border:solid 1px gray;
    padding:3px;
    margin:2px;
    cursor:pointer;
    text-decoration:none;
    margin-bottom:15px;
    
    
        
    }
    
    </style>




    <script language="javascript">

        var PageSize = 25;
        var PageNo = 1;
        var VID = 0;
        var UpdateStatus = 0;
        function Printt(PageNumber, PageSize, Cat2, BrandId) {


            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
            //document.getElementById('reportout').contentWindow.location = "Reports/RptBilling.aspx?BillNowPrefix=" + celValue;
            //document.getElementById('reportout').contentWindow.location = "Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize;
            document.getElementById('reportout').contentWindow.location = "Backoffice/Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize + "&Cat2=" + Cat2 + "&BrandId=" + BrandId;
            //window.location = "Backoffice/Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize + "&Cat2=" + Cat2;
            $.uiUnlock();

        }





        function DeActiveRecord(Id) {





            var VariationId = Id;


            var Code = $("#txtItemCode_" + VariationId).val();
            var ISActive = 'false';




            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + Code + '","VariationId":"' + VariationId + '","IsActive":"' + ISActive + '"}',
                url: "UpdateSaleReport.aspx/DeActiveVariationDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    alert("Information is Deleted");
                    return;



                },
                complete: function (msg) {
                    var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                    var BrandId = "";

                    if ($.trim(Cat2) != "") {
                        GetData(16);
                    }

                }

            });



        }



        function UpdateCategories() {





            var Code = $("#txtItemCode").val();
            if (Code == "") {
                alert("Please Enter Item Code");
                $("#txtItemCode").focus();
                return;
            }
            var Cat1 = "";

            var Cat2 = "";

            var BrandId = "";




            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + Code + '","Cat1":"' + Cat1 + '","Cat2":"' + Cat2 + '","BrandId":"' + BrandId + '"}',
                url: "UpdateSaleReport.aspx/UpdateCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    alert("Information is updated");
                    return;

                },
                complete: function (msg) {

                }

            });
        }


        function UpdateRecord(Id) {

        
            var ItemID = Id;

            var Sale_Rate = $("#txtSale_Rate_" + Id).val();
      
            var Mrp = $("#txtMrp_" + Id).val();
            var Sale_Rate2 = $("#txtSale_Rate2_" + Id).val();
            var tax = $("#txttax_" + Id).val();
            var Sale_Rate3 = $("#txtSale_Rate3_" + Id).val();
            var Sale_Rate4 = $("#txtSale_Rate4_" + Id).val();

            
            $.ajax({

                type: "POST",
                data: '{ "ItemId": "' + ItemID + '","Sale_Rate":"' + Sale_Rate + '","Mrp":"' + Mrp + '","Sale_Rate2":"' + Sale_Rate2 + '","tax":"' + tax + '","Sale_Rate3":"' + Sale_Rate3 + '","Sale_Rate4":"' + Sale_Rate4 + '"}',
                url: "UpdateSaleReport.aspx/UpdateVariationDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    alert("Information is updated");

                },
                complete: function (msg) {

                }

            });







        }

        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '27') {
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');

            }

        });

        $(document).on("keypress", "input[name='pitemcode']", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var pid = $(this).attr("id");
                var arrPid = pid.split('_');

                var VariationId = arrPid[1];
                VID = VariationId;


                return;


            }
        });

        function GetData(Deptid) {

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


            $.ajax({
                type: "POST",
                data: '{ "Dept": "' + Deptid + '"}',
                url: "UpdateSaleReport.aspx/GetData",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);


                    $("#dvProducts").html(obj.HTML);
                },
                complete: function (msg) {
                    $.uiUnlock();
                    $('#Sale_Rate').attr('readonly', 'true');
                }

            });


        }


        function GetDataByCodeorName(Type, Code, Name) {

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


            $.ajax({
                type: "POST",
                data: '{ "Type": "' + Type + '","Code": "' + Code + '","Name": "' + Name + '"}',
                url: "UpdateSaleReport.aspx/GetDataByNameOrCode",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);


                    $("#dvProducts").html(obj.HTML);
                },
                complete: function (msg) {
                    $.uiUnlock();
                }

            });


        }


        $(document).ready(function () {





            $("#btnAdd").click(
            function () {

                $.ajax({
                    type: "POST",
                    data: '{ }',
                    url: "UpdateSaleReport.aspx/UpdateItemToPackingBelongs",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        alert("Information is updated");

                    },
                    complete: function (msg) {

                    }

                });


            });


            $("#dvUpdateCat").click(
            function () {

                UpdateCategories();
            }
            );

            $("#txtItemCode").keyup(
     function () {


         GetByItemCode();
     });


            $("#btnGetByItemCode").click(
     function () {
         if ($("#txtItemCode").val() == "") {
             alert("Please enter code to be searched");
             $("#txtItemCode").focus();
             return;
         }
         else {
             GetDataByCodeorName('Code', $("#txtItemCode").val(), '');
         }

     }
            );


            $("#btnSearch").click(
     function () {

         if ($("#txtSearch").val() == "") {
             alert("Please enter Name to be searched");
             $("#txtSearch").focus();
             return;
         }
         else {

             GetDataByCodeorName('Name', '', $("#txtSearch").val());
         }
     }
            );


            $("#<%=ddlCat2.ClientID %>").change(
    function () {

        GetData($(this).val());
    });














            //GetData(1, PageSize);
            $("#dvclose").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });


            $("#dvPrint").click(function () {
                var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                var BrandId = "";
                Printt(PageNo, PageSize, Cat2, BrandId);
            });







            $("#btnOk").click(function () {
                 alert(VID);
                 UpdateRecord(VID);
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
            $("#btnCancel").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
        });
       
    </script>
</head>
<body>
<iframe id="reportout" width="0" height="0"   onload="processingComplete()"></iframe>
    <form id="form1" runat="server">
    <div>
    <table>

        <tr>
              
              <td colspan="100%">
              <table>
              <tr>
                                <td style="width: 146px">
                                    <input type="text"  placeholder="Enter Code" aria-describedby="basic-addon2"
                                        id="txtItemCode"  />
                                </td>
                                <td style="background:black">
                                    <span id="btnGetByItemCode" value="Search" >
                                        <img src="images/plusicon.png" alt="" style="width:35px; padding-left: 10px" /></span>
                                </td>
                                <td style=" float: right;width: 193px;">
                                    <input type="text"  placeholder="Search By Name" aria-describedby="basic-addon2"
                                        id="txtSearch"  autofocus />
                                </td>
                                <td style="background:black">
                                    <span   id="btnSearch" value="Search">
                                        <img src="images/search-button.png" alt="" style="width: 35px; padding-left: 10px" /></span>
                                </td>
                                </tr>
                                </table>
                            </td>   
                                 

                                 
                                  

                                       


                            </tr>
      <tr><td class="headings" style="padding-bottom:20px" colspan="100%"><table><tr>
      <td ><b>Choose Group:</b></td><td >  
                         <asp:DropDownList ID="ddlCat2" runat="server"></asp:DropDownList>
                  
                     </td></tr></table></td></tr>
                     <tr><td></td><td><div id="btnAdd" disabled= "disabled"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Update To Item Table</div></td></tr>



    <tr><td colspan="100%">
    <table class="table">
    <tbody  id="dvProducts">
    
    </tbody>
    
    </table>

    </td></tr>
   <tr><td colspan="100%"><div id="dvPages"></div></td></tr>
   <tr><td colspan="100%" align="center"><div id="dvPrint" style="cursor:pointer;display:none" class="btn btn-primary btn-small"><b>Print</b></div>
  
   </td>
   </tr>

   <tr><td colspan="100%">
      <div id="dvPopup" style="display:none;background-color:Black;color:White" >
  <table width="100%" cellpadding="3" >
                    
                  
                     <tr>
                     <td valign="top" style="text-align:center " colspan="100%" >
                     
                    <h3 style="padding-bottom: 10px">Product Detail</h3>
                  
                    </td>
                    <td valign="top">  <div id="dvclose" style="cursor:pointer"><b>Close</b></div></td>
                    </tr>
                  
                    <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetail" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
               <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetailLocal" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
                    <tr><td colspan="100%" style="float:right;padding-left:20px"><table><tr>
                    <td style="padding-right:20px">
                      <div id="btnOk" class="btn btn-primary btn-small" style="width:80px;display:none">Update</div>
                    </td>
                    <td>
                  
                    <div id="btnCancel" class="btn btn-primary btn-small" style="width:80px">Cancel</div>
                    </td>
                    </tr>
                    </table></td></tr>
                    </table>
                    </div>
   </td></tr>
    
    </table>
 


    </div>
    </form>
</body>
</html>
