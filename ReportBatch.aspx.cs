﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraPrinting;
using System.IO;

public partial class ReportBatch : System.Web.UI.Page
{
    public int Qty { get { return Request.QueryString["Qty"] != null ? Convert.ToInt32(Request.QueryString["Qty"]) : 0; } }
    public int ItemId { get { return Request.QueryString["ItemId"] != null ? Convert.ToInt32(Request.QueryString["ItemId"]) : 0; } }
    public decimal MRP { get { return Request.QueryString["MRP"] != null ? Convert.ToDecimal(Request.QueryString["MRP"]) : 0; } }
    public DateTime MfdDate { get { return Request.QueryString["MfdDate"] != null ? Convert.ToDateTime(Request.QueryString["MfdDate"]) : DateTime.Now; } }
    public string BatchNo { get { return Request.QueryString["BatchNo"] != null ? Convert.ToString(Request.QueryString["BatchNo"]) : string.Empty; } }
    public string Name { get { return Request.QueryString["Name"] != null ? Convert.ToString(Request.QueryString["Name"]) : string.Empty; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            rptBatchPrinting r = new rptBatchPrinting(ItemId,Qty,MfdDate, BatchNo, MRP, Name);
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);
            //
        }
    }
}