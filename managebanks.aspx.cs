﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managebanks : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAccounts();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BANKS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    void BindAccounts()
    {

        ddlAccount.DataSource = new AccountsBLL().GetAll();
        ddlAccount.DataValueField = "CCODE";
        ddlAccount.DataTextField = "CNAME";
        ddlAccount.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account To Debit--";
        li1.Value = "0";
        ddlAccount.Items.Insert(0, li1);

    }
    [WebMethod]
    public static string Insert(int BankId, string Title,string CCode,bool IsActive)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BANKS));

        string[] arrRoles = sesRoles.Split(',');


        if (BankId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
            Banks objBank = new Banks()
            {
                Bank_ID = BankId,
                Bank_Name = Title.Trim().ToUpper(),
                CCODE = CCode,
                IsActive = IsActive,
                UserId = Id,

            };
           
            status = new BankBLL().InsertUpdate(objBank);
           
        var JsonData = new
        {
            bank = objBank,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 BankId)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BANKS));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Banks objBank = new Banks()
        {
            Bank_ID = BankId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new BankBLL().DeleteBank(objBank);
        var JsonData = new
        {
            Bank = objBank,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}


