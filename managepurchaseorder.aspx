﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managepurchaseorder.aspx.cs" Inherits="backoffice_managepurchaseorder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


<%--     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />

    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
 <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>--%>


     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />
              <script type="text/javascript" src="js/jquery.uilock.js"></script>
                  <script src="js/jscommoncalculation.js" type="text/javascript"></script>
   
   <style type="text/css">
         
        .ui-jqgrid .ui-userdata { height: auto; }
        .ui-jqgrid .ui-userdata div { margin: .1em .5em .2em;}
        .ui-jqgrid .ui-userdata div>* { vertical-align: middle; }
    </style>
 <script>

     $(function () {
         $("#txtOrderDate").datepicker({
             yearRange: '1900:2030',
             changeMonth: true,
             changeYear: true,
             dateFormat: 'mm-dd-yy'
         });

     });
 </script>

 
 <script language="javascript" type="text/javascript">

     function ApplyRoles(Roles) {


         $("#<%=hdnRoles.ClientID%>").val(Roles);
       }


     function processingComplete() {


         $.uiUnlock();
     }


     function Printt(celValue) {
         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
         var iframe = document.getElementById('reportout');
         iframe = document.createElement("iframe");
         iframe.setAttribute("id", "reportout");
         iframe.style.width = 0 + "px";
         iframe.style.height = 0 + "px";
         document.body.appendChild(iframe);
         document.getElementById('reportout').contentWindow.location = "Reports/rptPurchaseOrderBill.aspx?Id=" + celValue;
      
     }

     function ServiceClick(counterId) {


         var x = $("#ddlProducts" + counterId).val();
         $("#ddlProducts" + counterId).html($("#<%=hdnProducts.ClientID%>").html());


         $("#ddlProducts" + counterId + " option").removeAttr("selected");
         $('#ddlProducts' + counterId + ' option[value=' + x + ']').prop('selected', 'selected');

         document.getElementById("ddlProducts" + counterId).onmouseover = null;
         document.getElementById("txtServiceId" + counterId).onmouseover = null;


     }
     function QtyChange(counterId) {


         var Rate = $("#txtRate" + counterId).val();
         var Qty = $("#txtQty" + counterId).val();
         $("#txtAmount" + counterId).val(Number(Rate) * Number(Qty));

         CommonCalculations();


     }


     function Discount1TextChange(counterId) {


         CommonCalculations();

     }

     function Discount2TextChange(counterId) {


         CommonCalculations();

     }



//     function CommonCalculations() {

//         var BillValue = 0;
//         var Discount1 = 0;
//         var Discount2 = 0;
//         var Discount3 = 0;
//         var Tax = 0;

//         var TotalAmt = 0;
//         var DisplayAmount = 0;
//         var OtherDiscount = 0;
//         var Adjustment = 0;
//         var netamount = 0;

//         //BILL VALUE//
//         $("input[name='txtAmount']").each(
//          function () {
//              BillValue = Number(BillValue) + Number($(this).val());


//          }
//          );
//         // DISCOUNT1//
//         $("input[name='txtDis1Per']").each(
//              function () {
//                  var counterId = $(this).attr('counter');
//                  var rowdis = 0;
//                  var bill = $("#txtAmount" + counterId).val();
//                  var dis1 = $(this).val();
//                  rowdis = (Number(bill) * Number(dis1)) / 100;
//                  if ($("#chkDis1InRs").prop('checked') == true) {
//                      Discount1 = Number(Discount1) + Number(dis1);
//                  }
//                  else {
//                      Discount1 = Number(Discount1) + Number(rowdis);
//                  }



//              }
//              );
//         $("input[name='txtDis2Per']").each(
//              function () {

//                  var counterId = $(this).attr('counter');
//                  var rowdis = 0;
//                  var bill = $("#txtAmount" + counterId).val();
//                  var dis2 = $(this).val();
//                  var dis1 = $("#txtDis1Per" + counterId).val();
//                  rowdis = (Number(bill) * Number(dis2)) / 100;
//                  if ($("#chkDis2After1").prop('checked') == false) {
//                      if ($("#chkDis2InRs").prop('checked') == true) {
//                          Discount2 = Number(Discount2) + Number(dis2);
//                      }
//                      else {

//                          Discount2 = Number(Discount2) + Number(rowdis);
//                      }
//                  }
//                  else {
//                      if ($("#chkDis2InRs").prop('checked') == true) {
//                          disc = Number(Discount2) + Number(dis2);
//                      }
//                      else {
//                          var discc1 = (Number(bill) * Number(dis1)) / 100;
//                          var disval = Number(bill) - Number(discc1);
//                          rowdis = (Number(dis2) * Number(disval)) / 100;
//                          Discount2 = Number(Discount2) + Number(rowdis);
//                      }
//                  }




//              }
//              );


//              $("input[name='txtTaxPer']").each(
//              function () {
//                   

//                  if ($("#rbLocal").prop('checked') == true) {
//                      $("#txttax").prop("readonly", true);
//                      $("#txttax").val("");

//                      var counterId = $(this).attr('counter');
//                      var rowdis = 0;
//                      var bill = $("#txtAmount" + counterId).val();
//                      var ta = $(this).val();

//                      if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {
//                          rowdis = (Number(bill) * Number(ta)) / 100;
//                          Tax = Number(Tax) + Number(rowdis);
//                      }
//                      else if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == false)) {
//                          var newbill = 0;
//                          var dis1 = $("#txtDis1Per" + counterId).val();
//                          if ($("#chkDis1InRs").prop('checked') == true) {
//                              newbill = Number(bill) - Number(dis1);
//                          }
//                          else {
//                              var billval = (Number(bill) * Number(dis1)) / 100;
//                              newbill = Number(bill) - Number(billval);
//                          }

//                          rowdis = (Number(newbill) * Number(ta)) / 100;
//                          Tax = Number(Tax) + Number(rowdis);


//                      }
//                      else if (($("#txtTaxBeforeDis1").prop('checked') == false) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {

//                          var newbill = 0;
//                          var dis1 = $("#txtDis2Per" + counterId).val();
//                          if ($("#chkDis2InRs").prop('checked') == true) {
//                              newbill = Number(bill) - Number(dis1);
//                          }
//                          else {
//                              var billval = (Number(bill) * Number(dis1)) / 100;
//                              newbill = Number(bill) - Number(billval);
//                          }

//                          rowdis = (Number(newbill) * Number(ta)) / 100;
//                          Tax = Number(Tax) + Number(rowdis);

//                      }
//                      else {
//                          var newbill = 0;
//                          var newbill1 = 0;
//                          var newbill2 = 0;
//                          var dis1 = $("#txtDis1Per" + counterId).val();
//                          var dis2 = $("#txtDis2Per" + counterId).val();
//                          if ($("#chkDis1InRs").prop('checked') == true) {
//                              newbill1 = Number(dis1);
//                          }
//                          else {
//                              var billval = (Number(bill) * Number(dis1)) / 100;
//                              newbill1 = Number(billval);
//                          }

//                          if ($("#chkDis2InRs").prop('checked') == true) {
//                              newbill2 = Number(dis2);
//                          }
//                          else {
//                              var billval1 = (Number(bill) * Number(dis2)) / 100;
//                              newbill2 = Number(billval1);
//                          }


//                          newbill = Number(bill) - (Number(newbill1) + Number(newbill2));
//                          rowdis = (Number(newbill) * Number(ta)) / 100;
//                          Tax = Number(Tax) + Number(rowdis);
//                      }
//                  } else {

//                      $("#txttax").prop("readonly", false);
//                      var outta = $("#txttax").val();
//                      var outdis1 = $("#txtDiscount1").val();
//                      var ourdis2 = $("#txtDiscount2").val();
//                      var outbillvalue = $("#txtBillValue").val();
//                      if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {
//                          Tax = (Number(outbillvalue) * Number(outta)) / 100;

//                      }
//                      else if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == false)) {
//                          outbillvalue = Number(outbillvalue) - Number(outdis1);
//                          Tax = (Number(outbillvalue) * Number(outta)) / 100;

//                      }
//                      else if (($("#txtTaxBeforeDis1").prop('checked') == false) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {
//                          outbillvalue = Number(outbillvalue) - Number(ourdis2);
//                          Tax = (Number(outbillvalue) * Number(outta)) / 100;
//                      }
//                      else {

//                          outbillvalue = (Number(outbillvalue) - (Number(outdis1) + Number(ourdis2)));
//                          Tax = (Number(outbillvalue) * Number(outta)) / 100;


//                      }


//                  }





//              }
//              );
//         $("#txtODisPer").text(
//              function () {
//                  var otherdisper = $("#txtODisPer").val();
//                  var otherdisamt = (Number(BillValue) * Number(otherdisper)) / 100;
//                  OtherDiscount = Number(otherdisamt);

//              }
//              );
//         $("#txtDis3Per").text(
//              function () {
//                  var dis3amt
//                  var dis3per = $("#txtDis3Per").val();
//                  if ($("#chkdis3").prop('checked') == true) {
//                      var afterdis = (Number(BillValue) - (Number(Discount1) + Number(Discount2)));
//                      dis3amt = (Number(afterdis) * Number(dis3per)) / 100;
//                  }
//                  else {
//                      dis3amt = (Number(BillValue) * Number(dis3per)) / 100;
//                  }
//                  Discount3 = Number(dis3amt);
//              }
//              );


//              $("#txtBillValue").val(BillValue.toFixed(2));
//        
//         $("#lblTotalAmount").html(BillValue.toFixed(2));
//         $("#txtDiscount1").val(Discount1.toFixed(2));
//         $("#txtDiscount2").val(Discount2.toFixed(2));
//         $("#txtDis3Amt").val(Discount3.toFixed(2));
//         $("#txttaxamt").val(Tax.toFixed(2));

//         TotalAmt = Number(TotalAmt) + Number(BillValue) + Number(Tax) - (Number(Discount1) + Number(Discount2) + Number(Discount3))
//         DisplayAmount = (Number(TotalAmt) - Number(OtherDiscount));
//         $("#txtODisAmt").val(OtherDiscount.toFixed(2));
//         $("#txtDisplayAmount").val(DisplayAmount.toFixed(2));
////         $("#txtAdjustments").text(
////              function () {
////                  var adj = $("#txtAdjustments").val();
////                  Adjustment = Number(adj);
////              }
////              );



//         $("#txtTotalAmount").val(TotalAmt.toFixed(2));
//         // $("#txtAdjustments").val(Adjustment);
//         netamount = Number(netamount) + Number(DisplayAmount) + Number(Adjustment);
//     
//         $("#txtNetCost").val(Math.round(netamount.toFixed(2)));
//         $("#txtNAmount").val(Math.round(netamount.toFixed(2)));
//         $("#txtAdjustments").val((Number(Math.round(netamount.toFixed(2))) - Number(netamount.toFixed(2))).toFixed(2));



//     }

     function DeleteRow(counterId) {

         var len = $("input[name='txtServiceId']").length;
         if (len == 1) {
             alert("Row deletion failed. Package must contain atleast one service");
             return;
         }
         var tr = $("#btnRemove" + counterId).closest("tr");
         tr.remove();
         CommonCalculations();




     }

     function CodeSearch(counterId, e) {


         if (e.keyCode == 13) {

             var serviceId = $("#txtServiceId" + counterId).val().toUpperCase();
             var cntr = 0;
             $("select[name='ddlProducts']").each(
                 function () {

                     if (serviceId == $(this).val().toUpperCase()) {
                         cntr++;

                     }

                 }
                 );


             if (cntr == 1) {

                 alert("Service Already Added");

                 return;
             }
             //  var ddlServiceVal = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').val();
             var ddlServiceVal = "";
             var cost = "0";
             var amount = "0";

             $('#ddlProducts' + counterId + ' option').each(
                 function () {

                     if ($(this).val() == serviceId) {
                         ddlServiceVal = $(this).val();
                         cost = $(this).attr('cost');
                         amount = $(this).attr('cost');

                     }
                 }
                 );

             if (ddlServiceVal == "") {



                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $("#txtAmount" + counterId).val("");
                 $("#txtRate" + counterId).val("");
                 $("#txtQty" + counterId).val("");
                 $("#txtServiceId" + counterId).val("");

                 $("#txtMRP" + counterId).val("");
                 $("#txtSRate" + counterId).val("");
                 $("#txtTaxPer" + counterId).val("");
                 $("#txtFree" + counterId).val("");
                 $("#txtDis1Per" + counterId).val("");
                 $("#txtDis2Per" + counterId).val("");


                 alert("Service Not Found!!");

             }
             else {

                  $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $('#ddlProducts' + counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');
                ddlServiceVal = serviceId;
                cost = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");
                amount = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");
                tax = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("tax");
                mrp = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrp");
                srate = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("srate");


         $("#txtQty" + counterId).focus();
         $("#txtAmount" + counterId).val(amount);
         $("#txtRate" + counterId).val(cost);
         $("#txtQty" + counterId).val("1");
         $("#txtMRP" + counterId).val(mrp);
         $("#txtSRate" + counterId).val(srate);
         $("#txtTaxPer" + counterId).val(tax);
         $("#txtFree" + counterId).val("0");
         $("#txtDis1Per" + counterId).val("0.0");
         $("#txtDis2Per" + counterId).val("0.0");

         $("#txtServiceId" + counterId).val(ddlServiceVal);

                 
                 CommonCalculations();
             }

         }


     }





     function ServiceChange(counterId) {


         var serviceId = $("#ddlProducts" + counterId).val();

         var cntr = 0;
         $("select[name='ddlProducts']").each(
                 function () {


                     if (serviceId == $(this).val()) {
                         cntr++;
                     }

                 }
                 );


         if (cntr > 1) {
             $("#ddlProducts" + counterId + " option").removeAttr("selected");
             $("#txtAmount" + counterId).val("");
             $("#txtRate" + counterId).val("");
             $("#txtQty" + counterId).val("");
             $("#txtServiceId" + counterId).val("");

             $("#txtMRP" + counterId).val("");
             $("#txtSRate" + counterId).val("");
             $("#txtTaxPer" + counterId).val("");
             $("#txtFree" + counterId).val("");
             $("#txtDis1Per" + counterId).val("");
             $("#txtDis2Per" + counterId).val("");


             alert("Product Already Added");
             return;
         }
         var ddlServiceVal = "";
         var cost = "0";
         var amount = "0";


         ddlServiceVal = serviceId;
         cost = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");
         amount = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");
         tax = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("tax");
         mrp = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrp");
         srate = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("srate");


         $("#txtQty" + counterId).focus();
         $("#txtAmount" + counterId).val(amount);
         $("#txtRate" + counterId).val(cost);
         $("#txtQty" + counterId).val("1");
         $("#txtMRP" + counterId).val(mrp);
         $("#txtSRate" + counterId).val(srate);
         $("#txtTaxPer" + counterId).val(tax);
         $("#txtFree" + counterId).val("0");
         $("#txtDis1Per" + counterId).val("0.0");
         $("#txtDis2Per" + counterId).val("0.0");

         $("#txtServiceId" + counterId).val(ddlServiceVal);
         CommonCalculations();



     }

     function addTRUpdate() {
         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

         $("#tbProducts").append("<tr id='loading'><td colspan='100%' style='text-align:center'><img src='images/ajax-loader.gif' alt='loading please wait...'/></td></tr>");


         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
       


                 $.ajax({
                     type: "POST",
                     data: '{ "pid": "' + $("#hdnOrderNo").val() + '"}',
                     url: "managepurchaseorder.aspx/BindPurchaseOrderDetail",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);
                         var tr = "";

                         $("#tbProducts").append(obj.ServiceData);
                         $("#hdnCounter").val(obj.Counter);

                     },
                     complete: function () {
                         $('#tbProducts tr#loading').remove();
                         $.uiUnlock();
                     },
                      error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
                   }


                 });







 







     }



     function addTR() {



         var qtyVal = $("#txtQty" + $("#hdnCounter").val()).val();

         if (qtyVal == "" || qtyVal == "0") {
             return 0;

         }


         var counterId = Number($("#hdnCounter").val()) + 1;
         $("#hdnCounter").val(counterId);
         var tr = "";


         tr = "<tr><td><input type='text' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' style='width:50px'/></td>" +
              "<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' style='height:30px;width:120px'>" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +
             "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:50px'/></td>" +
             "<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtFree' style='width:50px'/></td>" +
              "<td><input type='text' id='txtRate" + counterId + "'  onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtRate' style='width:50px' /></td>" +

             "<td><input type='text' id='txtSRate" + counterId + "'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small' name='txtSRate' style='width:50px'/></td>" +
             "<td><input type='text' id='txtMRP" + counterId + "' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small' name='txtMRP1' style='width:50px'/></td>" +

            "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtAmount' style='width:55px' /></td>" +
            "<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small' name='txtDis1Per' style='width:50px' value='0.0' /></td>" +
            "<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small' name='txtDis2Per' style='width:50px' value='0.0' /></td>" +
            "<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:40px' /></td>";



         tr = tr + "<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>";





         $("#tbProducts").append(tr);
         $("#txtServiceId" + counterId + "").focus();




     }




     function SaveRecords() {


         var OrderNo = $("#hdnOrderNo").val();
         var OrderDate = $("#txtOrderDate").val();
        

         var supplierId = $("#ddlSupplier").val();
         if(supplierId == "0")
         {
            alert("Choose Supplier");
            $("#ddlSupplier").focus();
            return;
         }
         var vehNo = $("#txtVehNo").val();
         var displayAmount = $("#txtDisplayAmount").val();
         var adjustment = $("#txtAdjustments").val();
         if (adjustment == "") {
             adjustment = "0";
         }
         var NAmount = $("#txtNAmount").val();
         var oDisPer = $("#txtODisPer").val();
         if (oDisPer == "") {
             oDisPer = "0";
         }
         var oDisAmt = $("#txtODisAmt").val();
         var dis3per = $("#txtDis3Per").val();
         if (dis3per == "") {
             dis3per = "0";
         }
         var dis3amt = $("#txtDis3Amt").val();
         var dis1amt = $("#txtDiscount1").val();
         var dis2amt = $("#txtDiscount2").val();
         var billvalue = $("#txtBillValue").val();
         var taxp = $("#txttax").val();
         if (taxp == "") {
             taxp = "0";
         }
         var taxamt = $("#txttaxamt").val();


         var dis1InRs = false;

         if ($('#chkDis1InRs').is(":checked")) {

             dis1InRs = true;
         }


         var dis2InRs = false;

         if ($('#chkDis2InRs').is(":checked")) {

             dis2InRs = true;
         }

         var dis2After1 = false;
         if ($('#chkDis2After1').is(":checked")) {

             dis2After1 = true;
         }

         var isLocal = false;
         if ($('#rbLocal').is(":checked")) {

             isLocal = true;
         }
         var dis3afterdis1dis2 = false;
         if ($('#chkdis3').is(":checked")) {
             dis3afterdis1dis2 = true;
         }

         var taxBeforeDis1 = false;
         var taxBeforeDis2 = false;


          var isLocal = "Out";
         if ($('#rbLocal').is(":checked")) {

             isLocal = "Local";
         }

         if ($('#txtTaxBeforeDis1').is(":checked")) {

             taxBeforeDis1 = true;

         }
         if ($("#txtTaxBeforeDis2").is(":checked")) {
             taxBeforeDis2 = true; 
         }

         var godownId = $("#ddlGodown").val();
          if(godownId == "0")
         {
            alert("Choose Godown");
            $("#ddlGodown").focus();
            return;
         }
         var netCost = $("#txtNetCost").val();
         var totalAmount = $("#lblTotalAmount").html();
         var len = $("input[name='txtServiceId']").length;

         Code = [];
         Qty = [];
         Free = [];
         Amount = [];
         Rate = [];
         Id = [];
         Dis1 = [];
         Dis2 = [];
         Tax = [];
         MRP = [];
         SRate = [];
         Taxxamt = [];
         $("input[name='txtServiceId']").each(
            function (x) {

                var counterId = $(this).attr("counter");

                Code[x] = $('#ddlProducts' + counterId).val();
                Qty[x] = $("#txtQty" + counterId).val();
                Amount[x] = $("#txtAmount" + counterId).val();
                Free[x] = $("#txtFree" + counterId).val();
                Dis1[x] = $("#txtDis1Per" + counterId).val();
                Dis2[x] = $("#txtDis2Per" + counterId).val();
                Tax[x] = $("#txtTaxPer" + counterId).val();
                MRP[x] = $("#txtMRP" + counterId).val();
                Rate[x] = $("#txtRate" + counterId).val();
                SRate[x] = $("#txtSRate" + counterId).val();
                Taxxamt[x] = ((Number(Amount[x]) * Number(Tax[x])) / 100);
                Id[x] = $('#ddlProducts' + counterId).find(":selected").attr("pid");

            }

            );

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');  


         if ($("#status").val() == "I") {

             $.ajax({
                 type: "POST",
                 data: '{"OrderDate": "' + OrderDate + '","SupplierId": "' + supplierId + '","Dis1InRs": "' + dis1InRs + '","Dis2InRs": "' + dis2InRs + '","Dis2After1": "' + dis2After1 + '","IsLocal": "' + isLocal + '","TaxBeforeDis1": "' + taxBeforeDis1 + '","TaxBeforeDis2": "' + taxBeforeDis2 + '", "GodownId": "' + godownId + '","TotalAmount": "' + totalAmount + '","NetAmount": "' + netCost + '","Adjustments": "' + adjustment + '","DisplayAmount": "' + displayAmount + '","ODisAmt": "' + oDisAmt + '","ODisPer": "' + oDisPer + '","dis3per":"' + dis3per + '","dis3amt": "' + dis3amt + '","dis3afterdis1dis2":"' + dis3afterdis1dis2 + '","dis1amt":"' + dis1amt + '","dis2amt":"' + dis2amt + '","billvalue":"' + billvalue + '","taxp":"' + taxp + '","taxamt":"' + taxamt + '","codeArr": "' + Code + '","qtyArr": "' + Qty + '","amountArr": "' + Amount + '","freeArr": "' + Free + '","dis1Arr": "' + Dis1 + '","dis2Arr": "' + Dis2 + '","dis3Arr": "' + Dis2 + '","taxArr": "' + Tax + '","pidArr": "' + Id + '","mrpArr": "' + MRP + '","rateArr": "' + Rate + '","srateArr": "' + SRate + '","taxaamtarr":"'+Taxxamt+'"}',

                 url: "managepurchaseorder.aspx/Insert",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -11) {
                         alert("You don't have permission to perform this action.");
                         return;
                     }
                     if (obj.Status == "0") {
                         alert("Insertion Failed. Please try again later.");
                         return;
                     }
                     jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.PurchaseOrder, "last");

                     alert("Purchase Order added Successfully");
                     $("#addDialog").dialog("close");
                     $("#innerSave").css("display", "none");

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
                 }


             });


         }
         else {




             $.ajax({
                 type: "POST",
                 data: '{"OrderNo":"' + OrderNo + '","OrderDate": "' + OrderDate + '","SupplierId": "' + supplierId + '","Dis1InRs": "' + dis1InRs + '","Dis2InRs": "' + dis2InRs + '","Dis2After1": "' + dis2After1 + '","IsLocal": "' + isLocal + '","TaxBeforeDis1": "' + taxBeforeDis1 + '","TaxBeforeDis2": "' + taxBeforeDis2 + '", "GodownId": "' + godownId + '","TotalAmount": "' + totalAmount + '","NetAmount": "' + netCost + '","Adjustments": "' + adjustment + '","DisplayAmount": "' + displayAmount + '","ODisAmt": "' + oDisAmt + '","ODisPer": "' + oDisPer + '","dis3per":"' + dis3per + '","dis3amt": "' + dis3amt + '","dis3afterdis1dis2":"' + dis3afterdis1dis2 + '","dis1amt":"' + dis1amt + '","dis2amt":"' + dis2amt + '","billvalue":"' + billvalue + '","taxp":"' + taxp + '","taxamt":"' + taxamt + '","codeArr": "' + Code + '","qtyArr": "' + Qty + '","amountArr": "' + Amount + '","freeArr": "' + Free + '","dis1Arr": "' + Dis1 + '","dis2Arr": "' + Dis2 + '","dis3Arr": "' + Dis2 + '","taxArr": "' + Tax + '","pidArr": "' + Id + '","mrpArr": "' + MRP + '","rateArr": "' + Rate + '","srateArr": "' + SRate + '","taxaamtarr":"' + Taxxamt + '"}',

                 url: "managepurchaseorder.aspx/Update",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -11) {
                         alert("You don't have permission to perform this action.");
                         return;
                     }
                     if (obj.Status == "0") {
                         alert("Updation Failed. Please try again later.");
                         return;
                     }
                     var myGrid = $("#jQGridDemo");
                     var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                     myGrid.jqGrid('setRowData', selRowId, obj.PurchaseOrder);

                     alert("Purchase Order Updated Successfully");
                     $("#addDialog").dialog("close");
                     $("#innerSave").css("display", "none");

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
                 }


             });




         }



     }



     $(document).ready(
    function () {




      $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });


                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

        $("#txtGrnDate").val($("#<%=hdntodaydate.ClientID %>").val());
       
         $("#txtBillDate").val($("#<%=hdntodaydate.ClientID %>").val());
      
         $("#txtGrDate").val($("#<%=hdntodaydate.ClientID %>").val());
          $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());
           $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());

         

         BindGrid();


         $("#btnGo").click(
         function(){
         BindGrid();
         }
         );
        $("#btnprint").click(
         function () {

             var myGrid = $('#jQGridDemo'),
             selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
             celValue = myGrid.jqGrid('getCell', selRowId, 'OrderNo');
             Printt(celValue);
             // window.location.href = "Reports/rptPurchaseOrderBill.aspx?Id=" + celValue;
         }
         );
        $("#chkDis1InRs").change(
          function () {
              CommonCalculations();
          }
          );
        $("#chkDis2InRs").change(
          function () {
              CommonCalculations();
          }
          );
        $("#chkDis2After1").change(
          function () {
              CommonCalculations();
          }
          );
        $("#txtTaxBeforeDis1").change(
          function () {
              CommonCalculations();
          }
          );
        $("#txtTaxBeforeDis2").change(
          function () {
              CommonCalculations();
          }
          );
        $("#rbLocal").change(
          function () {
              CommonCalculations();
          }
          );
        $("#rbOutStation").change(
          function () {
              CommonCalculations();
          }
          );

        $("#txttax").keyup(
          function () {

              CommonCalculations();
          }
          );
        $("#txtODisPer").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#txtODisAmt").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#txtDis3Per").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#txtDis3Amt").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#chkdis3").change(
          function () {
              CommonCalculations();
          }
        );
        //        $("#txtAdjustments").keyup(
        //          function () {
        //              CommonCalculations();
        //          }
        //        );



        $("#btnSave").click(
        function () {

            SaveRecords(true);

        }

        );

        $("#btnCloseMe").click(
        function () {

            $("#innerSave").hide();

        }

        );

        //        $("#txtTaxBeforeDis1").change(function () {


        //            if ($(this).prop('checked') == true) {

        //                $("#txtTaxBeforeDis2").prop('checked', false);
        //            }


        //        });

        //        $("#txtTaxBeforeDis2").change(function () {

        //            if ($(this).prop('checked') == true) {
        //                $("#txtTaxBeforeDis1").prop('checked', false);
        //            }


        //        });


        $("#chkDis2After1").change(function () {

            CommonCalculations();


        });


        $("#txtTaxBeforeDis1").change(function () {

            CommonCalculations();


        });

        $("#txtTaxBeforeDis2").change(function () {

            CommonCalculations();


        });


        $("#txtDisPercent").keyup(function () {
            CommonCalculations();

        });

        $("#chkDis1InRs").change(
        function () {

            if ($('#chkDis1InRs').is(":checked")) {

                $("#thDis1").html("Dis1र");


                CommonCalculations();
            }
            else {
                $("#thDis1").html("Dis1%");

                CommonCalculations();
            }
        }
        );

        $("#chkDis2InRs").change(
                function () {

                    if ($('#chkDis2InRs').is(":checked")) {
                        $("#thDis2").html("Dis2र");
                        CommonCalculations();
                    }
                    else {
                        $("#thDis2").html("Dis2%");
                        CommonCalculations();
                    }
                }
                );


        $("#txtDisAmount").keyup(function () {

            $("#txtDisPercent").val($("#txtDisAmount").val() * 100 / $("#lblTotalAmount").html());
            CommonCalculations();

        });


        $("#txtServiceId").change(function () {

            var sid = $("#txtServiceId").val();


            $.ajax({
                type: "POST",
                data: '{ "Id": "' + sid + '"}',

                url: "managepurchase.aspx/ValidateServiceID",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == "n") {

                        $("#lblMsg").css({ "color": "red" });
                        $("#lblMsg").html("Sorry Service Id already Exists");
                        $("#hdnStatus").val("0");


                    }
                    else {
                        $("#lblMsg").css({ "color": "green" });

                        $("#lblMsg").html("Service Id Accepted!!");
                        $("#hdnStatus").val("1");

                    }
                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });



        }
        );



        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');
        //        DataGrid.jqGrid('setGridHeight', '236');


        $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
                 $("#hdnOrderNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OrderNo'));

              


               

                         $("#txtOrderNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OrderNo'));
                         $("#txtOrderDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strorddate'));

                         $("#txtBillNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BillNo'));
                         $("#txtBillDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BillDate'));
                         $("#txtGrNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GRNo'));
                         $("#txtGrDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GRDate'));
                         $("#txtVehNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'VehNo'));
                         $("#lblTotalAmount").html($('#jQGridDemo').jqGrid('getCell', rowid, 'TotalAmount'));
                         $("#txtNetCost").val($('#jQGridDemo').jqGrid('getCell', rowid, 'NetAmount'));

                         $("#txtODisPer").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ODisP'));
                         $("#txtODisAmt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ODisAmt'));
                         $("#txtDisplayAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'DisplayAmount'));
                         $("#txtAdjustments").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Adjustment'));
                         $("#txtNAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'NetAmount'));
                         $("#txtDis3Per").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis3P'));
                         $("#txtDis3Amt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis3Amt'));
                         $("#txtDiscount2").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2Amt'));
                         $("#txtDiscount1").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis1Amt'));
                         $("#txtExciseAmt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ExciseAmt'));
                         $("#txtBillValue").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BillValue'));
                         $("#txtTotalAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TotalAmount'));
                         $("#txttaxamt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxAmt'));
                         $("#txttax").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxP'));

                         var supplierID = $('#jQGridDemo').jqGrid('getCell', rowid, 'SupplierId');

                         $("#ddlSupplier option").removeAttr("selected");
                         $('#ddlSupplier option[value=' + supplierID + ']').prop('selected', 'selected');


                         $("#ddlGodown option").removeAttr("selected");
                         $('#ddlGodown option[value=' + $('#jQGridDemo').jqGrid('getCell', rowid, 'GodownId') + ']').prop('selected', 'selected');



                         if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis1InRs') == "true") {
                             $('#chkDis1InRs').prop('checked', true);

                         }
                         else {
                             $('#chkDis1InRs').prop('checked', false);

                         }



                         if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2InRs') == "true") {
                             $('#chkDis2InRs').prop('checked', true);
                         }
                         else {
                             $('#chkDis2InRs').prop('checked', false);

                         }


                         if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2AftDedDis1') == "true") {
                             $('#chkDis2After1').prop('checked', true);
                         }
                         else {
                             $('#chkDis2After1').prop('checked', false);

                         }



                         //                         if ($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxAfterDis1') == "true") {
                         //                             $('#txtTaxBeforeDis1').prop('checked', true);
                         //                             $('#txtTaxBeforeDis2').prop('checked', false);

                         //                         }
                         //                         else {
                         //                             $('#txtTaxBeforeDis1').prop('checked', false);
                         //                             $('#txtTaxBeforeDis2').prop('checked', true);
                         //                         }


                         if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsLocal') == "true") {

                             $('#rbLocal').prop('checked', true);
                             $('#rbOutStation').prop('checked', false);

                         }
                         else {

                             $('#rbLocal').prop('checked', false);
                             $('#rbOutStation').prop('checked', true);

                         }

                   


                 
             }
         });





        function BindProducts() {

            //            $.ajax({
            //                type: "POST",
            //                data: '{}',
            //                url: "managepurchase.aspx/BindProducts",
            //                contentType: "application/json",
            //                dataType: "json",
            //                success: function (msg) {

            //                    var obj = jQuery.parseJSON(msg.d);

            //                    $("#hdnProducts").html("");

            //                    $("#hdnProducts").append("<option></option>");
            //                    $("#hdnProducts").append(obj.ProductOptions);



            //                },
            //                complete: function () {

            //                    ResetControls();
            //                },
            //                error: function (xhr, ajaxOptions, thrownError) {

            //                    var obj = jQuery.parseJSON(xhr.responseText);
            //                    alert(obj.Message);
            //                }


            //            });
        }




        function ResetControls() {

            $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#txtPackageName").val("");
            $("#txtShortName").val("");


            $("#txtValidFor").val("");
            $("#lblTotalAmount").html("");
            $("#hdnCounter").val(1);
            tr = "<tr><td><input type='text' id='txtServiceId1'  onkeyup='javascript:CodeSearch(1,event);'  counter='1' class='form-control input-small' name='txtServiceId' style='width:50px'/></td>" +
              "<td><select id='ddlProducts1'  onchange='javascript:ServiceChange(1);' name='ddlProducts' style='height:30px;width:120px'>" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +
             "<td><input type='text' id='txtQty1' onkeyup='javascript:QtyChange(1);'  counter='1'  class='form-control input-small' name='txtQty' style='width:50px'  /></td>" +
             "<td><input type='text' id='txtFree1' onkeyup='javascript:FreeChange(1);' counter='1'  class='form-control input-small' name='txtFree' style='width:50px'/></td>" +
             "<td><input type='text' id='txtRate1' onkeyup='javascript:QtyChange(1);'  counter='1' class='form-control input-small' name='txtRate' style='width:50px' /></td>" +

             "<td><input type='text' id='txtSRate1'  readonly=readonly onkeyup='javascript:FreeChange(1);' counter='1'  class='form-control input-small' name='txtSRate' style='width:50px'/></td>" +
             "<td><input type='text' id='txtMRP1' readonly=readonly  onkeyup='javascript:FreeChange(1);' counter='1'  class='form-control input-small' name='txtMRP1' style='width:50px'/></td>" +

            "<td><input type='text' id='txtAmount1'  counter='1' readonly=readonly class='form-control input-small' name='txtAmount' style='width:50px' /><input type='text' id='txtCost1' readonly=readonly class='form-control input-small' name='txtCost' style='width:50px;display:none' /></td>" +
            "<td><input type='text' id='txtDis1Per1' onkeyup='javascript:Discount1TextChange(1);'  counter='1'   class='form-control input-small' name='txtDis1Per' style='width:50px' /><input type='text' id='txtCost1' readonly=readonly class='form-control input-small' name='txtCost' style='width:80px;display:none' value='0.0' /></td>" +
            "<td><input type='text' id='txtDis2Per1' onkeyup='javascript:Discount2TextChange(1);' counter='1'  class='form-control input-small' name='txtDis2Per' style='width:50px' /><input type='text' id='txtCost1' readonly=readonly class='form-control input-small' name='txtCost' style='width:80px;display:none' value='0.0' /></td>" +
            "<td><input type='text' id='txtTaxPer1'  counter='1' readonly=readonly class='form-control input-small' name='txtTaxPer' style='width:40px' /><input type='text' id='txtCost1' readonly=readonly class='form-control input-small' name='txtCost' style='width:50px;display:none' /></td>" +

            "<td><div id='btnAddRow1' style='cursor:pointer' onclick='javascript:addTR();' counter='1'><i class='glyphicon glyphicon-plus-sign'></i></div> </td>" +
            "<td><div id='btnRemove1' style='cursor:pointer' onclick='javascript:DeleteRow(1);'  counter='1'><i class='glyphicon glyphicon-remove'></i></div> </td></tr>";




            $("#tbProducts").append(tr);



            //CodeSearch("txtServiceId1", 1)
            //ServiceChange("ddlProducts1", 1);

            // QtyChange("txtQty1", 1);

            // DiscountTextChange("txtDiscountItem1", 1);
            // DisAmountItemTextChange("txtDisAmountItem1", 1);


            $("#txtOrderDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDisPercent").val("");
            $("#txtDisAmount").val("");
            $("#txtNetCost").val("");
            $("#chkDiscountDeduct").prop("checked", false);
            $("#chkBridalPackage").prop("checked", false);
            $("#txtGrnDate").val("");
            $("#txtBillNo").val("");
            $("#txtBillDate").val("");
            $("#txtGrNo").val("");
            $("#txtGrDate").val("");
            $("#txtVehNo").val("");
            $("#chkDis1InRs").prop("checked", false);
            $("#chkDis2InRs").prop("checked", false);
            $("#chkDis2After1").prop("checked", false);
            $("#txtTaxBeforeDis1").prop("checked", false);
            $("#txtTaxBeforeDis2").prop("checked", false);
            $("#txtBillValue").val("");
            $("#txtDiscount1").val("");
            $("#txtDiscount2").val("");
            $("#txtDis3Per").val("");
            $("#txtDis3Amt").val("");
            $("#txttax").val("");
            $("#txttaxamt").val("");
            $("#txtTotalAmount").val("");
            $("#txtODisPer").val("");
            $("#txtODisAmt").val("");
            $("#txtDisplayAmount").val("");
            $("#txtAdjustments").val("");
            $("#txtNAmount").val("");
            $("#ddlSupplier").val(0);
            $("#ddlGodown").val(0);
            BindGrid();

        }
        //----------------------btnADD------------------------

        $("#ddlstate").change(
        function () {

            BindCities($("#ddlstate").val());

        }

        );


        function BindCities(sid) {
            $("#ddlcities").html('');
            var stateId = sid;
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            $.ajax({
                type: "POST",
                data: '{ "stateId": "' + stateId + '"}',
                url: "ManageSupplier.aspx/BindCities",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#ddlcities").append(obj.CitiesOptions);

                },
                complete: function (msg) {
                    $.uiUnlock();

                    $('#ddlcities option[value=' + $('#jQGridDemo').jqGrid('getCell', CurrentRowId, 'City') + ']').prop('selected', 'selected');

                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });
        }


        function InsertUpdateSupplier() {


            var suppliername = $("#txtSupplier").val();
            if (suppliername.trim() == "") {
                alert("Please enter suppliername");
                return;
            }

            var Address = $("#txtAddress").val();
            if (Address.trim() == "") {
                alert("Please enter Address");
                return;
            }

            var Address1 = $("#txtAddress1").val();
            if (Address1.trim() == "") {
                alert("Please enter Address1");
                return;
            }
            var City = $("#ddlcities").val();
            if (City == null) {
                alert("Please enter City");
                return;
            }

            var State = $("#ddlstate").val();
            if (State == null) {
                alert("Please enter State");
                return;
            }

            var ContactNumber = $("#txtContactNumber").val();
            if (ContactNumber.trim() == "") {
                alert("Please enter ContactNumber");
                return;
            }
            var ContactPerson = $("#txtContactPerson").val();
            if (ContactPerson.trim() == "") {
                alert("Please enter ContactPerson");
                return;
            }

            var CSTNo = $("#txtCSTNo").val();
            if (CSTNo == null) {
                alert("Please enter CSTNo");
                return;
            }
            var TINNo = $("#txtTINNo").val();
            if (TINNo == null) {
                alert("Please enter TINNo");
                return;
            }

            var IsActive = false;
            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            $.ajax({
                type: "POST",
                data: '{"Supplier": "' + suppliername + '","Address": "' + Address + '","Address1": "' + Address1 + '","City": "' + City + '","State": "' + State + '","ContactNumber": "' + ContactNumber + '","Contactperson": "' + ContactPerson + '","CSTNo": "' + CSTNo + '","TINNo": "' + TINNo + '","isActive": "' + IsActive + '"}',
                url: "managepurchaseorder.aspx/InsertSupplier",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Insertion Failed.Supplier with duplicate name already exists.");
                        return;

                    }
                    var option = "<option value='" + obj.Supplier.SupplierId + "'>" + obj.Supplier.Supplier + "</option>";
                    $("#ddlSupplier").append(option);

                    $("#ddlstate").html('');
                    $("#ddlcities").html('');
                    $("#txtSupplier").val("");
                    $("#txtAddress").val("");
                    $("#txtAddress1").val("");
                    $("#txtContactPerson").val("");
                    $("#txtContactNumber").val("");
                    $("#txtCSTNo").val("");
                    $("#txtTINNo").val("");


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }


            });


        }





        $("#dvSupplier").click(
        function () {

            $('#radhika').dialog(
        {
            autoOpen: false,

            width: 600,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateSupplier();
                    $(this).dialog("close");


                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{}',
                url: "ManageSupplier.aspx/BindStates",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlstate").append(obj.StateOptions);


                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }


            });






            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#radhika');
            dialogDiv.dialog("option", "position", [100, 200]);
            dialogDiv.dialog('open');
            return false;

        }
        );

        $("#dvGodown").click(
        function () {

            $('#AddGodown').dialog(
        {
            autoOpen: false,

            width: 300,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateGodown();
                    $(this).dialog("close");

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });



            function InsertUpdateGodown() {


                var Title = $("#txtTitle").val();
                if (Title.trim() == "") {
                    alert("Please enter Title");
                    return;
                }
                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                $.ajax({
                    type: "POST",
                    data: '{ "title": "' + Title + '"}',
                    url: "managepurchaseorder.aspx/InsertGodown",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        if (obj.Status == 0) {

                            alert("Insertion Failed.Godown with duplicate name already exists.");
                            return;

                        }
                        jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.Godown, "last");


                        var option = "<option value='" + obj.Godown.GodownId + "'>" + obj.Godown.Title + "</option>";
                        $("#ddlGodown").append(option);

                        $("#txtTitle").val("");
                        alert("Godown added successfully.");

                    }
                     , error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                    complete: function () {


                        $.uiUnlock();
                    }

                });

            }


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#AddGodown');
            dialogDiv.dialog("option", "position", [400, 200]);
            dialogDiv.dialog('open');
            return false;


        }
        );





          $("#btnSubmit").click(
          function(){


               if ($("#txtNetCost").val() == "") {
                        alert("Please select Products from Grid");

                        return;
                    }

          $("#innerSave").toggle();
          }
          );

          ValidateRoles();

          function ValidateRoles() {

              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

              for (var i = 0; i < arrRole.length; i++) {

                  if (arrRole[i] == "1") {

                      $("#btnAdd").click(
                      function () {

                          $("#status").val("I");

                          var IsFirstTime = $("#hdnFirstTime").val();

                          if (IsFirstTime == "1") {

                              ResetControls();

                              $("#hdnFirstTime").val("0");
                          }
                          else {

                              ResetControls();
                          }


                          $('#addDialog').dialog(
                      {
                          autoOpen: false,

                          width: 900,
                          resizable: false,
                          modal: true,
                          //            buttons: {


                          //                "Save Options": function () {

                          //                    $("#innerSave").toggle();





                          //                },
                          //                "Cancel": function () {
                          //                    $(this).dialog("close");
                          //                }
                          //            }
                      });


                          //change the title of the dialgo
                          linkObj = $(this);
                          var dialogDiv = $('#addDialog');
                          dialogDiv.dialog("option", "position", [70, 50]);
                          dialogDiv.dialog('open');
                          return false;


                      }
                      );
                  }
                  else if (arrRole[i] == "3") {

                      $("#btnEdit").click(
      function () {

          $("#status").val("U");
          addTRUpdate();
          $("#hdnFirstTime").val("0");
          var pid = $("#hdnOrderNo").val();

          if (pid == "0") {
              alert("No Row Selected");
              return;
          }

          //$("#innerSave").show();

          $('#addDialog').dialog(
      {
          autoOpen: false,

          width: 900,
          resizable: false,
          modal: true,
          //            buttons: {

          //                "Update Options": function () {
          //                    $("#innerSave").toggle();
          //                },

          //                "Cancel": function () {
          //                    $(this).dialog("close");
          //                }
          //            }
      });


          //change the title of the dialgo
          linkObj = $(this);
          var dialogDiv = $('#addDialog');
          dialogDiv.dialog("option", "position", [70, 50]);
          var viewUrl = "customeradd.aspx";
          dialogDiv.dialog('open');
          return false;

      }
      );

                  }
              }
          }


        $("#btncncl").click(
        function(){
          $("#addDialog").dialog("close");
        }
        );

      

    }
    );
</script>
   <style type="text/css">
   .headings
   {
       text-align:right;
   }
   </style>

         <iframe id="reportout" width="0" height="0"  onload="processingComplete()"></iframe>
   <form id="frmPurchaseorder" runat="server">
      
   <asp:HiddenField ID="hdnRoles" runat="server"/>
       <input type="hidden" id="hdnFirstTime" value="1" />
   <input type="hidden" id="hdnCounter" value="1" />
   <input type="hidden" id="hdnUpdateCounter" value="0" />
   <input type="hidden" id="status" value="I" />
   <input type="hidden" id="hdnOrderNo" value="0"/>
    <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
      <asp:DropDownList ID="hdnProducts" style="display:none" runat="server" ></asp:DropDownList>


   <div id="radhika" title="Add New Supplier" style="display:none">
     <table >
                      <tr><td>
                     <table    cellpadding="5" cellspacing="5">
                    
                     <tr><td class="headings">Supplier:</td><td><input type="text" class="form-control input-small" id="txtSupplier"   style="width:180px"/></td><td></td></tr>
                     <tr><td class="headings">Address:</td><td><input type="text" class="form-control input-small" id="txtAddress"   style="width:180px"/></td><td></td></tr>
                     <tr><td class="headings">Address 1:</td><td><input type="text" class="form-control input-small" id="txtAddress1"  style="width:180px" /></td><td></td></tr>
                     <tr><td class="headings">State:</td><td>
                     
                     <select id="ddlstate" style="width:180px;height:30px">
                     </select>
                     </td>
               
                     </tr>
                     <tr><td class="headings">City:</td><td>
                     
                     <select id="ddlcities" style="width:180px;height:30px">
                     </select>
                     </td>
                    
                     </tr>
                    
                   </table>
                     
                     </td>
                     <td valign="top">
                     <table>
                     <tr><td>
                    
                     
                     <table    cellpadding="5" cellspacing="5">
                     <tr><td class="headings">Contact Person: </td><td><input type="text" id="txtContactPerson" style="width:180px"  class="form-control input-small" /></td><td></td></tr> 
                     <tr><td class="headings">Contact Number: </td><td><input type="text" id="txtContactNumber" class="form-control input-small"  style="width:180px" /></td><td></td></tr>
                  
                     <tr><td class="headings">CST No: </td><td><input type="text" id="txtCSTNo"  class="form-control input-small" /></td><td></td></tr> 
                    <tr><td class="headings">TIN No: </td><td><input type="text" id="txtTINNo"  class="form-control input-small" /></td><td></td></tr> 
                  
                    <tr><td class="headings">Is Active: </td><td><input type="checkbox" id="chkIsActive"  /></td><td></td></tr>
                     <tr><td colspan="100%"></td></tr>
                     </table>
                    
                     </td></tr>
                     </table>
                     </td>
                                       
                     </tr>
                     </table>


                                    <div class="form-actions" style="padding-left:5px">
														<table>
                                   
                                    </table>
                                      
 
									</div>
   
   </div>


   <div id="AddGodown" title="Add New GoDown" style="display:none">

    <table id="tbGodown">
   <tr><td>Title:</td><td><input type="text"  id="txtTitle" class="form-control input-small" /></td></tr>
   
   </table>

    </div>
    
  
  


            <div class="right_col" role="main">



                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Purchase Order</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Purchase Order <small>Nikbakers Online Order Form</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
    
    
    
                                    <br />

<table width="100%">
<tr><td align="left">
<table style="width:450px;margin-bottom:10px">
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                           
                                                 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                        
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >
                                 <i class="fa fa-search"></i>
                                </div></td>
                                </tr>
                                </table>

</td></tr>

<tr><td>
 <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
</td></tr>

</table>






                                </div>


                  <div id="addDialog" style="display:none;background-color:White" >
        


 

  
  
     <div class ="col-md-12 col-sm-12 col-xs-12">
     <div class ="x_panel">

                                         
       <table style="width:100%">
     <tr>
     <td colspan="100%" valign="top" >
     <table>
     <tr>
     <td style="width:250px" valign="top">
     <table width="100%">
     <tr><td>Order No:</td><td><input type="text" id="txtOrderNo" class="form-control input-small"  disabled=disabled value="Automatic" style="width:92px"/></td></tr>
     <tr><td>Order Date:</td><td><input type="text" id="txtOrderDate"  class="form-control input-small" style="width:92px" /></td></tr>
      
     </table>
     
     </td>
     <td style="width:300px">
     <table width="100%">
    <tr><td><input type="checkbox" id="chkDis1InRs"/> <label  style="font-weight:normal" for="chkDis1InRs"> Dis 1 In (Rs)</label></td></tr>
     <tr><td><input type="checkbox" id="chkDis2InRs"/> <label style="font-weight:normal"  for="chkDis2InRs"> Dis 2 In (Rs)</label></td></tr>
     <tr><td><input type="checkbox" id="chkDis2After1"/> <label  style="font-weight:normal" for="chkDis2After1"> Dis 2 Aft Ded Dis 1</label></td></tr>
     <tr><td>
     <table>
     <tr><td>Supplier:</td><td>   <asp:DropDownList ID = "ddlSupplier" ClientIDMode="Static" runat="server" Width="180px" Height="30px" ></asp:DropDownList></td>
      <td>
     <div style="float:left;padding:4px;width:20px;color:White;cursor:pointer;"
      id="dvSupplier"> <i class="glyphicon glyphicon-plus-sign" style="color:Black"></i>                              
                       </div>   
                       </td>
     </tr>
     </table>
     </td> </tr>
    
     </table>
     
     </td>
     <td >
     
       <table>
    <tr><td><input type="radio" id="rbLocal" name="local" checked=checked/> <label for="rbLocal" style="font-weight:normal"> Local</label></td><td><input type="radio"  id="rbOutStation" name="local" /> <label style="font-weight:normal" for="rbOutStation"> Outstation</label></td></tr>
     <tr><td colspan="2" ><input type="checkbox" id="txtTaxBeforeDis1" name="tax"/> <label style="font-weight:normal" for="txtTaxBeforeDis1" > Tax/Vat Before Dis 1</label></td> </tr>
     <tr><td colspan="2"><input type="checkbox"  id="txtTaxBeforeDis2" name="tax"/> <label  style="font-weight:normal" for="txtTaxBeforeDis2" >Tax/Vat Before Dis 2</label></td> </tr>
     <tr><td>Godown:</td><td colspan="2">
 <asp:DropDownList ID ="ddlGodown" runat="server" ClientIDMode="Static" Width="120px" Height ="30px"></asp:DropDownList>
 </td><td>
     <div style="float:left;padding:4px; padding-right:42px; width:20px;color:White;cursor:pointer;"
      id="dvGodown"> <i class="glyphicon glyphicon-plus-sign" style="color:Black"></i>                              
                       </div>   
                       </td>
 </tr>
    
     </table>
     
     </td>
     </tr>
     </table>
     
     </td>
     </tr>
     

   
   
   
   
     <tr>


     <td colspan="100%">
     <br />
     <div style="overflow-y: scroll; width: 950px;max-height:200px">
     
      <table  style="border-color: silver; border-collapse: separate; border-spacing: 3px;border: solid 1px silver" id="tbProducts">
										<thead>
											<tr style="background: #eeeeee">
												<th width="50px">
													 Code
												</th>
												<th>
													 Product
												</th>
												<th width="32px">
													 Qty
												</th>
                                                <th width="50px">
													 Free
												</th>
                                                <th width="80px">
													 Rate
												</th>
                                                <th width="50px">
													 S.Rate
												</th>
                                                <th width="50px">
													 MRP
												</th>
                                                
                                                <th width="80px">
													 Amount
												</th>
                                              <th width="50px" id="thDis1">
													 Dis1%
												</th>

                                                <th width="50px"  id="thDis2">
													 Dis2%
												</th>

                                                <th width="50px">
													 Tax%
												</th>

                                                <th></th>
                                                <th></th>

											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan ="100%">
                                                
                                                
                                                </td>
                                                
											</tr>
										</tfoot>
										<tbody>
										 
									 	 
										</tbody>
										</table>
                                       
 </div>
 <br />

 <table>
 <tr>
 
 <td>
 
 <table>
                                                <tr>
                                                <td colspan="100%">
                                                <table>
                                               <tr>  <td>Total Amount:</td><td > Rs.<label id="lblTotalAmount">0</label></td></tr>
                                                </table>
                                               </td>
                                                </tr>
                                            
                                                <tr>
                                              <td >Net Package Cost:</td>
                                              <td colspan="100%">
                                              <input type="text" class="form-control input-small"  readonly=readonly id="txtNetCost" value="0"  />
                                              </td>
                                              </tr>
                                               
                                          
                                                </table>
 
 </td>
 <td><div  id="dvTaxDenomination" style="float:left;display:none;  background-color: rgb(76, 73, 77);color:white;bottom:0px;right:0px;border:solid 1px silver;border-radius:10px;padding:5px;-webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
          box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
 <table>
 
 <tr><td colspan="2">
 <table><tr><td></td><td>Tax Denomination:</td>
 <td></td></tr></table></td></tr>
 <tr><td>Tax</td><td>VatAmt</td><td style="width:70px">Vat</td><td>SurChg</td></tr>
 <tr><td id="gridTax" colspan ="100%"><asp:Repeater ID="gvTax" runat ="server">
 <ItemTemplate>
 
 <tr><td ><div  name="tax"><%#Eval("Tax_ID") %></div></td><td ><div myid='amt_<%#Eval("Tax_ID") %>' name="amt">0.00</div></td><td><div myid='vat_<%#Eval("Tax_ID") %>' name="vat">0.00</div></td><td ><div myid='sur_<%#Eval("Tax_ID") %>' name="sur">0.00</div></td></tr>
 </ItemTemplate>
 </asp:Repeater></td></tr>
 


 </table>
  
 </div></td>
 <td>
  <div  id="innerSave" style="position:absolute;float:left;display:none;background-color: rgb(76, 73, 77);color:white;bottom:0px;right:0px;border:solid 1px silver;border-radius:10px;padding:5px;-webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
          box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
 <table>
 
 <tr><td colspan="2">
 <table><tr><td>Bill Value:</td><td style="width:98px">
 <input type="text" style="width:50px;display:none" id="txtExciseAmt"/></td>
 <td><input type="text" id="txtBillValue" readonly="readonly" style="color:Black"/></td></tr></table></td></tr>
 <tr><td>Discount 1:</td><td><input type="text" id="txtDiscount1" readonly="readonly" style="color:Black" /></td></tr>
<tr><td>Discount 2:</td><td><input type="text" id="txtDiscount2" readonly="readonly" style="color:Black"/></td></tr>
<tr><td><input type="checkbox"  id="chkdis3" /><label>Dis3 after(Dis1 & Dis2)</label></td><td><input type="text" style="width:40px;color:Black" id="txtDis3Per" /><input type="text" id="txtDis3Amt" style="width:104px"/></td></tr>
<tr><td>Tax:</td><td><input type="text"  id="txttax" style="width:40px;color:Black"/><input type="text"  id="txttaxamt" style="width:104px;color:Black"/></td></tr>
<tr><td>Total Amount:</td><td><input type="text" id="txtTotalAmount" style="color:Black" /></td></tr>
<tr><td>Other Discount:</td><td><input type="text" id="txtODisPer" style="width:40px;color:Black"/><input type="text" id="txtODisAmt"  style="width:104px"/></td></tr>
<tr><td>Display Amount:</td><td><input type="text" id="txtDisplayAmount" readonly="readonly" style="color:Black" /></td></tr>
<tr><td>Adj.(-)Less/(+)Add</td><td><input type="text" id="txtAdjustments" readonly="readonly" style="color:Black"/></td></tr>
<tr><td>Net Amount</td><td><input type="text" id="txtNAmount" readonly="readonly" style="color:Black"/></td></tr>
<tr><td> </td><td>
<div id="btnSave"  class="btn btn-primary btn-small">Save</div>
<div id="btnCloseMe"  class="btn btn-primary btn-small">Close</div>
</td></tr>
 </table>
  
 </div>
 
 </td>
 </tr>
 </table>


                                    
                                      
    </td>
     </tr>
       </table>


  </div>
                                     

</div>

  <div id="btnSubmit" style="margin-left:10px"  data-toggle="modal" class="btn btn-primary"  >
   <i class="fa fa-save"></i>
  Submit</div>
                  <div id="btncncl"  data-toggle="modal" class="btn   btn-danger"  >
                   <i class="fa fa-mail-reply-all"></i>
                  Cancel</div>

</div>
 
     

               
                  <div id="btnAdd"  data-toggle="modal" class="btn btn-primary" style="margin-left:6px" >
                   <i class="fa fa-external-link"></i>
                  New</div>
                  <div id="btnEdit"  data-toggle="modal" class="btn  btn-success"  >
                  <i class="fa fa-edit m-right-xs"></i>
                  Edit</div>
                  
                 
                  
                            </div>
                        </div>
                    </div>


                    


 
 







                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">2014 - 2015 © Superstore. Brought to you by <a>Matrix Solutions</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Matrix Solutions!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
		 

          
  <script type="text/javascript">
    
      
      function BindGrid()
      {
        var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManagePurchaseOrder.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
           colNames: ['OrderNo', 'OrderDate', 'Dis1InRs', 'Dis2InRs', 'Dis2AftDedDis1', 
           'IsLocal', 'TaxAfterDis1', 'TaxAfterDis2', 'Remarks', 'BillValue', 'Dis1Amt', 'Dis2Amt', 
           'Dis3AftDis1PDis2', 'Dis3P', 'Dis3Amt', 'TaxP', 'TaxAmt', 'TotalAmount', 'ODisP', 'ODisAmt', 'DisplayAmount',
            'Adjustment', 'NetAmount', 'SupplierId','Supplier', 'GodownId'
           ],
            colModel: [
                    
                    { name: 'OrderNo',key:true, index: 'OrderNo', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
   		            //{ name: 'OrderDate',key:true, index: 'OrderDate',formatter:"date",formatoptions:{newformat:"m/d/y"}, sorttype: 'date',  searchoptions : { sopt: ['eq']}, width: 100, stype: 'text',sortable: true,hidden:false },
   		           { name: 'strorddate', index: 'strorddate', width: 100, stype: 'text', sortable: true,hidden:false },
                      { name: 'Dis1InRs', index: 'Dis1InRs', width: 100, stype: 'text', sortable: true,hidden:false },
   		             { name: 'Dis2InRs', index: 'Dis2InRs', width: 100, stype: 'text', sortable: true,hidden:false },
   		             { name: 'Dis2AftDedDis1', index: 'Dis2AftDedDis1', width: 100, stype: 'text', sortable: true,hidden:false },
   		             { name: 'IsLocal', index: 'IsLocal', width: 100, stype: 'text', sortable: true,hidden:false },
   		             { name: 'TaxAfterDis1', index: 'TaxAfterDis1', width: 100, stype: 'text', sortable: true,hidden:false },
   		             { name: 'TaxAfterDis2', index: 'TaxAfterDis2', width: 100, stype: 'text', sortable: true,hidden:true },
   		             { name: 'Remarks', index: 'Remarks', width: 100, stype: 'text', sortable: true,hidden:true },
   		             { name: 'BillValue', index: 'BillValue', width: 100, stype: 'text', sortable: true,hidden:true },
   		             { name: 'Dis1Amt', index: 'Dis1Amt', width: 100, stype: 'text', sortable: true,hidden:true },
   		             { name: 'Dis2Amt', index: 'Dis2Amt', width: 100, stype: 'text', sortable: true,hidden:true },
   		             { name: 'Dis3AftDis1PDis2', index: 'Dis3AftDis1PDis2', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'Dis3P', index: 'Dis3P', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'Dis3Amt', index: 'Dis3Amt', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'TaxP', index: 'TaxP', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'TaxAmt', index: 'TaxAmt', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'TotalAmount', index: 'TotalAmount', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'ODisP', index: 'ODisP', width: 100, stype: 'text', sortable: true,hidden:true },
   		               { name: 'ODisAmt', index: 'ODisAmt', width: 100, stype: 'text', sortable: true,hidden:true },
   		                         
   		               { name: 'DisplayAmount', index: 'DisplayAmount', width: 100, stype: 'text', sortable: true,hidden:true },
   		                   { name: 'Adjustment', index: 'Adjustment', width: 100, stype: 'text', sortable: true,hidden:true },
   		                   { name: 'NetAmount', index: 'NetAmount', width: 100, stype: 'text', sortable: true,hidden:true },
   		                   { name: 'SupplierId', index: 'SupplierId', width: 100, stype: 'text', sortable: true,hidden:true },
                           { name: 'SupplierName', index: 'SupplierName', width: 100, stype: 'text', sortable: true,hidden:false },
   		                   { name: 'GodownId', index: 'GodownId', width: 100, stype: 'text', sortable: true,hidden:true },
   		                      
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce:true,
            toppager : true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'PackageId',
            viewrecords: true,
            height: "100%",
            width:"600px",
            sortorder: 'desc',
            caption: "Purchase Order List",
            editurl: 'handlers/ManagePackages.ashx',
             
        toolbar: [true, "top"],
           ignoreCase: true,
      });
           var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });


        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );
                     }

                   
              
    </script>

    </form>
   
</asp:Content>



