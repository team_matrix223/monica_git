﻿<%@ WebHandler Language="C#" Class="ManageBills" %>

using System;
using System.Web;

public class ManageBills : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string dateFrom = context.Request.QueryString["dateFrom"];
        string dateTo = context.Request.QueryString["dateTo"];

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);   

        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new BillBLL().GetBillByDate(Convert.ToDateTime(dateFrom), Convert.ToDateTime(dateTo),BranchId)
           );

        context.Response.Write(xss);





    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}