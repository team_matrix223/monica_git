﻿<%@ WebHandler Language="C#" Class="ManageSGroupByGroupId" %>

using System;
using System.Web;

public class ManageSGroupByGroupId : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string Group = context.Request.QueryString["Group"];
        if (Group != null)
        {


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new PropSGroupBLL().GetByGroupId(Convert.ToInt32(Group))
               );

            context.Response.Write(xss);
        }







    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}