﻿<%@ WebHandler Language="C#" Class="CreditCustomerSearch" %>

using System;
using System.Web;

public class CreditCustomerSearch : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {


        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");


        string strSearchOn = context.Request.QueryString["searchon"];
        string strCriteria = context.Request.QueryString["criteria"];
        string strSearchText = context.Request.QueryString["stext"];
        string Type = context.Request.QueryString["cType"];
        string strResponse = string.Empty;




        if (strSearchText != null)
        {

            //oper = null which means its first load.

            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            context.Response.Write(jsonSerializer.Serialize(
             new CustomerBLL().AdvancedSearchCreditCustomers(strSearchOn, strCriteria, strSearchText,Type)
                 ));
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}