﻿<%@ WebHandler Language="C#" Class="CustomersList" %>

using System;
using System.Web;

public class CustomersList : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {


        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");


        string strSearchOn = context.Request.QueryString["searchon"];
        string strCriteria = context.Request.QueryString["criteria"];
        string strSearchText = context.Request.QueryString["stext"];

        string strResponse = string.Empty;




        if (strSearchText != null)
        {

            //oper = null which means its first load.

            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            context.Response.Write(jsonSerializer.Serialize(
             new CustomerBLL().AdvancedSearch(strSearchOn, strCriteria, strSearchText)
                 ));
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}