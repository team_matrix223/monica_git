﻿<%@ WebHandler Language="C#" Class="ManageDisocunts" %>

using System;
using System.Web;

public class ManageDisocunts : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string DiscountType = context.Request.QueryString["DiscountType"];
        if (DiscountType != null)
        {


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new DiscountBLL().GetDiscountsByCategory(Convert.ToString(DiscountType))
               );

            context.Response.Write(xss);
        }







    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}