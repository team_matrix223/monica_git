﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="BreakageExpiry.aspx.cs" Inherits="BreakageExpiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

   
<%--         <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

              <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />--%>


         <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>


            <!-- PNotify -->
    <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
    <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
    <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>

    <script>

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }
        $(function () {
            var cnt = 10; //$("#custom_notifications ul.notifications li").length + 1;
            TabbedNotification = function (options) {
                var message = "<div id='ntf" + cnt + "' class='text alert-" + options.type + "' style='display:none'><h2><i class='fa fa-bell'></i> " + options.title + "</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" + options.text + "</p></div>";

                if (document.getElementById('custom_notifications') == null) {
                    alert('doesnt exists');
                } else {
                    $('#custom_notifications ul.notifications').append("<li><a id='ntlink" + cnt + "' class='alert-" + options.type + "' href='#ntf" + cnt + "'><i class='fa fa-bell animated shake'></i></a></li>");
                    $('#custom_notifications #notif-group').append(message);
                    cnt++;
                    CustomTabs(options);
                }
            }

            CustomTabs = function (options) {
                $('.tabbed_notifications > div').hide();
                $('.tabbed_notifications > div:first-of-type').show();
                $('#custom_notifications').removeClass('dsp_none');
                $('.notifications a').click(function (e) {
                    e.preventDefault();
                    var $this = $(this),
                        tabbed_notifications = '#' + $this.parents('.notifications').data('tabbed_notifications'),
                        others = $this.closest('li').siblings().children('a'),
                        target = $this.attr('href');
                    others.removeClass('active');
                    $this.addClass('active');
                    $(tabbed_notifications).children('div').hide();
                    $(target).show();
                });
            }

            CustomTabs();

            var tabid = idname = '';
            $(document).on('click', '.notification_close', function (e) {
                idname = $(this).parent().parent().attr("id");
                tabid = idname.substr(-2);
                $('#ntf' + tabid).remove();
                $('#ntlink' + tabid).parent().remove();
                $('.notifications a').first().addClass('active');
                $('#notif-group div').first().css('display', 'block');
            });
        })
    </script>


  <%--
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />

     <script src="js/jquery-ui.js"></script>
   <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />
      --%>
      <script>

          $(function () {
              $("#txtBreakageDate1").datepicker({
                  yearRange: '1900:2030',
                  changeMonth: true,
                  changeYear: true,
                  dateFormat: 'mm-dd-yy'
              });

          });

      </script>
 
   <%-- <form id="form1" runat="server"></form>--%>
     <%--<asp:HiddenField ID="hdnDate" runat="server"/>--%>
  
  <input type="hidden" id="hdnCounter" value="1" /> 
    <input type="hidden" id="hdnBreakageId" value="0"/>
  
<div class="right_col" role="main">



                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Breakage Expiry</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Breakage Expiry <small>Monica's Online Order Form</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
    
    
    
                                    <br />

<table width="100%">
<tr><td align="left">
<table style="width:450px;margin-bottom:10px">
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                           
                                                 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                        
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  > 
                                <i class="fa fa-search"></i>
                                </div></td>
                                </tr>
                                </table>

</td></tr>

<tr><td>
  <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
</td></tr>

</table>






                                </div>


                  <div id="addDialog" style="display:none;background-color:White" >
        


 

  
  
     <div class ="col-md-12 col-sm-12 col-xs-12">
     <div class ="x_panel">

                                         <form id="antoform" class="form-horizontal calender" role="form" runat="server">
                                              <asp:HiddenField ID="hdnRoles" runat="server"/>
                                        <div class="form-group">
                                        <div class="x_title">BREAKAGE EXPIRY</div>
                                            <label class="col-sm-2 control-label">Code</label>
                                            <div class="col-sm-4">
                                                <input type="text" disabled="disabled" style="width:190px;height:40px" placeholder ="Auto" class="form-control" id="txtBreakageId" name="title">
                                            </div>
                                              <label class="col-sm-2 control-label">Godown</label>
                                            <div class="col-sm-4">
                                               <select id="ddlGodown"  style="width:190px;height:40px">
                                                <option></option>
                                               </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Date</label>
                                             <div class="col-md-4 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" style="width:190px;height:40px" class="form-control has-feedback-left" id="txtBreakageDate" placeholder="MM/dd/yyyy" aria-describedby="inputSuccess2Status">
                                                            <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span>
                                                        </div>
                                            
                                        </div>

            
          
     <div style="max-height:300px;overflow-y:scroll">
     
      <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProducts">
										<thead>
											<tr>
												<th width="50px">
													 Code
												</th>
												<th>
													 Item Name
												</th>
												  <%--<th width="70px">
													Stock Qty
												</th>--%>
                                                <th width="105px">
													 Expired Qty
												</th>
                                          
                                                <th width="90px">
													 MRP
												</th>
                                                    
                                               
                                               

                                                <th></th>
                                                <th></th>

											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan ="100%">
                                                
                                                
                                                </td>
                                                
											</tr>
										</tfoot>
										<tbody>
										 
									 	 
										</tbody>
										</table>
          </div>
                         
 </form>

  </div>
                                     

</div>

  <div id="btnSubmit" style="margin-left:10px"  data-toggle="modal" class="btn btn-primary"  >
  <i class="fa fa-save"></i>
  Submit</div>
                  <div id="btncncl"  data-toggle="modal" class="btn   btn-danger"  >
                  <i class="fa fa-remove"></i>
                  Cancel</div>

</div>
 
     

               
                  <div id="btnNew"  data-toggle="modal" class="btn btn-primary" >
                  <i class="fa fa-external-link"></i>
                   New</div>
                  <div id="btnEdit"  data-toggle="modal" class="btn btn-success" >
                  <i class="fa fa-edit m-right-xs"></i>
                  Edit</div>
                  
                   <select id="hdnProducts" style="display:none" >
                  
                            </div>
                        </div>
                    </div>


                    <script language="javascript" type="text/javascript">



                   
 function fillMRP(mrp,qty,counter)
 {
 var counterId=$("#hdnCounter").val();

 var serviceId = $("#ddlProducts" + counter).val();
  $("#txtMRP" + counter).val(mrp);
//  $("#txtStockQty" + counter).val(qty);
  $('#dvMrpData').dialog('close');

   var cntr = 0;
         $("select[name='ddlProducts']").each(
                 function () {

                 var cnt=$(this).attr('counter');
              
                  var mrpx= $("#txtMRP"+cnt).val();
   
            
                     if (serviceId == $(this).val() && mrp==mrpx )
                     {
                    
                         cntr++;
                       
                     }
                      

                 }
                 );
              
                
         if (cntr > 1) {
      
             $("#ddlProducts" + counter + " option").removeAttr("selected");
             $("#txtQty" + counter).val("");
             $("#txtMRP" + counter).val("");
             $("#txtSRate" + counter).val("");
            // $("#txtStockQty" + counter).val("");
      


             alert("Product Already Added");
             return;
         }


 }

     function showQty(itemId,counter) {


         var godId = $("#ddlGodown").val() ;
         
          //$.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
       
         $.ajax({
             type: "POST",
             data: '{"itemId":"' + itemId + '","godownId":"' + godId + '","counter":"' + counter + '"}',
             url: "manageintergodown.aspx/GetMRP",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 $("#dvMrpData").html(obj.MrpOptions);



                 $('#dvMrpData').dialog(
        {
            autoOpen: false,
             closeOnEscape: false,
            width: 90,
            resizable: false,
            modal: true,
          
        }).parent().find('.ui-dialog-titlebar-close').hide();


                 //change the title of the dialgo
                 linkObj = $(this);
                 var dialogDiv = $('#dvMrpData');
                 dialogDiv.dialog("option", "position", [500, 200]);
                 dialogDiv.dialog('open');
                 return false;


             }, 
              error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
              },
               complete: function () {
              // $.uiUnlock();
                          
                     } 


         });

     
     }

     function ServiceClick(counterId) {


         var x = $("#ddlProducts" + counterId).val();
         $("#ddlProducts" + counterId).html($("#hdnProducts").html());


         $("#ddlProducts" + counterId + " option").removeAttr("selected");
         $('#ddlProducts' + counterId + ' option[value=' + x + ']').prop('selected', 'selected');

         document.getElementById("ddlProducts" + counterId).onmouseover = null;
         document.getElementById("txtServiceId" + counterId).onmouseover = null;


     }

     function QtyChange(counterId) {
       
      
//         var StockQty = $("#txtStockQty" + counterId).val();

         var AvailableQty=$('option:selected', "#ddlProducts" + counterId).attr('sqty');
         var RequiredQty=0;

        
         var txtQty = $("#txtQty" + counterId);
 

 if(!jQuery.isNumeric(txtQty.val()))
 {
 txtQty.val('').focus();
 }


         var OptionId=$("#ddlProducts" + counterId).val();
       
              $("select[name='ddlProducts'] option:selected[value='"+OptionId+"']").each(
                 function () {
                 
                 var cntrId=   $(this).parent().attr('counter');
                  var sQt= $("#txtQty" + cntrId).val();
                 

                     RequiredQty=Number(RequiredQty)+Number(sQt);

                   }
                 );

 
         if (Number(RequiredQty) >Number(AvailableQty)) 
         {
             alert("Quantity cannot be greater than available stock ("+AvailableQty+" Products)!!");
             txtQty.val(0);
             return;
         }


     }
      

     

     function DeleteRow(counterId) {


         var len = $("input[name='txtServiceId']").length;
         if (len == 1) {
             alert("Row deletion failed. Grid must contain atleast one Row");
             return;
         }
         var tr = $("#btnRemove" + counterId).closest("tr");
         tr.remove();
         




     }

     function CodeSearch(counterId, e) {


         if (e.keyCode == 13) {

             var serviceId = $("#txtServiceId" + counterId).val().toUpperCase();
             $("#txtServiceId"+ counterId).attr('disabled', true); 
             var cntr = 0;
             
              $("select[name='ddlProducts']").each(
                 function () {
                  var mrpx= $('option:selected', this).attr('mrpcount');
                 

                 
                     if (serviceId == $(this).val().toUpperCase() && (mrpx=="1" || mrpx=="0")) {
                  
                         cntr++;
                     }
                      

                 }
                 );



             if (cntr == 1) {

               $("#txtServiceId"+ counterId).removeAttr("disabled");
                 alert("Product Already Added");

                 return;
             }
             //  var ddlServiceVal = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').val();
             var ddlServiceVal = "";
             var srate = "0";
             var sqty = "0";
             var mrp = "0";
             var mrpcount="0";
             var prodId="0";
             $('#ddlProducts' + counterId + ' option').each(
                 function () {

                     if ($(this).val().toUpperCase()  == serviceId.toUpperCase() ) {
                         ddlServiceVal = $(this).val();
                         srate = $(this).attr('srate');
                         sqty = $(this).attr('sqty');
                         mrp = $(this).attr('mrp');
                          mrpcount =  $(this).attr("mrpcount");
                          prodId=$(this).attr("pid");
                     }
                 }
                 );

             if (ddlServiceVal == "") {



                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $("#txtQty" + counterId).val("");
                 $("#txtServiceId" + counterId).val("");

                 $("#txtMRP" + counterId).val("");
                 $("#txtSRate" + counterId).val("");
         
//                 $("#txtStockQty" + counterId).val("");
             

             $("#txtServiceId"+ counterId).removeAttr("disabled");
                 alert("Product Not Found!!");

             }
             else {

                 $("#txtQty" + counterId).focus();
                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $('#ddlProducts' + counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');
          
           
                 $("#txtQty" + counterId).val("0");
                 $("#txtServiceId" + counterId).val(ddlServiceVal);
//                 $("#txtStockQty" + counterId).val(sqty);
            
                 $("#txtMRP" + counterId).val(mrp);
                 $("#txtSRate" + counterId).val(srate);


           
         if (mrpcount > 1) {
          
             var td = $("#txtMRP" + counterId).closest("td");
             td.find("div[name='dvQty']").html("<div style='cursor:pointer' onclick='javascript:showQty(" + prodId + ","+counterId+")'><i class='glyphicon glyphicon-search'></i><div>");

             showQty(prodId,counterId);
        
         }
         else {
             var td = $("#txtMRP" + counterId).closest("td");
             td.find("div[name='dvQty']").html("");
         }


                 
             }

         }


     }


     function ServiceChange(counterId) {


        var serviceId = $("#ddlProducts" + counterId).val();
        $("#txtServiceId"+ counterId).attr('disabled', true); 
         var mrp=$("#txtMRP" + counterId).val();
         var cntr = 0;
         $("select[name='ddlProducts']").each(
                 function () {
                  var mrpx= $('option:selected', this).attr('mrpcount');
                 
               
             
                     if (serviceId == $(this).val() && (mrpx=="1" || mrpx=="0")) {
                  
                         cntr++;
                     }
                      

                 }
                 );

         if (cntr > 1) {
             $("#ddlProducts" + counterId + " option").removeAttr("selected");
             $("#txtQty" + counterId).val("");
             $("#txtMRP" + counterId).val("");
             $("#txtSRate" + counterId).val("");
//             $("#txtStockQty" + counterId).val("");
      

       $("#txtServiceId"+ counterId).removeAttr("disabled");
             alert("Product Already Added");
             return;
         }
         var ddlServiceVal = "";
         var cost = "0";
         var amount = "0";
         var prodId = "0";

         ddlServiceVal = serviceId;
 
         mrp = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrp");
         srate = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("srate");
//         stockQty = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("sqty");
  
         prodId = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("pid");
                mrpcount = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrpcount");
         if (mrpcount > 1) {
          
             var td = $("#txtMRP" + counterId).closest("td");
             td.find("div[name='dvQty']").html("<div style='cursor:pointer' onclick='javascript:showQty(" + prodId + ","+counterId+")'><i class='glyphicon glyphicon-search'></i><div>");

             showQty(prodId,counterId);
        
         }
         else {
             var td = $("#txtMRP" + counterId).closest("td");
             td.find("div[name='dvQty']").html("");
         }
         $("#txtQty" + counterId).focus();
         $("#txtAmount" + counterId).val(amount);
         $("#txtRate" + counterId).val(cost);
         $("#txtQty" + counterId).val("0");
         $("#txtMRP" + counterId).val(mrp);
         $("#txtSRate" + counterId).val(srate);
//         $("#txtStockQty" + counterId).val(stockQty);
      
         $("#txtServiceId" + counterId).val(ddlServiceVal);
        



     }

     



      function addTRUpdate() {
         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

       //  $("#tbProducts").append("<tr id='loading'><td colspan='100%' style='text-align:center'><img src='images/ajax-loader.gif' alt='loading please wait...'/></td></tr>");

           $("#ddlGodown").html("<option value='0' ></option>");
//            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
             

        
            $.ajax({
                type: "POST",
                data: '{}',
                url: "BreakageExpiry.aspx/BindControls",
                contentType: "application/json",
                dataType: "json",
                async:false,
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                     $("#ddlGodown").html(obj.GodownFromOptions);

                     } ,
                       error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {

 
//                         $.uiUnlock();
                     }
                

            });
         


                 $.ajax({
                     type: "POST",
                     data: '{ "BreakageId": "' + $("#hdnBreakageId").val() + '"}',
                     url: "BreakageExpiry.aspx/BindBreakageDetail",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);
                         var tr = "";
                         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                         $("#tbProducts").append(obj.ServiceData);
                         $("#hdnCounter").val(obj.Counter);
                         $("#txtBreakageDate").val(obj.BreakageData.strBreakageDate);

                         $("#txtIssueNo").val( obj.BreakageData.IssueNo);

                         var godown = obj.BreakageData.Godown_ID;
                         
                         $("#ddlGodown option[value='" + godown + "']").prop("selected", true);
     
         

                     },
                     complete: function () {
                    // $.uiUnlock();
                         $('#tbProducts tr#loading').remove();
                     },
                      error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
              }


                 });

            

     }

     function addTR() {



         var qtyVal = $("#txtQty" + $("#hdnCounter").val()).val();

         if (qtyVal == "" || qtyVal == "0") {
             return 0;

         }


         var counterId = Number($("#hdnCounter").val()) + 1;
         $("#hdnCounter").val(counterId);
         var tr = "";


         tr = "<tr><td><input type='text' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' style='width:70px'/></td>" +
              "<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' counter='" + counterId + "'  style='height:30px;width:140px'>" + $("#hdnProducts").html() + "</select></td>" +
//             "<td><input type='text' id='txtStockQty" + counterId + "'  readonly=readonly  onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtStockQty' style='width:60px'/></td>" +

             "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:60px'/></td>" +

             "<td><div style='float:left'><input type='text' id='txtMRP" + counterId + "' readonly=readonly    counter='" + counterId + "' class='form-control input-small' name='txtMRP' style='width:55px'/></div><div name='dvQty' style='float:left;width:10px;padding-top:6px;padding-left:3px'></div></td>";
             
             


         tr = tr + "<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>";




         $("#tbProducts").append(tr);
         $("#txtServiceId" + counterId + "").focus();


     }


     
         function ResetControls() 
         {


         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
         $("#hdnBreakageId").val("0");

         $("#txtBreakageId").val("Auto");
         $("#ddlGodown").val("");
         $("#txtBreakageDate").val("");


         
        }

     function SaveRecords()
     
      {

      
         var errCounter = 0;
          var error = false;
     
         var DateOfBreakage = $("#txtBreakageDate").val();
         if(DateOfBreakage == "")
         {
         alert("Choose Date");
         $("#txtBreakageDate").focus();
         return;
         }
         
         var godownFrom = $("#ddlGodown").val();
         if(godownFrom == "")
         {
           alert("Choose Godown");
          $("#ddlGodown").focus();
          return;
         }
        var godownName=$("#ddlGodown option:selected").text();

         stockArr = [];
         pidArr = [];
         mrpArr = [];
         qtyArr = [];
         
  
         $("input[name='txtServiceId']").each(
            function (x) {

                var counterId = $(this).attr("counter");

                 
                qtyArr[x] = $("#txtQty" + counterId).val();
                stockArr[x] =0; //$("#txtStockQty" + counterId).val();
                mrpArr[x] = $("#txtMRP" + counterId).val();
                pidArr[x] = $('#ddlProducts' + counterId).find(":selected").attr("pid");

               if (!jQuery.isNumeric(qtyArr[x])) {


                    $("#txtQty" + counterId).focus();
                    error = true;

                    return false;
                }


            }

            );



             if (error == true) {
                                    //alert("Invalid Quantity");
                                    return;
                                }


            var BreakageId=$("#hdnBreakageId").val();
         
             $.ajax({
                 type: "POST",
                 data: '{"BreakageId":"'+BreakageId+'","DateOfBreakage": "' + DateOfBreakage + '","GodownId": "' + godownFrom + '","stockArr": "' + stockArr + '","pidArr": "' + pidArr + '","mrpArr": "' + mrpArr + '","qtyArr": "' + qtyArr + '","godownName":"'+godownName+'"}',

                 url: "BreakageExpiry.aspx/Insert",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     if (obj.Status == "0") {
                         alert("Insertion Failed. Please try again later.");
                         return;
                     }


                 
                     
                        if(BreakageId==0)
                     {
                     
                     jQuery("#jQGridDemo").jqGrid('addRowData',  obj.BreakageData.Ref_No, obj.BreakageData, "last");
                      $("#addDialog").dialog("close");
                     alert("Stock Modified Successfully");

                     }
                     else
                     {
                     
                      var myGrid = $("#jQGridDemo");
                     var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                      myGrid.jqGrid('setRowData', selRowId, obj.BreakageData);
                    $("#addDialog").dialog("close");
                     alert("Stock Modified Successfully");
                     

                     }




                   

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete:function()
                 {

                 $.uiUnlock();
                 }


             });

 

  



     }



      

     $(document).ready(
    function () {




      $('#txtDateFrom').daterangepicker({
                            
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                               
                            });
  $('#txtDateTo').daterangepicker({
                              
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                               
                            });


$("#txtDateFrom").val($.datepicker.formatDate('mm-dd-yy', new Date()));
$("#txtDateTo").val($.datepicker.formatDate('mm-dd-yy', new Date()));
BindGrid2();


    $("#btnGo").click(
    function()
    {
    BindGrid2();
    }
    );


    
     

    $("#btncncl").click(
    function(){
    $("#addDialog").css("display","none");
    $("#addDialog").dialog("close");
    }
    );


    $("#btnSubmit").click(
    function(){
    SaveRecords();
    }
    );





        $("#ddlGodown").change(
        function () {
            if ($(this).val() != "") {
       
  
                  BindProducts($(this).val());
                 
            }
            else {

              
                 $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            }

           

        }
        );





        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');
//        DataGrid.jqGrid('setGridHeight', '236');

 



      


        function BindProducts(godId) {
          
       

            $.ajax({
                type: "POST",
                data: '{"godownId":"' + godId + '"}',
                url: "BreakageExpiry.aspx/BindProductsByGodown",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                  
                    var data="";
                    for(var i=0;i<obj.ProductOptions.length;i++)
                    {

                    data +="<option value='"+obj.ProductOptions[i].Pid+"' mrp='"+obj.ProductOptions[i].MRP+"' mrpcount='"+obj.ProductOptions[i].MrpCount+"' sqty='"+obj.ProductOptions[i].sQty+"' srate='"+obj.ProductOptions[i].SaleRate+"' pid='"+obj.ProductOptions[i].Pid+"'>"+obj.ProductOptions[i].ProductName+"</option>";
                   
                    }
                    $("#hdnProducts").html("");

                    $("#hdnProducts").html("<option></option>");
                    $("#hdnProducts").append(data);

                  


                },
                complete: function () {

                    BindGrid();
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });
        }





        function BindGrid() {

 
   

            $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#hdnCounter").val(1);
            tr = "<tr><td><input type='text' id='txtServiceId1'  onkeyup='javascript:CodeSearch(1,event);'  counter='1' class='form-control input-small' name='txtServiceId' style='width:70px'/></td>" +
              "<td><select id='ddlProducts1'  onchange='javascript:ServiceChange(1);' name='ddlProducts'  counter='1'  style='height:30px;width:140px'>" + $("#hdnProducts").html() + "</select></td>" +
//             "<td><input type='text' id='txtStockQty1' readonly=readonly   counter='1'  class='form-control input-small' name='txtStockQty' style='width:60px'/></td>" +

             "<td><input type='text' id='txtQty1' onkeyup='javascript:QtyChange(1);'  counter='1'  class='form-control input-small' name='txtQty' style='width:60px'  /></td>" +

            "<td><div style='float:left'><input type='text' id='txtMRP1' readonly=readonly    counter='1'  class='form-control input-small' name='txtMRP' style='width:55px'/></div><div name='dvQty' style='float:left;width:10px;padding-top:6px;padding-left:3px'></div></td>" +
          
            "<td><div id='btnAddRow1' style='cursor:pointer' onclick='javascript:addTR();' counter='1'><i class='fa fa-plus'></i></div> </td>" +
            "<td><div id='btnRemove1' style='cursor:pointer' onclick='javascript:DeleteRow(1);'  counter='1'><i class='fa fa-remove'></i></div> </td></tr>";




            $("#tbProducts").append(tr);




        }
        //----------------------btnADD------------------------
  
 
  $("#btnEdit").click(
        function () {

               
           
            var pid = $("#hdnBreakageId").val();
           
            if (pid == "0") {
                alert("No Row Selected");
                return;
            }

                 addTRUpdate();

            $('#addDialog').dialog(
        {
            autoOpen: false,

            width: 690,
            resizable: false,
            modal: true,
     
        });


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#addDialog');
            dialogDiv.dialog("option", "position", [260, 50]);
            var viewUrl = "customeradd.aspx";
            dialogDiv.dialog('open');
            return false;


        }
        );
        


        $("#btnAdd").click(
        function () {
            $("#status").val("I");
            ResetControls() ;
         
            BindContols();


            $('#addDialog').dialog(
        {
            autoOpen: false,

            width: 690,
            resizable: false,
            modal: true,
            buttons: {


                "Update Stock": function () {
                if(confirm("Are you sure you want to continue with Transfer?"))
                {
                SaveRecords();
            
                }
                     
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#addDialog');
            dialogDiv.dialog("option", "position", [260, 50]);
            dialogDiv.dialog('open');
            return false;

        }
        );


        $("#btnRollBack").click(
        function () {
        var Grid= $("#jQGridDemo");

              var selRowId= Grid.jqGrid('getGridParam', 'selrow');
            if(selRowId==null)
            {
            alert("Please select a Row");
            return;
            }
             
           var BreakageId=  Grid.jqGrid('getCell', selRowId, 'BreakageId') ;
        
         if(confirm("Are you sure you want to rollback transaction with Code "+BreakageId))
         {

          $.ajax({
                type: "POST",
                data: '{"BreakageId":"' + BreakageId + '"}',
                url: "managebreakage.aspx/RollBack",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                  if (obj.Status == "0") {
                         alert("Rollback Transaction Failed. Please try again later.");
                         return;
                     }
           Grid.jqGrid('delRowData',selRowId);
                      
                     alert("Transaction Rollback Successfully");



                },
                complete: function () {
 
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });


         
         }


        }
        );

    }
    );
</script>


                    <script type="text/javascript">
                        $(document).ready(function () {

                            ValidateRoles();

                            function ValidateRoles() {

                                var arrRole = [];
                                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                                for (var i = 0; i < arrRole.length; i++) {
                                    if (arrRole[i] == "1") {

                                        $("#btnNew").click(
                                        function () {
                                            ResetControls();

                                            BindContols();



                                            $('#addDialog').dialog(
                    {
                        autoOpen: false,

                        width: 690,
                        resizable: false,
                        modal: true,

                    });


                                            //change the title of the dialgo
                                            linkObj = $(this);
                                            var dialogDiv = $('#addDialog');
                                            dialogDiv.dialog("option", "position", [260, 50]);
                                            dialogDiv.dialog('open');
                                            return false;



                                        }
                                        );

                                    }
                                    else if (arrRole[i] == "3") {
                                        $("#btnEdit").click(
       function () {



           var pid = $("#hdnBreakageId").val();

           if (pid == "0") {
               alert("No Row Selected");
               return;
           }

          // addTRUpdate();

           $('#addDialog').dialog(
       {
           autoOpen: false,

           width: 690,
           resizable: false,
           modal: true,

       });


           //change the title of the dialgo
           linkObj = $(this);
           var dialogDiv = $('#addDialog');
           dialogDiv.dialog("option", "position", [260, 50]);
           var viewUrl = "customeradd.aspx";
           dialogDiv.dialog('open');
           return false;


       }
       );



                                    }
                                }
                            }



                            $('#txtBreakageDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });


   

                                    function BindContols() {
       
         
            $("#ddlGodown").html("<option ></option>");


       
         
            $.ajax({
                type: "POST",
                data: '{}',
                url: "BreakageExpiry.aspx/BindControls",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                  
                    $("#ddlGodown").append(obj.GodownFromOptions);




                },
                complete: function () {


                   
          
                    
                }


            });

        }

        
       



                        });
                    </script>







                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">2014 - 2015 © Superstore. Brought to you by <a>Matrix Solutions</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Matrix Solutions!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


            <script type="text/javascript">
        


         function BindGrid2()
         {
      

         var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
 
      
     jQuery("#jQGridDemo").GridUnload();

        jQuery("#jQGridDemo").jqGrid({
          url: 'handlers/ManageBreakage.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
 
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
           colNames: ['Code', 'Date', 'GodownId','Godown'
           ],
            colModel: [
                    
                    { name: 'Ref_No',key:true, index: 'Ref_No', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
   		            { name: 'strBreakageDate', index: 'strBreakageDate', width: 100, stype: 'text', sortable: true,hidden:false },
   		              { name: 'Godown_ID', index: 'Godown_ID', width: 100, stype: 'text', sortable: true,hidden:true },
   		            { name: 'Godown', index: 'Godown', width: 100, stype: 'text', sortable: true,hidden:false },
   		              
                      ],
            rowNum: 10,
            mtype: 'GET',
            loadonce:true,
            toppager : true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'BreakageId',
            viewrecords: true,
            height: "100%",
            width:"1000px",
            sortorder: 'desc',
            caption: "Breakage/Expiry List",
             
             
       toolbar: [true, "top"],
           ignoreCase: true,
      });
           var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });


            
          $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
              
       
             var BreakageId=$('#jQGridDemo').jqGrid('getCell', rowid, 'Ref_No');
            
                 $("#hdnBreakageId").val(BreakageId);
                 $("#txtBreakageId").val(BreakageId);  
                     


                     
            $.ajax({
                type: "POST",
                data: '{"godownId":"' + $('#jQGridDemo').jqGrid('getCell', rowid, 'Godown_ID') + '"}',
                url: "BreakageExpiry.aspx/BindProductsByGodown",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                  
                    var data="";
                    for(var i=0;i<obj.ProductOptions.length;i++)
                    {

                    data +="<option value='"+obj.ProductOptions[i].BarCode+"' mrp='"+obj.ProductOptions[i].MRP+"' mrpcount='"+obj.ProductOptions[i].MrpCount+"' sqty='"+obj.ProductOptions[i].sQty+"' srate='"+obj.ProductOptions[i].SaleRate+"' pid='"+obj.ProductOptions[i].Pid+"'>"+obj.ProductOptions[i].ProductName+"</option>";
                   
                    }
                    $("#hdnProducts").html("");

                    $("#hdnProducts").html("<option></option>");
                    $("#hdnProducts").append(data);
                   



                },
                complete: function () {

                   // BindGrid();
                   
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }
               });



             }
         });



        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );

                       var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');

      }
              
    </script>

</asp:Content>


