﻿<table style="width: 100%">
                    <tr>
                        <td>
                            <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; margin-top: 5px;
                                width: 100%; border-collapse: separate; border-spacing: 5px">
                                <tr>
                                    <th colspan="100%" style='background: silver'>
                                        BASIC INFORMATION
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Code:
                                    </td>
                                    <td>
                                        <input type="hidden" id="hdnIMItemId" value="0" />
                                        <input type="text" id="txtIMItemCode" style="width: 80px" />
                                    </td>
                                    <td>
                                        Name:
                                    </td>
                                    <td>
                                        <input type="text" id="txtIMItemName" />
                                    </td>
                                    <td>
                                        Short Name1:
                                    </td>
                                    <td>
                                        <input type="text" id="txtIMShortName1" />
                                    </td>
                                    <td>
                                        Short Name2:
                                    </td>
                                    <td>
                                        <input type="text" id="txtIMShortName2" />
                                    </td>
                                    <td>
                                        Barcode:
                                    </td>
                                    <td>
                                        <input type="text" id="txtIMBarCode" style="width: 80px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" style="margin-top: 15px; margin-bottom: 10px">
                                <tr>
                                    <td valign="top" style="padding: 5px">
                                        <table cellspacing="5" cellpadding="5" style="height: 288px; border: solid 1px silver;
                                            width: 100%; border-collapse: separate; border-spacing: 5px">
                                            <thead>
                                                <tr>
                                                    <th colspan="100%" style='background: silver'>
                                                        MASTER INFORMATION
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td>
                                                    Department:
                                                </td>
                                                <td>
                                                    <select id="ddlIMDepartment" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Group:
                                                </td>
                                                <td>
                                                    <select id="ddlIMGroup" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Subgroup:
                                                </td>
                                                <td>
                                                    <select id="ddlIMSubGroup" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Company:
                                                </td>
                                                <td>
                                                    <select id="ddlIMCompany" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Category:
                                                </td>
                                                <td>
                                                    <select id="ddlIMCategory" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Tax:
                                                </td>
                                                <td>
                                                    <select id="ddlIMTax" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Item Type:
                                                </td>
                                                <td>
                                                    <select id="ddlIMItemType" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Excise/Terrif:
                                                </td>
                                                <td>
                                                    <select id="ddlIMExicise" style="width: 100px">
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" style="padding: 5px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    RATE INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Pur. Rate/Unit
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMPurRate" style="width: 70px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Max. Retail Price
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMMaxRetailPrice" style="width: 70px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sale Rate/Unit
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMSaleRate" style="width: 70px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sale Rate(Excl.)
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMSaleRateExcl" style="width: 70px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Mark Up(%)
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMMarkup" style="width: 70px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Rack & Shelf
                                                </td>
                                                <td>
                                                    <select style="width: 70px" id="ddlIMRackShelf">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="100%">
                                                    <input type="checkbox" id="chkIMItemDiscontinued" /><label>Item Discontinued</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="100%">
                                                    <input type="checkbox" id="chkIMCouponPrinting" /><label>Coupon Printing</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" style="padding: 5px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    QTY INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sales Unit
                                                </td>
                                                <td>
                                                    <select style="width: 100px" id="ddlIMSaleUnit">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Wgt/Ltr/Pcs
                                                </td>
                                                <td>
                                                    <input type="text" style="width: 50px" id="txtIMWgtLtr" /><select id="ddlIMWgtLtr"
                                                        style="width: 50px"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Transaction Type
                                                </td>
                                                <td>
                                                    <select style="width: 100px" id="ddlIMTransactionType">
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                            width: 100%; border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    PACKING INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td colspan="100%">
                                                    <input type="checkbox" id="chkInHousePacking" /><label>InHouse Packing</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Packed Qty.
                                                </td>
                                                <td>
                                                    <input type="text" id="chkIMPackedQty" style="width: 100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="chkIMDis1Per" />
                                                    Dis1(%)
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMDis1Per" style="width: 100%" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="chkIMDis2Per" />
                                                    Dis2(%)
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMDis2Per" style="width: 100%" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" style="padding: 5px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    OTHER INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;
                                                        border-spacing: 5px">
                                                        <tr>
                                                            <td>
                                                                Days:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtIMDays" style="width: 40px" />
                                                            </td>
                                                            <td>
                                                                <input type="radio" id="rdoIMExpiry" /><label>Expiry</label>
                                                            </td>
                                                            <td>
                                                                <input type="radio" id="rdoIMBestBefore" /><label>Best Before</label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;
                                                        border-spacing: 5px">
                                                        <tr>
                                                            <td>
                                                                <input type="radio" id="rdoIMVeg" /><label>Veg.</label>
                                                            </td>
                                                            <td>
                                                                <input type="radio" id="rdoIMNonVeg" /><label>Non Veg.</label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;
                                                        border-spacing: 5px">
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" id="chkIMSubItem" /><label>Sub-Item</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtIMSubItem" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Qty To Less
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtIMQtyToLess" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                            width: 100%; border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    STOCK LEVELING
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Max Level
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMMaxLevel" style="width: 50px" />
                                                </td>
                                                <td>
                                                    Min Level
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMMinLevel" style="width: 50px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="100%">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Re. Order Qty.
                                                            </td>
                                                            <td colspan="100%">
                                                                <input type="text" id="txtIMReOrderOty" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="padding: 10px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    RATE LEVELING
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Level 2
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMLevel2" style="width: 80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Level 3
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMLevel3" style="width: 80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Level 4
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMLevel4" style="width: 80px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding: 5px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    DEALER INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Cur. (Qty/Rate)
                                                </td>
                                                <td>
                                                    <select id="ddlIMCurQtyRate">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Dealer Margin (%)
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMDealerMargin" style="width: 80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Qty In Case
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMQtyInCase" style="width: 80px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding: 10px">
                                        <table cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                            border-collapse: separate; border-spacing: 5px">
                                            <tr>
                                                <th colspan="100%" style='background: silver'>
                                                    PURCHASE RATE INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    With Tax
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMWithTax" style="width: 80px" />
                                                </td>
                                                <td>
                                                    Latest Pur. Rate
                                                </td>
                                                <td>
                                                    <input type="text" id="txtIMLatestPurRate" style="width: 80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Comments
                                                </td>
                                                <td colspan="100%">
                                                    <textarea style="height: 57px;" id="txtIMComments"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="middle" style="padding-left: 20px;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <button class="btn btn-primary" id="btnIMAdd" style="width: 120px">
                                                        Add Item</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <button class="btn btn-danger" style="width: 120px">
                                                        Cancel</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>