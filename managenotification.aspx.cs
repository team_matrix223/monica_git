﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managenotification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBranches();
        }
    }



    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string FillSettings()
    {
        Notification ObjSettings = new Notification();

        var BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new NotificationBLL().GetSettings(ObjSettings, BranchId);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Insert(string Notification1, string Notification2, string Declaration,string Range,string Division,
        string CommissionRate,string TINNO,string PANNO,string CERegn, int BranchId)
    {
        int status = 0;
  

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Notification objNotify = new Notification()
        {
            Notification1 = Notification1,
            Notification2 = Notification2,
            Declaration = Declaration,
            Range = Range,
            Division = Division,
            CommissionRate = CommissionRate,
            TINNO = TINNO,
            PANNO = PANNO,
            CERegn = CERegn,
            BranchId = BranchId
           
        };
        status = new NotificationBLL().InsertUpdate(objNotify);
        var JsonData = new
        {
            ExciseData = objNotify,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}