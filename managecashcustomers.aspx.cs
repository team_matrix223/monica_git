﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managecashcustomers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            hdnDate.Value = DateTime.Now.ToShortDateString();

        }
        CheckRole();

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CASHCUSTOMERS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string Delete(Int16 CustomerId)
    {
        Customers objCustomer = new Customers()
        {
            Customer_ID = CustomerId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new CustomerBLL().DeleteCustomer(objCustomer);
        var JsonData = new
        {
            Customer = objCustomer,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdateCustomer(Customers objCustomer)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objCustomer.UserId = Id;
        objCustomer.BranchId = Branch;
        int status = new CustomerBLL().InsertUpdate(objCustomer);
        var JsonData = new
        {
            customer = objCustomer,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByCustomerId(int CustomerId)
    {
        Customers objCustomers = new Customers()
        {
            Customer_ID = CustomerId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CustomerBLL().GetById(objCustomers);
        var JsonData = new
        {
            Customer = objCustomers


        };
        return ser.Serialize(JsonData);
    }


}