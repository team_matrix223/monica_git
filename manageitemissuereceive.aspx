﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageitemissuereceive.aspx.cs" Inherits="manageitemissuereceive" %>


<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">
    <%--<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>

   <script type="text/javascript" src="js/jquery.uilock.js"></script>


    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
--%>

       
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
   
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function ApplyRoles(Roles) {



            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


        var m_RefNo = 0;
        var m_TransferID = 0;
        var ProductCollection = [];
        var mrpeditable = false;
        var saleRateeditable = false;




        function clsProduct() {

            this.Item_Code = "";
            this.Item_Name = "";
            this.Qty = 0;
            this.Rate = 0;
            this.MRP = 0;
            this.Amount = 0;
            this.Stock = 0;

        }

        var m_TransferID = 0;

        function CommonCalculation() {

            var Amt = Number($("#txtQty").val()) * $("#txtRate").val();

            $("#txtAmount").val(Amt);
        }


        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());

            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });



        function BindRows() {

            var TotalAmount = 0;
            var html = "";
            for (var i = 0; i < ProductCollection.length; i++) {




                html += "<tr>";
                html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
                html += "<td>" + ProductCollection[i]["Qty"] + "</td>";
                html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
                html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
                html += "<td>" + ProductCollection[i]["Amount"] + "</td>";

                html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
                html += "</tr>";

                TotalAmount += parseFloat(ProductCollection[i]["Amount"])

            }

            $("#txtTotalAmount").val(TotalAmount);

            $("#tbKitProducts").html(html);

        }


        function ResetControls() {

            m_TransferID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtRefNo").val("Auto");
            $("#txtDate").removeAttr("disabled");
            $("#ddlGodownTo").removeAttr("disabled");
            ProductCollection = [];
            ResetList();
        }

        function ResetList() {

            $("#txtCode").val("");
            $("#txtName").val("");
            $("#txtQty").val("");
            $("#txtRate").val("");
            $("#txtMarketPrice").val("");
            $("#txtAmount").val("");

            $("#ddlProducts").html("<option value='0'></option>");
            $("#txtddlProducts").val("").focus();





        }

        function GetPluginData(Type) {

            if (Type == "Product") {

                $("#txtCode").val($("#ddlProducts option:selected").attr("item_code"));
                $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
                $("#txtQty").val("").focus();
                $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
                $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
                $("#txtAmount").val(0);



            }
        }

        $(document).ready(function () {


            $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());


            $("#btnGo").click(
                function () {


                    BindGrid();
                }
                );


            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });



            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $("#txtDate").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            BindGodown();
            function BindGodown() {



                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "managedeliverynote.aspx/BindDealers",
                    data: {},
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);


                        var html1 = "<option value = 0>--SELECT--</option>";

                        for (var i = 0; i < obj.GodownOptions.length; i++) {

                            html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                        }

                        $("#ddlGodown").html(html1);

                        $("#ddlGodownTo").html(html1);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                        $.ajax({
                            type: "POST",
                            data: '{ }',
                            url: "managedeliverynote.aspx/GetDefaultGodown",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);



                                $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                                $("#ddlGodown").prop("disabled", true);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {



                            }

                        });



                    }
                });

            }




            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managekits.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    mrpeditable = obj.Mrp;
                    saleRateeditable = obj.Sale;


                    if (saleRateeditable == "0") {

                        $("#txtRate").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtRate").removeAttr('disabled');
                    }

                    if (mrpeditable == "0") {
                        $("#txtMarketPrice").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtMarketPrice").removeAttr('disabled');
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });


            ResetList();

            $("#btnCancelDialog").click(
            function () {
                $("#TransferDialog").dialog("close");

            }
            );



            $("#btnSave").click(
             function () {


                 //var val1 = parseFloat($("#txtTotalAmount").val());
                 //var val2 = parseFloat($("#txtSaleRate").val());


                 //if (!(val1 == val2)) {

                 //    alert("Total Amount  is not  Equal to Sale Rate.");
                 //    return;

                 //}


                 var Master_Code = "";


                 if (ProductCollection.length == 0) {

                     alert("Please choose  Items");
                     return;
                 }


                 var m_GodownId = $("#ddlGodown").val();
                 var m_GodownTo = $("#ddlGodownTo").val();
                 var m_Date = $("#txtDate").val();

                 var DTO = { 'objItemIssue': ProductCollection, 'RefNo': m_TransferID, 'GodownId': m_GodownId, 'Date': m_Date, 'GodownTo': m_GodownTo };


                 $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "manageitemissuereceive.aspx/InsertUpdate",
                     data: JSON.stringify(DTO),
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);

                         if (obj.status == -11) {
                             alert("You don't have permission to perform this action..Consult Admin Department.");
                             return;
                         }


                         if (m_TransferID == 0) {

                             alert("Item Issue/Receive Added Successfully");

                         }
                         else {

                             alert("Item Issue/Receive Updated Successfully");

                         }

                         BindGrid();
                         $("#TransferDialog").dialog("close");


                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {


                     }
                 });



             });


            $("#btnAddKitItems").click(
            function () {


                if (!(Number($("#txtQty").val()) > 0)) {
                    $("#txtQty").focus();
                    return;

                }

                TO = new clsProduct();

                TO.Item_ID = $("#ddlProducts option:selected").val();
                TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
                TO.Item_Name = $("#ddlProducts option:selected").attr("item_name");
                TO.Qty = $("#txtQty").val();
                TO.Rate = $("#txtRate").val();
                TO.MRP = $("#ddlProducts option:selected").attr("mrp")
                TO.Amount = $("#txtAmount").val();
                TO.Tax_ID = $("#ddlProducts option:selected").attr("tax_id")
                TO.Tax_Rate = $("#ddlProducts option:selected").attr("tax_code");

                ProductCollection.push(TO);

                BindRows();
                ResetList();

            }

            );



            $("#txtQty").keyup(
            function () {

                CommonCalculation();

            }
            );

            $("#txtRate").keyup(
            function () {
                CommonCalculation();


            }
            );





            $("#ddlProducts").supersearch({
                Type: "Product",
                Caption: "Please enter Item Name/Code ",
                AccountType: "",
                Width: 214,
                DefaultValue: 0,
                Godown: 0
            });





            BindGrid();


            ValidateRoles();

            function ValidateRoles() {

                $("#btnNew").click(
                                 function () {


                                     ResetControls();
                                     $("#TransferDialog").dialog({
                                         autoOpen: true,

                                         width: 1000,
                                         resizable: false,
                                         modal: true
                                     });
                                 }
                                 );





                $("#btnEdit").click(

 function () {


     $("#ddlGodownTo").prop("disabled", true);

     $("#txtDate").prop("disabled", true);
     $.ajax({
         type: "POST",
         contentType: "application/json; charset=utf-8",
         url: "manageitemissuereceive.aspx/GetById",
         data: '{"RefNo":"' + m_TransferID + '"}',
         dataType: "json",
         success: function (msg) {

             var obj = jQuery.parseJSON(msg.d);






             ProductCollection = obj.TransferDetail;


             BindRows();

             $("#TransferDialog").dialog({
                 autoOpen: true,

                 width: 1000,
                 resizable: false,
                 modal: true
             });

         },
         error: function (xhr, ajaxOptions, thrownError) {

             var obj = jQuery.parseJSON(xhr.responseText);
             alert(obj.Message);
         },
         complete: function () {

         }
     });



 }
 );




                $("#btnDelete").click(
 function () {

     var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
     if ($.trim(SelectedRow) == "") {
         alert("No Item is selected to Delete");
         return;
     }

     var RefNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RefNo')
     if (confirm("Are You sure to delete this record")) {



         $.ajax({
             type: "POST",
             data: '{"RefNo":"' + RefNo + '"}',
             url: "manageitemissuereceive.aspx/Delete",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);

                 if (obj.Status == "0") {

                     alert("An error occured during Transaction. Please try again Later");

                 }
                 else {
                     alert("Item Issue is Deleted successfully.");
                 }
                 BindGrid();

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {

             }
         });

     }


 }
 );







            }




        });

    </script>
    <form runat="server" id="formID" method="post">

          <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <asp:HiddenField ID="hdnDate" runat="server" />
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Manage Item Issue/Receive</h3>
                    </div>
                    <div class="x_panel">
                        <div class="form-group">
                            <div class="youhave">

                                  <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>
                                                    Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>



                                <table id="jQGridDemo">
                                </table>
                                <table cellspacing="0" cellpadding="0" style="margin-top: 5px">
                                    <tr>

                                        <td>
                                            <div id="btnNew" class="btn btn-primary">
                                                <i class="fa fa-external-link"></i>New
                                            </div>
                                        </td>

                                        <td>
                                            <div id="btnEdit" class="btn btn-success">
                                                <i class="fa fa-edit m-right-xs"></i>Edit
                                            </div>
                                        </td>

                                        <td>
                                            <div id="btnDelete" class="btn btn-danger" >
                                                <i class="fa fa-trash m-right-xs"></i>Delete
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="jQGridDemoPager">
                                </div>
                            </div>
                            <div class="row" id="TransferDialog" style="display: none">

                                <style type="text/css">
                                    .table > tbody > tr > td {
                                        padding: 4px;
                                    }

                                    .table > thead > tr > td {
                                        padding: 4px;
                                    }
                                </style>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel" style="padding-top: 0px">



                                        <div class="row">
                                            <div class="col-md-12">
                                                <table style="width: 100%">
                                                    <tr>

                                                        <td style="vertical-align: top; padding: 0px 2px">
                                                            <div class="x_panel" style="padding-bottom: 0px">
                                                                <div class="x_title">
                                                                    <h2>Item Issue/Receive Info</h2>

                                                                    <div class="clearfix"></div>

                                                                </div>


                                                                <div class="x_content">

                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                        
                                                                                <td>
                                                                                <label class="control-label">Issue No: </label></td>
                                                                            <td  >

                                                                             <input type="text" id="txtRefNo" value="Auto"  disabled="disabled"/>  

                                                                            </td>
                                                                            
                                                                            
                                                                                <td>
                                                                                <label class="control-label">Date: </label></td>
                                                                            <td  >

                                                                               
                                                                             <input type="text" id="txtDate"  />  
                                                                            </td>


                                                                              <td>
                                                                                <label class="control-label">Godown From: </label></td>
                                                                            <td  >

                                                                               
<select id="ddlGodown"></select>
                                                                            </td>




                                                                                              <td>
                                                                                <label class="control-label">Godown To: </label></td>
                                                                            <td  >

                                                                               
<select id="ddlGodownTo"></select>
                                                                            </td>



                                                                        </tr>

                                                                       

                                                                    </table>

                                                                </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="x_panel" style="background: seashell; padding: 0px">

                                                    <div class="x_content" style="padding-bottom: 0px">

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Item/Code</th>
                                                                                <th>Code</th>
                                                                                <th>Name</th>
                                                                                <th>Qty</th>
                                                                                <th>Rate</th>
                                                                                <th>MRP</th>
                                                                                <th>Amount</th>
                                                                            
                                                                            </tr>

                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>




                                                                                    <select style="width: 154px" id="ddlProducts" class="form-control"></select>
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" id="txtCode" readonly="readonly" class="form-control customTextBox" />
                                                                                </td>
                                                                                <td>
                                                                                    <input id="txtName" type="text" class="form-control customTextBox" style="width: 120px" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtQty" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtRate" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtAmount" class="form-control customTextBox" readonly="readonly" /></td>
                                                                                
                                                                                <td>
                                                                                    <button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td>
                                                                            </tr>
                                                                        </tbody>

                                                                    </table>
                                                                </td>
                                                            </tr>


                                                        </table>


                                                    </div>
                                                </div>


                                            </div>




                                            <div class="col-md-12">
                                                <div class="x_panel" style="max-height: 200px; overflow-y: scroll; min-height: 200px">
                                                    <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                                    <div class="x_content">

                                                        <table class="table table-striped" style="font-size: 12px">
                                                            <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Qty</th>
                                                                    <th>Rate</th>
                                                                    <th>MRP</th>
                                                                    <th>Amount</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbKitProducts">
                                                            </tbody>
                                                        </table>



                                                    </div>
                                                </div>


                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div id="btnSave" class="btn btn-success"><i class="fa fa-save"></i>Save</div>

                                                                    </td>
                                                                    <td>
                                                                        <button id="btnCancelDialog" class="btn btn-danger"><i class="fa fa-mail-reply-all"></i>Cancel</button>

                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>


                                        </div>

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->



            </div>
    </form>
    <script type="text/javascript">
        function BindGrid() {

            var DateFrom = $("#txtDateFrom").val();
            var DateTo = $("#txtDateTo").val();
            jQuery("#jQGridDemo").GridUnload();
            jQuery("#jQGridDemo").jqGrid({
                url: 'handlers/ManageItemIssue.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['RefNo', 'Date', 'Godown','GodownTo'],



                colModel: [
                            { name: 'RefNo', key: true, index: 'RefNo', width: 100, stype: 'text', sorttype: 'int' },

                            { name: 'strDate', index: 'strDate', width: 100, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'Godown_ID', index: 'Godown_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                           { name: 'GodownTo', index: 'GodownTo', width: 100, stype: 'text', sorttype: 'int', hidden: true },



                ],
                rowNum: 10,

                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'Kit_ID',
                viewrecords: true,
                height: "100%",
                width: "400px",
                sortorder: 'asc',
                caption: "Item Issue List",

                editurl: 'handlers/ManageKits.ashx',
                ignoreCase: true,
                toolbar: [true, "top"],


            });

            var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



            $("#jQGridDemo").jqGrid('setGridParam',
    {
        onSelectRow: function (rowid, iRow, iCol, e) {
            ResetControls();

            m_TransferID = 0;


            m_TransferID = $('#jQGridDemo').jqGrid('getCell', rowid, 'RefNo');

            $("#txtDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strDate'));
            $("#ddlGodown option[value='" + $('#jQGridDemo').jqGrid('getCell', rowid, 'Godown_ID') + "']").prop("selected", true);
            $("#ddlGodownTo option[value='" + $('#jQGridDemo').jqGrid('getCell', rowid, 'GodownTo') + "']").prop("selected", true);

            $("#txtRefNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'RefNo'));

        }
    });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>

