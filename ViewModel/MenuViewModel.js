﻿/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />




function Group(data) {
    this.GroupId = ko.observable(data.GroupId);
    this.Title = ko.observable(data.Title);
    this.ImageUrl = ko.observable(data.ImageUrl);
    this.IsActive = ko.observable(data.IsActive);
}


function SubGroup(data) {
    this.SubGroupId = ko.observable(data.SubGroupId);
    this.Title = ko.observable(data.Title);
    this.IsActive = ko.observable(data.IsActive);
    this.InActiveLink = ko.observable(data.InActiveLink);
    this.ActiveLink = ko.observable(data.ActiveLink);

}



function Product(data) {
    this.ItemID = ko.observable(data.ItemID);
    this.ItemImage = ko.observable(data.ItemImage);
    this.Item_Name = ko.observable(data.Item_Name);
    this.Item_Code = ko.observable(data.Item_Code);
    this.Sale_Rate = ko.observable(data.Sale_Rate);
    this.Tax_Code = ko.observable(data.Tax_Code);
    this.Tax_ID = ko.observable(data.Tax_ID);
    this.SurVal = ko.observable(data.SurVal);
    this.Likes = ko.observable(data.Likes);
    this.Qty = ko.observable(data.Qty);
    this.SubTotal = ko.observable(data.SubTotal);
    this.Remarks = ko.observable(data.Remarks);
  
}

//function OrderProduct(data) {
//    this.ItemID = ko.observable(data.ItemID);
//    this.ItemImage = ko.observable(data.ItemImage);
//    this.Item_Name = ko.observable(data.Item_Name);
//    this.Item_Code = ko.observable(data.Item_Code);
//    this.Sale_Rate = ko.observable(data.Sale_Rate);
//    this.Tax_Code = ko.observable(data.Tax_Code);
//    this.Tax_ID = ko.observable(data.Tax_ID);
//    this.SurVal = ko.observable(data.SurVal);
//    this.Likes = ko.observable(data.Likes);
//     
//}



function MenuViewModel() {
    var self = this;
    self.ItemID = ko.observable();
    self.ItemImage = ko.observable();
    self.Item_Name = ko.observable();
    self.Item_Code = ko.observable();
    self.Sale_Rate = ko.observable();
    self.Tax_Code = ko.observable();
    self.SurVal = ko.observable();
    self.Likes = ko.observable();
    self.Tax_ID = ko.observable();
    self.Keyword = ko.observable();
    self.SerTax = ko.observable(0);
    self.Products = ko.observableArray([]);
    self.Groups = ko.observableArray([]);
    self.SubGroups = ko.observableArray([]);
    self.OrderProducts = ko.observableArray([]);
    self.GroupName = ko.observable();
    self.SubGroupName = ko.observable();
    self.InActiveLink = ko.observable();
    self.ActiveLink = ko.observable();
    self.Remarks = ko.observable();



    self.GrossAmount = ko.computed(function () {
        var total =0;
         ko.utils.arrayForEach(self.OrderProducts(), function (item) {

             var value = parseFloat(item.Sale_Rate()) * parseFloat(item.Qty());
                    if (!isNaN(value)) {
                        total += value;
                    }
        });
        return total.toFixed(2);
    }, self);

  



    self.SerAmount = ko.computed(function () {
        var total = 0;
        total = self.GrossAmount() * self.SerTax() / 100;
        return total.toFixed(2);
    }, self);


    //self.VatAmount = ko.observable(0);


    self.VatAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.OrderProducts(), function (item) {



            var amount1 = (parseFloat(item.Sale_Rate()) * parseFloat(item.Qty())) * item.Tax_Code() / 100;

            var amount2 = parseFloat(amount1) * parseFloat(item.SurVal()) / 100;

            


                     var value = amount1 + amount2;
                   if (!isNaN(value)) {
                          total += value;
                        }
        });
        return total.toFixed(2);
    }, self);

   
   
    self.NetAmount = ko.computed(function () {
        var total = 0;
        total = parseFloat(self.GrossAmount()) + parseFloat(self.SerAmount()) + parseFloat(self.VatAmount());
        return total.toFixed(2);
    }, self);
   


    self.GroupId = ko.observable();
    self.ProductId = ko.observable();
    self.OrderProductId = ko.observable();
    self.SubGroupId = ko.observable();
    self.Qty = ko.observable();

    self.SubTotal= ko.observable();

    //-----------SEARCH PRODUCT-------------------------------


    $("#imgOrder,#cart").click(
            function () {

                if (self.OrderProducts().length == 0) {

                    alert("Cart is Empty. Please choose Items");
                    return;
                }

                $("#dvOrders").show();
                $("#dvProducts").hide();


            }
            );


    self.PlaceOrder = function (product) {


        if (self.OrderProducts().length == 0) {

            alert("No Product in the List. Please choose Items");
            return;
        }
        



        $.ajax({
            type: "POST",
            url: 'menucard.aspx/PlaceOrder',
            //            data: '{ "GrossAmount": "' + self.GrossAmount() + '", "SerTaxPer": "' + self.SerTax() + '", "SerTaxAmt": "' + self.SerAmount() + '", "Vat": "' + self.VatAmount() + '", "NetAmount": "' + self.NetAmount() + '" , "data": "' + self.OrderProducts + '"}',
            data: ko.toJSON({ GrossAmount: self.GrossAmount(), SerTaxPer: self.SerTax(), SerTaxAmt: self.SerAmount(), Vat: self.VatAmount(), NetAmount: self.NetAmount(), data: self.OrderProducts }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var obj = jQuery.parseJSON(results.d);

                $("#dvOrders").hide();
                $("#dvProducts").show();
                self.OrderProducts([]);

                $("#dvVerification").css("display", "none");
                $("#tbCalculations").css("display", "block");

                $("#txtUserName").val("");
                $("#txtPassword").val("");


                alert("Thanks for placing order with NikBakers. Your Order Number Is:" + obj.Order.Order_No + ". Please go to billing counter and get your Order Billed.");
            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }


    self.GetFavourites = function (product) {
        $("#dvOrders").hide();
        $("#dvProducts").show();

        $.ajax({
            type: "POST",
            url: 'menucard.aspx/GetFavourites',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var products = $.map(results.d, function (item) {

                    return new Product(item)
                });
                self.Products(products);
                self.SubGroupName("Favourites");
                self.GroupName("NikBakers");


            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }


    self.SearchProduct = function (product) {
        $("#dvOrders").hide();
        $("#dvProducts").show();

        fnFetchProducts(0,product.Keyword(),product.Keyword());

       



    }

    
    //-------FETCH ALL PRODUCTS BY SUBGROUP---------------------


    function fnFetchProducts(SGID,Keyword,Name) {



        $.ajax({
            type: "POST",
            url: 'menucard.aspx/FetchProducts',
            data: '{ "SubGroupId": "' + SGID + '", "Keyword": "' + Keyword + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {

                var products = $.map(results.d, function (item) {


                    return new Product(item)


                });

                self.Products(products);
                self.SubGroupName(Name);


            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }


    self.GetProductList = function (subgroup) {

        self.SubGroups()[0].ActiveLink(false);
        self.SubGroups()[0].InActiveLink(true);

     
        $("#dvOrders").hide();
        $("#dvProducts").show();

        ko.utils.arrayForEach(self.SubGroups(), function (sg) {

             sg.ActiveLink(false);
           sg.InActiveLink (true);

        });



     
 
        subgroup.ActiveLink(true);
        subgroup.InActiveLink(false);
       

        fnFetchProducts(subgroup.SubGroupId(), "", subgroup.Title());




    }


    //--AMOUNT CALCULATIONS----------------------------


    function CommonCalculations() {
    
    }


    self.LikeItem = function (product) {

         
        $.ajax({
            type: "POST",
            url: 'menucard.aspx/LikeItem',
            data: '{ "ItemId": "' + product.ItemID() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {

                var obj = jQuery.parseJSON(results.d);

                product.Likes(obj.Status)

            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }

    //ADD PRODUCTS TO LIST----------------------------------------

    self.AddProductToList = function (product) {

        var match = ko.utils.arrayFirst(self.OrderProducts(), function (line) {
            return line.ItemID() === product.ItemID();

        });

        if (!match) {


            var ST = product.Qty() * product.Sale_Rate();

            self.OrderProducts.push(new Product({
                ItemID: product.ItemID(),
                ItemImage: product.ItemImage(),
                Item_Name: product.Item_Name(),
                Item_Code: product.Item_Code(),
                Sale_Rate: product.Sale_Rate(),
                Tax_Code: product.Tax_Code(),
                Tax_ID: product.Tax_ID(),
                SurVal: product.SurVal(),
                Likes: product.Likes(),
                Qty: product.Qty(),
                SubTotal: ST,
                Remarks:product.Remarks()


            }));



            $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " ADDED SUCCESSFULLY!" });

        }
        else {

            match.Qty(match.Qty() + 1)
            match.SubTotal(match.Qty() * match.Sale_Rate());
          


            $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " QUANTITY INCREASED BY ONE!" });

        }

    }

    //----INCREMENT PRODUCT QUANTITY-----------------------
    self.IncrementQuantity = function (product) {
    
    //    alert($(this));
        product.Qty(product.Qty() + 1)
        product.SubTotal(product.Qty() * product.Sale_Rate());


    }


    //----INCREMENT PRODUCT QUANTITY-----------------------
    self.DecrementQuantity = function (product) {

        if (product.Qty() == 1) {

            self.OrderProducts.remove(product)
            $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " REMOVED FROM YOUR ORDER !" });
       

            return;
        }
        product.Qty(product.Qty() - 1)
        product.SubTotal(product.Qty() * product.Sale_Rate());

    }


    self.RemoveProduct = function (product) {
        self.OrderProducts.remove(product);
        $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " REMOVED FROM YOUR ORDER !" });
       

    }




    //---FETCH ALL SUBGROUPS BY GROUPS----------------------------

    self.GetByGroup = function (group) {
        self.Products([]);

        $("#dvOrders").hide();
        $("#dvProducts").show();

        $.ajax({
            type: "POST",
            url: 'menucard.aspx/FetchSubGroups',
            data: '{ "GroupId": "' + group.GroupId() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var subgroups = $.map(results.d, function (item) {
                    return new SubGroup(item)
                });
                self.SubGroups(subgroups);
                self.GroupName(group.Title());
                 




                $("#dvOrders").hide();
                $("#dvProducts").show();
                self.SubGroups()[0].ActiveLink(true);
                self.SubGroups()[0].InActiveLink(false);
                fnFetchProducts(self.SubGroups()[0].SubGroupId(), "", self.SubGroups()[0].Title());


                




            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });

    };




    //------------------FETCH ALL GROUPS ON PAGE LOAD-------------------

    $.ajax({
        type: "POST",
        url: 'menucard.aspx/FetchGroups',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var groups = $.map(results.d, function (item) {

                self.SerTax(item.SerTax);

                return new Group(item)
            });
            self.Groups(groups);





            $.ajax({
                type: "POST",
                url: 'menucard.aspx/FetchSubGroups',
                data: '{ "GroupId": "' + self.Groups()[0].GroupId() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (results) {
                    var subgroups = $.map(results.d, function (item) {
                       
                        return new SubGroup(item)
                    });




                    self.SubGroups(subgroups);
                    self.GroupName(self.Groups()[0].Title());



                    $("#dvOrders").hide();
                    $("#dvProducts").show();

                    self.SubGroups()[0].ActiveLink(true);
                    self.SubGroups()[0].InActiveLink(false);

                    fnFetchProducts(self.SubGroups()[0].SubGroupId(), "", self.SubGroups()[0].Title());







                },
                error: function (err) {
                    alert(err.status + " - " + err.statusText);
                }
            });









        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });

    //--------------------------------------------------------------------------


}


$(document).ready(function () {
    ko.applyBindings(new MenuViewModel());
});