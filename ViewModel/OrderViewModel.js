﻿
/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />
var BillBasicType = "";
var RountAmount = false;

function DeliveryMode(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}

function DeliveryTime(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}




function PaymentMode(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}

function Prefix(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}

function Employee(data) {
    this.Name = ko.observable(data.Name);
    this.Code = ko.observable(data.Code);

}

function Customer(data) {
    this.Customer_ID = ko.observable(data.Customer_ID);
    this.Customer_Name = ko.observable(data.Customer_Name);
    this.Address_1 = ko.observable(data.Address_1);


}


function Product(data) {

    this.Code = ko.observable(data.Code);
    this.Name = ko.observable(data.Name);
    this.Weight = ko.observable(data.Weight);
    this.Qty = ko.observable(data.Qty);
    this.MRP = ko.observable(data.MRP);
    this.Rate = ko.observable(data.Rate);
    this.Amount = ko.observable(data.Amount);
    this.Vat = ko.observable(data.Vat);
    this.TaxPer = ko.observable(data.TaxPer);
    this.Surcharge = ko.observable(data.Surcharge);
}




function OrderViewModel() {
    var self = this;

   

    self.DispatchedQty = ko.observable();

    self.OrderNo = ko.observable(0);
    self.OrderDate = ko.observable("");
    self.Customer_ID = ko.observable();
    self.CustomerName = ko.observable();
    self.Address = ko.observable();
    self.MobileNo = ko.observable();
    self.Employee = ko.observable("");
    self.CreditCardNumber = ko.observable("");

    self.DeliveryType = ko.observable("");
    self.DeliveryTime = ko.observable("");
    self.DelTime = ko.observable("");
    self.ManualOrderNo = ko.observable("");
     self.DeliveryAddress = ko.observable("");
    
    self.Advance = ko.observable(0);
   
    self.Remarks = ko.observable();
    self.OrderValue = ko.observable();
    

    self.OrderType = ko.observable("1");

    self.IsCakeItem = function (val) {

        $("#txtSearch").val("");

        var retVal = false;
        if (val == 1) {

            if (self.OrderType() == "1") {


                retVal = true;

            }
            else {
                retVal = false;

            }


        }
        else {
            if (self.OrderType() == "1") {

                $("#dvSearchResult").css("display", "none");
                retVal = false;

            }
            else {
                retVal = true;

            }

        }
        return retVal;


    };

//    self.IsCakeItem = ko.computed(function () {
//        var retVal = false;


//         if (self.OrderType() == "1") {

//          
//            retVal = true;

//        }
//    
//        return retVal;
//    }, self);



    self.PaymentMode = ko.observable("");
    self.Employees = ko.observableArray([]);

    self.DeliveryTimes = ko.observableArray([]);


    self.DeliveryTimes.push(new DeliveryTime({ Text: "09:00", Value: "09:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "09:30", Value: "09:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "10:00", Value: "10:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "10:30", Value: "10:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "11:00", Value: "11:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "11:30", Value: "11:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "12:00", Value: "12:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "12:30", Value: "12:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "13:00", Value: "13:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "13:30", Value: "13:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "14:00", Value: "14:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "14:30", Value: "14:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "15:00", Value: "15:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "15:30", Value: "15:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "16:00", Value: "16:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "16:30", Value: "16:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "17:00", Value: "17:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "17:30", Value: "17:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "18:00", Value: "18:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "18:30", Value: "18:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "19:00", Value: "19:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "19:30", Value: "19:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "20:00", Value: "20:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "20:30", Value: "20:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "21:00", Value: "21:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "21:30", Value: "21:30" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "22:00", Value: "22:00" }));
    self.DeliveryTimes.push(new DeliveryTime({ Text: "22:30", Value: "22:30" }));

   


    self.DeliveryModes = ko.observableArray([]);
    self.DeliveryModes.push(new DeliveryMode({
        Text: "By Hand",
        Value: "ByHand"

    }));
    self.DeliveryModes.push(new DeliveryMode({
        Text: "By Delivery",
        Value: "ByDelivery"

    }));


    self.PaymentModes = ko.observableArray([]);
    self.PaymentModes.push(new PaymentMode({
        Text: "Cash",
        Value: "Cash"

    }));
    self.PaymentModes.push(new PaymentMode({
        Text: "CreditCard",
        Value: "CreditCard"

    }));

    self.OrderProducts = ko.observableArray([]);
    self.ItemCode = ko.observable();
    self.ItemName = ko.observable();
    self.Weight = ko.observable();
    self.Qty = ko.observable(0);
    self.Rate = ko.observable(0);
    self.MRP = ko.observable(0);
    self.TaxAmount = ko.observable(0);

    self.TaxPer = ko.observable(0);
    self.Surcharge = ko.observable(0);
    self.BillAmount = ko.observable(0);

   

   
    self.Amount = ko.computed(function () {
        var total = 0;


        if (self.OrderType() == 1) {
            total = self.Qty() * self.Weight()* self.Rate();
        }
        else
        {
            total = self.Qty() * self.Rate();

        }
        return total.toFixed(2);
    }, self);




    self.TotalTax = ko.computed(function () {
        var total = 0;


        var TaxAmt = 0;

        if (BillBasicType == "I") {

            var Surval = (self.TaxPer() * self.Surcharge()) / 100;
            TaxAmt = ((self.Qty() * self.Rate()) * self.TaxPer()) / (100 + parseFloat(self.TaxPer())+Surval);
             
        }
        else {
            TaxAmt = (self.Qty() * self.Rate()) * self.TaxPer() / 100;

        }
        var SurchargeAmt = (TaxAmt * self.Surcharge()) / 100;



        total = TaxAmt + SurchargeAmt;



        return total.toFixed(2);
    }, self);



    self.GrossAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.OrderProducts(), function (item) {

            var value = parseFloat(item.Amount()); //parseFloat(item.Rate()) * parseFloat(item.Qty());
            if (!isNaN(value)) {
                total += value;
            }
        });
        return total.toFixed(2);
    }, self);




    self.DisPer = ko.observable(0);


    self.DisAmt = ko.computed(function () {
        var total = 0;
        total = self.GrossAmount() * self.DisPer() / 100;
     
        return total.toFixed(2);
    }, self);

    self.AfterDisAmt = ko.computed(function () {
        var total = 0;
        total = self.GrossAmount() - self.DisAmt();
        return total.toFixed(2);
    }, self);


    self.VatAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.OrderProducts(), function (item) {

            var DisAmt = (item.Qty() * item.Rate()) * self.DisPer() / 100;
            var TotalAmt = (item.Qty() * item.Rate()) - parseFloat(DisAmt);
            var TaxAmt = 0;

            if (BillBasicType == "I") {

                TaxAmt = ((TotalAmt) * item.TaxPer()) / (100 + parseFloat(item.TaxPer()));
                 
            }
            else {
                TaxAmt = (TotalAmt) * item.TaxPer() / 100;

            }
            

            var SurchargeAmt = (TaxAmt * item.Surcharge()) / 100;



            total += TaxAmt + SurchargeAmt;

            //            var DisAmt = (item.Qty() * item.Rate()) * self.DisPer() / 100;
            //            var TotalAmt = (item.Qty() * item.Rate()) - parseFloat(DisAmt);

            //            var TaxAmt = 0;

            //            if (BillBasicType == "I") {




            //                TaxAmt = ((TotalAmt) * self.TaxPer()) / (100 + parseFloat(self.TaxPer()));

            //            }
            //            else {
            //                TaxAmt = (TotalAmt) * self.TaxPer() / 100;

            //            }


            //            var SurchargeAmt = (TaxAmt * item.Surcharge()) / 100;



            //            total += TaxAmt + SurchargeAmt;




        });
        return total.toFixed(2);
    }, self);


    self.NetAmount = ko.computed(function () {
        var total = 0;

        var VatAmt = self.VatAmount();
        if (BillBasicType == "I") {
            VatAmt = 0;
        }
        if (RountAmount == 1) {
            total = Math.round(parseFloat(self.AfterDisAmt()) + parseFloat(VatAmt));
        }
        else {
            total = parseFloat(self.AfterDisAmt()) + parseFloat(VatAmt);
        }
        return total.toFixed(2);
    }, self);

    self.LeftPayRecd = ko.computed(function () {
        var total = 0;
        total = self.NetAmount() - self.Advance();
        return total.toFixed(2);
    }, self);

    self.BillValue = ko.observable(0);


    self.EditProduct = function (product)
    {
    
        $("#txtSearch").val(product.Name());
        self.ItemCode(product.Code());
        self.ItemName(product.Name());

        self.Qty(product.Qty());
     
        self.Weight(product.Weight());
        self.Qty(product.Qty());
        self.MRP(product.MRP());
        self.Rate(product.Rate());
        self.Amount(product.Amount());
       
       


    }

    self.NewOrder = function () {
        ClearData();
       
        //Ajax Function Get By Order No..Return-- ObjBooking and ProductData On Success Show Dialog


        $('#orderDialog').dialog({ autoOpen: true,

            width: 950,
            resizable: false,
            modal: true
        });



    }


    self.SearchCustomer = function (product) {



        $.ajax({
            type: "POST",
            url: "placeorder.aspx/LoadUserControl",
            data: "{message: '" + '' + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $("#dvCustomerSearch").html(r.d);


                $('#dvCustomerSearch').dialog({ autoOpen: true,

                    width: 800,
                    resizable: false,
                    modal: true
                });

            }
        });


     


    };


    self.CancelOrder = function (product) {


        ClearData();
        $('#orderDialog').dialog("close");
    }



    self.EditOrder = function () {



        self.OrderNo(m_OrderNo);

        if (self.OrderNo() == "0") {
            alert("No Order is selected for editing");
            return;
        }


        $.uiLock('');

        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/GetByOrderNo',
            data: ko.toJSON({ OrderNo: m_OrderNo }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var obj = jQuery.parseJSON(results.d);
                self.OrderNo(m_OrderNo);
                self.OrderDate(obj.Booking.strOD);
                self.Customer_ID(obj.Booking.Customer_ID);
                self.CustomerName(obj.Booking.CustomerName);
                self.CreditCardNumber(obj.Booking.CreditCardNumber);
                self.MobileNo(obj.Booking.MobileNo);
                self.DelTime(obj.Booking.DelTime);
                self.ManualOrderNo(obj.Booking.ManualOrderNo);

                self.DeliveryType(obj.Booking.DeliveryType);
                self.DeliveryTime(obj.Booking.strDD);
                self.DeliveryAddress(obj.Booking.DeliveryAddress);
                self.Address(obj.Booking.Address);
                self.Advance(obj.Booking.Advance);
                self.PaymentMode(obj.Booking.PaymentMode);
                self.DisPer(obj.Booking.DisPer);
                self.Employee(obj.Booking.Ecode);
                self.Remarks(obj.Booking.Remarks);


                var products = $.map(obj.BookingData, function (item) {
                    return new Product(item)
                });

                self.OrderProducts(products);

                var DisPerc = self.DisAmt() * 100 / self.GrossAmount();
                self.DisPer(DisPerc.toFixed(2));
                $.uiUnlock();
            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }

        });

        //Ajax Function Get By Order No..Return-- ObjBooking and ProductData On Success Show Dialog


        $('#orderDialog').dialog({ autoOpen: true,

            width: 950,
            resizable: false,
            modal: true
        });

    };


    function HasErrors2() {


        var m_errors = "";

        if (self.ItemCode() == ""  ) {

            m_errors += "Please enter ItemCode.\r\n";
        }

        if (!jQuery.isNumeric(self.Weight()) ) {

            m_errors += "Invalid Weight.\r\n";
        }

        if (!jQuery.isNumeric(self.Qty())) {

            m_errors += "Invalid Qty.\r\n";
        }


        if (!jQuery.isNumeric(self.MRP())) {

            m_errors += "Invalid MRP.\r\n";
        }

        if (!jQuery.isNumeric(self.Rate())) {

            m_errors += "Invalid Rate.\r\n";
        }


        return m_errors;

    }

    function HasErrors() {

        var m_errors = "";
        if (self.Advance()>0 && self.PaymentMode() == null) {
            m_errors += "Please choose Payment Mode.\r\n";
        }
        if (self.DeliveryType() == null) {
            m_errors += "Please choose Delivery Mode.\r\n";
        }
        if (self.Customer_ID() <= 0 || self.Customer_ID() == "") 
        {

            m_errors = "Please select Customer.\r\n";
        }


        //if (self.OrderDate() == "") {
        //    m_errors += "Please choose Order Date.\r\n";
        //}

        if (self.DeliveryTime() == "") {
            m_errors += "Please choose Delivery Date.\r\n";
        }



        if (self.Employee() == null) {
            m_errors += "Please choose Employee.\r\n";
        }
 

        if (self.DeliveryType() == "ByDelivery"  && self.DeliveryAddress()=="") {
           
            m_errors += "Please enter Venue.\r\n";
        }

        return m_errors;
    
    }

    self.PlaceOrder = function (product) {

        

        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }

        var errors = HasErrors();


        if (errors != "") {

            alert(errors);
            return;
        }




        if (self.OrderProducts().length == "0") {
            alert("There is no product is selected to place order.");
            return;
        }

			$.uiLock('');
		
        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/InsertUpdate',
            data: ko.toJSON({ booking: product, OrderProducts: self.OrderProducts }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var obj = jQuery.parseJSON(results.d);




                BindGrid();

                $('#orderDialog').dialog("close");

                if (product.OrderNo() == "0") {
                    alert("Thanks for placing order with Monica's. Your Order Number Is:" + obj.Booking.OrderNo + ".");

                }
                else {

                    alert("Order Updated Successfully");
                }
                ClearData();
                $('#orderDialog').dialog("close");
				$.uiUnlock();
            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }

  


      

    function ClearData() {

     
        self.OrderProducts([]);
        self.OrderNo(0);
        self.OrderDate("");
        self.Customer_ID(0);
        self.CustomerName("");
        self.Address("");
        self.MobileNo("")    
        self.Employee(0);

        self.ManualOrderNo("");
      
        self.DeliveryType("");
        self.DeliveryTime("");
        self.DeliveryAddress("");
       
        self.Advance(0);
        self.Remarks("");
        self.OrderValue(0);
        self.OrderType("1");
        self.PaymentMode("")

        self.ItemCode("");
       
        self.ItemName("");
        self.Weight(0);
        self.Qty(0);
        self.Rate(0);
        self.MRP(0);
        self.TaxAmount(0);
        self.TaxPer(0);
        self.Surcharge(0);
        self.BillAmount(0);


        self.DisPer(0);
   
        
       


    }



    self.RemoveProduct = function (product) {
 
        self.OrderProducts.remove(product);

      

        //        $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " REMOVED FROM YOUR ORDER !" });


    }



    self.AddProductToList = function (product) {


        var errors = HasErrors2();


        if (errors != "") {

            alert(errors);
            return;
        }


        $("#txtSearch").val("").focus();

        var match = ko.utils.arrayFirst(self.OrderProducts(), function (line) {
            return line.Code() === product.ItemCode();

        });

        if (!match) {




            self.OrderProducts.push(new Product({
                Code: product.ItemCode(),
                Name: product.ItemName(),
                Weight: product.Weight(),
                Qty: product.Qty(),
                MRP: product.MRP(),
                Rate: product.Rate(),
                Amount: product.Amount(),
                Vat: product.TotalTax(),
                TaxPer: product.TaxPer(),
                Surcharge: product.Surcharge()

            }));


            self.ItemCode("");
            self.ItemName("");
            self.Weight(1);
            self.Qty(0);
            self.MRP(0);
            self.Rate(0);
            self.TotalTax(0);

        }
        else {

            
            self.OrderProducts.remove(match);


            self.OrderProducts.push(new Product({
                Code: product.ItemCode(),
                Name: product.ItemName(),
                Weight: product.Weight(),
                Qty: product.Qty(),
                MRP: product.MRP(),
                Rate: product.Rate(),
                Amount: product.Amount(),
                Vat: product.TotalTax(),
                TaxPer: product.TaxPer(),
                Surcharge: product.Surcharge()

            }));


            self.ItemCode("");
            self.ItemName("");
            self.Weight(1);
            self.Qty(0);
            self.MRP(0);
            self.Rate(0);
            self.TotalTax(0);

            alert("Item Code Already Added to List");
        }






    };



    self.errors = ko.validation.group([self.CustomerName, self.Address, self.MobileNo]);
    self.hasError = function () {
        return self.errors().length > 0;
    };


    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };




    $.ajax({
        type: "POST",
        url: 'placeorder.aspx/FetchEmployees',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var employees = $.map(results.d, function (item) {



                return new Employee(item)
            });


            self.Employees(employees);



        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });


    $.ajax({
        type: "POST",
        data: '{}',
        url: "placeorder.aspx/GetByItemType",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

            var obj = jQuery.parseJSON(msg.d);

            $("#ddlItem").html(obj.ProductOptions);

        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {


        }


    });




   

    $("#ddlItem").change(
    function () {
        self.ItemCode($("#ddlItem option:selected").attr("value"));
        self.ItemName($("#ddlItem option:selected").attr("Name"));
        self.Qty($("#ddlItem option:selected").attr("Qty"));
        self.MRP($("#ddlItem option:selected").attr("MRP"));
        self.Rate($("#ddlItem option:selected").attr("Rate"));
       self.TaxPer($("#ddlItem option:selected").attr("TaxPer"));
       self.Surcharge($("#ddlItem option:selected").attr("SurValue"));

        self.Weight(1);



    });



    $('#txtCustomerSearch').keydown(function (e) {
        var $listItems = $("#tbCustSearchResult li");

        var key = e.keyCode,
            $selected = $listItems.filter('.selected'),
            $current;
        if (key == 13) {

            self.MobileNo($selected.attr("phone"));
            self.CustomerName($selected.attr("cname"));
            self.Address($selected.attr("addr"));
            self.Customer_ID($selected.attr("cid"));

            $("#dvCustSearchResult").css("display", "none");
            $("#txtCustomerSearch").val("").focus();

        }



        if (key != 40 && key != 38) return;

        $listItems.removeClass('selected');

        if (key == 40) // Down key
        {
            if (!$selected.length || $selected.is(':last-child')) {
                $current = $listItems.eq(0);
            }
            else {
                $current = $selected.next();
            }
        }
        else if (key == 38) // Up key
        {
            if (!$selected.length || $selected.is(':first-child')) {
                $current = $listItems.last();
            }
            else {
                $current = $selected.prev();
            }
        }

        $current.addClass('selected');
    });


    $(document).on("click", "a[name='anchorCustomerSearch']", function (event) {

        self.MobileNo($(this).attr("phone"));
        self.CustomerName($(this).attr("cname"));
        var m_PH = "";
        if ($(this).attr("phone").trim()!= "")
        {
            m_PH = "Phone No:" + $(this).attr("phone");

        }

        self.Address($(this).attr("addr") +" "+m_PH);
        self.Customer_ID($(this).attr("cid"));
       

        $("#dvCustSearchResult").css("display", "none");
    });



    $(document).on("click", "a[name='searchanchor']", function (event) {

        self.ItemCode($(this).attr("Code"));
        self.ItemName($(this).attr("PName"));
        self.Qty($(this).attr("Qty"));
        self.MRP($(this).attr("MRP"));
        self.Rate($(this).attr("Rate"));
        self.TaxPer($(this).attr("TaxPer"));
        self.Surcharge($(this).attr("SurValue"));

        self.Weight(1);

        $("#dvSearchResult").css("display", "none");
    });


    self.findCustomerDetail = function () {

      
        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/GetByMobileNo',
            data: '{ "MobileNo": "' + self.MobileNo() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.Customer.Customer_ID == 0) {
                    alert("Sorry, Mobile No is Not Registered with Our Database");
                    return;
                }


                self.Customer_ID(obj.Customer.Customer_ID);
                self.CustomerName(obj.Customer.Customer_Name);
                self.Address(obj.Customer.Address_1);


            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }






}



$(document).ready(function () {
    ko.applyBindingsWithValidation(new OrderViewModel());




    $("#txtAdvance").keyup(
        function ()
        {
            if (isNaN($(this).val())) {
                $(this).val(0);
            }

            if (parseFloat($(this).val()) > parseFloat($("#txtNetAmount").val()))
            {
                $(this).val($("#txtNetAmount").val());
            }

        }
        );


    (function ($) {
        $.fn.extend({
            donetyping: function (callback, timeout) {
                timeout = timeout || 1e3; // 1 second default timeout
                var timeoutReference,
                                        doneTyping = function (el) {
                                            if (!timeoutReference) return;
                                            timeoutReference = null;
                                            callback.call(el);
                                        };
                return this.each(function (i, el) {
                    var $el = $(el);
                    // Chrome Fix (Use keyup over keypress to detect backspace)
                    // thank you @palerdot
                    $el.is(':input') && $el.on('keyup keypress', function (e) {
                        // This catches the backspace button in chrome, but also prevents
                        // the event from triggering too premptively. Without this line,
                        // using tab/shift+tab will make the focused element fire the callback.
                        if (e.type == 'keyup' && e.keyCode != 8) return;

                        // Check if timeout has been set. If it has, "reset" the clock and
                        // start over again.
                        if (timeoutReference) clearTimeout(timeoutReference);
                        timeoutReference = setTimeout(function () {
                            // if we made it here, our timeout has elapsed. Fire the
                            // callback
                            doneTyping(el);
                        }, timeout);
                    }).on('blur', function () {
                        // If we can, fire the event since we're leaving the field
                        doneTyping(el);
                    });
                });
            }
        });
    })(jQuery);





    $.ajax({
        type: "POST",
        data: '{ }',
        url: "BillScreen.aspx/GetAllBillSetting",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

            var obj = jQuery.parseJSON(msg.d);
            BillBasicType = obj.setttingData.retail_bill;
            RountAmount = obj.setttingData.roundamt;

            if (BillBasicType == "I") {
                $("#trVatAmt").hide();
            }
            else {
                $("#trVatAmt").show();
            
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {


        }

    });


    $('#txtCustomerSearch').donetyping(function () {


        var Keyword = $(this).val();

        if (Keyword.length >= 3) {

            $("#dvCustSearchResult").css("display", "block");
            $("#tbCustSearchResult").html("<img src='images/searchloader.gif'>");

            $.ajax({
                type: "POST",
                data: '{"Keyword":"' + Keyword + '"}',
                url: "placeorder.aspx/CustomerKeywordSearch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    // var obj = jQuery.parseJSON(msg.d);


                    var Content = msg.d;
                    $("#tbCustSearchResult").html(Content);




                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });

        }
        else {
            $("#dvCustSearchResult").css("display", "none");

        }



    });




    $('#txtSearch').donetyping(function () {

        
        

        var Type = "NonCake";

        if ($('#rdoCakeOrder').is(':checked')) {

            Type = "Cake";
        }

        var Keyword = $(this).val();

        if (Keyword.length >= 3) {

            $("#dvSearchResult").css("display", "block");
            $("#tbSearchResult").html("<img src='images/searchloader.gif'>");

            $.ajax({
                type: "POST",
                data: '{"Keyword":"' + Keyword + '","Type":"' + Type+ '"}',
                url: "placeorder.aspx/KeywordSearch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    // var obj = jQuery.parseJSON(msg.d);


                    var Content = msg.d;
                    $("#tbSearchResult").html(Content);




                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });

        }
        else {
            $("#dvSearchResult").css("display", "none");

        }



    });

});