﻿ /// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
 /// <reference path="../Scripts/knockout.validation.js" />

 
function State(data)
{
    this.StateId = ko.observable(data.StateId);
    this.Title = ko.observable(data.Title);
    this.IsActive = ko.observable(data.IsActive);

}
 
 
function StateViewModel()
{
    
 
    var self = this;
    self.StateId = ko.observable().extend({ required: true,min: 10, digit: true});
    self.Title = ko.observable("Testing").extend({ required: true});
    self.IsActive = ko.observable(true);
  
    self.errors = ko.validation.group([self.Title,self.StateId]);
    self.hasError = function () {
        return self.errors().length > 0;
    };


    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    self.SaveState = function (obj) {

      self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }

       var objState= new State({
           StateId: self.StateId(),
            Title: self.Title(),
            IsActive: self.IsActive(),

        });
        
 
       $.ajax({
           type: "POST",
           url: 'managestates.aspx/SaveState',
           data: ko.toJSON({ data:objState}),
           contentType: "application/json; charset=utf-8",
           success: function (result) {
               alert(result.d);
           },
           error: function (err) {
               alert("Error");
               alert(err.status + " - " + err.statusText);
           }
       });




    };
   
  
   

}

$(document).ready(function () {
 
 
 
      ko.applyBindingsWithValidation(new StateViewModel());
});