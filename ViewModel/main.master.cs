﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class main : System.Web.UI.MasterPage
{
    DataSet ds = null;
    public string PageTitle { get; set; }
    public Int64  FreeDelAmt = 0;
    public Int64  MiniAmt =0;
    int m_TotalItems = 0;
    public int TotalItems { get { return m_TotalItems; } set { m_TotalItems = value; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["dummy"] == null)
        {
            this.Session["dummy"] = 1;
        }
        if (!IsPostBack)
        {
            BindOuterLinks();
            BindProductCategories();
            Settings objsett = new Settings();
            objsett = new SettingsDAL().GetSett();
            MiniAmt =Convert.ToInt64( objsett.MinimumCheckOutAmt);
            FreeDelAmt =Convert.ToInt64( objsett.FreeDeliveryAmt);
            //BindFeaturedCategories();
           // BindHeaderLinks();
        }
    }

    void BindFeaturedCategories()
    {
        List<Category> lst = new CategoryBLL().GetFeaturedCategories();
        string str = "";
        foreach (var item in lst)
        {
            str += "<li class='active'><a href='list.aspx?c=" + item.CategoryId + "' >" + item.Title + "</a></li>";
            //str += "<li class='active'><a href='list.aspx?s=" + item.CategoryId + "' style='color:black'>" + item.Title + "</a></li>";
        }
        //ltFeaturedCategories.Text = str;
    }

    void BindProductCategories()
    {
        repCategories.DataSource = new CategoryBLL().GetByParentId(0);
        repCategories.DataBind();

    }

   
    void BindOuterLinks()
    {
        ds = new Cms().GetAll();
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=1";
        repOuterLinks.DataSource = dv;
        repOuterLinks.DataBind();
    }


    void BindHeaderLinks()
    {
        
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=0 and ShowOn=0";
       // repHeaderLinks.DataSource = dv;
        //repHeaderLinks.DataBind();
    }
    
    
    public DataView GetInnerLinks(object ParentId)
    {     
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentPage=" + Convert.ToInt16(ParentId);
        return dv;
    }
}
