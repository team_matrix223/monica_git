﻿/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />


 
function Product(data) {
    this.ProcessingId = ko.observable(data.ProcessingId);
    this.Item_Code = ko.observable(data.Item_Code);
    this.Item_Name = ko.observable(data.Item_Name);
    this.ProcessingTime = ko.observable(data.ProcessingTime);
    this.Status = ko.observable(data.Status);
    this.TableNo = ko.observable(data.TableNo);
    this.ImageUrl = ko.observable(data.ImageUrl);
    this.BillNowPrefix = ko.observable(data.BillNowPrefix);
    this.RelatedItems = ko.observable(data.RelatedItems);
    this.DeliveryButtonText = ko.observable(data.DeliveryButtonText);
    this.IsDelivered = ko.observable(data.IsDelivered);
    this.IsNewDelivery = ko.observable(data.IsNewDelivery);
    this.IsNewOrStarted = ko.observable(data.IsNewOrStarted);
    this.ImagePath = ko.observable(data.ImagePath);
}


ko.observableArray.fn.distinct = function (prop) {

    var target = this;
    target.index = {};
    target.index[prop] = ko.observable({});

    ko.computed(function () {
        //rebuild index
        var propIndex = {};

        ko.utils.arrayForEach(target(), function (item) {
            var key = ko.utils.unwrapObservable(item[prop]);
            if (key) {
                propIndex[key] = propIndex[key] || [];
                propIndex[key].push(item);
            }
        });

        target.index[prop](propIndex);
    });

    return target;
};



function KOTViewModel() {
    var self = this;
    self.ProcessingId = ko.observable();
    self.Products = ko.observableArray([]).distinct('BillNowPrefix');
    self.Tables = ko.observableArray(["1", "2", "3", "0"]);

    self.getOrders = function (data) {


        return products();


    };


    function Parent(data) {

        this.BillNowPrefix = ko.observable(data.BillNowPrefix);

        this.TableNo = ko.observable(data.TableNo);
      


    }

 

    self.uniqueCountries = ko.computed(function () {

        var people = self.Products(),
          index = {},

          length = people.length,
          i, country;

        var uniqueCountries = [];
        var uniqueParent = ko.observableArray([]);
     

        for (i = 0; i < length; i++) {


            country = people[i].BillNowPrefix();
            var tableNo = people[i].TableNo();
            if (!index[country]) {
                index[country] = true;




                uniqueParent.push(new Parent({

                    TableNo: tableNo,
                    BillNowPrefix: country

                }));


                //uniqueCountries.push(country);
            }
        }


        return uniqueParent();
    });




 

      self.RefreshList = function () {
 
       
   
    $.ajax({
        type: "POST",
        url: 'kotdelivery.aspx/FetchProducts',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var products = $.map(results.d, function (item) {
              return new Product(item)
            });
            self.Products(products);


        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });
     

    }



self.StartProcessing = function (product) {

if(product.Status()=="Started")
{
alert("Processing already Started");
return;
}


var Status="Started";

$.ajax({
            type: "POST",
            url: 'kotdelivery.aspx/UpdateStatus',
            data: '{ "ProcessingId": "' + product.ProcessingId()+ '", "Status": "' +Status + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
          
 

              if(result.d=="1")
              {
            
              }

               
           



            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });
}




self.MarkDelivered = function (product) {

    if (product.IsNewOrStarted() == true) {
        alert("Item is not ready for Delivery");
        return;
    }
    else if (product.IsDelivered() == true) {
        alert("Item Already Marked Delivered");
        return;
    }



    var match = ko.utils.arrayFirst(self.uniqueCountries(), function (line) {
        return line.BillNowPrefix() === product.BillNowPrefix();

    });

    $.uiLock('');


    var Status = "Delivered";

    $.ajax({
        type: "POST",
        url: 'kotdelivery.aspx/MarkDelivered',
        data: '{ "ProcessingId": "' + product.ProcessingId() + '","TableNo": "' + match.TableNo() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {

            var products = $.map(results.d, function (item) {
                return new Product(item)
            });
            self.Products(products);

            $.uiUnlock();


        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });
}



    self.GetNewList = function () {


        $.ajax({
            type: "POST",
            url: 'kotdelivery.aspx/FetchProducts',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var products = $.map(results.d, function (item) {
                    return new Product(item)
                });
                self.Products(products);


            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });




    }

    setInterval(self.GetNewList, 10000);


  //------------------FETCH ALL GROUPS ON PAGE LOAD-------------------
  
    
    $.ajax({
        type: "POST",
        url: 'kotdelivery.aspx/FetchProducts',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var products = $.map(results.d, function (item) {
              return new Product(item)
            });
            self.Products(products);


        },

//        error: function (xhr, ajaxOptions, thrownError) {

//                        var obj = jQuery.parseJSON(xhr.responseText);
//                        alert(obj.Message);
//                    }
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });
 

    //--------------------------------------------------------------------------


}


$(document).ready(function () {
    ko.applyBindings(new KOTViewModel());
});