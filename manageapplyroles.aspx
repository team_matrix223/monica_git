﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageapplyroles.aspx.cs" Inherits="manageapplyroles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="js/jquery.uilock.js"></script>
   
     
   
 <script language="javascript">


     function ShowHide(val) {
         $("#tr" + val).slideToggle(50);
         var img = $("#imgShowHide" + val);
         var url = $(img).attr("src");
         if (url == "images/icoplus.gif") {
             $(img).attr("src", "images/icominus.gif");
         }
         else {
             $(img).attr("src", "images/icoplus.gif");
         }
     }

     function ResetControls() {
         var txtTitle = $("#txtTitle");
         var btnAdd = $("#btnAdd");
         var btnUpdate = $("#btnUpdate");

         txtTitle.focus();
         txtTitle.val("");
         txtTitle.focus();
         btnAdd.css({ "display": "block" });
         btnAdd.html("Add Bank");


         $("#chkIsActive").prop("checked", "checked");

         $("#btnReset").css({ "display": "none" });

         $("#hdnId").val("0");
         validateForm("detach");
     }



     function TakeMeTop() {
         $("html, body").animate({ scrollTop: 0 }, 500);
     }

     function RefreshGrid() {
         $('#jQGridDemo').trigger('reloadGrid');

     }

     $(document).ready(
    function () {

        $("#btnAdd").click(
        function () {
            var btnAdd = $("#btnAdd");
            $.uiLock('');

            btnAdd.html("<img src='images/loader.gif' alt='loading...'/>")

            var Pid = [];
            var Rid = [];
            var Did = $("#ddlDesignation").val();

            $("input[name='Roles']:checked").each(
            function (x) {

                var pageId = $(this).attr("pid");
                var roleId = $(this).val();

                Pid[x] = pageId;
                Rid[x] = roleId;

            }

            );



            $.ajax({
                type: "POST",
                data: '{"Pid":"' + Pid + '", "Rid": "' + Rid + '","Did": "' + Did + '"}',
                url: "manageapplyroles.aspx/Insert",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    btnAdd.html('Assign Roles');

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == "1") {
                        alert("Roles Assigned Successfully");
                    }
                    else {
                        alert("Operation Failed");
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    btnAdd.html('Assign Roles');

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }



            });



        }
        );

        $("#ddlDesignation").change(
        function () {
            $("#dvDesRoles").html("");
            $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"designationId":"' + $(this).val() + '"}',
                url: "manageapplyroles.aspx/GetRolesByDesignation",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#dvDesRoles").html(obj.Data);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();

                }



            });



        }
        );

       
            $.ajax({
                type: "POST",
                data: '{}',
                url: "manageapplyroles.aspx/GetDesignations",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#ddlDesignation").html(obj.DesignationOptions);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }



            });


     
        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );

        $("#btnUpdate").click(
        function () {
            InsertUpdate();


        }
        );


        $("#btnAdd").click(
        function () {
            InsertUpdate();


        }
        );

        $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {

                 validateForm("detach");
                 var txtTitle = $("#txtTitle");
                 $("#hdnId").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BankId'));


                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                     $('#chkIsActive').prop('checked', true);
                 }
                 else {
                     $('#chkIsActive').prop('checked', false);

                 }
                 txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                 txtTitle.focus();
                 $("#btnAdd").css({ "display": "none" });
                 $("#btnUpdate").css({ "display": "block" });
                 $("#btnReset").css({ "display": "block" });
                 TakeMeTop();
             }
         });

        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '500');
    }
    );

</script>


<input type="hidden" id="hdnId" value="0"/>
    
     
  <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Assign Roles</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">


                          <form class="form-horizontal form-label-left"   runat="server" id="formID" method="post">
                               <asp:HiddenField ID="hdnRoles" runat="server"/>
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Assign Roles To Users </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                    
                    	<div class="form-group">
										<label class="control-label">Choose Designation</label>
										<div class="controls">
										
                                        <select id="ddlDesignation" style="height:25px;width:150px"></select>
										</div>
									</div>
                    
                    
                        			<div class="form-group">
										<label class="control-label">Roles:</label>
										<div class="controls">
										 
                                               <div id="dvDesRoles" style="max-height:500px;overflow-y:scroll;width:300px">
                                               
                                               </div>

                                           
										
                                        </div>
									</div>
									



                                    <div class="form-group">
									
                                      
 	<table>
                                    <tr>
                                    <td><div id="btnAdd"  class="btn btn-primary btn-small"  style="height:30px" >Assign Roles</div>
   </td>
      
        

                                    </tr>
                                    </table>

                                        </div>


                                         </form>
                                </div>
                            </div>
									  </form>
                        </div>
                    </div>
 
                 
                </div>
                <!-- /page content -->

               

            </div>
		 
       

</asp:Content>


