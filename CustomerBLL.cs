﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for CustomerBLL
/// </summary>
public class CustomerBLL
{

    public string KeywordSearch(string Keyword)
    {
        DataSet ds = new CustomerDAL().KeywordSearch(Keyword);
        StringBuilder str = new StringBuilder();

        if (ds.Tables[0].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {


                str.Append(string.Format("<li   ><a  cid='{0}'  cname='{1}' addr='{2}' phone='{3}'   name='anchorCustomerSearch'>{1} - {3} - {2}</a></li>", ds.Tables[0].Rows[i]["Customer_ID"].ToString(), ds.Tables[0].Rows[i]["Customer_Name"].ToString(), ds.Tables[0].Rows[i]["Address_1"].ToString(), ds.Tables[0].Rows[i]["Contact_No"].ToString()));
            }

        }
        return str.ToString();
    }

    public string GetOptionsForVatCrCustomer()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetAllCreditCustomers();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option cadd='{2}',contno='{3}' value={0}>{1}</option>", dr["CCODE"].ToString(), dr["CNAME"].ToString(), dr["CADD1"].ToString(), dr["CONT_NO"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }

    public List<Customers> AdvancedSearch(string searchOn, string searchCriteria, string searchText)
    {
        List<Customers> customerList = new List<Customers>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new CustomerDAL().AdvancedSearch(searchOn, searchCriteria, searchText);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Customers objStates = new Customers()
                    {
                        Customer_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["Customer_ID"]),
                        Prefix = Convert.ToString(ds.Tables[0].Rows[i]["Prefix"]),
                        Customer_Name = Convert.ToString(ds.Tables[0].Rows[i]["Customer_Name"]),

                        Address_1 = Convert.ToString(ds.Tables[0].Rows[i]["Address_1"]),
                        Address_2 = Convert.ToString(ds.Tables[0].Rows[i]["Address_2"]),
                        Area_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["Area_ID"]),
                        City_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["City_ID"]),
                        State_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["State_ID"]),
                        //Date_Of_Birth = Convert.ToDateTime(ds.Tables[0].Rows[i]["Date_Of_Birth"]),
                        //Date_Anniversary = Convert.ToDateTime(ds.Tables[0].Rows[i]["Date_Anniversary"]),
                        Discount = Convert.ToDecimal(ds.Tables[0].Rows[i]["Discount"]),
                        Contact_No = Convert.ToString(ds.Tables[0].Rows[i]["Contact_No"]),
                        Tag = Convert.ToString(ds.Tables[0].Rows[i]["Tag"]),
                        FocBill = Convert.ToBoolean(ds.Tables[0].Rows[i]["FocBill"]),
                        grpid = Convert.ToInt32(ds.Tables[0].Rows[i]["grpid"]),
                        EmailId = Convert.ToString(ds.Tables[0].Rows[i]["EmailId"]),
                    };
                    customerList.Add(objStates);
                }

            }
        }

        finally
        {
            objParam = null;
        }
        return customerList;

    }


    public List<AccLedger> AdvancedSearchCreditCustomers(string searchOn, string searchCriteria, string searchText,string Type)
    {
        List<AccLedger> CreditcustomerList = new List<AccLedger>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new CustomerDAL().AdvancedSearchCreditCustomers(searchOn, searchCriteria, searchText,Type);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    AccLedger objAccLedger = new AccLedger()
                    {


                        AccountId = Convert.ToInt32(ds.Tables[0].Rows[i]["AccountId"]),
                        CODE = Convert.ToString(ds.Tables[0].Rows[i]["CODE"]),
                        CCODE = Convert.ToString(ds.Tables[0].Rows[i]["CustomeCode"]),
                        S_CODE = Convert.ToString(ds.Tables[0].Rows[i]["S_CODE"]),
                        SS_CODE = Convert.ToString(ds.Tables[0].Rows[i]["SS_CODE"]),
                        CNAME = Convert.ToString(ds.Tables[0].Rows[i]["CustomerName"]),
                        CADD1 = Convert.ToString(ds.Tables[0].Rows[i]["CADD1"]),
                        CADD2 = Convert.ToString(ds.Tables[0].Rows[i]["CADD2"]),
                        CITY_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["CITY_ID"]),
                        STATE_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["STATE_ID"]),
                        AREA_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["AREA_ID"]),
                        CST_NO = ds.Tables[0].Rows[i]["CST_NO"].ToString(),

                        //CST_DATE = Convert.ToDateTime(ds.Tables[0].Rows[i]["CST_DATE"]),
                        TINNO = Convert.ToString(ds.Tables[0].Rows[i]["TINNO"]),
                        TOTNO = Convert.ToString(ds.Tables[0].Rows[i]["TOTNO"]),
                        CR_LIMIT = Convert.ToDecimal(ds.Tables[0].Rows[i]["CR_LIMIT"]),
                        CR_DAYS = Convert.ToDecimal(ds.Tables[0].Rows[i]["CR_DAYS"]),
                        CONT_PER = Convert.ToString(ds.Tables[0].Rows[i]["CONT_PER"]),
                        CONT_NO = Convert.ToString(ds.Tables[0].Rows[i]["CONT_NO"]),
                        OP_BAL = Convert.ToDecimal(ds.Tables[0].Rows[i]["OP_BAL"]),
                        DR_CR = Convert.ToString(ds.Tables[0].Rows[i]["DR_CR"]),
                        DIS_PER = Convert.ToDecimal(ds.Tables[0].Rows[i]["DIS_PER"]),
                        OS_BAL = Convert.ToDecimal(ds.Tables[0].Rows[i]["OS_BAL"]),
                        PURSALE_ACC_PERCENT = Convert.ToDecimal(ds.Tables[0].Rows[i]["PURSALE_ACC_PERCENT"]),
                        PURSALE_ACC_TYPE = Convert.ToString(ds.Tables[0].Rows[i]["PURSALE_ACC_TYPE"]),
                        PREFIX = Convert.ToString(ds.Tables[0].Rows[i]["PREFIX"]),
                        ACC_ZONE = Convert.ToString(ds.Tables[0].Rows[i]["ACC_ZONE"]),
                        SRNO = Convert.ToInt32(ds.Tables[0].Rows[i]["SRNO"]),
                        ShowInLedger = Convert.ToBoolean(ds.Tables[0].Rows[i]["ShowInLedger"]),
                        Narr = Convert.ToString(ds.Tables[0].Rows[i]["Narr"]),
                        tAG = Convert.ToString(ds.Tables[0].Rows[i]["tAG"]),
                       
                    };
                    CreditcustomerList.Add(objAccLedger);
                }

            }
        }

        finally
        {
            objParam = null;
        }
        return CreditcustomerList;

    }

    public void GetByMobileNo(Customers objCustomers)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new CustomerDAL().GetByMobileNo(objCustomers.Contact_No);



            if (dr.HasRows)
            {
                dr.Read();



                objCustomers.Customer_ID = Convert.ToInt32(dr["Customer_ID"].ToString());
                objCustomers.Prefix = dr["Prefix"].ToString();
                objCustomers.Customer_Name = dr["Customer_Name"].ToString();
                objCustomers.Area_ID = Convert.ToInt32(dr["Area_ID"].ToString());
                objCustomers.City_ID = Convert.ToInt32(dr["City_ID"].ToString());
                objCustomers.State_ID = Convert.ToInt32(dr["State_ID"].ToString());
                objCustomers.Date_Of_Birth = Convert.ToDateTime(dr["Date_Of_Birth"].ToString());
                objCustomers.Date_Anniversary = Convert.ToDateTime(dr["Date_Anniversary"].ToString());
                objCustomers.Discount = Convert.ToDecimal(dr["Discount"].ToString());
                objCustomers.Contact_No = dr["Contact_No"].ToString();
                objCustomers.EmailId = dr["EmailId"].ToString();

            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }



    public Int32 DeleteCustomer(Customers objCustomer)
    {
        return new CustomerDAL().Delete(objCustomer);
    }


    public void GetById(Customers objCustomer)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetById(objCustomer);
            if (dr.HasRows)
            {
                dr.Read();

                objCustomer.Customer_Name = dr["Customer_Name"].ToString();
                objCustomer.Customer_ID = Convert.ToInt16(dr["Customer_ID"]);
                objCustomer.Prefix = dr["Prefix"].ToString();
                objCustomer.Address_1 = Convert.ToString(dr["Address_1"]);
                objCustomer.Address_2 = Convert.ToString(dr["Address_2"]);
                objCustomer.Area_ID = Convert.ToInt32(dr["Area_ID"]);
                objCustomer.City_ID = Convert.ToInt32(dr["City_ID"]);
                objCustomer.State_ID = Convert.ToInt16(dr["State_ID"]);
                objCustomer.Date_Of_Birth = Convert.ToDateTime(dr["Date_Of_Birth"]);
                objCustomer.Date_Anniversary = Convert.ToDateTime(dr["Date_Anniversary"]);
                objCustomer.Discount = Convert.ToDecimal(dr["Discount"]);
                objCustomer.Contact_No = Convert.ToString(dr["Contact_No"]);
                objCustomer.Tag = Convert.ToString(dr["Tag"]);
                objCustomer.FocBill = Convert.ToBoolean(dr["FocBill"]);
                objCustomer.grpid = Convert.ToInt32(dr["grpid"]);
                objCustomer.UserId = Convert.ToInt16(dr["UserId"]);
                objCustomer.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objCustomer.EmailId = dr["EmailId"].ToString();
                objCustomer.GSTNo = dr["GSTNo"].ToString();

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public List<Customers> GetCustomersByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Customers> CashCustomerList = new List<Customers>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new CustomerDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Customers objCustomer = new Customers()
                    {
                        Customer_Name = dr["Customer_Name"].ToString(),
                        Customer_ID = Convert.ToInt16(dr["Customer_ID"]),
                        Prefix = dr["Prefix"].ToString(),
                        Address_1 = Convert.ToString(dr["Address_1"]),
                        Address_2 = Convert.ToString(dr["Address_2"]),
                        Area_ID = Convert.ToInt32(dr["Area_ID"]),
                        City_ID = Convert.ToInt32(dr["City_ID"]),
                        State_ID = Convert.ToInt16(dr["State_ID"]),
                        Date_Of_Birth = Convert.ToDateTime(dr["Date_Of_Birth"]),
                        Date_Anniversary = Convert.ToDateTime(dr["Date_Anniversary"]),
                        Contact_No = Convert.ToString(dr["Contact_No"]),
                        Discount = Convert.ToDecimal(dr["Discount"]),
                        Tag = Convert.ToString(dr["Tag"]),
                        FocBill = Convert.ToBoolean(dr["FocBill"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        grpid = Convert.ToInt32(dr["grpid"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        EmailId = dr["EmailId"].ToString(),
                    };
                    CashCustomerList.Add(objCustomer);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return CashCustomerList;

    }




    public List<Customers> GetAll()
    {
        List<Customers> CustomerList = new List<Customers>();

        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Customers objCustomer = new Customers()
                    {
                        Customer_Name = dr["Customer_Name"].ToString(),
                        Customer_ID = Convert.ToInt16(dr["Customer_ID"]),
                        Prefix = dr["Prefix"].ToString(),
                        Address_1 = Convert.ToString(dr["Address_1"]),
                        Address_2 = Convert.ToString(dr["Address_2"]),
                        Area_ID = Convert.ToInt32(dr["Area_ID"]),
                        City_ID = Convert.ToInt32(dr["City_ID"]),
                        State_ID = Convert.ToInt16(dr["State_ID"]),
                        Date_Of_Birth = Convert.ToDateTime(dr["Date_Of_Birth"]),
                        Date_Anniversary = Convert.ToDateTime(dr["Date_Anniversary"]),
                        Contact_No = Convert.ToString(dr["Contact_No"]),
                        Discount = Convert.ToDecimal(dr["Discount"]),
                        Tag = Convert.ToString(dr["Tag"]),
                        FocBill = Convert.ToBoolean(dr["FocBill"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        grpid = Convert.ToInt32(dr["grpid"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        EmailId = Convert.ToString(dr["EmailId"]),

                    };
                    CustomerList.Add(objCustomer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return CustomerList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Customer_ID"].ToString(), dr["Customer_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Customers objCustomer)
    {
        
        return new CustomerDAL().InsertUpdate(objCustomer);
    }

    public Int16 Insert(Customers objCustomer)
    {

        return new CustomerDAL().Insert(objCustomer);
    }



    public List<Customers> AdvancedSearchFOC(string searchOn, string searchCriteria, string searchText)
    {
        List<Customers> customerList = new List<Customers>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new CustomerDAL().AdvancedSearchFOC(searchOn, searchCriteria, searchText);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Customers objStates = new Customers()
                    {
                        Customer_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["Customer_ID"]),
                        Prefix = Convert.ToString(ds.Tables[0].Rows[i]["Prefix"]),
                        Customer_Name = Convert.ToString(ds.Tables[0].Rows[i]["Customer_Name"]),

                        Address_1 = Convert.ToString(ds.Tables[0].Rows[i]["Address_1"]),
                        Address_2 = Convert.ToString(ds.Tables[0].Rows[i]["Address_2"]),
                        Area_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["Area_ID"]),
                        City_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["City_ID"]),
                        State_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["State_ID"]),
                        //Date_Of_Birth = Convert.ToDateTime(ds.Tables[0].Rows[i]["Date_Of_Birth"]),
                        //Date_Anniversary = Convert.ToDateTime(ds.Tables[0].Rows[i]["Date_Anniversary"]),
                        Discount = Convert.ToDecimal(ds.Tables[0].Rows[i]["Discount"]),
                        Contact_No = Convert.ToString(ds.Tables[0].Rows[i]["Contact_No"]),
                        Tag = Convert.ToString(ds.Tables[0].Rows[i]["Tag"]),
                        FocBill = Convert.ToBoolean(ds.Tables[0].Rows[i]["FocBill"]),
                        grpid = Convert.ToInt32(ds.Tables[0].Rows[i]["grpid"]),
                    };
                    customerList.Add(objStates);
                }

            }
        }

        finally
        {
            objParam = null;
        }
        return customerList;

    }


    public void GetByMobileNoFOC(Customers objCustomers)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new CustomerDAL().GetByMobileNoFOC(objCustomers.Contact_No);



            if (dr.HasRows)
            {
                dr.Read();



                objCustomers.Customer_ID = Convert.ToInt32(dr["Customer_ID"].ToString());
                objCustomers.Prefix = dr["Prefix"].ToString();
                objCustomers.Customer_Name = dr["Customer_Name"].ToString();
                objCustomers.Area_ID = Convert.ToInt32(dr["Area_ID"].ToString());
                objCustomers.City_ID = Convert.ToInt32(dr["City_ID"].ToString());
                objCustomers.State_ID = Convert.ToInt32(dr["State_ID"].ToString());
                objCustomers.Date_Of_Birth = Convert.ToDateTime(dr["Date_Of_Birth"].ToString());
                objCustomers.Date_Anniversary = Convert.ToDateTime(dr["Date_Anniversary"].ToString());
                objCustomers.Discount = Convert.ToDecimal(dr["Discount"].ToString());
                objCustomers.Contact_No = dr["Contact_No"].ToString();
                objCustomers.EmailId = dr["EmailId"].ToString();
            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

    public List<AccLedger> GetAllCreditCustomer()
    {
        List<AccLedger> CreditcustomerList = new List<AccLedger>();

        SqlDataReader dr = null;
        try
        {
            dr = new CustomerDAL().GetAllCreditCustomers();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccLedger objAccLedger = new AccLedger()
                    {


                        AccountId = Convert.ToInt32(dr["AccountId"]),
                        CODE = Convert.ToString(dr["CODE"]),
                        CCODE = Convert.ToString(dr["CCODE"]),
                        S_CODE = Convert.ToString(dr["S_CODE"]),
                        SS_CODE = Convert.ToString(dr["SS_CODE"]),
                        CNAME = Convert.ToString(dr["CNAME"]),
                        CADD1 = Convert.ToString(dr["CADD1"]),
                        CADD2 = Convert.ToString(dr["CADD2"]),
                        CITY_ID = Convert.ToInt32(dr["CITY_ID"]),
                        STATE_ID = Convert.ToInt32(dr["STATE_ID"]),
                        AREA_ID = Convert.ToInt32(dr["AREA_ID"]),
                        CST_NO = dr["CST_NO"].ToString(),

                        //CST_DATE = Convert.ToDateTime(ds.Tables[0].Rows[i]["CST_DATE"]),
                        TINNO = Convert.ToString(dr["TINNO"]),
                        TOTNO = Convert.ToString(dr["TOTNO"]),
                        CR_LIMIT = Convert.ToDecimal(dr["CR_LIMIT"]),
                        CR_DAYS = Convert.ToDecimal(dr["CR_DAYS"]),
                        CONT_PER = Convert.ToString(dr["CONT_PER"]),
                        CONT_NO = Convert.ToString(dr["CONT_NO"]),
                        OP_BAL = Convert.ToDecimal(dr["OP_BAL"]),
                        DR_CR = Convert.ToString(dr["DR_CR"]),
                        DIS_PER = Convert.ToDecimal(dr["DIS_PER"]),
                        OS_BAL = Convert.ToDecimal(dr["OS_BAL"]),
                        PURSALE_ACC_PERCENT = Convert.ToDecimal(dr["PURSALE_ACC_PERCENT"]),
                        PURSALE_ACC_TYPE = Convert.ToString(dr["PURSALE_ACC_TYPE"]),
                        PREFIX = Convert.ToString(dr["PREFIX"]),
                        ACC_ZONE = Convert.ToString(dr["ACC_ZONE"]),
                        SRNO = Convert.ToInt32(dr["SRNO"]),
                        ShowInLedger = Convert.ToBoolean(dr["ShowInLedger"]),
                        Narr = Convert.ToString(dr["Narr"]),
                        tAG = Convert.ToString(dr["tAG"]),

                    };
                    CreditcustomerList.Add(objAccLedger);

                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return CreditcustomerList;

    }

}