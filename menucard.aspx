﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="menucard.aspx.cs" Inherits="menucard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css3/bootstrap.css" rel="stylesheet" />
<link href="css3/bootstrap-theme.css" rel="stylesheet" />
<link href="css/Stylesheet.css" rel="stylesheet" />
   <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
 

   <script src="Scripts/jquery-2.0.3.min.js"></script>
    <script src="Scripts/knockout-3.0.0.js"></script>
 
    <link href="Styles/Style.css" rel="stylesheet" /> 
    <script src="Scripts/knockout.validation.js" type="text/javascript"></script>
    <script src="ViewModel/MenuViewModel.js" type="text/javascript"></script>
     

    <link rel="stylesheet" href="css/jquery.notifyBar.css">
      <script src="jquery.notifyBar.js"></script>
       
    <script language="javascript" type="text/javascript">


     

        $(document).ready(
        function () {



//            $("#btnPlaceOrder").click(
//            function () {

               // $("#dvVerification").css("display", "block");
                //$("#tbCalculations").css("display", "none");

                

//            }
//            );

            $("#btnCancelOrder").click(
            function () {

                $("#dvVerification").css("display", "none");
                $("#tbCalculations").css("display", "block");



            }
            );
            

           

            $("#imghome").click(
            function () {
                $("#dvOrders").hide();
                $("#dvProducts").show();

            }
            );



        }

        );
       
        
    </script>


<meta charset="utf-8">
 
<title>Index</title>

<style type="text/css">
    
     
  
    
    .InActiveLink
    {
          background-color:#c0392b;
    }
    
   .ActiveLink
    {
          background-color:#e74c3c;
    }
  
   
    ::-webkit-scrollbar {
    width: 10px;
    height: 15px;
    border-bottom: 1px solid #eee; 
    border-top: 1px solid #eee;
}
::-webkit-scrollbar-thumb {
    border-radius: 8px;
    background-color: maroon;
    border: 2px solid #eee;
}

::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
} 
    
 
   .btn-success {
    background-color: blue;
} 
 
 
.nav-tabs1 {
    border-bottom: 0 none;

    margin:auto;
     
}
.nav-tabs1 li {
    float:left;
    margin: 0;
    display:inline;
}
.nav-tabs1 li a {
  
    opacity:0.8;
    border: 0 none;
    margin-right: 0;
    border-top-right-radius:0px;
    border-top-left-radius:0px;
    
}
.nav-tabs1 li a:hover {
    background-color: #e74c3c;
}
.nav-tabs1 .glyphicon {
    color: #fff;
}
.nav-tabs1 .active .glyphicon {
    color: #333;
}
.nav-tabs1 > li.active > a, .nav-tabs1 > li.active > a:hover, .nav-tabs1 > li.active > a:focus {
    border: 0 none;
  
}
.tab-content {
    margin-left: 45px;
}
.tab-content .tab-pane {
    
    display: none;
    overflow-y: auto;
    padding: 1.6rem;
}
.tab-content .active {
    display: block;
}   
 
    
    
    
    

.tabs-left {
    margin-top: 3rem;
}
.nav-tabs {
    border-bottom: 0 none;
    float: left;
    width:100%;
}
.nav-tabs li {
    float: none;
    margin: 0;
}
.nav-tabs li a {
    
    border: 0 none;
    margin-right: 0;
    border-top-right-radius:0px;
    border-top-left-radius:0px;
    font-size:16px;
    font-weight:bold;
    border-bottom:dotted 1px white;
padding-top:15px;
padding-bottom:15px;
}
.nav-tabs li a:hover {
    background-color: #e74c3c;
}
.nav-tabs .glyphicon {
    color: #fff;
}
.nav-tabs .active .glyphicon {
    color: #333;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    border: 0 none;
  
}
.tab-content {
    margin-left: 45px;
}
.tab-content .tab-pane {
    
    display: none;
    overflow-y: auto;
    padding: 1.6rem;
}
.tab-content .active {
    display: block;
}
</style>
</head>

<body>




<div class="container">
<div class="background">

<div class="row">
<div class="col-lg-12">
<div class="butt">
 
<img src="images/star.png" alt="" style="cursor:pointer"  data-bind="click: $root.GetFavourites" />
<img src="images/help.png" alt="" />
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="menubg">

<div class="bgh">
<div class="row">
<div class="col-xs-8">
<span class="dest">
<span data-bind="text:GroupName"></span>
 >>
</span>

<span class="dest2">
<span data-bind="text:SubGroupName"></span>
</span>
</div>
<div class="col-xs-4">
<div class="item" id="cart" style="cursor:pointer;text-align:right">
<span data-bind="text:OrderProducts().length"></span> Item(s), <span data-bind="text:GrossAmount"></span>  <img style="width:10px" src="images/rssymbol.png">
</div>
</div>

</div>


<div class="row">
 
<div class="col-xs-12">
<div class="dotte">
</div>
</div>
 
</div>

<div class="row">
 <div class="col-xs-3" style="padding-right:0px">

    <table width="100%" style="background:oldlace" >
    <tr><td><input type="text" style="width:100%" data-bind="value:Keyword"/></td><td> <input type="button" data-bind="click: $root.SearchProduct" value="Search" style="padding:2px 5px 2px 5px" class="btn btn-success"/></td></tr>
    </table>

    <ul class="nav nav-tabs"  data-bind="foreach: SubGroups" >
          <li data-bind="click: $root.GetProductList">
         <%-- <span data-bind="text:InActiveLink"></span>--%>
          <a data-toggle="tab" href="#"  data-bind="css: { 'ActiveLink': ActiveLink,'InActiveLink': InActiveLink}"><span class="glyphicon glyphicon-heart"></span> <span data-bind="text: Title" style="cursor:pointer;color:White"></span></a></li>
            </ul>


            <table width="100%" style="background:oldlace">
<%--<tr><td  style="font-size:18px;font-weight:bold;color:Black;text-align:left">Items:</td><td style="font-size:18px;font-weight:bold;color:Black;text-align:left"><span data-bind="text:OrderProducts().length"></span></td></tr>
<tr><td  style="font-size:18px;font-weight:bold;color:Black;text-align:left">Price:</td><td  style="font-size:18px;font-weight:bold;color:Black;text-align:left"><span  data-bind="text:GrossAmount"></span> <img style="width:10px" src="images/rssymbol.png"></td></tr>
<tr>
<td colspan="100%">

<input type="button" id="cart"  value="View Details" style="padding:2px 5px 2px 5px;width:100%" class="btn btn-success"/>
</td>
</tr>--%>
<tr>
<td colspan="100%" align="center">
   <img src="images/special.png" style="width:100%" />
</td>
</tr>
<tr>
<td colspan="100%">
<table>
<tr><td align="center" style="text-align:center"><span  data-bind="text:GrossAmount" style="font-size:25px;font-weight:bold"></span>   <img style="width:10px" src="images/rssymbol.png"></td></tr>
<tr><td>
<div id="cart" class="btn btn-success" style="font-size:25px">Order Now!</div>

</td></tr>
</table>


</td>
</tr>
</table>


  </div>

  <div id="dvOrders" class="col-xs-9" style="padding-left:0px;display:none">
  
  
  <table style="float:right;margin-right:25px">
  <tr><td>
  <div style="max-height:530px;overflow-y:scroll">
  <table  >
     <thead>
      <tr style="background:seashell;color:black;text-shadow:none">
     <th></th>
    
     <th style="width:200px">Item</th>
     <th>Price</th>
     <th>S.T</th>
      <th>Remarks</th>
     <th style="text-align:center">Qty</th>
     <th> </th>
    
     </tr>
     </thead>
    
    <tbody data-bind="foreach: OrderProducts">
   
    <tr>
 
    <td>
<img width="50px" height="50px" class="img-rounded" data-bind="attr:{src: ItemImage}"/>
    </td>
     
    <td>
     <span data-bind="text: Item_Name" style="cursor:pointer"></span>
    </td>
     <td>
     <span data-bind="text: Sale_Rate" style="cursor:pointer"></span>
    </td>


     <td>
     <span data-bind="text: SubTotal" style="cursor:pointer"></span>
    </td>
     <td>
    <textarea data-bind ="value:Remarks" style="width:80px;height:50px"></textarea>
    </td>


    

  <td>
    
    <table>
    <tr>
    <td><input type="button" data-bind="click: $root.DecrementQuantity"  value="-"  class="btn btn-success"/></td>
    <td>
    <span data-bind="text: Qty" ></span>
 
 
 </td>
    <td><input type="button"  data-bind="click: $root.IncrementQuantity" value="+" class="btn btn-success"  /></td>

    </tr>
    </table>

    </td>
   <td>
   <input type="button" data-bind="click: $root.RemoveProduct"  value="Delete" class="btn btn-danger" />
   </td>
   
    </tr>
  </tbody>
    </table>
    </div>
    
    </td></tr>
  <tr><td style="float:right">
   
<table id="dvVerification" style="display:none">
<tr><td>Employee Name</td><td><input type="text" id="txtUserName" /></td></tr>
<tr><td>Password</td><td><input type="password" id="txtPassword" /></td></tr>
<tr><td></td><td> 
<table>
<tr>
<td><div class="btn btn-danger" style="width:100%"   >Order Now</div></td>
<td><div class="btn btn-danger" style="width:100%"  id="btnCancelOrder">Cancel</div></td>
</tr>
</table>


   </td></tr>
</table>


 


  <table id="tbCalculations">
    <tr>
    <td>Gross Amount:</td><td><span  data-bind="text:GrossAmount"></span></td>
    </tr>
    <tr>
    <td>Service Tax:</td><td>
    <table>
    <tr>
    <td><span data-bind="text:SerTax"></span>%</td>
    <td><span data-bind="text:SerAmount"></span></td>
    </tr>
    </table>
    
    
    
    </td>
    </tr>
    <tr>
    <td>Vat:</td><td><span data-bind="text:VatAmount"></span></td>
    </tr>
    <tr>
    <td>Net Amount:</td><td><span  data-bind="text:NetAmount"></span></td>

    </tr>
    <tr>
    <td colspan="100%">
   
    <div class="btn btn-danger" style="width:100%;font-size:25px" id="btnPlaceOrder" data-bind="click: $root.PlaceOrder">Place Order</div>
   
    
    </td>

    </tr>
   
    </table>
  </td></tr>
  
  </table>
 
     
    
 
  
  </div>




    
  <div id="dvProducts" class="col-xs-9 enable_touchmove"  style="padding-left:0px" >
  <%--
      ;background:#EFECB4--%>
  <div style="overflow-y:scroll;max-height:730px;float:left;" data-bind="foreach: Products">
    
<div class="col-xs-12">
<table style="width:100%;margin:0px;box-shadow:none">
<tr>
<td style="width:10%"><div class=""style="margin:10px 0 0 0;"><img class="img-circle" width="100px" data-bind="attr:{src: ItemImage}"/></div></td>
<td style="text-align:left;width:70%">

<div class=""style="margin:10px 0 0 0;">
<div class="list">
<p  data-bind="text: Item_Name" > </p>
<h6>Two slices of bakiava served with vanilla ice cream</h6>
</div>
<div class="abc" style="margin:10px 0 0 0;">
<p>
<span data-bind="click: $root.LikeItem" style="cursor:pointer">
<img src="images/star1.png" alt="" style="float:left; margin:2px 5px 2px 5px"    />
Like
</span>  (<span data-bind="text:Likes" style="font-style:italic;font-size:12px"  ></span><span  style="font-style:italic;font-size:12px" > People like It</span>)

  

 
</p>


</p>

</div>
<%--<div class="middledoted"></div>--%>
</div>
</td>
<td style="width:20%">
<div class="" style="margin:10px 0 0 0;">
<div class="doller">
<table>
    <tbody><tr><td><h5 style="font-weight:bold;font-size:18px" data-bind="text: Sale_Rate"> </h5></td><td>  
    <img style="width:10px" src="images/rssymbol.png"></td></tr>
  </tbody></table>
<%--
<h5 data-bind="text: Sale_Rate" style="font-weight:bold;font-size:18px" > </h5>--%>
</div>
<div class="adv" style="margin:10px 0 0 0;cursor:pointer"  data-bind="click: $root.AddProductToList" ><img src="images/img3.png" alt="" /></div>

</div>

</td>
</tr>
</table>

<hr />




</div>

</div>
 
</div>

</div>





</div>



<br>

</div>

</div>

 


</div>


<div class="row">
<div class="col-xs-12" style="margin:10px 0 0 0" >
<div class="footer">
<table width="100%">
<tr>
<td><img src="images/home.png" style="cursor:pointer"  id="imghome" alt=""/></td>
<td align="center" class="nav nav-tabs1">
<table>
<tr  data-bind="foreach: Groups">

<td  data-bind="click: $root.GetByGroup" style="padding-right:15px;padding-left:15px"><a data-toggle="tab" href="#home"><span  class="glyphicon glyphicon-star"></span><span data-bind="text: Title"  style="cursor:pointer;padding-left:5px;color:White;text-shadow:none;font-weight:bold;font-size:16px"></span></a> 
         
</td>
</tr>

</table>



 
</td>
<td>
<img src="images/order.png" alt="" id="imgOrder" style="cursor:pointer"/>
</td>
</tr>

</table>
 
 
 
 
</div>

</div>
</div>

</div>


</body>
</html>


