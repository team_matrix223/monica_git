﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="PhysicalStock.aspx.cs" Inherits="PhysicalStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


    


    <script src="Scripts/knockout-3.0.0.js"></script>
    
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script src="Scripts/knockout.validation.js" type="text/javascript"></script>
         <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

              <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />
    <script src="js/jquery.uilock.js" type="text/javascript"></script>

       <script language="javascript" type="text/javascript">

           function ChangeText(val) {

              
               $("#spName").text(val);

           }
       </script>
        <div class="right_col" role="main" id="frmPhysicalStock">

       

                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>PHYSICAL STOCK</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                             


                  <div id="addDialog" style="background-color:White" >
        


 

  
  
     <div class ="col-md-12 col-sm-12 col-xs-12">
     <div class ="x_panel">
 

                                         <form id="antoform" class="form-horizontal calender" role="form" runat="server">
                                            <asp:HiddenField ID="hdnOpenedDay" runat="server" />
    <asp:HiddenField ID="hdnClosedDay" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnLastOpenedDate" runat="server" />


                                          <div class="form-group">
                                           <div class="x_title">PHYSICAL ENTRY LISTING</div>


                                              <label class="col-sm-1 control-label">Date:</label>
                                             <div class="col-md-4 xdisplay_inputx form-group has-feedback">
                                                            <input type="text"  readonly="readonly" style="width:180px;height:40px" class="form-control has-feedback-left" id="txtBreakageDate" placeholder="MM/DD/YYYY" aria-describedby="inputSuccess2Status">
                                                            <span class="fa fa-calendar  form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span>
                                                        </div>
                                                
                                       
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="showColumn" class="btn-group" data-toggle="buttons">
                                                
                                                 <input type="radio" id= "rdbFinished" name="Finished" value="Finished"  /> &nbsp;<label for="rdbFinished" style="font-weight:normal">Finished</label>   &nbsp;
                                             <input type="radio"  id="rdbSemiFinished" name="Finished" value="Semifinished" checked="" required />  &nbsp; <label for="rdbSemiFinished" style="font-weight:normal">Semi Finished</label>  &nbsp;
                                             <input type="radio"  id ="rdbRaw" name="Finished" value="Raw" checked=""  />  <label for="rdbRaw" style="font-weight:normal">Raw</label>  
                                            
                                            
                                                  
                                                    
                                                </div>
                                            </div>
                                        </div>
                                   



                                   
                                         


                                        

                                        <div class="row">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table width="100%" class="table" style="margin:0px">
                                    <tr>
                                    <th>Type</th><th>Godown</th><th><span id="spName">Group</span></th><th></th>
                                    </tr>
                                    <tr>
                                    <td>        <input type="radio"  id="rdbDept" onchange="javascript:ChangeText('Department')"  name="Department"  value="Department"  /> &nbsp;<label for="rdbDept" style="font-weight:normal">Department</label> &nbsp;
                                                        <input type="radio" onchange="javascript:ChangeText('Group')" id="rdbGroup" name="Department" value="Group" checked="" required /> <label for="rdbGroup" style="font-weight:normal">Group</label> 
                                                 </td>
                                    
                                    <td>
                                    <select id="ddlGodown"  style="width:120px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>

                                    <td>
                                    
                                       <select id="ddlGroup"  style="width:130px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>
                                    <td>
                                    <div id="btnShowList"  data-toggle="modal" class="btn btn-success"  >Show List</div>
                                    </td>

                                    </tr>



                                      
                                        </table>
                                        </div>


                                        </div>


                                        <div class="row">
                                        <div class="col-md-12">
                                        <table width="100%" style="margin:10px 0 0  0;background:#FFFFD7;" class="table">
                                        <tr>
                                        <td>
                                 
                                               
                                               <input type="radio"  id="rdbCode" name="Code" value="Code"  /> &nbsp;<label style="font-weight:normal" for="rdbCode">Sort By Code </label> &nbsp;
                                                        <input type="radio"  id ="rdbName" name="Code" value="Name" checked="" required /> <label style="font-weight:normal" for="rdbName">Sort By Name </label>
                                         
                                        
                                        
                                        </td>
                                        <td style="text-align:right;padding-right:20px">
                                                 <input type="radio"  id="rdbUnit" name="Column" value="Unit"  /> &nbsp; <label style="font-weight:normal" for="rdbUnit">Show Unit Column </label>&nbsp;
                                                 
                                                    
                                                        <input type="radio" id="rdbRate" name="Column" value="Rate" checked="" required /> <label style="font-weight:normal" for="rdbRate">Sort Rate Column</label>
                                              
                                           
                                        </td>
                                        
                                        </tr>
                                        
                                        </table>
                                        
                                        </div>
                                        
                                        </div>
                                         

                                          
                                         <div style="max-height:300px;overflow-y:scroll">
     
      <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProducts">
										<thead>
											<tr>
												<th width="50px">
													 Code
												</th>
												<th>
													 Item Name
												</th>
												  <%--<th width="70px">
													Stock Qty
												</th>--%>
                                                <th width="70px">
                                                <label id="lblunit">Rate</label>
													 
												</th>
                                          
                                                <th width="90px">
													 PhyStock
												</th>
                                                    
                                               

											</tr>
										</thead>
										<tfoot>
											<tr>
												<td colspan ="100%">
                                                
                                                
                                                </td>
                                                
											</tr>
										</tfoot>
										<tbody>
										 
									 	 
										</tbody>
										</table>
          </div>
                                      
                                                
                                              
                                                       
                                                  
                                               
                                           


   
 
          
      
 </form>

  </div>
                                     

</div>



</div>
 
     

               
                  <div id="btnNew"  data-toggle="modal" class="btn  btn-success" style="margin-left:10px" >Add</div>
                  <div id="btnEdit" style="display:none"  data-toggle="modal" class="btn  btn-info"  >Edit Record</div>
                   <select id="hdnProducts" style="display:none" >
                  
                            </div>
                        </div>
                    </div>


                    <script type="text/javascript">
                        $(document).ready(function () {

                            $("#txtBreakageDate").val($("#<%=hdnLastOpenedDate.ClientID%>").val());

                            $('#rdbDept').change(function () {
                                BindContols();

                            });

                            $('#rdbGroup').change(function () {
                                BindContols();

                            });


                            $('#rdbUnit').change(function () {
                                $("#lblunit").html("Unit");

                                BindProductDetail();
                            });


                            $('#rdbRate').change(function () {
                                $("#lblunit").html("Rate");

                                BindProductDetail();
                            });

                            $('#rdbName').change(function () {

                                BindProductDetail();
                            });

                            $('#rdbCode').change(function () {

                                BindProductDetail();
                            });

                            BindContols();
                            $("#btnNew").click(
                            function () {
                                SaveRecords();

                            }
                            );


                            $("#btnShowList").click(
                            function () {
                                BindProductDetail();


                            }
                            );



                            $('#txtBreakageDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });



                            function BindContols() {

                                $.uiLock('');
                                $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                                var Type = "";
                                if ($("#rdbGroup").is(':checked') == true) {
                                    Type = "G";
                                }
                                else {
                                    Type = "D";
                                }



                                $("#ddlGodown").html("<option ></option>");

                                $("#ddlGroup").html("<option ></option>");


                                $.ajax({
                                    type: "POST",
                                    data: '{"Type":"' + Type + '"}',
                                    url: "PhysicalStock.aspx/BindControls",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {

                                        var obj = jQuery.parseJSON(msg.d);



                                        $("#ddlGodown").append(obj.GodownFromOptions);
                                        $("#ddlGroup").append(obj.GroupStock);




                                    },
                                    complete: function () {


                                        $.uiUnlock();


                                    }


                                });

                            }




                            function SaveRecords() {



                                var GodownId = $("#ddlGodown").val();
                                var GodownName = $("#ddlGroup option:selected").text();

                                stockArr = [];
                                ItemArr = [];
                                var Date = $("#txtBreakageDate").val();
                                if (Date == "") {
                                    alert("Choose Date");
                                    return;
                                }


                                var errCounter = 0;
                                var error = false;

                                $("input[name='txtRate']").each(
            function (x) {


                var counterId = $(this).attr("counter");


                ItemArr[x] = $("#txtServiceId" + counterId).html();

                stockArr[x] = $("#txtPhysicalStock" + counterId).val();

                if (!jQuery.isNumeric(stockArr[x])) {


                    $("#txtPhysicalStock" + counterId).focus();
                    error = true;

                    return false;
                }


            }

            );

                                if (error == true) {
                                    alert("Invalid Stock Entry");
                                    return;
                                }



                                if (ItemArr.length == 0) {

                                    alert("List is Empty. Please select atleast one Item");
                                    return;
                                }

                                $.uiLock('');



                                $.ajax({
                                    type: "POST",
                                    data: '{"GodownId":"' + GodownId + '","Status": "' + GodownName + '","ItemCodeArr": "' + ItemArr + '","phystockArr": "' + stockArr + '"}',

                                    url: "PhysicalStock.aspx/Insert",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {

                                        var obj = jQuery.parseJSON(msg.d);


                                        alert("Physical Stock Added Successfully");








                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function () {

                                        $.uiUnlock();
                                        Reset();

                                    }


                                });
                            }


                            function Reset() {
                                $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                $("#ddlGodown").val("");
                                $("#ddlGroup").val("");
                            }


                            function BindProductDetail() {



                                var Godown = $("#ddlGodown").val();
                                if (Godown == "") {
                                    alert("Choose Godown");

                                    $("#ddlGodown").focus();
                                    return;
                                }
                                $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                var Type = "";
                                if ($("#rdbFinished").is(":checked") == true) {
                                    Type = "F";
                                }
                                else if ($("#rdbSemiFinished").is(":checked") == true) {
                                    Type = "S";
                                }
                                else if ($("#rdbRaw").is(":checked") == true) {
                                    Type = "R";
                                }

                                var Sorting = "";
                                if ($("#rdbGroup").is(':checked') == true) {
                                    Sorting = "G";
                                }
                                else {
                                    Sorting = "D";
                                }


                                var Id = $("#ddlGroup").val();
                                if (Id == "") {


                                    $("#ddlGroup").focus();
                                    return;
                                }

                                var Name = $("#ddlGroup option:selected").text();
                                var OrderType = "";
                                if ($("#rdbName").is(":checked") == true) {
                                    OrderType = 'Name';
                                }
                                else {
                                    OrderType = 'Code';
                                }



                                $.uiLock('');

                                $.ajax({
                                    type: "POST",
                                    data: '{ "Type": "' + Type + '","Sorting": "' + Sorting + '","Id": "' + Id + '","Name": "' + Name + '","OrderType":"' + OrderType + '"}',
                                    url: "PhysicalStock.aspx/GetAllPrroductsByType",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {

                                        var obj = jQuery.parseJSON(msg.d);
                                        var tr = "";

                                        var counterId = 0;

                                        for (var i = 0; i < obj.ServiceData.length; i++) {



                                            var Unit = "";

                                            if ($("#rdbUnit").is(':checked') == true) {


                                                Unit = obj.ServiceData[i].Unit;

                                            }
                                            else {


                                                Unit = obj.ServiceData[i].Rate;

                                            }



                                            counterId = counterId + 1;

                                            tr = tr + "<tr><td><label    id='txtServiceId" + counterId + "'  counter='" + counterId + "'  name='txtServiceId'  >" + obj.ServiceData[i].Code + "</label></td>" +
                        "<td><label  id='txtItemNameId" + counterId + "'  counter='" + counterId + "'  name='txtItemName'  >" + obj.ServiceData[i].IName + "</label></td>" +
                        "<td><input type='text' id='txtRate" + counterId + "'  counter='" + counterId + "' readonly=readonly class='form-control input-small' name='txtRate' style='width:60px'   value='" + Unit + "' /></td>" +
                        "<td><div style='float:left'><input type='text' id='txtPhysicalStock" + counterId + "'    counter='" + counterId + "'  class='form-control input-small' name='txtPhysicalStock' style='width:55px'   value='" + obj.ServiceData[i].Crstock + "' /></div></td></tr>"



                                        }
                                        //                                        $("#tbProducts").append(obj.ServiceData);
                                        $("#tbProducts").append(tr);



                                    },
                                    complete: function () {
                                        $.uiUnlock();
                                        $('#tbProducts tr#loading').remove();
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    }


                                });

                            }










                        });
                    </script>







                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">2014 - 2015 © Superstore. Brought to you by <a>Matrix Solutions</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Matrix Solutions!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
</asp:Content>

