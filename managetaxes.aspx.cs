﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managetaxes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
        BindBranches();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.TAXSTRUCTURE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }


    [WebMethod]
    public static string Insert(int TaxId, decimal TaxRate, string Description, string DrAcc,string CrAcc,bool DrVat,bool CrVat,string DrVatAcc,string CrVatAcc,bool ChkSur,decimal SurVal,string DrSur,string CrSur,bool IsActive,int BranchId )
    {


        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.TAXSTRUCTURE));

        string[] arrRoles = sesRoles.Split(',');


        if (TaxId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
      
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_ID = TaxId,
            Tax_Rate = TaxRate,
            Description = Description.Trim().ToUpper(),
            Dr_AccCode = DrAcc,
            Cr_AccCode = CrAcc,
            VatCode_Dr = DrVat,
            Dr_VatCode = DrVatAcc,
            VatCode_Cr = CrVat,
            Cr_VatCode = CrVatAcc,
            ChkSur = ChkSur,
            SurValue = SurVal,
            Dr_Sur = DrSur,
            Cr_Sur = CrSur,
            UserId = Id,
            IsActive = IsActive,
            BranchId = BranchId,
        };
        status = new TaxStructureBLL().InsertUpdate(objTaxStructure);
        var JsonData = new
        {
            Tax = objTaxStructure,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(Int32 TaxId)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.TAXSTRUCTURE));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_ID = TaxId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new TaxStructureBLL().DeleteTax(objTaxStructure);
        var JsonData = new
        {
            Tax = objTaxStructure,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}