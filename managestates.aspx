﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managestates.aspx.cs" Inherits="managestates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">
    var m_StateID = -1;

    function GetVMResponse(Id, Title, IsActive, Status) {

        var obj = { State_ID: Id, State_Name: Title, IsActive: IsActive };

        $("#dvVMDialog").dialog("close");
        if (Status == "I") {


            jQuery("#jQGridDemo").jqGrid('addRowData', Id, obj, "last");

            alert("State Added Successfully");

        }
        else {


            var myGrid = $("#jQGridDemo");
            var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


            myGrid.jqGrid('setRowData', selRowId, obj);

            alert("State Updated Successfully");
        }


        //   BindGrid();
    }


    function OpenVMDialog(Id) {

        $.uiLock('');
        $.ajax({
            type: "POST",
            data: '{"Id":"' + Id + '","Type": "' + "State" + '"}',
            url: "managearea.aspx/LoadUserControl",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvVMDialog").remove();
                $("body").append("<div id='dvVMDialog'/>");
                $("#dvVMDialog").html(msg.d).dialog({ modal: true });
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {
        BindGrid();

        $("#btnAdd").click(
       function () {

           OpenVMDialog(-1);


       }
       );

        $("#btnEdit").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No State is selected to Edit");
               return;
           }


           OpenVMDialog(m_StateID);



       }
       );

        $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No State is selected to Delete");
               return;
           }

           var Stateid = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'State_ID')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');

               $.ajax({
                   type: "POST",
                   data: '{"StateId":"' + Stateid + '"}',
                   url: "managestates.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       BindGrid();
                       alert("State is Deleted successfully.");




                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });
           }


       }
       );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>States</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                 
                      

    
     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage States</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
      
      <table>
      
      <tr><td>
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
      </td></tr>

      <tr><td>
      
         <table cellspacing="0" cellpadding="0" style="margin-top:5px">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add State</div></td>
                                            <td>&nbsp;</td><td> <div id="btnEdit"  class="btn btn-primary btn-small" >Edit State</div></td>
                                            <td>&nbsp;</td><td> <div id="btnDelete"  class="btn btn-primary btn-small" >Delete State</div></td>
                                            </tr>
                                            </table>
      </td></tr>
      </table>
                    
      
     
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageStates.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['State Id', 'Title','IsActive','UserId'],
                        colModel: [
                                    { name: 'State_ID', key: true, index: 'State_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'State_Name', index: 'State_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'UserId', index: 'UserId', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                   


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'State_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "States List",

                        editurl: 'handlers/ManageStates.ashx',



                      ignoreCase: true,
                         toolbar: [true, "top"],

                    });



                     var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });





                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_StateID = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_StateID = $('#jQGridDemo').jqGrid('getCell', rowid, 'State_ID');


                  
            
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>


