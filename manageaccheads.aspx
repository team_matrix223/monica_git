﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageaccheads.aspx.cs" Inherits="manageaccheads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
 
     
   
<script language="javascript" type="text/javascript">

    function ApplyRoles(Roles) {


        $("#<%=hdnRoles.ClientID%>").val(Roles);
       }
    var m_HeadId = 0;

    function ResetControls() {
        m_HeadId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        $("#ddlAccount").val("");
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add AccHead");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update AccHead");

        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_HeadId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }

        var Divison = $("#ddlAccount").val();
      



        $.ajax({
            type: "POST",
            data: '{"HeadId":"' + Id + '", "HeadName": "' + Title + '","Account": "' + Divison + '"}',
            url: "manageaccheads.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -11) {
                    alert("You don't have permission to perform this action..Consult Admin Department.");
                    return;
                }



                if (obj.Status == 0) {

                    alert("Insertion Failed.Account Head with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    BindGrid();
                    alert("AccountHead is added successfully.");
                }
                else {
                    ResetControls();
                    BindGrid();
                    alert("AccountHead is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {
        BindGrid();


        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").click(
                    function () {


                        m_HeadId = 0;
                        InsertUpdate();
                    }
                    );
                }
                else if (arrRole[i] == "3") {
                    $("#btnUpdate").click(
      function () {
          InsertUpdate();
      }
      );

                }

            }
        }


      

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Acc Heads</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit AccHead</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" style="width:300px">
                     
                   
                     <tr><td class="headings" >Acc GroupName:</td><td colspan = "3">  <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                     <tr><td class="headings">Acc Master:</td><td colspan = "3">    <select id="ddlAccount" style="width:300px" class="validate ddlrequired" >
                     <option value="B">BALANCE SHEET</option>
                     <option value="I">INCOME/EXPENDITURE</option></td></tr>                               
                    

                      
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add AccHead</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update AccHead</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Account Heads</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageAccHeads.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['AccountId', 'AccountCode','Name','Amount','Dr/CR','Bal/INC'],
                        colModel: [
                                    { name: 'H_Id', key: true, index: 'H_Id', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'H_CODE', index: 'H_CODE', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'H_NAME', index: 'H_NAME', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Amount', index: 'Amount', width: 200, stype: 'text', sortable: true,hidden: true, editable: true, editrules: { required: true } },
                                    { name: 'DR_CR', index: 'DR_CR', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                    { name: 'BAL_INC', index: 'BAL_INC', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'H_Id',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "AccHeads List",

                        editurl: 'handlers/ManageAccHeads.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_HeadId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_HeadId = $('#jQGridDemo').jqGrid('getCell', rowid, 'H_Id');
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'H_NAME'));

                    txtTitle.focus();
                    var div = $('#jQGridDemo').jqGrid('getCell', rowid, 'BAL_INC')
                     $('#ddlAccount option[value=' + div + ']').prop('selected', 'selected');
                   
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '600');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>
