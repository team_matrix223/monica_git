﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindBranches();
        }
    }



    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string InsertUpdate(DeliveryDetail[] objDeliveryDetails, string ItemType,int Branch)
    {
       
        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Scheme");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1P");
        dt.Columns.Add("Dis2P");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("Dis3P");
        dt.Columns.Add("Godown_ID");
        dt.Columns.Add("RowNum");
        dt.Columns.Add("Excise_Duty");
        dt.Columns.Add("Excise_Amt");
        DataRow dr;


        Int32 RowNumm = 1;
        foreach (var item in objDeliveryDetails)
        {
            dr = dt.NewRow();
            dr["Item_Code"] = item.Item_Code;
            dr["Scheme"] = 0;
            dr["Qty"] = 0;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = 0;
            dr["Dis1P"] = 0;
            dr["Dis2P"] = 0;
            dr["TaxP"] = 0;
            dr["Dis3P"] = 0;
            dr["Godown_ID"] = 0;
            dr["RowNum"] = RowNumm;
            dr["Excise_Duty"] = 0;
            dr["Excise_Amt"] = 0;
            dt.Rows.Add(dr);
            RowNumm = RowNumm + 1;


        }

        int Status = new SaleBLL().Insert(Branch, dt, ItemType);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetProductsByType(string ItemType,int Branch)
    {
        
        List<Sale> lstProducts = new SaleBLL().GetProductsByType(ItemType, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetOrdersBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch)
    {
       

        List<Bill> lstBills = new BillBLL().GetOrderBillsByDate(DateFrom,DateTo,ItemType, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetOrderItemBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch)
    {


        List<BillDetail> lstBills = new BillBLL().GetOrdersBillItemByDate(DateFrom, DateTo, ItemType, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetOptions(string ProcessType)
    {


        List<Bill> lstOptions = new BillBLL().GetOptions(ProcessType);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            lstOptions = lstOptions

        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string Insert(string BillNowPrefix, string Amount, string BillNo, string ItemType, string Saletype,int Branch)
    {
        
        string[] BillNowPrefixData = BillNowPrefix.Split(',');
        string[] AmountData = Amount.Split(',');
        string[] BillNoData = BillNo.Split(',');

        DataTable dt = new DataTable();

        dt.Columns.Add("BillNowPrefix");
        dt.Columns.Add("Amount");
        dt.Columns.Add("BillNo");



        for (int i = 0; i < BillNowPrefixData.Length; i++)
        {
            if (BillNowPrefixData[i] != "")
            {
                DataRow dr = dt.NewRow();

                dr["BillNowPrefix"] = Convert.ToString(BillNowPrefixData[i]);
                dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                dr["BillNo"] = Convert.ToInt32(BillNoData[i]);

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteBills(Branch, dt,ItemType,Saletype);
        var JsonData = new
        {

           Status = status
        };
        return ser.Serialize(JsonData);



    }


    [WebMethod]
    public static string DeleteItemsBill(string ItemCode, string Amount,  string ItemType, string Saletype, int Branch,DateTime DateFrom,DateTime DateTo)
    {

        string[] ItemCodeData = ItemCode.Split(',');
        string[] AmountData = Amount.Split(',');
       

        DataTable dt = new DataTable();

     
        dt.Columns.Add("Amount");
        dt.Columns.Add("ItemCode");



        for (int i = 0; i < ItemCodeData.Length; i++)
        {
            if (ItemCodeData[i] != "")
            {
                DataRow dr = dt.NewRow();

              
                dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                dr["ItemCode"] = Convert.ToString(ItemCodeData[i]);

              

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteItemBills(Branch, dt, ItemType, Saletype,DateFrom,DateTo);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }


    [WebMethod]
    public static string UpdateStock(int BranchId)
    {

        JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new SaleBLL().UpdateStock(BranchId);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }


}


