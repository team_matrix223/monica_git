﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class screen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string BindCategories()
    {
        Settings objSettings = new Settings();
        string catData = new CategoriesBLL().GetCategoriesHTML(objSettings);
        var JsonData = new
        {
            categoryData = catData,
            setttingData=objSettings
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string AdvancedSearch(int CategoryId, string Keyword)
    {

        string prodData = new ProductBLL().AdvancedSearch(CategoryId, Keyword.Trim());
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindBanks()
    {

        string bankData = new BankBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BankOptions = bankData

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]


    public static string InsertUpdate(string CustomerId, string CustomerName, decimal BillValue, decimal DiscountPer, decimal DiscountAmt, decimal AddTaxAmt, decimal NetAmt, string BillMode, string CreditBank, decimal CashAmt, decimal CreditAmt, decimal CrCardAmt, decimal RoundAmt, int CashCustCode, string CashCustName, int TableNo, decimal SerTax, string itemcodeArr, string qtyArr, string priceArr, string taxArr, string orgsalerateArr, string AmountArr, string TaxAmountArr, string SurValArr)
    {
        Bill objBill = new Bill()
        {
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            Bill_Value = BillValue,
            DiscountPer = DiscountPer,
            Less_Dis_Amount = DiscountAmt,
            Add_Tax_Amount = AddTaxAmt,
            Net_Amount = NetAmt,
            BillMode = BillMode,
            CreditBank = CreditBank,
            UserNO = 0,
            Bill_Type = 1,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CrCard_Amount = CrCardAmt,
            Round_Amount = RoundAmt,
            Bill_Printed = false,
            Passing = false,
            CashCust_Code = CashCustCode,
            CashCust_Name = CashCustName,
            Tax_Per = 0,
            R_amount = RoundAmt,
            tableno = TableNo,
            remarks = string.Empty,
            servalue = SerTax,
            ReceiviedGRNNo = 0,
            EmpCode = 0,

        };

        string[] ItemCode = itemcodeArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] Tax = taxArr.Split(',');
        string[] OrgSaleRate = orgsalerateArr.Split(',');
        string[] Amount = AmountArr.Split(',');
        string[] TaxAmount = TaxAmountArr.Split(',');
        string[] SurVal = SurValArr.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("OrgSaleRate");
        dt.Columns.Add("SurVal");



        for (int i = 0; i < ItemCode.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = ItemCode[i];
            dr["Rate"] = Convert.ToDecimal(Price[i]);
            dr["Qty"] = Convert.ToDecimal(Qty[i]);
            dr["Amount"] = Convert.ToDecimal(Amount[i]);
            dr["Tax"] = Convert.ToDecimal(Tax[i]);
            dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
            dr["OrgSaleRate"] = Convert.ToDecimal(OrgSaleRate[i]);
            dr["SurVal"] = Convert.ToDecimal(SurVal[i]);
            dt.Rows.Add(dr);
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new BillBLL().InsertUpdate(objBill, dt);
        var JsonData = new
        {
            Bill = objBill,
            Status = status
        };
        return ser.Serialize(JsonData);
    }

}