﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Form1.aspx.cs" Inherits="Form1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="js/SearchPlugin.js"></script>




   <script type="text/javascript">


       var ProductCollection = [];
       var BillCollection = [];
       var ItemBillCollection = [];
       var TotalItem = 0;
       function clsProduct() {
         
           this.Item_Code = "";
           this.Item_Name = "";
           this.Rate = 0;
           this.Sale_Rate = 0;
           this.MRP = 0;
           this.QTY_TO_LESS = 0;
       

       }


       function clsBill() {
          
           this.BillNo = "";
           this.Amount = 0;
           this.BillId = 0;


       }


       function clsItemBill() {

        
           this.Amount = 0;
         
           this.ItemCode = "";
           this.ItemName = "";


       }
       function GetPluginData(Type) {

               var m_ItemCode = $("#ddlProducts option:selected").attr("item_code");

               $("#txtCode").val(m_ItemCode);
               $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
             
               $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
               $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
             
       }


       $(document).on('change', '[name=chkbox1]', function () {
           if ($(this).prop('checked') == true) {

               var Val = $(this).attr('val');
               TotalItem = Number(TotalItem) + Number(Val);
               $("#lblSalee").html(TotalItem);



           }
           else {
               var Val = $(this).attr('val');

               TotalItem = Number(TotalItem) - Number(Val);
               $("#lblSalee").html(TotalItem);
           }

       });
       
       function BindBills() {

           var html = "";

           for (var i = 0; i < BillCollection.length; i++) {

               
         

               html += "<tr>";
               html += "<td>" + BillCollection[i]["BillNo"] + "</td>";
               html += "<td>" + BillCollection[i]["Amount"] + "</td>";
               html += "<td><input type='checkbox' name= 'chkbox' id='chk_" + BillCollection[i]["BillId"] + "' /></td>";
              
             
               html += "</tr>";


           }
          
        
           $("#tbShowBills").html(html);



       }

       function BindItemBills() {


           var html = "";
          

           for (var i = 0; i < ItemBillCollection.length; i++) {


             

               html += "<tr>";
              
               html += "<td>" + ItemBillCollection[i]["ItemCode"] + "</td>";
            
               html += "<td>" + ItemBillCollection[i]["ItemName"] + "</td>";
               
               html += "<td>" + ItemBillCollection[i]["Amount"] + "</td>";

               html += "<td><input type='checkbox' name= 'chkbox1' val = " + ItemBillCollection[i]["Amount"] + "  id='chk_" + ItemBillCollection[i]["ItemCode"] + "' /></td>";
              


               html += "</tr>";

              

           }


           $("#tbShowItems").html(html);



       }

       function BindRows() {


           var TotalAmount = 0;
           var html = "";

           for (var i = 0; i < ProductCollection.length; i++) {



               html += "<tr>";
               html += "<td>" + ProductCollection[i]["Item_Code"] + "</td>";
               html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["Qty_In_Case"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["Scheme"] + "</td>";
               html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
               html += "<td>" + ProductCollection[i]["Sale_Rate"] + "</td>";
               html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["QTY_TO_LESS"] + "</td>";
               html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer;width:20px'   /></td>";
               html += "</tr>";

               TotalAmount += parseFloat(ProductCollection[i]["Amount"])

           }

           $("#txtBillval").val(TotalAmount.toFixed(2));
           //var DisPer = $("#ddlDealer option:selected").attr("dis");
           var DisPer = "0";
           var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
           $("#txtDisPer").val(DisPer);
           $("#txtDisAmt").val(DisAmt);
           $("#txtAdj").val("0");
           $("#txtnetAmt").val(Number(TotalAmount) - Number(DisAmt));
           $("#tbKitProducts").html(html);



       }

       function ResetList() {


           $("#txtCode").val("");
           $("#txtName").val("");
           
           $("#txtRate").val("");
           $("#txtMarketPrice").val("");
      
        
           $("#ddlProducts").html("<option value='0'></option>");
           $("#txtddlProducts").val("").focus();
           TotalItem = 0;
           $("#lblSalee").html("0");
        



       }








       $(document).ready(
           function () {



               $("#btnUpdateStock").click(
               function () {


                   if ($("#ddlBranch").val() == "0") {

                       alert("Please choose Branch");
                       return;

                   }
                   else {

                       if (confirm("Are You sure you want to update stock of " + $("#ddlBranch option:selected").text() + "?")) {

                           $.uiLock('');
                         


                           $.ajax({
                               type: "POST",
                               data: '{"BranchId":"' + $("#ddlBranch").val() + '"}',
                               url: "Form1.aspx/UpdateStock",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                  

                                   if (obj.Status == "0") {
                                       alert("Stock Updated Succesfully");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();

                               }



                           });









                       }
                   }
               }
               );


               $("#txtStartDate,#txtEndDate").val($("#<%=hdnDate.ClientID%>").val());

               $("#ddlProType").change(
         function () {

             var type = $("#ddlProType").val();
             var processtype = "";
             if (type == "0") {
                 alert("Choose Which Type Of Processing you want to do ...?");
                 return;
             }
             if (type == "1") {
                 processtype = "ItemWise";
             }
             else {
                 processtype = "BillWise";
             }

             //$.ajax({
             //    type: "POST",
             //    data: '{ "ProcessType": "' + processtype + '"}',
             //    url: "managesales.aspx/GetOptions",
             //    contentType: "application/json",
             //    dataType: "json",
             //    success: function (msg) {

             //        var obj = jQuery.parseJSON(msg.d);
             //        $("#ddlBillType").html(obj.lstOptions);

             //    },
             //    error: function (xhr, ajaxOptions, thrownError) {

             //        var obj = jQuery.parseJSON(xhr.responseText);
             //        alert(obj.Message);
             //    },
             //    complete: function () {

             //        $.uiUnlock();
             //    }

             // });

         });



               $("#dvShowBills").css("display", "none");
               $("#dvShowItems").css("display", "none");
               $("#btnShow").click(
                   function () {

                       TotalItem = 0;
                       $("#lblSalee").html("0");

                       BillCollection = [];
                       ItemBillCollection = [];

                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       $('#tbShowItems tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       var ItemType = "";

                       var ItemType = $("#ddlBillType option:selected").text();

                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();
                       var Branch = $("#ddlBranch").val();
                       var ProType = $("#ddlProType").val();
                       if (ProType == "2") {

                           $("#dvShowBills").css("display", "block");
                           $("#dvaddItems").css("display", "none");
                           $("#dvShowItems").css("display", "none");

                           $.ajax({
                               type: "POST",
                               data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","ItemType": "' + ItemType + '","Branch": "' + Branch + '"}',
                               url: "Form1.aspx/GetOrdersBYDate",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                   if (obj.BillsList.length > 0) {
                                       for (var i = 0; i < obj.BillsList.length; i++) {


                                           TO = new clsBill();


                                           TO.BillNo = obj.BillsList[i]["BillNowPrefix"];
                                           TO.Amount = obj.BillsList[i]["Net_Amount"];
                                           TO.BillId = obj.BillsList[i]["Bill_No"];
                                           BillCollection.push(TO);

                                       }
                                       BindBills();
                                   }
                                   else {
                                       alert("No Bills Found.");
                                       return;

                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {

                                   $.uiUnlock();
                               }

                           });
                       }
                       else if (ProType == "1") {

                           $("#dvShowBills").css("display", "none");
                           $("#dvaddItems").css("display", "none");
                           $("#dvShowItems").css("display", "block");

                           $.ajax({
                               type: "POST",
                               data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","ItemType": "' + ItemType + '","Branch": "' + Branch + '"}',
                               url: "Form1.aspx/GetOrderItemBYDate",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {



                                   var obj = jQuery.parseJSON(msg.d);


                                   if (obj.BillsList.length > 0) {
                                       for (var i = 0; i < obj.BillsList.length; i++) {


                                           TO = new clsItemBill();

                                           TO.Amount = obj.BillsList[i]["Amount"];
                                           TO.ItemCode = obj.BillsList[i]["Item_Code"];
                                           TO.ItemName = obj.BillsList[i]["Item_Name"];
                                           ItemBillCollection.push(TO);

                                       }


                                       BindItemBills();
                                   }
                                   else {
                                       alert("No Bills Found.");
                                       return;

                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {


                                   $.uiUnlock();
                               }

                           });

                       }



                   });




               $("#btnDelete").click(
                   function () {

                       var IsChked = false;
                       var Billno = [];
                       var BillNowPrefix = [];
                       var Amount = [];
                       var Saletype = "";
                       var ItemType = "";
                       var ItemValue = $("#ddlItemType").val();
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }


                       Saletype = $("#ddlBillType option:selected").text();
                       var Branch = $("#ddlBranch").val();
                       for (var i = 0; i < BillCollection.length; i++) {
                           var billid = 0;
                           var Billprefix = "";
                           var netamount = 0;
                           billid = BillCollection[i]["BillId"];
                           Billprefix = BillCollection[i]["BillNo"];
                           netamount = BillCollection[i]["Amount"];


                           if ($("#chk_" + billid).prop('checked') == true) {


                               Billno[i] = billid;
                               BillNowPrefix[i] = Billprefix;
                               Amount[i] = netamount;
                               IsChked = true;
                           }

                       }

                       if (IsChked != true) {

                           alert("atleast one bill should be there to Delete");
                           $.uiUnlock();
                           return;
                       }


                       if (confirm("Are You sure to delete this record")) {
                           $.uiLock('');


                           $.ajax({
                               type: "POST",
                               data: '{"BillNowPrefix":"' + BillNowPrefix + '", "Amount": "' + Amount + '","BillNo": "' + Billno + '","ItemType": "' + ItemType + '","Saletype": "' + Saletype + '","Branch": "' + Branch + '"}',
                               url: "Form1.aspx/Insert",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);

                                   if (obj.Status == "-4") {
                                       alert("Please Insert Items First");
                                       return;
                                   }

                                   if (obj.Status == "0") {
                                       alert("Sale Deleted Successfully");

                                       BillCollection = [];

                                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                   }
                                   else {
                                       alert("Operation Failed");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();

                               }



                           });

                       }



                   });




               $("#btnDeleteItem").click(
                   function () {


                       var IsChked = false;

                       var ItemC = [];
                       var Amount = [];
                       var Saletype = "";
                       var ItemType = "";
                       var ItemValue = $("#ddlItemType").val();
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }

                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();

                       Saletype = $("#ddlBillType option:selected").text();
                       var Branch = $("#ddlBranch").val();
                       for (var i = 0; i < ItemBillCollection.length; i++) {

                           var ItemCode = "";
                           var netamount = 0;
                           ItemCode = ItemBillCollection[i]["ItemCode"];

                           netamount = ItemBillCollection[i]["Amount"];


                           if ($("#chk_" + ItemCode).prop('checked') == true) {



                               ItemC[i] = ItemCode;
                               Amount[i] = netamount;
                               IsChked = true;
                           }

                       }


                       if (IsChked != true) {

                           alert("atleast one bill should be there to Delete");
                           $.uiUnlock();
                           return;
                       }


                       if (confirm("Are You sure to delete this record")) {
                           $.uiLock('');



                           $.ajax({
                               type: "POST",
                               data: '{"ItemCode":"' + ItemC + '", "Amount": "' + Amount + '","ItemType": "' + ItemType + '","Saletype": "' + Saletype + '","Branch": "' + Branch + '","DateFrom":"' + DateFrom + '","DateTo":"' + DateTo + '"}',
                               url: "Form1.aspx/DeleteItemsBill",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                   if (obj.Status == "-4") {
                                       alert("Please Insert Items First");
                                       return;
                                   }

                                   if (obj.Status == "0") {
                                       alert("Sale Deleted Successfully");

                                       ItemBillCollection = [];

                                       $('#tbShowItems tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                   }
                                   else {
                                       alert("Operation Failed");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();

                               }



                           });

                       }



                   });




               $("#btnItem").click(
                   function () {
                       $("#dvShowBills").css("display", "none");
                       ProductCollection = [];

                       $('#tbKitProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       var ItemType = "";
                       $("#dvaddItems").css("display", "block");
                       var Branch = $("#ddlBranch").val();

                       var ItemValue = $("#ddlItemType").val();
                       if (ItemValue == "0") {
                           alert("Choose Type");
                           return;
                       }
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }

                       $.uiLock();
                       $.ajax({
                           type: "POST",
                           data: '{ "ItemType": "' + ItemType + '", "Branch": "' + Branch + '"}',
                           url: "Form1.aspx/GetProductsByType",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);

                               for (var i = 0; i < obj.productLists.length; i++) {


                                   TO = new clsProduct();


                                   TO.Item_Code = obj.productLists[i]["Item_Code"];
                                   TO.Item_Name = obj.productLists[i]["Item_Name"];
                                   TO.Rate = obj.productLists[i]["Rate"];
                                   TO.Sale_Rate = obj.productLists[i]["Rate"];
                                   TO.MRP = obj.productLists[i]["MRP"];
                                   TO.QTY_TO_LESS = obj.productLists[i]["qty_to_less"];

                                   ProductCollection.push(TO);
                                   BindRows();

                               }

                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {

                               $.uiUnlock();
                           }

                       });





                   }

                   );


               $(document).on("click", "#btnDel", function (event) {

                   var RowIndex = Number($(this).closest('tr').index());

                   ProductCollection.splice(RowIndex, 1);
                   BindRows();


               });


               $("#btnSave").click(
               function () {



                   if (ProductCollection.length == 0) {

                       alert("Please choose Delivery Items");
                       $("#ddlProducts").focus();
                       return;
                   }
                   var ItemType = '';
                   if ($("#ddlItemType").val() == "1") {
                       ItemType = 'Sale';
                   }
                   else {
                       ItemType = 'Order';
                   }


                   var Branch = $("#ddlBranch").val();


                   var DTO = { 'objDeliveryDetails': ProductCollection, 'ItemType': ItemType, 'Branch': Branch };



                   $.ajax({
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       url: "Form1.aspx/InsertUpdate",
                       data: JSON.stringify(DTO),
                       dataType: "json",
                       success: function (msg) {

                           var obj = jQuery.parseJSON(msg.d);


                           alert("Item Saved Successfully");

                           ProductCollection = [];

                           $('#tbKitProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();



                       },
                       error: function (xhr, ajaxOptions, thrownError) {

                           var obj = jQuery.parseJSON(xhr.responseText);
                           alert(obj.Message);
                       },
                       complete: function () {

                       }
                   });









               });


               $("#btnAddKitItems").click(
   function () {



       TO = new clsProduct();
       TO.Item_ID = $("#ddlProducts option:selected").val();
       TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
       TO.Item_Name = $("#ddlProducts option:selected").attr("item_name");
       TO.Qty_In_Case = $("#ddlProducts option:selected").attr("Qty_In_Case");
       TO.Scheme = "false";
       TO.Rate = $("#txtRate").val();
       TO.Sale_Rate = $("#ddlProducts option:selected").attr("Sale_Rate");
       TO.MRP = $("#ddlProducts option:selected").attr("mrp");
       TO.QTY_TO_LESS = $("#ddlProducts option:selected").attr("Qty_To_Less");
       TO.Bill_Date = '1/1/1';
       ProductCollection.push(TO);

       BindRows();
       ResetList();

   }

   );




               $('#txtStartDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });

               $('#txtEndDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });


               $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");

               var Godown = "17";





               $("#ddlProducts").supersearch({
                   Type: "Product",
                   Caption: "Please enter Item Name/Code ",
                   AccountType: "",
                   Godown: Godown,
                   Width: 214,
                   DefaultValue: 0
               });


           }
           );
   </script>

    


<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
     <form id="form1" runat="server">
      <asp:HiddenField ID="hdnDate" runat="server"/>
    <div class="right_col" role="main">
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Manage Sales</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">

                                  <tr>
                                  <td colspan="100%">
                                  <table><tr>
                                  
                                                   <td  style="width: 150px">
                                     <label class="control-label">Choose Branch:</label>
                                    </td>

                                         <td colspan="300px">      
                                     <select id="ddlBranch" style="width:321px;height:30px" clientidmode="Static" runat="server">
                                           
                                             </select>


                                    </td>

                                    <td><button type="button" class="btn btn-success" id="btnUpdateStock"  style="width:150px">Update Stock</button></td>
                                  
                                  </tr></table>
                 
                                   </td>
                                   
                                    </tr>
                                    <tr>

                                        
                                    <td  style="width: 100px">
                                     <label class="control-label">Choose Items:</label>
                                    </td>
                                        <td style="width: 200px">      
                                     <select id="ddlItemType" style="width:150px">
                                            <option value="0">--SELECT--</option>
                                             <option value="1">Bill Items</option>
                                             <option value="2">Order Items</option>
                                             </select>


                                    </td>
                                           <td colspan="100%"><button type="button" class="btn btn-success" id="btnItem" style="width:150px;margin-top:5px">Items</button></td>

                                        </tr>
                                       <tr>
                                       <td style="width: 120px"> <label class="control-label">Processing Type:</label></td>
                                    <td style="width: 200px">      
                                     <select id="ddlProType" style="width:150px">
                                            <option value="0">--SELECT--</option>
                                             <option value="1">Items Wise</option>
                                             <option value="2">Bill Wise</option>
                                             </select>


                                    </td>                                          
                                     <td style="width: 100px"><label class="control-label">Choose Type:</label></td>
                                     <td style="width: 200px">      
                                     <select id="ddlBillType" style="width:150px">
                                             <option value="0">--SELECT--</option>
                                             <option value="">Retail Bill</option>
                                             <option value="2">Order</option>
                                             </select>


                                    </td>   
                                      <td style="width:80px"><label class="control-label">From:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtStartDate" style="width:107px" /></td>
                                       <td style="width:80px"><label class="control-label">To:</label></td>  <td  style="width:150px"> <input type="text" class="form-control"  id="txtEndDate" style="width:107px"  /></td>
                                        <td><button type="button" class="btn btn-success" id="btnShow"  style="width:150px">Show</button></td>





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>
                          


                    <div class="row" id="dvaddItems" style="display:none"> 
                  <div class="col-md-12">
                            <div class="x_panel" style="background:seashell;padding:0px">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                 
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px">
<thead>
<tr><th>
 

Item/Code</th><th>Code</th><th>Name</th><th>Rate</th><th>MRP</th></tr>

</thead>
<tbody>
<tr>
<td id="DKID">
 
 


<select style="width:154px" id="ddlProducts" class="form-control"></select>
</td>
<td>
<input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtName" type="text"  class="form-control customTextBox"   style="width:320px"/></td>

<td><input type="text" id="txtRate"  class="form-control customTextBox"   /></td>
<td><input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>

<td>
<button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  
                                  
                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:300px;overflow-y:scroll;min-height:300px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th style="display:none">CaseQty</th><th style="display:none">Scheme</th><th>Rate</th><th>SaleRate</th><th>MRP</th><th style="display:none">QTy_To_Less</th></tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" style="margin-top:5px"  class="btn btn-danger" > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>

                      <div class="row" id="dvShowBills" style="display:none"> 
               
                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped"  style="font-size:12px;margin-top:-18px;">
                                         <thead>
<tr><th>BillNo</th><th>Amount</th><th>Select</th></tr>
</thead>
<tbody id="tbShowBills">
 
 


 
</tbody>
                                    </table>








                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnDelete" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Delete</div>
    
                                     </td>
                                    
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                         <div class="row" id="dvShowItems" style="display:none"> 
               
                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">




                                                                        <table    class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th>Amount</th><th>Select</th></tr>
</thead>
<tbody id="tbShowItems">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnDeleteItem" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Delete</div>
    
                                     </td>
                                     <td>TotalSales:</td><td><asp:Label  style="font-weight:bold" ID ="lblSalee" runat="server" ClientIDMode ="Static"></asp:Label></td>
                                    
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                            </div>
                        </div>
  
    </div>
         </form>

</asp:Content>

