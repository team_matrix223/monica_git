﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageexcise.aspx.cs" Inherits="manageexcise" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    
<script language="javascript" type="text/javascript">

    function ApplyRoles(Roles) {

        $("#<%=hdnRoles.ClientID%>").val(Roles);
    }

    </script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">

         
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Excise</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Excise</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                     <tr><td class="headings" >Description:</td><td >  <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                      <tr><td class="headings" >Teriff No:</td><td >  <input type="text"  name="txtTeriff" class="form-control validate required alphanumeric"   data-index="1" id="txtTeriff" style="width: 300px"/></td></tr>
                       <tr><td class="headings" style="width:150px">Excise Duty:</td><td >  <input type="text"  name="txtDuty" class="form-control validate required float  "   data-index="1" id="txtDuty" style="width: 300px"/></td></tr>
                     <tr><td class="headings">Acc. To Credit:</td><td style="text-align:left" ><asp:DropDownList ClientIDMode="Static" style="width:300px;height:35px"  ID ="ddlCrAccount" runat="server" ><asp:ListItem Value="0"></asp:ListItem></asp:DropDownList>
                    </td></tr>                               
                     <tr><td class="headings">Acc. To Debit:</td><td style="text-align:left">  <asp:DropDownList ClientIDMode="Static"  style="width:300px;height:35px" ID ="ddlDrAccount" runat="server"><asp:ListItem Value="0"></asp:ListItem></asp:DropDownList>
                    </td></tr>   
                     <tr><td class="headings" >Abatement:</td><td >  <input type="text"  name="txtAbatement" class="form-control validate required float"   data-index="1" id="txtAbatement" style="width: 300px"/></td></tr>                            
                    <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                      
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add  </div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-trash m-right-xs"></i> Update  </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >
<i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Excise</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave"  >
                    
      	          <table id="jQGridDemo">
    </table>

     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete" style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

<link href="semantic.css" rel="stylesheet" type="text/css" />
 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
     <script src="<%=CommonFunctions.SiteUrl()%>js/customValidation.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    
   <script type="text/javascript" src="js/SearchPlugin.js"></script> 



   
<script language="javascript" type="text/javascript">

   

    var m_ExciseId = -1;


    function ResetControls() {
        m_ExciseId = -1;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        $("#ddlCrAccount").html("<option value='0'></option>");
        $("#ddlDrAccount").html("<option value='0'></option>");
        $("#txtddlDrAccount").val('');
        $("#txtddlCrAccount").val('');
        $("#txtAbatement").val('');
        btnAdd.css({ "display": "none" });
        var arrRole = [];
        arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
       
        
        for (var i = 0; i < arrRole.length; i++) {
           
            if (arrRole[i] == "1") {
               
                $("#btnAdd").css({ "display": "block" });
            }

        }


        $("#txtTeriff").val("");
        $("#txtDuty").val("");
       
        $("#chkIsActive").prop("checked", "checked");
        btnUpdate.css({ "display": "none" });
    

        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_ExciseId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }

        var teriffNo = $("#txtTeriff").val();
        var exciseduty = $("#txtDuty").val();
        var CrAccount = 0;
        CrAccount = $("#ddlCrAccount").val();
       
        if (CrAccount == 0)
        {
            alert("Choose Account to credit.");
            $("#txtddlCrAccount").focus();
            return;
        }
        var DrAccount = 0;
        DrAccount = $("#ddlDrAccount").val();
       
        if (DrAccount == 0)
        {
            alert("Choose Account to debit.");
            $("#txtddlDrAccount").focus();
            return;
        }
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }

        var Abatement = $("#txtAbatement").val();

        $.ajax({
            type: "POST",
            data: '{"ExciseId":"' + Id + '", "ExciseHead": "' + Title + '","TeriffNo": "' + teriffNo + '","ExciseDuty": "' + exciseduty + '","CrAccCode": "' + CrAccount + '","DrAccCode": "' + DrAccount + '","IsActive": "' + IsActive + '","Abatement":"'+Abatement+'"}',
            url: "manageexcise.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == -11) {
                    alert("You don't have permission to perform this action..Consult Admin Department.");
                    return;
                }

                if (obj.Status == "-1") {

                    alert("Insertion Failed.Excise with TeriffNo with duplicate name already exists.");
                    return;
                }

                if (Id == "-1") {
                    ResetControls();
                    BindGrid();
                    alert("Excise with TeriffNo is added successfully.");
                }
                else {
                    ResetControls();
                    BindGrid();
                    alert("Excise with TeriffNo is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {

        $("#ddlCrAccount").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 300,
            DefaultValue: 0,
            Godown:0
        });


        $("#ddlDrAccount").supersearch({
            Type: "Accounts",
            Caption: "Please enter Account ",
            AccountType: "A",
            Width: 300,
            DefaultValue: 0,
            Godown: 0
        });



        BindGrid();
      
        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
           
            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {
                   
                    $("#btnAdd").show();
                    $("#btnAdd").click(
                    function () {

                        m_ExciseId = -1;
                        InsertUpdate();
                    }
                    );
                }
                else if (arrRole[i] == "3") {
                   
                    $("#btnUpdate").click(
                 function () {

                        var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                        if ($.trim(SelectedRow) == "") {
                        alert("No Excise is selected to Edit");
                        return;
                    }

                       InsertUpdate();
                    }
                    );
                }
                else if (arrRole[i] == "2") {
                    $("#btnDelete").show();
                    $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Excise is selected to Delete");
             return;
         }

         var ExciseId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Excise_ID')
         if (confirm("Are You sure to delete this record")) {
             $.uiLock('');

             $.ajax({
                 type: "POST",
                 data: '{"ExciseId":"' + ExciseId + '"}',
                 url: "manageexcise.aspx/Delete",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.status == -10) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }



                     if (obj.status == -1) {
                         alert("Deletion Failed. Excise is in Use.");
                         return
                     }



                     BindGrid();
                     alert("Excise is Deleted successfully.");
                     ResetControls();



                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     $.uiUnlock();
                 }
             });



         }


     }
     );


                }
            }
        }



      



      

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageExcise.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ExciseId', 'ExciseHead','TeriffNo','Excise Duty','CreditCode','DebitCode','Credit To','Debit To','IsActive','1','2','Abatement'],
                        colModel: [
                                    { name: 'Excise_ID', key: true, index: 'Excise_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'Excise_Head', index: 'Excise_Head', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Teriff_No', index: 'Teriff_No', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'ExciseDuty', index: 'ExciseDuty', width: 200, stype: 'text', sortable: true,hidden: false, editable: true, editrules: { required: true } },
                                    { name: 'Cr_AccCode', index: 'Cr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                    { name: 'Dr_AccCode', index: 'Dr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                     { name: 'Cr_AccCName', index: 'Cr_AccCName', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                    { name: 'Dr_AccCName', index: 'Dr_AccCName', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                     { name: 'sDr_AccCode', index: 'sDr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'sCr_AccCode', index: 'sCr_AccCode', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Abatement', index: 'Abatement', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                  
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Excise_Id',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Excise List",

                        editurl: 'handlers/ManageExcise.ashx',



                 
                     ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });





                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_ExciseId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                     $("#btnUpdate").css({ "display": "none" });
                     $("#btnReset").css({ "display": "none" });
                     $("#btnAdd").css({ "display": "none" });
                  

                     for (var i = 0; i < arrRole.length; i++) {

                         if (arrRole[i] == 1) {

                             $("#btnAdd").css({ "display": "block" });
                         }

                         if (arrRole[i] == 3) {


                             m_ExciseId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Excise_ID');
                             txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Excise_Head'));
                             $("#txtTeriff").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Teriff_No'));
                             $("#txtDuty").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ExciseDuty'));
                             $("#txtAbatement").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Abatement'));
                             txtTitle.focus();


                             var DrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_AccCode');
                             var sDrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Dr_AccCName');
                             $("#ddlDrAccount").html("<option selected=selected value='" + DrAccount + "'>" + sDrAccount + "</option>");
                             $("#txtddlDrAccount").val(sDrAccount);


                             var CrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_AccCode');
                             var sCrAccount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Cr_AccCName');
                             $("#ddlCrAccount").html("<option  selected=selected  value='" + CrAccount + "'>" + sCrAccount + "</option>");
                             $("#txtddlCrAccount").val(sCrAccount);

                             if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                 $('#chkIsActive').prop('checked', true);
                             }
                             else {
                                 $('#chkIsActive').prop('checked', false);

                             }
                             $("#btnAdd").css({ "display": "none" });
                             $("#btnUpdate").css({ "display": "block" });
                             $("#btnReset").css({ "display": "block" });

                         }
                     }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '600');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>

