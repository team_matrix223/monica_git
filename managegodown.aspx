﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managegodown.aspx.cs" Inherits="managegodown" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
     <script type="text/javascript" src="js/jquery.uilock.js"></script>
     
   
<script language="javascript" type="text/javascript">

   

    var m_GodownId = -1;
    function ApplyRoles(Roles) {


        $("#<%=hdnRoles.ClientID%>").val(Roles);
      }
    function ResetControls() {
        m_GodownId = -1;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        $("#ddlDivison").val("");
        $("#txtYear").val("");
        $("#txtCaption").val("");


        var arrRole = [];
        arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
        btnAdd.css({ "display": "none" });
        for (var i = 0; i < arrRole.length; i++) {
            if (arrRole[i] == "1") {

                btnAdd.css({ "display": "block" });
            }
      
        }


       
        $("#chkIsActive").prop("checked", "checked");
        btnUpdate.css({ "display": "none" });
    

        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("frmCity")) {
            return;
        }
        var Id = m_GodownId;
        
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }

        var Divison = $("#ddlDivison").val();
        var FinanclYr = $("#txtYear").val();
        var Caption = $("#txtCaption").val();

        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }

        $.ajax({
            type: "POST",
            data: '{"GodownId":"' + Id + '", "GodownName": "' + Title + '","Divison": "' + Divison + '","FinancialYr": "' + FinanclYr + '","Caption": "' + Caption + '","IsActive": "' + IsActive + '"}',
            url: "managegodown.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == -11) {
                    alert("You don't have permission to perform this action..Consult Admin Department.");
                    return;
                }
                if (obj.Status == -1) {

                    alert("Insertion Failed.Godown with duplicate name already exists.");
                    return;
                }

                if (Id == "-1") {
                    ResetControls();
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.godown.Godown_ID, obj.godown, "last");
                    alert("Godown is added successfully.");
                }
                else {
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.godown);
                    alert("Godown is Updated successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }

    $(document).ready(
    function () {
        BindGrid();



        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").show();
                    $("#btnAdd").click(
                    function () {

                       
                        m_GodownId = -1;
                        InsertUpdate();
                    }
                    );
                }
                else if (arrRole[i] == "3") {

                    $("#btnUpdate").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Godown is selected to Edit");
               return;
           }

           InsertUpdate();
       }
       );

                }
                else if (arrRole[i] == "2") {

                    $("#btnDelete").show();

                    $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Godown is selected to Delete");
               return;
           }

           var GodownId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Godown_Id')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');

               $.ajax({
                   type: "POST",
                   data: '{"GodownId":"' + GodownId + '"}',
                   url: "managegodown.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       if (obj.status == -10) {
                           alert("You don't have permission to perform this action..Consult Admin Department.");
                           return;
                       }

                       BindGrid();
                       alert("Godown is Deleted successfully.");




                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });



           }


       }
       );

                }

            }
        }


       


        

        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Godowns</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Godown</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                     <tr><td class="headings" >GodownName:</td><td colspan = "3" style="text-align:left">  <input type="text"  name="txtTitle" class="form-control validate required "   data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                     <tr><td class="headings">Div./Godown:</td><td colspan = "3" style="text-align:left">    <select id="ddlDivison" class="form-control" style="width:300px" >
                     <option value="Godown">Godown</option>
                     <option value="Divison">Divison</option></select></td></tr>                               
                      <tr><td class="headings">FinancialYear:</td><td style="text-align:left"> 
                    
                       <input type="text"  name="txtYear" class="form-control validate required" style="width:115px" placeholder = "eg-2016-2017"   data-index="1" id="txtYear" />
                       
                       </td><td  class="headings">Caption</td><td><input type="text"  name="txtCaption" class="form-control"   data-index="1" id="txtCaption" style="width:100px" /></td></tr>                               
                       <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add  </div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i> Update  </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >

<i class="fa fa-trash m-right-xs"></i> Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Godowns</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>

       <table cellspacing="0" cellpadding="0" style="margin-top:5px">
                                            <tr>
                                           
                                            
                                   <td > <div id="btnDelete" style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageGodowns.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Godown Id', 'Name','Divison','FinancialYear','Caption','IsActive'],
                        colModel: [
                                    { name: 'Godown_Id', key: true, index: 'Godown_Id', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'Godown_Name', index: 'Godown_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Divison', index: 'Divison', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'FinacialYear', index: 'FinacialYear', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Caption', index: 'Caption', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                       
                       
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Godown_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Godowns List",

                        editurl: 'handlers/ManageGodowns.ashx',



                     ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });







                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                   
                     
                    m_GodownId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                    $("#btnUpdate").css({ "display": "none" });
                    $("#btnReset").css({ "display": "none" });
                    $("#btnAdd").css({ "display": "none" });
                  

                      for (var i = 0; i < arrRole.length; i++) {

                          if (arrRole[i] == 1) {
                         
                              $("#btnAdd").css({ "display": "block" });
                          }

                          if (arrRole[i] == 3) {
                              m_GodownId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Godown_Id');
                              $("#btnUpdate").css({ "display": "block" });
                              $("#btnReset").css({ "display": "block" });
                              $("#btnAdd").css({ "display": "none" });


                              txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Godown_Name'));

                              txtTitle.focus();
                              var div = $('#jQGridDemo').jqGrid('getCell', rowid, 'Divison')
                              $('#ddlDivison option[value=' + div + ']').prop('selected', 'selected');
                              $("#txtYear").val($('#jQGridDemo').jqGrid('getCell', rowid, 'FinacialYear'));

                              $("#txtCaption").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Caption'));
                              if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                  $('#chkIsActive').prop('checked', true);
                              }
                              else {
                                  $('#chkIsActive').prop('checked', false);

                              }

                          }
                      }


                  

                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '600');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>

