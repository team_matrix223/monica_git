﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managecities : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CITIES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string Insert(int CityId, string Title)
    {
        Cities objCity = new Cities()
        {
            City_ID = CityId,
            City_Name = Title.Trim().ToUpper(),


        };
        int status = new CitiesBLL().InsertUpdate(objCity);
        var JsonData = new
        {
            City = objCity,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]

    public static string Delete(Int32 CityId)
    {
        Cities objCity = new Cities()
        {
            City_ID = CityId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new CitiesBLL().DeleteCity(objCity);
        var JsonData = new
        {
            City = objCity,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}