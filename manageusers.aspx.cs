﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class manageusers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string InsertUpdate(User objUser)
    {


        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        objUser.BranchId = Branch;
        int Status = new UserBLL().InsertUpdate(objUser);
      
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            Status = Status

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static Designations[] GetDesignations()
    {
         var data = new DesignationBLL().GetAll();
        return data.ToArray();
    }


    
    [WebMethod]
    public static Branches[] GetBranches()
    {
         var data = new BranchBLL().GetAll();
        return data.ToArray();
    }

    
}