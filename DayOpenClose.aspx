﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DayOpenClose.aspx.cs" Inherits="backoffice_DayOpenClose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      
	 
		
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:Panel ID="pnlOpenClose" runat="server">
       
    <table style="width:350px;background-color:#DCDCDC">
    <tr><td style="background-color:gray;color:White;font-weight:bold">Day Open Close</td></tr>
    <tr><td>
    <table style="width:100%">
    <tr><td><asp:RadioButton ID="rbDayOpen" Checked="true" runat="server"  Text="Day Open"  GroupName="Grp1"/></td><td align="right">mm/dd/yy<asp:TextBox ID="txtDate" ForeColor="Gray" ReadOnly="true" Font-Italic="true" runat="server" BackColor="#DCDCDC"  ></asp:TextBox></td></tr>
    <tr><td><asp:RadioButton ID="rbDayClose" runat="server"  Text="Day Close" GroupName="Grp1"/></td><td></td></tr>
    <tr><td colspan="2"></td></tr>
    </table>
    
    </td></tr>
    <tr>
    <td style="padding-left:10px">
    
    <asp:HiddenField ID="hdnOpenedDay" runat="server" />
    <asp:HiddenField ID="hdnClosedDay" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnLastOpenedDate" runat="server" />

    <asp:Label ForeColor="#EA2F00" Font-Italic="true" ID="lblMessage" runat="server"></asp:Label></td>
    </tr>
    <tr>
   
    <td align="right" style="background-color:Silver">
    <table>
    <tr><td><asp:Button ID="btnOK"  class="btn btn-primary btn-small"   runat="server" 
            Text="OK" onclick="btnOK_Click" /></td><td>
            <asp:Button  class="btn btn-primary btn-small"   ID="btnCancel" runat="server" 
                Text="Cancel" onclick="btnCancel_Click" /></td></tr>
    </table>
    </td>
    </tr>
    </table>
    </asp:Panel>
    
        <asp:Panel ID="pnlVerify" runat="server"  Visible="false">
            <table style="width:350px;background-color:#DCDCDC">
    <tr><td style="background-color:gray;color:White;font-weight:bold">Day Open Close</td></tr>
    <tr>
        <td >

            <table>
                <tr><td>There are some Pending Delivery Notes. Are You Sure you want to Continue.</td></tr>
                <tr><td>
                    <table>
                        <tr>
                            <td><asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click"/></td>
                            <td><asp:Button ID="btnNo" runat="server" Text="No" OnClick="btnNo_Click"/></td>
                        </tr>

                    </table>

                    </td</tr>
 
            </table>

        </td>

    </tr>
    </table>

        </asp:Panel>

    </form>
</body>
</html>
