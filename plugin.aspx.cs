﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class plugin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string KeywordSearch(string Keyword, string Type,string AccountType,int GodownId)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string options = "";
        string Data = new AccountsBLL().KeywordSearch(Keyword.Trim(), Type,AccountType,GodownId,Branch,out options);
        var JsonData = new
        {
            Data = Data,
            Options = options
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

     
    }


    [WebMethod]
    public static string KeywordSearchSaleOnly(string Keyword, string Type, string AccountType, int GodownId)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string options = "";
        string Data = new AccountsBLL().KeywordSearchSaleOnly(Keyword.Trim(), Type, AccountType, GodownId,Branch, out options);
        var JsonData = new
        {
            Data = Data,
            Options = options
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);


    }


    [WebMethod]
    public static string KeywordSearchRaw(string Keyword, string Type, string AccountType, int GodownId)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string options = "";
        string Data = new AccountsBLL().KeywordSearchRaw(Keyword.Trim(), Type, AccountType, GodownId,Branch, out options);
        var JsonData = new
        {
            Data = Data,
            Options = options
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);


    }

    [WebMethod]
    public static string KeywordSearchForDN(string Keyword, string Type, string AccountType, int GodownId, string ItemType,string Excise)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string options = "";
        string Data = new AccountsBLL().KeywordSearchForDN(Keyword.Trim(), Type, AccountType, GodownId, ItemType,Branch,Excise, out options);
        var JsonData = new
        {
            Data = Data,
            Options = options
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string KeywordSearchForStock(string Keyword, string Type, string AccountType, int GroupId, string ItemType, int Godown)
    {
        Int32 BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string options = "";
        string Data = new AccountsBLL().KeywordSearchForStock(Keyword.Trim(), Type, AccountType, GroupId, ItemType, Godown,BranchId, out options);
        var JsonData = new
        {
            Data = Data,
            Options = options
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
}