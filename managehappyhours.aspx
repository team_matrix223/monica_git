﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managehappyhours.aspx.cs" Inherits="managehappyhours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="chosen.jquery.js"type="text/javascript"></script>
    <link rel="stylesheet" href="docsupport/prism.css">
    <link rel="stylesheet" href="chosen.css">

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.timepicker.js" type="text/javascript"></script>
    <script src="js/jquery.timepicker.min.js" type="text/javascript"></script>
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
     <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="js/jquery.uilock.js"></script>
  <script language="javascript">

      function ResetControls() {
        
          var btnAdd = $("#btnAdd");
         
          $("#<%=ddlCategories.ClientID %> > option").attr("selected", false);
          $("#<%=ddlCategories.ClientID %>").trigger("chosen:updated");
         
          btnAdd.css({ "display": "block" });
          

          

          $("#txtStartTime").val("");
          $('#chkIsActive').prop('checked', false);
          $("#txtEndTime").val("");
          
      }


      function fillHappyHours() {

          $.ajax({
              type: "POST",
              data: '{ }',
              url: "managehappyhours.aspx/FillHappyHours",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {
                  var obj = jQuery.parseJSON(msg.d);


                  $("#txtStartTime").val(obj.HappyHourData.TimeFrom);
                  $("#txtEndTime").val(obj.HappyHourData.TimeTo);
                  var strRoles = obj.HappyHourData.CategoryId;
                  var parts = strRoles.split(',');

                  $("#<%=ddlCategories.ClientID %> > option").attr("selected", false);

                  for (var i = 0; i < parts.length; i++) {

                      $('#<%=ddlCategories.ClientID %> option[value=' + parts[i] + ']').prop('selected', 'selected');
                  }
                  $("#<%=ddlCategories.ClientID %>").trigger("chosen:updated");



                  if (obj.HappyHourData.IsActive == true) {
                      
                      $('#chkIsActive').prop('checked', true);
                  }
                  else {
                      $('#chkIsActive').prop('checked', false);

                  }


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {


              }

          });

      }




      function InsertUpdate() {


          var btnAdd = $("#btnAdd");
          

          $.uiLock('');


          var arrCategories = $('#<%=ddlCategories.ClientID %>').val();
        
          if (arrCategories == null) {
              alert("Choose Categories For Happy Hours");
              $('#<%=ddlCategories.ClientID %>').focus();
              $.uiUnlock();
              return;

          }

          var StartTime = $("#txtStartTime").val();
          var EndTime = $("#txtEndTime").val();
          if (StartTime == "") {
              alert("Select Start Time For happy hours");
              $("#txtStartTime").focus();
              $.uiUnlock();
              return;

          }

          if (EndTime == "") {
              alert("Select End Time For happy hours");
              $("#txtEndTime").focus();
              $.uiUnlock();
              return;

          }

          var IsActive = false;
          
          if ($('#chkIsActive').is(":checked")) {
              IsActive = true;
          }



          $.ajax({
              type: "POST",
              data: '{"StartTime": "' + StartTime + '","EndTime": "' + EndTime + '","isActive": "' + IsActive + '","arrCategories":"' + arrCategories + '"}',
              url: "managehappyhours.aspx/Insert",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);



                  btnAdd.bind('click', function () { InsertUpdate(); });

                  ResetControls();

                  alert("Happy Hours added successfully.");
                  fillHappyHours();


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {
                  $.uiUnlock();

              }



          });

      }


      $(document).ready(
       function () {

           
           $('#txtStartTime').timepicker();

           $('#txtEndTime').timepicker();

           fillHappyHours();
           $("#btnAdd").click(
     function () {

         InsertUpdate();


     }
     );

           


       });




  </script>



       
  <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Happy Hours</h3>
                        </div>
                       
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">


                          <form class="form-horizontal form-label-left"   runat="server" id="formID" method="post">
                               <asp:HiddenField ID="hdnRoles" runat="server"/>
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Add Happy Hour </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                                       <table>
                                       <tr>
                                       <td class="headings" align="left" style="text-align:left;width:170px">
                                       Choose Category
                                       </td>
                                       <td colspan = "3" align="left" style="text-align:left">
                                       
                                            
                                           
                                            <asp:DropDownList ID="ddlCategories"  runat="server" data-placeholder="" multiple class="chosen-select validate ddlrequired" style="width:421px;">
                                             
                                             </asp:DropDownList>
                                       
                                       </td>
                                       
                                       
                                       </tr>
                                        <tr>
                                        <td class="headings" align="left" style="text-align:left;width:170px">Start Time:</td>
                                        <td align="left" style="text-align:left"><input type="text"  name="txtStartTime" class="form-control validate required"  data-index="1" id="txtStartTime" PlaceHolder="Click Here to pick Time" style="width: 170px"/></td>
                                        <td class="headings" align="left" style="text-align:left">End Time</td>
                                        <td align="left" style="text-align:left"><input type="text"  name="txtEndTime" class="form-control validate required " PlaceHolder="Click Here to pick Time"  data-index="1" id="txtEndTime" style="width:170px"/></td>
                                        </tr>

                                        <tr>
                                        
                                        <td class="headings" align="left" style="text-align:left;width:170px">Full Day Discount</td>
                                        <td align="left" style="text-align:left" colspan="3"><input type="checkbox" id="chkIsActive" data-index="2"  name="chkIsActive" /></td>
                                        </tr>
                                        
                                         <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td style="padding-top:10px"> <div id="btnAdd" class="btn btn-primary btn-small" ><i class="fa fa-external-link">Add</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>


                                       </table>
                                        
                                       

                                        

                                    </form>
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
 
                 
                </div>
                <!-- /page content -->

               

            </div>
            
            <script type="text/javascript">
                var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': { allow_single_deselect: true },
                    '.chosen-select-no-single': { disable_search_threshold: 10 },
                    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    '.chosen-select-width': { width: "95%" }
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }

  </script>

</asp:Content>

