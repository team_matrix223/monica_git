﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class menucard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static Group[] FetchGroups()
    {
        var data = new GroupBLL().GetAll();
        return data.ToArray();
    }
    [WebMethod]
    public static SubGroup[] FetchSubGroups(int GroupId)
    {
        var data = new SubGroupBLL().GetByGroup(GroupId);
        return data.ToArray();
    }

    [WebMethod]
    public static Product[] FetchProducts(int SubGroupId,string Keyword)
    {
        var data = new ProductBLL().AdvancedSearchForMenuCard(SubGroupId, Keyword);
        return data.ToArray();
    }



    [WebMethod]
    public static Product[] GetFavourites()
    {
        var data = new OrdersBLL().GetFavourites();
        return data.ToArray();
    }

    [WebMethod]
    public static string PlaceOrder(string GrossAmount, string SerTaxPer, string SerTaxAmt, string Vat, string NetAmount, Product[] data)
     {

        Orders objOrders = new Orders()
        {
            Customer_ID = "",
            Customer_Name = "",
            Order_Value = Convert.ToDecimal(GrossAmount),
         
            Less_Dis_Amount = 0,
            Add_Tax_Amount = Convert.ToDecimal(Vat),
            Net_Amount = Convert.ToDecimal(NetAmount),
           
            CreditBank = "",
          
            Cash_Amount = 0,
            Credit_Amount = 0,
            CrCard_Amount = 0,
            Round_Amount = 0,
      
            Passing = false,
            CashCust_Code = 0,
            CashCust_Name = "",
            Tax_Per = 0,
            R_amount = 0,
            tokenno = 0,
            tableno = 0,
            remarks = string.Empty,
            servalue = Convert.ToDecimal(SerTaxAmt),
            ReceiviedGRNNo = 0,
            EmpCode = 0,

        };
        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("OrgSaleRate");
        dt.Columns.Add("SurVal");
        dt.Columns.Add("Remarks");

        DataRow dr;

        foreach (var item in data)
        {
            dr = dt.NewRow();
            dr["ItemCode"] = item.Item_Code;
            dr["Rate"] = Convert.ToDecimal(item.Sale_Rate);
            dr["Qty"] = Convert.ToDecimal(item.Qty);
            dr["Amount"] = Convert.ToDecimal(item.SubTotal);
            dr["Tax"] = Convert.ToDecimal(0);
            dr["Tax_Amount"] = Convert.ToDecimal(0);
            dr["OrgSaleRate"] = Convert.ToDecimal(0);
            dr["SurVal"] = Convert.ToDecimal(item.SurVal);
            dr["Remarks"] = Convert.ToString(item.Remarks);
            dt.Rows.Add(dr);
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new OrdersBLL().InsertUpdate(objOrders, dt);
        var JsonData = new
        {
            Order = objOrders,
            Status=status
            
        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string LikeItem(int ItemId)
    {

        
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new OrdersBLL().LikeProduct(ItemId);
        var JsonData = new
        {
            
            Status = status

        };
        return ser.Serialize(JsonData);

    }




}