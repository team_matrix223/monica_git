﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managesalesunit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.SALESUNIT));

      
        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }


    }

    [WebMethod]

    public static string Delete(Int32 SaleUnitId)
    {
        SaleUnits objSaleUnit = new SaleUnits()
        {
            Unit_Id = SaleUnitId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new SaleUnitBLL().DeleteSaleUnit(objSaleUnit);
        var JsonData = new
        {
            SaleUnit = objSaleUnit,
            status = Status
        };
        return ser.Serialize(JsonData);
    }
}