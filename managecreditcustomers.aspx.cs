﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managecreditcustomers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CREDITCUSTOMERS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string Delete(Int32 AccountId)
    {
        AccLedger objAccLedger = new AccLedger()
        {
            AccountId = AccountId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new AccountsBLL().DeleteAccount(objAccLedger);
        var JsonData = new
        {
            Customer = objAccLedger,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetByCustomerId(int AccountId)
    {
        AccLedger objCustomers = new AccLedger()
        {
            AccountId = AccountId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new AccountsBLL().GetById(objCustomers);
        var JsonData = new
        {
            Customer = objCustomers


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string InsertUpdateCustomer(AccLedger objAccLedger)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        objAccLedger.BranchId = Branch;
        objAccLedger.UserId = Id;
        int status = new AccountsBLL().InsertUpdateAccLedger(objAccLedger);
        var JsonData = new
        {
            customer = objAccLedger,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}