﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
public partial class PaymentReceipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindDropDown();
            
        }

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PAYMENTRECEIPT));


        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");
            
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    public void BindDropDown()
    {
        ListItem li = new ListItem();
        li.Text = "None";
        li.Value = "0";
        ddlBank.DataSource = new BankBLL().GetAll();
        ddlBank.DataTextField = "Bank_Name";
        ddlBank.DataValueField = "Bank_ID";
        ddlBank.DataBind();
        ddlBank.Items.Insert(0, li);
        ddlcreditbank.DataSource = new BankBLL().GetAll();
        ddlcreditbank.DataTextField = "Bank_Name";
        ddlcreditbank.DataValueField = "Bank_ID";
        ddlcreditbank.DataBind();
        ddlcreditbank.Items.Insert(0, li);
        ddlCust.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlCust.DataTextField = "CNAME";
        ddlCust.DataValueField = "CCODE";
        ddlCust.DataBind();
        ddlCust.Items.Insert(0, li);
    }



    [WebMethod]
    public static string InsertUpdate(int ReceiptNo,DateTime ReceiptDate,string CCode, string CName,decimal Amount,string ModeofPayment,
        string BankName,string CHqNo)
    {

        Int32 UserId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 BranchID = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Prop_ReceiptMaster objreceiptmaster = new Prop_ReceiptMaster()
        {
            Receipt_No = ReceiptNo,
            Receipt_Date = ReceiptDate,
            CCode = CCode,
            CName = CName,
            Amount = Amount,
            ModeOfPayment = ModeofPayment,
            Bank_Name = BankName,
            CHQNO = CHqNo,
            userno = UserId,
            BranchId = BranchID,
            



        };



        JavaScriptSerializer ser = new JavaScriptSerializer();
       
        int status = new Prop_ReceiptMasterBLL().InsertUpdate(objreceiptmaster);
        var JsonData = new
        {

            Status = status,
            BillMaster = objreceiptmaster
        };
        return ser.Serialize(JsonData);



    }

    [WebMethod]
    public static string GetById(Int32 ReceiptNowprefix)
    {

        Prop_ReceiptMaster objReceiptMaster = new Prop_ReceiptMaster()
        {
            Receipt_No = ReceiptNowprefix
        };

        new Prop_ReceiptMasterBLL().GetByIdForReceiptMaster(objReceiptMaster);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            BillMasterData = objReceiptMaster,


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetOutstandingForReceiptMaster(string CustomerId)
    {
        Int32 BranchID = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string Qty = new Prop_ReceiptMasterBLL().GetOutstandingForReceiptMaster(CustomerId, BranchID);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ClaimQuantity = Qty,
        };
        return ser.Serialize(JsonData);
    }
}