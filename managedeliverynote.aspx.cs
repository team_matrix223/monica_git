﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class managedeliverynote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }

        hdntodaydate.Value = DateTime.Now.ToShortDateString();
    }




    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string BindDealers()
    {
        List<AccLedger> Accledger = new List<AccLedger>();
        List<Godowns> Godown = new List<Godowns>();
        List<Branches> Branch = new List<Branches>();
        Accledger = new DeliveryNoteBLL().GetAllDealer();
        Godown = new GodownsBLL().GetAll();
        Branch = new BranchBLL().GetAll();

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            DealerOptions = Accledger,
            GodownOptions = Godown,
            BranchOptions = Branch,

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertUpdate(DeliveryDetail[] objDeliveryDetails, DeliveryMaster objDeliveryMaster,bool Excise)
    {
        objDeliveryMaster.BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        objDeliveryMaster.UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Scheme");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1P");
        dt.Columns.Add("Dis2P");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("Dis3P");
        dt.Columns.Add("Godown_ID");
        dt.Columns.Add("RowNum");
        dt.Columns.Add("Excise_Duty");
        dt.Columns.Add("Excise_Amt");
        DataRow dr;


        Int32 RowNumm = 1;
        foreach (var item in objDeliveryDetails)
        {
            dr = dt.NewRow();
            dr["Item_Code"] = item.Item_Code;
            dr["Scheme"] = item.Scheme;
            dr["Qty"] = item.Qty;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = item.Amount;
            dr["Dis1P"] = item.Dis1P;
            dr["Dis2P"] = item.Dis2P;
            dr["TaxP"] = item.TaxP;
            dr["Dis3P"] = item.Dis3P;
            dr["Godown_ID"] = item.Godown_ID;
            dr["RowNum"] = RowNumm;
            dr["Excise_Duty"] = item.Excise_duty;
            dr["Excise_Amt"] = item.Excise_Amt;
            dt.Rows.Add(dr);
            RowNumm = RowNumm + 1;


        }

        int Status = new DeliveryNoteBLL().Insert(objDeliveryMaster, dt, Excise);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }





    [WebMethod]
    public static string GetDefaultGodown()
    {
        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int GodownId = new CommonMasterDAL().GetDefaultGodown(BranchId);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            DefaultGodown=GodownId,
         
        };
        return ser.Serialize(JsonData);


    }


    [WebMethod]
    public static string Delete(Int32 BillNo, string Prefix)
    {


        int Status = 0;
        Int32 BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        DeliveryMaster objDelivery = new DeliveryMaster()
        {
            Bill_No = BillNo,
            Prefix = Prefix,
            BranchId = BranchId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new DeliveryNoteBLL().DeleteDeliveryNote(objDelivery);
        var JsonData = new
        {
            Deliverymaster = objDelivery,
            status = Status
        };
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string GetById(string BillNo)
    {
        string[] Bill = BillNo.Split('-');
        JavaScriptSerializer ser = new JavaScriptSerializer();
        DeliveryMaster objDelivery = new DeliveryMaster();
        objDelivery.Bill_No = Convert.ToInt32(Bill[1].ToString());
        objDelivery.Prefix = Bill[0].ToString();
        List<DeliveryDetail> DeliveryDetail = new DeliveryNoteBLL().GetDeliveryById(objDelivery);
        var JsonData = new
        {
            Delivery = objDelivery,
            DeliveryDetail = DeliveryDetail
        };
        return ser.Serialize(JsonData);


    }

    [WebMethod]
    public static string FillSettings()
    {
        string DRate;
        string DSRate;
        string DMRP;


        new CommonSettingsBLL().GetDeliveryNoteGridSettings(out DRate, out DSRate, out DMRP);
        var JsonData = new
        {

            DRate = DRate,
            DSRate = DSRate,
            DMRP = DMRP,
           

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



}