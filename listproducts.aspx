﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listproducts.aspx.cs" Inherits="listproducts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/css.css" rel="stylesheet" />
 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Super Store Billing</title>
  <link rel="stylesheet" href="css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
   
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/bootstrap-glyphicons.css" />
 
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="js/jquery.uilock.js"></script>
 


<%--<script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>--%>
<style type="text/css">
    
    .form-control
    {
        margin:3px;
        border:solid 1px silver;
        padding:3px;
        }
    
#tbProductInfo tr
{
    border-bottom:dotted 1px silver;
    
    }

#tbProductInfo tr td
{
padding:3px;
    
    }
 
 
 
 #tboption tr
{
    border-bottom:solid 1px black;
    
    }

#tboption tr td
{
padding:10px;
    
    }   
    
</style>


 <script language="javscript" type="text/javascript">

  var Sertax = 0;
  var Takeaway = 0;
  var Takeawaydefault = 0;
  var mode = "";
  var count = 0;
      var modeRet = "";

  //.....................................

  function DEVBalanceCalculation()
{


var txtCashReceived=$("#txtCashReceived");
var txtCreditCard=$("#Text13");
var txtCheque=$("#Text15");
var txtFinalBillAmount=$("#txtFinalBillAmount");


if(Number(txtCashReceived.val())>=Number(txtFinalBillAmount.val()))
{
txtCreditCard.val(0);
txtCheque.val(0);
}
else if( (Number(txtCashReceived.val())+ Number(txtCreditCard.val()))>=Number(txtFinalBillAmount.val()))
{
txtCreditCard.val(Number(txtFinalBillAmount.val())-Number(txtCashReceived.val()));
txtCheque.val(0);

}
else if( (Number(txtCashReceived.val())+ Number(txtCreditCard.val())+ Number(txtCheque.val()))>=Number(txtFinalBillAmount.val()))
{
txtCheque.val(Number(txtFinalBillAmount.val())-(Number(txtCashReceived.val())+Number(txtCreditCard.val())));

}



var balReturn=Number((Number(txtCashReceived.val())+Number(txtCreditCard.val())+Number(txtCheque.val())- Number(txtFinalBillAmount.val()))  );
 

$("#txtBalanceReturn").val(balReturn.toFixed(2));
 

}





  //...................................

   var DiscountAmt = 0;
      
         var Total = 0;
         var DisPer = 0;
         var VatAmt = 0;
         var TaxAmt = 0;


          function bindGrid2() {
    
    var searchon=$("input[name='searchon1']:checked").val();
    var criteria=$("input[name='searchcriteria1']:checked").val();
    var stext=$("#Txtsrchcredit").val();
  
      
                   jQuery("#jQGridDemoCredit").GridUnload();
  
             jQuery("#jQGridDemoCredit").jqGrid({
            url: 'handlers/CreditCustomerSearch.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['Code','Name', 'CSTNO', 'TINNO' ],
            colModel: [
                        { name: 'CCODE',key:true, index: 'CCODE', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'CNAME', index: 'CNAME', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'CST_NO', index: 'CST_NO', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'TINNO', index: 'TINNO', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 
                         
   		     
   		     
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerCredit',
            sortname: 'CCODE',
            viewrecords: true,
            height: "100%",
            width:"800px",
            sortorder: 'desc',
            caption: "Customers List" 
             
             
        });


        $('#jQGridDemoCredit').jqGrid('navGrid', '#jQGridDemoPagerCredit',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );

       var Datad = jQuery('#jQGridDemoCredit'); 
     Datad.jqGrid('setGridWidth', '240');

        $("#jQGridDemoCredit").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {

         
        $("#CustomerSearchWindow").hide();
        $("#dvProductList").hide();
        $("#dvBillWindow").show();
         $("#dvCreditCustomerSearch").hide();
            
             var customerId=  $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CCODE') ;
                   $("#hdnCreditCustomerId").val(customerId);
                   $("#lblCreditCustomerName").text($('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME'));
                  //$('#dvCreditCustomerSearch').dialog('close');
                     $("#creditCustomer").css("display","block");
                      $("#ddlbilltype option[value='Credit']").prop("selected",true);



             }
         });

      }
      



  function bindGrid(searchon,mobile) {
    
    //var searchon=$("input[name='searchon']:checked").val();
    var criteria=$("input[name='searchcriteria']:checked").val();
    var stext=$("#txtSearch1").val();
    if(searchon=="M")
    {
    stext=mobile;
    
    }
        
     
                   jQuery("#jQGridDemo").GridUnload();
  
             jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/CustomersList.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ID','Name', 'Address1', 'Address2','Area','City','State','DateOfBirth','AnniversaryDate','Discount','ContactNo','Tag','FocBill','Group'],
            colModel: [
                        { name: 'Customer_ID',key:true, index: 'Customer_ID', width: 100, stype: 'text',sorttype:'int',hidden:true },
   		                { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
   		                 { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                
   		                 { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Date_Of_Birth', index: 'Date_Of_Birth', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Date_Anniversary', index: 'Date_Anniversary', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'Discount', index: 'Discount', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
   		                
   		                { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'grpid', index: 'grpid', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'Code',
            viewrecords: true,
            height: "100%",
            width:"800px",
            sortorder: 'desc',
            caption: "Customers List" 
             
             
        });


        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );

       var Datad = jQuery('#jQGridDemo'); 
     Datad.jqGrid('setGridWidth', '290');

     $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
         
     
                var Discount=$('#jQGridDemo').jqGrid('getCell', rowid, 'Discount');
                 
                  // $('#dvSearch').dialog('close');
     
                  
                  $("div[id='dvdisper']").html(Discount);
                   DiscountAmt = (Number(Total)* Number(Discount))/100;
                   $("div[id='dvdiscount']").html( DiscountAmt.toFixed(2));
                 $("div[id='dvnetAmount']").html(Math.round( ( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                   $("#hdnnetamt").val(( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                       $("#CustomerSearchWindow").hide();
        $("#dvProductList").show();     
               $("#dvBillWindow").hide();
             }
         });

   


     }

        function RestControls() {
       
            
              ProductCollection = [];

              $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            

            
              $("#dvdisper").html("0");
              $("#dvdiscount").html("0");
              $("#dvnetAmount").html("0");
              $("#dvTax").html("0");
              $("#dvVat").html("0");
              $("#dvsertaxper").html("0");
              $("#dvsbtotal").html("0");
              m_ItemId =0;
              m_ItemCode = "";
              m_ItemName = "";
              m_Qty = 0;
              m_Price = 0;
              m_Vat = 0;

                $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
          
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
             $("#dvBillWindow").hide();

          }

   
   function InsertHoldBill(){

              var custcode = 0;
             var custName = "";
             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
              if ($.trim(SelectedRow) == "") {
                custcode = 0;
                custName = "CASH"
              }
              else
              {
              custcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_ID');
              custName= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_Name');
              }


            
               var BIllValue = $("#dvsbtotal").html();
           
             var DisPer = $("#dvdisper").html();
             var addtaxamt = $("#dvVat").html();
             var NetAmt = $("#dvnetAmount").html();
           
             var billmode = "0";
           
               var ItemCode = [];
                var Price = [];
                var Qty = [];
                 var PAmt = [];
                var Tax = [];

                var Ptax = [];


                   if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Price[i] = ProductCollection[i]["Price"];
                    Qty[i] = ProductCollection[i]["Qty"];  
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                   
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    Ptax[i] = ProductCollection[i]["Producttax"];

                }


                alert('"CustomerId": "' + custcode + '","CustomerName": "' + custName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '"');
                          
                 $.ajax({
                    type: "POST",
                    data: '{ "CustomerId": "' + custcode + '","CustomerName": "' + custName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '"}',
                    url: "screen.aspx/InsertHoldBill",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        
                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else
                        {
                           alert("Bill Holded Successfully");
                           RestControls();
                        }
                      

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        RestControls();
                        $.uiUnlock();
                    }

                });

   }








    function InsertUpdate() {
      if($("#ddlbillttype").val() == "Cash")
      {
             
              var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var BIllValue = $("#dvsbtotal").html();
           
             var DisPer = $("#dvdisper").html();
          
             var lessdisamt = $("#dvdiscount").html();
              
             var addtaxamt = $("#dvVat").html();

             var NetAmt = $("#txtFinalBillAmount").val();
              
             var billmode = $("#ddlbillttype").val();
             
             var CreditBank = $("#ddlBank").val();
              
             var CashAmt = $("#txtCashReceived").val();
             
             var creditAmt =0;
             if(billmode == "Credit")
             {
             creditAmt = Number(BIllValue)-Number(CashAmt);
            
             
             }
             
             
             var CreditCardAmt = $("#Text13").val();
              
     
              var RoundAmt = 0;
             var hdnNetamt = $("#hdnnetamt").val();
             RoundAmt = Number(NetAmt) - Number(hdnNetamt);

             var cashcustcode = 0;
             var cashcustName = "";
              if ($.trim(SelectedRow) == "") {
                cashcustcode = 0;
                cashcustName = "CASH"
              }
              else
              {
              cashcustcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_ID');
              cashcustName= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_Name');
              }
           
              var Tableno = 0;
             var setatx = $("#dvTax").html();
                
                
                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];


                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    OrgSaleRate[i] = ProductCollection[i]["Price"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];

                
                }
                }
              

              else   if($("#ddlbillttype").val() == "Credit")
      {
             
              var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
              var CustomerId = 0;
              var CustomerName = "";
               if ($.trim(SelectedRow) == "") {
                CustomerId = 0;
                CustomerName = "";
              }
              else
              {
              CustomerId= $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CCODE');
              CustomerName= $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CNAME');
              }
            
             var BIllValue = $("#dvsbtotal").html();
           
             var DisPer = $("#dvdisper").html();
          
             var lessdisamt = $("#dvdiscount").html();
              
             var addtaxamt = $("#dvVat").html();

             var NetAmt = $("#txtFinalBillAmount").val();
              
             var billmode = $("#ddlbillttype").val();
             
             var CreditBank = $("#ddlBank").val();
              
             var CashAmt = $("#txtCashReceived").val();
             
             var creditAmt =0;
             if(billmode == "Credit")
             {
             creditAmt = Number(BIllValue)-Number(CashAmt);
            
             
             }
             
             
             var CreditCardAmt = $("#Text13").val();
              
             var RoundAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";


              var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
              if ($.trim(SelectedRow2) == "") {
                cashcustcode = 0;
                cashcustName = "";
              }
              else
              {
              cashcustcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow2, 'Customer_ID');
              cashcustName= $('#jQGridDemo').jqGrid('getCell', SelectedRow2, 'Customer_Name');
              }
           
              var Tableno = 0;
             var setatx = $("#dvTax").html();
                
                
                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];


                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    OrgSaleRate[i] = ProductCollection[i]["Price"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];

                
                }
                }
            

             TaxDen=[];
             VatAmtDen =[];
             VatDen = [];
             SurDen =[];
            
             $("div[name='tax']").each(
            function (y) {
             TaxDen[y] = $(this).html();
            }
            );
             $("div[name='amt']").each(
            function (z) {
             VatAmtDen[z] = $(this).html();
            }
            );
             $("div[name='vat']").each(
            function (a) {
             VatDen[a] = $(this).html();
            }
            );
             $("div[name='sur']").each(
            function (a) {
             SurDen[a] = $(this).html();
            }
            );

          
                $.ajax({
                    type: "POST",
                    data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + Sertax + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","arrTaxden":"'+TaxDen+'","arrVatAmtden":"'+VatAmtDen+'","arrVatden":"'+VatDen+'","arrSurden":"'+SurDen+'"}',
                    url: "screen.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        
                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else
                        {
                           alert("Bill Saved Successfully");
                           RestControls();
                        }
                      

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        RestControls();
                        $.uiUnlock();
                    }

                });

            }


 var option = "";


     $(document).ready(
        function () {

        
      $("#btnhold").click(
        function () {
       
        InsertHoldBill();
        });



$("#btnreturn").click(
   function()
   {
   if(modeRet == "Return")
   {
     modeRet = "";
   }
   else
   {
      modeRet = "Return";
   }
      //count = count+1;
   }
   );




         $("div[id='dvdisper']").html("0");
           $("div[id='dvdiscount']").html( "0.00");
        
          $("#btnSearch").click(
        function () {

            var Keyword = $("#txtSearch");
            if (Keyword.val().trim() != "") {

                Search(0, Keyword.val());
            }
            else {
                Keyword.focus();
            }

        });


        
            $("#txtSearch").keyup(
            function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {


                    var Keyword = $("#txtSearch");
                    if (Keyword.val().trim() != "") {

                        Search(0, Keyword.val());
                    }
                    else {
                        Keyword.focus();
                    }


                }

            }

            );



  $("#btnBillWindowClose").click(
  function(){
 
        $("#CustomerSearchWindow").hide();
        $("#dvProductList").show();
        $("#dvBillWindow").hide();

        $("#dvCreditCustomerSearch").hide();

        
 // $("#dvBillWindow").dialog('close');
 
 
  }
 
  );

         $("#dvGetCreditCustomers,#dvGetCreditCustomersMember,#dvGetCreditCustomersPackage").click(
        function () {

        
            var DataGrid = jQuery('#jQGridDemo1');
            DataGrid.jqGrid('setGridWidth', '680');
         
            jQuery('#jQGridDemo1').GridUnload();


                    $("#CustomerSearchWindow").hide();
        $("#dvProductList").hide();
        $("#dvBillWindow").hide();
          $("#dvCreditCustomerSearch").show();




//            $('#dvCreditCustomerSearch').dialog(
//        {
//            autoOpen: false,

//            width: 720,
//            resizable: false,
//            modal: false

//        });

// 
//            linkObj = $(this);
//            var dialogDiv = $('#dvCreditCustomerSearch');
//            dialogDiv.dialog("option", "position", [233, 48]);
//            dialogDiv.dialog('open');
//            return false;


        });





        $("#ddlbillttype").change(
    function()
    {
    
 

    if($(this).val()=="Credit")
    {


    
            $("#lblCashHeading").text("Receipt Amt:");
            $("#ddlbillttype option[value='Credit']").prop("selected",true);
            $("#txtCashReceived").val("0").prop("readonly",false);
            $("#Text13").val("0").prop("readonly",true);
            $("#Text14").val("").prop("readonly",true);
            $("#Text15").val("0").prop("readonly",true);
            $("#Text16").val("").prop("readonly",true);
            $("#ddlType").prop("disabled",true);
            $("#ddlBank").prop("disabled",true);
           // alert($("#hdnCreditCustomerId").val()); 
            
            if($("#hdnCreditCustomerId").val()=="0")
            {

             
                jQuery('#jQGridDemo1').GridUnload();
                              $("#CustomerSearchWindow").hide();
        $("#dvProductList").hide();
        $("#dvBillWindow").hide();
          $("#dvCreditCustomerSearch").show();


//                    $('#dvCreditCustomerSearch').dialog(
//                    {
//                        //autoOpen: false,

//                        width: 720,
//                        resizable: false,
//                        modal: false

//                    });

// 
//                var dialogDiv = $('#dvCreditCustomerSearch');
//                dialogDiv.dialog("option", "position", [233, 48]);
//               dialogDiv.dialog('open');
//                return false;
           
            }
            else
            {
            $("#creditCustomer").css("display","block");
            }
        

           



    }
    else
    {
    $("#lblCashHeading").text("Cash Rec:");
     $("#creditCustomer").css("display","none");

            $("#ddlbillttype option[value='Cash']").prop("selected",true);
            
                    
            $("#txtCashReceived").val("0").prop("readonly",false);
            $("#Text13").val("0").prop("readonly",false);
            $("#Text14").val("").prop("readonly",false);
            $("#Text15").val("0").prop("readonly",false);
            $("#Text16").val("").prop("readonly",false);
            $("#ddlType").prop("disabled",false);
            $("#ddlBank").prop("disabled",false);


    }

    }
    );




         option = $("#<%=ddloption.ClientID%>").val();
       
       $("#<%=ddloption.ClientID%>").change(
        function(){
        option = $("#<%=ddloption.ClientID%>").val();
        Bindtr();
          
        }
       );

         


            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                  

                    $("#categories").html(obj.categoryData);
                      
                    Sertax = obj.setttingData.SerTax;
                    Takeaway = obj.setttingData.TakeAway;
                    Takeawaydefault = obj.setttingData.TakeAwayDefault;

                   
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );



        $("#btnBillWindowOk").click(
function(){



var billtype = $("#ddlbillttype").val();
if(billtype == "")
{
alert("Select BillType First");
$("#ddlbillttype").focus();
return;

}
  $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');



 
  DEVBalanceCalculation();



  var txtcreditcardcheck = $("#Text13").val();

     if(txtcreditcardcheck != "0")
     {
         if($("#ddlType").val() == "")
         {
         $.uiUnlock();
         alert("Please Select Credit Card Type");
         $("#ddlType").focus();
         return;
         }
         if($("#Text14").val() == "")
         {
         $.uiUnlock();
         alert("Please Enter Credit Card No");
         $("#Text14").focus();
         return;
         }

     }

     var  txtchequecheck = $("#Text15").val();
     if(txtchequecheck != "0")
     {
        if($("#ddlBank").val() == "")
        {
        $.uiUnlock();
         alert("Please Select Bank");
         $("#ddlBank").focus();
         return;
        }
     if($("#Text16").val() == "")
     {
     $.uiUnlock();
     alert("Please Enter Cheque No");
     $("#Text16").focus();
       return;
     }
     }


      var cashamount = $("#txtCashReceived").val();

      
      if(billtype=="Cash")
      {
         if(Number($("#txtBalanceReturn").val())<0)
        {
        $.uiUnlock();
        alert("Total amount is not equal to Bill Amount....Please first tally amount.");
        return;
        }
        else
        {
        cashamount=cashamount-Number($("#txtBalanceReturn").val());
        }

       }
  
        if(Number(cashamount)<0)
        {
        $.uiUnlock();
         alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
            return;
        }

        InsertUpdate();


}
);







        $("#txtCashReceived").keyup(
function()
{
DEVBalanceCalculation();

}
);



$("#Text13").keyup(
function()
{
DEVBalanceCalculation();

}
);
$("#Text15").keyup(
function()
{
DEVBalanceCalculation();

}
);

  function ResetBillCntrols(){

            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
          
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            modeRet = "";
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
  }



        
        $("#btnsavebill").click(
        function () {

              if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");
                 
                    return;
                }

            ResetBillCntrols();
            $("#hdnCreditCustomerId").val("0");
            $("#ddlbillttype option[value='Cash']").prop("selected",true); 
            $("#creditCustomer").css("display","none");

     
$("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly",true);
   
   
     $.ajax({
            type: "POST",
            data: '{}',
            url: "screen.aspx/BindBanks",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                $("#ddlBank").html(obj.BankOptions);
                 
            }


        });

        
        $("#CustomerSearchWindow").hide();
        $("#dvProductList").hide();
        $("#dvBillWindow").show();
          $("#dvCreditCustomerSearch").hide();


//             $('#dvBillWindow').dialog(
//          {
//            autoOpen: false,
//                closeOnEscape: false,
//                draggable:false,
//            width:570,
//            resizable: false,
//            modal: false,
//         
//            
//        });
//        
//             linkObj = $(this);
//            var dialogDiv = $('#dvBillWindow');
//            dialogDiv.dialog("option", "position", [180,200]);
//            dialogDiv.dialog('open');
//         
//            return false;

        });




         $("#btncreditsrch").click(
   function()
   {
    bindGrid2();
   }
   );




            $("#btnCustomer").click(
        function () {

        $("#CustomerSearchWindow").show();
        $("#dvProductList").hide();
        $("#dvBillWindow").hide();
         $("#dvCreditCustomerSearch").hide();

        

        bindGrid("M",$("#txtMobSearchBox").val());

//             $('#dvSearch').dialog(
//            {
//            autoOpen: false,

//            width:720,
//     
//          
//            resizable: false,
//            modal: true,
//                  
//            });
//            linkObj = $(this);
//            var dialogDiv = $('#dvSearch');
//            dialogDiv.dialog("option", "position", [238, 36]);
//            dialogDiv.dialog('open');
//            return false;
                
   

        });


            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {

                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");

                }
                Bindtr();

            });





            $("#btnSearch1").click(
        function () {
        
          bindGrid("N","");

        }
        );

        });

     var m_ItemId = 0;
     var m_ItemCode = "";
     var m_ItemName = "";
     var m_Qty = 0;
     var m_Price = 0;
     var m_TaxRate = 0;
     var m_Surval = 0;
     
      
     var ProductCollection = [];
     function clsproduct() {
         this.ItemId = 0;
         this.ItemCode= "";
         this.ItemName = "";
         this.Qty = 0;
         this.Price = 0;
         this.TaxCode = 0;
         this.SurVal = 0;
         this.ProductAmt = 0;
         this.Producttax = 0;
         this.ProductSurchrg = 0;
         this.TaxId = 0; 
     }

     function Bindtr() {
           DiscountAmt = 0;
           VatAmt = 0;
           Total = 0;
           var fPrice = 0;
                 $("div[name='vat']").html("0.00");
        $("div[name='amt']").html("0.00");
        $("div[name='sur']").html("0.00");

         $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
         for (var i = 0; i < ProductCollection.length; i++) {

         var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose'><img src='images/trash.png'/></i></td></tr>";
 
//  var tr = "<tr> <td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td>";
//             tr=tr+"<td style='text-align:center'><table ><tr style='border:0px'><td><div id='btnMinus' class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>-</div></td><td>" + ProductCollection[i]["Qty"] + " </td><td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>+</div></td></tr></table> </td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
//            
 
              $("#tbProductInfo").append(tr);
             fPrice = 0;
            fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
             var TAx = (Number(fPrice)* Number(ProductCollection[i]["TaxCode"])/100);
             var surchrg =(Number(TAx)* Number(ProductCollection[i]["SurVal"]))/100;
             var tottax = TAx+surchrg;
             VatAmt = VatAmt+ tottax;
             Total = Total + fPrice;
            ProductCollection[i]["ProductAmt"] = fPrice;
            ProductCollection[i]["Producttax"] = TAx;
            ProductCollection[i]["ProductSurchrg"] = surchrg;

             var amt  =  $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
                var vat  =  $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
                     var sur  =  $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
           $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt)+Number (fPrice.toFixed(2)));
            $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
            $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));

            CommonCalculation();

         }
        
         
     }


      function CommonCalculation() {

      $("div[id='dvsbtotal']").html(Total.toFixed(2)); 
         $("div[id='dvVat']").html(VatAmt.toFixed(2));
         TaxAmt =  GetServiceTax();

         $("div[id='dvnetAmount']").html(Math.round( ( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
      $("#hdnnetamt").val(( Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2)))  - Number(DiscountAmt.toFixed(2)))).toFixed(2));
     }

     function addToList(ProductId,Name, Price,TaxCode,SurVal,Code,Tax_Id) {

         m_ItemId = ProductId;
         m_ItemCode = Code;
         m_Price = Price;
         m_ItemName = Name;
        
          if(modeRet == "Return")
         {
           m_Qty = -1;
         }
         else
         {
          m_Qty = 1;
         }
         m_TaxRate = TaxCode; 
         m_Surval = SurVal;
          m_Tax_Id  = Tax_Id;

         var item = $.grep(ProductCollection, function (item) {
             return item.ItemId == m_ItemId;
         });

         if (item.length) {

             alert("Product Alredy Added in shop list");

             return;

         }


         TO = new clsproduct();

         TO.ItemId = m_ItemId;
         TO.ItemCode = m_ItemCode
         TO.ItemName = m_ItemName;
         TO.Qty = m_Qty;
         TO.Price = m_Price;
         TO.TaxCode = m_TaxRate;
         TO.SurVal = m_Surval;
         TO.TaxId = m_Tax_Id;

     
         ProductCollection.push(TO);
         Bindtr();


     }
    

     function GetServiceTax() {
      

       var TaxAmt = 0;

       $("#dvsertaxper").html(Sertax);
      
     
      if(option == "TakeAway")
     {
        if(Takeaway == "1")
        {
         TaxAmt = (Number(Total) * Number(Sertax))/100 ; 
         $("#dvTax").html(TaxAmt.toFixed(2));
       
       
        }     
        else
        {
          $("#dvsertaxper").html("0");
         TaxAmt == "0";
         $("#dvTax").html(TaxAmt.toFixed(2));
        }
      }
      else  if(option == "Dine")
      {
        TaxAmt = (Number(Total) * Number(Sertax))/100 ; 
        $("#dvTax").html(TaxAmt.toFixed(2));
       

      }

    
     return TaxAmt;
     
     }


        function BindProductListGrid(CatId,Keyword)
     {
        
         jQuery("#jQGridDemoProducts").GridUnload();
  
         jQuery("#jQGridDemoProducts").jqGrid({
          url: 'handlers/ListOfProducts.ashx?CategoryId='+CatId+'&Keyword='+Keyword+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ItemId','Code', 'Name', 'Price','TaxRate','SurCharge','Tax_ID' ],
            colModel: [
                        { name: 'ItemID',key:true, index: 'ItemID', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'Item_Name', index: 'Item_Name', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'Sale_Rate', index: 'Sale_Rate', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 
                        { name: 'Tax_Code', index: 'Tax_Code', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true }},
   		                { name: 'SurVal', index: 'SurVal', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true }},
   		                 
   		                { name: 'Tax_ID', index: 'Tax_ID', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true }},
   		     
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerProducts',
            sortname: 'CCODE',
            viewrecords: true,
            height: "100%",
            width:"1600px",
            sortorder: 'desc',
            caption: "Products List" 
             
             
        });


        $('#jQGridDemoProducts').jqGrid('navGrid', '#jQGridDemoPagerProducts',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );

       var Datad = jQuery('#jQGridDemoProducts'); 
     Datad.jqGrid('setGridWidth', '750');

        $("#jQGridDemoProducts").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {

             var ProductId = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'ItemID');
             var Name = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'Item_Name');
             var Price = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'Sale_Rate');
             var TaxCode = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'Tax_Code');
             var SurVal = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'SurVal');
             var Code = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'Item_Code');
             var Tax_Id = $('#jQGridDemoProducts').jqGrid('getCell', rowid, 'Tax_ID');
             addToList(ProductId,Name, Price,TaxCode,SurVal,Code,Tax_Id)
                

             }
         });
     
     }

     function Search(CatId, Keyword) {

     BindProductListGrid(CatId,Keyword);

     }

     //........................................

     $(document).on("click", "#btnPlus", function (event) {


         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];

         var Mode = "Plus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];
         var fQty = 0;
         if(Qty < 0)
         {
           fQty = Number(Qty) + (-1);
         }
         else
         {
           fQty = Number(Qty) + 1;
         }




         ProductCollection[RowIndex]["Qty"] = fQty;

         Bindtr();

     });


     $(document).on("click", "#btnMinus", function (event) {

         var RowIndex = Number($(this).closest('tr').index());
         var PId = ProductCollection[RowIndex]["ItemId"];
         var Mode = "Minus";

         var Qty = ProductCollection[RowIndex]["Qty"];
         var Price = ProductCollection[RowIndex]["Price"];

        var fQty = 0;
         if(Qty < 0)
         {
          fQty =Number(Qty) - (-1);
         }
         else
         {
           fQty =Number(Qty) - 1;
         }

         ProductCollection[RowIndex]["Qty"] = fQty;
         if (fQty == "0") {
             ProductCollection.splice(RowIndex, 1);
         }

         Bindtr();


     });



     //............................................




    
    </script>


    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
        <script>
            $(function () {
                $('#txtSearch,#txtSearch1').keyboard();



                $('#txtMobSearchBox').keyboard({
                    layout: 'custom',
                    customLayout: {
                        'default': [
    '1 2 3 4 5',
    '6 7 8 9 0',
    ' {bksp}',
    '{a} {c}'
   ]
                    },
                    maxLength: 10,
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    useCombos: false // don't want A+E to become a ligature
                }).addTyping();




                $('#txtCashReceived').keyboard({
                    layout: 'custom',
                    customLayout: {
                        'default': [
    '9 8 7 6 5',
    '4 3 2 1 0',
    ' . {bksp}',
    '{a} {c}'
   ]
                    },
                    maxLength: 6,
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    useCombos: false // don't want A+E to become a ligature
                }).addTyping();


            });
	</script>


</head>



<body>
<form id="Form1" runat = "server">
 <input type="hidden" id="hdnnetamt" value="0" />
 <input type="hidden" id="hdnCreditCustomerId" value="0" />
<div class="container">

<div class="row">
<div class="nav">


  
<ul id ="categories">

  
</ul>
</div>
</div>

<div class="row">

<div class="col-md-4" style="padding-left:0px">

 

<div id="dvProductList"  style="display:block">
<div class="leftside">

<table style="width:100%">

  <tr>

 
   
 

     <th style="width:200px;text-align:center; color: #FFFFFF;">Name</th>
 
    <th style="width:50px;text-align:center; color: #FFFFFF;">Qty</th>
 
    <th style="width:70px;text-align:center; color: #FFFFFF;">Price</th>
    
    <th style="width:30px;text-align:center; color: #FFFFFF;"> </th>
  </tr>
  </table>
</div>

<div class="cate">
 <table style="width:100%;font-size:10px" id="tbProductInfo">
 
   <tr style="border-bottom:0px">
    <td colspan="100%" style="text-align:center;font-weight:bold;font-size:12px">
        ITEM(S) WILL BE DISPLAYED HERE
    </td>
  </tr>
 </table>
  


 
 

</div>
</div>

<div id="CustomerSearchWindow" style="display:none">
<div class="leftside" style="padding-left:10px;font-weight:bold">

CUSTOMER SEARCH
 
</div>



<div class="cate">

<div id="dvSearch"  >
  <table >
  <tr><td>
    
 
   <table cellpadding="5" cellspacing="0" width="100%">
 
   <tr><td><input type="radio" id="rbStartingWith" value="S"  name="searchcriteria"/> <label for="rbStartingWith"  style="font-weight:normal"> Start  With</label></td><td><input type="radio" value="C" id="rbContaining" checked="checked"  name="searchcriteria"/> <label for="rbContaining" style="font-weight:normal" > Containing</label></td><td><input type="radio" id="rbEndingWith" value="E" name="searchcriteria"/> <label for="rbEndingWith"  style="font-weight:normal"> End With</label></td><td><input type="radio" id="rbExact" name="searchcriteria" value="EX"/> <label for="rbExact"  style="font-weight:normal"> Exact</label></td></tr>
   </table>
   
  

  </td></tr>
  <tr><td>
  <table>
  <tr><td><input type="text" class="form-control input-small" placeholder="Enter Customer Name" style="width:240px;padding:5px;margin-right:5px;margin-bottom:5px;margin-top:5px"   id="txtSearch1"/></td><td><div id="btnSearch1"  class="btn btn-primary btn-small"    >Search</div></td></tr>
  </table>
  
  </td></tr>
   <tr><td>
   
   			           <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
   
   </td></tr>

  </table>
  
  </div>

</div>
</div>



<div id="dvBillWindow" style="display:none">
<div class="leftside" style="padding-left:10px;font-weight:bold">

Billing Window
 
</div>

 
 <div class="cate">
 
 
<table style="border-collapse:separate;border-spacing:1"  >
<tr><td>Amount:</td><td><input type="text"    class="form-control input-small" style="width:70px"  id="txtFinalBillAmount" /></td><td>Bill Type:</td><td>
<select id="ddlbillttype" style="height:25px;width:70px;padding-left:0px"  class="form-control">
 
<option value="Cash">Cash</option>
<option value="Credit">Credit</option>
 
</select></td></tr>
<tr >
<td colspan="100%">
<table width="100%" id="creditCustomer" style="display:none;border:dashed 1px silver;border-collapse:separate;border-spacing:1" cellpadding="2">
<tr> <td   ><label  id="lblCreditCustomerName" style="font-weight:normal;margin-top:1px"   ></label></td>
<td style="padding-left:10px"><div style='cursor:pointer' id="dvGetCreditCustomers">
<img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png"/></div></td></tr>
</table>
</td>
</tr>


<tr><td><label id="lblCashHeading" style="font-weight:normal" >Cash Rec:</label></td><td colspan="100%"><input type="text"    class="form-control input-small" style="width:70px" value="0"  id="txtCashReceived" /></td></tr>
<tr><td>Credit Card</td><td><input type="text"    class="form-control input-small" style="width:70px" value="0"  id="Text13" /></td><td>Type:</td><td>
<select id="ddlType" style="height:25px;width:90" class="form-control">
<option></option>
<option value="Visa">VISA</option>
<option value="Maestro">MAESTRO</option>
<option value="Master">MASTER</option>
 
</select></td></tr>
<tr>
 
 
<td>Cc No:</td><td colspan="100%">
<input type="text"    class="form-control input-small" style="width:200px"  id="Text14" />
</td>

 

</tr>


<tr><td>Cheque:</td><td><input type="text"    class="form-control input-small" style="width:70px"  id="Text15" value="0" /></td><td>Bank:</td><td><select id="ddlBank" style="height:25px;width:90px"  class="form-control"></select></td></tr>
<tr><td>Cheque No:</td><td colspan="100%"><input type="text"    class="form-control input-small" style="width:200px"  id="Text16" /></td></tr>
<tr>
<td colspan="100%">
<table style="border-collapse:separate;border-spacing:1">
<tr><td style="width:64px">Balance:</td><td style="width:90px"><input type="text"    class="form-control input-small" style="width:90px"  id="txtBalanceReturn" readonly ="readonly" /></td><td colspan="100%">

<table cellpadding="2" cellspacing="0"><tr><td><div id="btnBillWindowOk"  class="btn btn-primary btn-small"  style="margin:2px"  >Generate Bill</div></td><td><div id="btnBillWindowClose"  class="btn btn-primary btn-small"    >Close</div></td></tr></table></td></tr>



</table>
</td>
</tr>
</table>
</div>

</div>



<div id="dvCreditCustomerSearch" style="display:none">
<div class="leftside" style="padding-left:10px;font-weight:bold">
Credit Customers Search
 
</div>

 <div class="cate">
  <table width="100%">
  <tr><td>
   <div id="dvLeft1" style="border:1px solid silver">
 
  
   <table  cellpadding="5" cellspacing="0"  width="100%" style="background:silver;">
     
   <tr><td style="padding:5px"><input type="radio" id="rbPhoneNo1" value="M" checked="checked"  name="searchon1"/> <label for="rbPhoneNo1" style="font-weight:normal" > Phone No.</label></td><td><input type="radio" id="rbCustomerName1" value="N" name="searchon1"/> <label for="rbCustomerName1"  style="font-weight:normal"> Customer Name</label></td></tr>
   </table>
     
   </div>
   <div id="dvRight1" style="float:left;width:100%;border:1px solid silver;display:none">
   <table cellpadding="5" cellspacing="0" width="100%">
   <tr style="background-color:#E6E6E6"><td colspan="100%" >Search Criteria:</td></tr>
   <tr><td><input type="radio" id="rbStartingWith1" value="S"  name="searchcriteria1"/> <label for="rbStartingWith1"  style="font-weight:normal"> Starting With</label></td><td><input type="radio" value="C" id="rbContaining1" checked="checked"  name="searchcriteria1"/> <label for="rbContaining1" style="font-weight:normal" > Containing</label></td><td><input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1"/> <label for="rbEndingWith1"  style="font-weight:normal"> Ending With</label></td><td><input type="radio" id="rbExact1" name="searchcriteria1" value="EX"/> <label for="rbExact1"  style="font-weight:normal"> Exact</label></td></tr>
   </table>
   
   </div>

  </td></tr>
  <tr><td>
  <table>
  <tr><td><input type="text" class="form-control input-small" style="width:240px" placeholder="Enter Search Keyword"  id="Txtsrchcredit"/></td>
  <td><div id="btncreditsrch"  class="btn btn-primary btn-small"    >Search</div></td>
  
  
  
  </tr>
  </table>
  
  </td></tr>
   <tr><td>
   
   			           <table id="jQGridDemoCredit">
    </table>
    <div id="jQGridDemoPagerCredit">
    </div>
   
   </td></tr>

  </table>
  </div>
  </div>






<div class="cate2">

<table id = "tbamountinfo"  style="width:100%;text-align:right;background:#F5FFFA">
                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr><td style="width:60%;font-weight:bold;text-align:right" text>Amount:</td><td style="width:100px;text-align:right;"><div id ="dvsbtotal"></div></td></tr>
                     <tr><td style="width:60%;font-weight:bold;text-align:right">Discount:</td><td style="width:30px;text-align:right;"><div id ="dvdisper"></div></td><td>%</td><td style="width:100px;text-align:right;"><div id ="dvdiscount"></div></td></tr>
                     <tr><td style="width:60%;font-weight:bold;text-align:right">ServiceTax:</td><td style="width:30px;text-align:right;"><div id ="dvsertaxper"></div></td><td>%</td><td style="width:100px;text-align:right"><div id ="dvTax"></div></td></tr>
                       
                         <tr><td  style="width:60%;font-weight:bold;text-align:right">Vat:</td><td style="width:100px;text-align:right;"><div id ="dvVat"></div></td></tr>
                     <tr><td  style="width:60%;font-weight:bold;text-align:right">Net Amount:</td><td style="width:100px;text-align:right"><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                
                        
                     </tr>

                     </table>


<table>
<tr>
<td colspan="100%">
<table>
        <tbody><tr><td><input type="text" placeholder="Mobile Number" style="border:solid 1px silver;padding:5px" id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all" aria-haspopup="true" role="textbox"></td><td>

 <div id="btnCustomer">
 <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
  </div>

</td>

<td style="padding-left:10px">
  <asp:DropDownList ID="ddloption" runat="server" style="width:100px;height:30px;border:solid 1px silver"  >
                <asp:ListItem Text="Take Away" Value ="TakeAway"></asp:ListItem>
                <asp:ListItem Text="Dine" Value ="Dine"></asp:ListItem>
               
                </asp:DropDownList>
</td>
 
</tr>
      </tbody></table>
</td>
</tr>
<tr>
<td><div class="button1" id="btnreturn" >Return</div> </td>
<td><div class="button1" id="btnsavebill" >Bill</div></td>
<td><div class="button1" id="btnhold" >Hold</div></td>
</tr>
<tr>
<td colspan="100%">
<table><tr> <td><div  id="dvTaxDenomination" style="float:left;display:none;  background-color: #DBEAE8;color:#63a69a;bottom:0px;right:0px;border:solid 1px silver;border-radius:10px;padding:5px;-webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
          box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
 <table>
 
 <tr><td colspan="2">
 <table><tr><td></td><td>Tax Denomination:</td>
 <td></td></tr></table></td></tr>
 <tr><td>Tax</td><td>VatAmt</td><td style="width:70px">Vat</td><td>SurChg</td></tr>
 <tr><td id="gridTax" colspan ="100%"><asp:Repeater ID="gvTax" runat ="server">
 <ItemTemplate>
 
 <tr><td ><div  name="tax"><%#Eval("Tax_Rate")%></div></td><td ><div myid='amt_<%#Eval("Tax_ID") %>' name="amt">0.00</div></td><td><div myid='vat_<%#Eval("Tax_ID") %>' name="vat">0.00</div></td><td ><div myid='sur_<%#Eval("Tax_ID") %>' name="sur">0.00</div></td></tr>
 </ItemTemplate>
 </asp:Repeater></td></tr>
 


 </table>
  
 </div></td></tr></table>
</td>
</tr>
</table>
</div>



</div>


<div class="col-md-8" style="padding-left:0px">
 
<%--<div id ="products" style="overflow-y: scroll; height: 408px;">

</div>
 --%>


    <table id="jQGridDemoProducts">
                </table>
                <div id="jQGridDemoPagerProducts">
                </div>

<div class="Search">
<div class="input-group">
  <input type="text" class="form-control" placeholder="Search Products By Keyword" aria-describedby="basic-addon2" id="txtSearch" style="padding:10px 10px; width:80%;border:0px;height:50px">
  <span class="input-group-addon" id="btnSearch" value="Search"> <img src="images/search-button.png" alt="" style="width:50px;padding-left:10px"/></span>
</div>

</div>


</div>

 

</div>


 





</div>
</form>
</body>
 
</html>
