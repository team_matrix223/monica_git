﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managedepartment.aspx.cs" Inherits="managedepartment" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">


    function ApplyRoles(Roles) {


        $("#<%=hdnRoles.ClientID%>").val(Roles);
    }
    function GetVMResponse(Id, Title,IsActive, Status) {


        var obj = { Prop_ID: Id, Prop_Name: Title, IsActive: IsActive };
        $("#dvVMDialog").dialog("close");
        if (Status == "I") {


            jQuery("#jQGridDemo").jqGrid('addRowData', Id, obj, "last");
            alert("Department Added Successfully");

        }
        else {

            var myGrid = $("#jQGridDemo");
            var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


            myGrid.jqGrid('setRowData', selRowId, obj);

            alert("Department Updated Successfully");
        }


        //BindGrid();
    }


    function OpenVMDialog(Id) {

        $.uiLock('');
        $.ajax({
            type: "POST",
            data: '{"Id":"' + Id + '","Type": "' + "Department" + '"}',
            url: "managearea.aspx/LoadUserControl",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvVMDialog").remove();
                $("body").append("<div id='dvVMDialog'/>");
                $("#dvVMDialog").html(msg.d).dialog({ modal: true });
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }


    var m_DepartmentId = -1;
    $(document).ready(
   function () {
       BindGrid();

          
       ValidateRoles();

       function ValidateRoles() {

           var arrRole = [];
           arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

           for (var i = 0; i < arrRole.length; i++) {
               if (arrRole[i] == "1") {

                   $("#btnAdd").show();

                   $("#btnAdd").click(
                   function () {

                       OpenVMDialog(-1);


                   }
                   );

               }
               else if(arrRole[i] == "3"){


                   $("#btnEdit").show();
                   $("#btnEdit").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Department is selected to Edit");
             return;
         }


         OpenVMDialog(m_DepartmentId);



     }
     );
               }

               else if(arrRole[i] == "2"){

                   $("#btnDelete").show();
                   $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Department is selected to Delete");
               return;
               }

           var departmentid = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Prop_ID')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');

               $.ajax({
                   type: "POST",
                   data: '{"DepartmentId":"' + departmentid + '"}',
                   url: "managedepartment.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       if (obj.status == -1) {
                           alert("Deletion Failed. Department is in Use.");
                           return
               }

                       BindGrid();
                       alert("Department is Deleted successfully.");




               },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
               },
                   complete: function () {
                       $.uiUnlock();
               }
               });
               }


               }
       );


               }

           }

       }


     

       



   });



  
     
</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

 

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Department</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     

 


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Department</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
      
      <table>
      
      <tr><td>
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
      </td></tr>

      <tr><td>
      
         <table cellspacing="0" cellpadding="0" style="margin-top:5px">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  style="display:none;"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add  </div></td>
                                              <td>&nbsp;</td><td> <div id="btnEdit"  style="display:none;"  class="btn btn-success btn-small" ><i class="fa fa-edit m-right-xs"></i> Edit  </div></td>
                                            <td>&nbsp;</td><td> <div id="btnDelete"  style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete  </div></td>
                                            </tr>
                                            </table>
      </td></tr>
      </table>
                    
      
     
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
                <!-- /footer content -->

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageDepartment.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Department Id', 'Title','IsActive','UserId'],
                        colModel: [
                                    { name: 'Prop_ID', key: true, index: 'Prop_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'Prop_Name', index: 'Prop_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                    { name: 'UserId', index: 'UserId', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Prop_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Department List",

                        editurl: 'handlers/ManageAreas.ashx',



                     ignoreCase: true,
                         toolbar: [true, "top"],

                    });



                     var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });





                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_DepartmentId = 0;
                    validateForm("detach");

                    
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                    $("#btnEdit").css({ "display": "none" });
                    $("#btnDelete").css({ "display": "none" });
                    $("#btnAdd").css({ "display": "none" });
                  

                    for (var i = 0; i < arrRole.length; i++) {

                        if (arrRole[i] == 1) {

                            $("#btnAdd").css({ "display": "block" });
                        }
                        if (arrRole[i] == 2) {

                            $("#btnDelete").css({ "display": "block" });
                        }
                        if (arrRole[i] == 3) {


                            m_DepartmentId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Prop_ID');
                            $("#btnEdit").css({ "display": "block" });
                        }
                    }


                  
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>



</asp:Content>



