﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVariables.ascx.cs" Inherits="Templates_ucVariables" %>
<script language="javascript" type="text/javascript">
    $(document).ready(
    function () {

        var VMID = $("#hdnVMID").val();
        var VMType = $("#hdnVMType").val();

        if (VMID >=0) {

             $("#btnVMInsert").html("<i class='fa fa-save'></i> Update");
            $.ajax({
                type: "POST",
                data: '{"Id":"' + VMID + '","Type":"' + VMType + '"}',
                url: "VariableService.asmx/GetById",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);
                   

                    $("#txtVMTitle").val(obj.Variable.Title);
                    if (obj.Variable.IsActive == true) {
                        $('#chkVMIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkVMIsActive').prop('checked', false);

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }



    }

    );

    function InsertUpdateVariableMaster() { 



    
        var VMID = $("#hdnVMID").val();
        var VMTitle = $("#txtVMTitle").val();
        if (VMTitle.trim() == "") {
            $("#txtVMTitle").focus();
            return;
        }
        var VMIsActive=false;
        if($("#chkVMIsActive").prop("checked"))
        {
        VMIsActive=true;
        }

      

        var VMType = $("#hdnVMType").val();
        var VMStatus = "U";
        if (VMID == -1) {
            VMStatus = "I";
        }

        $.ajax({
            type: "POST",
            data: '{"Id":"' + VMID + '","Title":"' + VMTitle + '","IsActive":"' + VMIsActive + '","Type":"' + VMType + '"}',
            url: "VariableService.asmx/InsertUpdate",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);
              

                if (obj.Variable.Id == -1) {
                    if (VMType == 'Color') {

                        VMType = 'Category';
                    }

                    alert(VMType + " " + "with duplicate name already exists");
                    return;
                }



                GetVMResponse(obj.Variable.Id, obj.Variable.Title, obj.Variable.IsActive, VMStatus, VMType)



            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });



    
    }

</script>

<table class="table-condensed">
<tr><td>Title</td><td><input type="text" id="txtVMTitle"    /></td></tr>
<tr><td>IsActive</td><td style="text-align:left"><input type="checkbox" id="chkVMIsActive" /></td></tr>

                                            <tr>
                                             <td></td>
                                            <td    style="text-align:left">
                                          
                                            
                                             <div id="btnVMInsert" onclick="javascript:InsertUpdateVariableMaster();"  class="btn btn-primary btn-small" ><i class="fa fa-save"></i> Add</div>
                                             
                                             
                                             </td>
                                       
                                     
                                            
                                            </tr>

</table>
<asp:Literal ID="ltContent" runat="server"></asp:Literal>