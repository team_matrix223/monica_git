﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderDispatch.ascx.cs" Inherits="Templates_OrderTemplates_OrderDispatch" %>
  <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" >
                             
                                <div class="x_content" style="background:seashell;margin-bottom:10px">
                                    <br />
                                   <table style="width:70%">
                                   <tr>
                                   <td>
                                   <label class="control-label" >Order No <span class="required">*</span>   
                                            </label>
                                   </td>
                                   <td>
                                    <input type="text" id="txtOrderNo" readonly = "readonly"  class="form-control"  >
                                               
                                   </td>
                                   <td>
                                   
                                   </td>
                                   <td>
                                   
                                   
                                   </td>
                                   <td>
                                   
                                      <label class="control-label"> Customer Name <span class="required">*</span>   
                                            </label>
                                   </td>
                                   <td>
                                     <input type="text"  id="txtCname" readonly = "readonly" class="form-control"  >
                                               
                                   </td>
                                   
                                   </tr>
                                   
                                   </table>
                                </div>

                             

                    <div class="row">
                  <div class="col-md-12 col-xs-12">
                            <div class="x_panel" style="background:seashell">
                                <div class="x_title">
                                    <h2>ORDER ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                      <div class="x_panel" style="max-height:300px;overflow-y:scroll;min-height:215px">
                               
                                <div class="x_content">

                                    <table class="table table-striped" id ="tbDispatchProducts">
                                         <thead>
<tr><th>Code</th><th>Name</th><th>Weight</th><th>Qty</th><th>Dispatched</th><th>Rate</th><th>DispQty</th><th>Amount</th><th>Tax</th></tr>
</thead>

                                    </table>


                                 
                                </div>
                            </div>


                                </div>
                              
                            </div>

                      
                        </div>
         
                           <div class="col-md-8 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>ORDER INFO</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                     <form class="form-horizontal form-label-left input_mask">
                                     <table id="tbCal">
     <tbody><tr>
    <td>Order Value</td><td colspan="2"><input type="text" id="txtOValue" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
    <td>Bill Value</td><td colspan="2"><input type="text" id="txtBillValue" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
    <td>Dispatched Amount</td><td colspan="2"><input type="text" id="txtDispatchedAmt" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
  
           </tr>
    <tr>

    
    <td>Advance</td>
     <td colspan="2">
      <input type="text" class="form-control input-small"  readonly=readonly style="width:70px;background-color:White" 
      id="txtAdv" />
     </td>
     <td>Discount</td><td><input type="text" id="txtDiscountPer" readonly="readonly" style="width:30px;background-color:White" class="form-control input-small"> </td><td><input type="text" id="txtDiscountAmt"  readonly=readonly  style="width:35px;background-color:White" class="form-control input-small"></td>
      <td >Last Balance</td><td ><input type="text"  readonly=readonly  id="txtLastBalance" style="width:70px;background-color:White" class="form-control input-small"></td>
     </tr>
    

  
     <tr>
     <td colspan = "3">
     
     </td>
    <td>Vat Amount</td><td colspan="2">
        <input type="text"  readonly=readonly id="txtTotalVat"  style="width:70px;background-color:White" class="form-control input-small"></td>
    
    <td>BillMode</td><td colspan="2"> <select id="ddlPrefix" style="width:70px;height:34px">
      <option value="Cash">Cash</option>
      <option value="Credit">Credit</option>
      <option value="CreditCard">Credit Card</option>
  
      </select></td>
    
    </tr>

<tr>

    <td colspan="100%">

        <div id="dvCreditCust" style="border:dashed 1px silver;margin:2px;display:none">

            <table>
                <tr>
                    <td>

<table width="100%" cellpadding="2" style="padding: 0px 5px; background: rgb(42, 63, 84) none repeat scroll 0% 0%; color: white; display: block; border: 1px dashed silver; border-collapse: separate;" id="creditCustomer">
                                        <tbody><tr>
                                            <td>
                                                <label style="font-weight: normal; margin-top: 1px" id="lblCreditCustomerName">
                                                     

                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </tbody></table>

                    </td>
                    <td>

                         <select id="ddlCreditCustomers">
                                             <option value="0"></option>
                                             </select>

                    </td>

                </tr>

            </table>
                       
        </div>

        <div id="dvCreditCard" style="border:dashed 1px silver;margin:2px;display:none">
            
            <table>
                

                <tr>
                   <td>
                                    Cc No:
                                </td>
                    <td colspan="100%">
                        <table>
                            
                                <td colspan="100%">
                                    <input type="text" id="Text14" style="width: 160px" class="form-control input-small">
                                </td>

                            <td>
                                    Type:
                                </td>
                                <td>
                                    <select class="form-control" style="height: 25px; width: 90px" id="ddlType">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>



                            <td>
                                    Bank:
                                </td>
                                <td>
                                     <select class="form-control" style="height: 25px; width: 90px" id="ddlBank"><option></option><option value="3">AXIS CHANDIGARH</option><option value="8">SBI</option><option value="11">PNB</option><option value="12">HDFC</option></select>

                                    <input type="text" value="0" id="Text15" style="width: 70px;display:none" class="form-control input-small">
                                </td>

                        </table>
                        

                    </td>

                                
                            </tr>


                

            </table>

        </div>


    </td>
</tr>


     <tr>
      <td colspan = "3">
     
     </td>

    <td>Net Amount</td><td colspan="2"><input type="text" readonly=readonly  id="txtTotalNetAmount"  style="width:70px;background-color:White" class="form-control input-small"></td>
    <td>CashReceived</td><td colspan="2"><input type="text"  id="txtCashRecvd" style="width:70px;background-color:White" class="form-control input-small"></td>
    
    </tr>
   
    </tbody></table>

                                    </form>
                                </div>
                            </div>

                      
                        </div>
                                   
                          <div class="col-md-4 col-sm-4 col-xs-12">
                          


                                <table width="100%">
                                     <tr><td>
                                     <table >
                                     <tr><td>
                                       <button id="btnBillSave"  Style="padding:15px;font-size:20px;width:130px"  class="btn btn-success">
                                       <i class="fa fa-save"></i>
                                       Bill&Save</button>
    
                                     </td>
                                     </tr>
                                     <tr>
                                     <td>
                                       <button id="btnDispatchCancel"  class="btn btn-danger"  Style="padding:15px;font-size:20px;width:130px"  >
                                       <i class="fa fa-mail-reply-all"></i>
                                       Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>

                        </div>

                               


                    </div>

                            </div>
                        </div>

 
   