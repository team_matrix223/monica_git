﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddCashCustomer.ascx.cs" Inherits="Templates_OrderTemplates_AddCashCustomer" %>

<script runat="server">


</script>

      <div id="dvSearch" style="background-color :#FFF7E6">
  <table width="100%">
  <tr><td>
   <div id="dvLeft" style="float:left;width:100%;border:1px solid silver">
 
  
   <table  cellpadding="5" cellspacing="0"  width="100%">
    <tr style="background-color:#FFE7B3"><td colspan="100%" >Add Customer:</td></tr>
   <tr><td>Prefix</td><td><select class="form-control" data-bind="options:Prefix, ptionsText: 'Text', optionsValue: 'Value', value: Prefix, optionsCaption: 'Choose..'" style="width:100px"></select></td>
   <td>Area</td><td><select data-bind="options:Areas, optionsText: 'Name', optionsValue: 'Code', value: Area, optionsCaption: 'Choose..'"> </select></td></tr>
   <tr><td>CustomerName</td><td> <input type="text"  data-bind="value:CustomerName"  class="form-control"  ></td>
   <td>City</td><td><select data-bind="options:Cities, optionsText: 'Name', optionsValue: 'Code', value: City, optionsCaption: 'Choose..'"> </select></td></tr>    
   <tr><td>Address1</td><td><textarea id="txtAddress1" data-bind="value:Address1" class="form-control" style="height:55px"></textarea></td>
   <td>State</td><td><select data-bind="options:States, optionsText: 'Name', optionsValue: 'Code', value: State, optionsCaption: 'Choose..'"> </select></td></tr> 
   <tr><td>Address2</td><td><textarea id="txtAddress2" data-bind="value:Address2" class="form-control" style="height:55px"></textarea></td>
    <td>Date Of Birth</td><td><input type="text"  class="form-control has-feedback-left" id="txtBirthDate"  data-bind="value:DateOfBirth" aria-describedby="inputSuccess2Status">
                                                 <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span></td></tr> 
  <tr><td>Anniversary Date</td><td><input type="text"  class="form-control has-feedback-left" id="txtAnniversary"  data-bind="value:DateOfAnniversary" aria-describedby="inputSuccess2Status">
                                                 <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span2" class="sr-only">(success)</span></td>
                                                    <td>ContactNo</td>  <td><input type="text"  data-bind="value:MobileNo"  class="form-control"  ></td>      </tr>
                                              
   </table>
     
   </div>
  
  </td></tr>
  <tr><td>
  <table>
  <tr><td><div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td></tr>
  </table>
  
  </td></tr>
 
  </table>


  
  </div>