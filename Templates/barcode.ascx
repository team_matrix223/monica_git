﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="barcode.ascx.cs" Inherits="Templates_barcode" %>

<script language="javascript">
    var m_ItemId = 0;
    $(document).ready(
      function () {


          function BindInformation(ItemId) {
            
              $.ajax({
                  type: "POST",
                  data: '{"ItemId":"' + ItemId + '"}',
                  url: "managebarcodes.aspx/BindIemDetail",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      var obj = jQuery.parseJSON(msg.d);
                      m_ItemId = obj.ItemOptions.ItemId;

                      $("#txtItemCOde").val(obj.ItemOptions.Item_Code);
                      $("#txtItemName").val(obj.ItemOptions.Item_Name);
                      $("#txtMRP").val(obj.ItemOptions.Max_Retail_Price);
                      $("#txtSaleRate").val(obj.Customer.Sale_Rate);



                      $("#ProductDialog").dialog({
                          autoOpen: true,
                          left: 367,
                          width: 300,
                          resizable: false,
                          modal: true
                      });

                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {
                      $.uiUnlock();
                  }
              });


          }




      });

   </script>



<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                    <br>
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="formbarcode" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                    <tr>
                                    <td style="width:80px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemCOde" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:80px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemName" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:80px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtMRP" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:80px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtSaleRate" disabled="disabled"> </td>
                                    
                                    </tr>

                                     <tr>
                                    <td style="width:80px"> <label>Qty 
                                            </label></td><td>  <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber"  id="txtQty">  </td>
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                <button class="btn btn-primary" type="button" onclick=""><i class="fa fa-external-link"></i> Print</button>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
 
                            
   
 