﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeliveryReceive.ascx.cs" Inherits="Templates_DeliveryReceive" %>
<script type ="text/javascript">


    $(document).ready(
    function () {
        BindDealer();


        function BindDealer() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/BindDealers",
                data: {},
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html = "<option value = 0>--SELECT--</option>";
                    var html1 = "<option value = 0>--SELECT--</option>";
                    for (var i = 0; i < obj.DealerOptions.length; i++) {

                        html = html + "<option value='" + obj.DealerOptions[i]["CCODE"] + "' Dis ='" + obj.DealerOptions[i]["DIS_PER"] + "'>" + obj.DealerOptions[i]["CNAME"] + "</option>";
                    }

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }
                    $("#ddlDealer").html(html);
                    $("#ddlGodown").html(html1);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {




                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });





                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:4px;
    }

.table > thead > tr > td
{
padding:4px;    
}
</style>
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:0px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Receive Delivery Note</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td>         <label class="control-label">Bill No</label></td>
                                    <td>      
                                    <input type="text" class="form-control" 
                                    
                                    id="txtBillNo" style="width:107px" />
                                    </td>

                                          <td> <label class="control-label">Godown</label></td>
                                    <td>      
                                    <select id="ddlGodown" style="width:100px">
                                             <option value="0"></option>
                                             </select>


                                    </td>


                                     <td> <div id="btnAddInfo" class="btn btn-primary">
                                            Add Information</div></td>




                                    </tr>
                                   
                                  
                                   
                                    </table>

                                </div>
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    
                          


                    <div class="row">
                  
                    

                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:200px;overflow-y:scroll;min-height:200px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th style="display:none">CaseQty</th><th>Qty</th><th style="display:none">Scheme</th><th>Rate</th><th>SaleRate</th><th>MRP</th><th>Amount</th><th>Dis3</th><th style="display:none">QTy_To_Less</th><th>Stock</th><th>Tax</th><th>Excise</th></tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding-top:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table><tr><td>         <label class="control-label">Remarks</label></td>
                                    <td>
                                 <textarea id="txtremarks" style="height: 57px;"></textarea> 
                                   
                                    </td>
                                    </tr></table>
                                    </td>

                                    <td colspan = "100%" style="float:right">
                                    <table><tr>


                                    <td> <label class="control-label">Bill Value</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtBillval" readonly="readonly" style="width:107px"  > 


                                    </td>
                                    </tr>
                                    <tr>

                                    <tr>


                                    <td> <label class="control-label">Excise</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtExciseAmt" readonly="readonly" style="width:107px"  > 


                                    </td>
                                    </tr>

                                    <td colspan ="100%">
                                    <table style="width:100%">
                                    <tr>
                                    <td> <label class="control-label">Discount</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtDisPer" readonly="readonly" style="width:40px;display:none"  > 


                                    </td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtDisAmt" readonly="readonly" style="width:107px;margin-left:57px"  > 

                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>

                                    <tr>

                                    <td> <label class="control-label">Adj(-)Less/(+)Add</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtAdj" readonly="readonly" style="width:107px"  > 


                                    </td>


                                   
                                    </tr>

                                       <tr>

                                    <td> <label class="control-label">Net Amount</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtnetAmt" readonly="readonly" style="width:107px" data-bind="value:GrNo" > 


                                    </td>


                                   
                                    </tr>

                                    </table>
                                    </td>





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                                <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" data-bind="click: $root.PlaceOrder" Style="padding:5px;font-size:25px;margin-left:10px"  class="btn btn-success">Transfer</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" data-bind="click: $root.CancelOrder" class="btn btn-danger"  Style="padding:5px;font-size:25px"  >Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>

                        </div>


                    </div>

                            </div>
                        </div>