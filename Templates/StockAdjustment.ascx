﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StockAdjustment.ascx.cs" Inherits="Templates_StockAdjustment" %>

<script type ="text/javascript">


    $(document).ready(
    function () {


        $("#rdbDept").change(function () {
           
            BindDealer();

        });

        $("#rdbGroup").change(function () {
          
            BindDealer();

        });
        $("#rdbcompany").change(function () {
           
            BindDealer();

        });

        BindDealer();


        function BindDealer() {


            var Type = "";
            if ($("#rdbGroup").is(':checked') == true) {
                Type = "G";
            }
            else if ($("#rdbDept").is(':checked') == true) {
                Type = "D";
            }
            else if ($("#rdbcompany").is(':checked') == true) {
                Type = "C";
            }
       

            $("#ddlGroup").html("<option ></option>");

            $.ajax({
                type: "POST",

                contentType: "application/json; charset=utf-8",
                url: "StockAdjustment.aspx/BindDealers",
                data: '{"Type":"' + Type + '"}',
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html1 = "<option value = 0>--SELECT--</option>";

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }

                    $("#ddlGodown").html(html1);
                    $("#ddlGroup").append(obj.GroupStock);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });




                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Stock Adjustment</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                    <div class="form-group">
                                        
                                        <label class="col-sm-1 control-label">RefNo:</label>
                                         <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" style="width:135px;height:30px" class="form-control has-feedback-left" id="txtRefNo" >
                                                           
                                                        </div>

                                              <label class="col-sm-1 control-label">Date:</label>
                                             <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" style="width:180px;height:40px" class="form-control has-feedback-left" id="txtBreakageDate" placeholder="MM/DD/YYYY" aria-describedby="inputSuccess2Status">
                                                            <span class="fa fa-calendar  form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="Span1" class="sr-only">(success)</span>
                                                        </div>
                                                
                                       
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="showColumn" class="btn-group" data-toggle="buttons">
                                                
                                                 <input type="radio" id= "rdbFinished" name="Finished" value="Finished" checked="checked"  /> &nbsp;<label for="rdbFinished" style="font-weight:normal">Finished</label>   &nbsp;
                                             <input type="radio"  id="rdbSemiFinished" name="Finished" value="Semifinished"  />  &nbsp; <label for="rdbSemiFinished" style="font-weight:normal">Semi Finished</label>  &nbsp;
                                             <input type="radio"  id ="rdbRaw" name="Finished" value="Raw"   />  <label for="rdbRaw" style="font-weight:normal">Raw</label>  
                                            
                                            
                                                  
                                                    
                                                </div>
                                            </div>
                                        </div>
                                   



                                   
                                         


                                        

                                        <div class="row">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table width="100%" class="table" style="margin:0px">
                                    <tr>
                                    <th>Type</th><th>Godown</th><th><span id="spName">Group</span></th><th></th>
                                    </tr>
                                    <tr>
                                    <td>
                                     <input type="radio"  id="rdbcompany" onchange="javascript:ChangeText('Company')"  name="Department"  value="Company"  /> &nbsp;<label for="rdbcompany" style="font-weight:normal">Company</label> &nbsp;
                                            <input type="radio"  id="rdbDept" onchange="javascript:ChangeText('Department')"  name="Department"  value="Department"  /> &nbsp;<label for="rdbDept" style="font-weight:normal">Department</label> &nbsp;
                                                        <input type="radio" onchange="javascript:ChangeText('Group')" id="rdbGroup" name="Department" value="Group" checked="" required /> <label for="rdbGroup" style="font-weight:normal">Group</label> 
                                                 </td>
                                    
                                    <td>
                                    <select id="ddlGodown"  style="width:120px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>

                                    <td>
                                    
                                       <select id="ddlGroup"  style="width:130px;height:30px">
                                                <option></option>
                                               </select>
                                    </td>
                                    

                                    <td>
                              
                                    </td>

                                    </tr>



                                      
                                        </table>
                                        </div>


                                        </div>


                                        <div class="row">
                                        <div class="col-md-12">
                                        <table width="100%" style="margin:10px 0 0  0;background:#FFFFD7;" class="table">
                                        <tr>
                                        <td>
                                 
                                               
                                               <input type="radio"  id="rdbCode" name="Code" value="Code"  /> &nbsp;<label style="font-weight:normal" for="rdbCode">Sort By Code </label> &nbsp;
                                                        <input type="radio"  id ="rdbName" name="Code" value="Name" checked="" required /> <label style="font-weight:normal" for="rdbName">Sort By Name </label>
                                         
                                        
                                        
                                        </td>
                                       <%-- <td style="text-align:right;padding-right:20px">
                                                 <input type="radio"  id="rdbUnit" name="Column" value="Unit"  /> &nbsp; <label style="font-weight:normal" for="rdbUnit">Show Unit Column </label>&nbsp;
                                                 
                                                    
                                                        <input type="radio" id="rdbRate" name="Column" value="Rate" checked="" required /> <label style="font-weight:normal" for="rdbRate">Sort Rate Column</label>
                                              
                                           
                                        </td>--%>
                                        
                                        </tr>
                                        
                                        </table>
                                        
                                        </div>
                                        
                                        </div>


                            
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    
                          


                    <div class="row">
                  <div class="col-md-12">
                            <div class="x_panel" style="background:seashell;padding:0px">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                  
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px">
<thead>
<tr><th>
 

Item/Code</th><th>Code</th><th>Name</th><th>Stock</th><th>ActualStock</th><th>Rate</th><th>MRP</th><th>Adjusted</th><th>Amount</th></tr>

</thead>
<tbody>
<tr>
<td id="DKID">
 
 


<select style="width:154px" id="ddlProducts" class="form-control"></select>
</td>
<td>
<input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtName" type="text"  class="form-control customTextBox"   style="width:120px"/></td>
<td><input type="text" id="txtStock" class="form-control customTextBox"  readonly="readonly"  /></td>
<td><input type="text" id="txtQty" class="form-control customTextBox"  /></td>
<td><input type="text" id="txtRate"  class="form-control customTextBox" readonly="readonly"  /></td>
<td><input type="text" id="txtMarketPrice" class="form-control customTextBox" readonly="readonly" /></td>
<td><input type="text" id="txtAdjusted" class="form-control customTextBox" /></td>
<td>
<input type="text"  class="form-control customTextBox" readonly="readonly" id="txtAmount"  /></td>
<td>
<button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  

                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:150px;overflow-y:scroll;min-height:150px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th>Bal.Stock</th><th>ActualStock</th><th>Rate</th><th>MRP</th><th>Adjusted</th><th>Amount</th></tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                     <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" style="margin-top:5px" data-bind="click: $root.PlaceOrder" class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" style="margin-top:5px" data-bind="click: $root.CancelOrder" class="btn btn-danger"   > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>

                            </div>
                        </div>
  
