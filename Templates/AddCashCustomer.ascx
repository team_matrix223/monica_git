﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddCashCustomer.ascx.cs" Inherits="Templates_OrderTemplates_AddCashCustomer" %>

<script language="javascript">

    $(document).ready(


      function () {




          $('#cm_DOB').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1"
          }, function (start, end, label) {
              

              console.log(start.toISOString(), end.toISOString(), label);
          });


          $('#cm_DOA').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1"
          }, function (start, end, label) {
 

              console.log(start.toISOString(), end.toISOString(), label);
          });







      }
    );

    function ClearCustomerDialog() {
        validateForm("detach");
        
        $("#cm_ddlPrefix").val();
        $("#cm_Name").val("");
        $("#cm_Address1").val("");
        $("#cm_Address2").val("");
        $("#cm_DOB").val("1/1/1");
        $("#cm_DOA").val("1/1/1");
        $("#cm_Discount").val("0");
        $("#cm_ContactNumber").val("");
        $("#cm_EmailId").val("");
        $("#CustomerDialog").dialog("close");
    }

    function InsertUpdateCustomer() 
    {

        debugger;
 
        if (!validateForm("formID")) {
            return;

            debugger;
        }

   
        var objCustomer = {};
        objCustomer.Customer_ID = 0;
        objCustomer.Prefix = $("#cm_ddlPrefix").val() ;
        objCustomer.Customer_Name = $("#cm_Name").val();
        objCustomer.Address_1 = $("#cm_Address1").val();
        objCustomer.Address_2 = $("#cm_Address2").val();
        objCustomer.Area_ID = $("#cm_ddlArea").val();
        objCustomer.City_ID = $("#cm_ddlCity").val();
        objCustomer.State_ID = $("#cm_ddlState").val();
        objCustomer.Date_Of_Birth = $("#cm_DOB").val();
        objCustomer.Date_Anniversary = $("#cm_DOA").val();
        objCustomer.Discount = $("#cm_Discount").val();
        objCustomer.Contact_No = $("#cm_ContactNumber").val();
        objCustomer.EmailId = $("#cm_EmailId").val();
        objCustomer.GSTNo = $("#cm_GSTNo").val();
       
        var IsActive = false;


        if ($('#cm_FOC').is(":checked")) {
            IsActive = true;
        }

        objCustomer.FocBill = IsActive;
    

         var DTO = { 'objCustomer': objCustomer };


         
         $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "placeorder.aspx/Insert",
             data: JSON.stringify(DTO),
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 

                 if (obj.Status == "-1") {

                     alert("Sorry. Contact Number Already Registered with our Database");
                     $("#cm_ContactNumber").focus();
                     return;
                 }



                 $("#CustomerDialog").dialog("close");

                 alert("Customer Added Successfully");

                 $("#cm_ddlPrefix").val();
                 $("#cm_Name").val("");
                 $("#cm_Address1").val("");
                 $("#cm_Address2").val("");
                 $("#cm_DOB").val("1/1/1");
                 $("#cm_DOA").val("1/1/1");
                 $("#cm_Discount").val("0");
                 $("#cm_ContactNumber").val("");
                 $("#cm_EmailId").val("");
                 $("#txtId").val(obj.Customer.Customer_ID).change();
                 $("#txtMobile").val(obj.Customer.Contact_No).change();
                 $("#txtCustomerName").val(obj.Customer.Customer_Name).change();
                 $("#txtAddress").val(obj.Customer.Address_1).change();

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {


             }

         });


    }
</script>

 <div class="x_panel">
                                <div class="x_title">
                                    <h2>Customer Information</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-wrench"></i></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <form class="form-horizontal form-label-left" data-parsley-validate="" id="formID" novalidate="">

                             <table width="100%">
                             <tr><td>
     
                              <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Prefix <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                             <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                       
                                         
                                            </div>
                                        </div>
     
                    
                                  
                             <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address1 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12" id="cm_Address1"  required></textarea>
                                             </div>
                                        </div>


                                      
   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Area <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                            <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlArea" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                      
                                         
                                            </div>
                                        </div>


                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                           <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlState" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                      
                                         
                                            </div>
                                        </div>


                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Date Of Birth <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text"   class="form-control col-md-7 col-xs-12" value="1/1/1"  id="cm_DOB"> 
                                            </div>
                                        </div>

                                        
                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Contact No <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber"  id="cm_ContactNumber"> 
                                            </div>
                                        </div>
                                   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">EmailId<span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12 "  id="cm_EmailId"> 
                                            </div>
                                        </div>
                             </td>
                             
                             <td valign="top">
                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Customer Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required alphanumeric"    id="cm_Name"> 
                                            </div>
                                        </div>


                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12" id="cm_Address2" ></textarea>
                                             </div>
                                        </div>
                                  

                                    <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">City <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                        <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlCities" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                      
                                         
                                         
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Discount <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber" value="0"   id="cm_Discount" disabled="disabled"> 
                                            </div>
                                        </div>

                                                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Anniversary Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12"  value="1/1/1" required="required" id="cm_DOA"> 
                                            </div>
                                        </div>

                                                               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">FOC <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                            <input type="checkbox" id="cm_FOC" disabled="disabled"/>
                                           
                                            </div>
                                        </div>

                             </td>
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                <button class="btn btn-success" type="button" onclick="javascript:InsertUpdateCustomer()">Submit</button>
                                           <button class="btn btn-primary" onclick="javascript:ClearCustomerDialog()" type="button">Cancel</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
 
                            
   
 

 
 