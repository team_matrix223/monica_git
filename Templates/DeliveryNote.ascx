﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeliveryNote.ascx.cs" Inherits="Templates_DeliveryNote" %>

<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker"  TagPrefix="cc1" %>
<script type ="text/javascript">


    $(document).ready(
    function () {
        BindDealer();






        function BindDealer() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/BindDealers",
                data: {},
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html = "<option value = 0>--SELECT--</option>";
                    var html1 = "<option value = 0>--SELECT--</option>";
                    var html2 = "<option value = 0>--SELECT--</option>";
                    for (var i = 0; i < obj.DealerOptions.length; i++) {

                        html = html + "<option value='" + obj.DealerOptions[i]["CCODE"] + "' Dis ='" + obj.DealerOptions[i]["DIS_PER"] + "'>" + obj.DealerOptions[i]["CNAME"] + "</option>";
                    }

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }

                    for (var i = 0; i < obj.BranchOptions.length; i++) {

                        html2 = html2 + "<option value='" + obj.BranchOptions[i]["BranchId"] + "'>" + obj.BranchOptions[i]["BranchName"] + "</option>";
                    }
                    $("#ddlDealer").html(html);
                    $("#ddlGodown").html(html1);
                    $("#ddlBranch").html(html2);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {




                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled",true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {



                            $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
                            var ItemType = "";

                            if ($("#rdbcfinished").prop("checked") == true) {
                                ItemType = "3";
                            }
                            else if ($("#rdbraw").prop("checked") == true) {
                                ItemType = "1";
                            }
                            else if ($("#rdbsemi").prop("checked") == true) {
                                ItemType = "5";
                            }



                            var Excise = "0";
                            if ($("#rdbExcise").prop("checked") == true) {
                                Excise = "1";

                            }
                            else if ($("#rdbnonexcise").prop("checked") == true) {
                                Excise = "2";
                            }
                            else if ($("#rdballexcise").prop("checked") == true) {
                                Excise = "2";
                            }



                            $("#ddlProducts").supersearch({
                                Type: "Product",
                                Caption: "Please enter Item Name/Code ",
                                AccountType: "",
                                Godown: $("#ddlGodown").val(),
                                ItemType: ItemType,
                                Excise: Excise,
                                Width: 214,
                                DefaultValue: 0
                            });


                        }

                    });



                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Dealer Billing</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 <div class="x_content">
                                    
                                       <table style="width:100%">

                                     <tr><td style="display:none"><input type="radio" id="rdbExcise" name="Excise" /> <label class="control-label" for="rdbExcise" style="font-weight:normal">Excisable Items</label></td><td><input type="radio" id="rdbnonexcise" name="Excise" checked="checked" /> <label class="control-label" for="rdbnonexcise" style="font-weight:normal"> Non-Exciseable Items</label></td>
                                    
                                     <td style="display:none"><input type="radio" id="rdballexcise" name="Excise" /> <label class="control-label" for="rdballexcise" style="font-weight:normal"> All Items</label></td><td></td><td></td><td></td></tr>
                                    <tr><td>         <label class="control-label">Bill No</label></td>
                                    <td>      
                                    <input type="text" class="form-control" 
                                    
                                    id="txtBillNo" style="width:107px" readonly ="readonly" data-bind="value:BillNo" >
                                    </td>


                                    <td> <label class="control-label">Gr No</label></td>
                                    <td>      
                                    <input type="text" class="form-control" 
                                    
                                    id="txtGrNo" style="width:107px" data-bind="value:GrNo" class="validate required" >


                                    </td>


                                       <td> <label class="control-label">Order Date</label></td>
                                    <td>      
                                    <input type="text" class="form-control" 
                                    
                                    id="txtOrderDate" style="width:107px" data-bind="value:GrNo" >


                                    </td>





                                    </tr>
                                   
                                    <tr><td ><label class="control-label">Bill Date</label></td><td style="padding-top:5px"> <input type="text" class="form-control" 
                                    
                                    id="txtBillDate" style="width:107px" data-bind="value:TotalAmount" ></td>
                                    
                                    <td><label class="control-label">Date</label></td><td style="padding-top:5px"><input type="text" class="form-control" 
                                   
                                    id="txtDate" style="width:107px"      ></td>

                                    
                                    <td><input type="radio" id="rdbcfinished" name="cash" checked="checked"/> <label class="control-label" for="rbLocal" style="font-weight:normal"> Finished</label></td>
                                    <td><input type="radio" id="rdbraw" name="cash" /> <label class="control-label" for="rbLocal" style="font-weight:normal"> Raw</label></td>
                                    
                                    
                                    </tr>
                                  

                                    <tr><td ><label class="control-label">Disp Date</label></td><td style="padding-top:5px"> <input type="text" class="form-control" 
                                    
                                    id="txtDispDate" style="width:107px" data-bind="value:TotalAmount" ></td>
                                    
                                    <td><label class="control-label">Veh No.</label></td><td style="padding-top:5px"><input type="text" class="form-control" 
                                   
                                    id="txtVehNo" style="width:107px"      ></td>
                                    
                                   <td><input type="radio" id="rdbsemi" name="cash" /> <label class="control-label" for="rbLocal" style="font-weight:normal"> Semi-Finished</label></td>
                                   <td></td>
                                    
                                    
                                    </tr>




                                    <tr>

                                    <td><label class="control-label">Dispatch Time</label></td>
                                    <td><cc1:TimeSelector  style="height:30px" ID="tsDispatch" runat="server"  DisplaySeconds="false">
</cc1:TimeSelector></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                    
                                    </tr>


                                   
                                    </table>

                                </div>
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td>         <label class="control-label" style="display:none">Dealer</label></td>
                                    <td>      
                                   <select id="ddlDealer" style="width:100px;display:none">
                                             <option value="0"></option>
                                             </select>
                                    </td>
                                   

                                        <td> <label class="control-label">Branch</label></td>
                                    <td>      
                                    <select id="ddlBranch" style="width:100px">
                                             <option value="0"></option>
                                             </select>


                                    </td>


                                    <td> <label class="control-label">Godown</label></td>
                                    <td>      
                                    <select id="ddlGodown" style="width:100px">
                                             <option value="0"></option>
                                             </select>


                                    </td>


                                       <td> <label class="control-label">Form</label></td>
                                    <td>      
                                     <select id="ddlRefno" style="width:100px">
                                            <option value="0">--SELECT--</option>
                                             <option value="C FORM">C FORM</option>
                                             <option value="F FORM">F FORM</option>
                                             <option value="NOT AGAINST FORM">NOT AGAINST FORM</option>
                                             </select>


                                    </td>





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>
                          


                    <div class="row">
                  <div class="col-md-12">
                            <div class="x_panel" style="background:seashell;padding:0px">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                  
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px">
<thead>
<tr><th>
 

Item/Code</th><th>Code</th><th>Name</th><th>Stock</th><th>Qty</th><th>Rate</th><th>MRP</th><th>Amount</th></tr>

</thead>
<tbody>
<tr>
<td id="DKID">
 
 


<select style="width:154px" id="ddlProducts" class="form-control"></select>
</td>
<td>
<input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtName" type="text"  class="form-control customTextBox"   style="width:120px"/></td>
<td><input type="text" id="txtStock" class="form-control customTextBox"  readonly="readonly"  /></td>
<td><input type="text" id="txtQty" class="form-control customTextBox"  /></td>
<td><input type="text" id="txtRate"  class="form-control customTextBox"   /></td>
<td><input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>
<td>
<input type="text"  class="form-control customTextBox" readonly="readonly" id="txtAmount"  /></td>
<td>
<button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  

                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:150px;overflow-y:scroll;min-height:150px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th style="display:none">CaseQty</th><th>Qty</th><th style="display:none">Scheme</th><th>Rate</th><th>MRP</th><th>Amount</th><th>Dis3</th><th style="display:none">QTy_To_Less</th><th  >Stock</th><th>Tax</th><th>Excise</th><th style="display:none">Abatement</th></tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table><tr><td>         <label class="control-label">Remarks</label></td>
                                    <td>
                                 <textarea id="txtremarks" style="height: 57px;"></textarea> 
                                   
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" style="margin-top:5px"  class="btn btn-danger"   > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    <td colspan = "100%" style="float:right">
                                    <table><tr>


                                    <td> <label class="control-label">Bill Value</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtBillval" readonly="readonly" style="width:107px"  > 


                                    </td>
                                    </tr>

                                    <tr>


                                    <td> <label class="control-label">Excise</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtExcise" readonly="readonly" style="width:107px"  > 


                                    </td>
                                    </tr>

                                    <tr>

                                    <td colspan ="100%">
                                    <table style="width:100%">
                                    <tr>
                                    <td> <label class="control-label">Discount</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtDisPer" readonly="readonly" style="width:40px;display:none"  > 


                                    </td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtDisAmt" readonly="readonly" style="width:107px;margin-left:57px"  > 

                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                    </tr>

                                    <tr>

                                    <td> <label class="control-label">Adj(-)Less/(+)Add</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtAdj" readonly="readonly" style="width:107px"  > 


                                    </td>


                                   
                                    </tr>

                                       <tr>

                                    <td> <label class="control-label">Net Amount</label></td>
                                    <td>      
                                    <input type="text" class="form-control" id="txtnetAmt" readonly="readonly" style="width:107px" data-bind="value:GrNo" > 


                                    </td>


                                   
                                    </tr>

                                    </table>
                                    </td>





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>

                            </div>
                        </div>
  
