﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerSearch.ascx.cs" Inherits="Templates_CustomerSearch" %>


<script language="javascript" type="text/javascript">



    var m_CustomerID = 0;
    function GetSearchResult() {

        bindGrid();
       
    
    }

</script>
  <div id="dvSearch" style="background-color :#FFF7E6">
  <table width="100%">
  <tr><td>
   <div id="dvLeft" style="float:left;width:37%;border:1px solid silver">
 
  
   <table  cellpadding="5" cellspacing="0"  width="100%">
      <tr style="background-color:#FFE7B3"><td colspan="100%" >Search On:</td></tr>
   <tr><td><input type="radio" id="rbPhoneNo" value="M" name="searchon" checked="checked" /> <label for="rbPhoneNo" style="font-weight:normal" > Phone No.</label></td><td><input type="radio" id="rbCustomerName" value="N" name="searchon" /> <label for="rbCustomerName"  style="font-weight:normal"> Customer Name</label></td></tr>
   </table>
     
   </div>
   <div id="dvRight" style="float:right;width:60%;border:1px solid silver">
   <table cellpadding="5" cellspacing="0" width="100%">
   <tr style="background-color:#FFE7B3"><td colspan="100%" >Search Criteria:</td></tr>
   <tr><td><input type="radio" id="rbStartingWith" value="S"  name="searchcriteria"/> <label for="rbStartingWith"  style="font-weight:normal"> Starting With</label></td><td><input type="radio" value="C" id="rbContaining" checked="checked"  name="searchcriteria"/> <label for="rbContaining" style="font-weight:normal" > Containing</label></td><td><input type="radio" id="rbEndingWith" value="E" name="searchcriteria"/> <label for="rbEndingWith"  style="font-weight:normal"> Ending With</label></td><td><input type="radio" id="rbExact" name="searchcriteria" value="EX"/> <label for="rbExact"  style="font-weight:normal"> Exact</label></td></tr>
   </table>
   
   </div>

  </td></tr>
  <tr><td>
  <table>
  <tr><td>Type to Search:</td><td><input type="text" class="form-control input-small" style="width:400px"   id="txtSearch"/></td><td><div id="btnSearch"  class="btn btn-primary btn-small"   onclick="javascript:GetSearchResult();"  >Search</div></td></tr>
  </table>
  
  </td></tr>
   <tr><td>
   
   			           <table id="jQGridDemo1">
    </table>
    <div id="jQGridDemoPager1">
    </div>
   
   </td></tr>

  </table>

  <script language="javascript" type="text/javascript">
    

    
  function bindGrid() {
    
   

      var searchon=$("input[name='searchon']:checked").val();
    var criteria=$("input[name='searchcriteria']:checked").val();
    var stext=$("#txtSearch").val();
        
     
                   jQuery("#jQGridDemo1").GridUnload();
  
             jQuery("#jQGridDemo1").jqGrid({
            url: 'handlers/CustomersList.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ID','Name', 'Address1', 'Address2','Area','City','State','DateOfBirth','AnniversaryDate','Discount','ContactNo','Tag','FocBill','Group'],
            colModel: [
                        { name: 'Customer_ID',key:true, index: 'Customer_ID', width: 100, stype: 'text',sorttype:'int',hidden:true },
   		                { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text',sorttype:'int',hidden:false,editable:false },
   		             
                        { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
   		                 { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                
   		                 { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                 { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:false},
   		                 { name: 'strAD', index: 'strAD', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'Discount', index: 'Discount', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text',hidden:false, sortable: true, editable: true ,editrules: { required: true }},
   		                
   		                { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true },hidden:true},
   		                { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       { name: 'grpid', index: 'grpid', width: 150, stype: 'text',hidden:true, sortable: true, editable: true ,editrules: { required: true }},
                       ],
            rowNum: 10,
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager1',
            sortname: 'Code',
            viewrecords: true,
            height: "100%",
            width:"800px",
            sortorder: 'desc',
            caption: "Customers List" 
             
             
        });


        $('#jQGridDemo1').jqGrid('navGrid', '#jQGridDemoPager1',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: true,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );

       var Datad = jQuery('#jQGridDemo1'); 
     Datad.jqGrid('setGridWidth', '500');



     $("#jQGridDemo1").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
         
     
                 //  $("#hdnCustomerId").val(CustomerId).change();
             
                 $("#txtId").val($('#jQGridDemo1').jqGrid('getCell', rowid, 'Customer_ID')).change();
                  $("#txtMobile").val($('#jQGridDemo1').jqGrid('getCell', rowid, 'Contact_No')).change();
                  $("#txtCustomerName").val($('#jQGridDemo1').jqGrid('getCell', rowid, 'Customer_Name')).change();
                  $("#txtAddress").val($('#jQGridDemo1').jqGrid('getCell', rowid, 'Address_1')).change();
                  $('#dvCustomerSearch').dialog("close");
                    
             }
         });

   


     }






       

    </script>
  
  </div>