﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Templates_CreditCustomers : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        BindDropDownList();
        BindAccsgroup();
        hdnDate.Value = DateTime.Now.ToShortDateString();
    }


    void BindDropDownList()
    {

        DataSet ds = new CommonMasterDAL().GetAll();

        cm_ddlCities.DataSource = ds.Tables[0];
        cm_ddlCities.DataValueField = "CIty_ID";
        cm_ddlCities.DataTextField = "City_Name";
        cm_ddlCities.DataBind();

        cm_ddlArea.DataSource = ds.Tables[1];
        cm_ddlArea.DataTextField = "Area_Name";
        cm_ddlArea.DataValueField = "Area_ID";
        cm_ddlArea.DataBind();


        cm_ddlState.DataSource = ds.Tables[2];
        cm_ddlState.DataTextField = "State_Name";
        cm_ddlState.DataValueField = "STATE_ID";
        cm_ddlState.DataBind();

        cm_ddlPrefix.DataSource = ds.Tables[3];
        cm_ddlPrefix.DataTextField = "PROP_NAME";
        cm_ddlPrefix.DataValueField = "PROP_NAME";
        cm_ddlPrefix.DataBind();


    }

    void BindAccsgroup()
    {
        cm_ddlAccSGroup.DataSource = new AccSGroupBLL().GetAll();
        cm_ddlAccSGroup.DataValueField = "SS_CODE";
        cm_ddlAccSGroup.DataTextField = "SS_NAME";
        cm_ddlAccSGroup.DataBind();
    }
}