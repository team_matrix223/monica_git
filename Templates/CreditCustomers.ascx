﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreditCustomers.ascx.cs" Inherits="Templates_CreditCustomers" %>
 <style type="text/css">
 
 .form-control
 {
     margin-bottom:10px;
 }
 </style>
<script language="javascript">

    var m_CustomrId = -1;

    $(document).ready(


      function () {

      $("#cm_CSTDate").val($("#<%=hdnDate.ClientID%>").val());
        

          $('#cm_CSTDate').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1"
          }, function (start, end, label) {


              console.log(start.toISOString(), end.toISOString(), label);
          });
          





      });

   

      function BindInformation(CustomerId) {




          $.ajax({
              type: "POST",
              data: '{"AccountId":"' + CustomerId + '"}',
              url: "managecreditcustomers.aspx/GetByCustomerId",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);
                  m_CustomrId = obj.Customer.AccountId;
                  $("#cm_ddlPrefix").val(obj.Customer.Prefix);
                  $("#cm_Name").val(obj.Customer.CNAME);
                  $("#cm_ShortName").val(obj.Customer.CNAME);
                  $("#cm_Address").val(obj.Customer.CADD1);
                  $("#cm_Address1").val(obj.Customer.CADD2);
                  var area = obj.Customer.AREA_ID;
                  $("#cm_ddlArea").val(area);
                  var city = obj.Customer.CITY_ID;
                  $("#cm_ddlCity").val(city);

                  var state = obj.Customer.STATE_ID;
                  $("#cm_ddlState").val(state);

                  $("#cm_CreditLimit").val(obj.Customer.CR_LIMIT);
                  $("#cm_CreditDays").val(obj.Customer.CR_DAYS);
                  $("#cm_OpBal").val(obj.Customer.OP_BAL);

                  $("#cm_ddlOption").val(obj.Customer.DR_CR);
                  $("#cm_CSTNo").val(obj.Customer.CST_NO);
                  $("#cm_CSTDate").val(obj.Customer.strCSTDate);
                  $("#cm_TinNo").val(obj.Customer.TINNO);
                  $("#cm_TOTNo").val(obj.Customer.TOTNO);
                  $("#cm_ContactPerson").val(obj.Customer.CONT_PER);
                  $("#cm_ContactNumber").val(obj.Customer.CONT_NO);
                  $("#cm_Discount").val(obj.Customer.DIS_PER);
                  $("#cm_ddlAccSGroup").val(obj.Customer.SS_CODE);


                  $('#cm_DeliveryNote').prop("checked", false);


                  if (obj.Customer.ACC_ZONE == "DELIVERY NOT") {
                      $('#cm_DeliveryNote').prop("checked", true);
                  }


                  $("#CustomerDialog").dialog({ autoOpen: true,

                      width: 800,
                      resizable: false,
                      modal: true
                  });

              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {
                  $.uiUnlock();
              }
          });


      }

      

      function GetVMResponse(Id, Title, IsActive, Status, Type) {
          $("#dvVMDialog").dialog("close");

          var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
          if (Type == "Area") {

              $("#cm_ddlArea").append(opt);

           }

           if (Type == "City") {

               $("#cm_ddlCities").append(opt);

           }

           if (Type == "State") {

               $("#cm_ddlState").append(opt);

           }



       }

      function OpenVMDialog(Type) {


          $.ajax({
              type: "POST",
              data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
              url: "managearea.aspx/LoadUserControl",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  $("#dvVMDialog").remove();
                  $("body").append("<div id='dvVMDialog'/>");
                  $("#dvVMDialog").html(msg.d).dialog({ modal: true });
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }
          });

      }

      function ClearCustomerDialog() {
          validateForm("detach");

          $("#cm_ShortName").val();
          $("#cm_Name").val("");
          $("#cm_Address").val("");
          $("#cm_Address1").val("");
          $("#cm_CreditLimit").val("0");
          $("#cm_CSTDate").val($("#<%=hdnDate.ClientID%>").val());
          $("#cm_CSTNo").val("");
          $("#cm_CreditDays").val("0");
          $("#cm_OpBal").val("0");
          $("#cm_TinNo").val("0");
          $("#cm_TOTNo").val("0");
          $("#cm_ContactPerson").val("");
          $("#cm_ContactNumber").val("0");
          $("#cm_Discount").val("0");
          $("#CustomerDialog").dialog("close");
          m_CustomrId = 0;
          $('#cm_DeliveryNote').prop("checked", false);
      }

      function InsertUpdateCustomer() {

          if (!validateForm("myFormID")) {
              return;
          }

          var objAccLedger = {};
          objAccLedger.AccountID = m_CustomrId;
          objAccLedger.CODE = "D";
          objAccLedger.SS_CODE = $("#cm_ddlAccSGroup").val();
          objAccLedger.CNAME = $("#cm_Name").val();
          objAccLedger.CADD1 = $("#cm_Address").val();
          objAccLedger.CADD2 = $("#cm_Address1").val();
        
          if ($("#cm_ddlArea").val() == 0)
          {
              objAccLedger.Area_ID = 0;
          }
          else
          {
              objAccLedger.Area_ID = $("#cm_ddlArea").val();
          }
          objAccLedger.CITY_ID = $("#cm_ddlCities").val();
          objAccLedger.State_ID = $("#cm_ddlState").val();

          objAccLedger.CST_NO = $("#cm_CSTNo").val();
          objAccLedger.CST_DATE = $("#cm_CSTDate").val();
          objAccLedger.TINNO = $("#cm_TinNo").val();
          objAccLedger.TOTNO = $("#cm_TOTNo").val();
          objAccLedger.CR_LIMIT = $("#cm_CreditLimit").val();
          objAccLedger.CR_DAYS = $("#cm_CreditDays").val();

          objAccLedger.CONT_PER = $("#cm_ContactPerson").val();
          objAccLedger.CONT_NO = $("#cm_ContactNumber").val();
          objAccLedger.OP_BAL = $("#cm_OpBal").val();
          objAccLedger.DR_CR = $("#cm_ddlOption").val();

          objAccLedger.DIS_PER = $("#cm_Discount").val();
          objAccLedger.PREFIX = $("#cm_ddlPrefix").val();

          

          var AccZone = "";
          if ($('#cm_DeliveryNote').is(":checked")) {
              AccZone = "DELIVERY NOT";
          }
          objAccLedger.ACC_ZONE = AccZone;
         
          var DTO = { 'objAccLedger': objAccLedger };

          $.ajax({
              type: "POST",
              contentType: "application/json; charset=utf-8",
              url: "managecreditcustomers.aspx/InsertUpdateCustomer",
              data: JSON.stringify(DTO),
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);

                  if (obj.Status == -1) {


                      alert("An error occured during transaction. Please try again.");
                      return;
                  }
                  else {
                      alert("Customer Saved Successfully");
                      ClearCustomerDialog();
                  }
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {


              }

          });


      }


          </script>

 <div class="x_panel" id="myFormID">
     <asp:HiddenField ID="hdnDate" runat="server"/>
                                <div class="x_title">
                                    <h2>Credit Customer Information</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-wrench"></i></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <form class="form-horizontal form-label-left" data-parsley-validate="" id="formID" novalidate="">

                             <table width="100%">
                             <tr><td valign="top">
                              
                               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Prefix <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                             <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server">
                                            
                    
                                             </asp:DropDownList>
                                       
                                         
                                            </div>
                                        </div>
     


                              <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                              <input type="text"   class="form-control col-md-7 col-xs-12" id="cm_Name"> 
                                       
                                         
                                            </div>
                                        </div>
     
                            <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Short Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                              <input type="text"   class="form-control col-md-7 col-xs-12" id="cm_ShortName"> 
                                       
                                         
                                            </div>
                                        </div>
                                  
                             <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address1 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12  validate required" id="cm_Address"  required></textarea>
                                             </div>
                                        </div>

                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12 " id="cm_Address1"  required></textarea>
                                             </div>
                                        </div>


                                      
                                     <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Area <span class="required">*</span>
                                            </label>

                                         <div class="col-md-7 col-sm-7 col-xs-12">
                                          <table>

                                              <tr>
                                                  <td> <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlArea" ClientIDMode="Static"  runat="server"></asp:DropDownList></td>
                                                  <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                    <span class="fa fa-plus" onclick="javascript:OpenVMDialog('Area')"  style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                  </td>
                                              </tr>
                                          </table>
                                            

                                           
                                      
                                         
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">City <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                         <table>
                                             <tr>
                                                 <td>
                                                      <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlCities" ClientIDMode="Static" style="width:168px"  runat="server"></asp:DropDownList>
                                      

                                                 </td>
                                                 <td  style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')"  style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>
                                                 </td>
                                             </tr>

                                         </table>
                                       
                                         
                                         
                                            </div>
                                        </div>


                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <table>
                                                    <tr>
                                                        <td>
                                                     <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlState" ClientIDMode="Static"  runat="server"></asp:DropDownList>

                                                        </td>
                                                        <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                        <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                        </td>
                                                    </tr>

                                                </table>
                                          
                                      
                                         
                                            </div>
                                        </div>


                                  <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Op. Bal <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                           <table>
                                            <tr><td>
                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required float" value="0"  style="width:100px"  id="cm_OpBal"> 

                                                </td>
                                            <td style="padding-left:10px">
                                        
                                            <select id="cm_ddlOption" style="width:50px;height:30px;margin-top:0px" class="validate ddlrequired" >
                                            <option value="DR">DR</option>
                                            <option value="CR">CR</option></select>
                                                </td>

                                                </tr>

                                                </table>
                                         
                                            </div>
                                        </div>



                                      
                                            
                                          

                                         
                             </td>
                             
                             <td valign="top">
                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">GST No 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required"    id="cm_CSTNo"> 
                                            </div>
                                        </div>
                                       

                                          <div class="form-group" style="display:none"  >
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">GST Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                             <input type="text" class="form-control col-md-7 col-xs-12 "    id="cm_CSTDate"> 
                                             </div>
                                        </div>


                                         <div class="form-group" style="display:none">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">TIN No <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
        <input type="text" class="form-control col-md-7 col-xs-12 " value="0"   id="cm_TinNo"> 
                                        
                                            </div>
                                        </div>


                 

                                                            <div class="form-group" style="display:none">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">TOT No <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12"   id="cm_TOTNo"> 
                                            </div>
                                        </div>

                                                               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Contact Person <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                            <input type="text" class="form-control col-md-7 col-xs-12" id="cm_ContactPerson"> 
                                           
                                            </div>
                                        </div>

                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Contact Number 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                              <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber" value="0"   required="required" id="cm_ContactNumber"> 
                                           
                                            </div>
                                        </div>



                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Discount(%) 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required float" value="0"  id="cm_Discount" /> 
                                            </div>
                                        </div>


                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Acc SubGroup <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                           <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlAccSGroup" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                      
                                         
                                            </div>
                                        </div>


                                           <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Credit Limit <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text"   class="form-control col-md-7 col-xs-12 validate required float" value="0"   id="cm_CreditLimit"> 
                                            </div> 
                                        </div>


                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Credit Days <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text"   class="form-control col-md-7 col-xs-12 validate required valNumber" value="0"  id="cm_CreditDays"> 
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">DeliveryNote 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                            <input type="checkbox" id="cm_DeliveryNote" />
                                           
                                            </div>
                                        </div>
                                                           
                                       
                                               
                                       
                                            
                                            </td>
                                            
                                            </tr>
                                            </table>
                                            
                                           

                                            
                                        </div>

                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                           <button class="btn btn-danger" onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>