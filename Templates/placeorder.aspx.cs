﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.IO;

public partial class placeorder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            hdnDate.Value = DateTime.Now.ToShortDateString();

        }
    }



    [WebMethod]
    public static string InsertUpdateCustomer(Customers objCustomer)
    {
        int Status = new CustomerBLL().InsertUpdate(objCustomer);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            Status = Status,
            Customer = objCustomer

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string LoadUserControl(string message)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/CustomerSearch.ascx");
            //(userControl.FindControl("lblMessage") as Label).Text = message;
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }

    [WebMethod]
    public static string LoadControlAddCustomer(string message)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddCashCustomer.ascx");
            //(userControl.FindControl("lblMessage") as Label).Text = message;
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }




    [WebMethod]
    public static string GetByItemType()
    {

        string Items = new ProductBLL().GetProductsByItemType("ORDER");


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ProductOptions = Items

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdate(Booking booking, BookingDetail[] OrderProducts)
    {
        int Status = 0;


        Booking objbooking = new Booking()
        {
            OrderNo = booking.OrderNo,
            OrderDate = booking.OrderDate,
            Customer_ID = booking.Customer_ID,
            CustomerName = booking.CustomerName,
            Address = booking.Address,
            DeliveryType = booking.DeliveryType,
            DeliveryTime = booking.DeliveryTime,
            DeliveryAddress = booking.DeliveryAddress,
            Advance = booking.Advance,
            PaymentMode = booking.PaymentMode,
            LeftPayRecd = booking.LeftPayRecd,
            Remarks = booking.Remarks,
            UserNo = 0,
            Passing = false,
            DateBalRecd = booking.DateBalRecd,
            Ecode = 0,
            DisAmt = booking.DisAmt,
            Picture = "",
            VatAmount = booking.VatAmount,
            NetAmount = booking.NetAmount,
            OrderTime = DateTime.Now,
            IndRemarks = "",
            Prefix = "",
            ReadTag = false,
            FDispatch = false,


        };


        //Booking objBooking = new Booking(); objBooking.OrderId=34
        //new PlaceOrderBLL().GetOrderDetail(objBooking)


        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("ItemName");
        dt.Columns.Add("Weight");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Vat");

        DataRow dr;

        foreach (var item in OrderProducts)
        {
            dr = dt.NewRow();
            dr["ItemCode"] = item.Code;
            dr["ItemName"] = Convert.ToString(item.Name);
            dr["Weight"] = Convert.ToDecimal(item.Weight);
            dr["Qty"] = Convert.ToDecimal(item.Qty);
            dr["MRP"] = Convert.ToDecimal(item.MRP);
            dr["Rate"] = Convert.ToDecimal(item.Rate);
            dr["Amount"] = Convert.ToDecimal(item.Amount);
            dr["Vat"] = Convert.ToDecimal(item.Vat);
            dt.Rows.Add(dr);
        }


        Status = new BookingBLL().InsertUpdate(objbooking, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Booking = objbooking,
            ON = objbooking.OrderNo,
            Status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static Employees[] FetchEmployees()
    {


        var data = new EmployeeBLL().GetAll();
        return data.ToArray();
    }

    [WebMethod]
    public static string GetByOrderNo(int OrderNo)
    {

        Booking objBooking = new Booking();
        objBooking.OrderNo = OrderNo;

        List<BookingDetail> lst = new BookingBLL().GetBookingByOrderNo(objBooking);

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Booking = objBooking,
            BookingData = lst


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByMobileNo(string MobileNo)
    {
        Customers objCustomers = new Customers()
        {
            Contact_No = MobileNo,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CustomerBLL().GetByMobileNo(objCustomers);
        var JsonData = new
        {
            Customer = objCustomers


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetDispatchDetailByOrderNo(int OrderNo)
    {

        Booking objBooking = new Booking();
        objBooking.OrderNo = OrderNo;

        List<BookingDetail> lst = new BookingBLL().GetDispatchDetailByOrderNo(objBooking);

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Booking = objBooking,
            BookingData = lst


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdateDispatch(string OrderNo, Int32 CustomerCode, decimal DispatchValue, decimal Discount, decimal DiscountPer, decimal TaxAmount, decimal NetAmount, string PayMode, decimal CrPay, decimal CrPayLeft, string arrItemCode, string arrItemName, string arrWeight, string arrMRP, string arrRate, string arrTax, string arrDispQty)
    {
        int Status = 0;


        OrderDispatch objOrderDispatch = new OrderDispatch()
        {
            OrderNo = OrderNo,
            CustCode = CustomerCode,
            DSpValue = DispatchValue,
            Discount = Discount,
            TaxAmount = TaxAmount,
            NetAmount = NetAmount,
            PayMOde = PayMode,
            CrPay = CrPay,
            CrLeftPay = CrPayLeft,
            TaxType = "",
            UserId = 0,
            PcName = "",
            Payment = NetAmount,
            Disper = DiscountPer,

        };


        string[] ItemCodeData = arrItemCode.Split(',');
        string[] ItemNameData = arrItemName.Split(',');
        string[] WeightData = arrWeight.Split(',');
        string[] DQtyData = arrDispQty.Split(',');
        string[] MRPData = arrMRP.Split(',');
        string[] RateData = arrRate.Split(',');
        string[] VatData = arrTax.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("ItemName");
        dt.Columns.Add("Weight");
        dt.Columns.Add("DispatchQty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Vat");



        for (int i = 0; i < ItemCodeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = Convert.ToString(ItemCodeData[i]);
            dr["ItemName"] = Convert.ToString(ItemNameData[i]);
            dr["Weight"] = Convert.ToDecimal(WeightData[i]);
            dr["DispatchQty"] = Convert.ToDecimal(DQtyData[i]);
            dr["MRP"] = Convert.ToDecimal(MRPData[i]);
            dr["Rate"] = Convert.ToDecimal(RateData[i]);
            dr["Vat"] = Convert.ToDecimal(VatData[i]);
            dt.Rows.Add(dr);

        }





        Status = new BookingBLL().InsertUpdateDispatch(objOrderDispatch, dt);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Dispatch = objOrderDispatch,

            Status = Status
        };
        return ser.Serialize(JsonData);
    }

}