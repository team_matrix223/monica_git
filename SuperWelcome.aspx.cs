﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SuperWelcome : System.Web.UI.Page
{
    public Int32 BranchId { get { return Request.QueryString["bid"] != null ? Convert.ToInt32(Request.QueryString["bid"]) : 0; } }

    public string BranchName { get { return Request.QueryString["bname"] != null ? Convert.ToString(Request.QueryString["bname"]) : ""; } }
    public Int32 UserId { get { return Request.QueryString["UserId"] != null ? Convert.ToInt32(Request.QueryString["UserId"]) : 0; } }
    public string UserName { get { return Request.QueryString["UserName"] != null ? Convert.ToString(Request.QueryString["UserName"]) : ""; } }
    public string Sale_Rate { get { return Request.QueryString["Sale_Rate"] != null ? Convert.ToString(Request.QueryString["Sale_Rate"]) : ""; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        User objUser = new User
        {
            UserNo = UserId
        };
        new UserBLL().GetByUserId(objUser);

        Response.Cookies[Constants.AdminId].Value = UserId.ToString();
        Response.Cookies[Constants.DesignationId].Value = objUser.Counter_NO.ToString(); 
        Response.Cookies[Constants.BranchId].Value = BranchId.ToString();
        Response.Cookies[Constants.BranchName].Value = BranchName;
        Response.Cookies[Constants.EmployeeName].Value = UserName;
        Response.Cookies[Constants.Sale_Rate].Value = Sale_Rate;
        Response.Redirect("BillScreen.aspx");

    }
}