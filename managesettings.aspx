﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="managesettings.aspx.cs" Inherits="managesettings" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">

    function InsertUpdate() {


        var Apptype = $("#<%=ddlDisplay.ClientID %>").val();
        if ($.trim(Apptype) == "") {
            $("#<%=ddlDisplay.ClientID %>").focus();

            return;
        }


        $.ajax({
            type: "POST",
            data: '{ "Apptype": "' + Apptype + '"}',
            url: "managesettings.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == "") {

                    alert("Insertion Failed.slot already exists.");
                    return;
                }
                else {
                    alert("Settings Saved successfully");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }


    $(document).ready(
    function () {


        $("#btnAdd").click(
        function () {
 
            InsertUpdate();

        }
        );



    }
    );


</script>
</head>
<body>
    <form id="form1" runat="server">
     <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Settings
                        </span>
    <div class="youhave" style="padding-left:30px">

    <table>
    <tr>
     <td>Display Type:</td>
                    <td> <asp:DropDownList ID="ddlDisplay"  class="validate ddlrequired" runat="server"   Width="195px"  >
                    
                     <asp:ListItem Value ="ImageView" Text ="Image View"></asp:ListItem>
                     <asp:ListItem Value ="ListView" Text="List View"></asp:ListItem>
                    </asp:DropDownList>
                    </td>
    </tr>
    <tr>
       <td colspan="100%" style="padding-left:155px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td> <div id="btnAdd">Add</div></td>
                                            </tr>
                                            </table>
                                            </td>
    </tr>
    </table>
    </div>
    </div>
    </form>
</body>
</html>
