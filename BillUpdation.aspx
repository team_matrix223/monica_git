﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="BillUpdation.aspx.cs" Inherits="BillUpdation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


<form id="form1" runat="server">

        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
<%--    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-glyphicons.css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
   
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <style type="text/css">
        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }
    </style>
    <script language="javscript" type="text/javascript">
      

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode="";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var DiscountValues = [];
        function clsDiscount() {
            this.StartValue = 0;
            this.EndValue = 0;
            this.DisPer = 0;

        }

        function ResetCashCustmr() {

            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
          
        }


        function BindAddOn(counter) {

            $.ajax({
                type: "POST",
                data: '{"counter": "' + counter + '"}',
                url: "BillScreen.aspx/LoadUserControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    $("#dvAddOn").html(msg.d);

                    $("#dvAddOn").dialog({
                        autoOpen: true,

                        width: 500,
                        resizable: false,
                        modal: false
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
               );



        }

        function processingComplete() {
            $.uiUnlock();

        }


        var CshCustSelId = 0;
        var CshCustSelName = "";
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var m_BillNowPrefix = "";
        var m_DiscountType = "";
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var billingmode = ""
        var OrderId = "";
        var Type = "";
        var TotalItems = 0;
        //.....................................


        function BindTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlTable").html(obj.TableOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }



        function OpenBillWindow() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").hide();
            $("#dvBillWindow").show();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
        }

        function OpenProductWindow() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
        }

        function UnHoldBill(HoldNo) {
            RestControls();

            $.ajax({
                type: "POST",
                data: '{ "HoldNo": "' + HoldNo + '"}',
                url: "screen.aspx/GetByHoldNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"],obj.productLists[i]["Edit_SaleRate"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }



        function EditBill(BillNowPrefix) {


            BillNowPrefix = m_BillNowPrefix;
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                   
                    $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"],obj.productLists[i]["Edit_SaleRate"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }




        function GenerateBill(OrderNo) {
            RestControls();
            OrderId = OrderNo;

            $.ajax({
                type: "POST",
                data: '{ "OrderNo": "' + OrderNo + '"}',
                url: "screen.aspx/GetByOrderNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["ItemDiscount"],1,obj.productLists[i]["Edit_SaleRate"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


        function BackToList() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();

        }




        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }



            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }




        //...................................

        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;


        function bindGrid2() {

            var searchon = $("input[name='searchon1']:checked").val();
            var criteria = $("input[name='searchcriteria1']:checked").val();
            var stext = $("#Txtsrchcredit").val();


            jQuery("#jQGridDemoCredit").GridUnload();

            jQuery("#jQGridDemoCredit").jqGrid({
                url: 'handlers/CreditCustomerSearch.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '&cType=' + "Customer",
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['Code', 'Name', 'CSTNO', 'TINNO'],
                colModel: [
                            { name: 'CCODE', key: true, index: 'CCODE', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                            { name: 'CNAME', index: 'CNAME', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                            { name: 'CST_NO', index: 'CST_NO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                            { name: 'TINNO', index: 'TINNO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },




                ],
                rowNum: 10,
                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPagerCredit',
                sortname: 'CCODE',
                viewrecords: true,
                height: "100%",
                width: "800px",
                sortorder: 'desc',
                caption: "Customers List"


            });


            $('#jQGridDemoCredit').jqGrid('navGrid', '#jQGridDemoPagerCredit',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );

            var Datad = jQuery('#jQGridDemoCredit');
            Datad.jqGrid('setGridWidth', '240');

            $("#jQGridDemoCredit").jqGrid('setGridParam',
             {
                 onSelectRow: function (rowid, iRow, iCol, e) {

                     if (billingmode == "direct") {
                         $("#btnOk").css("display", "block");
                         $("#btnCancel").css("display", "block");
                     }
                     else {
                         $("#CustomerSearchWindow").hide();
                         $("#dvProductList").hide();
                         $("#dvBillWindow").show();
                         $("#dvCreditCustomerSearch").hide();
                         $("#dvHoldList").hide();
                         $("#dvOrderList").hide();
                         var customerId = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CCODE');
                         CrdCustSelId = customerId;
                         $("#hdnCreditCustomerId").val(customerId);
                         $("#lblCreditCustomerName").text($('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME'));
                         CrdCustSelName = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME')
                         //$('#dvCreditCustomerSearch').dialog('close');
                         $("#creditCustomer").css("display", "block");
                         $("#ddlbilltype option[value='Credit']").prop("selected", true);

                     }



                 }
             });

        }




        function bindGrid(searchon, mobile) {


            //var searchon=$("input[name='searchon']:checked").val();
            var criteria = $("input[name='searchcriteria']:checked").val();
            var stext = $("#txtSearch1").val();
            if (searchon == "M") {
                stext = mobile;

            }


            jQuery("#jQGridDemo").GridUnload();


            jQuery("#jQGridDemo").jqGrid({
                url: 'handlers/CustomersList.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '',
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['ID', 'Name', 'Address1', 'Address2', 'Area', 'City', 'State', 'DateOfBirth', 'AnniversaryDate', 'Discount', 'ContactNo', 'Tag', 'FocBill', 'Group'],
                colModel: [
                            { name: 'Customer_ID', key: true, index: 'Customer_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                            { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                            { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: false },
                             { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },

                             { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Date_Of_Birth', index: 'Date_Of_Birth', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Date_Anniversary', index: 'Date_Anniversary', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Discount', index: 'Discount', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                             { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },

                            { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                            { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                           { name: 'grpid', index: 'grpid', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                ],
                rowNum: 10,
                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'Code',
                viewrecords: true,
                height: "100%",
                width: "800px",
                sortorder: 'desc',
                caption: "Customers List"


            });


            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }



                         );

            var Datad = jQuery('#jQGridDemo');
            Datad.jqGrid('setGridWidth', '290');



            $("#jQGridDemo").jqGrid('setGridParam',
                {
                    onSelectRow: function (rowid, iRow, iCol, e) {


                        var Discount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Discount');
                        $("#lblCashCustomerName").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));

                        CshCustSelId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_ID');
                        CshCustSelName = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name');
                        //                 $("#lblCashCustomerAddress").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));
                        // $('#dvSearch').dialog('close');
                        $("#CashCustomer").css("display", "block");

                        if (EnableCustomerDiscount == 1) {
                            $("#dvdisper").val(Discount.toFixed(2));

                        }

                        $("#CustomerSearchWindow").hide();
                        $("#dvProductList").show();
                        $("#dvBillWindow").hide();
                        $("#dvHoldList").hide();
                        $("#dvOrderList").hide();
                        CommonCalculation();
                    }
                });




        }

        function RestControls() {
            $("#lblCreditCustomerName").text("");

            $("ddlCreditCustomers").hide();
            $("ddlChosseCredit").hide();
            $("ddlChosseCredit").val(0);
            BillNowPrefix1 = "";
            Type = "";
            $("#CashCustomer").css("display", "none");
            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            $("#dvdisper").val(0);
            $("#dvdiscount").val(0);
            $("#dvnetAmount").html("0");
            $("#dvRound").html("0");
            $("#dvTax").html("0");
            $("#dvVat").html("0");
            $("#dvDeliveryChrg").val("0");
            $("#dvsertaxper").html("0");
            $("#dvsbtotal").html("0");
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;


            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            $("#ddlTable option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvProductList").show();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            $("#txtCashReceived").val("0").prop("readonly", false);
            CrdCustSelId = 0;
            CshCustSelId = 0;
            CshCustSelName = "";
            CrdCustSelName = "";
            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

             $("#btnEdit").css({ "display": "none" });
             $("#btnShowScreen").css({ "display": "none" });

             $("#btnDelete").css({ "display": "none" });

             for (var i = 0; i < arrRole.length; i++) {

                 if (arrRole[i] == "9") {

                     $("#btnShowScreen").css({ "display": "block" });
                 }

                 if (arrRole[i] == "3") {

                     $("#btnEdit").css({ "display": "block" });
                 }

                 if (arrRole[i] == "2") {

                     $("#btnDelete").css({ "display": "block" });
                 }
             }

           



        }


        function InsertHoldBill() {

        if(m_BillMode!="" )
           {
           alert("Sorry, You cannot Hold Bill in Edit Mode");
           return;
           }


            var custcode = 0;
            var custName = "";
            if (CshCustSelId == "0") {
                custcode = 0;
                custName = "CASH";

            }
            else {
                custcode = CshCustSelId;
                custName = CshCustSelName;

            }
            //             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            //              if ($.trim(SelectedRow) == "") {
            //                custcode = 0;
            //                custName = "CASH"
            //              }
            //              else
            //              {
            //              custcode= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_ID');
            //              custName= $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Customer_Name');
            //              }



            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();
            var addtaxamt = $("#dvVat").html();
            var NetAmt = $("#dvnetAmount").html();

            var billmode = "0";

            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var PAmt = [];
            var Tax = [];

            var Ptax = [];
            var ItemRemarks = [];

            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {

                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Price[i] = ProductCollection[i]["Price"];
                Qty[i] = ProductCollection[i]["Qty"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];

                Tax[i] = ProductCollection[i]["TaxCode"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
            }


            $.ajax({
                type: "POST",
                data: '{ "CustomerId": "' + custcode + '","CustomerName": "' + custName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","ItemRemarksArr": "' + ItemRemarks + '"}',
                url: "screen.aspx/InsertHoldBill",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {
                        alert("Bill Holded Successfully");
                        RestControls();
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }



        function AddRemarks(counterId) {

            var DRemarks = $("#txtRemarks" + counterId).val();
            ProductCollection[counterId]["ItemRemarks"] = DRemarks;


        }




        function InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName,OnlinePayment) {

         
            var OrderNo = OrderId;
            if (OrderNo == "") {
                OrderNo = "0";
            }

            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();

            var lessdisamt = $("#dvdiscount").val();

            var addtaxamt = $("#dvVat").html();
            var RoundAmt = 0;
            var hdnNetamt = $("#hdnnetamt").val();
            RoundAmt = Number(NetAmt) - Number(hdnNetamt);

            var DeliveryCharges = $("#dvDeliveryChrg").val(); 
            var remarks = option;


            var Tableno = $("#ddlTable").val();
            if (Tableno == "") {
                Tableno = "0";
            }

            if (option == "Dine") {


                if (Tableno == "0") {
                    alert("Please choose Table Number");
                    $("#ddlTable").focus();
                    return;
                }
            }

            var setatx = $("#dvTax").html();
            var ServiceTax = $("#dvsertaxper").html();
            
            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];
            var OrgSaleRate = [];
            var PAmt = [];
            var Ptax = [];
            var PSurChrg = [];
            var ItemRemarks = [];
            var SurPer = [];
            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {
             
                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                OrgSaleRate[i] = ProductCollection[i]["Price"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                SurPer[i] = ProductCollection[i]["SurVal"];

            }
           
            TaxDen = [];
            VatAmtDen = [];
            VatDen = [];
            SurDen = [];

            $("div[name='tax']").each(
           function (y) {
               TaxDen[y] = $(this).html();
           }
           );
            $("div[name='amt']").each(
           function (z) {
               VatAmtDen[z] = $(this).html();
           }
           );
            $("div[name='vat']").each(
           function (a) {
               VatDen[a] = $(this).html();
           }
           );
            $("div[name='sur']").each(
           function (a) {
               SurDen[a] = $(this).html();
           }
           );
            var BillNowPrefix = "";

            $.uiLock();
          
            $.ajax({
                type: "POST",
                data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","Mode":"Edit"}',
                url: "screen.aspx/InsertUpdate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    BillNowPrefix = obj.BNF;

                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == -5) {
                        alert("Please Login Again and Try Again..");
                        return;
                    }

                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {
                        $("#lblAmmmt").html(NetAmt);
                        $("#dvbillSave").toggle();
                        //                             Printt(BillNowPrefix);
                        $.uiUnlock();
                        RestControls();
                        BindGrid();
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                $("#btnBillWindowOk").removeAttr('disabled');


                    $.uiUnlock();


                }

            });

        }


        var option = "";


        function Printt(celValue) {



            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");

            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";

            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);

            document.getElementById('reportout').contentWindow.location = "Reports/rptRetailBill.aspx?BillNowPrefix=" + celValue;



        }

        function GetByItemCode(ItemCode) {

            $.uiLock('');

         

            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + ItemCode + '"}',
                url: "screen.aspx/GetByItemCode",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                   

                    if (obj.productData.ItemID == 0) {
                        alert("No Item Found Corresponding to Code " + obj.productData.Item_Code);
                        $("#txtItemCode").val("").focus();

                    }
                    else {

                        addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, "", obj.productData.Discount,0,obj.productData.Edit_SaleRate)

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });

        }


        function GetDiscountType() {
            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/GetDiscountType",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    m_DiscountType = obj.Discount.DiscountType;
                    if (m_DiscountType == "DisPerAmount") {
                        $("#dvdiscount").removeAttr("disabled");
                        $("#dvdisper").removeAttr("disabled");
                    }
                    else if (m_DiscountType == "BackEndDiscount") {


                        $("#dvdiscount").prop("disabled", "disabled").val("0");
                        $("#dvdisper").prop("disabled", "disabled").val("0");
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });

        }

        function GetPluginData(Type) {

            if (Type == "CashCustomer") {

        
                var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
                $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

                CshCustSelId = $("#ddlMobSearchBox option:selected").val();
                CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");
                 
                $("#CashCustomer").css("display", "block");

                if (EnableCustomerDiscount == 1) {
                    $("#dvdisper").val(Discount.toFixed(2));

                }

                $("#CustomerSearchWindow").hide();
                $("#dvProductList").show();
                $("#dvBillWindow").hide();
                $("#dvHoldList").hide();
                $("#dvOrderList").hide();
                CommonCalculation();





            }
            else if (Type == "Accounts") {
                var customerId = $("#ddlCreditCustomers").val();
                CrdCustSelId = customerId;
                $("#hdnCreditCustomerId").val(customerId);


                $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
                CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



                $("#creditCustomer").css("display", "block");
            }
        }


        $(document).ready(
         function () {

         $("#DelCharges").hide();

             $("#ddlChosseCredit").change(
                 function () {

                    
                     if ($("#ddlChosseCredit").val() != "0") {
                       
                         if ($("#lblCreditCustomerName").text() == "") {
                            
                             CrdCustSelId = $("#ddlChosseCredit").val();
                             $("#hdnCreditCustomerId").val(CrdCustSelId);
                             CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                             $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())
                            
                             $("#creditCustomer").css("display", "block");
                         }
                     }

                   


                 });


         $(window).keydown(function (e) {

              
                switch (e.keyCode) {
                    case 112:   // F1 key is left or up

                   CashSave();
                       
                      return false;
                    case 113: //F2 key is left or down

                        ClearData();
                        
                        return false; //"return false" will avoid further events
                    case 114: //F3 key is left or down
                    ReturnItem();

                    return false;
                    case 119: //F8 key is left or down

                      CreditCardSave();

                     
                    return false;

                     case 122: //F11 key is left or down
                     BillUnhold();
                    return false;

                  case 123: //F12 key is left or down
                     InsertHoldBill();
                    return false;


                }
                return; //using "return" other attached events will execute
            });




              $("#chkDelivery").change(function () {

                if($('#chkDelivery').prop('checked') == true)

                {
                $("#DelCharges").show();
                }
                else{

                $("#DelCharges").hide();
                }

              });



          $("#btnMsgClose").click(
         function()
         {
         $("#dvbillSave").css("display", "none");
         }
         )
         ;


         $("#btnReprint").click(
         function()
         {
            
             var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
             if ($.trim(SelectedRow) == "") {
                 alert("No Bill is selected to Reprint");
                 return;
             }
             
             m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
             
         $.uiLock('');
          var PrintType="RetailBill";
   
           $.ajax({
                 type: "POST",
                 data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
                 url: "BillScreen.aspx/Reprint",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 $.uiUnlock();

                 }

             });




         
         }
         );



         $("#colDvProductList").mouseenter(
         function()
         {
     
         $("#colProducts").slideUp(200);
         }
         
         );

             $("#ddlCreditCustomers").val("").removeAttr("disabled");
             $("#ddlCreditCustomers").css({ "display": "none" });

             $("#ddlChosseCredit").css({ "display": "none" });
             $("#ddlChosseCredit").val(0);

             $.ajax({
                 type: "POST",
                 data: '{ }',
                 url: "BillScreen.aspx/GetAllBillSetting",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     BillBasicType = obj.setttingData.retail_bill;
                     TakeAwayDine = obj.setttingData.TakeAwayDine;
                     AllowServicetax = obj.setttingData.ServiceTax;
                     Takeaway = obj.setttingData.AlloServicetax_TakeAway;
                     HomeDelCharges = obj.setttingData.homedel_charges;
                     minbillvalue = obj.setttingData.min_bill_value;
                     DeliveryCharges =  obj.setttingData.del_charges;
                     EnableCashCustomer = obj.setttingData.CashCustomer;
                     DefaultBank = obj.setttingData.defaultBankID;
                     RoundBillAmount = obj.setttingData.roundamt;

                     PrintShortName = obj.setttingData.shortname;
                   
                     FocAffect = obj.setttingData.focaffect;
                     NEgativeStock = obj.setttingData.NegtiveStock;

                     DefaultPaymode = obj.setttingData.defaultpaymodeID;
                     AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;

                     if (AllowDiscountOnBilling == 1) {
                         EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                         EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                         DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                         BackEndDiscount = obj.setttingData.Back_End_Discount;

                     }
                   

                     if (EnableCashCustomer == 1) {

                         $("#btnCash").removeAttr("disabled")
                         //$("#txtMobSearchBox,#imgSrchCashCust").show();
                         $("#txtddlMobSearchBox").removeAttr("disabled");

                     }
                     else {
                         $("#btnCash").prop("disabled", true).css("background", "gray");
                         //$("#txtMobSearchBox,#imgSrchCashCust").hide();
                      
                         $("#txtddlMobSearchBox").attr("disabled", "disabled");
                     }


                     if (BillBasicType == "I") {
                       
                         $("#vatIncOrExc").hide();
                     }
                     else {
                         $("#vatIncOrExc").show();
                     }

                    $("#txtddlMobSearchBox").attr("disabled", "disabled");
                    if(HomeDelCharges == 1)
                    {
                    
                     if(minbillvalue == 0 || DeliveryCharges== 0)
                     {
                       $("#dvDeliveryChrg").removeAttr("disabled");
                     }
                     else
                     {
                      $("#dvDeliveryChrg").attr("disabled","disabled");
                     }

                    }
                    else
                    {
                      
                    }

                     if (DiscountOnBillValue == 1) {

                         DiscountValues = obj.DiscountDetail;



                     }

                     if (EnableDiscountAmount == 1) {

                         $("#dvdisper").removeAttr('disabled');
                         $("#dvdiscount").removeAttr('disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }
                     else {

                         $("#dvdisper").attr('disabled', 'disabled');
                         $("#dvdiscount").attr('disabled', 'disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {



                 }

             });



             ValidateRoles();

             function ValidateRoles() {

                 var arrRole = [];
                 arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                 for (var i = 0; i < arrRole.length; i++) {
                 
                     if(arrRole[i] == "18")
                     {
                       RoleForEditRate = true;

                        
                     }
                    else if (arrRole[i] == "9") {
                         $("#btnShowScreen").click(
                         function () {
                             RestControls();
                             m_BillNowPrefix = "";
                             m_BillMode="";
                             $("#ddlbillttype").removeAttr("disabled");
                                $("#btnCreditCard").css("background","#f06671");
                             $("#ddlCreditCustomers").html("");
                             $("#txtddlCreditCustomers").val("");

                                $("#btnCash").css("background","#f06671");
                                $("#btnhold").css("background","#f06671");

                             $("#screenDialog").dialog({
                                 autoOpen: true,

                                 width: 800,
                                 resizable: false,
                                 modal: false
                             });

                         }

                         );

                     }

                     else if (arrRole[i] == "2") {
                         $("#btnDelete").show();

                         $("#btnDelete").click(
        function () {
           
            var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
          
            if ($.trim(SelectedRow) == "") {
                alert("No Bill is selected to Cancel");
                return;
            }

            var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
            if (confirm("Are You sure to delete this record")) {
                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
                    url: "BillScreen.aspx/Delete",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        BindGrid();
                        alert("Bill is Canceled successfully.");

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $.uiUnlock();
                    }
                });

            }


        }
        );

                     }




                     else if (arrRole[i] == "3") {
                         $("#btnEdit").show();
                         $("#btnEdit").click(
       function () {
           RestControls();

           var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Product is selected to add");
               return;
           }
           CshCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code');
           CshCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Name');
           CrdCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_ID');
           CrdCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name');

             m_BillMode=$('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillMode');


         $("#btnhold").css("background","gray");
         
            
             

           if(CshCustSelId!=0)
           {

           $("#ddlMobSearchBox").html("<option value='"+CshCustSelId+"'>"+CshCustSelName+"</option>");
           $("#txtddlMobSearchBox").val(CshCustSelName);
           $("#CashCustomer").show();
           $("#lblCashCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCustAddress'));
          }
          
           
          //  $("#ddlbillttype").prop("disabled",true);

         
           if(m_BillMode=="Credit")
           {

                    $("#btnCash").css("background","gray");
                     $("#btnCreditCard").css("background","gray");

                    $("#lblCreditCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CreditCustAddress'));
          
           }
           else if (m_BillMode=="CreditCard")
           {
                    $("#btnCash").css("background","gray");
          $("#btnCreditCard").css("background","#f06671");
           
           }
           else if(m_BillMode=="Cash")
           {
           $("#btnCash").css("background","#f06671");
             $("#btnCreditCard").css("background","gray");
           }
           
           var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');
           option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');




           $("#<%=ddloption.ClientID%>").val(option);
          if (option == "Dine") {
              $("#ddlTable").removeAttr("disabled");

              $("#ddlTable option").removeAttr("selected");

              $('#ddlTable option[value=' + TableNo + ']').prop('selected', 'selected');
          }



          $("#dvdisper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));


        

          EditBill(m_BillNowPrefix);
          $("#dvsertaxper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'servalue'));

          $("#screenDialog").dialog({
              autoOpen: true,

              width: 800,
              resizable: false,
              modal: false
          });

      }

        );

  }
  else if (arrRole[i] == "10") {
      $("#btnhold").click(
function () {

    InsertHoldBill();
});

  }



}

}

             $("#ddlTable").prop("disabled", true);
             BindTables();
             GetDiscountType();

             $("#btnGetByItemCode").click(
             function () {


                 var ItemCode = $("#txtItemCode");

                 if (ItemCode.val().trim() == "") {
                     ItemCode.focus();
                     return;
                 }


                 GetByItemCode(ItemCode.val());

             }

             );




             $("#ddlMobSearchBox").supersearch({
                 Type: "CashCustomer",
                 Caption: "Please enter Customer Name/Mobile ",
                 AccountType: "D",
                 Width: 120,
                 DefaultValue: 0,
                 Godown: 0
             });



             $("#ddlCreditCustomers").supersearch({
                 Type: "Accounts",
                 Caption: "Please enter Customer Name/Code ",
                 AccountType: "D",
                 Width: 100,
                 DefaultValue: 0,
                 Godown: 0
             });




             $("#dvdisper").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         if (value > 100) {
             $(this).val(100);

         }

         CommonCalculation();

     }
     );

             $("#dvdiscount").keyup(
           function () {
               var regex = /^[0-9]*$/;


               var value = jQuery.trim($(this).val());

               if (value.length >= 1) {
                   if (!regex.test(value) || value <= 0) {

                       $(this).val(0);


                   }
               }

               var ttlAmt = $("#dvsbtotal").html();

               if (value > Number(ttlAmt)) {
                   $(this).val(ttlAmt);

               }


               $("#dvdisper").val(($("#dvdiscount").val() * 100 / $("#dvsbtotal").html()).toFixed(2));
               CommonCalculation();

           }
           );



            $("#dvDeliveryChrg").keyup(
           function () {
               var regex = /^[0-9]*$/;


               var value = jQuery.trim($(this).val());

               if (value.length >= 1) {
                   if (!regex.test(value) || value <= 0) {

                       $(this).val(0);


                   }
               }

               CommonCalculation();

           }
           );



             $("#btnCash").click(
           function () {
            $("#btnCash").attr('disabled','disabled');
         CashSave();

           });


           function CashSave()
           {
             if(m_BillMode=="Credit" || m_BillMode=="CreditCard")
           {
           //alert("Bill Mode is "+m_BillMode+" and cannot be changed to Cash.");
           //return;
           }

               var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
               var CustomerId = "CASH";
               var CustomerName = "CASH";
               var billmode = "Cash";
               var NetAmt = $("#dvnetAmount").html();
               var CreditBank = "";
               var CashAmt = $("#dvnetAmount").html();
               var creditAmt = 0;
               var CreditCardAmt = 0;
               var cashcustcode = 0;
               var cashcustName = "";
               if (CshCustSelId == "0") {
                   cashcustcode = 0;
                   cashcustName = "CASH"
               }
               else {
                   cashcustcode = CshCustSelId;
                   cashcustName = CshCustSelName;
                   CustomerId=CshCustSelId;
                   CustomerName=CshCustSelName;
               }
               var OnlinePayment = 0
               InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName,OnlinePayment);
               $("#btnCash").removeAttr('disabled');
           }

             $("#btnClear").click(
             function () {

             ClearData();
          

             });


             function ClearData()
             {
              m_BillNowPrefix = "";
                             m_BillMode="";
                             $("#ddlbillttype").removeAttr("disabled");
                                $("#btnCreditCard").css("background","#f06671");
                             $("#ddlCreditCustomers").html("");
                             $("#txtddlCreditCustomers").val("");
                             $("#ddlChosseCredit").val(0);
                                $("#btnCash").css("background","#f06671");
                                $("#btnhold").css("background","#f06671");

                 RestControls();
             }



             $("#btnOk").click(
         function () {
             var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
             var CustomerId = 0;
             var CustomerName = "";
             if (CrdCustSelId == "0") {
                 CustomerId = 0;
                 CustomerName = "";
             }
             else {
                 CustomerId = CrdCustSelId;
                 CustomerName = CrdCustSelName;
             }
             var billmode = "Credit";
             var NetAmt = $("#dvnetAmount").html();
             var CreditBank = "";
             var CashAmt = 0
             var creditAmt = $("#dvnetAmount").html();
             var CreditCardAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";


             var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "";
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }
             var OnlinePayment = 0;
             InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName,OnlinePayment);

         });



             $("#btnCredit").click(
            function () {


                if (ProductCollection.length == 0) {
                    alert("First Choose Products for billing");
                    return;
                }



                billingmode = "direct";
                $("#dvCreditCustomerSearch").show();
                $("#dvProductList").hide();

            });

             $("#btnUnHold").click(
            function () {
              

               BillUnhold();



            });

            function BillUnhold()
            {
             $.ajax({
                    type: "POST",
                    data: '{}',
                    url: "screen.aspx/GetHoldList",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        $("#tbHoldList").html(obj.holdList);

                        $("#CustomerSearchWindow").hide();
                        $("#dvProductList").hide();
                        $("#dvBillWindow").hide();

                        $("#dvCreditCustomerSearch").hide();
                        $("#dvHoldList").show();
                        $("#dvOrderList").hide();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                    }

                }
          );

            }


             $("#btnOrdersList").click(
               function () {


                   $.ajax({
                       type: "POST",
                       data: '{}',
                       url: "screen.aspx/GetOrdersList",
                       contentType: "application/json",
                       dataType: "json",
                       success: function (msg) {

                           var obj = jQuery.parseJSON(msg.d);

                           $("#tbOrderList").html(obj.OrderList);

                           $("#CustomerSearchWindow").hide();
                           $("#dvProductList").hide();
                           $("#dvBillWindow").hide();

                           $("#dvCreditCustomerSearch").hide();
                           $("#dvHoldList").hide();
                           $("#dvOrderList").show();
                           $("#btnreturn").attr('disabled', true);
                           $("#btnsavebill").attr('disabled', true);



                           $("#btnUnHold").attr('disabled', true);
                           $("#btnCash").attr('disabled', true);
                           $("#btnCredit").attr('disabled', true);


                       },
                       error: function (xhr, ajaxOptions, thrownError) {

                           var obj = jQuery.parseJSON(xhr.responseText);
                           alert(obj.Message);
                       },
                       complete: function () {
                           $("#btnhold").attr('disabled', 'disabled');
                       }

                   }
             );

               });







             $("#btnreturn").click(
                function () {


                ReturnItem();

                   
                    //count = count+1;
                }
                );



                function ReturnItem()
                {
                 if (modeRet == "Return") {
                        $(this).css("background", "#f06671");
                        modeRet = "";
                    }
                    else {

                        $(this).css("background", "red");
                        modeRet = "Return";

                    }
                }

             //      $("#txtMobSearchBox").keyup(
             //            function (event) {

             //             var keycode=(event.keyCode ? event.keyCode: event.which);
             // 
             //      if(keycode=='13')
             //      {
             //            alert("Hello");
             //      }


             //            });


             $("#dvdisper").val("0");
             $("#dvdiscount").val("0");

             $("#btnSearch").click(
           function () {

               var Keyword = $("#txtSearch");
               if (Keyword.val().trim() != "") {
                 $("#colProducts").slideDown(200);
                   Search(0, Keyword.val());
               }
               else {
                   Keyword.focus();
               }
               $("#txtSearch").val("");

           });



             $("#txtSearch").keyup(
             function (event) {

                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {


                     var Keyword = $("#txtSearch");
                     if (Keyword.val().trim() != "") {
                       $("#colProducts").slideDown(200);
                         Search(0, Keyword.val());
                         $("#txtSearch").val("");
                     }
                     else {
                         Keyword.focus();
                     }


                 }


             }

             );





             $("#txtItemCode").keyup(
             function (event) {

                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {



                     if ($(this).val().trim() != "") {

                         GetByItemCode($(this).val());
                         $(this).val("").focus();
                     }
                     else {
                         $(this).focus().val("");
                     }


                 }

             }

             );




             $("#btnBillWindowClose").click(
             function () {

                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 $("#dvBillWindow").hide();

                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();
                 // $("#dvBillWindow").dialog('close');


             }

             );

             $("#dvGetCreditCustomers,#dvGetCreditCustomersMember,#dvGetCreditCustomersPackage").click(
            function () {


                var DataGrid = jQuery('#jQGridDemo1');
                DataGrid.jqGrid('setGridWidth', '680');

                jQuery('#jQGridDemo1').GridUnload();


                $("#CustomerSearchWindow").hide();
                $("#dvProductList").hide();
                $("#dvBillWindow").hide();
                $("#dvCreditCustomerSearch").show();
                $("#dvHoldList").hide();

                $("#dvOrderList").hide();

                //            $('#dvCreditCustomerSearch').dialog(
                //        {
                //            autoOpen: false,

                //            width: 720,
                //            resizable: false,
                //            modal: false

                //        });

                // 
                //            linkObj = $(this);
                //            var dialogDiv = $('#dvCreditCustomerSearch');
                //            dialogDiv.dialog("option", "position", [233, 48]);
                //            dialogDiv.dialog('open');
                //            return false;


            });





             $("#ddlbillttype").change(
         function () {



             if ($(this).val() == "Credit") {


                 $("#dvOuter_ddlCreditCustomers").show();
                 $("#lblCashHeading").text("Receipt Amt:");
                 $("#ddlbillttype option[value='Credit']").prop("selected", true);
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);
                 $("#ddlChosseCredit").show();

             }
             else if ($(this).val() == "CreditCard") {
                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();


                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);

             }
             else if ($(this).val() == "Cash") {


                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);


             }
             else if($(this).val() == "OnlinePayment")
             {

                var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);
             }

         }
         );




             option = $("#<%=ddloption.ClientID%>").val();

            $("#<%=ddloption.ClientID%>").change(
                  function () {
                      option = $("#<%=ddloption.ClientID%>").val();
                 if (option == "TakeAway") {

                     $("#ddlTable").prop("disabled", true);
                     $("#ddlTable option").removeAttr("selected");
                 }
                 else {
                     $("#ddlTable").prop("disabled", false).focus();
                     // alert("Choose Table No");
                 }
                 Bindtr();

             }
       );




             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindCategories",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                     $("#categories").html(obj.categoryData);
                     if (AllowServicetax == 1) {
                         Sertax = obj.setttingData.SerTax;
                     }
                     else {
                         Sertax = 0;
                     }
                   //  Takeaway = obj.setttingData.TakeAway;
                     Takeawaydefault = obj.setttingData.TakeAwayDefault;

                     var CatId = obj.CategoryId;

                     Search(CatId, "");
                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
         );



             $("#btnBillWindowOk").click(
     function () {        

        $("#btnBillWindowOk").attr('disabled','disabled');

         var billtype = $("#ddlbillttype").val();
         if (billtype == "") {
             alert("Select BillType First");
             $("#ddlbillttype").focus();
             return;

         }


         if (billtype == "Credit") {

             if (CrdCustSelId == 0) {

                 alert("Please select Credit Customer");
                 $("#txtddlCreditCustomers").focus();
                  $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }


         }


         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');




         DEVBalanceCalculation();



         var txtcreditcardcheck = $("#Text13").val();

         if (txtcreditcardcheck != "0") {
             if ($("#ddlType").val() == "") {
                 $.uiUnlock();
                 alert("Please Select Credit Card Type");
                 $("#ddlType").focus();
                  $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             //if ($("#Text14").val() == "") {
             //    $.uiUnlock();
             //    alert("Please Enter Credit Card No");
             //    $("#Text14").focus();
             //    return;
             //}

         }

         var txtchequecheck = $("#Text15").val();
         if (txtchequecheck != "0") {
             if ($("#ddlBank").val() == "") {
                 $.uiUnlock();
                 alert("Please Select Bank");
                 $("#ddlBank").focus();
                  $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             if ($("#Text16").val() == "") {
                 $.uiUnlock();
                 alert("Please Enter Cheque No");
                 $("#Text16").focus();
                  $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
         }


         var cashamount = $("#txtCashReceived").val();


         if (billtype == "Cash" || billtype == "CreditCard") {
             if (Number($("#txtBalanceReturn").val()) < 0) {
                 $.uiUnlock();
                 alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                  $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             else {
                 cashamount = cashamount - Number($("#txtBalanceReturn").val());
             }

         }

         if (Number(cashamount) < 0) {
             $.uiUnlock();
             alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
              $("#btnBillWindowOk").removeAttr('disabled');
             return;
         }

        
         if (billtype == "CreditCard") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = $("#ddlBank option:selected").text();
             var CustomerName = $("#ddlBank option:selected").text();
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt = 0
             var creditAmt = 0;
             var OnlinePayment = 0;
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }
         if (billtype == "Cash") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt = $("#txtCashReceived").val();
             var creditAmt = 0;
             var OnlinePayment = 0;
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }


           if (billtype == "OnlinePayment") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt =0;
             var creditAmt = 0;
             var OnlinePayment = $("#txtFinalBillAmount").val();
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }
         else if (billtype == "Credit") {

             var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
             var CustomerId = 0;
             var CustomerName = "";
             if (CrdCustSelId == "0") {
                 CustomerId = 0;
                 CustomerName = "";
             }
             else {
                 CustomerId = CrdCustSelId;
                 CustomerName = CrdCustSelName;
             }

             var NetAmt = $("#txtFinalBillAmount").val();

             var billmode = billtype;

             var CreditBank = $("#ddlBank").val();
             var OnlinePayment = 0;
             var CashAmt = $("#txtCashReceived").val();
             
             var BIllValue = $("#dvsbtotal").html();
             var creditAmt = 0;
             if (billmode == "Credit") {
                 creditAmt = Number(BIllValue) - Number(CashAmt);


             }

             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";


             var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "";
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }

         InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName,OnlinePayment);


     }
     );







             $("#txtCashReceived").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }


         DEVBalanceCalculation();

     }
     );



             $("#Text13").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }


                 DEVBalanceCalculation();

             }
             );
             $("#Text15").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }

                 DEVBalanceCalculation();

             }
             );

             function ResetBillCntrols() {

                 $("#txtFinalBillAmount").val("");
                 $("#ddlbillttype").val("");
                 $("#txtCashReceived").val("0");
                 $("#Text13").val("0");
                 $("#Text15").val("0");
                 $("#Text14").val("");
                 $("#ddlType").val("");
                 $("#ddlBank").val("");

                 $("#Text16").val("");
                 $("#txtBalanceReturn").val("0");
                 modeRet = "";
                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 $("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();

                 
                
                 $("#hdnCreditCustomerId").val("0");
                 $("#ddlbillttype option[value='" + DefaultPaymode + "']").prop("selected", true);
                 $("#creditCustomer").css("display", "none");
              

                 if(m_BillNowPrefix!="")
                 {

               if (m_BillMode == "Credit") {


                   $("#dvOuter_ddlCreditCustomers").show();
                   $("#ddlChosseCredit").show();

                 $("#lblCashHeading").text("Receipt Amt:");
                 $("#ddlbillttype option[value='Credit']").prop("selected", true);
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);

             }
             else if (m_BillMode== "CreditCard") {
                  
                 
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);

             }
             else if (m_BillMode== "Cash"){


                
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);


             }
             }
              
             else
             {


               if (DefaultPaymode == "Credit") {


                   $("#dvOuter_ddlCreditCustomers").show();
                   $("#ddlChosseCredit").show();

                 $("#lblCashHeading").text("Receipt Amt:");
                 $("#ddlbillttype option[value='Credit']").prop("selected", true);
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);

             }
             else if (DefaultPaymode== "CreditCard") {
                  
                 
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();




                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);

             }
             else if (DefaultPaymode== "Cash"){


                
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);


             }



             }







                 
                  if(m_BillNowPrefix!="")
                  {
                  $("#ddlbillttype option[value='" + m_BillMode + "']").prop("selected", true);
                  }
                 if(m_BillMode=="Credit")
                 {

                   $("#ddlCreditCustomers").html("<option value='"+CrdCustSelId+"'>"+CrdCustSelName+"</option>");
                   $("#txtddlCreditCustomers").val(CrdCustSelName);
                   $("#creditCustomer").show();
                 
                   if ($("#ddlChosseCredit").val() != "0")
                   {
                       CrdCustSelId = $("#ddlChosseCredit").val();
                       CrdCustSelName =$("#ddlChosseCredit option:selected").text();
                   }


                 }
               

             }




                   $("#btnCreditCardBilling").click(
             function()
             {

           
           
               var CustomerId = $("#ddlCreditCardBank").val()
               var CustomerName = $("#ddlCreditCardBank option:selected").text();
               var billmode = "CreditCard";
               var NetAmt = $("#dvnetAmount").html();
               var CreditBank = $("#ddlCreditCardBank").val()
               var CashAmt = 0;
               var creditAmt = 0;
               var CreditCardAmt = $("#dvnetAmount").html();
               var cashcustcode = 0;
               var cashcustName = "";
               if (CshCustSelId == "0") {
                   cashcustcode = 0;
                   cashcustName = "CASH";
               }
               else {
                   cashcustcode = CshCustSelId;
                   cashcustName = CshCustSelName;
               }
              var OnlinePayment = 0;
               InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName,OnlinePayment);
               $("#dvCreditCardWindow").dialog('close');
        




             });

             $("#btnCreditCard").click(
             function()
             {
               $("#btnCreditCard").attr('disabled','disabled');             
             CreditCardSave();

            }
             );



             function CreditCardSave()
             {
                           if(m_BillMode=="Credit" || m_BillMode=="Cash")
           {
              $("#btnCreditCard").removeAttr('disabled');
          // alert("Bill Mode is "+m_BillMode+" and cannot be changed to Credit Card.");
          // return;
           }
       
             $("#dvCreditCardWindow").dialog({
             
                    
                             
                             modal: true 
             
             });
             
               $.ajax({
                     type: "POST",
                     data: '{}',
                     url: "screen.aspx/BindBanks",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);
                         $("#ddlCreditCardBank").html(obj.BankOptions);
                         $("#ddlCreditCardBank option[value='" + DefaultBank + "']").prop("selected", true);
                     }

                     
                 });
             }



             $("#btnsavebill").click(
             function () {

                 if (ProductCollection.length == 0) {
                     alert("Please first Select ProductsFor Billing");

                     return;
                 }



                 ResetBillCntrols();
                


                 $("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly", true);


                 $.ajax({
                     type: "POST",
                     data: '{}',
                     url: "screen.aspx/BindBanks",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);
                         $("#ddlBank").html(obj.BankOptions);
                         $("#ddlBank option[value='" + DefaultBank + "']").prop("selected", true);
                     }


                 });


                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").hide();
                 $("#dvBillWindow").show();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();

                 //             $('#dvBillWindow').dialog(
                 //          {
                 //            autoOpen: false,
                 //                closeOnEscape: false,
                 //                draggable:false,
                 //            width:570,
                 //            resizable: false,
                 //            modal: false,
                 //         
                 //            
                 //        });
                 //        
                 //             linkObj = $(this);
                 //            var dialogDiv = $('#dvBillWindow');
                 //            dialogDiv.dialog("option", "position", [180,200]);
                 //            dialogDiv.dialog('open');
                 //         
                 //            return false;

             });




             $("#btncreditsrch").click(
       function () {
           bindGrid2();
       }
       );




             $("#btnCustomer").click(
         function () {

             if (ProductCollection.length == 0) {
                 alert("Please first Select ProductsFor Billing");

                 return;
             }
             else {
                 $("#CustomerSearchWindow").show();
                 $("#dvProductList").hide();
                 $("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();
                 bindGrid("M", $("#txtMobSearchBox").val());

             }



             //             $('#dvSearch').dialog(
             //            {
             //            autoOpen: false,

             //            width:720,
             //     
             //          
             //            resizable: false,
             //            modal: true,
             //                  
             //            });
             //            linkObj = $(this);
             //            var dialogDiv = $('#dvSearch');
             //            dialogDiv.dialog("option", "position", [238, 36]);
             //            dialogDiv.dialog('open');
             //            return false;



         });


             $(document).on("click", "#dvClose", function (event) {

                 var RowIndex = Number($(this).closest('tr').index());
                 var tr = $(this).closest("tr");
                 tr.remove();

                 ProductCollection.splice(RowIndex, 1);

                 if (ProductCollection.length == 0) {

                     //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");


                     RestControls();



                 }
                 Bindtr();

             });





             $(document).on("click", "a[name='categories']", function (event) {

                 $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
                 $(this).addClass("ancSelected").removeClass("ancBasic");



             });






             $("#btnSearch1").click(
         function () {

             bindGrid("N", "");

         }
         );

         });

    var m_ItemId = 0;
    var m_ItemCode = "";
    var m_ItemName = "";
    var m_Qty = 0;
    var m_Price = 0;
    var m_TaxRate = 0;
    var m_Surval = 0;
    var m_ItemDiscount = 0;
    var m_EditSaleRate = 0;

    var ProductCollection = [];
    function clsproduct() {
        this.ItemId = 0;
        this.ItemCode = "";
        this.ItemName = "";
        this.Qty = 0;
        this.Price = 0;
        this.TaxCode = 0;
        this.SurVal = 0;
        this.ProductAmt = 0;
        this.Producttax = 0;
        this.ProductSurchrg = 0;
        this.TaxId = 0;
        this.ItemRemarks = "";
        this.ItemDiscount = 0;
        this.EditSaleRate = false;
    }

    function Bindtr() {

        DiscountAmt = 0;
        VatAmt = 0;
        Total = 0;
        var fPrice = 0;
        TotalItems = 0;
        $("div[name='vat']").html("0.00");
        $("div[name='amt']").html("0.00");
        $("div[name='sur']").html("0.00");

        $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        var counterId = 0;
        BEndDiscountAmt = 0;
        var Count = 0;
        for (var i = 0; i < ProductCollection.length; i++) {

        if(RoleForEditRate == true)
        {
         var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

        }
        else
        {
         if(ProductCollection[i]["EditSaleRate"] == false)
           {
          
            var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
            }
            else{
            var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
            }

        }
            counterId = counterId + 1;
                   
            Count = Count + 1;
            $("#tbProductInfo").append(tr);

            fPrice = 0;
            fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
           
            if (BillBasicType == "I") {
           

                if(ProductCollection[i]["SurVal"] == "0")
                {
                  var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                }
                else
                {
                   
                    var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
                    var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"]) + Survalll));

                }
            }
            else {

                var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);
               
               
            }

              
            var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
            var tottax = Number(TAx) +  Number(surchrg);
           
          
            var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);

            VatAmt = VatAmt + tottax;


            Total = Total + fPrice;
            ProductCollection[i]["ProductAmt"] = fPrice;
            ProductCollection[i]["Producttax"] = TAx;
            ProductCollection[i]["ProductSurchrg"] = surchrg;
          
          
            var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
            var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
            var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
            $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
            $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
            $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


            BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
            TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
           
            
        }
       
      
        CommonCalculation();

        if (ProductCollection.length == 0) {
            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
        }
        
        $("#CustomerSearchWindow").hide();
        $("#dvProductList").show();
        $("#dvBillWindow").hide();
        $("#dvCreditCustomerSearch").hide();
        $("#dvHoldList").hide();
        $("#dvOrderList").hide();

        
    }


    function CommonCalculation() {
       
        var m_Total = Total.toFixed(2);
        if(HomeDelCharges == 1)
        { if(minbillvalue != 0)
          {
          if(m_Total > minbillvalue)
          {
           $("#dvDeliveryChrg").val(0)

          }
          else{
           $("#dvDeliveryChrg").val(DeliveryCharges)


          }
          }
        }
        $("div[id='dvsbtotal']").html(m_Total);



        $("div[id='dvVat']").html(VatAmt.toFixed(2));
        TaxAmt = GetServiceTax();

       
        if(BillBasicType == "E") 
        {
        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
        $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
        }
        else
        {
        
            if ($("#<%=ddloption.ClientID %>").val() == "Dine")
            {
               
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             
            }
            else if($("#<%=ddloption.ClientID %>").val() == "TakeAway")
            {
              if(Takeaway == 1)
             { 
              $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }
             else
             {
               $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }

              
            }
     
        }
        var BEndDiscountPer = 0;


        if (BackEndDiscount == 1) {



            BEndDiscountPer = BEndDiscountAmt * 100 / m_Total;

            $("#dvdisper").val(BEndDiscountPer.toFixed(2));
        }


        if (DiscountOnBillValue == 1) {

            for (var i = 0; i < DiscountValues.length; i++) {

                var StartValue = DiscountValues[i]["StartValue"];
                var EndValue = DiscountValues[i]["EndValue"];
                var DisPer = DiscountValues[i]["DisPer"];


                if (parseFloat(m_Total) >= parseFloat(StartValue) && parseFloat(m_Total) <= parseFloat(EndValue)) {

                    $("#dvdisper").val(DisPer.toFixed(2));
                    break;

                }
                else {
                    $("#dvdisper").val(0);

                }



            }
        }



        DiscountAmt = (Number(Total) * Number($("#dvdisper").val())) / 100;

         
        $("#dvdiscount").val(DiscountAmt.toFixed(2));
        

        if (RoundBillAmount == 1)
        {
            if (BillBasicType == "E") {
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
               
            }
            else
            {

                if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }
                else if($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
                    
                    if(Takeaway ==1)
                    {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) +Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else{
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                }
               

            }
        }
        else
        {
            if (BillBasicType == "E") {
                $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
            else
            {
                if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) +Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
               } else if($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

                   if(Takeaway == 1)
                   {
                     $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) +Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                   }else{
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
               }


            }


        }


        if (BillBasicType == "E") {

            $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

         }
        else {

            if ($("#<%=ddloption.ClientID %>").val() == "Dine")
           {
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
           }
           else if($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
              
              if(Takeaway ==1)
              {
              $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
              }else{
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
            }

        }

        
        var netamount = $("div[id='dvnetAmount']").html();
        var hdnNetamt = $("#hdnnetamt").val();
        var Round = Number(netamount) - Number(hdnNetamt)

    

        $("div[id='dvRound']").html(Round.toFixed(2));
       

        $("#lblNoItems").html(TotalItems);
      
        //alert(Total);
        //alert(TaxAmt);
        //alert(VatAmt);
        //alert(DiscountAmt);

        //alert($("#hdnnetamt").val());
        //alert($("div[id='dvnetAmount']").html());


    }

    function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount,B_Qty,EditSaleRate) {

    
        m_ItemId = ProductId;
        m_ItemCode = Code;
        m_Price = Price;
        m_ItemName = Name;
        m_EditSaleRate = EditSaleRate;

        if (B_Qty == 0) {
            if (modeRet == "Return") {
                m_Qty = -1;
            }
            else {
                m_Qty = 1;
            }
        }
        else {
            m_Qty = B_Qty;
        }
        m_TaxRate = TaxCode;
        m_Surval = SurVal;
        m_Tax_Id = Tax_Id;
        m_ItemDiscount = Discount;

        //         var item = $.grep(ProductCollection, function (item) {
        //             return item.ItemId == m_ItemId;
        //         });

        //         if (item.length) {

        //               for (var i = 0; i < ProductCollection.length; i++)
        //               {
        //                   if(ProductCollection[i]["ItemId"] == m_ItemId)
        //                   {
        //                       var qty = ProductCollection[i]["Qty"];
        //                       qty = qty+1;
        //                       ProductCollection[i]["Qty"] = qty ;
        //                       Bindtr();
        //                       return;
        //                   }
        //               }

        //         }


        TO = new clsproduct();

        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;      
        ProductCollection.push(TO);
       
        Bindtr();
      


    }


    function GetServiceTax() {


        var TaxAmt = 0;


        $("#dvsertaxper").html(Sertax);

     
        if (option == "TakeAway") {
            if (Takeaway == "1") {
           
                TaxAmt = (Number(Total) * Number(Sertax)) / 100;
                $("#dvTax").html(TaxAmt.toFixed(2));


            }
            else {
                $("#dvsertaxper").html("0");
                TaxAmt == "0";
                $("#dvTax").html(TaxAmt.toFixed(2));
            }
        }
        else if (option == "Dine") {
            TaxAmt = (Number(Total) * Number(Sertax)) / 100;
            $("#dvTax").html(TaxAmt.toFixed(2));


        }


        return TaxAmt;

    }


    function Search(CatId, Keyword) {



        $.ajax({
            type: "POST",
            data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
            url: "screen.aspx/AdvancedSearch",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                $("#products").html(obj.productData);

              

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }

        }
       );


    }

    //........................................

	
	$(document).on("change", "#txtBillQty", function (event) {


        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
         

			
			var fQty=$(this).val();
		 if(isNaN(fQty))
		 {
		 fQty=1;
		 
		 }
        ProductCollection[RowIndex]["Qty"] =fQty;

        Bindtr();

    });


    $(document).on("change", "#txtBillPrice", function (event) {

        
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
         

			
			var fPrice=$(this).val();
		
        ProductCollection[RowIndex]["Price"] =fPrice;

        Bindtr();

    });

	
	
    $(document).on("click", "#btnPlus", function (event) {


        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
        var fQty = 0;
        if (Qty < 0) {
            fQty = Number(Qty) + (-1);
        }
        else {
            fQty = Number(Qty) + 1;
        }




        ProductCollection[RowIndex]["Qty"] = fQty;

        Bindtr();

    });


    $(document).on("click", "#btnMinus", function (event) {

        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];
        var Mode = "Minus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];

        var fQty = 0;
        if (Qty < 0) {
            fQty = Number(Qty) - (-1);
        }
        else {
            fQty = Number(Qty) - 1;
        }

        ProductCollection[RowIndex]["Qty"] = fQty;
        if (fQty == "0") {
            ProductCollection.splice(RowIndex, 1);
        }

        Bindtr();


    });



    //............................................





    </script>
    
 <div id="dvAddOn"></div>
<div class="right_col" role="main">
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Bills List</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group" margin-bottom:10px">

                                 <div class="form-group">
                                
                                <table>
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>

                <table id="jQGridDemoBill">
                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                </div>


                                   
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table>
                                                    <tr><td><div class="btn btn-primary" style="display: block;" id="btnShowScreen">
                                           <i class="fa fa-external-link"></i> New</div></td>
                                           <td><div class="btn btn-success" style="display:none;" id="btnEdit">
                                           <i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                           
                                           <td><div class="btn btn-danger" style="display: block;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> Reprint</div></td>
                                            <td >
                                        <div id="btnDelete" style="display:none;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i>Cancel Bill</div>
                                    </td>
                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     








                    
                    <script type="text/javascript">


                        $(document).ready(function () {


                            if ($("#ddlbillttype").val() == "Cash") {
                                var customerId = 0;
                                CrdCustSelId = 0;
                                CrdCustSelName = "";
                                $("#hdnCreditCustomerId").val(0);
                                $("#lblCreditCustomerName").text("");
                                CrdCustSelName = "";
                                $("#ddlCreditCustomers").html("");
                                $("#txtddlCreditCustomers").val("");

                                $("#lblCashHeading").text("Cash Rec:");
                                $("#creditCustomer").css("display", "none");

                                $("#dvOuter_ddlCreditCustomers").hide();

                                $("#ddlChosseCredit").hide();
                                $("#ddlChosseCredit").val(0);

                                $("#txtCashReceived").val("0").prop("readonly", false);
                                $("#Text13").val("0").prop("readonly", false);
                                $("#Text14").val("").prop("readonly", false);
                                $("#Text15").val("0").prop("readonly", false);
                                $("#Text16").val("").prop("readonly", false);
                                $("#ddlType").prop("disabled", false);
                                $("#ddlBank").prop("disabled", false);


                            }


                            $("#txtDateFrom,#txtDateTo").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();



                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                      );




                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


<div id="dvbillSave" style="float: left; display:none; position:absolute;z-index:1000;left:340px;width:300px;bottom:240px;background-color: #293C52;
                                                                color: white; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                                <table style="width:100%;">
                                                                    <tr style="text-align:center">
                                                                    <td style="text-align:center">BILL SAVED SUCCESSFULLY</td>
                                                                        
                                                                    </tr>
                                                                    <tr><td style="text-align:center;font-size:20px">AMOUNTED Rs.<asp:Label ID ="lblAmmmt" runat="server" ClientIDMode ="Static"></asp:Label></td></tr>
                                                                    <tr><td>
                                                                    <div class="button1" style="background-color:White;color:Black;width:60px;box-shadow: -2px -5px 5px transparent;margin-left:41%;height:40px;"  id="btnMsgClose">
                                    OK</div>
                                                                   </td></tr>
                                                                    
                                                                  
                                                                </table>
                                                            </div>


<div id="screenDialog" style="background: url('images/nikbg2.jpg');display:none">

    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
    <div class="container">
        <div class="row">
            <div class="nav1" style="padding-top:0px;margin-top:-5px;display:none"> 
                <ul id="categories">
                </ul>
            </div>
        </div>
        <div class="row">

         <div class="col-xs-12">

                
                <div class="Search" style="padding-left:200px">
                    <div class="input-group">
                        <table width="100%">
                            <tr>
                                <td style="width: 100px">
                                    <input type="text" class="form-control" placeholder="Enter Code" aria-describedby="basic-addon2"
                                        id="txtItemCode" style="padding: 10px 10px; width: 100%; border: 0px; height: 50px" />
                                </td>
                                <td>
                                    <span id="btnGetByItemCode" value="Search" >
                                        <img src="images/plusicon.png" alt="" style="width: 50px; padding-left: 10px" /></span>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Search By Name" aria-describedby="basic-addon2"
                                        id="txtSearch" style="padding: 10px 10px; width: 100%; border: 0px; height: 50px" />
                                </td>
                                <td>
                                    <span   id="btnSearch" value="Search">
                                        <img src="images/search-button.png" alt="" style="width: 50px; padding-left: 10px" /></span>
                                </td>
                                <td style="padding-left:50px">
                                 <input type="checkbox" data-index="27" style="font-size:25px" id="chkDelivery" /><label style="color:White;font-size:18px">Delivery Charges</label>
                                </td>
                                 
                            </tr>
                        </table>
                    </div>
                </div>
                
                </div>
                

                <div class="col-xs-12" style="display:none;margin-bottom:5px" id="colProducts">
                <div id="products" style="overflow-y: scroll; height: 190px; background: #2A1003">
                </div>
                    </div>






            <div class="col-xs-12" style="padding-left: 0px" id="colDvProductList">
                <div id="dvProductList" style="display: block;margin-top:-10px">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 200px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Qty
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                
                                  <th style="width: 200px;text-align:center;color: #FFFFFF;display:none">
                                    Add-On
                                </th>


                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    <div class="cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="txtFinalBillAmount" />
                                </td>
                                <td>
                                    Bill Type:
                                </td>
                                <td>
                                    <select id="ddlbillttype" style="height: 27px; width: 120px; padding-left: 0px" class="form-control">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>
                                <td>

                                  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                             <option value="0"></option>
                                             </select>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="txtCashReceived" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                    Type:
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 25px; width: 90px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" style="height: 25px; width: 90px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 64px">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" checked="checked" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan = "100%">
                             <table>
                             <td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </table>
                                              
                                           
                                           
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Table
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <div class="cate2" style="padding:0px;margin-top:0px">
                    <table id="tbamountinfo" style="width: 100%; text-align: right; background: #F5FFFA;padding:0px;font-size:17px">
                        <tr>
                            <td valign="top" style="padding-top:10px">
							<asp:Literal id="ltDateTime" runat="Server"/>

                                   <div style="width:200px;margin-top:5px">
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2">
                                                    <tr>
                                                        <td>
                                                               <label style="font-weight: bold;color:green">
                                                               CASH CUSTOMER</label>
                                                               <img src="images/close-icon.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' />
                                                     
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="lblCashCustomerName" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                                   </div>
                                <div>
                                <table>
                                     <tr><td>No Of Items: </td><td colspan="100%">
                            
                              <div style="text-align:justify;width:60%;margin:0px 0px 8px;border-left:0px;border-top:0px;padding:5px">
                                  <asp:Label ID ="lblNoItems"  runat="server" ClientIDMode ="Static"></asp:Label>
                                  </div>
                            
                                           </td>
                        </tr>

                                </table>

                                </div>

                            </td>

                            
                            <td valign="top" align="right" style="padding-right:10px" >
                                <div style="border:solid 1px silver;box-shadow:2px 2px 3px silver;width:60%;margin:0px 0px 8px;border-left:0px;border-top:0px;padding:5px">

                                
                                <table  id="myCustomTable" style="font-size:17px">
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: right" text>
                                            Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;" colspan="100%">
                                            <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: right">
                                            Discount:
                                        </td>
                                        <td colspan="100%">
                                            <table>
                                                <tr>

<td style="width: 30px; text-align: right;">
                                        <input type="text" class="form-control input-small" style="width:45px;height:25px" 
                                                    id="dvdisper" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="3">
                                           <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvdiscount" value ="0" />
                                        </td>
                                                </tr>

                                            </table>

                                        </td>


                                        
                                    </tr>
                                    <tr>
                                        <td style="width: 200px;padding-right: 10px;height: 25px;   font-weight: bold; text-align: right">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: right;"  colspan="3">
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="50%">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="vatIncOrExc">
                                        <td style="padding-right: 10px;height: 25px;      font-weight: bold; text-align: right">
                                            Vat Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges">
                                        <td style="padding-right: 10px;height: 25px;      font-weight: bold; text-align: right">
                                            DeliveryCharges:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvDeliveryChrg" value ="0" />
                                            
                                            </div>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: right">
                                            Round Off:
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="100%">
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: right">
                                            Net Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="100%">
                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                                    </td>

                           
                        </tr>
                     
                    </table>
                    <table>
                        <tr>
                            <td colspan="100%" align="right" style="padding-right:10px">
                                <table>
                                    <tbody>
                                        <tr>  <td><b>Choose Service</b></td>
                                               <td><b><asp:DropDownList ID="ddloption" runat="server" 
                                                    Style="width: 86px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList></b>  <b><select id="ddlTable" style="height: 25px; width: 86px" class="form-control"></b>
                                            </td>
                                            
                                              </tr>
                                        <tr>
                                        
                                        <td><b>CHOOSE CASH CUSTOMER </b></td>
                                            <td style="padding-left:5px">


                                                <select id="ddlMobSearchBox">

                                                </select>


                                                <input type="text" placeholder="Mobile Number" style="display:none;border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox">

                                                 <div id="btnCustomer" style="display:none;">
                                                    <img id="imgSrchCashCust" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                            
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                        <td rowspan="100%" style="width:400px">
                        <img src="images/logo.png" style="width:250px" />
                        </td>
                            <td>
                                <div class="button1" id="btnreturn">
                                    (F3) Return</div>
                            </td>
                            <td>
                                <div class="button1" id="btnsavebill">
                                    Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnhold">
                                    (F12) Hold</div>
                            </td>

                             <td rowspan = "2">
                               <div class="button1" id="btnClear"  style="height:105px;vertical-align:middle">
                                    (F2) Refresh Bill Screen</div>
                            </td>


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>


                        </tr>
                        <tr>
                            <td>
                                <div class="button1" id="btnUnHold">
                                    (F11) UnHold</div>
                            </td>
                            <td>
                                <button  class="button1" id="btnCash">(F1) Cash</button>
                               
                            </td>
                            <td>

                             <div id="dvCreditCardWindow" style="display:none">
                                    <table>
                                    <tr><td>Bank:</td><td><select id="ddlCreditCardBank"></select></td>
                                    <td>
                                    
                                    <div  class="btn btn-primary" id="btnCreditCardBilling"    >
                                    Create Bill
                                     </div> 
                                    </td>
                                    
                                    </tr>
                                  

                                    </table>
                                    
                                    </div>
                             
                              <div class="button1" id="btnCreditCard"  >
                                    (F8) Credit Card</div> 

                                 <div class="button1" id="btnCredit" style="background:gray;display:none" disabled>
                                    (F8) Credit</div> 
                            </td>
                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>
    </div>

</div>

            
    </form>

     
  <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          jQuery("#jQGridDemoBill").GridUnload();

          jQuery("#jQGridDemoBill").jqGrid({
              url: 'handlers/ManageBills.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer','CashCustAddr','CreditCustAddr'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },

                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },

              ],

              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'BillNowPrefix',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Bills List",




          });

          var $grid = $("#jQGridDemoBill");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


              $("#btnEdit").css({ "display": "none" });
              $("#btnDelete").css({ "display": "none" });
                $("#btnShowScreen").css({ "display": "none" });


                for (var i = 0; i < arrRole.length; i++) {

                    if (arrRole[i] == 9) {

                        $("#btnShowScreen").css({ "display": "block" });
                    }
                    if (arrRole[i] == 2) {

                        $("#btnDelete").css({ "display": "block" });
                    }
                    if (arrRole[i] == 3) {
                        $("#btnEdit").css({ "display": "block" });
                        m_BillNowPrefix = 0;
                        m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

                    }
                }




             


              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
            //  $("#txtddlMobSearchBox").val(CashCustName);

            //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

          }
      });





          $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
                  {
                      refresh: false,
                      edit: false,
                      add: false,
                      del: false,
                      search: false,
                      searchtext: "Search",
                      addtext: "Add",
                  },

                  {//SEARCH
                      closeOnEscape: true

                  }

                    );


          var DataGrid = jQuery('#jQGridDemoBill');
          DataGrid.jqGrid('setGridWidth', '700');


      }
  </script>
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
</asp:Content>

