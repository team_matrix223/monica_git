﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="PaymentReceipt.aspx.cs" Inherits="PaymentReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

    <form id="form1" runat="server">
     <asp:HiddenField ID="hdnDate" runat="server"/>
           <asp:HiddenField ID="hdnRoles" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
<%-- <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-glyphicons.css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
   
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

           <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>

     <link href="css/css.css" rel="stylesheet" />
    <style type="text/css">
        .form-control
        {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }
        
        .ui-widget-content a
        {
            color:White;
            text-decoration:none;
            }
        #tbProductInfo tr
        {
            border-bottom: dotted 1px silver;
        }
        
        #tbProductInfo tr td
        {
            padding: 3px;
        }
        

        #tboption tr
        {
            border-bottom: solid 1px black;
        }
        
        #tboption tr td
        {
            padding: 10px;
        }

    </style>

    <script type ="text/javascript">

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }



        function PaymentReceipt(CustomerId) {
           
           
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $("#txtoutstanding").val("");
            $.ajax({
                type: "POST",
                data: '{"CustomerId":"' + CustomerId + '"}',
                url: "PaymentReceipt.aspx/GetOutstandingForReceiptMaster",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#txtoutstanding").val(obj.ClaimQuantity);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }

            });
        }


        function ResetControls() {
            $("#txtoutstanding").val("");
            $("#txtreceived").val("");
            $("#Text6").val("");
            $("#Text8").val("");
            $("#<%=ddlBank.ClientID %>").val("0");
            $("#ddlpaymode").val("CASH");
            $("#txtBillNo").val("Auto");
            $("#hdnbillnowprefix").val("");
            $("#<%=ddlCust.ClientID %>").val("");

            $("#btnEdit").css({ "display": "none" });
            $("#btnShowScreen").css({ "display": "none" });

          

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "1") {

                    $("#btnShowScreen").css({ "display": "block" });
                }

                if (arrRole[i] == "3") {

                    $("#btnEdit").css({ "display": "block" });
                }

              
            }
            PayMode();
        }



        $(document).ready(
        function () {


            ValidateRoles();

            function ValidateRoles() {

                var arrRole = [];
                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                for (var i = 0; i < arrRole.length; i++) {
                    if (arrRole[i] == "1") {
                        $("#btnShowScreen").show();
                        $("#btnShowScreen").click(
                         function () {

                             $("#packageDialog").dialog({
                                 autoOpen: true,

                                 width: 600,
                                 resizable: false,
                                 modal: false
                             });

                             PayMode();
                             //change the title of the dialgo
                             linkObj = $(this);
                             var dialogDiv = $('#packageDialog');
                             dialogDiv.dialog("option", "position", [300, 120]);
                             dialogDiv.dialog('open');
                             return false;

                         }

                         );
                    }
                    else if (arrRole[i] == "3") {

                        $("#btnEdit").show();

                        $("#btnEdit").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemo2').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Receipt is selected");
                return;
            }

            var ReceiptNO
            ReceiptNO = $('#jQGridDemo2').jqGrid('getCell', SelectedRow, 'Receipt_No')

            $("hdnbillnowprefix").val(ReceiptNO);




            $.ajax({
                type: "POST",
                data: '{"ReceiptNowprefix":"' + ReceiptNO + '"}',
                url: "PaymentReceipt.aspx/GetById",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#hdnbillnowprefix").val(obj.BillMasterData.Receipt_No);
                    $("#txtBillNo").val(obj.BillMasterData.Receipt_No)

                    $("#txtBillDate").val(obj.BillMasterData.strChequedate);
                    $("#<%=ddlCust.ClientID %>").val(obj.BillMasterData.CCode);
                    $("#ddlpaymode").val(obj.BillMasterData.ModeOfPayment);
                    $("#txtreceived").val(obj.BillMasterData.Amount);

                    if (obj.BillMasterData.ModeOfPayment == "CREDITCARD") {
                        $("#Text8").val(obj.BillMasterData.CHQNO);
                        $("#<%=ddlcreditbank.ClientID %>").val(obj.BillMasterData.Bank_Name);

                        $("#Text6").val("");
                        $("#<%=ddlBank.ClientID %>").val(0);

                    }
                    else if (obj.BillMasterData.ModeOfPayment == "CHEQUE") {

                        $("#Text8").val("");
                        $("#<%=ddlcreditbank.ClientID %>").val(0);
                        $("#Text6").val(obj.BillMasterData.CHQNO);
                        $("#<%=ddlBank.ClientID %>").val(obj.BillMasterData.Bank_Name);
                    }
                    else if (obj.BillMasterData.ModeOfPayment == "CASH") {

                        $("#Text8").val("");
                        $("#<%=ddlcreditbank.ClientID %>").val(0);
                        $("#Text6").val("");
                        $("#<%=ddlBank.ClientID %>").val(0);
                    }






                    $("#txtoutstanding").val(obj.BillMasterData.OutstandingAmount);





                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    PaymentReceipt($("#<%=ddlCust.ClientID %>").val());
                    $('#packageDialog').dialog(
        {
            autoOpen: false,

            width: 600,
            resizable: false,
            modal: false

        });

                    PayMode();
                    //change the title of the dialgo
                    linkObj = $(this);
                    var dialogDiv = $('#packageDialog');
                    dialogDiv.dialog("option", "position", [300, 120]);
                    dialogDiv.dialog('open');
                    return false;
                }


            });










        }
        );


                    }


                }
            }



            $("#btnCancel").click(
                            function () {

                                $("#packageDialog").dialog('close');
                                ResetControls();
                            }
                            );






            $("#<%=ddlCust.ClientID %>").change(
            function () {
                PaymentReceipt($(this).val());
            }
            );




            $("#btnSavePrint").click(
           function () {

               InsertUpdatePaymentBilling();


           }
        );




            function InsertUpdatePaymentBilling() {


                var RecNowPrefix = $("#hdnbillnowprefix").val();
                var Receiptno = $("#txtBillNo").val();
                if (Receiptno == "Auto") {
                    Receiptno = "0";
                }
                var Receiptdate = $("#txtBillDate").val();
                var paymode = $("#ddlpaymode").val();
                var outstanding = $("#txtoutstanding").val();
                if (outstanding == "") {
                    alert("Please Enter Outstanding Amount");
                    $("#txtoutstanding").focus();
                    return;
                }
                var received = $("#txtreceived").val();
                if (received == "") {
                    alert("Please Enter Received Amount");
                    $("#txtreceived").focus();
                    return;
                }
                var Customerid = $("#<%=ddlCust.ClientID %>").val();
                if (Customerid == "") {
                    alert("Please Select Customer");
                    $("#txtClientId").focus();
                    return;
                }
                var Cname = $("#<%=ddlCust.ClientID %>").text();
                var DDChequeNo = $("#Text6").val();
                if (DDChequeNo == "") {
                    if ($("#ddlpaymode").val() == "CHEQUE") {
                        alert("Please Enter Cheque No");
                        $("#Text6").focus();
                        return;
                    }
                    else {
                        DDChequeNo = "";
                    }

                }

                var bank = $("#<%=ddlBank.ClientID %>").val();
                if (bank == "0") {


                    if ($("#ddlpaymode").val() == "CHEQUE") {
                        alert("Please Select Bank");
                        $("#<%=ddlBank.ClientID %>").focus();
                        return;
                    }
                    else {
                        bank = "0";
                    }

                }
                var bank1 = $("#<%=ddlcreditbank.ClientID %>").val();
                if (bank1 == "0") {


                    if ($("#ddlpaymode").val() == "CREDITCARD") {
                        alert("Please Select Bank");
                        $("#<%=ddlcreditbank.ClientID %>").focus();
                        return;
                    }
                    else {
                        bank1 = "0";
                    }

                }


                var CrCardNo = $("#Text8").val();
                if (CrCardNo == "") {
                    if ($("#ddlpaymode").val() == "CREDITCARD") {
                        alert("Please Enter Credit Card No");
                        $("#Text8").focus();
                        return;
                    }
                    else {
                        CrCardNo = "";
                    }
                }


                var BankName = "";
                var CHQNo = "";
                if ($("#ddlpaymode").val() == "CASH") {
                    BankName = "";
                    CHQNo = "";
                }
                else if ($("#ddlpaymode").val() == "CHEQUE") {
                    BankName = bank;
                    CHQNo = DDChequeNo;
                }
                else if ($("#ddlpaymode").val() == "CREDITCARD") {
                    BankName = bank1;
                    CHQNo = CrCardNo;
                }

                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                $.ajax({
                    type: "POST",
                    data: '{"ReceiptNo":"' + Receiptno + '","ReceiptDate":"' + Receiptdate + '","CCode":"' + Customerid + '","CName":"' + Cname + '","Amount":"' + received + '","ModeofPayment":"' + paymode + '","BankName":"' + BankName + '","CHqNo":"' + CHQNo + '"}',
                    url: "PaymentReceipt.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        if (Receiptno == 0) {

                            alert("Payment Receipt added successfully.");

                        }
                        else {

                            alert("Payment Receipt Updated successfully.");

                        }










                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        $("#packageDialog").dialog('close');
                        $.uiUnlock();
                        ResetControls();

                    }



                });



            }

            PayMode();

            $("#ddlpaymode").change(
            function () {

                PayMode();
            }
            )


            function PayMode() {
                if ($("#ddlpaymode").val() == "CASH") {

                    $("#Text6").val("");
                    $("#Text6").prop('readonly', true);
                    $("#<%=ddlBank.ClientID %>").val("");
                    $("#<%=ddlBank.ClientID %>").prop('disabled', true);
                    $("#Text8").val("");
                    $("#Text8").prop('readonly', true);
                    $("#<%=ddlcreditbank.ClientID %>").val("");
                    $("#<%=ddlcreditbank.ClientID %>").prop('disabled', true);
                }
                else if ($("#ddlpaymode").val() == "CREDITCARD") {

                    $("#Text6").val("");
                    $("#Text6").prop('readonly', true);
                    $("#<%=ddlBank.ClientID %>").val("0");
                    $("#<%=ddlBank.ClientID %>").prop('disabled', true);
                    $("#Text8").prop('readonly', false);
                    $("#<%=ddlcreditbank.ClientID %>").prop('disabled', false);
                }
                else if ($("#ddlpaymode").val() == "CHEQUE") {

                    $("#Text6").prop('readonly', false);
                    $("#<%=ddlBank.ClientID %>").prop('disabled', false);

                    $("#Text8").val("");
                    $("#Text8").prop('readonly', true);
                    $("#<%=ddlcreditbank.ClientID %>").val("");
                    $("#<%=ddlcreditbank.ClientID %>").prop('disabled', true);
                }
            }




            $('#txtBillDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $("#btnGo").click(
                     function () {
                         

                         BindGrid();

                     }
                      );




            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

        }
        );
    </script>
   
    
 <div id="dvAddOn"></div>
<div class="right_col" role="main">
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Payment Receipt</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group" margin-bottom:10px">

                                 <div class="form-group">
                                
                                <table>
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  > <i class="fa fa-search"></i></div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo2">
                </table>
                <div id="jQGridDemoPager2">
                </div>

                </div>
                                </div>


                                   <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <table>

                                                    <tr>

                                                        <td>
                                                              <div id="btnShowScreen" style="display: none;" class="btn btn-primary"><i class="fa fa-external-link"></i>New</div>
                                                        </td>
                                                        <td>
                                                            <div  id="btnEdit" style="display:none;" class="btn btn-success" > <i class="fa fa-edit m-right-xs"></i>Edit</div>
                                                        </td>
                                                        <td>

                                                             <div id="btnDelete" style="display:none;" class="btn btn-danger">
                                                           <i class="fa fa-trash m-right-xs"></i>Cancel Bill</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                              
                                                
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     








                    
                    <script type="text/javascript">


                        $(document).ready(function () {


                          




                            $("#txtDateFrom,#txtDateTo,#txtBillDate").val($("#<%=hdnDate.ClientID%>").val());

                            BindGrid();



                           
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>





<div id="packageDialog" style="display:none" title="Payment Receipt">
  <table width="100%" cellpadding="3" >
                     <tr>
                     <td align="center">
                   
                     </td>
                     </tr>

                     <tr>
                     <td>
                     <table style="width:100%">
                     <tr>
                     <td valign="top"  >
                     <table  cellpadding="3" style="border:solid 1px silver;height: 140px;width:100%">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px;">
                         <td colspan="100%">Bill Information</td>
                         </tr>


                     <tr>

                     <td>
                     <table cellpadding="0" cellspacing="0">

                          <tr>
                   
                     <td>Receipt No:</td><td style="padding-left:10px" ><input type="text"  readonly ="readonly" id="txtBillNo"  class="form-control input-small" value="Auto" style="width:130px;height:25px;background-color:White"  /></td>
                     
                   
                     <td>Receipt Date:</td><td style="padding-left:10px" ><input type="text"    class="form-control input-small" style="width:130px;height:25px;background-color:White" readonly="readonly"  id="txtBillDate"/></td>
                     </tr>

                     <tr><td>Customer</td><td style="padding-left:10px"><asp:DropDownList ID = "ddlCust" runat = "server" Width="130px"></asp:DropDownList></td>
                   
                   
                     <td>Outstanding:</td><td style="padding-left:10px" ><input disabled="disabled" type="text" id="txtoutstanding" class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                     </tr>
                     
                      <tr>
                   
                     <td>Received Amt:</td><td style="padding-left:10px" ><input type="text" id="txtreceived"  class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                     
                   
                     <td>PayMode:</td><td style="padding-left:10px" ><select id="ddlpaymode" style="height:30px;width:130px"><option value="CASH">CASH</option><option value="CREDITCARD">CREDIT CARD</option><option value="CHEQUE">CHEQUE</option></select></td>
                     </tr>

                     </table>
                     </td>
                     </tr>

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     
                     </td>
                     
                     
                

                     </tr>
                     </table>


               

                     <tr>
                     <td  >

                    <table>
                    <tr>
      
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table cellpadding="5" style="border:solid 1px silver">
                      <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">Detail</td>
                         </tr>
                     
                     <tr><td>Bank:</td><td><asp:DropDownList ID="ddlBank"  runat="server" class="form-control input-small" style="width:120px;height:25px;background-color:White"></asp:DropDownList></td><td>DD/ChequeNo:</td><td><input type="text"   class="form-control input-small" style="width:120px;height:25px;" id="Text6"/></td></tr>
                     
                     <tr><td>Credit Card Bank:</td><td><asp:DropDownList ID="ddlcreditbank"  runat="server" class="form-control input-small" style="width:120px;height:25px;background-color:White"></asp:DropDownList></td><td>CrCardNo:</td><td><input type="text"   class="form-control input-small" style="width:120px;height:25px;" id="Text8"/></td></tr>
               


          

                    
                    
                     </table>
                     </td>
                     <td valign="top">
                     <table>
                     <tr><td>  <div id="btnSavePrint"  class="btn btn-primary btn-small" style="width:100px;height:29px" >Save</div></td></tr>
                    <%-- <tr><td>  <div id="btnSave"  class="btn btn-primary btn-small"  style="width:100px;height:29px">Save </div></td></tr>
                     <tr><td>  <div id="btnPrint"  class="btn btn-primary btn-small"  style="width:100px;height:29px">Print </div></td></tr>
                   --%>
                     <tr><td>  <div id="btnCancel" style="background-color:Maroon;width:100px;height:29px" class="btn btn-primary btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>
                     
                     </td>
                     </tr>
                     </table></td>
                    </tr>
                    </table> 
                     
                     </td>
                     </tr>
                     </table>
  
  </div>

            
    </form>

     
  <script language="javascript" type="text/javascript">



     function BindGrid()
     { 
   
     var DateFrom=$("#txtDateFrom").val();
    
     var DateTo=$("#txtDateTo").val();
    
   

         jQuery("#jQGridDemo2").GridUnload();
        jQuery("#jQGridDemo2").jqGrid({
            url: 'handlers/PaymentBilling.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo+'',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ReceiptNo','ReceiptDate','CustomerId','CustomerName', 'Mode', 'ReceivedAmount','BankName', 'ChequeNo'],
            colModel: [
                        { name: 'Receipt_No', key:true, index: 'Receipt_No', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'strChequedate', index: 'strChequedate', width: 100, stype: 'text',sorttype:'int',hidden:false },
                        { name: 'CCode', index: 'CCode', width: 100, stype: 'text',sorttype:'int',hidden:false },
   		                { name: 'CName', index: 'CustomerName', width: 100, stype: 'text',sorttype:'int',hidden:false },
                        { name: 'ModeOfPayment', index: 'ModeOfPayment', width: 70, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 { name: 'Amount', index: 'Amount', width: 70, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'Bank_Name', index: 'Bank_Name', width: 70, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                { name: 'CHQNO', index: 'CHQNO', width: 70, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		              
                     
   		                  
                       ],
            rowNum: 10,
          
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager2',
            sortname: 'Receipt_No',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Package Bill History",
         
            editurl: 'handlers/ManageBanks.ashx',
         
                    
             
         toolbar: [true, "top"],
           ignoreCase: true,
      });
           var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });

           
   
      $("#jQGridDemo2").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {



 var RecpNoWPrefix=     $('#jQGridDemo2').jqGrid('getCell', rowid, 'Receipt_No')  

     
               $.ajax({
            type: "POST",
            data: '{"ReceiptNowprefix":"'+RecpNoWPrefix+'"}',
            url: "PaymentReceipt.aspx/GetById",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                     
                var obj = jQuery.parseJSON(msg.d);
              $("#hdnbillnowprefix").val(obj.BillMasterData.Receipt_No);
               $("#txtBillNo").val(obj.BillMasterData.Receipt_No)
              
               $("#txtBillDate").val(obj.BillMasterData.strChequedate);
                $("#txtClientId").val(obj.BillMasterData.CCode);
                $("#ddlpaymode").val(obj.BillMasterData.ModeOfPayment);
                $("#txtreceived").val(obj.BillMasterData.Amount);
             
              if(obj.BillMasterData.ModeOfPayment == "CREDITCARD")
              {
               $("#Text8").val(obj.BillMasterData.CHQNO);
                $("#<%=ddlcreditbank.ClientID %>").val(obj.BillMasterData.Bank_Name);

                $("#Text6").val("");
                 $("#<%=ddlBank.ClientID %>").val(0);

              }
              else if(obj.BillMasterData.ModeOfPayment == "CHEQUE")
              {

              $("#Text8").val("");
                $("#<%=ddlcreditbank.ClientID %>").val(0);
               $("#Text6").val(obj.BillMasterData.CHQNO);
                 $("#<%=ddlBank.ClientID %>").val(obj.BillMasterData.Bank_Name);
              }
              else if(obj.BillMasterData.ModeOfPayment == "CASH")
              {

              $("#Text8").val("");
                $("#<%=ddlcreditbank.ClientID %>").val(0);
               $("#Text6").val("");
                 $("#<%=ddlBank.ClientID %>").val(0);
              }
               
               
               

             
           
                $("#txtoutstanding").val(obj.BillMasterData.OutstandingAmount);
               
                
         
               

            },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   }


        });
 
     

                      
                  
             }
         });


           var DataGrid = jQuery('#jQGridDemo2');
                    DataGrid.jqGrid('setGridWidth', '600');


//        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
//                   {
//                       refresh:false,
//                       edit: false,
//                       add: false,
//                       del: false,
//                       search: false,
//                       searchtext: "Search",
//                       addtext: "Add",
//                     } ,
//                  
//                   {//SEARCH
//                       closeOnEscape: true

//                   }



//                     );

                   
    } 
  </script>
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
     
</asp:Content>

