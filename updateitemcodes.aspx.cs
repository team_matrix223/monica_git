﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

public partial class test : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            BinDDept();
        }
       
    }


    public void BinDDept()
    {
        ddlCat2.DataSource = new PropGroupBLL().GetAll();
        ddlCat2.DataValueField = "Group_ID";
        ddlCat2.DataTextField = "Group_Name";
        ddlCat2.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Group--";
        li1.Value = "0";
        ddlCat2.Items.Insert(0, li1);
    }



    //[WebMethod]
    //public static string GetByItemCode(string ItemCode)
    //{
    //    Products objProduct = new Products() { ItemCode = ItemCode };
     
    //    new ProductsBLL().GetProductDetail(objProduct);

    //    var JsonData = new
    //    {
    //        productData = objProduct
    //    };
    //    JavaScriptSerializer ser = new JavaScriptSerializer();
    //    ser.MaxJsonLength = int.MaxValue;
    //    return ser.Serialize(JsonData);
    //}





    //[WebMethod]
    //public static string BindBrand(int SubCat)
    //{

    //    string Cat2 = new BrandBLL().GetAllBySubCat(SubCat);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Brand = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}

    //[WebMethod]
    //public static string BindAllBrand()
    //{

    //    string Cat2 = new BrandBLL().GetOptions();
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Brand = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}



    //[WebMethod]
    //public static string BindCat1()
    //{

    //    string Cat1 = new CategoryBLL().GetLevel2Options(1);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category1 = Cat1,
    //    };
    //    return ser.Serialize(JsonData);
    //}



    //[WebMethod]
    //public static string BindCat2ByCat1(int CatId)
    //{

    //    string Cat1 = new CategoryBLL().GetOptions(CatId);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category1 = Cat1,
    //    };
    //    return ser.Serialize(JsonData);
    //}


    //[WebMethod]
    //public static string BindCat2()
    //{

    //    string Cat2 = new CategoryBLL().GetLevel2Options(2);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category2 = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}


    [WebMethod]
    public static string GetData(int Dept)
    {
        int TotalRows = 0;
        string html = new DepartmentBLL().GetProductByDept(Dept, out TotalRows);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            HTML = html,
            TR = TotalRows
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetDataByNameOrCode(string Type,string Code,string Name)
    {
        int TotalRows = 0;
        string html = new DepartmentBLL().GetProductByCodeorName(Type,Code,Name, out TotalRows);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            HTML = html,
            TR = TotalRows
        };
        return ser.Serialize(JsonData);
    }



    //[WebMethod]
    //public static string GetProductDetail(string ItemCode, int VariationId)
    //{
    //    int VId = 0;
    //    string HtmlLocal = "";
    //    int OStatus = 0;
    //    int LStatus = 0;
    //    string html = new ProductsBLL().GetProductDetail(ItemCode, VariationId, out VId, out HtmlLocal, out OStatus, out LStatus);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {
    //        HL = HtmlLocal,
    //        HTML = html,
    //        VariationId = VId,
    //        OStatus = OStatus,
    //        LStatus = LStatus
    //    };
    //    return ser.Serialize(JsonData);
    //}

    [WebMethod]
    public static string UpdateVariationDetail(Int64 ItemId, decimal Price, decimal Mrp,decimal wsr,decimal tax,string HSNCode,string Ingredients)
    {

        int status = new DepartmentDAL().UpdatePackingbelongsDetail(ItemId, Price, Mrp,wsr,tax,HSNCode, Ingredients);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string UpdateItemToPackingBelongs()
    {
        SqlDataReader dr = null;
        dr  = new DepartmentDAL().UpdateItemToPackingBelongs();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = dr,

        };
        return ser.Serialize(JsonData);
    }



    //[WebMethod]
    //public static string UpdateCategories(string ItemCode, int Cat1, int Cat2, int BrandId)
    //{

    //    int status = new ProductsDAL().UpdateCategories(ItemCode, Cat1, Cat2, BrandId);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Status = status,

    //    };
    //    return ser.Serialize(JsonData);
    //}


    //[WebMethod]
    //public static string DeActiveVariationDetail(string ItemCode, string VariationId,bool IsActive)
    //{

    //    int status = new ProductsDAL().DeActiveVariationDetail(ItemCode, VariationId,IsActive);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Status = status,

    //    };
    //    return ser.Serialize(JsonData);
    //}








}