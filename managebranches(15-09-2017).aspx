﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebranches.aspx.cs" Inherits="managebranches" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   <script language="javascript" type="text/javascript">
       var m_BranchId = -1;
       function ApplyRoles(Roles) {


           $("#<%=hdnRoles.ClientID%>").val(Roles);
       }


       function ResetControls() {
           m_BranchId = -1;
           var txtTitle = $("#txtTitle");
           var btnAdd = $("#btnAdd");
           var btnUpdate = $("#btnUpdate");
           txtTitle.focus();
           $("#chkIsActive").prop("checked", "checked");
           txtTitle.val("");
           txtTitle.focus();

           var arrRole = [];
           arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
           $("#btnAdd").css({ "display": "none" });
           for (var i = 0; i < arrRole.length; i++) {

               
               if (arrRole[i] == "1")
               {
                 
                  $("#btnAdd").css({ "display": "block" });
               }

               
           }
           

           btnUpdate.css({ "display": "none" });
            

           $("#btnReset").css({ "display": "none" });
           $("#hdnId").val("0");
           validateForm("detach");
       }



       function TakeMeTop() {
           $("html, body").animate({ scrollTop: 0 }, 500);
       }

       function RefreshGrid() {
           $('#jQGridDemo').trigger('reloadGrid');

       }

       function InsertUpdate() {

           if (!validateForm("frmCity")) {
               return;
           }
           var Id = m_BranchId;
           var Title = $("#txtTitle").val();
           var StateId = $("#ddlState").val();
           if ($.trim(Title) == "") {
               $("#txtTitle").focus();

               return;
           }


           var IsActive = false;

           if ($('#chkIsActive').is(":checked")) {
               IsActive = true;
           }

           $.uiLock('');
           $.ajax({
               type: "POST",
               data: '{"BranchId":"' + Id + '", "BranchName": "' + Title + '","IsActive": "' + IsActive + '","StateId":"' + StateId + '"}',
               url: "managebranches.aspx/Insert",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);


                   if (obj.Status == -11) {
                       alert("You don't have permission to perform this action..Consult Admin Department.");
                       return;
                   }
                   if (obj.Status == -1) {

                       alert("Insertion Failed.Branch with duplicate name already exists.");
                       return;
                   }

                   if (Id == "-1") {
                       ResetControls();

                       jQuery("#jQGridDemo").jqGrid('addRowData', obj.branch.BranchId, obj.branch, "last");

                       alert("Branch is added successfully.");
                   }
                   else {
                       ResetControls();

                       var myGrid = $("#jQGridDemo");
                       var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                       myGrid.jqGrid('setRowData', selRowId, obj.branch);
                       alert("Branch is Updated successfully.");
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }

       $(document).ready(
       function () {




           BindGrid();


           
           ValidateRoles();

           function ValidateRoles() {

               var arrRole = [];
               arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

               for (var i = 0; i < arrRole.length; i++) {
                   if (arrRole[i] == "1") {

                       $("#btnAdd").show();
                       $("#btnAdd").click(
                       function () {


                           m_BranchId = -1;
                           InsertUpdate();
                       }
                       );

                   }

                   else if (arrRole[i] == "3") {

                       $("#btnUpdate").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Branch is selected to Edit");
               return;
           }

           InsertUpdate();
       }
       );


                   }

               }

           }


       




           $('#txtTitle').focus();
           $('#txtTitle').keypress(function (event) {


               var keycode = (event.keyCode ? event.keyCode : event.which);

               if (keycode == '13') {

                   InsertUpdate();
               }


           });


           $("#btnReset").click(
           function () {

               ResetControls();

           }
           );


       }
       );

</script>


<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Branches</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Branch</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                     <tr><td class="headings">Title:</td><td>  <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 213px"/></td></tr>
                    <tr> <td class="headings">State:</td> <td> <asp:DropDownList Height="30px"  class="form-control " ID="ddlState" ClientIDMode="Static"  runat="server"></asp:DropDownList></td></tr>
                      
                       <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                      
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;" class="btn btn-primary btn-small" > <i class="fa fa-external-link"></i> Add  </div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i> Update  </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >
<i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Branches</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave"  >
                    
      	          <table id="jQGridDemo">
    </table>

     
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

              

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageBranches.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Branch Id', 'Name', 'IsActive','StateId'],
                        colModel: [
                                    { name: 'BranchId', key: true, index: 'BranchId', width: 100, stype: 'text', sorttype: 'int', hidden: true },

                                    { name: 'BranchName', index: 'BranchName', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },


                                     { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                                     { name: 'StateId', key: true, index: 'StateId', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'BranchId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Branchs List",

                        editurl: 'handlers/ManageBranches.ashx',
                        ignoreCase: true,
                        toolbar: [true, "top"],


                    });


                    var $grid = $("#jQGridDemo");
                    // fill top toolbar
                    $('#t_' + $.jgrid.jqID($grid[0].id))
                        .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                    $("#globalSearchText").keypress(function (e) {
                        var key = e.charCode || e.keyCode || 0;
                        if (key === $.ui.keyCode.ENTER) { // 13
                            $("#globalSearch").click();
                        }
                    });
                    $("#globalSearch").button({
                        icons: { primary: "ui-icon-search" },
                        text: false
                    }).click(function () {
                        var postData = $grid.jqGrid("getGridParam", "postData"),
                            colModel = $grid.jqGrid("getGridParam", "colModel"),
                            rules = [],
                            searchText = $("#globalSearchText").val(),
                            l = colModel.length,
                            i,
                            cm;
                        for (i = 0; i < l; i++) {
                            cm = colModel[i];
                            if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                                rules.push({
                                    field: cm.name,
                                    op: "cn",
                                    data: searchText
                                });
                            }
                        }
                        postData.filters = JSON.stringify({
                            groupOp: "OR",
                            rules: rules
                        });
                        $grid.jqGrid("setGridParam", { search: true });
                        $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                        return false;
                    });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_BranchId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");


                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                      $("#btnUpdate").css({ "display": "none" });
                      $("#btnReset").css({ "display": "none" });
                      $("#btnAdd").css({ "display": "none" });

                      for (var i = 0; i < arrRole.length; i++) {

                          if (arrRole[i] == 1) {

                              $("#btnAdd").css({ "display": "block" });
                          }

                          if (arrRole[i] == 3) {
                              m_BranchId = $('#jQGridDemo').jqGrid('getCell', rowid, 'BranchId');


                              txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'BranchName'));

                               $("#ddlState").val($('#jQGridDemo').jqGrid('getCell', rowid, 'StateId'))

                              if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                  $('#chkIsActive').prop('checked', true);
                              }
                              else {
                                  $('#chkIsActive').prop('checked', false);

                              }
                              txtTitle.focus();
                              $("#btnAdd").css({ "display": "none" });
                              $("#btnUpdate").css({ "display": "block" });
                              $("#btnReset").css({ "display": "block" });

                          }
                      }
                    TakeMeTop();
                }
            });

                    var DataGrid = jQuery('#jQGridDemo');
                    DataGrid.jqGrid('setGridWidth', '500');

                    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                                     {
                                         refresh: false,
                                         edit: false,
                                         add: false,
                                         del: false,
                                         search: false,
                                         searchtext: "Search",
                                         addtext: "Add",
                                     },

                                     {//SEARCH
                                         closeOnEscape: true

                                     }

                                       );



                }





    </script>


</asp:Content>

