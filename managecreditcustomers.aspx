﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managecreditcustomers.aspx.cs" Inherits="managecreditcustomers" %>

<%@ Register src="~/Templates/CreditCustomers.ascx" tagname="AddCreditCustomer" tagprefix="uc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" Runat="Server">
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   <script language="javascript" type="text/javascript">

       var m_CustomerID = 0;

       function ApplyRoles(Roles) {

           $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

       $(document).ready(function () {


           $("#btnSearch").click(
   function () {

       var txt = $("#txtSearch");
       if (txt.val().trim() == "") {
           txt.focus();
           return;
       }

       BindGrid();
   }
   );
           ValidateRoles();

           function ValidateRoles() {

               var arrRole = [];
               arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

               for (var i = 0; i < arrRole.length; i++) {
                   if (arrRole[i] == "9") {

                       $("#btnNew").show();

                       $("#btnNew").click(
                                        function () {

                                            $("#CustomerDialog").dialog({
                                                autoOpen: true,

                                                width: 800,
                                                resizable: false,
                                                modal: true
                                            });
                                        }
                                        );
                   }
                   else if (arrRole[i] == "3") {
                       $("#btnEdit").show();
                       $("#btnEdit").click(
                function () {

                    if (m_CustomerID == "") {
                        alert("No Customer is selected for editing");
                        return;
                    }


                    var CustomerId = m_CustomerID;

                    BindInformation(CustomerId);



                }
                          );


                   }

                   else if (arrRole[i] == "2") {

                       $("#btnDelete").show();
                       $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Customer is selected to Delete");
               return;
           }

           var CustomerId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'AccountId')
           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');


               $.ajax({
                   type: "POST",
                   data: '{"AccountId":"' + CustomerId + '"}',
                   url: "managecreditcustomers.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       BindGrid();
                       alert("Customer is Deleted successfully.");




                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }


       }
       );
                   }
               }
           }


         

           


       });
   
   </script>
   
   <style type="text/css">
   .form-control
   {
   padding:0px 2px 0px;
   }
   
   </style>
<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:HiddenField ID="hdnDate" runat="server"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Customers</h3>
                        </div>
                       
                    



                    <div class="x_panel">
   
   
                    <div class="x_title">
                            <h2>Search Filter</h2>
                             
                            <div class="clearfix"></div>
                        </div>
   
   
   <div class="form-group">
                            


                    <div id="dvSearch">
            <table width="100%" style="margin-top:15px"  >
                <tr>
                    <td>
                        <div id="dvLeft" style="float: left; width: 36%; margin-left:19px; border: 1px solid silver">
                            <table cellpadding="5" cellspacing="0" width="100%"  class="table" style="margin:0px">
                                <tr style="background-color: #E6E6E6">
                                    <td colspan="100%" style="padding:6px">
                                        Search On:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" id="rbPhoneNo" value="M" name="searchon" />
                                        <label for="rbPhoneNo" style="font-weight: normal">
                                            Phone No.</label>
                                    </td>
                                    <td>
                                        <input type="radio" id="rbCustomerName" value="N" checked="checked" name="searchon" />
                                        <label for="rbCustomerName" style="font-weight: normal">
                                            Customer Name</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="dvRight" style="float: right; width: 58%; margin-right:15px; border: 1px solid silver">
                            <table cellpadding="5" cellspacing="0" width="100%"  class="table" style="margin:0px">
                                <tr style="background-color: #E6E6E6">
                                    <td colspan="100%"  style="padding:6px">
                                        Search Criteria:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                        <label for="rbStartingWith" style="font-weight: normal">
                                            Starting With</label>
                                    </td>
                                    <td>
                                        <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                        <label for="rbContaining" style="font-weight: normal">
                                            Containing</label>
                                    </td>
                                    <td>
                                        <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                        <label for="rbEndingWith" style="font-weight: normal">
                                            Ending With</label>
                                    </td>
                                    <td>
                                        <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                        <label for="rbExact" style="font-weight: normal">
                                            Exact</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="margin-left:19px;">
                            <tr>
                                <td>
                                    Type to Search:
                                </td>
                                <td style="padding-top:12px">
                                 
                                <div class="input-group">
                                                    <input type="text" class="form-control" id="txtSearch" style="width:400px;height:34px">
                                                    <span class="input-group-btn">
                                            <button id="btnSearch" class="btn btn-primary" type="button" style="padding:9px"><i class="fa fa-search m-right-xs"></i>
    </button> 
                                        </span>
                                                </div>
                               
                               
                                </td>
           
                                <td><div id="btnNew" style="display:none;"  class="btn btn-primary btn-small"  ><i class="fa fa-external-link"></i> New</div>
   </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
            </table>
        </div>
        </div>
        </div>







                    <div class="x_panel">


                   <%-- <div class="x_title">
                            <h2>Manage Credit Customers</h2>
                             
                            <div class="clearfix"></div>
                        </div>--%>

                         <div class="form-group">
                                
                             

                <div class="youhave" style="padding-left:30px">
                               <div id="dvHNoCustomer" style="display:block;margin:auto;width:200px;">No Customers in List.....</div>
      	          <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0"  id="tbButtons" style="display:none;margin-top:5px;">
                                            <tr>
                                       <td style="padding:5px"> <div id="btnEdit" style="display:none;"  class="btn btn-danger" ><i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                            <td>&nbsp;</td><td style="padding:5px"> <div id="btnDelete" style="display:none;"  class="btn btn-success" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
                </div>
                            
<div class="row" id="CustomerDialog" style="display:none">
<uc1:AddCreditCustomer ID="ucAddCreditCustomer" runat="server" />
</div>


                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>
</div>

 
</form>


 <script type="text/javascript">
                function BindGrid() {


                    
         $("#tbButtons").css("display", "block");
    $("#dvHNoCustomer").css("display", "none");

    var searchon=$("input[name='searchon']:checked").val();
    var criteria=$("input[name='searchcriteria']:checked").val();
    var stext=$("#txtSearch").val();


                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                            url: 'handlers/CreditCustomerSearch.ashx?searchon='+searchon+'&criteria='+criteria+'&stext='+stext+'',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Account Id','CODE','CCODE','H_CODE','S_CODE','SS_CODE','CNAME','OP. BAL','DR/CR'],
  


                        colModel: [
                                    { name: 'AccountId', key: true, index: 'AccountId', width: 100, stype: 'text', sorttype: 'int', hidden: false
                                    },
                                  
                                    { name: 'CODE', index: 'CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'CCODE', index: 'CCODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'H_CODE', index: 'H_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'S_CODE', index: 'S_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'SS_CODE', index: 'SS_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'CNAME', index: 'CNAME', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                   { name: 'OP_BAL', index: 'OP_BAL', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
 
                                    { name: 'DR_CR', index: 'DR_CR', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'AccountId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Accounts List",

                        editurl: 'handlers/ManageAccounts.ashx',

                           ignoreCase: true,
                         toolbar: [true, "top"],

                    });


                     var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });


                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_CustomerID = 0;

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                         $("#btnEdit").css({ "display": "none" });
                         $("#btnDelete").css({ "display": "none" });
                         $("#btnNew").css({ "display": "none" });
                  

                         for (var i = 0; i < arrRole.length; i++) {

                             if (arrRole[i] == 9) {

                                 $("#btnNew").css({ "display": "block" });
                             }
                             if (arrRole[i] == 2) {

                                 $("#btnDelete").css({ "display": "block" });
                             }
                             if (arrRole[i] == 3) {
                                 $("#btnEdit").css({ "display": "block" });
                                 m_CustomerID = $('#jQGridDemo').jqGrid('getCell', rowid, 'AccountId');
                             }
          

             
                }
				}
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


            </script>



</asp:Content>


