﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managesettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {

        }
    }

    [WebMethod]
    public static string Insert(string Apptype)
    {
        Settings objSettings = new Settings()
        {
           Apptype = Apptype

        };
        string status = new SettingsBLL().InsertUpdate(objSettings);
        var JsonData = new
        {
            Settings = objSettings,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}