﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class manageaccounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    [WebMethod]
    public static string BindAccGroups(string BALINC)
    {

       var AccountGroups =  new AccGroupsBLL().GetAccGroupsByBALINC(BALINC);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            AccGroupOptions = AccountGroups

        };
        return ser.Serialize(JsonData);
    }
}